<?php

class ControllerCommonShop extends Controller
{
    use Device_Util;
    use Theme_Config_Util;
    use Product_Util_Trait;

    public function index()
    {
        $this->load->language('common/shop');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('catalog/collection');
        $this->load->model('catalog/manufacturer');
        $this->load->model('tool/image');
        $this->document->setTitle($this->language->get('text_shop'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));

        if (isset($this->request->get['route'])) {
            $this->document->addLink($this->config->get('config_url'), 'canonical');
        }

        $data['theme_directory'] = '/catalog/view/theme/' . $this->getCurrentThemeDir();
        $data['is_on_mobile'] = $this->isMobile() ? 1 : 0;

        $this->load->model('extension/module/theme_builder_config');

        // banner
        $shop_banner_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_BANNER);
        $shop_banner_config = json_decode($shop_banner_config, true);
        if (isset($shop_banner_config['display'])) {
            $data['banner'] = reset($shop_banner_config['display']);
        }
        //category
        $product_category_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_PRODUCT_CATEGORY);
        $product_category_config = json_decode($product_category_config, true);

        if (isset($product_category_config['setting']['title'])) {
            $data['category_title'] = $product_category_config['setting']['title'];
        }
        if (isset($product_category_config['display']['menu']['id'])) {
            $this->load->model('custom/common');
            $category_lists = $this->model_custom_common->getListMenuItemByGroupMenuId($product_category_config['display']['menu']['id']);
            $data['category_lists'] = $this->categoryMapList($category_lists);
        }

        // manufacture, prime  (category filter)
        $category_filter_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_FILTER);
        $category_filter_config = json_decode($category_filter_config, true);

        if (isset($category_filter_config['display']['supplier']['title'])) {
            $data['manufacture_title'] = $category_filter_config['display']['supplier']['title'];
        }
        $data['manufacture_visible'] = $category_filter_config['display']['supplier']['visible'];
        // $manufactures = $this->model_catalog_product->getManufacturer(); // TODO: remove...
        // $manufactures = $this->model_catalog_manufacturer->getManufacturersHadProduct(); // TODO: use this instead of all manufacturers?...
        $manufactures = $this->model_catalog_manufacturer->getManufacturers(false ,false);
        foreach ($manufactures as &$manufacture) {
            $manufacture['url'] = $this->url->link('common/shop', 'manufacture=' . $manufacture['manufacturer_id'], true);
        }
        $data['manufactures'] = $manufactures;
        unset($manufactures);

        if (isset($category_filter_config['display']['product-price']['title'])) {
            $data['prime_title'] = $category_filter_config['display']['product-price']['title'];
        }
        $data['prime_visible'] = $category_filter_config['display']['product-price']['visible'];
        $prime = array();
        if (isset($category_filter_config['display']['product-price']['range'])) {
            $prime = $category_filter_config['display']['product-price']['range'];
        }
        //$data['primes'] = $this->getPrime($prime);

        //product
        $url = '';
        $filter = [];
        $valueoptiton_post = (isset($this->request->get['valueoptiton']) ? $this->request->get['valueoptiton'] : '');
        if (isset($this->request->get['valueoptiton'])) {
            $url .= '&valueoptiton=' . $valueoptiton_post;
            $filter['valueoptiton'] = $valueoptiton_post;
            $data['valueoptiton'] = $valueoptiton_post;
        }
        if (isset($this->request->get['search'])) {
            $txtSearch = trim($this->request->get['search']);
            $url .= '&search=' . $txtSearch;
            $filter['search'] = $txtSearch;
            $data['getsearch'] = $txtSearch;
        }
        if (isset($this->request->get['path'])) {
            $url .= '&path=' . $this->request->get['path'];
            $parts = explode('_', $this->request->get['path']);
            $filter['filter_category_id'] = (int)end($parts);
            $data['filter_category_id'] = $filter['filter_category_id'];
            $filter['filter_sub_category'] = true;

            //get category name by path
            $path_category = $this->model_catalog_category->getCategory($data['filter_category_id']);
            if (isset($path_category['name'])) {
                $data['category_name'] = $path_category['name'];
            }
        }
        if (isset($this->request->get['manufacture'])) {
            $url .= '&manufacture=' . $this->request->get['manufacture'];
            $filter['filter_manufacturer_id'] = $this->request->get['manufacture'];
            $data['filter_manufacturer_id'] = $filter['filter_manufacturer_id'];
        }
        if (isset($this->request->get['collection'])) {
            $url .= '&collection=' . $this->request->get['collection'];
            $filter['filter_collection_id'] = $this->model_catalog_product->getCollectionList($this->request->get['collection']);
            $data['filter_collection_id'] = $this->request->get['collection'];
        }
        if (isset($this->request->get['nametag'])) {
            $url .= '&nametag=' . $this->request->get['nametag'];
            $filter['filter_nametag'] = $this->request->get['nametag'];
            $data['filter_nametag'] = $filter['filter_nametag'];
        }
        if (isset($this->request->get['min'])) {
            $url .= '&min=' . $this->request->get['min'];
            $filter['price_min'] = $this->request->get['min'];
            $data['price_min'] = $filter['price_min'];
        }
        if (isset($this->request->get['max'])) {
            $url .= '&max=' . $this->request->get['max'];
            $filter['price_max'] = $this->request->get['max'];
            $data['price_max'] = $filter['price_max'];
        }
        if (isset($this->request->get['attribute_name']) && isset($this->request->get['attribute_value'])) {
            $url .= '&attribute_name=' . $this->request->get['attribute_name'] . '&attribute_value=' . $this->request->get['attribute_value'];
            $filter['attribute_name'] = $this->request->get['attribute_name'];
            $filter['attribute_value'] = $this->request->get['attribute_value'];
            $data['filter_attribute_name'] = $filter['attribute_name'];
            $data['filter_attribute_value'] = $filter['attribute_value'];
        }
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }
        if (isset($this->request->get['limit'])) {
            $limit = $this->request->get['limit'];
            $url .= '&limit=' . $limit;
        } else {
            $limit = $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit') ? $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit') : $this->config->get('config_limit_admin');
        }
        $filter['start'] = ($page - 1) * (int)$limit;
        $filter['limit'] = $limit;
        $data['limit'] = $limit;

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => "Trang chủ",
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => "Sản phẩm",
            'href' => $this->url->link('common/shop')
        );

        $categoryFull = !empty($filter['filter_category_id']) ? array_reverse($this->model_catalog_category->getAllParentsCategoryById($filter['filter_category_id'])) : [];

        if (!empty($categoryFull)) {
            $length = count($categoryFull);
            foreach ($categoryFull as $key => $item) {
                if ($key == $length - 1) {
                    $data['breadcrumbs'][] = array(
                        'text' => $item['name'],
                        'active' => true
                    );

                    break;
                }

                $data['breadcrumbs'][] = array(
                    'text' => $item['name'],
                    'href' => $item['href']
                );
            }
        }

        // TODO: getting products uses $this->getProductsByFilter() instead...
        // see controller/api/v1_products->products() for more details
        $products = $this->model_catalog_product->getProductsCustomOptimize($filter);
        $customer_total = $this->model_catalog_product->getTotalProductsCustomOptimize($filter);
        $data['start_product'] = ($page - 1) * (int)$limit + 1;
        $data['end_product'] = $data['start_product'] + (int)$limit - 1;
        $data['total_product'] = $customer_total;
        $data['currency'] = $this->session->data['currency'];

        $best_sales_products = $this->model_catalog_product->getProductBestSales();
        $new_products = $this->model_catalog_product->getProductNews();

        $data['products'] = array();

        foreach ($products as $product) {
            $p_image = '';
            if ($product['multi_versions']) {
                if (!empty($product['pv_image'])) {
                    $p_image = $product['pv_image'];
                } else {
                    $p_image = $product['image'];
                }
            } else {
                $p_image = $product['image'];
            }

            if ($p_image) {
//                $image = $this->model_tool_image->resize($product['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                $image = $this->model_tool_image->resize($p_image, 500, 500);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
            }
            if ($product['multi_versions'] == 1) {
                $SumProduct_version = $this->model_catalog_product->getSumProduct_version($product['product_id']);
            } else {
                $SumProduct_version = $product['quantity'];
            }

            $categories = $this->model_catalog_product->getCategoriesCustom($product['product_id']);
            if ($categories) {
                foreach ($categories as $category) {
                    $categories[] = [
                        'name' => $category['name'],
                        'image' => $category['image'],
                        'href' => $this->url->link('common/shop', 'category=' . $category['category_id'], true),
                    ];
                }
            }

            // TODO: for what reason?...
            $category_name = $categories;
            $name_cate = (!empty($category_name[0]['name']) ? $category_name[0]['name'] : '');

            /** Get other image */
            $product_images = $this->model_catalog_product->getProductImages($product['product_id']);
            $product_images = empty($product_images) || !is_array($product_images) ? [] : $product_images;
            foreach ($product_images as &$product_image) {
                $product_image['image'] = $this->resizeImage($product_image['image'], 2000, 2000); // Avoid breaking when zooming in, TODO ...
            }
            unset($product_image);
            //add image default
            $product_image_default = [
                'image' => $image
            ];
            array_unshift($product_images, $product_image_default);
            $product_image_empty = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));
            /* fix if product has only image */
            $alternative_product_images = (isset($product_images[0]['image']) ? $product_images[0]['image'] : $product_image_empty);
            $other_image_product = (isset($product_images[1]['image']) ? $product_images[1]['image'] : $alternative_product_images);

            /* percent sale */
            $percent_sale = empty($product['max_price_version']) ? getPercent($product['price_master'], $product['compare_price_master']) : getPercent($product['min_price_version'], $product['min_compare_price_version'], $product['max_percent_pv']);
            $percent_sale_value = empty($product['max_price_version'])
                ? getPercent($product['price_master'], $product['compare_price_master'], 0, "")
                : getPercent($product['min_price_version'], $product['min_compare_price_version'], $product['max_percent_pv'], "");

            if ((float)$product['price_discount'] == 0 && !empty($product['discount_id'])) {
                $percent_sale = -100;
                $percent_sale_value = '-100%';
            }

            //$product_version_id = (int)$product['product_version_id']; // NOT use, TODO: remove...
            $product_id = $product['product_id'];

            // Add-on Deal
            $this->load->model('discount/add_on_deal');
            $hasDeal = false;
            $dealName = $this->model_discount_add_on_deal->getAddOnDealNameByProductId($product['product_id']);

            if ($dealName) {
                $hasDeal = true;
            }

            $pv_id = '';
            if (!empty($product['product_version_id'])) {
                $pv_id = $product['product_version_id'];
            }

            if (!empty($product['product_version_id_2'])) {
                $pv_id = $product['product_version_id_2'];
            }

            $param_text = !empty($pv_id) ? "&variant={$pv_id}" : '';

            $new_pd = array(
                'product_id' => $product['product_id'],
                'is_new_product' => in_array($product['product_id'], $new_products) ? true : false,
                'is_best_sale_product' => in_array($product['product_id'], $best_sales_products) ? true : false,
                'thumb' => $this->changeUrl($image, true, 'product'),
                'name' => $product['name'],
                'multi_versions' => $product['multi_versions'],
                'quantity' => ($SumProduct_version > 0 || $product['sale_on_out_of_stock'] == 1) ? 1 : 0,
                'description' => utf8_substr(trim(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                'sub_description' => $product['sub_description'],
                'price_compare_zero' => $product['price'],
                'price' => $this->currency->formatCustom($product['price_master'], $this->session->data['currency']),
                'compare_price' => $this->currency->formatCustom($product['compare_price_master'], $this->session->data['currency']),
                'price_version_check_null' => (int)$product['price_version_check_null'],
                'price_version_min' => $this->currency->formatCustom($product['min_price_version'], $this->session->data['currency']),
                'price_version_max' => $this->currency->formatCustom($product['max_price_version'], $this->session->data['currency']),
                'compare_version_min' => $this->currency->formatCustom($product['min_compare_price_version'], $this->session->data['currency']),
                'compare_version_max' => $this->currency->formatCustom($product['max_compare_price_version'], $this->session->data['currency']),
                'price_2_text' => $this->currency->formatCustom($product['price_2'], $this->session->data['currency']),
                'compare_price_2_text' => $this->currency->formatCustom($product['compare_price_2'], $this->session->data['currency']),
                'price_2' => $product['price_2'],
                'compare_price_2' => $product['compare_price_2'],
                /* TODO: remove if check is_stock ok */
                //'product_is_stock' => (($product['product_version_status_max'] !== "0" && $product['sale_on_out_of_stock'] == 1) || ($product['product_version_status_max'] !== "0" && !empty($product['product_version_id']) && (int)$product['product_version_quantity'] > 0) || (empty($product['product_version_id']) && (int)$product['product_master_quantity'] > 0)) ? true : false,
                'product_is_stock' => $this->model_catalog_product->isStock($product_id), // $product_version_id null for all versions
                'Percent_protduct' => $percent_sale,
                'percent_sale' => $percent_sale,
                'percent_sale_value' => $percent_sale_value,
                'nameCategory' => (!empty($name_cate) ? $name_cate : ''),
                'manufacturer' => $product['manufacturer'],
                'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'] . $param_text),
                'categories' => $categories,
                'product_images' => $this->changeUrl($other_image_product),
                'price_discount' => $product['p_price_discount'],
                'discount_id' => $product['multi_versions'] ? $product['pv_discount_id'] : $product['p_discount_id'],
                'has_deal' => $hasDeal,
                'deal_name' => $dealName ?? '',
            );

            $new_pd['is_contact_for_price'] = $this->isContactForPrice($new_pd['multi_versions'], $new_pd['compare_price'], $new_pd['compare_version_min']);

            $data['products'][] = $new_pd;
        }

        $pagination = new CustomPaginate();
        $pagination->total = $customer_total;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->url = $this->url->link('common/shop', $url . '&page={page}', true);
        $data['customer_total'] = $customer_total;
        $data['pagination'] = $pagination->render();

        $data['urlsortProduct'] = $this->url->link('common/shop', modQuery($url, 'valueoptiton') . '', true);

        // filter cho một số them không có column left
        $data['filter'] = $this->getFilterProduct();
        /** ---------------- */
        $data['visible_supplier'] = $data['filter']['visible_supplier']['visible'];
        $data['visible_supplier_title'] = $data['filter']['visible_supplier']['title'];
        $data['manufacturers'] = (isset($data['filter']['manufacturer']) ? $data['filter']['manufacturer'] : '');
        $data['manufacturer_all'] = (isset($data['filter']['manufacturer_all']) ? $data['filter']['manufacturer_all'] : '');
        $data['manufacturer_selected_name'] = (isset($data['filter']['manufacturer_selected_name']) ? $data['filter']['manufacturer_selected_name'] : $data['visible_supplier_title']);
        /** ---------------- */
        $data['visible_category'] = $data['filter']['visible_product_type']['visible'];
        $data['visible_category_title'] = $data['filter']['visible_product_type']['title'];
        //$data['categorys'] = (isset($data['filter']['category']) ? $data['filter']['category'] : '');
        $data['categories'] = $this->generateCategories();
        $data['category_selected'] = isset($this->request->get['path']) ? $this->request->get['path'] : '';
        $data['category_all'] = (isset($data['filter']['category_all']) ? $data['filter']['category_all'] : '');
        $data['category_selected_name'] = (isset($data['filter']['category_selected_name']) ? $data['filter']['category_selected_name'] : $data['visible_category_title']);
        /** ---------------- */
        $data['visible_collection'] = $data['filter']['visible_collection']['visible'];
        $data['visible_collection_title'] = $data['filter']['visible_collection']['title'];
        $data['collections'] = (isset($data['filter']['collection']) ? $data['filter']['collection'] : '');
        $data['collection_all'] = (isset($data['filter']['collection_all']) ? $data['filter']['collection_all'] : '');
        $data['collection_selected_name'] = (isset($data['filter']['collection_selected_name']) ? $data['filter']['collection_selected_name'] : $data['visible_collection_title']);
        /** ---------------- */
        $data['visible_tag'] = $data['filter']['visible_tag']['visible'];
        // todo: load more tags
        $data['tags'] = []; // (isset($data['filter']['tag']) ? $data['filter']['tag'] : '');
        $data['urlserach'] = (isset($data['filter']['urlserach']) ? $data['filter']['urlserach'] : '');
        $data['tag_selected_name'] = (isset($data['filter']['tag_selected_name']) ? $data['filter']['tag_selected_name'] : 'TAGS');
        $data['tag_all'] = (isset($data['filter']['tag_all']) ? $data['filter']['tag_all'] : '');
        /** ---------------- */
        $data['visible_attribute'] = $data['filter']['visible_attribute']['visible'];
        $data['attributes'] = (isset($data['filter']['attribute']) ? $data['filter']['attribute'] : '');
        $data['attribute_selected_name'] = (isset($data['filter']['attribute_selected_name']) ? $data['filter']['attribute_selected_name'] : '');
        $data['attribute_selected_value'] = (isset($data['filter']['attribute_selected_value']) ? $data['filter']['attribute_selected_value'] : '');
        $data['attribute_all'] = (isset($data['filter']['attribute_all']) ? $data['filter']['attribute_all'] : '');
        /** ---------------- */
        $data['visible_product_price'] = $data['filter']['visible_product_price']['visible'];
        $data['visible_product_price_from'] = $data['filter']['visible_product_price']['range']['from'];
        $data['visible_product_price_to'] = $data['filter']['visible_product_price']['range']['to'];

        $data['href_product_list'] = $this->url->link('common/shop', '', true);

        $banners = $this->getBanner();
        $data['banners'] = (isset($banners['banners_visible']) ? $banners['banners_visible'] : '');
        /*$data['url_banner_product'] =  $data['banners']['banners_visible']['url_banner'];
        $data['image_banner_product'] =  $data['banners']['banners_visible']['url_banner'];
        $data['url_banner_product'] =  $data['banners']['banners_visible']['url_banner'];*/
        $all_banners = (isset($shop_banner_config['visible']) && $shop_banner_config['visible']) ? $shop_banner_config['display'] : [];
        $data['all_banners'] = $all_banners;
        //var_dump($data['all_banners']);die;

        /* check if exists has-image categories */
        $data['categories_new'] = $this->getCategories();
        $data['have_image_categories'] = false;
        if (is_array($data['categories_new'])) {
            foreach ($data['categories_new'] as $category) {
                if (!empty($category['image'])) {
                    $data['have_image_categories'] = true;
                    break;
                }
            }
        }
        unset($category);

        $filter['valueoptiton'] = 3;
        /* base layout */
        $data['shop_link'] = $this->url->link('common/shop', '', true);
        $data['home_link'] = $this->url->link('common/home', '', true);
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $data['header'] .= '<script type="application/ld+json">' . $this->schemaOrganization() . '</script>' . "\n";
        $data['header'] .= '<script type="application/ld+json">' .
            $this->schemaBreadcrumbList($this->url->link('common/shop'), "Sản phẩm, Products", $categoryFull) . '</script>' . "\n";

        $schemeString = $this->schemaItemOfCategory($categoryFull, $this->model_catalog_product->getProductsCustomOptimize($filter));
        if (!is_array($schemeString)) {
            $data['header'] .= '<script type="application/ld+json">' . $schemeString . '</script>' . "\n";
        }

        // placeholder image
        $data['placeholder_image_url'] = $this->session->data['placeholder_image_url'];

        // check is collection in menu, then may redirect to collection page instead of shop page
        // DO NOT use query param "is_menu" due to breaking down seo url when running ads
        // TODO: other way?... HOT FIX ASAP!
        $collection = isset($this->request->get['collection'])
            ? $this->model_catalog_collection->getCollection([
                'filter_collection_id' => $this->request->get['collection']
            ])
            : [];
        $data['theme_directory'] = '/catalog/view/theme/' . $this->getCurrentThemeDir(); // old code: DIR_TEMPLATE . $this->getCurrentThemeDir(); // TODO: remove old code?...
        $collection_twig = DIR_TEMPLATE . $this->getCurrentThemeDir() . '/template/common/collection.twig';
        if (isset($this->request->get['is_menu']) &&
            isset($this->request->get['collection']) &&
            isset($collection['type']) &&
            $collection['type'] == 1 &&
            is_file($collection_twig)
        ) {
            $data['collections_is_menu'] = $this->model_catalog_collection->getCollections(['filter_collection_id' => $this->request->get['collection']]);
            foreach ($data['collections_is_menu'] as &$collection_data) {
                if ($collection_data['image']) {
                    // no need resize
                } else {
                    $collection_data['image'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                }
                $collection_data['url'] = $this->url->link('common/shop', 'collection=' . $collection_data['collection_id']);
            }
            unset($collection_data);
            $this->response->setOutput($this->load->view('common/collection', $data));
        } else {
            if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
                $this->response->setOutput($this->load->view('common/shop_append', $data));
            } else {
                $this->response->setOutput($this->load->view('common/shop', $data));
            }
        }
    }

    private function getCategories()
    {
        $this->load->model('catalog/category');
        $categories = $this->model_catalog_category->getCategoriesHadProduct();
        $result = [];

        foreach ($categories as $category) {
            $result[] = array(
                'category_id' => $category['category_id'],
                'name' => $category['name'],
                'image' => $this->changeUrl($category['image']),
                'id' => $category['category_id'],
                'parent_id' => $category['parent_id'],
                'url' => str_replace(' ', '', $this->url->link('common/shop', '&path=' . $category['category_id'], true)),
            );
        }

        return $result;
    }

    public function getCountProductInCategory() {
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');

        $data = [];
        $related_category = $this->model_catalog_category->getCategoriesHadProduct();
        foreach ($related_category as $category) {
            $filter_data_category = array(
                'filter_category_id' => $category['category_id']
            );

            $count_products = $this->model_catalog_product->getTotalProductsCustom($filter_data_category);

            $data[] = array(
                'category_id' => $category['category_id'],
                'count_product' => $count_products,
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($data));
    }
    public function getCountProductInManufacturer() {
        $this->load->model('catalog/product');
        $this->load->model('catalog/manufacturer');

        $data = [];
        $related_manufacturer = $this->model_catalog_manufacturer->getManufacturers(false, false);
        foreach ($related_manufacturer as $manufacturer) {
            $filter_data_manufacturer = array(
                'filter_manufacturer_id' => $manufacturer['manufacturer_id']
            );

            $count_products = $this->model_catalog_product->getTotalProductsCustom($filter_data_manufacturer);

            $data[] = array(
                'manufacturer_id' => $manufacturer['manufacturer_id'],
                'count_product' => $count_products,
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($data));
    }

    public function getCountProductInCollection() {
        $this->load->model('catalog/product');
        $this->load->model('catalog/collection');

        $data = [];
        $related_collection = $this->model_catalog_collection->getAllcollection();
        foreach ($related_collection as $collection) {
            $filter_data_collection = array(
                'filter_collection_id' => $collection['collection_id']
            );

            $count_products = $this->model_catalog_product->getTotalProductsCustom($filter_data_collection);
            $count_collections = '1' == $collection['type'] ? $this->model_catalog_collection->getCountCollections($filter_data_collection) : 0;

            $data[] = array(
                'collection_id' => $collection['collection_id'],
                'count_product' => $count_products,
                'count_collection' => $count_collections,
                'type' => $collection['type']
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($data));
    }

    private function getBanner()
    {
        $data = array();
        $banners = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_BANNER);
        $banners = json_decode($banners, true);
        $this->load->model('tool/image');
        if ($banners['display'][0]['visible'] == 1) {
            $data['banners_visible'] = array(
                'url_banner' => $banners['display'][0]['url'],
                'url_image' => empty($banners['display'][0]['image-url']) ? $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height')) : cloudinary_change_http_to_https($banners['display'][0]['image-url']),
                'visible_banner' => $banners['display'][0]['visible'],
                'alt' => $banners['display'][0]['description'],
                'description' => $banners['display'][0]['description'],
            );
        }
        return $data;
    }

    private function getPrime($range)
    {
        $result = array();
        foreach ($range as $item) {
            if ($item['from'] == null && $item['to'] == null) {
                continue;
            }

            if ($item['from'] == null) {
                $type = 'max';
                $result[] = array(
                    'range' => '<' . number_format($item['to']),
                    'count' => $this->model_catalog_product->countProductPriceInRange($item, $type),
                    'url' => $this->url->link('common/shop', 'max=' . $item['to'], true)
                );
            } else if ($item['to'] == null) {
                $type = 'min';
                $result[] = array(
                    'range' => '>' . number_format($item['from']),
                    'count' => $this->model_catalog_product->countProductPriceInRange($item, $type),
                    'url' => $this->url->link('common/shop', 'min=' . $item['from'], true)
                );
            } else {
                $type = 'range';
                $result[] = array(
                    'range' => number_format($item['from']) . ' - ' . number_format($item['to']),
                    'count' => $this->model_catalog_product->countProductPriceInRange($item, $type),
                    'url' => $this->url->link('common/shop', 'min=' . $item['from'] . '&max=' . $item['to'], true)
                );
            }
        }
        return $result;
    }

    private function categoryMapList($category_lists)
    {
        $categories = array();
        foreach ($category_lists as $category) {
            $name = $category['text'];
            $path = $category['id'];
            $filter['filter_category_id'] = (int)$category['id'];
            $filter['filter_sub_category'] = true;
            $count = $this->model_catalog_product->getTotalProducts($filter);
            $categories[] = array(
                'name' => $name,
                'count' => (int)$count,
                'href' => $this->url->link('common/shop', 'path=' . $path, true)
            );
        }

        return $categories;
    }

    private function resizeImage($image, $width = null, $height = null)
    {
        if ($image) {
            if ($width == null) {
                $width = $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width');
            }
            if ($height == null) {
                $height = $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height');
            }
            $image = $this->model_tool_image->resize($image, $width, $height);
        } else {
            $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));
        }

        return $image;
    }
}
