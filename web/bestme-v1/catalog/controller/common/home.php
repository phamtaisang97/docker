<?php

class ControllerCommonHome extends Controller
{
    use Theme_Config_Util;
    use Device_Util;
    use Product_Util_Trait;
    use Flash_Sale_Trait;
    /*
     * Important: this const is used to avoid too much memory used and reduce loading time
     * Current set 32 products to satisfied with grid config 2, 4, 6 with 1, 2 product lines
     * TODO: admin config or choose other satisfied number...
     */
    const LIMIT_PRODUCT_IN_BLOCK = 32;
    const CHANGEABLE_BLOG_CONFIG = [
        'grid' => [
            'quantity' => [2, 3, 4],
            'row' => [1, 2, 3, 4]
        ],
        'grid_mobile' => [
            'quantity' => [1, 2],
            'row' => [1, 2, 3, 4]
        ],
    ];
    const CHANGEABLE_RATE_CONFIG = [
        'grid' => [
            'quantity' => [1, 2, 3, 4],
            'row' => [1, 2, 3, 4]
        ],
        'grid_mobile' => [
            'quantity' => [1, 2],
            'row' => [1, 2, 3, 4]
        ],
    ];
    const CHANGEABLE_BLOGS_PER_ROW = [2, 3, 4];
    const CHANGEABLE_ROWS = [1, 2, 3, 4];
    const CHANGEABLE_BLOGS_PER_ROW_ON_MOBILE = [1, 2];
    const CHANGEABLE_ROWS_ON_MOBILE = [1, 2, 3, 4];

    /* for caching between functions call */
    // already in Product_Util_Trait. TODO: remove...
    /*private $hot_products = [];
    private $hot_products_with_config = [];

    private $best_sale_products = [];
    private $best_sale_products_with_config = [];

    private $new_products = [];
    private $new_products_with_config = [];

    private $IS_DEBUG_RUNNING_TIME = false;*/
    private $start_runing_first = 0;
    private $prev_running = 0;

    public function index()
    {
        $this->start_runing_first = $this->prev_running = round(microtime(true) * 1000);

        $this->load->language('common/home');

        $this->document->setTitle($this->language->get('text_home'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));

        $this->load->model('tool/image');
        $this->load->model('setting/setting');

        $data = [];

        $data['title'] = $this->model_setting_setting->getSettingValue('config_home_name');

        $data['theme_name'] = $this->getCurrentThemeDir();
        $data['theme_directory'] = '/catalog/view/theme/' . $data['theme_name'];
        /* get header config */
        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

        // add SEO schema
        $this->load->model('setting/setting');
        $shop_name = $this->model_setting_setting->getSettingValue('shop_name');
        $footer_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER);
        $footer_config = json_decode($footer_config, true);
        $contact = [
            'address' => '',
            'phone' => '',
        ];
        if (isset($footer_config['contact']['visible']) && $footer_config['contact']['visible'] == 1) {
            //$data['contact'] = array(...) // why $data['contact'] instead of $contact?... TODO: remove if stable...
            $contact = [
                'address' => $footer_config['contact']['address'],
                'phone' => $footer_config['contact']['phone-number'],
            ];
        }

        $schema = array(
            "@context" => "http://schema.org",
            "@type" => "Website",
            "name"=> $shop_name,
            "description"=> $this->document->getDescription(),
            "address"=> [
                "@type"=> "PostalAddress",
                "addressLocality"=> $contact['address'],
                "streetAddress"=> $contact['address']
            ],
            "openingHours"=> [
                "Mo-Sa 09:00-22:30",
                "Su 09:00-23:00"
            ],
            "telephone"=> $contact['phone'],
            "menu"=> $this->url->link('common/shop', '', true)
        );
        $schema =  json_encode($schema, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        // get current theme
        $current_theme = $this->config->get('config_theme');

        if ($this->isSupportThemeFeature(self::$CUSTOMIZE_LAYOUT)) {
            $customize_layout = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CUSTOMIZE_LAYOUT);
            $customize_layout = json_decode($customize_layout, true);

            // get default theme config from json file
            $theme_directory = $this->model_setting_setting->getSettingValue('theme_' . $current_theme . '_directory');

            $file_path = DIR_APPLICATION . 'view/theme/' . $theme_directory . '/asset/default_config/default.json';
            if (file_exists($file_path) && is_readable($file_path)) {
                $default_config = file_get_contents($file_path);
                if ($default_config) {
                    $defaultConfig = json_decode($default_config, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
                    if ($defaultConfig) {
                        $is_change_version = isset($defaultConfig['version']) && ((isset($customize_layout['version']) && $defaultConfig['version'] > $customize_layout['version']) || !isset($customize_layout['version']));

                        // if db has no data or default config change version => get config from default config
                        if (!isset($customize_layout['default_customize_layout']) || $is_change_version) {
                            if (isset($defaultConfig['default_customize_layout']) && $defaultConfig['default_customize_layout']) {
                                $customize_layout = $defaultConfig['default_customize_layout'];
                            }
                        } else {
                            $customize_layout = isset($customize_layout['default_customize_layout']) ? $customize_layout['default_customize_layout'] : [];
                        }
                    }
                }
            }

            $data['sections_layout'] = $customize_layout;
        }

        $header_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER);
        $header_config = json_decode($header_config, true);
        $data['logo'] = isset($header_config['logo']['url']) ? cloudinary_change_http_to_https($header_config['logo']['url']) : 'image/no_image.png';
        $data['logo_height'] = isset($header_config['logo']['height']) ? $header_config['logo']['height'] : 50;

        $data['homepage_description'] = $this->config->get('config_home_des');

        $data['is_on_mobile'] = $this->isMobile() ? 1 : 0;
        $data['is_ios_device'] = $this->isIosDevice() ? 1 : 0;
        if (isset($this->request->get['route'])) {
            $this->document->addLink($this->config->get('config_url'), 'canonical');
        }

        $data['limit_product_in_block'] = $limit = $this->getLimitProductInBlock();

        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

        $sections_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_SECTIONS);
        $sections_config = json_decode($sections_config, true);
        $sections_config = is_array($sections_config) ? $sections_config : [];
        $sections = array();

        if (isset($sections_config['section']) && is_array($sections_config['section'])) {
            foreach ($sections_config['section'] as $section) {
                if (!array_key_exists('name', $section) || !array_key_exists('visible', $section)) {
                    continue;
                }
                $sections[$section['name']] = $section['visible'];
            }
            unset($sections_config, $section);
        }

        if (isset($sections['content-customize']) && $sections['content-customize'] == '1') {
            // new, without migration
            $content_customizes = $this->getContentCustomize();
            $data = array_merge($data, $content_customizes);
            unset($content_customizes);
        }

        // new product (key: new_products)
        if (isset($sections['new-products']) && $sections['new-products'] == '1') {
            $result_new_product = $this->getNewProductsWithCache();

            $data = array_merge($data, $result_new_product);
            unset($result_new_product);

            $this->_logRunningTime('after new-products');
        }

        // best sales product (key: feature-products)
        if (isset($sections['feature-products']) && $sections['feature-products'] == '1') {
            $result_best_sale = $this->getBestSalesProductsWithCache();

            $data = array_merge($data, $result_best_sale);
            unset($result_best_sale);

            $this->_logRunningTime('after best-sales-products');
        }

        // hot product (key: hot-deals)
        if (isset($sections['hot-deals']) && $sections['hot-deals'] == '1') {
            $result_hot_product = $this->getHotProductWithCache();

            $data = array_merge($data, $result_hot_product);
            unset($result_hot_product);

            $this->_logRunningTime('after hot-products');
        }

        // index - detail product (show or hide)
        $detail_product_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_DETAIL_PRODUCT);
        $detail_product_config = json_decode($detail_product_config, true);
        if (isset($detail_product_config['display'])) {
            $data['detail_product_config'] = $detail_product_config['display'];
        }

        // banner
        if (isset($sections['banner']) && $sections['banner'] == '1') {
            $result = $this->getBanner();
            $data = array_merge($data, $result);
            unset($result);
        }

        // slide-show
        if (isset($sections['slide-show']) && $sections['slide-show'] == '1') {
            $slides_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_SLIDESHOW);
            $slides_config = json_decode($slides_config, true);
            $data['slideshow'] = [];
            $data['slideshow']['visible'] = 1;
            $data['slideshow']['transition_time'] = (int)$slides_config['setting']['transition-time'] * 1000;

            $slides_config['display'] = isset($slides_config['display']) && is_array($slides_config['display']) ? $slides_config['display'] : [];
            foreach ($slides_config['display'] as $key => $slide) {

                if (isset($slide['video-url']) && empty($slide['video-url']) && $slide['type'] == 'video') {
                    $slides_config['display'][$key]['image-url'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                    $slides_config['display'][$key]['type'] = 'image';
                    continue;
                }

                if (isset($slide['image-url'])) {
                    if (empty($slide['image-url'])) {
                        $image_url = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                        $slides_config['display'][$key]['image-url'] = $this->changeUrl($image_url);
                    } else {
                        $slide['image-url'] = $this->model_tool_image->cloudinary_auto_optimize($slide['image-url']);
                        $image_url = cloudinary_change_http_to_https($slide['image-url']);
                        $slides_config['display'][$key]['image-url'] = $this->changeUrl($image_url);
                    }
                }
            }
            $data['slideshow']['slides'] = $slides_config['display'];
        }

        // partner (key: partners)
        if (isset($sections['partners']) && $sections['partners'] == '1') {
            $partners_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_PARTNER);
            $partners_config = json_decode($partners_config, true);

            $partners_config['display'] = isset($partners_config['display']) && is_array($partners_config['display']) ? $partners_config['display'] : [];
            foreach ($partners_config['display'] as $key => $partner) {
                if (isset($partner['image-url'])) {
                    if (empty($partner['image-url'])) {
                        unset($partners_config['display'][$key]);
                        //$partners_config['display'][$key]['image-url'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                    } else {
                        $partner['image-url'] = $this->model_tool_image->cloudinary_auto_optimize($partner['image-url']);
                        $partner_url_img = cloudinary_change_http_to_https($partner['image-url']);
                        $partners_config['display'][$key]['image-url'] = $this->changeUrl($partner_url_img);
                    }
                }
            }

            $data['partners'] = $partners_config['display'];
            if (!isset($partners_config['text']) || $partners_config['text'] == null) {
                $data['partners_title'] = "Đối tác";
            } else {
                $data['partners_title'] = $partners_config['text'];
            }
            if (isset($partners_config['setting'])) {
                $data['partners_setting'] = $partners_config['setting'];
            } else {
                // set default setting value
                $data['partners_setting'] = [
                    'loop' => true,
                    'autoplay' => true
                ];
            }

            if (count($data['partners']) > 0) {
                $data['partners_visible'] = true;
            } else {
                $data['partners_visible'] = false;
            }

            $data['partners_limit_per_line'] = $partners_config['limit-per-line'];
        }

        $this->_logRunningTime('after default blocks');

        $data['group_block'] = $this->getGroupBlock($model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_PRODUCT_GROUPS));
        $data['blocks'] = $this->getProductBlocksList($data['group_block'], $sections);
        // new key for "blocks"
        $data['product_group_list'] = $data['group_block'];

        $this->_logRunningTime('after custom blocks');

        $data['is_on_mobile'] = $this->isMobile() ? 1 : 0;

        /* get title category*/
        $keys = array(
            'config_category_title'
        );
        $this->load->model('catalog/category');
        $category_title = $this->model_catalog_category->getMultiSettingValue($keys);
        if (!empty($category_title) && $category_title[0]['value'] != null) {
            $data['category_title'] = $category_title[0]['value'];
        } else {
            $data['category_title'] = "Danh mục sản phẩm";
        }

        /* blog config */
        $blog_section_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_SECTIONS);
        $blog_section_config = json_decode($blog_section_config, true);

        $data['blog_title'] = 'Bài viết';
        if ($this->isSupportThemeFeature(self::$CUSTOMIZE_LAYOUT)) {
            $blog_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_BLOG);
            $blog_config = json_decode($blog_config, true);

            // blog grid
            $blog_grid = isset($blog_config['display']['grid']['quantity']) && in_array($blog_config['display']['grid']['quantity'], self::CHANGEABLE_BLOG_CONFIG['grid']['quantity']) ? $blog_config['display']['grid']['quantity'] : 3;
            $blog_row = isset($blog_config['display']['grid']['row']) && in_array($blog_config['display']['grid']['row'], self::CHANGEABLE_BLOG_CONFIG['grid']['row']) ? $blog_config['display']['grid']['row'] : 1;
            $blog_grid_mobile = isset($blog_config['display']['grid_mobile']['quantity']) && in_array($blog_config['display']['grid_mobile']['quantity'], self::CHANGEABLE_BLOG_CONFIG['grid_mobile']['quantity']) ? $blog_config['display']['grid_mobile']['quantity'] : 1;
            $blog_row_mobile = isset($blog_config['display']['grid_mobile']['row']) && in_array($blog_config['display']['grid_mobile']['row'], self::CHANGEABLE_BLOG_CONFIG['grid_mobile']['row']) ? $blog_config['display']['grid_mobile']['row'] : 1;

            $data['blog_config'] = [
                'grid' => $data['is_on_mobile'] ? $blog_grid_mobile : $blog_grid,
                'row' => $data['is_on_mobile'] ? $blog_row_mobile : $blog_row
            ];

            if (isset($blog_config['setting']['title'])) {
                $data['blog_title'] =  $blog_config['setting']['title'];
            }
        } else {
            if (isset($blog_section_config['section']['blog']['text'])) {
                $data['blog_title'] = $blog_section_config['section']['blog']['text'];
            }
        }

        $data['current'] = "is-page-home";
        // get blog list
        if (isset($sections['blog']) && $sections['blog'] == '1') {
            $this->load->model('blog/blog');
//            $data['latest_blog'] = $this->convertBlogList($this->model_blog_blog->getBlogList());
//
//            // get first 2 oldest blogs by created date
            $new_blog_condition = [
                'in_home' => 1,
                'sort' => 'b.date_modified',
                'limit' => 3
            ];
            $data['new_blog'] = $this->convertBlogList($this->model_blog_blog->getBlogList($new_blog_condition));

            $data['blog_category'] = $this->model_blog_blog->getALlBlogCategory();

        } else {
            $data['latest_blog'] = [];
            $data['new_blog'] = [];
        }
        $data['url_blog'] = $this->url->link('blog/blog');

        /* get all rate website */
        $this->load->model('rate/rate');
        $rates = $this->model_rate_rate->getAllRates();
        $data['rate_website_all'] = $rates;

        /* rate config */
        $rate_section_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_RATE);
        $rate_section_config = json_decode($rate_section_config, true);
        $sections_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_SECTIONS);
        $sections_config = json_decode($sections_config, true);
        $data['rate_visible'] = !empty($sections_config['section']['rate']) ? $sections_config['section']['rate']['visible'] : 1;
        $rate_grid = isset($rate_section_config['display']['grid']['quantity']) && in_array($rate_section_config['display']['grid']['quantity'], self::CHANGEABLE_RATE_CONFIG['grid']['quantity']) ? $rate_section_config['display']['grid']['quantity'] : 3;
        $rate_row = isset($rate_section_config['display']['grid']['row']) && in_array($rate_section_config['display']['grid']['row'], self::CHANGEABLE_RATE_CONFIG['grid']['row']) ? $rate_section_config['display']['grid']['row'] : 1;
        $rate_grid_mobile = isset($rate_section_config['display']['grid_mobile']['quantity']) && in_array($rate_section_config['display']['grid_mobile']['quantity'], self::CHANGEABLE_RATE_CONFIG['grid_mobile']['quantity']) ? $rate_section_config['display']['grid_mobile']['quantity'] : 1;
        $rate_row_mobile = isset($rate_section_config['display']['grid_mobile']['row']) && in_array($rate_section_config['display']['grid_mobile']['row'], self::CHANGEABLE_RATE_CONFIG['grid_mobile']['row']) ? $rate_section_config['display']['grid_mobile']['row'] : 1;
        $sections_config['setting']['title'] = isset($sections_config['setting']['title']) ? $sections_config['setting']['title'] : 'Đánh giá website';

        $data['rate_config'] = $rate_section_config;
        $data['rate_config']['display'] = [
            'grid' => $data['is_on_mobile'] ? $rate_grid_mobile : $rate_grid,
            'row' => $data['is_on_mobile'] ? $rate_row_mobile : $rate_row
        ];
        // list categories
        $data['isset_cart'] = isset($this->request->get['show_cart']) ? true : false;
        $data['categories'] = $this->generateCategories();

        /* check if exists has-image categories */
        $data['have_image_categories'] = false;
        if (is_array($data['categories'])) {
            foreach ($data['categories'] as $category) {
                if (!empty($category['image'])) {
                    $data['have_image_categories'] = true;
                    break;
                }
            }
        }
        unset($category);

        $this->_logRunningTime('after getCategories');

        $data['url_shop'] = $this->url->link('common/shop', '', true);

        /* load contact data */
        if ($this->isSupportThemeFeature(self::$CONTACT_IN_HOME)) {
            $data['contact_data'] = $this->load->controller('contact/contact', true);
            $data['contact_data']['href_submit_form'] = $this->url->link('common/home', '', true);
        }

    /* flash sale */
        $flash_sale = $this->flashSale();
        $data['view_more_flash_sale'] = self::$FLASH_SALE_URL;
        $data['products_flash_sale'] = isset($flash_sale['products_flash_sale']) ? $flash_sale['products_flash_sale'] : [];
        $data['future_time'] = $flash_sale['future_time'];
        $data['time_flashsale_happening'] = isset($flash_sale['time_flashsale_happening']) ? $flash_sale['time_flashsale_happening'] : null;

        /* get footer config */
        $footer_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER);
        $footer_config = json_decode($footer_config, true);
        // subscribe
        $data['visible_subscribe'] = $footer_config['subscribe']['visible'];
        $data['visible_subscribe_title'] = $footer_config['subscribe']['title'];

        /* common layout */
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $this->_logRunningTime('after left/right/top/bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $this->_logRunningTime('after footer');
        $data['header'] = $this->load->controller('common/header');
        $data['header'] .= '<script type="application/ld+json">' . $schema . '</script>' . "\n";
        $this->_logRunningTime('after header');

        $this->_logRunningTime('end', $start_time = null, $end = $this->IS_DEBUG_RUNNING_TIME, $show_sum = true);

        // placeholder image
        $data['placeholder_image_url'] = $this->session->data['placeholder_image_url'];

        $this->response->setOutput($this->load->view('common/home', $data));
    }

    private function getContentCustomize($visible = 1)
    {
        $data = [];
        $content_customize_configs = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CONTENT_CUSTOMIZE);
        $content_customize_config = json_decode($content_customize_configs, true);

        $data['display_customizes'] = isset($content_customize_config['display']) ? $content_customize_config['display'] : [];

        foreach ($data['display_customizes'] as &$customize) {
            if (!$customize['content']['icon']) {
                $customize['content']['icon'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
            }
        }
        unset($customize);

        return $data;
    }

    /**
     * keep for future... TODO: use or not?...
     */
    private function getBlogList()
    {
        $data = array();
        $blog_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_BLOG);
        $blog_config = json_decode($blog_config, true);
        return;
    }

    private function getBanner()
    {
        $data = [];
        $banners = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_BANNER);
        $banners = json_decode($banners, true);
        if (isset($banners['display']) && is_array($banners['display'])) {
            foreach ($banners['display'] as $key => $banner) {
                if (isset($banner['image-url'])) {
                    if (empty($banner['image-url'])) {
                        $banners['display'][$key]['visible'] = 0;
//                        $banners['display'][$key]['image-url'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                    } else {
                        $banners['display'][$key]['image-url'] = $this->model_tool_image->cloudinary_auto_optimize($banners['display'][$key]['image-url']);
                        $banner_img = cloudinary_change_http_to_https($banners['display'][$key]['image-url']);
                        $banners['display'][$key]['image-url'] = $this->changeUrl($banner_img);
                    }
                }
            }

            // named banner
            list($data['banner_highlight'], $data['banner_left'], $data['banner_middle']) = $banners['display'];

            // more banners
            $data['banners'] = $banners['display'];
            $data['named_banners'] = [];
            list($data['named_banners']['banner_highlight'], $data['named_banners']['banner_left'], $data['named_banners']['banner_middle']) = $banners['display'];
            if (count($banners['display']) > 3) {
                $data['named_banners'] = array_merge($data['named_banners'], array_slice($banners['display'], 3));
            }
        }

        $data['banners_visible'] = true;

        return $data;
    }

    private function generateCategories() {
        $this->load->model('catalog/category');
        $categories = $this->model_catalog_category->getCategoriesHadProduct();
        $category_level_1 = [];
        $category_level_1_temp = [];

        foreach ($categories as $key => $category) {
            if($category['parent_id'] == 0) {
                $row = array(
                    'category_id' => $category['category_id'],
                    'name' => $category['name'],
                    'image' => $category['image'],
                    'id' => $category['category_id'],
                    'parent_id' => $category['parent_id'],
                    'url' => str_replace(' ', '', $this->url->link('common/shop', '&path=' . $category['category_id'], true)),
                    'list_sub' => []
                );
                $category_level_1_temp[] = $category['category_id'];
                $category_level_1[] = $row;
                unset($categories[$key]);
            }
        }
        $category_level_2 = [];
        $category_level_2_temp = [];

        foreach ($categories as $key => $category) {
            $search = array_search($category['parent_id'], $category_level_1_temp);
            if($search !== false) {
                $row = array(
                    'parent_key' => $search,
                    'category_id' => $category['category_id'],
                    'name' => $category['name'],
                    'image' => $category['image'],
                    'id' => $category['category_id'],
                    'parent_id' => $category['parent_id'],
                    'url' => str_replace(' ', '', $this->url->link('common/shop', '&path=' . $category['category_id'], true)),
                    'list_sub' => []
                );
                $category_level_2[] = $row;
                $category_level_2_temp[] = $category['category_id'];
                unset($categories[$key]);
            }
        }

        foreach ($categories as $key => $category) {
            $search = array_search($category['parent_id'], $category_level_2_temp);
            if($search !== false) {
                $row = array(
                    'category_id' => $category['category_id'],
                    'name' => $category['name'],
                    'image' => $category['image'],
                    'id' => $category['category_id'],
                    'parent_id' => $category['parent_id'],
                    'url' => str_replace(' ', '', $this->url->link('common/shop', '&path=' . $category['category_id'], true)),
                    'list_sub' => []
                );
                $category_level_2[$search]['list_sub'][] = $row;
                unset($categories[$key]);
            }
        }

        foreach ($category_level_2 as $category) {
            $category_level_1[$category['parent_key']]['list_sub'][] = $category;
        }
        return $category_level_1;
    }

    private function getCategories()
    {
        $this->load->model('catalog/category');
        $categories = $this->model_catalog_category->getCategoriesHadProduct();
        $result = [];

        foreach ($categories as $category) {
            $result[] = array(
                'category_id' => $category['category_id'],
                'name' => $category['name'],
                'image' => $category['image'],
                'id' => $category['category_id'],
                'parent_id' => $category['parent_id'],
                'url' => str_replace(' ', '', $this->url->link('common/shop', '&path=' . $category['category_id'], true)),
            );
        }

        return $result;
    }


    private function getGroupBlock($config)
    {
        $config = json_decode($config, true);
        $groups = isset($config['list']) ? $config['list'] : [];

        foreach ($groups as $key => $group) {
            if ($group['visible'] != 1) {
                unset($groups[$key]);
            }
        }

        return $groups;
    }

    private function convertBlogList($blog_list)
    {
        foreach ($blog_list as &$blog) {
            $blog['link'] = $this->url->link("blog/blog/detail", 'blog_id=' . $blog['blog_id']);
            $blog['use_default_image'] = true;
            if ($blog['type'] == 'video' && !$blog['video_url']) {
                $blog['type'] = 'image';
                $blog_image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                $blog['image'] = $this->changeUrl($blog_image);
            } elseif (!$blog['image']) {
                $blog_image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                $blog['image'] = $this->changeUrl($blog_image);
            } else {
                $blog['use_default_image'] = false;
                $blog['image'] = $this->changeUrl($blog['image'], true, 'blog_home');
            }
        }
        unset($blog);

        return $blog_list;
    }

    private function resizeImage($image, $width = null, $height = null)
    {
        if ($image) {
            if ($width == null) {
                $width = $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width');
            }
            if ($height == null) {
                $height = $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height');
            }
            $image = $this->model_tool_image->resize($image, $width, $height);
        } else {
            $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));
        }

        return $image;
    }
}
