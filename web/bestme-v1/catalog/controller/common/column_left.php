<?php
class ControllerCommonColumnLeft extends Controller {

    use Product_Util_Trait;

	public function index() {
        /*$this->load->model('design/layout');

        if (isset($this->request->get['route'])) {
            $route = (string)$this->request->get['route'];
        } else {
            $route = 'common/home';
        }

        $layout_id = 0;

        if ($route == 'product/category' && isset($this->request->get['path'])) {
            $this->load->model('catalog/category');

            $path = explode('_', (string)$this->request->get['path']);

            $layout_id = $this->model_catalog_category->getCategoryLayoutId(end($path));
        }

        if ($route == 'product/product' && isset($this->request->get['product_id'])) {
            $this->load->model('catalog/product');

            $layout_id = $this->model_catalog_product->getProductLayoutId($this->request->get['product_id']);
        }

        if ($route == 'information/information' && isset($this->request->get['information_id'])) {
            $this->load->model('catalog/information');

            $layout_id = $this->model_catalog_information->getInformationLayoutId($this->request->get['information_id']);
        }

        if (!$layout_id) {
            $layout_id = $this->model_design_layout->getLayout($route);
        }

        if (!$layout_id) {
            $layout_id = $this->config->get('config_layout_id');
        }

        $this->load->model('setting/module');

        $data['modules'] = array();

        $modules = $this->model_design_layout->getLayoutModules($layout_id, 'column_left');

        foreach ($modules as $module) {
            $part = explode('.', $module['code']);

            if (isset($part[0]) && $this->config->get('module_' . $part[0] . '_status')) {
                $module_data = $this->load->controller('extension/module/' . $part[0]);

                if ($module_data) {
                    $data['modules'][] = $module_data;
                }
            }

            if (isset($part[1])) {
                $setting_info = $this->model_setting_module->getModule($part[1]);

                if ($setting_info && $setting_info['status']) {
                    $output = $this->load->controller('extension/module/' . $part[0], $setting_info);

                    if ($output) {
                        $data['modules'][] = $output;
                    }
                }
            }
        }*/

        $url = '';
        $valueoptiton_post = (isset($this->request->get['valueoptiton']) ? $this->request->get['valueoptiton'] : '');
        if (isset($this->request->get['valueoptiton'])) {
            $url .= '&valueoptiton=' . $valueoptiton_post;
            $data['valueoptiton'] = $valueoptiton_post;
        }

        if (isset($this->request->get['search'])) {
            $txtSearch = trim($this->request->get['search']);
            $url .= '&search=' . $txtSearch;
            $data['getsearch'] = $txtSearch;
        }
        if (isset($this->request->get['path'])) {
            $url .= '&path=' . $this->request->get['path'];
            $parts = explode('_',$this->request->get['path']);
            $data['filter_category_id'] = (int)end($parts);
        }
        if (isset($this->request->get['manufacture'])) {
            $url .= '&manufacture=' . $this->request->get['manufacture'];
            $data['filter_manufacturer_id'] = $this->request->get['manufacture'];
        }
        if (isset($this->request->get['collection'])) {
            $url .= '&collection=' . $this->request->get['collection'];
            $data['filter_collection_id'] = $this->request->get['collection'];
        }
        if (isset($this->request->get['nametag'])) {
            $url .= '&nametag=' . $this->request->get['nametag'];
            $data['filter_nametag'] = $this->request->get['nametag'];
        }
        $data['url_filter_price'] = $this->url->link('common/shop', $url, true);

        $data['filter'] = $this->getFilterProduct();
        /** ---------------- */
        $data['visible_supplier'] = $data['filter']['visible_supplier']['visible'];
        $data['visible_supplier_title'] = $data['filter']['visible_supplier']['title'];
        $data['manufacturers'] = (isset($data['filter']['manufacturer']) ? $data['filter']['manufacturer'] : '');
        $data['manufacturer_all'] = (isset($data['filter']['manufacturer_all']) ? $data['filter']['manufacturer_all'] : '');
        $data['manufacturer_selected_name'] = (isset($data['filter']['manufacturer_selected_name']) ? $data['filter']['manufacturer_selected_name'] : $data['visible_supplier_title']);
        /** ---------------- */
        $data['visible_category'] = $data['filter']['visible_product_type']['visible'];
        $data['visible_category_title'] = $data['filter']['visible_product_type']['title'];
        $data['categorys'] = (isset($data['filter']['category']) ? $data['filter']['category'] : '');
        $data['categories'] = $data['categorys']; // maintain key to correct lang
        $data['category_all'] = (isset($data['filter']['category_all']) ? $data['filter']['category_all'] : '');
        $data['category_selected_name'] = (isset($data['filter']['category_selected_name']) ? $data['filter']['category_selected_name'] : $data['visible_category_title']);
        /** ---------------- */
        $data['visible_collection'] = $data['filter']['visible_collection']['visible'];
        $data['visible_collection_title'] = $data['filter']['visible_collection']['title'];
        $data['collections'] = (isset($data['filter']['collection']) ? $data['filter']['collection'] : '');
        $data['collection_all'] = (isset($data['filter']['collection_all']) ? $data['filter']['collection_all'] : '');
        $data['collection_selected_name'] = (isset($data['filter']['collection_selected_name']) ? $data['filter']['collection_selected_name'] : $data['visible_collection_title']);
        /** ---------------- */
        $data['visible_tag'] = $data['filter']['visible_tag']['visible'];
        $data['tags'] = (isset($data['filter']['tag']) ? $data['filter']['tag'] : '');
        $data['urlserach'] = (isset($data['filter']['urlserach']) ? $data['filter']['urlserach'] : '');
        $data['tag_selected_name'] = (isset($data['filter']['tag_selected_name']) ? $data['filter']['tag_selected_name'] : 'TAGS');
        $data['tag_all'] = (isset($data['filter']['tag_all']) ? $data['filter']['tag_all'] : '');
        /** ---------------- */
        $data['visible_product_price'] = $data['filter']['visible_product_price']['visible'];
        $data['visible_product_price_from'] = $data['filter']['visible_product_price']['range']['from'];
        $data['visible_product_price_to'] = $data['filter']['visible_product_price']['range']['to'];

        if (isset($this->request->get['min'])) {
            $data['price_min'] =  $this->request->get['min'];
        }
        if (isset($this->request->get['max'])) {
            $data['price_max'] = $this->request->get['max'];
        }

		return $this->load->view('common/column_left', $data);
	}

	// already in Product_Util_Trait, let here for review. TODO: remove if stable...
    /*public function getFilterProduct()
    {
        $this->load->language('common/shop');
        $this->load->model('extension/module/theme_builder_config');
        $this->load->model('catalog/manufacturer');
        $this->load->model('catalog/category');
        $this->load->model('catalog/collection');

        $result = array();
        $related_filter_product_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_FILTER);
        $related_filter_product_config = json_decode($related_filter_product_config, true);
        $result['visible_supplier'] = $related_filter_product_config['display']['supplier']; // nha cung cap
        $result['visible_product_type'] = $related_filter_product_config['display']['product-type']; // loai san pham
        $result['visible_collection'] = $related_filter_product_config['display']['collection'];  // bo suu tap
        $result['visible_product_price'] = $related_filter_product_config['display']['product-price'];  // giá sản phẩm
        $url = '';
        $result['urlserach']  = $this->url->link('common/shop', modQuery($url,'serach'). '', true);

        if (isset($this->request->get['valueoptiton'])) {
            $url .= '&valueoptiton=' . $this->request->get['valueoptiton'];
        }
        if (isset($this->request->get['search'])) {
            $url .= '&search=' . trim($this->request->get['search']);
        }
        if (isset($this->request->get['path']) && isset($result['visible_product_type']['visible']) && $result['visible_product_type']['visible'] == 1) {
            $url .= '&path=' . $this->request->get['path'];
        }
        if (isset($this->request->get['manufacture']) && isset($result['visible_supplier']['visible']) && $result['visible_supplier']['visible'] == 1) {
            $url .= '&manufacture=' . $this->request->get['manufacture'];
        }
        if (isset($this->request->get['collection']) && isset($result['visible_collection']['visible']) && $result['visible_collection']['visible'] == 1) {
            $url .= '&collection=' . $this->request->get['collection'];
        }
        if (isset($this->request->get['nametag'])) {
            $url .= '&nametag=' . $this->request->get['nametag'];
        }
        if (isset($this->request->get['min']) && isset($result['visible_product_price']['visible']) && $result['visible_product_price']['visible'] == 1) {
            $url .= '&min=' . $this->request->get['min'];
        }
        if (isset($this->request->get['max']) && isset($result['visible_product_price']['visible']) && $result['visible_product_price']['visible'] == 1) {
            $url .= '&max=' . $this->request->get['max'];
        }

        $result['product_price_url'] = $this->url->link('common/shop', modQuery($url,'min'), true);
        $result['product_price_url'] = modQuery($result['product_price_url'],'max');

        $this->load->model('catalog/tag');
        $tags = $this->model_catalog_tag->getAllTag();
        foreach ($tags as $tag){
            if(isset($this->request->get['nametag']) && $this->request->get['nametag'] == $tag['tag_id']){
                $tag_url = $this->url->link('common/shop', modQuery($url,'nametag'), true);
            }else{
                $tag_url = $this->url->link('common/shop', modQuery($url,'nametag'). '&nametag=' . $tag['tag_id'], true);
            }
            $result['tag'][] = array(
                'tag_id'  => $tag['tag_id'],
                'name'  => $tag['value'],
                'url' => $tag_url,
            );
        }

        if(isset($result['visible_supplier']['visible']) && $result['visible_supplier']['visible'] == 1){
            $related_manufacturer = $this->model_catalog_manufacturer->getManufacturersHadProduct();
            foreach ($related_manufacturer as $manufacturer){
                if(isset($this->request->get['manufacture']) && $this->request->get['manufacture'] == $manufacturer['manufacturer_id']){
                    $manufacturer_url = $this->url->link('common/shop', modQuery($url,'manufacture'), true);
                    $result['manufacturer_selected_name'] = $manufacturer['name'];
                }else{
                    $manufacturer_url = $this->url->link('common/shop', modQuery($url,'manufacture'). '&manufacture=' . $manufacturer['manufacturer_id'], true);
                }
                $result['manufacturer'][] = array(
                    'manufacturer_id'  => $manufacturer['manufacturer_id'],
                    'name'  => $manufacturer['name'],
                    'url' => $manufacturer_url,
                );
            }
            // option: tất cả nhà cung cấp
            $result['manufacturer_all'] = array(
                'manufacturer_id'  => '-1',
                'name'  => $this->language->get('text_all_supplier'),
                'url' => $this->url->link('common/shop', modQuery($url,'manufacture'), true),
            );
        }
        if(isset($result['visible_product_type']['visible']) && $result['visible_product_type']['visible'] == 1){
            $related_category = $this->model_catalog_category->getCategories();
            foreach ($related_category as $category){
                if(isset($this->request->get['path']) && $this->request->get['path'] == $category['category_id']){
                    $category_url = $this->url->link('common/shop', modQuery($url,'path'), true);
                    $result['category_selected_name'] = $category['name'];
                }else{
                    $category_url = $this->url->link('common/shop', modQuery($url,'path'). '&path=' . $category['category_id'], true);
                }
                $result['category'][] = array(
                    'category_id'  => $category['category_id'],
                    'name'  => $category['name'],
                    'url' => $category_url,
                );
            }
            // option: tất cả loại sản phẩm
            $result['category_all'] = array(
                'category_id'  => '-1',
                'name'  => $this->language->get('text_all_category'),
                'url' => $this->url->link('common/shop', modQuery($url,'path'), true),
            );
        }
        if(isset($result['visible_collection']['visible']) && $result['visible_collection']['visible'] == 1){
            $related_collection = $this->model_catalog_collection->getAllcollection();
            foreach ($related_collection as $collection){
                if(isset($this->request->get['collection']) && $this->request->get['collection'] == $collection['collection_id']){
                    $collection_url = $this->url->link('common/shop', modQuery($url,'collection'), true);
                    $result['collection_selected_name'] = $collection['title'];
                }else{
                    $collection_url = $this->url->link('common/shop', modQuery($url,'collection'). '&collection=' . $collection['collection_id'], true);
                }
                $result['collection'][] = array(
                    'collection_id'  => $collection['collection_id'],
                    'name'  => $collection['title'],
                    'url' => $collection_url,
                );
            }
            // option: tất cả bộ sưu tập
            $result['collection_all'] = array(
                'collection_id' => '-1',
                'name' => $this->language->get('text_all_collection'),
                'url' => $this->url->link('common/shop', modQuery($url,'collection'), true),
            );
        }
        return $result;
    }*/
}
