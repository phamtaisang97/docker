<?php

class ControllerCommonHeader extends Controller
{
    use Device_Util;
    use Theme_Config_Util;
    use Product_Util_Trait;
    use Setting_Util_Trait;

    const CONFIG_GG_SHOPPING = 'config_gg_shopping';
    const ASSET_RESOURCES_JSON_FILE_NAME = 'resources.json';
    const CART_ROUTE = 'checkout/my_orders';
    const ORDER_ROUTE = 'checkout/order_preview';
    const ORDER_SUCCESS_ROUTE = 'checkout/order_preview/orderSuccess';

    const PLACEHOLDER_IMAGE_URL = '/catalog/view/theme/default/images/placeholder.png';

    const FINAL_MINIFIED_FILE_NAME = 'final_minified';

    public function index()
    {
        // check if customer be block or delete then logout
        $this->checkCustomerStatus();

        // count website traffic
        // $this->countWebTraffic();
        // end

        $this->load->language('common/header');
        $this->load->model('tool/image');
        $this->load->model('catalog/collection');

        $data = [];
        $data['is_on_mobile'] = $this->isMobile() ? 1 : 0;

        // load resource
        $reSource = $this->loadResourcesRaw();
        $data['cssResource'] = array_key_exists('css', $reSource) ? $reSource['css'] : '';
        $data['jsResource'] = array_key_exists('js', $reSource) ? $reSource['js'] : '';
        $data['theme_name'] = $this->getCurrentThemeDir();
        $data['theme_directory'] = '/catalog/view/theme/' . $data['theme_name'];
        $data['final_minified_file_name'] = self::FINAL_MINIFIED_FILE_NAME;

        $data['scripts'] = $this->document->getScripts();

        // Analytics
        $this->load->model('setting/extension');
        $this->load->model('catalog/category');
        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

        $data['analytics'] = array();

        $analytics = $this->model_setting_extension->getExtensions('analytics');

        foreach ($analytics as $analytic) {
            if ($this->config->get('analytics_' . $analytic['code'] . '_status')) {
                $data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get('analytics_' . $analytic['code'] . '_status'));
            }
        }

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
            $this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
        } elseif ($this->model_tool_image->imageIsExist($this->config->get('config_icon'))) {
            $this->document->addLink($this->config->get('config_icon'), 'icon');
        }

        if (!is_null($this->config->get('online_store_config_homepage_title'))) {
            $this->document->setTitle($this->config->get('online_store_config_homepage_title'));
        }

        $data['title'] = $this->document->getTitle();

        /* get favicon config */
        $favicon_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_FAVICON);
        $favicon_config = json_decode($favicon_config, true);

        $data['favicon'] = isset($favicon_config['image']['url']) ? cloudinary_change_http_to_https($favicon_config['image']['url']) : '';

        $data['base'] = $server;
        $data['description'] = $this->document->getDescription();
        $data['keywords'] = $this->document->getKeywords();
        $data['links'] = $this->document->getLinks();
        $data['styles'] = $this->document->getStyles();
        $data['scripts'] = $this->document->getScripts('header');
        //$data['scripts'][] = 'catalog/view/javascript/jquery/jquery-bestme.min.js';
//        $data['scripts'][] = 'catalog/view/javascript/bestme_app_render.js';

        $data['lang'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');

        $data['name'] = $this->config->get('config_name');

        /* get title category*/
        $keys = array(
            'config_category_title'
        );
        $category_title = $this->model_catalog_category->getMultiSettingValue($keys);
        if (!empty($category_title) && $category_title[0]['value'] != null){
            $data['category_title'] = $category_title[0]['value'];
        }
        else {
            $data['category_title'] = "Danh mục sản phẩm";
        }

        /* get header config */
        $header_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER);
        $header_config = json_decode($header_config, true, 512, JSON_UNESCAPED_UNICODE);

        /* display notify bar */
        $data['notify_bar']['visible'] = isset($header_config['notify-bar']) ? $header_config['notify-bar']['visible'] : '';
        $data['notify_bar']['content'] = isset($header_config['notify-bar']) ? $header_config['notify-bar']['content'] : '';
        $data['notify_bar']['url'] = isset($header_config['notify-bar']) ? $header_config['notify-bar']['url'] : '';

        /* display logo */
        $data['logo'] = isset($header_config['logo']['url']) ? cloudinary_change_http_to_https($header_config['logo']['url']) : 'image/no_image.png';
        $data['alt'] = isset($header_config['logo']['alt']) ? cloudinary_change_http_to_https($header_config['logo']['alt']) : 'Cannot find photo';

        $data['logo_height'] = isset($header_config['logo']['height']) ? $header_config['logo']['height'] : 50;

        /* display list product categories */
        //menu doc
        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

        $list_product_categories = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_LIST_PRODUCT);
        $list_product_categories = json_decode($list_product_categories, true);

        $group_menu_id_hor = $list_product_categories['display']['menu']['id'];
        $this->load->model('custom/common');
        // NOT USE ??
//        $list_product_categories = $this->model_custom_common->getListMenuItemByGroupMenuId($group_menu_id_hor);
//        foreach ($list_product_categories as $product_category) {
//            $url = ($product_category['url'] == '') ? 'index.php?route=product/category&path=' . $product_category['id'] : $product_category['url'];
//            $data['list_product_categories'][] = array(
//                'id' => $product_category['id'],
//                'text' => $product_category['text'],
//                'url' => $url,
//                'items' => $product_category['items']
//            );
//        }

        // NOTICE: list categories in system (for header menu categories listing, ...)
        $data['all_categories'] = $this->generateCategories();

        /* display main menu */
        $menu_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER);
        $menu_config = json_decode($menu_config, true);

        $mainMenuId = isset($menu_config['menu']['display-list']['id']) ? $menu_config['menu']['display-list']['id'] : -1;
        //menu ngang
        $this->load->model('custom/menu');
        $mainMenu = $this->model_custom_menu->getMenuById($mainMenuId);

        // default if empty
        // $mainMenu = [];
        $mainMenu = !empty($mainMenu) ? $mainMenu : [
            [
                'text' => 'Trang chủ',
                'url' => $this->url->link('common/home', '', true)
            ],
        ];
        $data['main_menu'] = $mainMenu;

        /* categories on search area */
        $data['all_categories_search'] = $list_product_categories;

        // Active menu item
        $arr_mainMenu = array();
        foreach ($mainMenu as $vl_mainMenu) {
            $arr_mainMenu[] = $vl_mainMenu['url'];
        }

        $url_referer = $_SERVER['QUERY_STRING'];
        $input = strtok($url_referer, '&');
        $input = $input_suffix = str_replace("_route_=", "", $input);
        $index_url = $this->url->link('common/home', '', true);
        $input = $index_url . $input; // $input = $this->url->link('common/home', '', true) = (example) http://x2.bestme.test/
        $result_active_menu = '';
        foreach ($arr_mainMenu as $item) {
            if (!empty($item) && is_string($item)) { // input_suffix support for menu type url
                if ($item == $input || $item == $input_suffix) {
                    $result_active_menu = $item;
                    break;
                }
                if (strpos($input, $item) === 0 || strpos($input_suffix, $item) === 0) {
                    // ignore highlight home page
                    if ($index_url != $item) {
                        /**
                         * get longer item (more accurate)
                         * eg: $arr_mainMenu = [
                         *   'http://x2.bestme.test/',
                         *   'http://x2.bestme.test/san-pham'
                         * ]
                         * $input = 'http://x2.bestme.test/san-pham/tranh-dinh-da'
                         * => $result_active_menu = 'http://x2.bestme.test/san-pham'
                         */
                        $result_active_menu = strlen($result_active_menu) < strlen($item) ? $item : $result_active_menu;
                    }
                }
            }
        }

        /* Old code. TODO: restore or remove...
        $url_referer = $_SERVER['QUERY_STRING'];
        $input_seo = strtok($url_referer, '&');
        $input_seo = str_replace("_route_=","",$input_seo);
        $input = '?route=' . (!isset($this->request->get['route']) || empty($this->request->get['route']) ? 'common/home' : $this->request->get['route']);

        $result_active_menu = '';
        foreach ($mainMenu as $item){
            // 1. check raw router
            if ($input == $item['target_value']){
                $result_active_menu = $item['target_value'];
                break;
            }

            // 2. check seo router
            if ($input_seo == $item['target_value']){
                $result_active_menu = $item['target_value'];
                break;
            }
        }*/

        $data['result_active_menu'] = $result_active_menu;
        // Active menu item

        $this->load->language('common/header');
        $this->load->model('appstore/my_app');

        // Wishlist
        if ($this->customer->isLogged()) {
            $this->load->model('account/wishlist');

            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
        } else {
            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
        }

        /* GTM code */
        $data['gtm_code_header'] = $this->config->get('online_store_config_gtm_code_header');
        $data['gtm_code_header'] = empty($data['gtm_code_header']) ? null : html_entity_decode($data['gtm_code_header']);
        $data['gtm_code_body'] = $this->config->get('online_store_config_gtm_code_body');
        $data['gtm_code_body'] = empty($data['gtm_code_body']) ? null : html_entity_decode($data['gtm_code_body']);

        /* Fb sdk code */
        $data['fbsdk_code_body'] = $this->config->get('online_store_config_fbsdk_code_body');
        $data['fbsdk_code_body'] = empty($data['fbsdk_code_body']) ? null : html_entity_decode($data['fbsdk_code_body']);

        /* GG Analytics code */
        $data['google_analytics_code'] = $this->config->get('online_store_config_google_code');
        $data['google_analytics_code'] = empty($data['google_analytics_code']) ? null : html_entity_decode($data['google_analytics_code']);

        $data['google_analytics_code_cart'] = $this->config->get('online_store_config_google_code_cart');
        $data['google_analytics_code_order'] = $this->config->get('online_store_config_google_code_order');
        $data['google_analytics_code_order_success'] = $this->config->get('online_store_config_google_code_order_success');

        if (!empty($data['google_analytics_code'])) {
            $data['google_analytics_code'] .= (!empty($data['google_analytics_code_cart']) && isset($this->request->get['route']) && $this->request->get['route'] == self::CART_ROUTE) ? html_entity_decode($data['google_analytics_code_cart']) : '';
            $data['google_analytics_code'] .= (!empty($data['google_analytics_code_order']) && isset($this->request->get['route']) && $this->request->get['route'] == self::ORDER_ROUTE) ? html_entity_decode($data['google_analytics_code_order']) : '';
            $data['google_analytics_code'] .= (!empty($data['google_analytics_code_order_success']) && isset($this->request->get['route']) && $this->request->get['route'] == self::ORDER_SUCCESS_ROUTE) ? html_entity_decode($data['google_analytics_code_order_success']) : '';
        }

        /* Facebook Pixel ID */
        $data['facebook_pixel'] = $this->config->get('online_store_config_facebook_pixel');
        $data['facebook_pixel'] = empty($data['facebook_pixel']) ? null : html_entity_decode($data['facebook_pixel']);

        $data['facebook_pixel_cart'] = $this->config->get('online_store_config_facebook_pixel_cart');
        $data['facebook_pixel_order'] = $this->config->get('online_store_config_facebook_pixel_order');
        $data['facebook_pixel_order_success'] = $this->config->get('online_store_config_facebook_pixel_order_success');

        /* very good job on code append here. @kyvv. this makes no need to change all themes to add following codes */
        if (!empty($data['facebook_pixel'])) {
            $data['facebook_pixel'] .= (!empty($data['facebook_pixel_cart']) && isset($this->request->get['route']) && $this->request->get['route'] == self::CART_ROUTE) ? html_entity_decode($data['facebook_pixel_cart']) : '';
            $data['facebook_pixel'] .= (!empty($data['facebook_pixel_order']) && isset($this->request->get['route']) && $this->request->get['route'] == self::ORDER_ROUTE) ? html_entity_decode($data['facebook_pixel_order']) : '';
            $data['facebook_pixel'] .= (!empty($data['facebook_pixel_order_success']) && isset($this->request->get['route']) && $this->request->get['route'] == self::ORDER_SUCCESS_ROUTE) ? html_entity_decode($data['facebook_pixel_order_success']) : '';
        }

        /* Maxlead code */
        if($this->model_appstore_my_app->checkAppActive(ModelAppstoreMyApp::APP_MAX_LEAD)){
            $data['maxlead_code'] = $this->config->get('online_store_config_maxlead_code');
            $data['maxlead_code'] = empty($data['maxlead_code']) ? null : html_entity_decode($data['maxlead_code']);
        };

        $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

        // livechatx9
        if($this->model_appstore_my_app->checkAppActive('mar_ons_livechatx9')){
            // TODO
        };

        /* get currency symbol */
        $data['currency'] = $this->currency->getSymbolRight($this->session->data['currency']);
        if (empty($data['currency'])) {
            $data['currency'] = $this->currency->getSymbolLeft($this->session->data['currency']);
        }
        $data['currency'] = 'đ';

        $data['home'] = $this->url->link('common/home');

        $data['is_home'] = false;
        $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        if ((isset($this->request->get['route']) && $this->request->get['route'] == 'common/home') || in_array($path, ['/trang-chu', '/home', '/'])) {
            $data['is_home'] = true;
        }

        $data['register'] = $this->url->link('account/register', '', true);
        $data['login'] = $this->url->link('account/login', '', true);
        $data['logout'] = $this->url->link('account/logout', '', true);
        $data['my_account'] = $this->url->link('checkout/profile', '', true);
        $data['wishlist'] = $this->url->link('account/wishlist', '', true);
        $data['search'] = $this->url->link('common/header/getSearchKeyup', '', true);

        /* total money in cart */
        $this->load->model('catalog/product');
        $key_for_current_order = "current_order_" . $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
        $current_order = isset($this->session->data[$key_for_current_order]) ? $this->session->data[$key_for_current_order] : [];
        $total_into_money = 0;
        $total_product_cart = 0;
        $totalQuantityAddOnProduct = 0;

        foreach ($current_order as $c_order) {
            if (!isset($c_order['product_id']) || !isset($c_order['quantity'])) {
                continue;
            }

            // find product
            $product_id = $c_order['product_id'];
            $product = $this->model_catalog_product->getProduct($product_id);
            $data['is_product_existing'] = true;
            if (empty($product) || !is_array($product)) {
                $data['is_product_existing'] = false;
            }

            if (isset($c_order['attribute']) && $c_order['attribute'] && isset($c_order['product_version']['price'])) {
                if ((int)$c_order['product_version']['price'] == 0) $c_order['product_version']['price'] = $c_order['product_version']['compare_price'];
                $price = (float)$c_order['product_version']['price'];
            } else {
                if ((int)$product['price'] == 0) $product['price'] = $product['compare_price_master'];
                $price = (float)$product['price'];
            }

            $total = $price * $c_order['quantity'];
            $total_into_money += $total;
            // $total_product_cart += $c_order['quantity'];
            $total_product_cart += 1;

            // count add-on product
            if (!empty($c_order['add_on_products'])) {
                foreach ($c_order['add_on_products'] as $add_on_product) {
                    $totalQuantityAddOnProduct += 1;
                }
            }

        }
        $data['order_total_money'] = $this->currency->formatCustom($total_into_money, $this->session->data['currency']);
        $data['total_product_cart'] = $total_product_cart + $totalQuantityAddOnProduct;

        /* go to cart button */
        $data['href_my_orders'] = $this->url->link('checkout/my_orders', '', true);

        /* asset_css_override_dir */
        if (isset($this->request->get['config'])) { // preview
            // No need to use old overridden css files. TODO: remove...
            //$local_override_asset_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/';

            // IMPORTANT: support load override css for load-balancing
            // use "token" as sub-dir for latest overridden css
            // if defined "token", load it!
            if (isset($this->session->data['preview_builder_config_version'])) {
                $override_css_version = $this->session->data['preview_builder_config_version'];
            } else {
                $override_css_version = token(32);
                $this->session->data['preview_builder_config_version'] = $override_css_version;
            }

            $local_override_asset_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/' . $override_css_version . '/';
        } else { // real shop
            $local_override_asset_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/';

            // IMPORTANT: support load override css for load-balancing
            // use "token" as sub-dir for latest overridden css
            // if defined "token", load it!
            $this->load->model('setting/setting');
            $override_css_version = $this->model_setting_setting->getSettingValue('builder_config_version');
            if ($override_css_version) {
                $local_override_asset_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/' . $override_css_version . '/';
            }
        }

        if (!is_dir($local_override_asset_dir)) {
            if ($override_css_version) {
                if (isset($this->request->get['config'])) {
                    $this->rebuildCss($override_css_version, true);
                } else {
                    $this->rebuildCss($override_css_version);
                }
            }
        }

        if (is_dir($local_override_asset_dir)) {
            if (isset($this->request->get['config'])) {
                $data['asset_css_override_dir'] = HTTP_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/';
                if ($override_css_version) {
                    $data['asset_css_override_dir'] = HTTP_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/' . $override_css_version . '/';
                }
            } else {
                $data['asset_css_override_dir'] = HTTP_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/';
                if ($override_css_version) {
                    $data['asset_css_override_dir'] = HTTP_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/' . $override_css_version . '/';
                }
            }
        } else {
            /* NOT LOAD DEFAULT override css. return empty instead */
            /*$server = $this->request->server['HTTPS'] ? HTTPS_SERVER : HTTP_SERVER;
            $data['asset_css_override_dir'] = $server . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/';
            if (isset($this->request->get['config'])) {
                $data['asset_css_override_dir'] = $server . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/';
            }*/
            $data['asset_css_override_dir'] = '';
        }

        $data['urlserach'] = str_replace(' ', '', $this->url->link('common/shop', modQuery('', 'serach') . '', true));
        $data['href_product_list'] = $this->url->link('common/shop', '', true);

        /* login|register|logout button */
        $data['logged_in'] = $this->customer->isLogged();
        $data['logged_user_email'] = $this->customer->getEmail();
        $data['logged_firstname'] = $this->customer->getFirstname();
        $data['logged_lastname'] = $this->customer->getLastname();
        $data['logged_user'] = $data['logged_lastname'] . ' ' . $data['logged_firstname'];
        $data['href_profile'] = $this->url->link('checkout/profile', '', true);
        $data['href_history'] = $this->url->link('checkout/history', '', true);
        $data['href_search_action'] = 'index.php';

        /* build seo meta*/
        $getUrl_product_id = ((isset($this->request->get['product_id'])) ? $this->request->get['product_id'] : '');
        $getUrl_blog_id = ((isset($this->request->get['blog_id'])) ? $this->request->get['blog_id'] : '');
        $data['filter'] = $this->getFilterProduct();

        $data['meta_robots_content'] = 'index, follow';
        $arrNoIndexNoFollow = ['account/register', 'account/login', 'checkout/my_orders', 'checkout/order_preview', 'checkout/profile', 'account/order/info', 'checkout/setting'];

        if (!empty($this->request->get['route']) && in_array($this->request->get['route'], $arrNoIndexNoFollow)) {
            $data['meta_robots_content'] = 'noindex, nofollow';
        }

        // if page product detail
        if (isset($getUrl_product_id) && $getUrl_product_id > 0) {
            $data['action'] = $this->url->link('checkout/my_orders', '', true);
            $this->load->model('catalog/product');
            $this->load->model('design/seo_url');
            $product = $this->model_catalog_product->getProduct($getUrl_product_id);
            if ($product) {
                if ($product['max_price_version'] == NULL) {
                    $special_price = number_format($product['price_master'], 0, ",", ".") . " " . "VND";
                    $meta_special_price = (float)$product['price_master'];
                } else {
                    $special_price = number_format($product['min_price_version'], 0, ",", ".") . " " . "VND";
                    $meta_special_price = (float)$product['min_price_version'];
                    if ($product['max_price_version'] != $product['min_price_version']) {
                        $special_price .= ' ~ ' . number_format($product['max_price_version'], 0, ",", ".") . " " . "VND";
                    }
                }

                if ($product['max_compare_price_version'] == NULL) {
                    $original_price = number_format($product['compare_price_master'], 0, ",", ".") . " " . "VND";
                    $meta_original_price = (float)$product['compare_price_master'];
                } else {
                    $original_price = number_format($product['min_compare_price_version'], 0, ",", ".") . " " . "VND";
                    $meta_original_price = (float)$product['min_compare_price_version'];
                    if ($product['max_compare_price_version'] != $product['min_compare_price_version']) {
                        $original_price .= ' ~ ' . number_format($product['max_compare_price_version'], 0, ",", ".") . " " . "VND";
                    }
                }

                // for new meta name "product_price" for AutoAds Dynamic Re-Marketing
                $_original_price = $original_price;
                $_special_price = $special_price;
                $_price = empty($_special_price) ? extract_number($_original_price, ',') : extract_number($_special_price, ',');
                // end - for new meta name "product_price" for AutoAds Dynamic Re-Marketing
                $_meta_price = empty($meta_special_price) ? $meta_original_price : $meta_special_price;

                $original_price = $original_price > 0 ? ($special_price > 0 ? 'Giá gốc: ' : 'Giá: ') . $original_price : '';
                $special_price = $special_price > 0 ? 'Giá KM: ' . $special_price . ' ' : '';
                $append = $special_price == '' ? $original_price : $special_price;
                $seo_url_products = $this->model_design_seo_url->getSeoUrlsByQueryProduct($getUrl_product_id);
                $seo_url_product = (isset($seo_url_products['keyword']) ? $seo_url_products['keyword'] : '');
                $SeoProduct = $this->model_catalog_product->getSeoProduct($getUrl_product_id);
                if (isset($SeoProduct) && !empty($SeoProduct)) {
                    $title = htmlentities($SeoProduct[0]['seo_title'] == null ? $product['name'] : $SeoProduct[0]['seo_title'], ENT_QUOTES);// . SEO_TITLE_BRAND;
                    $description = htmlentities($SeoProduct[0]['seo_description'] == null ? $product['name'] : $SeoProduct[0]['seo_description'], ENT_QUOTES);
                } else {
                    $title = '';
                    $description = '';
                }

                $product['sale_on_out_stock_check'] = true;
                if (empty($product['product_version_id']) && $product['product_master_quantity'] < 1 && $product['sale_on_out_stock'] == 0) {
                    $product['sale_on_out_stock_check'] = false;
                }
                $data['product_detail'] = $product;
                $data['product_currency'] = $this->session->data['currency'];
                $data['product_current_detail'] = $this->url->link('product/product', '&product_id=' . $product['product_id']);

                $keywords = $product['meta_keyword'];
                $main_domain = $this->getMainDomain();
                if (empty($keywords)) {
                    $keywords = "{$title},{$main_domain}";
                }

                $image = $this->getUrlWithCb($product['image']);
                $url = $this->url->link('product/product', '&product_id=' . $product['product_id']);
                $data['seo_meta'] = '<title>' . $title . '</title>' . "\n";
                // for new meta for AutoAds Dynamic Re-Marketing
//                $data['seo_meta'] .= '    <meta name="product_id" content="' . $getUrl_product_id . '" />' . "\n";
//                $data['seo_meta'] .= '    <meta name="product_price" content="' . $_price . '" />' . "\n";
                // end - for new meta for AutoAds Dynamic Re-Marketing
                $data['seo_meta'] .= '    <meta property="product:price:currency" content="VND"> ' . "\n";
                $data['seo_meta'] .= '    <meta property="product:price:amount" content="'.$_meta_price.'">' . "\n";

                $data['seo_meta'] .= '    <meta name="title" content="' . $title . '" />' . "\n";
                $data['seo_meta'] .= '    <meta name="twitter:card" content="' . $description . '">' . "\n";
                $data['seo_meta'] .= '    <meta name="twitter:description" content="' . $append . " - " . $description . '" >' . "\n";
                $data['seo_meta'] .= '    <meta name="twitter:domain" content="' . $url . '">' . "\n";
                $data['seo_meta'] .= '    <meta name="twitter:image:src" content="' . $image . '">' . "\n";
                $data['seo_meta'] .= '    <meta name="twitter:site" content="' . $title . '">' . "\n";
                $data['seo_meta'] .= '    <meta name="twitter:title" content="' . $title . '">' . "\n";
                $data['seo_meta'] .= '    <meta name="description" content="' . $description . '" />' . "\n";
                $data['seo_meta'] .= '    <meta name="keywords" content="' . $keywords . '" />' . "\n";
                $data['seo_meta'] .= '    <meta name="image" content="' . $image . '" />' . "\n";
                $data['seo_meta'] .= '    <link rel="canonical" href="' . $url . '" />' . "\n";
                $data['seo_meta'] .= '    <meta property="og:url" content="' . $url . '" />' . "\n";
                $data['seo_meta'] .= '    <meta property="og:title" content="' . $title . '" />' . "\n";
                $data['seo_meta'] .= '    <meta property="og:description" content="' . $append . " - " . $description . '" />' . "\n";
                $data['seo_meta'] .= '    <meta property="og:image" content="' . $image . '" />' . "\n";
                $data['seo_meta'] .= '    <meta property="og:image:secure_url" content="' . $image . '" />' . "\n";
                $data['seo_meta'] .= '    <meta property="og:image:width" content="450"/>' . "\n";
                $data['seo_meta'] .= '    <meta property="og:image:height" content="298"/>' . "\n";
                $data['seo_meta'] .= '    <meta property="og:site_name" content="' . $main_domain . '" />' . "\n";
                $data['seo_meta'] .= '    <meta property="og:type" content="website"/>' . "\n";
                $data['seo_meta'] .= '    <meta property="og:locale" content="vi_VN"/>' . "\n";
                $data['seo_meta'] .= '    <link rel="image_src" href="' . $image . '" />' . "\n";
                $data['seo_meta'] .= '    <link rel="alternate" href="' . $url . '" hreflang="vi-vn" />' . "\n";

                $data['meta_robots_content'] = $this->getSeoRobotsContent($product);
                $meta_pixel_view_content_data['product_id'] = $getUrl_product_id;
            }
        } elseif (isset($getUrl_blog_id) && $getUrl_blog_id > 0) { // page blog
            $data['action'] = $this->url->link('checkout/my_orders', '', true);
            $this->load->model('blog/blog');
            $this->load->model('design/seo_url');
            $blog = $this->model_blog_blog->getBlogById($getUrl_blog_id);
            $title = htmlentities($blog['meta_title'], ENT_QUOTES);
            $description = htmlentities($blog['meta_description'], ENT_QUOTES);

            $keywords = $blog['seo_keywords'];
            $main_domain = $this->getMainDomain();
            if (empty($keywords)) {
                $keywords = "{$title},{$main_domain}";
            }

            $image = $this->getUrlWithCb($blog['image']);
            $seo_url_blogs = $this->model_design_seo_url->getSeoUrlsByQueryBlog($getUrl_blog_id);
            $seo_url_blog = (isset($seo_url_blogs['keyword']) ? $seo_url_blogs['keyword'] : '');

            $url = $this->url->link('blog/blog/detail', '&blog_id=' . $blog['blog_id']);
            $data['title'] = empty($title) ? $data['title'] : $title;
            $data['seo_meta'] = "";
            $data['seo_meta'] .= '<title>' . $title . '</title>' . "\n";
            $data['seo_meta'] .= '    <meta name="title" content="' . $title . '" />' . "\n";
            $data['seo_meta'] .= '    <meta name="twitter:card" content="' . $description . '">' . "\n";
            $data['seo_meta'] .= '    <meta name="twitter:description" content="' . $description . '" >' . "\n";
            $data['seo_meta'] .= '    <meta name="twitter:domain" content="' . $url . '">' . "\n";
            $data['seo_meta'] .= '    <meta name="twitter:image:src" content="' . $image . '">' . "\n";
            $data['seo_meta'] .= '    <meta name="twitter:site" content="' . $title . '">' . "\n";
            $data['seo_meta'] .= '    <meta name="twitter:title" content="' . $title . '">' . "\n";
            $data['seo_meta'] .= '    <meta name="description" content="' . $description . '" />' . "\n";
            $data['seo_meta'] .= '    <meta name="keywords" content="' . $keywords . '" />' . "\n";
            $data['seo_meta'] .= '    <meta name="image" content="' . $image . '" />' . "\n";
            $data['seo_meta'] .= '    <link rel="canonical" href="' . $url . '" />' . "\n";
            $data['seo_meta'] .= '    <meta property="og:url" content="' . $url . '" />' . "\n";
            $data['seo_meta'] .= '    <meta property="og:title" content="' . $title . '" />' . "\n";
            $data['seo_meta'] .= '    <meta property="og:description" content="' . $description . '" />' . "\n";
            $data['seo_meta'] .= '    <meta property="og:image" content="' . $image . '" />' . "\n";
            $data['seo_meta'] .= '    <meta property="og:image:secure_url" content="' . $image . '" />' . "\n";
            $data['seo_meta'] .= '    <meta property="og:image:width" content="450"/>' . "\n";
            $data['seo_meta'] .= '    <meta property="og:image:height" content="298"/>' . "\n";
            $data['seo_meta'] .= '    <meta property="og:site_name" content="' . $main_domain . '" />' . "\n";
            $data['seo_meta'] .= '    <meta property="og:type" content="website"/>' . "\n";
            $data['seo_meta'] .= '    <meta property="og:locale" content="vi_VN"/>' . "\n";
            $data['seo_meta'] .= '    <link rel="image_src" href="' . $image . '" />' . "\n";
            $data['seo_meta'] .= '    <link rel="alternate" href="' . $url . '" hreflang="vi-vn" />' . "\n";
        } elseif ($data['is_home']) { // page homepage
            $this->load->model('setting/setting');
            $title = HOME_PAGE_META_TITLE;
            $description = HOME_PAGE_META_DESC;

            $keywords = HOME_PAGE_META_KEYWORD;
            $main_domain = $this->getMainDomain();
            if (empty($keywords)) {
                $keywords = "{$title},{$main_domain}";
            }

            $image = $this->getUrlWithCb($data['logo']);
            $data['seo_meta'] = '';
            $data['seo_meta'] .= '<title>' . $title . '</title>' . "\n";
            $data['seo_meta'] .= '<meta name="title" content="' . $title . '" />' . "\n";
            $data['seo_meta'] .= '<meta name="twitter:card" content="' . $description . '">' . "\n";
            $data['seo_meta'] .= '<meta name="twitter:description" content="' . $description . '">' . "\n";
            $data['seo_meta'] .= '<meta name="twitter:domain" content="' . $data['home'] . '">' . "\n";
            $data['seo_meta'] .= '<meta name="twitter:image:src" content="' . $image . '">' . "\n";
            $data['seo_meta'] .= '<meta name="twitter:site" content="' . $title . '">' . "\n";
            $data['seo_meta'] .= '<meta name="twitter:title" content="' . $title . '">' . "\n";
            $data['seo_meta'] .= '<meta name="twitter:url" content="' . $data['home'] . '">' . "\n";
            $data['seo_meta'] .= '<meta name="description" content="' . $description . '" />' . "\n";
            $data['seo_meta'] .= '<meta name="keywords" content="' . $keywords . '" />' . "\n";
            $data['seo_meta'] .= '<meta name="image" content="' . $image . '" />' . "\n";
            $data['seo_meta'] .= '<link rel="canonical" href="' . $data['home'] . '" />' . "\n";
            $data['seo_meta'] .= '<meta property="og:url" content="' . $data['home'] . '" />' . "\n";
            $data['seo_meta'] .= '<meta property="og:title" content="' . $title . '" />' . "\n";
            $data['seo_meta'] .= '<meta property="og:description" content="' . $description . '" />' . "\n";
            $data['seo_meta'] .= '<meta property="og:image" content="' . $image . '" />' . "\n";
            $data['seo_meta'] .= '<meta property="og:image:secure_url" content="' . $data['home'] . '" />' . "\n";
            $data['seo_meta'] .= '<meta property="og:image:width" content="450"/>' . "\n";
            $data['seo_meta'] .= '<meta property="og:image:height" content="298"/>' . "\n";
            $data['seo_meta'] .= '<meta property="og:site_name" content="' . $main_domain . '" />' . "\n";
            $data['seo_meta'] .= '<meta property="og:type" content="website"/>' . "\n";
            $data['seo_meta'] .= '<meta property="og:locale" content="vi_VN"/>' . "\n";
            $data['seo_meta'] .= '<link rel="image_src" href="' . $image . '" />' . "\n";
            $data['seo_meta'] .= '<link rel="alternate" href="' . $data['home'] . '" hreflang="vi-vn" />' . "\n";
        } else { // page
            $url = HTTPS_DOMAIN_SERVER . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $main_domain = HTTPS_DOMAIN_SERVER;

            $path = explode("&", $url);

            if (!empty($this->request->get['search'])) {
                $title = "Kết quả tìm kiếm {$this->request->get['search']} | {$main_domain}";
            } else {
                $title = (!empty($data['title']) ? $data['title'] : $this->language->get('text_home'));
            }

            if (isset($this->request->get['route']) &&
                ($this->request->get['route'] == 'common/shop' && empty($data['filter']['category_selected_name']))
            ) {
                $title = $this->replaceTitleAndDescription(PRODUCTS_PAGE_META_TITLE);
                $description = $this->replaceTitleAndDescription(PRODUCTS_PAGE_META_DESC);
                $keywords = PRODUCTS_PAGE_META_KEYWORD;
            } elseif (isset($this->request->get['route']) &&
                ($this->request->get['route'] == 'blog/blog' && empty($this->request->get['blog_category_id']))) {
                $title = BLOGS_PAGE_META_TITLE;
                $description = BLOGS_PAGE_META_DESC;
                $keywords = BLOGS_PAGE_META_KEYWORD;
            } else {
                $description = $title;
                $keywords = '';
            }

            if (isset($this->request->get['route']) && $this->request->get['route'] == 'blog/blog') {
                // blog page
                $blog_category_id = empty($this->request->get['blog_category_id']) ? '' : $this->request->get['blog_category_id'];
                if ($blog_category_id) {
                    // blog category page
                    $this->load->model('blog/category');
                    $blog_category = $this->model_blog_category->getBlogCategory($blog_category_id);
                    $title = empty($blog_category['title']) ? $this->language->get('text_blogs') : $blog_category['title'];
                    $description = !empty($blog_category['meta_description']) ? $blog_category['meta_description'] : $title;
                    $keywords = !empty($blog_category['meta_keyword']) ? $blog_category['meta_keyword'] : '';
                }
            }

            if (isset($this->request->get['route']) && $this->request->get['route'] == 'contact/contact') {
                $title = CONTACT_PAGE_META_TITLE;
                $description = CONTACT_PAGE_META_DESC;
                $keywords = CONTACT_PAGE_META_KEYWORD;
            }

            if (empty($keywords)) {
                $keywords = "{$title},{$main_domain}";
            }

            $data['page'] = !empty($path[0]) ? $path[0] : $url;

            if (isset($this->request->get['route']) && $this->request->get['route'] == 'error/not_found') {
                $data['page'] = $main_domain;
                    $data['page_no_index_no_follow'] = 0;
            }

            $data['seo_meta'] = '';
            if (isset($this->request->get['route'])) {
                if ($this->request->get['route'] == 'common/shop') {
                    if (isset($data['filter']['category_selected_name'])) {
                        // product category page
                        $data['category_filter_name'] = isset($data['filter']['category_selected_name']) ? $data['filter']['category_selected_name'] : $data['title'];
                        $data['category_filter_meta_title'] = isset($data['filter']['category_selected_meta_title']) ? $data['filter']['category_selected_meta_title'] : $data['title'];
                        $data['category_filter_meta_keyword'] = isset($data['filter']['category_selected_meta_keyword']) ? $data['filter']['category_selected_meta_keyword'] : $data['title'];
                        $data['category_filter_meta_description'] = isset($data['filter']['category_selected_meta_description']) ? $data['filter']['category_selected_meta_description'] : $data['title'];
                        $title = !empty($data['category_filter_meta_title']) ? $data['category_filter_meta_title'] : $data['category_filter_name'];

                        $title = $this->replaceTitleAndDescription($title);
                        $data['category_filter_meta_title'] = $this->replaceTitleAndDescription($data['category_filter_meta_title']);
                        $data['category_filter_meta_description'] = $this->replaceTitleAndDescription($data['category_filter_meta_description']);

                        $data['seo_meta'] .= '<title>' . $title . SEO_TITLE_BRAND . '</title>' . "\n";
                        $data['seo_meta'] .= '<meta name="title" content="' . $data['category_filter_meta_title'] . '" />' . "\n";
                        $data['seo_meta'] .= '<meta name="keywords" content="' . $data['category_filter_meta_keyword'] . '" />' . "\n";
                        $data['seo_meta'] .= '<meta name="description" content="' . $data['category_filter_meta_description'] . '" />' . "\n";

                        $category_data = $this->model_catalog_category->getCategoryByName(['name' => $data['filter']['category_selected_name']]);
                        $data['meta_robots_content'] = $this->getSeoRobotsContent($category_data);
                    } elseif (isset($data['filter']['collection_selected_name'])) {
                        $data['collection_filter_name'] = isset($data['filter']['collection_selected_name']) ? $data['filter']['collection_selected_name'] : $data['title'];
                        $data['collection_filter_meta_title'] = isset($data['filter']['collection_selected_meta_title']) ? $data['filter']['collection_selected_meta_title'] : $data['title'];
                        $data['collection_filter_meta_keyword'] = isset($data['filter']['collection_selected_meta_keyword']) ? $data['filter']['collection_selected_meta_keyword'] : $data['title'];
                        $data['collection_filter_meta_description'] = isset($data['filter']['collection_selected_meta_description']) ? $data['filter']['collection_selected_meta_description'] : $data['title'];
                        $title = !empty($data['collection_filter_meta_title']) ? $data['collection_filter_meta_title'] : $data['collection_filter_name'];

                        $title = $this->replaceTitleAndDescription($title);
                        $data['collection_filter_meta_title'] = $this->replaceTitleAndDescription($data['collection_filter_meta_title']);
                        $data['collection_filter_meta_description'] = $this->replaceTitleAndDescription($data['collection_filter_meta_description']);

                        $data['seo_meta'] .= '<title>' . $title . SEO_TITLE_BRAND . '</title>' . "\n";
                        $data['seo_meta'] .= '<meta name="title" content="' . $data['collection_filter_meta_title'] . '" />' . "\n";
                        $data['seo_meta'] .= '<meta name="keywords" content="' . $data['collection_filter_meta_keyword'] . '" />' . "\n";
                        $data['seo_meta'] .= '<meta name="description" content="' . $data['collection_filter_meta_description'] . '" />' . "\n";

                        $collection_data = $this->model_catalog_collection->getCollectionByName(['name' => $data['filter']['collection_selected_name']]);
                        $data['meta_robots_content'] = $this->getSeoRobotsContent($collection_data);
                    } else {
                        $data['seo_meta'] .= '<title>' . $title . '</title>' . "\n";
                        $data['seo_meta'] .= '<meta name="title" content="' . $title . '" />' . "\n";
                        $data['seo_meta'] .= '<meta name="description" content="' . $description . '" />' . "\n";
                        $data['seo_meta'] .= '<meta name="keywords" content="' . $keywords . '" />' . "\n";
                    }
                } elseif ($this->request->get['route'] !== 'error/not_found') {
                    $data['seo_meta'] .= '<title>' . $title . '</title>' . "\n";
                    $data['seo_meta'] .= '<meta name="title" content="' . $title . '" />' . "\n";
                    $data['seo_meta'] .= '<meta name="description" content="' . $description . '" />' . "\n";
                    $data['seo_meta'] .= '<meta name="keywords" content="' . $keywords . '" />' . "\n";
                } else {
                    $data['seo_meta'] .= '<title>' . $title . '</title>' . "\n";
                }
            } else {
                $data['seo_meta'] .= '<title>' . $title . '</title>' . "\n";
            }

            $image = $this->getUrlWithCb($data['logo']);
            if (isset($this->request->get['route']) && $this->request->get['route'] !== 'error/not_found') {
                $data['seo_meta'] .= '<meta name="twitter:card" content="' . $description . '">' . "\n";
                $data['seo_meta'] .= '<meta name="twitter:description" content="' . $description . '">' . "\n";
                $data['seo_meta'] .= '<meta name="twitter:domain" content="' . $data['page'] . '">' . "\n";
                $data['seo_meta'] .= '<meta name="twitter:image:src" content="' . $image . '">' . "\n";
                $data['seo_meta'] .= '<meta name="twitter:site" content="' . $title . '">' . "\n";
                $data['seo_meta'] .= '<meta name="twitter:title" content="' . $title . '">' . "\n";
                $data['seo_meta'] .= '<meta name="twitter:url" content="' . $data['page'] . '">' . "\n";
                $data['seo_meta'] .= '<meta name="image" content="' . $image . '" />' . "\n";
                $data['seo_meta'] .= '<link rel="canonical" href="' . $data['page'] . '" />' . "\n";
                $data['seo_meta'] .= '<meta property="og:url" content="' . $data['page'] . '" />' . "\n";
                $data['seo_meta'] .= '<meta property="og:title" content="' . $title . '" />' . "\n";
                $data['seo_meta'] .= '<meta property="og:description" content="' . $description . '" />' . "\n";
                $data['seo_meta'] .= '<meta property="og:image" content="' . $image . '" />' . "\n";
                $data['seo_meta'] .= '<meta property="og:image:secure_url" content="' . $data['page'] . '" />' . "\n";
                $data['seo_meta'] .= '<meta property="og:image:width" content="450"/>' . "\n";
                $data['seo_meta'] .= '<meta property="og:image:height" content="298"/>' . "\n";
                $data['seo_meta'] .= '<meta property="og:site_name" content="' . $main_domain . '" />' . "\n";
                $data['seo_meta'] .= '<meta property="og:type" content="website"/>' . "\n";
                $data['seo_meta'] .= '<meta property="og:locale" content="vi_VN"/>' . "\n";
                $data['seo_meta'] .= '<link rel="image_src" href="' . $image . '" />' . "\n";
                $data['seo_meta'] .= '<link rel="alternate" href="' . $data['page'] . '" hreflang="vi-vn" />' . "\n";
            }
        }
//        else {
//            $data['seo_meta'] = ' <title>' . (!empty($data['title']) ? $data['title'] : $this->language->get('text_home')) . '</title>';
//        }

        if (isset($this->request->get['route']) && $this->request->get['route'] !== 'error/not_found') {
            $data['seo_meta'] .= '<meta name="copyright" content="' . COPYRIGHT . '" />';
            $data['seo_meta'] .= '<meta name="author" content="' . COPYRIGHT . '" />';
            $data['seo_meta'] .= '<meta name="GENERATOR" content="' . COPYRIGHT . '" />';
        }

        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

        $sections_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_SECTIONS);
        $sections_config = json_decode($sections_config, true);
        $sections_config = is_array($sections_config) ? $sections_config : [];
        $sections = array();

        if (isset($sections_config['section']) && is_array($sections_config['section'])) {
            foreach ($sections_config['section'] as $section) {
                if (!array_key_exists('name', $section) || !array_key_exists('visible', $section)) {
                    continue;
                }
                $sections[$section['name']] = $section['visible'];
            }
            unset($sections_config, $section);
        }

        if (isset($sections['content-customize']) && $sections['content-customize'] == '1') {
            // new, without migration
            $content_customizes = $this->getContentCustomize();
            $data = array_merge($data, $content_customizes);
            unset($content_customizes);
        }

        // filter cho một số them không có column left
        /** ---------------- */
        $data['visible_supplier'] = $data['filter']['visible_supplier']['visible'];
        $data['visible_supplier_title'] = $data['filter']['visible_supplier']['title'];
        $data['manufacturers'] = (isset($data['filter']['manufacturer']) ? $data['filter']['manufacturer'] : '');
        $data['manufacturer_all'] = (isset($data['filter']['manufacturer_all']) ? $data['filter']['manufacturer_all'] : '');
        $data['manufacturer_selected_name'] = (isset($data['filter']['manufacturer_selected_name']) ? $data['filter']['manufacturer_selected_name'] : $data['visible_supplier_title']);
        /** ---------------- */
        $data['visible_category'] = $data['filter']['visible_product_type']['visible'];
        $data['visible_category_title'] = $data['filter']['visible_product_type']['title'];
        // NOTICE: categories list for filter only (due to filter category setting in builder)
        $data['categorys'] = (isset($data['filter']['category']) ? $data['filter']['category'] : '');
        $data['categories'] = $data['categorys']; // maintain key to correct lang
        $data['category_selected'] = isset($this->request->get['path']) ? $this->request->get['path'] : '';
        $data['category_all'] = (isset($data['filter']['category_all']) ? $data['filter']['category_all'] : '');
        $data['category_selected_name'] = (isset($data['filter']['category_selected_name']) ? $data['filter']['category_selected_name'] : $data['visible_category_title']);
        /** ---------------- */
        $data['visible_collection'] = $data['filter']['visible_collection']['visible'];
        $data['visible_collection_title'] = $data['filter']['visible_collection']['title'];
        $data['collections'] = (isset($data['filter']['collection']) ? $data['filter']['collection'] : '');
        $data['collection_all'] = (isset($data['filter']['collection_all']) ? $data['filter']['collection_all'] : '');
        $data['collection_selected_name'] = (isset($data['filter']['collection_selected_name']) ? $data['filter']['collection_selected_name'] : $data['visible_collection_title']);
        /** ---------------- */
        $data['visible_tag'] = $data['filter']['visible_tag']['visible'];
        $data['tags'] = (isset($data['filter']['tag']) ? $data['filter']['tag'] : '');
        $data['urlserach'] = (isset($data['filter']['urlserach']) ? $data['filter']['urlserach'] : '');
        $data['tag_selected_name'] = (isset($data['filter']['tag_selected_name']) ? $data['filter']['tag_selected_name'] : 'TAGS');
        $data['tag_all'] = (isset($data['filter']['tag_all']) ? $data['filter']['tag_all'] : '');
        /** ---------------- */
        $data['visible_product_price'] = $data['filter']['visible_product_price']['visible'];
        $data['visible_product_price_from'] = $data['filter']['visible_product_price']['range']['from'];
        $data['visible_product_price_to'] = $data['filter']['visible_product_price']['range']['to'];

        $this->load->model('setting/setting');
        $data['google_shopping_meta'] = $this->model_setting_setting->getSettingValue(self::CONFIG_GG_SHOPPING);

        /* get categories new */
        $data['categories_new'] = $this->getCategories();

        /* get footer config */
        $footer_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER);
        $footer_config = json_decode($footer_config, true);
        // contact
        if(isset($footer_config['contact']['visible']) && $footer_config['contact']['visible'] == 1){
            $contact_addresses = explode('&lt;li&gt;', $footer_config['contact']['address']);
            $data['contact'] = array(
                'title'  => $footer_config['contact']['title'],
                'address'  => $contact_addresses[0],
                'phone'  => $footer_config['contact']['phone-number'],
                'email'  => $footer_config['contact']['email'],
                'visible'  => $footer_config['contact']['visible']
            );
        }
        // social
        $data['visible_subscribe_social_network'] = $footer_config['subscribe']['social_network'];
        $data['social_configs'] = [];
        if (isset($data['visible_subscribe_social_network']) && $data['visible_subscribe_social_network'] == 1) {
            $social_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_SOCIAL);
            $social_display_in_footer = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER);
            $data['social_config'] = json_decode($social_config, true);
            $data['social_display'] = json_decode($social_display_in_footer, true);
            $data['social_display'] = isset($data['social_display']['subscribe']) ? $data['social_display']['subscribe'] : [];

            if (isset($data['social_config']) && is_array($data['social_config']) && !empty($data['social_config'])) {
                foreach ($data['social_config'] as $social) {
                    $social_name_in_footer = $social['name'] . '_visible';
                    // important: check !isset for $social_name_in_footer because social visible may be not existing
                    // this for working without migration old social config in footer
                    if (!isset($data['social_display'][$social_name_in_footer]) || $data['social_display'][$social_name_in_footer] == 1) {
                        $data['social_configs'][] = array(
                            'name' => (!empty($social['name']) ? $social['text'] : ''),
                            'url' => (!empty($social['name']) ? $social['url'] : ''),
                        );
                        if($social['name'] == "facebook") {
                            $data['url_facebook'] = $social['url'];
                        }
                    }
                }
            }
        }

        // banner
        if (isset($sections['banner']) && $sections['banner'] == '1') {
            $result = $this->getBanner(0);
            $data = array_merge($data, $result);
            unset($result);
        }

        /* contact url */
        $data['order_now_href'] = $data['is_home'] ? '#order-now-anchor' : $this->url->link('contact/contact', '', true);

        // placeholder image
        $this->session->data['placeholder_image_url'] = $data['placeholder_image_url'] = self::PLACEHOLDER_IMAGE_URL;
        $data['HTTPS_SERVER'] = HTTPS_SERVER;

        // set meta_robots_content for not production environment
        if ('production' != ENVIRONMENT_VALUE) {
            $data['meta_robots_content'] = 'noindex, nofollow';
        }

        // set meta_robots_content for not found page
        if (!empty($this->request->get['route']) && $this->request->get['route'] == 'error/not_found') {
            $data['meta_robots_content'] = 'noindex, nofollow';
        }

        if (isset($this->request->get['search'])) {
            $data['meta_robots_content'] = 'noindex, nofollow';
        }
        // if ((isset($this->request->get['route']) && $this->request->get['route'] == 'product/product') && $this->isMobile()) {
        //     return $this->load->view('common/header_mobile_custom', $data);
        // }
        // else {
            return $this->load->view('common/header', $data);
        //}

    }

    private function getBanner($key)
    {
        $data = [];
        $banners = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_BANNER);
        $banners = json_decode($banners, true);
        if (isset($banners['display']) && is_array($banners['display'])) {
            // more banners
            $data['banner_header'] = isset($banners['display'][$key]) ? $banners['display'][$key] : null;
        }

        $data['banners_visible'] = true;

        return $data;
    }

    private function getCategories()
    {
        $this->load->model('catalog/category');
        $categories = $this->model_catalog_category->getCategoriesHadProduct();
        $result = [];

        foreach ($categories as $category) {
            $result[] = array(
                'category_id' => $category['category_id'],
                'name' => $category['name'],
                'image' => $category['image'],
                'id' => $category['category_id'],
                'parent_id' => $category['parent_id'],
                'url' => str_replace(' ', '', $this->url->link('common/shop', '&path=' . $category['category_id'], true)),
            );
        }

        return $result;
    }

    private function getContentCustomize($visible = 1)
    {
        $data = [];
        $content_customize_configs = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CONTENT_CUSTOMIZE);
        $content_customize_config = json_decode($content_customize_configs, true);

        $data['display_customizes'] = isset($content_customize_config['display']) ? $content_customize_config['display'] : [];

        foreach ($data['display_customizes'] as &$customize) {
            if (!$customize['content']['icon']) {
                $customize['content']['icon'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
            }
        }
        unset($customize);

        return $data;
    }

//    private function getCategories()
//    {
//        $this->load->model('catalog/category');
//        $categories = $this->model_catalog_category->getCategories();
//        $result = [];
//
//        foreach ($categories as $category) {
//            $result[] = array(
//                'category_id' => $category['category_id'],
//                'name' => $category['name'],
//                'url' => str_replace(' ', '', $this->url->link('common/shop', '&path=' . $category['category_id'], true)),
//            );
//        }
//
//        return $result;
//    }

    public function getSearchKeyup()
    {
        $this->load->model('catalog/product');
        $filter_data['filter_name'] = $this->request->post['search'];
        $filter_data['start'] = 0;
        $filter_data['limit'] = 8;
        if (!empty($filter_data['filter_name'])) {
            $products = $this->model_catalog_product->getProductsCustom($filter_data);
            if ($products) {
                $html = '<ul>';
                foreach ($products as $product) {
                    $url_product = $this->url->link('product/product', 'product_id=' . $product['product_id']);
                    $html .= "<li onclick=\"fill_search('" . $product['name'] . "')\"><a href='" . $url_product . "'>" . $product['name'] . " - " . $product['sku'] . "</a></li>";
                }
                $html .= '</ul>';
                echo $html;
            }
        }
    }

    private function countWebTraffic()
    {
        $this->load->model('setting/setting');
        $shop_name = $this->model_setting_setting->getSettingValue('shop_name');
        $md5_shop_name = md5($shop_name . '_web_traffic');

        if (!isset($_COOKIE[$md5_shop_name])) {
            $this->doUpdateWebTrafficReport();
            $ttl = defined('WEB_TRAFFIC_COOKIE_TTL') ? (int)WEB_TRAFFIC_COOKIE_TTL : 1800;
            setcookie($md5_shop_name, 1, time() + $ttl);  /* expire in 30 minute */
        }
    }

    private function doUpdateWebTrafficReport()
    {
        return $this->pushToRedis();
    }


    private function pushToRedis()
    {
        $task = BESTME_WEB_TRAFFICT_REDIS;
        $key = DB_PREFIX.'traffic';
        //check exist redis traffic
        $redis = $this->redisConnect();
        $check_traffict = $redis->hGet($task, $key);
        if($check_traffict) {
            $data = json_decode($check_traffict);
            $data->count += 1;
            $data_to_save = $data;
            $redis->hset($task, $key, json_encode($data_to_save));
        }else{
            $this->load->model('setting/setting');
            $shop_name = $this->model_setting_setting->getSettingValue('shop_name');
            /* create job */
            $job = [
                'db_hostname' => DB_HOSTNAME,
                'db_port' => DB_PORT,
                'db_username' => DB_USERNAME,
                'db_password' => DB_PASSWORD,
                'db_database' => DB_DATABASE,
                'db_prefix' => DB_PREFIX,
                'db_driver' => DB_DRIVER,
                'shop_name' => $shop_name,
                'count' => 1,
            ];
            $redis->hset($task, $key, json_encode($job));
        }
        $this->redisClose($redis);
    }
    /**
     * Rebuild Css
     * @param $override_css_version
     * @param bool $preview
     * @return bool
     */
    private function rebuildCss($override_css_version, $preview = false)
    {
        $result = true;

        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

        /* apply to css */
        // theme color
        $color = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_COLOR);
        $color = json_decode($color, true);
        if (is_array($color)) {
            $result &= $this->applyThemeColorConfig($color, $preview, $override_css_version);
        }

        // theme text
        $text = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_TEXT);
        $text = json_decode($text, true);
        if (is_array($text)) {
            $result &= $this->applyThemeTextConfig($text, $preview, $override_css_version);
        }

        // section slide_show
        $slide_show = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_SLIDESHOW);
        $slide_show = json_decode($slide_show, true);
        if (is_array($slide_show)) {
            $result &= $this->applySectionSlideShowConfig($slide_show, $preview, $override_css_version);
        }

        // override css
        $override_css = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_OVERRIDE_CSS);
        $override_css = json_decode($override_css, true);
        $override_css = isset($override_css['css']) ? $override_css['css'] : '';
        $result &= $this->applyThemeOverrideCssConfig($override_css, $preview, $override_css_version);

        return $result;
    }

    private function applyThemeColorConfig(array $color, $preview, $override_css_version)
    {
        // get template for css
        $css_template_file = DIR_APPLICATION . 'view/theme/' . $this->getCurrentThemeDir() . '/asset/sass/override/color.css';
        $css_file_content = file_get_contents($css_template_file);

        $this->load->library('theme_config/theme_config');

        /** @var \theme_config\Theme_Config $themeConfig */
        $themeConfig = $this->theme_config;

        $themeConfig->applyThemeColorConfig($color, $css_file_content);

        // write to css

        if ($preview) {
            $css_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/' . $override_css_version . '/';
            $css_dir_parrent = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/';
        } else {
            $css_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/' . $override_css_version . '/';
            $css_dir_parrent = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/';
        }
        try {
            $this->removeOldOverrideDir($css_dir_parrent, $override_css_version);
            if (!is_dir($css_dir)) {
                mkdir($css_dir, 0755, true);
            }
            $css_file = $css_dir . 'color.css';
            //$this->log->write('$css_file_content: ' . $css_file_content);
            file_put_contents($css_file, $css_file_content);
        } catch (Exception $e) {
            $this->log->write('applyThemeColorConfig got error: ' . $e->getMessage());
        }

        return true;
    }

    private function applyThemeOverrideCssConfig($override_css, $preview, $override_css_version)
    {
        $this->load->library('theme_config/theme_config');

        if ($preview) {
            $css_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/' . $override_css_version . '/';
            $css_dir_parrent = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/';
        } else {
            $css_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/' . $override_css_version . '/';
            $css_dir_parrent = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/';
        }
        try {
            $this->removeOldOverrideDir($css_dir_parrent, $override_css_version);
            if (!is_dir($css_dir)) {
                mkdir($css_dir, 0755, true);
            }
            $css_file = $css_dir . 'custom_css.css';
            //$this->log->write('$css_file_content: ' . $css_file_content);
            file_put_contents($css_file, $override_css);
        } catch (Exception $e) {
            $this->log->write('applyThemeColorConfig got error: ' . $e->getMessage());
        }

        return true;
    }

    private function applyThemeTextConfig(array $text, $preview, $override_css_version)
    {
        // get template for css
        $css_template_file = DIR_APPLICATION . 'view/theme/' . $this->getCurrentThemeDir() . '/asset/sass/override/text.css';
        $css_file_content = file_get_contents($css_template_file);

        $this->load->library('theme_config/theme_config');

        /** @var \theme_config\Theme_Config $themeConfig */
        $themeConfig = $this->theme_config;

        $themeConfig->applyThemeTextConfig($text, $css_file_content);

        // write to css

        if ($preview) {
            $css_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/' . $override_css_version . '/';
            $css_dir_parrent = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/';
        } else {
            $css_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/' . $override_css_version . '/';
            $css_dir_parrent = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/';
        }
        try {
            $this->removeOldOverrideDir($css_dir_parrent, $override_css_version);
            if (!is_dir($css_dir)) {
                mkdir($css_dir, 0755, true);
            }
            $css_file = $css_dir . 'text.css';
            //$this->log->write('$css_file_content: ' . $css_file_content);
            file_put_contents($css_file, $css_file_content);
        } catch (Exception $e) {
            $this->log->write('applyThemeTextConfig got error: ' . $e->getMessage());
        }

        return true;
    }

    private function applySectionSlideShowConfig(array $slide_show, $preview, $override_css_version)
    {
        // get template for css
        $css_template_file = DIR_APPLICATION . 'view/theme/' . $this->getCurrentThemeDir() . '/asset/sass/override/slideshow.css';
        $css_file_content = file_get_contents($css_template_file);

        $this->load->library('theme_config/theme_config');

        /** @var \theme_config\Theme_Config $themeConfig */
        $themeConfig = $this->theme_config;

        $themeConfig->applySectionSlideshowConfig($slide_show, $css_file_content);

        // write to css

        if ($preview) {
            $css_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/' . $override_css_version . '/';
            $css_dir_parrent = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/';
        } else {
            $css_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/' . $override_css_version . '/';
            $css_dir_parrent = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/';
        }
        try {
            $this->removeOldOverrideDir($css_dir_parrent, $override_css_version);
            if (!is_dir($css_dir)) {
                mkdir($css_dir, 0755, true);
            }
            $css_file = $css_dir . 'slideshow.css';
            //$this->log->write('$css_file_content: ' . $css_file_content);
            file_put_contents($css_file, $css_file_content);
        } catch (Exception $e) {
            $this->log->write('applySectionSlideShowConfig got error: ' . $e->getMessage());
        }

        return true;
    }

    private function removeOldOverrideDir($dir, $current_dir = null)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") {
                        if ($object != $current_dir || $current_dir == null) {
                            $this->removeOldOverrideDir($dir . "/" . $object);
                            rmdir($dir . "/" . $object);
                        }
                    } else {
                        unlink($dir . "/" . $object);
                    }
                }
            }
            reset($objects);
        }
    }

    private function loadResourcesRaw()
    {
        $redis = $this->redis_connect();
        $key = REDIS_THEME_RESOURCE_KEY . $this->getCurrentThemeDir();
        $key_version = REDIS_THEME_RESOURCE_KEY . $this->getCurrentThemeDir() . '_version';
        $theme_directory = DIR_APPLICATION . 'view/theme/' . $this->getCurrentThemeDir() . '/';
        $cssContent = "";
        $jsContent = "";
        $result = [];

        if (is_file($theme_directory . "/asset/" . self::ASSET_RESOURCES_JSON_FILE_NAME)) {
            // TODO: do not load file immediately, load redis first...
            $source = file_get_contents($theme_directory . "asset/" . self::ASSET_RESOURCES_JSON_FILE_NAME);
            $source = json_decode($source, true);
            $version = is_array($source) && array_key_exists('version', $source) ? $source['version'] : '1';

            // load from redis if existed
            // for dev test only, TODO: remove "false &&"
            if (false && $redis->exists($key) && $redis->exists($key_version) && $redis->get($key_version) == $version) {
                $result = $redis->get($key);
                $result = json_decode($result, true);
            } else {
                // or load from resource.json file if exist
                if (array_key_exists('css', $source) && is_array($source['css'])) {
                    foreach ($source['css'] as $item) {
                        if (strpos($item, 'https://') === 0 || strpos($item, 'http://') === 0) {
                            $itemCssContent = file_get_contents($item);
                        } else {
                            $itemCssContent = file_get_contents($theme_directory . $item);
                        }

                        if (!empty($itemCssContent)) {
                            $cssContent .= "<style>" . $itemCssContent . "</style>";
                        }
                    }
                }

                if (array_key_exists('js', $source) && is_array($source['js'])) {
                    foreach ($source['js'] as $item) {
                        if (strpos($item, 'https://') === 0 || strpos($item, 'http://') === 0) {
                            $itemJsContent = file_get_contents($item);
                        } else {
                            $itemJsContent = file_get_contents($theme_directory . $item);
                        }
                        if (!empty($itemJsContent)) {
                            $jsContent .= "<script>" . $itemJsContent . "</script>";
                        }
                    }
                }

                $result = array(
                    'css' => empty($cssContent) ? "" : $cssContent,
                    'js' => empty($jsContent) ? "" : $jsContent
                );
            }
        }

        return $result;
    }

    private function redis_connect()
    {
        // for dev test only. TODO: remove...
        return;

        $redis = new \Redis();
        $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT);
        $redis->select(MUL_REDIS_DB);

        //echo "Connection to server sucessfully";
        //// check whether server is running or not
        //echo "Server is running: ".$redis->ping();

        return $redis;
    }

    /**
     * get Url With Cache buster to avoid caching
     *
     * @param $url
     * @return string
     */
    private function getUrlWithCb($url)
    {
        $query = parse_url($url, PHP_URL_QUERY);

        // Returns a string if the URL has parameters or NULL if not
        $cb = time();
        return ($query) ? "{$url}&cb={$cb}" : "{$url}?cb={$cb}";
    }

    private function generateCategories() {
        $this->load->model('catalog/category');
        $categories = $this->model_catalog_category->getCategoriesHadProduct();
        $category_level_1 = [];
        $category_level_1_temp = [];

        foreach ($categories as $key => $category) {
            if($category['parent_id'] == 0) {
                $row = array(
                    'category_id' => $category['category_id'],
                    'name' => $category['name'],
                    'image' => $category['image'],
                    'id' => $category['category_id'],
                    'parent_id' => $category['parent_id'],
                    'url' => str_replace(' ', '', $this->url->link('common/shop', '&path=' . $category['category_id'], true)),
                    'list_sub' => []
                );
                $category_level_1_temp[] = $category['category_id'];
                $category_level_1[] = $row;
                unset($categories[$key]);
            }
        }
        $category_level_2 = [];
        $category_level_2_temp = [];

        foreach ($categories as $key => $category) {
            $search = array_search($category['parent_id'], $category_level_1_temp);
            if($search !== false) {
                $row = array(
                    'parent_key' => $search,
                    'category_id' => $category['category_id'],
                    'name' => $category['name'],
                    'image' => $category['image'],
                    'id' => $category['category_id'],
                    'parent_id' => $category['parent_id'],
                    'url' => str_replace(' ', '', $this->url->link('common/shop', '&path=' . $category['category_id'], true)),
                    'list_sub' => []
                );
                $category_level_2[] = $row;
                $category_level_2_temp[] = $category['category_id'];
                unset($categories[$key]);
            }
        }

        foreach ($categories as $key => $category) {
            $search = array_search($category['parent_id'], $category_level_2_temp);
            if($search !== false) {
                $row = array(
                    'category_id' => $category['category_id'],
                    'name' => $category['name'],
                    'image' => $category['image'],
                    'id' => $category['category_id'],
                    'parent_id' => $category['parent_id'],
                    'url' => str_replace(' ', '', $this->url->link('common/shop', '&path=' . $category['category_id'], true)),
                    'list_sub' => []
                );
                $category_level_2[$search]['list_sub'][] = $row;
                unset($categories[$key]);
            }
        }

        foreach ($category_level_2 as $category) {
            $category_level_1[$category['parent_key']]['list_sub'][] = $category;
        }
        return $category_level_1;
    }

    private function getCategoriesMulti()
    {
        $this->load->model('catalog/category');
        $categories = $this->model_catalog_category->getCategoriesHadProduct();
        $result = [];

        foreach ($categories as $category) {
            $result[] = array(
                'category_id' => $category['category_id'],
                'name' => $category['name'],
                'image' => $category['image'],
                'id' => $category['category_id'],
                'parent_id' => $category['parent_id'],
                'url' => str_replace(' ', '', $this->url->link('common/shop', '&path=' . $category['category_id'], true)),
            );
        }

        return $result;
    }

    private function checkCustomerStatus(){
        if ($this->customer->isLogged()) {
            $this->load->model('customer/customer');
            $customer_info = $this->model_customer_customer->getCustomerByEmail($this->customer->getEmail());
            if (!isset($customer_info['status']) || $customer_info['status'] == 0) {
                $this->customer->logout();

                unset($this->session->data['shipping_address']);
                unset($this->session->data['shipping_method']);
                unset($this->session->data['shipping_methods']);
                unset($this->session->data['payment_address']);
                unset($this->session->data['payment_method']);
                unset($this->session->data['payment_methods']);
                unset($this->session->data['comment']);
                unset($this->session->data['order_id']);
                unset($this->session->data['coupon']);
                unset($this->session->data['reward']);
                unset($this->session->data['voucher']);
                unset($this->session->data['vouchers']);
            }
        }
    }

    /**
     * @return null|Redis
     */
    private function redisConnect()
    {
        $redis = null;

        try {
            $redis = new \Redis();
            $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT);
            $redis->select(MUL_REDIS_DB_BESTME_COUNT_TRAFFICT);
        } catch (Exception $e) {
            // could not connect to redis
        }

        return $redis;
    }

    /**
     * @param Redis $redis
     */
    private function redisClose($redis)
    {
        if (!$redis instanceof \Redis) {
            return;
        }

        try {
            $redis->close();
        } catch (Exception $e) {
            // could not connect to redis
        }
    }

    private function getSeoRobotsContent($seo_data) {
        $result = [];

        if ($seo_data['noindex']) {
            $result[] = 'noindex';
            $result[] = 'nofollow';
        } else {
            $result[] = 'index';
            $result[] = $seo_data['nofollow'] ? 'nofollow' : 'follow';
            if ($seo_data['noarchive']) {
                $result[] = 'noarchive';
            }
            if ($seo_data['noimageindex']) {
                $result[] = 'noimageindex';
            }
            if ($seo_data['nosnippet']) {
                $result[] = 'nosnippet';
            }
        }

        return implode(', ', $result);
    }
}
