<?php

class ControllerCommonFooter extends Controller
{

    use Theme_Config_Util;
    use Device_Util;

    const CONFIG_KEY_SALE_NOTI_MOIT_STATUS = 'sale_noti_moit_status';
    const CONFIG_KEY_SALE_NOTI_MOIT_REG_NUMBER = 'sale_noti_moit_reg_number';
    const CONFIG_KEY_SHOP_NAME = 'config_name';

    const SALE_NOTI_MOIT_REG_NUMBER_BESTME = '68803';

    const PLACEHOLDER_IMAGE_URL = '/catalog/view/theme/default/images/placeholder.png';

    public function index()
    {
        $this->load->language('common/footer');
        $this->load->model('tool/image');
        $this->load->model('catalog/information');
        $this->load->model('catalog/collection');
        $this->load->model('extension/module/theme_builder_config');
        $this->load->model('custom/common');
        $this->load->model('setting/setting');

        $data = [];

        $data['homepage_description'] = $this->config->get('config_home_des');
        $data['is_on_mobile'] = $this->isMobile() ? 1 : 0;

        /* Logo Sale Notification - Ministry of Industry and Trade */
        $sale_noti_moit_status = $this->model_setting_setting->getSettingValue(self::CONFIG_KEY_SALE_NOTI_MOIT_STATUS);
        $data['sale_noti_moit_status'] = $sale_noti_moit_status == 1 ? 1 : 0;

        $sale_noti_moit_reg_number = $this->model_setting_setting->getSettingValue(self::CONFIG_KEY_SALE_NOTI_MOIT_REG_NUMBER);
        $data['sale_noti_moit_reg_number'] = empty($sale_noti_moit_reg_number) ? self::SALE_NOTI_MOIT_REG_NUMBER_BESTME : $sale_noti_moit_reg_number;

        $shopname = $this->model_setting_setting->getSettingValue(self::CONFIG_KEY_SHOP_NAME);
        $data['shop_name'] = empty($shopname) ? self::CONFIG_KEY_SHOP_NAME : $shopname;

        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

        /* get header config */
        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        $header_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER);
        $header_config = json_decode($header_config, true);

        /* display logo */
        $data['logo'] = isset($header_config['logo']['url']) ? cloudinary_change_http_to_https($header_config['logo']['url']) : 'image/no_image.png';

        $data['theme_directory'] = 'catalog/view/theme/' . $this->getCurrentThemeDir();

        /* home url */
        $data['home'] = $this->url->link('common/home');

        /* get footer config */
        $footer_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER);
        $footer_config = json_decode($footer_config, true);
        // support contact_more
        if (!isset($footer_config['contact_more'])) {
            $footer_config['contact_more'] = [];
        }

        // contact
        $data['visible_contact'] = $footer_config['contact']['visible'];
        $data['visible_contact_title'] = $footer_config['contact']['title'];

        // DMCA
        $data['DMCA_URL'] = DMCA_URL;
        $data['DMCA_SRC'] = DMCA_SRC;

        // contact more
        $data['contact_more'] = $footer_config['contact_more'];
        $data['text_more_visible'] = false;
        foreach ($footer_config['contact_more'] as $value){
            if ($value['visible'] == 1){
                $data['text_more_visible'] = true;
                break;
            }
        }
        /* contact map */
        $contact_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CONTACT_SECTIONS);  //       CONFIG_KEY_SECTION_CONTACT_SECTIONS
        $contact_config = json_decode($contact_config, true);
        $key_map = 'map';
        $data['contacts_map'] = [];
        if ($contact_config['section'][$key_map]['visible'] == 1) {
            $data['contacts_map'] = array(
                'address' => (!empty($contact_config['section'][$key_map]['address']) ? $contact_config['section'][$key_map]['address'] : ''),
                'visible' => 1,
            );
        }
        // collection
        $data['visible_collection'] = $footer_config['collection']['visible'];
        $data['visible_collection_title'] = $footer_config['collection']['title'];

        // quick links
        $data['visible_quick_links'] = $footer_config['quick-links']['visible'];
        $data['visible_quick_links_title'] = $footer_config['quick-links']['title'];

        // subscribe
        $data['visible_subscribe'] = $footer_config['subscribe']['visible'];
        $data['visible_subscribe_social_network'] = $footer_config['subscribe']['social_network'];
        $data['visible_subscribe_title'] = $footer_config['subscribe']['title'];
        $data['visible_social_network'] = $footer_config['subscribe']['social_network'];
        $data['visible_subscribe_title'] = $footer_config['subscribe']['title'];
        if (isset($data['visible_contact']) && $data['visible_contact'] == 1) {
            $data['contact'] = array(
                'title' => $footer_config['contact']['title'],
                'address' => $footer_config['contact']['address'],
                'phone' => $footer_config['contact']['phone-number'],
                'email' => $footer_config['contact']['email'],
                'visible' => $footer_config['contact']['visible'],
            );
        } else {
            $data['contact'] = '';
        }
        $this->load->model('custom/menu');
        if (isset($data['visible_collection']) && $data['visible_collection'] == 1) {
            $childMenu = isset($footer_config['collection']['menu_id']) ? $this->model_custom_menu->getMenuById($footer_config['collection']['menu_id']) : [];

            foreach ($childMenu as $child_menu) {
                $data['collections'][] = array(
                    'name' => $child_menu['text'],
                    'url_link' => $child_menu['url'],
                );
            }
        } else {
            $data['collections'] = '';
        }

        if (isset($data['visible_quick_links']) && $data['visible_quick_links'] == 1) {
            $childMenu = $this->model_custom_menu->getMenuById($footer_config['quick-links']['menu_id']);
            foreach ($childMenu as $child_menu) {
                $data['quick_links'][] = array(
                    'name' => $child_menu['text'],
                    'url_link' => $child_menu['url'],
                );
            }
        } else {
            $data['quick_links'] = '';
        }

        // check product_detail theme fashion_m_boutique
        $getUrl_product_id = ((isset($this->request->get['product_id'])) ? $this->request->get['product_id'] : '');
        if (isset($getUrl_product_id) && $getUrl_product_id > 0) {
            $data['action'] = $this->url->link('checkout/my_orders', '', true);
            $this->load->model('catalog/product');
            $product = $this->model_catalog_product->getProduct($getUrl_product_id);
            $data['product_detail'] = $product;
        }
        /*
                if(isset($data['visible_subscribe']) && $data['visible_subscribe'] == 1){
                    $social_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_SOCIAL);
                    $data['social_config'] = json_decode($social_config, true);
                    foreach ($data['social_config'] as $social) {
                        $data['social_configs'][] = array(
                            'name'  => (!empty($social['name']) ? $social['text'] : ''),
                            'url'  => (!empty($social['name']) ? $social['url'] : ''),
                        );
                    }
                }
                else{
                    $data['social_config'] = '';
                }
        */
        if (isset($data['visible_subscribe_social_network']) && $data['visible_subscribe_social_network'] == 1) {
            $social_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_SOCIAL);
            $social_display_in_footer = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER);
            $data['social_config'] = json_decode($social_config, true);
            $data['social_display'] = json_decode($social_display_in_footer, true);
            $data['social_display'] = isset($data['social_display']['subscribe']) ? $data['social_display']['subscribe'] : [];

            if (isset($data['social_config']) && is_array($data['social_config']) && !empty($data['social_config'])) {
                foreach ($data['social_config'] as $social) {
                    $social_name_in_footer = $social['name'] . '_visible';
                    // important: check !isset for $social_name_in_footer because social visible may be not existing
                    // this for working without migration old social config in footer
                    if (!isset($data['social_display'][$social_name_in_footer]) || $data['social_display'][$social_name_in_footer] == 1) {
                        $data['social_configs'][] = array(
                            'name' => (!empty($social['name']) ? $social['text'] : ''),
                            'url' => (!empty($social['name']) ? $social['url'] : ''),
                        );
                        if($social['name'] == "facebook") {
                            $data['url_facebook'] = $social['url'];
                        }
                    }
                }
            } else {
                $data['social_config'] = '';
            }

            $ecommerce_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_ECOMMERCE);
            $data['ecommerce_config'] = json_decode($ecommerce_config, true);
            if (isset($data['ecommerce_config']) && is_array($data['ecommerce_config']) && !empty($data['ecommerce_config'])) {
                foreach ($data['ecommerce_config'] as $ecommerce) {
                    $social_name_in_footer = $ecommerce['name'] . '_visible';
                    // important: check !isset for $social_name_in_footer because social visible may be not existing
                    // this for working without migration old social config in footer
                    if (!isset($data['social_display'][$social_name_in_footer]) || $data['social_display'][$social_name_in_footer] == 1) {
                        $data['ecommerce_configs'][] = array(
                            'name' => (!empty($ecommerce['name']) ? $ecommerce['text'] : ''),
                            'url' => (!empty($ecommerce['name']) ? $ecommerce['url'] : ''),
                        );
                    }
                }
            } else {
                $data['social_config'] = '';
            }

        } else {
            $data['social_config'] = '';
        }

        $data['href_subscribe'] = $this->url->link('common/footer/subscribe', '', true);
        $data['href_subscribe_v2'] = $this->url->link('common/footer/subscribe_v2', '', true);

        $data['copy_right_link'] = BESTME_SERVER . '?utm_source=bestme&utm_medium=cpc_click&utm_campaign=bestme';

        $data['href_get_province'] = $this->url->link('tool/vietnam_administrative/getProvinces', '', true);
        $data['href_get_district'] = $this->url->link('tool/vietnam_administrative/getDistrictsByProvinceCode', '', true);
        $data['href_get_ward'] = $this->url->link('tool/vietnam_administrative/getWardsByDistrictCode', '', true);

        // placeholder image
        $this->session->data['placeholder_image_url'] = $data['placeholder_image_url'] = self::PLACEHOLDER_IMAGE_URL;

        return $this->load->view('common/footer', $data);
    }

    /**
     * @return array
     */
    public function getFilterProduct()
    {
        $this->load->model('extension/module/theme_builder_config');
        $this->load->model('catalog/manufacturer');
        $this->load->model('catalog/category');
        $this->load->model('catalog/collection');

        $result = array();
        $related_filter_product_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_FILTER);
        $related_filter_product_config = json_decode($related_filter_product_config, true);
        $result['visible_supplier'] = $related_filter_product_config['display']['supplier']; // nha cung cap
        $result['visible_product_type'] = $related_filter_product_config['display']['product-type']; // loai san pham
        $result['visible_collection'] = $related_filter_product_config['display']['collection'];  // bo suu tap

        $url = '';
        if (isset($result['visible_supplier']['visible']) && $result['visible_supplier']['visible'] == 1) {
            $related_manufacturer = $this->model_catalog_manufacturer->getManufacturers();
            foreach ($related_manufacturer as $manufacturer) {
                $result['manufacturer'][] = array(
                    'manufacturer_id' => $manufacturer['manufacturer_id'],
                    'name' => $manufacturer['name'],
                    'url' => str_replace(' ', '', $this->url->link('common/shop', modQuery($url, 'manufacture') . '&manufacture=' . $manufacturer['manufacturer_id'], true)),
                );
            }
        }

        if (isset($result['visible_product_type']['visible']) && $result['visible_product_type']['visible'] == 1) {
            $related_category = $this->model_catalog_category->getCategories();
            foreach ($related_category as $category) {
                $result['category'][] = array(
                    'category_id' => $category['category_id'],
                    'name' => $category['name'],
                    'url' => str_replace(' ', '', $this->url->link('common/shop', modQuery($url, 'path') . '&path=' . $category['category_id'], true)),
                );
            }
        }

        if (isset($result['visible_collection']['visible']) && $result['visible_collection']['visible'] == 1) {
            $related_collection = $this->model_catalog_collection->getAllcollection();
            foreach ($related_collection as $collection) {
                $result['collection'][] = array(
                    'collection_id' => $collection['collection_id'],
                    'name' => $collection['title'],
                    'url' => str_replace(' ', '', $this->url->link('common/shop', modQuery($url, 'collection') . '&collection=' . $collection['collection_id'], true)),
                );
            }
        }

        return $result;
    }

    /**
     * AJAX: subscribe
     */
    public function subscribe()
    {
        // temp implement subscribe by sending email.
        // TODO: build in admin or use subscribe app...
        $result = $this->subscribeBySendingEmail();

        $json = [
            'error_code' => $result === true ? 0 : 1,
            'message' => $result === true ? $this->language->get('subscribe_email_sent_ok') : $result,
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
    public function subscribe_v2()
    {
        $this->load->language('common/footer');

        if ($this->request->server['REQUEST_METHOD'] != 'POST') {
            return 'Support POST only';
        }

        $customer_email = getValueByKey($this->request->post, 'email');
        $this->load->model('customer/customer');
        if (!$this->model_customer_customer->checkEmailSubcribersExist($customer_email)) {
            $result = $this->model_customer_customer->addEmailSubscribers($customer_email);
            $this->subscribeBySendingEmail();
            $json = [
                'error_code' => $result === true ? 0 : 1,
                'message' => $result === true ? $this->language->get('subscribe_email_sent_ok') : $result,
            ];
        }
        else {
            $json = [
                'error_code' => 0,
                'message' => $this->language->get('subscribe_email_sent_exits')
            ];

        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * @return bool|string true if success, or message string if failed
     */
    public function subscribeBySendingEmail()
    {
        $this->load->language('common/footer');

        if ($this->request->server['REQUEST_METHOD'] != 'POST') {
            return 'Support POST only';
        }

        $customer_email = getValueByKey($this->request->post, 'email');
        if (empty($customer_email) || !filter_var($customer_email, FILTER_VALIDATE_EMAIL)) {
            return $this->language->get('subscribe_email_invalid');
        }

        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
        $data = [];
        $contact_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CONTACT_SECTIONS);  //       CONFIG_KEY_SECTION_CONTACT_SECTIONS
        $contact_config = json_decode($contact_config, true);
        $key_form = 'form';
        $data['contact_config'] = $contact_config['section'][$key_form];

        $data['contacts_email'] = [
            'title' => '',
            'address' => '',
        ];

        if ($contact_config['section'][$key_form]['visible'] == 1) {
            $data['contacts_email'] = array(
                'title' => (!empty($contact_config['section'][$key_form]['title']) ? $contact_config['section'][$key_form]['title'] : ''),
                'address' => (!empty($contact_config['section'][$key_form]['email']) ? $contact_config['section'][$key_form]['email'] : ''),
            );
        }

        $mail = new Mail($this->config->get('config_mail_engine'));
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = MAIL_SMTP_HOST_NAME;
        $mail->smtp_username = MAIL_SMTP_USERNAME;
        $mail->smtp_password = html_entity_decode(MAIL_SMTP_PASSWORD, ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = MAIL_SMTP_PORT;
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($data['contacts_email']['address']);

        $mail->setFrom($this->config->get('config_mail_smtp_username'));

        $sender = html_entity_decode($this->config->get('config_mail_smtp_username'), ENT_QUOTES, 'UTF-8');
        $mail->setSender($sender);

        $subject = sprintf($this->language->get('subscribe_email_subject'), $sender);
        $mail->setSubject($subject);

        $body_text = sprintf($this->language->get('subscribe_email_body_text'), $customer_email);
        $mail->setText($body_text);

        try {
            $this->log->write("footer sends subscribe mail: processing... ($body_text)");
            $mail->send();
        } catch (Exception $e) {
            $this->log->write('footer sends subscribe mail got error: ' . $e->getMessage());

            return $this->language->get('subscribe_email_sent_failed');
        }

        return true;
    }
}
