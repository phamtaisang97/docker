<?php
// product title
$_['product_detail_title']                  = 'Sản phẩm';
$_['text_error_not_found']                  = 'Lỗi: Sản phẩm không tồn tại!';

// product detail
$_['text_product_detail_title']             = 'Chi tiết sản phẩm';
$_['text_product_type']                     = 'Loại: ';
$_['text_product_branch']                   = 'Thương hiệu: ';
$_['text_product_sku']                      = 'SKU: ';
$_['text_product_rating']                   = 'Đánh giá: ';
$_['text_product_status']                   = 'Tình trạng: ';
$_['text_vat_hint']                         = '(Chưa bao gồm VAT)';
$_['text_quantity']                         = 'Số lượng: ';
$_['text_button_add_to_cart']               = 'Thêm vào giỏ';

// related products
$_['text_related_products']                 = 'Sản phẩm liên quan';
$_['text_product_viewed_title']             = 'Sản phẩm đã xem';
// hot products
$_['text_hot_products']                     = 'Sản phẩm khuyến mại';

$_['text_home']              = 'Trang chủ';

$_['contact']                              = 'Liên hệ';
$_['not_existed_product_version']                              = 'Không tồn tại version sản phẩm';