<?php
// Locale
$_['code']                  = 'en';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['datetime_format']       = 'd/m/Y H:i:s';
$_['decimal_point']         = ',';
$_['thousand_point']        = '.';

// Text
$_['text_home']             = '<i class="fa fa-home"></i>';
$_['text_yes']              = 'Yes';
$_['text_no']               = 'No';
$_['text_none']             = ' --- None --- ';
$_['text_select']           = ' --- Please Select --- ';
$_['text_all_zones']        = 'All Zones';
$_['text_pagination']       = 'Showing %d to %d of %d (%d Pages)';
$_['text_loading']          = 'Loading...';
$_['text_no_results']       = 'No results!';

// Buttons
$_['button_address_add']    = 'Thêm địa chỉ';
$_['button_back']           = 'Quay lại';
$_['button_continue']       = 'Tiếp tục';
$_['button_cart']           = 'Thêm vào giỏ hàng';
$_['button_cancel']         = 'Hủy';
$_['button_compare']        = 'So sánh sản phẩm này';
$_['button_wishlist']       = 'Thêm vào ưa thích';
$_['button_checkout']       = 'Xem giỏ hàng';
$_['button_confirm']        = 'Xác nhận đặt hàng';
$_['button_coupon']         = 'Apply Coupon';
$_['button_delete']         = 'Xóa';
$_['button_download']       = 'Tải xuống';
$_['button_edit']           = 'Sửa';
$_['button_filter']         = 'Refine Search';
$_['button_new_address']    = 'Địa chỉ mới';
$_['button_change_address'] = 'Thay đổi địa chỉ';
$_['button_reviews']        = 'Các đánh giá';
$_['button_write']          = 'Viết đánh giá';
$_['button_login']          = 'Đăng nhập';
$_['button_update']         = 'Cập nhật';
$_['button_remove']         = 'Xóa';
$_['button_reorder']        = 'Sắp xếp';
$_['button_return']         = 'Trở về';
$_['button_shopping']       = 'Tiếp tục mua sắm';
$_['button_search']         = 'Tìm kiếm';
$_['button_shipping']       = 'Apply Shipping';
$_['button_submit']         = 'Xác nhận';
$_['button_guest']          = 'Guest Checkout';
$_['button_view']           = 'Xem';
$_['button_voucher']        = 'Apply Gift Certificate';
$_['button_upload']         = 'Tải file';
$_['button_reward']         = 'Apply Points';
$_['button_quote']          = 'Get Quotes';
$_['button_list']           = 'Danh sách';
$_['button_grid']           = 'Grid';
$_['button_map']            = 'Xem Google bản đồ';

// Error
$_['error_exception']       = 'Error Code(%s): %s in %s on line %s';
$_['error_upload_1']        = 'Warning: The uploaded file exceeds the upload_max_filesize directive in php.ini!';
$_['error_upload_2']        = 'Warning: The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form!';
$_['error_upload_3']        = 'Warning: The uploaded file was only partially uploaded!';
$_['error_upload_4']        = 'Warning: No file was uploaded!';
$_['error_upload_6']        = 'Warning: Missing a temporary folder!';
$_['error_upload_7']        = 'Warning: Failed to write file to disk!';
$_['error_upload_8']        = 'Warning: File upload stopped by extension!';
$_['error_upload_999']      = 'Warning: No error code available!';
$_['error_curl']            = 'CURL: Error Code(%s): %s';