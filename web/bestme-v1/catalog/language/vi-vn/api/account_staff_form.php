<?php
// Heading
$_['heading_title']                  = 'Thêm tài khoản';
$_['edit_heading_title']             = 'Sửa tài khoản';

// Text
$_['breadcrumb_setting']                        = 'Cài đặt';
$_['breadcrumb_account_setting']                = 'Cài đặt tài khoản';
$_['breadcrumb_account_setting_add_staff']      = 'Thêm tài khoản';

$_['title_logout_account']                      = 'Đăng xuất tài khoản';
$_['title_logout_account_des']                  = 'Chấm dứt các phiên đăng nhập của tài khoản này.';
$_['title_logout_account_button']               = 'Chấm dứt các phiên đăng nhập';
$_['title_logout_account_button_des']           = 'Tài khoản này sẽ phải đăng nhập lại từ tất cả các thiết bị.';

$_['title_delete_account']                      = 'Xóa tài khoản nhân viên';
$_['title_delete_account_des']                  = 'Thông tin được sử dụng trong các thông báo về đơn hàng và địa chỉ để liên hệ đến cửa hàng.';
$_['btn_delete_account']                        = 'Xóa tài khoản';
$_['btn_delete_account_des']                    = 'Tài khoản sẽ bị xóa vĩnh viễn khỏi hệ thống và không thể được khôi phục.';

$_['save_setting_button']                       = 'Lưu thay đổi';
$_['skip_setting_button']                       = 'Bỏ qua';

$_['txt_not_change_pass']                       = 'Bỏ qua nếu không thay đổi';

$_['txt_group_staff']                           = 'Nhóm nhân viên';
$_['txt_store_staff']                           = 'Cửa hàng';

// Form
$_['staff_account_title']                       = 'Tài khoản nhân viên';
$_['staff_account_fill']                        = 'Điền thông tin tài khoản nhân viên.';
$_['staff_account_profile']                     = 'Hồ sơ tài khoản';
$_['staff_account_add_image']                   = 'Chọn ảnh';
$_['staff_account_lastname']                    = 'Họ';
$_['staff_account_lastname_placeholder']        = 'Nhập họ';
$_['staff_account_firstname']                   = 'Tên';
$_['staff_account_firstname_placeholder']       = 'Nhập tên';
$_['staff_account_email']                       = 'Địa chỉ email';
$_['staff_account_email_placeholder']           = 'Nhập Email';
$_['staff_account_phone']                       = 'Số điện thoại';
$_['staff_account_phone_placeholder']           = 'Nhập số điện thoại';
$_['staff_account_intro']                       = 'Thông tin giới thiệu (Tùy chọn)';
$_['staff_account_intro_placeholder']           = 'Nhập thông tin giới thiệu';

// password
$_['staff_account_new_password']                          = 'Nhập mật khẩu mới';
$_['staff_account_new_password_placeholder']              = 'Mật khẩu mới';
$_['staff_account_new_password_confirm']                  = 'Xác nhận mật khẩu mới';
$_['staff_account_new_password_confirm_placeholder']      = 'Nhập lại mật khẩu';

// permission
$_['staff_account_all_permission']                          = 'Tài khoản này được phép truy cập tất cả các chức năng';
$_['staff_account_permission_choose']                       = 'Chọn các chức năng tài khoản này có thể truy cập: ';
$_['staff_permission_management']                           = 'Quản lý';
$_['staff_permission_home']                                 = 'Trang chủ';
$_['staff_permission_order']                                = 'Đơn hàng';
$_['staff_permission_product']                              = 'Sản phẩm';
$_['staff_permission_customer']                             = 'Khách hàng';
$_['staff_permission_setting']                              = 'Cài đặt';
$_['staff_permission_online_store']                         = 'Cửa hàng online';
$_['staff_permission_interface']                            = 'Giao diện';
$_['staff_permission_menu']                                 = 'Menu';
$_['staff_permission_blog_page']                            = 'Blog và Trang nội dung';
$_['staff_permission_domain']                               = 'Tên miền';
$_['group_staff_placeholder']                               = 'Chọn nhóm nhân viên';
$_['store_placeholder']                                     = 'Chọn cửa hàng';

// alert
$_['alet_password_confirm']                                 = 'Mật khẩu và mật khẩu xác nhận không giống nhau !';


// Error
$_['error_warning']             = 'Warning: Please check the form carefully for errors!';
$_['error_permission']          = 'Warning: You do not have permission to modify customers!';
$_['error_exists']              = 'Warning: Địa chỉ email đã được đăng ký trước đó!';
$_['error_phone_exists']        = 'Warning: Số điện thoại đã được đăng ký trước đó!';
$_['error_phone']               = 'Số điện thoại không hợp lệ!';
$_['error_firstname']           = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']            = 'Last Name must be between 1 and 32 characters!';
$_['error_email']               = 'Địa chỉ email không hợp lệ!';
$_['error_telephone']           = 'Telephone must be between 3 and 32 characters!';
$_['error_password']            = 'Password must be between 4 and 20 characters!';
$_['error_confirm']             = 'Password and password confirmation do not match!';
$_['error_address_1']           = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']                = 'City must be less than 128 characters!';
$_['error_district']            = 'District must be less than 128 characters!';
$_['error_postcode']            = 'Postcode must be between 2 and 10 characters for this country!';
$_['error_country']             = 'Please select a country!';
$_['error_zone']                = 'Please select a region / state!';
$_['error_custom_field']        = '%s required!';
$_['error_tracking']            = 'Tracking Code required!';
$_['error_tracking_exists']     = 'Tracking code is being used by another affiliate!';
$_['error_cheque']              = 'Cheque Payee Name required!';
$_['error_paypal']              = 'PayPal Email Address does not appear to be valid!';
$_['error_bank_account_name']   = 'Account Name required!';
$_['error_bank_account_number'] = 'Account Number required!';

$_['error_empty']                  = 'Không được bỏ trống!';
$_['error_input_max']              = 'Dữ liệu trường dài hơn 255!';
$_['error_input_max_min_telephone']          = 'Dữ liệu trường phải lớn hơn 8 nhỏ hơn 15!';
$_['error_input_telephone']        = 'Số điện thoại không đúng';
$_['error_input_max_min_password']           = 'Dữ liệu trường phải lớn hơn 6 nhỏ hơn 255!';
$_['error_number']                           = 'Số điện thoại không hợp lệ!';
$_['error_mail_address']                     = 'Bạn nhập sai định dang email!';

$_['confirm_password_title']                    = 'Xác nhận mật khẩu của bạn';
$_['confirm_password_message']                  = 'Vui lòng xác nhận bạn có quyền thay đổi ';
$_['confirm_password_placeholder']              = 'Xác nhận mật khẩu';
$_['confirm_password_skip']                     = 'Bỏ qua';
$_['confirm_password_accept']                   = 'Xác nhận';
$_['txt_account']                               = 'Tài khoản';
$_['alarm_limit_staff']                         = "Bạn đã sử dụng 80% hạn mức số tài khoản nhân viên của gói dịch vụ";