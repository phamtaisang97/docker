<?php
// Text
$_['setting_address']                     = 'Cài đặt địa chỉ';
$_['add_addresss']                        = 'Thêm mới địa chỉ';
$_['entry_firstname']                     = 'Tên:';
$_['entry_setting_addess']                = 'Cài đặt địa chỉ:';
$_['entry_lastname']                      = 'Tên:';
$_['account_phone_number']                = 'Số điện thoại';
$_['account_company']                     = 'Địa chỉ';
$_['account_district']                    = 'Quận/huyện:';
$_['account_city']                        = 'Tỉnh/thành phố:';
$_['account_xa']                          = 'Xã/Phường';
$_['account_zipcode']                     = 'Zipcode:';
$_['button_change']                       = 'Thay đổi';
$_['button_save']                         = 'Lưu';
$_['text_home']                           = 'Trang chủ';
$_['text_account']                        = 'Tài khoản';
$_['text_history_order']                  = 'Đặt làm địa chỉ mặc định';
$_['error_ajax_add']                      = 'Phải nhập đầy đủ thông tin';
$_['error_error_phone']                   = 'Số điện thoại phải là dạng số';
$_['text_delete']                         = 'Bạn có chắc chắn muốn xóa không ?';

$_['error_phone']                       = 'Số điện thoại không được để trống';
$_['error_phone_exist']                 = 'Số điện thoại đã tồn tại trong hệ thống';
$_['error_phone_2']                     = 'Số điện thoại không hợp lệ';
$_['error_phone_3']                     = 'Số điện thoại không quá 15 ký tự';
$_['error_lastname']                    = 'Họ không quá 20 ký tự';
$_['error_firstname']                   = 'Tên không quá 20 ký tự';
$_['error_fullname']                    = 'Tên không được để trống hoặc vượt quá 40 ký tự';
$_['error_fullname_special_character']  = 'Họ và tên không được chứa ký tự đặc biệt';


$_['error_address']                     = 'Địa chỉ không được để trống';
$_['error_city_delivery']               = 'Tỉnh/thành phố không được để trống';
$_['error_district_delivery']           = 'Quận/huyện không được để trống';
$_['error_ward_delivery']               = 'Phường/xã không được để trống';






// text
$_['txt_province']                      = 'Tỉnh/Thành phố';
$_['txt_district']                      = 'Quận/Huyện';
$_['txt_ward']                          = 'Xã/Phường';

