<?php
// Text
$_['history_title']                          = 'Lịch sử';

// Table
$_['history_text_name']                      = 'Tên sản phẩm';
$_['history_text_customer']                  = 'Khách hàng';
$_['history_text_address']                   = 'Địa chỉ';
$_['history_text_value']                     = 'Giá trị';
$_['history_text_payment_status']            = 'Trạng thái đơn hàng';
