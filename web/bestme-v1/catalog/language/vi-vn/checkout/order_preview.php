<?php
// Heading
$_['heading_title']                         = 'Giỏ hàng';

// Order detail
$_['order_buyer_info']                      = 'Thông tin người mua';
$_['order_buyer_name']                      = 'Họ và tên';
$_['order_buyer_first_name']                = 'Tên';
$_['order_buyer_last_name']                 = 'Họ đệm';
$_['order_buyer_name_placeholder']          = 'Nhập tên của bạn';
$_['order_buyer_first_name_placeholder']     = 'Nhập tên';
$_['order_buyer_last_name_placeholder']     = 'Nhập họ đệm';
$_['order_buyer_fist_name_placeholder']     = 'Nhập tên';
$_['order_buyer_last_name_placeholder']     = 'Nhập họ đệm';
$_['order_buyer_email']                     = 'Email';
$_['order_buyer_email_placeholder']         = 'Nhập Email';
$_['order_buyer_phone_number']              = 'Số điện thoại';
$_['order_buyer_phone_number_placeholder']  = 'Nhập số điện thoại';
$_['order_buyer_password']                  = 'Password';
$_['order_buyer_password_placeholder']      = 'Password';

// Order receiver
$_['order_receiver_address']                    = 'Địa chỉ giao hàng';
$_['order_receiver_name']                       = 'Họ và tên';
$_['order_receiver_first_name']                 = 'Tên';
$_['order_receiver_last_name']                  = 'Họ đệm';
$_['order_receiver_name_placeholder']           = 'Nhập tên của bạn';
$_['order_receiver_first_name_placeholder']      = 'Nhập tên';
$_['order_receiver_last_name_placeholder']      = 'Nhập họ đệm';
$_['order_receiver_phone_number']               = 'Số điện thoại';
$_['order_receiver_phone_number_placeholder']   = 'Nhập số điện thoại';
$_['order_receiver_province']                   = 'Tỉnh/Thành phố';
$_['order_receiver_province_select']            = '-- Chọn --';
$_['order_receiver_district']                   = 'Quận/Huyện';
$_['order_receiver_district_select']            = '-- Chọn --';
$_['order_receiver_ward']                       = 'Phường/Xã';
$_['order_receiver_ward_select']                = '-- Chọn --';
$_['order_receiver_address']                    = 'Địa chỉ';
$_['order_receiver_address_placeholder']        = 'Nhập tên đường/số nhà';

// order button
$_['order_button_order']                    = 'ORDER';
$_['order_button_order_help_text']          = '(Xin vui lòng kiểm tra lại đơn hàng trước khi Đặt mua)';

// Order final
$_['order_summary_template']                = 'Đơn hàng (%d sản phẩm)';
$_['order_product_unit']                    = 'sản phẩm';
$_['order_edit']                            = 'Sửa';
$_['order_temp_total']                      = 'Tạm tính';
$_['order_transport_fee']                   = 'Phí vận chuyển';
$_['order_total']                           = 'Thành tiền';
$_['order_help_text_1']                     = '- Thanh toán bằng tiền mặt khi nhận hàng';
$_['order_help_text_2']                     = '- Hàng sẽ được chuyển trong vòng 1 tuần';
$_['cannot_stop_unique_payment_method']                     = 'Bạn không thể ngưng kích hoạt phương thức thanh toán duy nhất';


$_['text_note_placeholder']                     = 'Ghi chú';