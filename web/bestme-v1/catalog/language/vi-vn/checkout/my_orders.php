<?php
// Heading
$_['heading_title']                           = 'Giỏ hàng';

// Order list
$_['order_title']                             = 'Đơn hàng';
$_['text_ordered_at']                         = 'Đặt hàng lúc: ';
$_['text_amount']                             = 'Số lượng: ';

$_['text_into_money']                         = 'Thành tiền:';
$_['text_vat_included']                       = '(Đã bao gồm VAT)';
$_['text_button_order']                       = 'Đặt hàng';

$_['text_remove_alert_confirm']               = 'Bạn có muốn bỏ sản phẩm này khỏi giỏ hàng ?';
$_['text_remove_fail']                        = 'Không thành công';
$_['text_remove_success']                     = 'Thành công';
