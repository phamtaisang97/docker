<?php
// Heading
$_['heading_title'] = 'Không tìm thấy trang';

// Text
$_['text_error']    = 'The page you requested cannot be found.';

// Button
$_['btn_submit']    = 'Đồng ý';