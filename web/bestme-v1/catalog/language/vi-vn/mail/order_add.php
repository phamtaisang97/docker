<?php
// Text
$_['text_subject']              = '[%s] Đơn hàng %s đặt bởi %s';
$_['text_subject_to_customer']  = 'Thông báo đặt thành công đơn hàng %s';
$_['text_greeting']             = 'Xin chào %s, %s vừa đặt hàng trên cửa hàng của bạn lúc %s.';
$_['text_greeting_to_customer'] = 'Xin chào %s, bạn vừa đặt thành công đơn hàng %s. Chi tiết như sau';
$_['text_link']                 = 'Xem đơn hàng';
$_['text_order_detail']         = 'Thông tin đơn hàng';
$_['text_order_code']           = 'Đơn hàng %s';
$_['text_date_added']           = 'Ngày tạo:';
$_['text_order_status']         = 'Trạng thái đơn hàng:';
$_['text_payment_method']       = 'Phương thức thanh toán:';
$_['text_shipping_method']      = 'Phương thức vận chuyển:';
$_['text_email']                = 'E-mail:';
$_['text_telephone']            = 'Số điện thoại:';
$_['text_shipping_address']     = 'Địa chỉ nhận hàng';
$_['text_product']              = 'Tên sản phẩm';
$_['text_sku']                  = 'Mã sản phẩm: ';
$_['text_quantity']             = 'Số lượng sản phẩm';
$_['text_price']                = 'Đơn giá';
$_['text_total']                = 'Thành tiền';
$_['text_order_total']          = 'Tổng tiền hàng';
$_['text_shipping_fee']         = 'Phí vận chuyển';
$_['text_final_total']          = 'Tổng cần thanh toán';
$_['text_footer']               = 'Nếu bạn có bất kỳ câu hỏi nào, đừng ngần ngại liên lạc với chúng tôi tại %s';
$_['text_order_date']           = 'Ngày đặt đơn:';
$_['text_website']              = 'website:';
$_['text_customer_phone']       = 'Điện thoại liên hệ:';