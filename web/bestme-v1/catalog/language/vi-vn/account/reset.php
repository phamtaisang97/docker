<?php
// Heading
$_['heading_title']  = 'Đặt lại mật khẩu của bạn';

// Text
$_['text_password']  = 'Vui lòng nhập vào mật khẩu mới của bạn.';
$_['text_success']   = 'Thành công: Mật khẩu của bạn đã được thay đổi thành công.';

// Entry
$_['entry_password'] = 'Mật khẩu';
$_['entry_confirm']  = 'Xác nhận mật khẩu';

// Error
$_['error_password'] = 'Độ dài mật khẩu phải trong khoảng 4 đến 20 ký tự!';
$_['error_confirm']  = 'Mật khẩu và Xác nhận mật khẩu không khớp!';

// More
$_['error_disabled_forgotten']       = 'Hiện tại tính năng quên mất khẩu không được hỗ trợ (LỖI: 0). Vui lòng đăng nhập!';
$_['error_invalid_activation_link']  = 'Liên kết kích hoạt mật khẩu mới không còn giá trị. Vui lòng gửi lại yêu cầu đặt lại mật khẩu!';