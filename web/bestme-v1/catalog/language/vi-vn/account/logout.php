<?php
// Heading
$_['heading_title'] = 'Đăng xuất tài khoản';

// Text
$_['text_message']  = '<p>Bạn vừa đăng xuất tài khoản. Giờ bạn có thể an toàn rời khỏi máy tính của mình.</p><p>Giỏ hàng của bạn đã được lưu lại, các mặt hàng trong giỏ hàng sẽ được khôi phục lại khi bạn đăng nhập lại tài khoản của mình.</p>';
$_['text_account']  = 'Tài khoản';
$_['text_logout']   = 'Đăng xuất';