<?php
// Heading
$_['heading_title']                = 'Đăng nhập tài khoản';

// Text
$_['text_account']                 = 'Tài khoản';
$_['text_login']                   = 'Đăng nhập';
$_['text_new_customer']            = 'Khách hàng mới';
$_['text_register_2']              = 'Đăng ký tại đây';
$_['text_register_account']        = 'Đăng ký tài khoản giúp quý khách mua sắm nhanh hơn, cho phép cập nhật trạng thái đơn hàng và xem thông tin lịch sử các đơn hàng đã tạo trước đó.';
$_['text_returning_customer']      = 'Khách hàng quay lại';
$_['text_i_am_returning_customer'] = 'Tôi là một khách hàng quay lại';
$_['text_forgotten_2']             = 'Quên mật khẩu?';

// Entry
$_['entry_email']                  = 'Email';
$_['entry_password']               = 'Mật khẩu';

// Error
$_['error_login']                  = 'Cảnh báo: Địa chỉ E-Mail và/hoặc Mật khẩu không đúng.';
$_['error_attempts']               = 'Cảnh báo: Tài khoản của bạn vượt quá số lần thử đăng nhập sai. Hãy thử lại sau 1 giờ.';
$_['error_approved']               = 'Cảnh báo: Tài khoản của bạn cần được chấp nhận trước khi bạn có thể đăng nhập.';

// or with gg, fb
$_['other_or_with']                = 'Hoặc, đăng nhập bằng';
$_['other_or_with_google']         = 'Google';
$_['other_or_with_facebook']       = 'Facebook';

// don't have account
$_['text_dont_have_account']       = 'Bạn chưa có tài khoản?';

