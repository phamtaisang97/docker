<?php
// Heading
$_['heading_title']        = 'Đăng ký tài khoản';

// Text
$_['text_account']         = 'Tài khoản';
$_['text_register']        = 'Đăng ký';
$_['text_login']           = 'Đăng nhập';
$_['text_or']              = 'hoặc';
$_['text_account_already'] = 'Nếu bạn đã có tài khoản, vui lòng đăng nhập đường dẫn tại <a href="%s">trang đăng nhập</a>.';
$_['text_your_details']    = 'Chi tiết thông tin cá nhân';
$_['text_newsletter']      = 'Email';
$_['text_your_password']   = 'Mật khẩu của bạn';
$_['text_agree']           = 'Tôi đã đọc và đồng ý với <a href="%s" class="agree"><b>%s</b></a>';
$_['text_your_reset_password']   = 'Khôi phục mật khẩu';
$_['text_your_cencal']   = 'Bỏ qua';
$_['text_your_send']   = 'Gửi';

// Entry
$_['entry_customer_group'] = 'Nhóm khách hàng';
$_['entry_first']      = 'Họ';
$_['entry_firstname']      = 'Tên';
$_['entry_lastname']       = 'Họ và đệm';
$_['entry_fullname']       = 'Họ và tên';
$_['entry_email']          = 'Email';
$_['entry_telephone']      = 'Số điện thoại';
$_['entry_newsletter']     = 'Đăng ký theo dõi';
$_['entry_password']       = 'Mật khẩu';
$_['entry_confirm']        = 'Xác nhận mật khẩu';
$_['entry_address']        = 'Địa chỉ';
// Error
$_['error_exists']         = 'Cảnh báo: Email đã được đăng ký trên hệ thống!';
$_['error_firstname']      = 'Họ có độ dài từ 1 đến 32 ký tự!';
$_['error_lastname']       = 'Tên có đội dài từ 1 đến 32 ký tự!';
$_['error_telephone']      = 'Số điện thoại có đội dài từ 1 đến 32 ký tự!';
$_['error_custom_field']   = '%s bắt buộc!';
$_['error_password']       = 'Mật khẩu có đội dài từ 4 đến 20 ký tự!';
$_['error_confirm']        = 'Mật khẩu xác nhận không khớp!';
$_['error_agree']          = 'Cảnh báo: Bạn cần đồng ý với %s!';
$_['error_email']          = 'Email chưa được đăng ký, vui lòng thử lại!';
$_['error_approved']       = 'Email chưa được kích hoạt trên cửa hàng.';

// More
$_['entry_forget_password']               = 'Quên mật khẩu?';
$_['other_register_with']                 = 'Hoặc, đăng ký với';
$_['other_register_with_facebook']        = 'Facebook';
$_['other_register_with_google']          = 'Google';
$_['text_have_an_account']                = 'Đã có tài khoản?';
$_['text_login_here']                     = 'Đăng nhập tại đây';
$_['text_accept']                     = 'Bằng việc bấm vào nút đăng ký, tôi đồng ý với chính sách của cửa hàng.';
