<?php
// Heading
$_['heading_title']         = 'Lịch sử đơn hàng';

// Text
$_['text_account']          = 'Tài khoản';
$_['text_order']            = 'Thông tin đơn hàng';
$_['text_order_detail']     = 'Chi tiết đơn hàng';
$_['text_invoice_no']       = 'Số hóa đơn.:';
$_['text_order_id']         = 'ID Đơn hàng:';
$_['text_date_added']       = 'Ngày thêm:';
$_['text_shipping_address'] = 'Địa chỉ vận chuyển';
$_['text_shipping_method']  = 'Phương thức vận chuyển:';
$_['text_payment_address']  = 'Địa chỉ thanh toán';
$_['text_payment_method']   = 'Phương thức thanh toán:';
$_['text_comment']          = 'Nhận xét đơn hàng';
$_['text_history']          = 'Lịch sử đơn hàng';
$_['text_success']          = 'Thành công: Bạn đã thêm <a href="%s">%s</a> tới <a href="%s">giỏ hàng</a>!';
$_['text_empty']            = 'Bạn chưa có bất kỳ đơn hàng trước đó!';
$_['text_error']            = 'Đơn hàng bạn yêu cầu không tìm thấy!';
$_['text_vnpay_payment']    = 'Thanh toán qua VNPay';
$_['text_payment_status_0'] = 'Chưa thanh toán';
$_['text_payment_status_1'] = 'Đã thanh toán';
$_['text_payment_status_2'] = 'Đã hoàn tiền';
$_['text_payment_status_4'] = 'Thanh toán lỗi';
// Column
$_['column_order_id']       = 'ID Đơn hàng';
$_['column_customer']       = 'Khách hàng';
$_['column_product']        = 'STT. sản phẩm';
$_['column_name']           = 'Tên sản phẩm';
$_['column_model']          = 'Model';
$_['column_quantity']       = 'Số lượng';
$_['column_price']          = 'Giá';
$_['column_total']          = 'Tổng';
$_['column_action']         = 'Hành động';
$_['column_date_added']     = 'Ngày thêm';
$_['column_status']         = 'Trạng thái';
$_['column_comment']        = 'Nhận xét';

// Error
$_['error_reorder']         = '%s hiện không có sẵn để đặt lại.';