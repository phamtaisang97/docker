<?php
// Heading
$_['heading_title'] = 'Tài khoản của bạn đã được tạo!';

// Text
$_['text_message']  = '<p>Chúc mừng! Tài khoản mới của bạn đã được tạo thành công!</p> <p>Bây giờ bạn có thể tận hưởng lợi ích thành viên khi tham gia trải nghiệm mua sắm cùng chúng tôi.</p> <p>Nếu bạn có câu hỏi gì về hoạt động của cửa hàng, vui lòng liên hệ với chủ cửa hàng.</p> <p>Một email xác nhận đã được gửi tới hòm thư của bạn. Nếu bạn không nhận được email này trong vòng 1 giờ, vui lòng <a href="%s">liên hệ chúng tôi</a>.</p>';
$_['text_approval'] = '<p>Cảm ơn bạn đã đăng ký!</p><p>Bạn sẽ nhận được email thông báo khi tài khoản của bạn được kích hoạt bởi chủ cửa hàng.</p><p>Nếu có BẤT KỲ câu hỏi nào, Vui lòng<a href="%s"> liên hệ với chủ cửa hàng để được giải đáp</a>.</p>';
$_['text_account']  = 'Tài khoản';
$_['text_success']  = 'Thành công';