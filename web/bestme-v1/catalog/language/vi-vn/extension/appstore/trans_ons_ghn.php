<?php
$_['ghn_code_ready_to_pick']                  = "Đơn hàng được tạo thành công";
$_['ghn_code_picking']                        = "Nhân viên giao hàng đang trên đường lấy hàng";
$_['ghn_code_cancel']                         = "Đơn hàng bị hủy";
$_['ghn_code_money_collect_picking']          = "Nhân viên giao hàng đang tương tác với chủ shop";
$_['ghn_code_picked']                         = "Nhân viên giao hàng đã lấy hàng thành công";
$_['ghn_code_storing']                        = "Đơn hàng đang được lưu tại kho GHN";
$_['ghn_code_transporting']                   = "Đơn hàng đang được trung chuyển";
$_['ghn_code_sorting']                        = "Đơn hàng đang được phân loại tại kho phân loại";
$_['ghn_code_delivering']                     = "Đơn hàng đang được giao tới người nhận";
$_['ghn_code_money_collect_delivering']       = "Nhân viên giao hàng đang tương tác với người bán";
$_['ghn_code_delivered']                      = "Đơn hàng được giao thành công";
$_['ghn_code_delivery_fail']                  = "Đơn hàng giao thất bại";
$_['ghn_code_waiting_to_return']              = "Đơn hàng đang trong hàng chờ hoàn trả (có thể giao lại trong 24/48h)";
$_['ghn_code_return']                         = "Đơn hàng đang đợi nhân viên giao hàng đến trả cho chủ shop sau 3 lần giao hàng lại thất bại";
$_['ghn_code_return_transporting']            = "Đơn hàng đang được trung chuyển giữa các kho";
$_['ghn_code_return_sorting']                 = "Đơn hàng đang được phân loại tại kho phân loại của GHN";
$_['ghn_code_returning']                      = "Nhân viên giao hàng đang đi trả hàng";
$_['ghn_code_return_fail']                    = "Đơn hàng bị trả thất bại";
$_['ghn_code_returned']                       = "Đơn hàng được hoàn trả thành công";
$_['ghn_code_exception']                      = "Đơn hàng được xử lý ngoại lệ (trường hợp không đi đúng quy trình)";
$_['ghn_code_damage']                         = "Đơn hàng bị tác động làm hư hại";
$_['ghn_code_lost']                           = "Đơn hàng bị mất";

// migrate old - incorrect status orders
$_['ghn_code_Cancel'] = "Đơn hàng bị hủy"; // ghn_code_cancel