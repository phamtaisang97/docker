<?php
// Heading
$_['heading_title']                             = 'Cài đặt vận chuyển';

// Text
$_['breadcrumb_setting']                        = 'Cài đặt';
$_['breadcrumb_delivery_setting']               = 'Cài đặt vận chuyển';

$_['delivery_setting_title']                    = 'Vận chuyển';

$_['text_success']                              = 'Cập nhật thông tin thành công!';

$_['trans_ons_ghtk']                                   = 'Giao hàng tiết kiệm';
$_['not_login']                                        = 'Chưa đăng nhập';
$_['not_map_address']                                  = 'Chưa liên kết địa chỉ lấy hàng';

$_['text_ghtk']                                        = 'Giao hàng tiết kiệm';
$_['display_name']                                     = 'Giao hàng tiết kiệm';
$_['text_viettelpost']                                 = 'ViettelPost';
$_['text_shipping_unit']                               = 'Đơn vị vận chuyển';
$_['text_bestme_connection_with_shipping_partners']    = 'Kết nối ' . PRODUCTION_BRAND_NAME . ' với các đối tác vận chuyển giúp bạn tạo vận đơn và quản lý thuận tiện hơn.';
$_['text_fast_delivery_connection']                    = 'Kết nối giao hàng tiết kiệm ';
$_['text_after_processing_the_order_on_Bestme']        = 'Sau khi xử lý đơn hàng trên <b>' . PRODUCTION_BRAND_NAME . '</b>, hệ thống sẽ tự động tạo vận đơn trên <b>Giao hàng tiết kiệm</b>.';
$_['text_connected']                                   = 'Đã kết nối';
$_['text_see_the_instructions']                        = 'Xem hướng dẫn';
$_['text_connect']                                     = 'Kết nối ngay';
$_['text_create_new_account']                          = 'Tạo tài khoản mới';
$_['text_collection_of_goods']                         = 'Địa điểm lấy hàng';
$_['text_cancel_collection']                           = 'Hủy kết nối';
$_['text_user_name_ghtk']                              = 'Tài khoản GHTK (Email/Số điện thoại)';
$_['text_password']                                    = 'Mật khẩu';
$_['text_button_connect']                              = 'Kết nối';
$_['text_button_cancel']                               = 'Bỏ qua';
$_['text_disconnecting_from']                          = 'Hủy kết nối với đơn vị vận chuyển';
$_['text_after_canceling_the_connection']              = 'Sau khi hủy kết nối, website sẽ không thể tiếp tục tự động tạo vận đơn trên';
$_['text_are_you_sure']                                = 'Bạn có chắc chắn muốn hủy';
$_['text_close']                                       = 'Đóng';
$_['text_destroy']                                     = 'Hủy';
$_['text_link_location']                               = 'Liên kết địa điểm lấy hàng';
$_['text_address_config']                              = 'Cấu hình địa chỉ';

// error
$_['text_error_account_info_invalid']                         = 'Thông tin tài khoản chưa chính xác vui lòng kiểm tra lại.';

/* webhook status code and message */
$_['ghtk_code_-1']            = "Hủy đơn hàng";
$_['ghtk_code_1']             =	"Chưa tiếp nhận";
$_['ghtk_code_2']             =	"Đã tiếp nhận";
$_['ghtk_code_3']             =	"Đã lấy hàng/Đã nhập kho";
$_['ghtk_code_4']             =	"Đã điều phối giao hàng/Đang giao hàng";
$_['ghtk_code_5']             =	"Đã giao hàng/Chưa đối soát";
$_['ghtk_code_6']             =	"Đã đối soát";
$_['ghtk_code_7']             =	"Không lấy được hàng";
$_['ghtk_code_8']             =	"Hoãn lấy hàng";
$_['ghtk_code_9']             =	"Không giao được hàng";
$_['ghtk_code_10']            =	"Delay giao hàng";
$_['ghtk_code_11']            =	"Đã đối soát công nợ trả hàng";
$_['ghtk_code_12']            =	"Đã điều phối lấy hàng/Đang lấy hàng";
$_['ghtk_code_13']            =	"Đơn hàng bồi hoàn";
$_['ghtk_code_20']            =	"Đang trả hàng (COD cầm hàng đi trả)";
$_['ghtk_code_21']            =	"Đã trả hàng (COD đã trả xong hàng)";
$_['ghtk_code_123']           =	"Shipper báo đã lấy hàng";
$_['ghtk_code_127']           =	"Shipper (nhân viên lấy/giao hàng) báo không lấy được hàng";
$_['ghtk_code_128']           =	"Shipper báo delay lấy hàng";
$_['ghtk_code_45']            =	"Shipper báo đã giao hàng";
$_['ghtk_code_49']            =	"Shipper báo không giao được giao hàng";
$_['ghtk_code_410']           =	"Shipper báo delay giao hàng";