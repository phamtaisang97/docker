<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Địa chỉ liên hệ';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Certificates';
$_['text_affiliate']    = 'Affiliate';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Powered By <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';
$_['text_phone']        = 'Số điện thoại: ';
$_['text_address']      = 'Địa chỉ: ';
$_['text_email']        = 'Email: ';
$_['show_more']         = 'Show More';
$_['show_less']         = 'Show Less';
$_['txt_registration_email']          = 'Đừng bỏ lỡ hàng ngàn sản phẩm và chương trình siêu hấp dẫn!';

// Email subscribe
$_['subscribe_email_subject']            = '[' . PRODUCTION_BRAND_NAME_UPPERCASE . '] Đăng ký theo dõi từ %s';
$_['subscribe_email_body_text']          = 'Xin chào chủ website. Bạn nhận được thông tin đăng ký theo dõi từ khách hàng. Chi tiết:' . PHP_EOL .
                                           '- Email: %s' . PHP_EOL . PHP_EOL .
                                           'Cảm ơn bạn đã đồng hàng cùng ' . PRODUCTION_BRAND_NAME . ' !' . PHP_EOL .
                                           'Trân trọng,' . PHP_EOL .
                                           'Đội ngũ ' . PRODUCTION_BRAND_NAME . ' ' . PHP_EOL;

$_['subscribe_email_invalid']            = 'Email không hợp lệ';
$_['subscribe_email_sent_ok']            = 'Gửi email thành công. Cảm ơn bạn đã đăng ký nhận thông tin theo dõi từ chúng tôi.';
$_['subscribe_email_sent_exits']         = 'Email này đã được đăng ký nhận thông tin theo dõi từ chúng tôi.';
$_['subscribe_email_sent_failed']        = 'Gửi email bị lỗi, vui lòng thử lại sau.';

$_['text_right_copy']               = 'Bản quyền thuộc về <a href="%s">%s</a>. Thiết kế và cung cấp bởi <a href="%s">' . PRODUCTION_BRAND_NAME . '</a> Mọi quyền được bảo lưu';
$_['text_right_copy_mobile']        = 'Bản quyền thuộc về <a href="%s">%s</a> <br><br> Thiết kế và cung cấp bởi <a href="%s">' . PRODUCTION_BRAND_NAME . '</a> <br><br> Mọi quyền được bảo lưu';

$_['text_right_copy_apollo']               = '<span class="ft-license">Bản quyền thuộc về <a href="%s">%s</a>.</span>
                                              <span class="ft-design"> Thiết kế bởi <a href="#">Apollo</a>.</span>
                                              <span class="ft-provide"> Cung cấp bởi <a href="%s">' . PRODUCTION_BRAND_NAME . '</a>.</span> 
                                              <span class="ft-all-right-reserved"> Mọi quyền được bảo lưu</span>';
$_['text_right_copy_mobile_apollo']        = '<span class="ft-license">Bản quyền thuộc về <a href="%s">%s</a>.</span> <br><br> 
                                              <span class="ft-design"> Thiết kế bởi <a href="#">Apollo</a>.</span>
                                              <span class="ft-provide"> Cung cấp bởi <a href="%s">' . PRODUCTION_BRAND_NAME . '</a>.</span> <br><br> 
                                              <span class="ft-all-right-reserved"> Mọi quyền được bảo lưu</span>';

$_['text_right_copy_egany']               = 'Bản quyền thuộc về <a href="%s">%s</a> Thiết kế bởi <a href="#">Egany</a>. Cung cấp bởi <a href="%s">' . PRODUCTION_BRAND_NAME . '</a>. Mọi quyền được bảo lưu';
$_['text_right_copy_mobile_egany']        = 'Bản quyền thuộc về <a href="%s">%s</a> <br><br> Thiết kế bởi <a href="#">Egany</a>. Cung cấp bởi <a href="%s">' . PRODUCTION_BRAND_NAME . '</a>. <br><br> Mọi quyền được bảo lưu';

$_['text_right_copy_jupiter']               = 'Bản quyền thuộc về <a href="%s">%s</a> Thiết kế bởi <a href="#">Jupiter</a>. Cung cấp bởi <a href="%s">' . PRODUCTION_BRAND_NAME . '</a>. Mọi quyền được bảo lưu';
$_['text_right_copy_mobile_jupiter']        = 'Bản quyền thuộc về <a href="%s">%s</a> <br><br> Thiết kế bởi <a href="#">Jupiter</a>. Cung cấp bởi <a href="%s">' . PRODUCTION_BRAND_NAME . '</a>. <br><br> Mọi quyền được bảo lưu';

$_['text_right_copy_jupiter']               = 'Bản quyền thuộc về <a href="%s">%s</a> Thiết kế bởi <a href="#">Jupiter</a>. Cung cấp bởi <a href="%s">' . PRODUCTION_BRAND_NAME . '</a>. Mọi quyền được bảo lưu';
$_['text_right_copy_mobile_jupiter']        = 'Bản quyền thuộc về <a href="%s">%s</a> <br><br> Thiết kế bởi <a href="#">Jupiter</a>. Cung cấp bởi <a href="%s">' . PRODUCTION_BRAND_NAME . '</a>. <br><br> Mọi quyền được bảo lưu';
