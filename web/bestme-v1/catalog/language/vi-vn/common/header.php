<?php
// Text
$_['text_home']          = 'Trang chủ';
$_['text_wishlist']      = 'Danh sách yêu thích (%s)';
$_['text_shopping_cart'] = 'Giỏ hàng';
$_['text_category']      = 'Danh mục';
$_['text_all_category']  = 'Tất cả Danh mục';
$_['text_account']       = 'Tài khoản';
$_['text_register']      = 'Đăng ký';
$_['text_login']         = 'Đăng nhập';
$_['text_order']         = 'Lịch sử đặt hàng';
$_['text_transaction']   = 'Giao dịch';
$_['text_download']      = 'Tải về';
$_['text_logout']        = 'Đăng xuất';
$_['text_checkout']      = 'Xem giỏ hàng';
$_['text_search_button']             = 'Tìm kiếm';
$_['text_search_button_placeholder'] = 'Tìm kiếm...';
$_['text_all']           = 'Hiện tất cả';
$_['text_slogan']        = 'Mua bán thời trang online, giá cực sốc';
$_['text_shop_dept']     = 'Shop theo bộ phận';
$_['text_blogs']     = 'Bài viết';

// menu when logged in
$_['menu_item_account_dashboard']    = 'Thông tin tài khoản';
$_['menu_item_my_orders']            = 'Đơn hàng của tôi';
$_['menu_item_history']              = 'Lịch sử đơn hàng';
