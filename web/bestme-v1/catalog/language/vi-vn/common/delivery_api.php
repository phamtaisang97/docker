<?php

$_['text_success']     = 'Thành công!';
$_['text_error']       = 'Xảy ra lỗi!';
$_['text_error_choose_service']       = 'Lỗi chưa chọn dịch vụ!';
$_['text_error_unknown']       = 'Lỗi không xác định!';
$_['ghn']         = "Giao hàng nhanh";
$_['viettel_post']         = "ViettelPost";
$_['token_error'] = "Lỗi tài khoản bạn chưa kết nối hoặc tài khoản không đúng.";

$_['update_hub_error'] = "Cập nhật liên kết địa điểm lấy hàng lỗi. Vui lòng thực hiện lại hoặc kiểm tra lại mã OTP (nếu có)";
