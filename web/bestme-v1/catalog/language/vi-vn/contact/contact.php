<?php
// Contact
$_['heading']                              = 'Liên hệ';
$_['form_title']                           = 'Liên hệ với chúng tôi';
$_['form_description']                     = 'Vui lòng để lại thông tin, chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.';
$_['form_full_name']                       = 'Họ và tên';
$_['form_full_name_placeholder']           = 'Nhập tên của bạn';
$_['form_email']                           = 'Email';
$_['form_email_placeholder']               = 'Nhập Email';
$_['form_phone_number']                    = 'Số điện thoại';
$_['form_phone_number_placeholder']        = 'Nhập số điện thoại';
$_['form_your_mind']                       = 'Ý kiến của bạn';
$_['form_button_text']                     = 'Gửi';

// alert
$_['alert_fail']                           = 'Không thành công!';

// Email
$_['email_subject']                        = '[Liên hệ] Liên hệ từ %s';
$_['email_body_text']                      = 'Họ và tên    : %s' . PHP_EOL .
                                             'Email        : %s' . PHP_EOL .
                                             'Số điện thoại: %s' . PHP_EOL .
                                             'Ý kiến       : %s' . PHP_EOL;
$_['text_success']                         = 'Gửi thông tin liên hệ thành công! Cảm ơn quý khách! Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.';

// Contact for tra gop
// Email
$_['installment_email_subject']            = '[Trả góp] Liên hệ mua trả góp từ %s';
$_['installment_email_body_text']          = 'Xin chào chủ website. Bạn nhận được thông tin liên hệ mua trả góp từ khách hàng. Chi tiết:' . PHP_EOL .
                                             '- Họ và tên     : %s' . PHP_EOL .
                                             '- Số điện thoại : %s' . PHP_EOL .
                                             '- Sản phẩm  : %s' . PHP_EOL .
                                             '- Ý kiến        : %s' . PHP_EOL . PHP_EOL .
                                             'Cảm ơn bạn đã đồng hàng cùng ' . PRODUCTION_BRAND_NAME . ' !' . PHP_EOL .
                                             'Trân trọng,' . PHP_EOL .
                                             'Đội ngũ ' . PRODUCTION_BRAND_NAME . ' ' . PHP_EOL;
$_['installment_text_success']             = 'Gửi thông tin liên hệ mua trả góp thành công! Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.';

// Contact by Product
$_['contact_by_product_email_subject']            = '[Liên hệ] Liên hệ mua sản phẩm từ %s';
$_['contact_by_product_email_body_text']          = 'Xin chào chủ website. Bạn nhận được thông tin liên hệ mua sản phẩm từ khách hàng. Chi tiết:' . PHP_EOL .
                                             '- Họ và tên     : %s' . PHP_EOL .
                                             '- Số điện thoại : %s' . PHP_EOL .
                                             '- Sản phẩm  : %s' . PHP_EOL .
                                             '- Ý kiến        : %s' . PHP_EOL . PHP_EOL .
                                             'Cảm ơn bạn đã đồng hàng cùng ' . PRODUCTION_BRAND_NAME . ' !' . PHP_EOL .
                                             'Trân trọng,' . PHP_EOL .
                                             'Đội ngũ ' . PRODUCTION_BRAND_NAME . ' ' . PHP_EOL;
$_['contact_by_product_text_success']             = 'Gửi thông tin liên hệ mua sản phẩm thành công! Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.';

// v3.6.1.2
$_['province_not_null'] = "Tỉnh/Thành phố không được để trống";
$_['district_not_null'] = "Quận/Huyện không được để trống";
$_['ward_not_null'] = "Phường/Xã không được để trống";
$_['address_not_null'] = "Địa chỉ không được để trống";
