<?php
$_['order_status_draft']                        = "Đơn nháp";
$_['order_status_processing']                   = "Đang xử lý";
$_['order_status_delivering']                   = "Đang vận chuyển";
$_['order_status_comback']                      = "Đang hoàn đơn";
$_['order_status_complete']                     = "Hoàn thành";
$_['order_status_canceled']                     = "Đã huỷ";