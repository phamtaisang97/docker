<?php
$_['order_status_draft']                        = "Draft";
$_['order_status_processing']                   = "Processing";
$_['order_status_delivering']                   = "Shipping";
$_['order_status_comback']                      = "Completing the application";
$_['order_status_complete']                     = "Complete";
$_['order_status_canceled']                     = "Canceled";