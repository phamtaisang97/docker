<?php
// Contact
$_['heading']                              = 'Contact us';
$_['form_title']                           = 'Contact us';
$_['form_description']                     = 'Jot us a note and we’ll get back to you as quickly as possible.';
$_['form_full_name']                       = 'Full name';
$_['form_full_name_placeholder']           = 'Enter your full name';
$_['form_email']                           = 'Email';
$_['form_email_placeholder']               = 'Enter your email';
$_['form_phone_number']                    = 'Phone number';
$_['form_phone_number_placeholder']        = 'Enter phone number';
$_['form_your_mind']                       = 'What is your mind';
$_['form_button_text']                     = 'Submit';

// Email
$_['email_subject']                        = '[Contact] Contact from %s';
$_['email_body_text']                      = 'Full name        : %s' . PHP_EOL .
                                             'Email            : %s' . PHP_EOL .
                                             'Phone number     : %s' . PHP_EOL .
                                             'What is your mind: %s' . PHP_EOL;
$_['text_success']                         = 'Form is sent successfully. Thank you! We’ll get back to you as quickly as possible.';
// Email
$_['installment_email_subject']            = '[Contact] Contact to buy installment payment from %s';
$_['installment_email_body_text']          = 'First and last name    : %s' . PHP_EOL .
                                             'Phone: %s' . PHP_EOL .
                                             'Content       : %s' . PHP_EOL;
$_['installment_text_success']             = 'Send contact information for successful installment purchase! We will contact you as soon as possible.';

// v3.6.1.2
$_['province_not_null'] = "Province not empty";
$_['district_not_null'] = "District not empty";
$_['ward_not_null'] = "Ward not empty";
$_['address_not_null'] = "Address not empty";