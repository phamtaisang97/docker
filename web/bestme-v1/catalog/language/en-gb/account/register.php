<?php
// Heading
$_['heading_title']        = 'Register Account';

// Text
$_['text_account']         = 'Account';
$_['text_register']        = 'Register';
$_['text_account_already'] = 'If you already have an account with us, please login at the <a href="%s">login page</a>.';
$_['text_your_details']    = 'Your Personal Details';
$_['text_newsletter']      = 'Newsletter';
$_['text_your_password']   = 'Your Password';
$_['text_agree']           = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Customer Group';
$_['entry_firstname']      = 'First Name';
$_['entry_lastname']       = 'Last Name';
$_['entry_fullname']       = 'Họ và tên';
$_['entry_email']          = 'E-Mail';
$_['entry_telephone']      = 'Telephone';
$_['entry_newsletter']     = 'Subscribe';
$_['entry_password']       = 'Password';
$_['entry_confirm']        = 'Password Confirm';
$_['entry_address']        = 'Address';

// Error
$_['error_exists']         = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']      = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']       = 'Last Name must be between 1 and 32 characters!';
$_['error_fullname']       = 'Last Name must be between 1 and 64 characters!';
$_['error_email']          = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']      = 'Telephone must be between 3 and 32 characters!';
$_['error_custom_field']   = '%s required!';
$_['error_password']       = 'Password must be between 4 and 20 characters!';
$_['error_confirm']        = 'Password confirmation does not match password!';
$_['error_agree']          = 'Warning: You must agree to the %s!';

// More
$_['entry_forget_password']               = 'Foget Password?';
$_['other_register_with']                 = 'Or, register with';
$_['other_register_with_facebook']        = 'Facebook';
$_['other_register_with_google']          = 'Google';
$_['text_have_an_account']                = 'Have an account?';
$_['text_login_here']                     = 'Login here';
