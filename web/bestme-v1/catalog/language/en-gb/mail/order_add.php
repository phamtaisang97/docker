<?php
// Text
$_['text_subject']              = '[%s] Order %s by %s';
$_['text_subject_to_customer']  = 'Thông báo đặt thành công đơn hàng %s';
$_['text_greeting']             = 'Hello %s, %s just ordered on your store at %s.';
$_['text_greeting_to_customer'] = 'Xin chào %s, bạn vừa đặt thành công đơn hàng %s. Chi tiết như sau';
$_['text_link']                 = 'View order';
$_['text_order_detail']         = 'Order Details';
$_['text_order_id']             = 'Order %s';
$_['text_date_added']           = 'Date Added:';
$_['text_order_status']         = 'Order Status:';
$_['text_payment_method']       = 'Payment Method:';
$_['text_shipping_method']      = 'Shipping Method:';
$_['text_email']                = 'E-mail:';
$_['text_telephone']            = 'Telephone:';
$_['text_shipping_address']     = 'Shipping Address';
$_['text_product']              = 'Product';
$_['text_sku']                  = 'SKU';
$_['text_quantity']             = 'Quantity';
$_['text_price']                = 'Price';
$_['text_order_total']          = 'Order Totals';
$_['text_total']                = 'Subtotal';
$_['text_shipping_fee']         = 'Shipping Fee';
$_['text_final_total']          = 'Total';
$_['text_footer']               = 'If you have any questions, do not hesitate to contact us at %s';
$_['text_order_date']           = 'Date Added:';
$_['text_website']              = 'website:';
$_['text_customer_phone']       = 'Telephone:';