<?php
// Text
$_['history_title']                          = 'History';

// Table
$_['history_text_name']                      = 'Name';
$_['history_text_customer']                  = 'Customer';
$_['history_text_address']                   = 'Address';
$_['history_text_value']                     = 'Value';
$_['history_text_payment_status']            = 'Payment Status';
