<?php
// Text
$_['account_info_title']                  = 'Account information';
$_['account_email']                       = 'Email:';
$_['account_address']                     = 'Address:';
$_['entry_firstname']                     = 'Firstname:';
$_['entry_lastname']                      = 'Lastname:';
$_['account_phone_number']                = 'Phone number:';
$_['account_company']                     = 'Company:';
$_['account_district']                    = 'District:';
$_['account_city']                        = 'City:';
$_['account_country']                     = 'Country:';
$_['account_zipcode']                     = 'Zipcode:';
$_['button_change']                       = 'Change';
$_['button_save']                         = 'Save';