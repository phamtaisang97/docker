<?php
// Heading
$_['heading_title']                           = 'My orders';

// Order list
$_['order_title']                             = 'My orders';
$_['text_ordered_at']                         = 'Ordered at: ';
$_['text_amount']                             = 'Amount: ';

$_['text_into_money']                         = 'Into Money:';
$_['text_vat_included']                       = '(VAT included)';
$_['text_button_order']                       = 'Order';

$_['text_remove_alert_confirm']               = 'Do you want to remove this product ?';
$_['text_remove_fail']                        = 'Fail';
$_['text_remove_success']                     = 'Success';
