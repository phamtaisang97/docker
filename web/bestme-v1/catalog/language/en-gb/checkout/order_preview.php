<?php
// Heading
$_['heading_title']                         = 'My Orders';

// Order detail
$_['order_buyer_info']                      = 'Buyer information';
$_['order_buyer_name']                      = 'Full name';
$_['order_buyer_first_name']                = 'First name';
$_['order_buyer_last_name']                 = 'Last name';
$_['order_buyer_name_placeholder']          = 'Enter your full name';
$_['order_receiver_first_name_placeholder']  = 'Enter fist name';
$_['order_receiver_last_name_placeholder']  = 'Enter last name';
$_['order_buyer_email']                     = 'Email';
$_['order_buyer_email_placeholder']         = 'Enter your Email';
$_['order_buyer_phone_number']              = 'Phone number';
$_['order_buyer_phone_number_placeholder']  = 'Enter your phone number';
$_['order_buyer_password']                  = 'Password';
$_['order_buyer_password_placeholder']      = 'Password';

// Order receiver
$_['order_receiver_address']                    = 'Delivery address';
$_['order_receiver_name']                       = 'Full name';
$_['order_receiver_first_name']                 = 'First name';
$_['order_receiver_last_name']                  = 'Last name';
$_['order_receiver_name_placeholder']           = 'Enter your full name';
$_['order_receiver_first_name_placeholder']      = 'Enter first name';
$_['order_receiver_last_name_placeholder']      = 'Enter last name';
$_['order_receiver_phone_number']               = 'Phone number';
$_['order_receiver_phone_number_placeholder']   = 'Enter phone number';
$_['order_receiver_province']                   = 'Province/City';
$_['order_receiver_province_select']            = '-- Select --';
$_['order_receiver_district']                   = 'District';
$_['order_receiver_district_select']            = '-- Select --';
$_['order_receiver_ward']                       = 'Ward';
$_['order_receiver_ward_select']                = '-- Select --';
$_['order_receiver_address']                    = 'Address';
$_['order_receiver_address_placeholder']        = 'Enter street name/number';

// order button
$_['order_button_order']                    = 'Order';
$_['order_button_order_help_text']          = '(Please check the Order carefully before click Order)';

// Order final
$_['order_summary_template']                = 'Order (%d products)';
$_['order_product_unit']                    = 'products';
$_['order_edit']                            = 'Edit';
$_['order_temp_total']                      = 'Temporary Total';
$_['order_transport_fee']                   = 'Transport Fee';
$_['order_total']                           = 'Total';
$_['order_help_text_1']                     = '- Payment with cash when delivered';
$_['order_help_text_2']                     = '- The goods will be delivered in one week';
$_['cannot_stop_unique_payment_method']                     = 'You cannot deactivate the unique payment method';

$_['text_note_placeholder']                     = 'Note';