<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_all_category']  = 'All Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';
$_['text_slogan']        = 'Shopping with love';
$_['text_shop_dept']     = 'Shop By Department';
$_['text_blogs']     = 'Blogs';

// menu when logged in
$_['menu_item_account_dashboard']    = 'Account dashboard';
$_['menu_item_my_orders']            = 'My orders';
$_['menu_item_history']              = 'History';