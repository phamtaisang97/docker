<?php

$_['text_success']     = 'Success!';
$_['text_error']       = 'Error!';
$_['text_error_choose_service']       = 'Error selecting service!';
$_['text_error_unknown']       = 'An unknown error!';
$_['ghn']         = "Fast delivery";
$_['viettel_post']         = "ViettelPost";
$_['token_error'] = "Account error you are not connected or the account is incorrect.";

$_['update_hub_error'] = "Update map delivery address got error. Please try again later or check OTP (if has)";