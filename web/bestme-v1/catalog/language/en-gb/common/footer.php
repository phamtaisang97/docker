<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Certificates';
$_['text_affiliate']    = 'Affiliate';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Powered By <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';
$_['text_phone']        = 'Số điện thoại: ';
$_['text_address']      = 'Địa chỉ: ';
$_['show_more']         = 'Show More';
$_['show_less']         = 'Show Less';
$_['txt_registration_email']          = 'Don\'t miss thousand products and super attractive sale off!';

// Email subscribe
$_['subscribe_email_subject']            = '[' . PRODUCTION_BRAND_NAME_UPPERCASE . '] Subscribe from %s';
$_['subscribe_email_body_text']          = 'Dear website owner. You\'ve received a subscribe info from customer. Detail:' . PHP_EOL .
                                           '- Email: %s' . PHP_EOL . PHP_EOL .
                                           'Thank you for using ' . PRODUCTION_BRAND_NAME . ' !' . PHP_EOL .
                                           'Best regards,' . PHP_EOL .
                                           '' . PRODUCTION_BRAND_NAME . ' team' . PHP_EOL;

$_['subscribe_email_invalid']            = 'Email invalid';
$_['subscribe_email_sent_ok']            = 'Email sent successfully. Thank you for subscribing our website.';
$_['subscribe_email_sent_failed']        = 'Sending email got error, please try again later.';