<?php
// Heading
$_['heading_title']                             = 'Delivery setting';

// Text
$_['breadcrumb_setting']                        = 'Setting';
$_['breadcrumb_delivery_setting']               = 'Setting delivery';

$_['delivery_setting_title']                    = 'Delivery';

$_['text_success']                              = 'Update info successfully!';

$_['trans_ons_ghtk']                                   = 'Giao hàng tiết kiệm';
$_['not_login']                                        = 'Not yet logged in';
$_['not_map_address']                                  = 'Not yet linked picking goods address';

$_['text_ghtk']                                        = 'Giao hàng tiết kiệm';
$_['display_name']                                     = 'Giao hàng tiết kiệm';
$_['text_viettelpost']                                 = 'ViettelPost';
$_['text_shipping_unit']                               = 'Shipping unit';
$_['text_bestme_connection_with_shipping_partners']    = 'Connecting ' . PRODUCTION_BRAND_NAME . ' with shipping partners helps you create bills of lading and manage more conveniently.';
$_['text_fast_delivery_connection']                    = 'Connect to Giao hàng tiết kiệm ';
$_['text_after_processing_the_order_on_Bestme']        = 'After processing your order on ' . PRODUCTION_BRAND_NAME . ' , the system will automatically create a waybill on Express Delivery.';
$_['text_connected']                                   = 'Already connected';
$_['text_see_the_instructions']                        = 'Tutorial';
$_['text_connect']                                     = 'Connect';
$_['text_create_new_account']                          = 'Create New Account';
$_['text_collection_of_goods']                         = 'Picking Goods Addresses';
$_['text_cancel_collection']                           = 'Disconnect';
$_['text_user_name_ghtk']                              = 'GHTK Account (Email/Number Phone)';
$_['text_password']                                    = 'Password';
$_['text_button_connect']                              = 'Connect';
$_['text_button_cancel']                               = 'Cancel';
$_['text_disconnecting_from']                          = 'Disconnect';
$_['text_after_canceling_the_connection']              = 'After disconnecting, website will not be able to create Order on';
$_['text_are_you_sure']                                = 'Are you sure you want to Disconnect';
$_['text_close']                                       = 'Cancel';
$_['text_destroy']                                     = 'Disconnect';
$_['text_link_location']                               = 'Mapping the Picking Goods Addresses';
$_['text_address_config']                              = 'Address setting';

// error
$_['text_error_account_info_invalid']                         = 'Account info invalid. Please check again.';

/* webhook status code and message */
$_['ghtk_code_-1']            = "Cancel Order";
$_['ghtk_code_1']             =	"Pending";
$_['ghtk_code_2']             =	"Order has been received";
$_['ghtk_code_3']             =	"Have been picked up/stored";
$_['ghtk_code_4']             =	"Have coordinated delivery/delivering";
$_['ghtk_code_5']             =	"Have been delivered/not yet cross-checked";
$_['ghtk_code_6']             =	"Cross checked";
$_['ghtk_code_7']             =	"Not picked";
$_['ghtk_code_8']             =	"Pending pick up the goods";
$_['ghtk_code_9']             =	"Not delivered";
$_['ghtk_code_10']            =	"Delay";
$_['ghtk_code_11']            =	"Cross-checked debt payment";
$_['ghtk_code_12']            =	"Have coordinated pick up/Picking up";
$_['ghtk_code_13']            =	"Refund order";
$_['ghtk_code_20']            =	"Returning goods (COD is picking up the goods to return)";
$_['ghtk_code_21']            =	"Returned goods (COD was returned the goods)";
$_['ghtk_code_123']           =	"Shipper notes got goods";
$_['ghtk_code_127']           =	"Shipper (staff get/deliver) notes could not get goods";
$_['ghtk_code_128']           =	"Shipper notes delay getting goods";
$_['ghtk_code_45']            =	"Shipper notes delivered";
$_['ghtk_code_49']            =	"Shipper notes could not delivered";
$_['ghtk_code_410']           =	"Shipper notes delay delivering";