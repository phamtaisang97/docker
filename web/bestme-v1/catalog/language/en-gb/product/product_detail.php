<?php
// product title
$_['product_detail_title']                  = 'Product';
$_['text_error_not_found']                  = 'Error: The product does not exist!';

// product detail
$_['text_product_type']                     = 'Type: ';
$_['text_product_branch']                   = 'Branch: ';
$_['text_product_sku']                      = 'SKU: ';
$_['text_product_rating']                   = 'Rating: ';
$_['text_product_status']                   = 'Status: ';
$_['text_vat_hint']                         = '(Not include VAT)';
$_['text_quantity']                         = 'Quantity: ';
$_['text_button_add_to_cart']               = 'ADD TO CART';

// related products
$_['text_related_products']                 = 'Related Products';
// hot products
$_['text_hot_products']                     = 'Upsell Products';

$_['contact']                              = 'Contact';
$_['not_existed_product_version']                              = 'Not existed product version';