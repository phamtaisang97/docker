<?php
class ModelLocalisationCurrency extends Model {
	public function getCurrencyByCode($currency) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "currency WHERE code = '" . $this->db->escape($currency) . "'");

		return $query->row;
	}

	public function getCurrencies() {
	    $sql = "SELECT * FROM " . DB_PREFIX . "currency ORDER BY title ASC";

        $cache_key = DB_PREFIX . 'currency:getCurrencies_catalog_' . md5($sql);
        $cache_currency = $this->cache->get($cache_key);

        if ($cache_currency) {
            $currency_data = $cache_currency;
        } else {
            $query = $this->db->query($sql);
            $currency_data = $query->rows;
            $this->cache->set($cache_key, $currency_data);
        }

        $result = [];
        foreach ($currency_data as $currency) {
            $result[$currency['code']] = array(
                'currency_id'   => $currency['currency_id'],
                'title'         => $currency['title'],
                'code'          => $currency['code'],
                'symbol_left'   => $currency['symbol_left'],
                'symbol_right'  => $currency['symbol_right'],
                'decimal_place' => $currency['decimal_place'],
                'value'         => $currency['value'],
                'status'        => $currency['status'],
                'date_modified' => $currency['date_modified']
            );
        }

		return $result;
	}
}