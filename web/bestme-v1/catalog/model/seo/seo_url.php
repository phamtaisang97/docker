<?php
class ModelSeoSeoUrl extends Model {
    public function getSeoUrls($data = []) {
        $sql = $this->getSeoUrlsSql($data);
        $get_all = true;

        if (isset($data['start']) || isset($data['limit'])) {
            $data['start'] = isset($data['start']) && $data['start'] > 0 ? $data['start'] : 0;
            $data['limit'] = isset($data['limit']) && $data['start'] > 1 ? $data['limit'] : 1;

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];

            $get_all = $data['limit'] > 1;
        }

        $query = $this->db->query($sql);

        return $get_all ? $query->rows : $query->row;
    }

    private function getSeoUrlsSql($data) {
        $language_id = (int)$this->config->get('config_language_id');
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}seo_url` WHERE `language_id` = '{$language_id}'";

        if (!empty($data['filter_query'])) {
            $sql .= " AND `query` = '" . $this->db->escape($data['filter_query']) . "'";
        }

        if (!empty($data['filter_keyword'])) {
            $sql .= " AND `keyword` = '" . $this->db->escape($data['filter_keyword']) . "'";
        }

        if (isset($data['filter_store_id']) && $data['filter_store_id'] !== '') {
            $sql .= " AND `store_id` = '" . (int)$data['filter_store_id'] . "'";
        }

        $sort_data = array(
            'query',
            'keyword',
            'language_id',
            'store_id'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY `query`";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        return $sql;
    }
}