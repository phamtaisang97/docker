<?php

class ModelToolImage extends Model
{
    const DIR_ROOT_IMAGE = DIR_IMAGE . '../';

    public function resize($url, $width, $height)
    {
        if (strlen(str_replace(' ', '', $url)) == 0) {
            return null;
        }
        if ($width == $height && $height < 600) {
            $width = $height = 600;  //
        } else {
            if (is_file(DIR_IMAGE . $url)) {
                if ($this->request->server['HTTPS']) {
                    return $this->config->get('config_ssl') . 'image/' . $url;
                } else {
                    return $this->config->get('config_url') . 'image/' . $url;
                }
            } else {
                return $url;
            }
        }
        if (!$this->imageIsExist($url)) {
            $result = $this->resizeOnLocal($url, $width, $height);
            if ($result == null) {
                $result = $this->resizeOnLocal('no_image.png', $width, $height);
            }
            return $result;
        }
        $url_new = $this->genarateNewLink($url, $width, $height);

        if (!$this->imageIsExist($url_new)) {
            return $url;
        }

        return $url_new;
    }

    public function resizeOnLocal($filename, $width, $height)
    {
        if (!is_file(DIR_IMAGE . $filename) || substr(str_replace('\\', '/', realpath(DIR_IMAGE . $filename)), 0, strlen(DIR_IMAGE)) != str_replace('\\', '/', DIR_IMAGE)) {
            if (!is_file(self::DIR_ROOT_IMAGE . $filename) || substr(str_replace('\\', '/', realpath(self::DIR_ROOT_IMAGE . $filename)), 0, strlen(self::DIR_ROOT_IMAGE)) != str_replace('\\', '/', self::DIR_ROOT_IMAGE)) {
                return '/image/no_image.png';
            }else{
                $DIR_IMAGE = self::DIR_ROOT_IMAGE;
            }
        }else{
            $DIR_IMAGE = DIR_IMAGE;
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $image_old = $filename;
        $image_new = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . (int)$width . 'x' . (int)$height . '.' . $extension;

        if (!is_file($DIR_IMAGE . $image_new) || (filemtime($DIR_IMAGE . $image_old) > filemtime($DIR_IMAGE . $image_new))) {
            list($width_orig, $height_orig, $image_type) = getimagesize($DIR_IMAGE . $image_old);

            if (!in_array($image_type, array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF))) {
                return $DIR_IMAGE . $image_old;
            }

            $path = '';

            $directories = explode('/', dirname($image_new));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!is_dir($DIR_IMAGE . $path)) {
                    @mkdir($DIR_IMAGE . $path, 0777);
                }
            }

            if ($width_orig != $width || $height_orig != $height) {
                $image = new Image($DIR_IMAGE . $image_old);
                $image->resize($width, $height);
                $image->save($DIR_IMAGE . $image_new);
            } else {
                copy($DIR_IMAGE . $image_old, $DIR_IMAGE . $image_new);
            }
        }

        $image_new = str_replace(' ', '%20', $image_new);  // fix bug when attach image on email (gmail.com). it is automatic changing space " " to +

        if ($this->request->server['HTTPS']) {
            return $this->config->get('config_ssl') . 'image/' . $image_new;
        } else {
            return $this->config->get('config_url') . 'image/' . $image_new;
        }
    }

    public function uploadThumbnailImage($tmp_name, $imageName, $width, $height)
    {
        $width = $height = 600; //
        list($width_orig, $height_orig, $image_type) = getimagesize($tmp_name);

        if (!in_array($image_type, array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF))) {
            return false;
        }
        $extension = pathinfo($imageName, PATHINFO_EXTENSION);
        if ($width_orig != $width || $height_orig != $height) {
            $image = new Image($tmp_name);
            $image->resize($width, $height);
            $file = DIR_IMAGE . TEMPORY_IMAGE_PREFIX . $width . 'x' . $height . '.' . $extension;
            $image->save($file);
            if (is_file($file)) {
                return $file;
            }
        }

        return false;
    }

    public function imageIsExist($url)
    {
        $file_headers = @get_headers($url);
        if (!$file_headers || strpos($file_headers[0], '404') !== false) {
            return false;
        } else {
            return true;
        }
    }

    public function genarateNewLink($old_url, $width, $height)
    {
        $arrLink = explode('/', $old_url);
        array_splice($arrLink, count($arrLink) - 1, 0, $width . 'x' . $height);
        $newLink = implode('/', $arrLink);

        return $newLink;
    }
}
