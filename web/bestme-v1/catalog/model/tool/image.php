<?php

class ModelToolImage extends Model
{
    const DIR_ROOT_IMAGE = DIR_IMAGE . '../';

    public function resize($url, $width, $height)
    {
        if (trim($url) == '') {
            return '';
        }

        if ($url == 'no_image.png') {
            return '/image/no_image.png';
        }

        if ($url == 'placeholder.png') {
            return '/image/placeholder.png';
        }

        $url_new = $this->genarateNewLink($url, $width, $height);

        return $url_new;
    }

    public function imageIsExist($url)
    {
        $file_headers = @get_headers($url);
        if (!$file_headers || strpos($file_headers[0], '404') !== false) {
            return false;
        } else {
            return true;
        }
    }

    public function genarateNewLink($old_url, $width, $height)
    {
        $ext = pathinfo($old_url, PATHINFO_EXTENSION); // file extension
        if ($ext == 'ico') {
            return $old_url;
        }

        $image_server_name = $this->getServerFromImage($old_url);
        if ($image_server_name) {
            $image_server = Image_Server::getImageServer($image_server_name);
            if ($image_server) {
                $newLink = $image_server->resizeImage($old_url, $width, $height);
            } else {
                $newLink = $old_url;
            }

            // only for cloudinary image
            $newLink = cloudinary_change_http_to_https($newLink);
        }else{
            $newLink = $old_url;
        }

        return $newLink;
    }

    function getServerFromImage($url)
    {
        if (strpos($url, 'http://res.cloudinary.com') === 0 || strpos($url, 'https://res.cloudinary.com') === 0) {
            return 'cloudinary';
        } else if (strpos($url, BESTME_IMAGE_SEVER_SERVE_URL) === 0) {
            return 'bestme';
        } else {
            return false;
        }
    }

    // REMOVE LATER
    function cloudinary_resize($url, $width, $height)
    {
        /* temp disable resize image due to Cloudinary out of credits. TODO: restore credits and remove 1 below code line */
        return $url;

        if (strpos($url, 'http://res.cloudinary.com') === 0 || strpos($url, 'https://res.cloudinary.com') === 0) {
            $arrLink = explode('/', $url);
            //$scale = 'c_fill_pad,g_auto,b_auto,w_' . $width . ',h_' . $height; // original
            //$scale = 'f_auto,q_auto,dpr_auto,w_' . $width; // for optimizing image size. TODO: later, after all themes image responsive fixed!...
            /*
             * old with "c_fill_pad,g_auto,b_auto" made image gets truncated, e.g real wide-image 2400x800px
             * $scale = 'f_auto,q_auto,dpr_auto,c_fill_pad,g_auto,b_auto,w_' . $width . ',h_' . $height;
             *
             * new without above: working perfectly. Testing...
             */
            $scale = 'f_auto,q_auto,dpr_auto,c_pad,w_' . $width . ',h_' . $height;
            array_splice($arrLink, (int)array_search('upload', $arrLink) + 1, 0, array($scale));
            $url = implode('/', $arrLink);
        }

        return $url;
    }

    function cloudinary_auto_optimize($url)
    {
        /* temp disable resize image due to Cloudinary out of credits. TODO: restore credits and remove 1 below code line */
        return $url;

        if (strpos($url, 'http://res.cloudinary.com') === 0 || strpos($url, 'https://res.cloudinary.com') === 0) {
            $arrLink = explode('/', $url);
            $scale = 'f_auto,q_auto,dpr_auto';
            array_splice($arrLink, (int)array_search('upload', $arrLink) + 1, 0, array($scale));
            $url = implode('/', $arrLink);
        }

        return $url;
    }
}
