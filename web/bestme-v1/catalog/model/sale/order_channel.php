<?php

class ModelSaleOrderChannel extends Model
{
    /**
     * @param $order_id
     * @param $channel_id
     */
    public function addOrderChannel($order_id, $channel_id)
    {
        try {
            if (!$order_id || !$channel_id) {
                return;
            }

            if ($this->exists($order_id, $channel_id)) {
                return;
            }

            $DB_PREFIX = DB_PREFIX;
            $this->db->query("INSERT INTO `{$DB_PREFIX}order_channel` 
                                SET `order_id` = '" . (int)$order_id . "', `channel_id` = '" . $this->db->escape($channel_id) . "'");
        } catch (Exception $e) {
            $this->log->write('addOrderChannel(): resp: ' . $e->getMessage());
        }
    }

    private function exists($order_id, $channel_id)
    {
        $DB_PREFIX = DB_PREFIX;

        $result = $this->db->query("SELECT `order_id` FROM `{$DB_PREFIX}order_channel` WHERE `order_id` = {$order_id} AND `channel_id` = '" . $this->db->escape($channel_id) . "'");

        if (isset($result->row['order_id']) && !empty($result->row['order_id'])) {
            return true;
        }

        return false;
    }
}
