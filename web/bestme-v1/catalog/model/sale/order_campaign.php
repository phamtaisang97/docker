<?php

class ModelSaleOrderCampaign extends Model
{
    /**
     * @param $order_id
     * @param $campaign_id
     */
    public function addOrderCampaign($order_id, $campaign_id)
    {
        try {
            if (!$order_id || !$campaign_id) {
                return;
            }

            if ($this->exists($order_id, $campaign_id)) {
                return;
            }

            $DB_PREFIX = DB_PREFIX;
            $this->db->query("INSERT INTO `{$DB_PREFIX}order_campaign` 
                                SET `order_id` = '" . (int)$order_id . "', `campaign_id` = '" . $this->db->escape($campaign_id) . "'");
        } catch (Exception $e) {
            $this->log->write('addOrderCampaign(): resp: ' . $e->getMessage());
        }

    }

    private function exists($order_id, $campaign_id)
    {
        $DB_PREFIX = DB_PREFIX;

        $result = $this->db->query("SELECT `order_id` FROM `{$DB_PREFIX}order_campaign` WHERE `order_id` = {$order_id} AND `campaign_id` = '" . $this->db->escape($campaign_id) . "'");

        if (isset($result->row['order_id']) && !empty($result->row['order_id'])) {
            return true;
        }

        return false;
    }
}
