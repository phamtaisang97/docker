<?php

class ModelSaleCampaignVoucher extends Model
{
    /**
     *
     * @param $order_id
     * @param array $voucher format as
     *     [
     *          'voucher_code' => 'ABCDEFGH99',
     *          'voucher_type' => 1, //1:  percent / 2: amount
     *          'amount' => 20000,
     *          'products' => [
     *              [
     *                  'id' => 83,
     *                  'product_version_id' => null
     *              ],
     *              [
     *                  'id' => 104,
     *                  'product_version_id' => 1
     *              ]
     *           ],
     *           'start_at' => "2021-04-22 13:41:00",
     *           'end_at' => "2021-04-22 13:41:59",
     *           'user_create_id' => "10"
     *      ]
     *
     * @return int
     */
    public function addCampaignVoucher($order_id, array $voucher)
    {
        if (!$order_id || empty($voucher)) {
            return 0;
        }

        $campaign_voucher_id = 0;
        // build list for updating
        if (isset($voucher['voucher_code']) ||
            isset($voucher['voucher_type']) ||
            isset($voucher['amount']) ||
            isset($voucher['products']) ||
            isset($voucher['start_at']) ||
            isset($voucher['end_at']) ||
            isset($voucher['user_create_id'])
        ) {
            if ($this->getCampaignVoucherIdCodeAndAmount($order_id, $voucher['voucher_code'], $voucher['amount'])) {
                $campaign_voucher_id = $this->getCampaignVoucherIdCodeAndAmount($order_id, $voucher['voucher_code'], $voucher['amount']);
            } else {
                $voucher['start_at'] = isset($voucher['start_at']) ? $voucher['start_at'] : '';
                $voucher['end_at'] = isset($voucher['end_at']) ? $voucher['end_at'] : '';
                $voucher['user_create_id'] = isset($voucher['user_create_id']) ? $voucher['user_create_id'] : 1;

                $campaign_voucher_id = $this->add($order_id, $voucher);
            }
        }

        return $campaign_voucher_id;
    }

    private function add($order_id, $voucher)
    {
        $DB_PREFIX = DB_PREFIX;
        $this->db->query("INSERT INTO `{$DB_PREFIX}campaign_voucher` 
                                SET `voucher_code` = '". $voucher['voucher_code'] ."', 
                                    `voucher_type` = {$voucher['voucher_type']}, 
                                    `order_id` = {$order_id}, 
                                    `amount` = {$voucher['amount']}, 
                                    `user_create_id` = {$voucher['user_create_id']}, 
                                    `start_at` = '". $voucher['start_at']."', 
                                    `end_at` = '". $voucher['end_at'] ."', 
                                    `date_added` = NOW(), 
                                    `date_modified` = NOW()
                                ");

        $campaign_voucher_id = $this->db->getLastId();
        return $campaign_voucher_id;
    }

    private function getCampaignVoucherIdCodeAndAmount($order_id, $code, $amount)
    {
        if (!isset($order_id) || empty($order_id)) {
            return 0;
        }

        if (!isset($code) || empty($code)) {
            return 0;
        }

        $DB_PREFIX = DB_PREFIX;
        $amount = (float)$amount;

        $result = $this->db->query("SELECT `campaign_voucher_id` FROM `{$DB_PREFIX}campaign_voucher` 
                                      WHERE `order_id` = '". $order_id ."' AND `voucher_code` = '". $code . "' AND `amount` = {$amount}");

        if (isset($result->row['campaign_voucher_id']) && !empty($result->row['campaign_voucher_id'])) {
            return $result->row['campaign_voucher_id'];
        }

        return 0;
    }
}
