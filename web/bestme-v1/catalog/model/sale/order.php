<?php

class ModelSaleOrder extends Model
{
    use Sync_Bestme_Data_To_Welcome_Util;
    use AUTO_CREATE_RECEIPT_VOUCHER_UTIL_TRAIT;

    const ORDER_STATUS_ID_DRAFT = 6;
    const ORDER_STATUS_ID_PROCESSING = 7;
    const ORDER_STATUS_ID_DELIVERING = 8;
    const ORDER_STATUS_ID_COMPLETED = 9;
    const ORDER_STATUS_ID_CANCELLED = 10;
    const ORDER_STATUS_ID_DELETED = -1;

    // voucher
    const VOUCHER_APPLY_FOR_ALL_ORDER = 1;
    const VOUCHER_APPLY_FOR_VALUE_ORDER = 2;
    const VOUCHER_APPLY_FOR_PRODUCT = 3;

    const VOUCHER_APPLICABLE_TYPE_QUANTITY_PRODUCT = 1;
    const VOUCHER_APPLICABLE_TYPE_TOTAL_ORDER = 2;

    const VOUCHER_PRODUCT_ONE_ON_ORDER = 1;
    const VOUCHER_PRODUCT_QUANTITY_ON_ORDER = 2;

    const VOUCHER_TYPE_AMOUNT = 1;
    const VOUCHER_TYPE_PERCENT = 2;

    const ORDER_TAG_NEW_PREFIX = 'NewTag_';

    const VNP_APP_CODE = 'paym_ons_vnpay';

    use RabbitMq_Trait;

    public function getOrder($order_id)
    {
        $order_query = $this->db->query("SELECT *, (SELECT CONCAT(c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = o.customer_id) AS customer, (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");

        if ($order_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

            if ($country_query->num_rows) {
                $payment_iso_code_2 = $country_query->row['iso_code_2'];
                $payment_iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $payment_iso_code_2 = '';
                $payment_iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

            if ($zone_query->num_rows) {
                $payment_zone_code = $zone_query->row['code'];
            } else {
                $payment_zone_code = '';
            }

            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

            if ($country_query->num_rows) {
                $shipping_iso_code_2 = $country_query->row['iso_code_2'];
                $shipping_iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $shipping_iso_code_2 = '';
                $shipping_iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

            if ($zone_query->num_rows) {
                $shipping_zone_code = $zone_query->row['code'];
            } else {
                $shipping_zone_code = '';
            }

            $reward = 0;

            $order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

            foreach ($order_product_query->rows as $product) {
                $reward += $product['reward'];
            }

            $this->load->model('customer/customer');

            $affiliate_info = $this->model_customer_customer->getCustomer($order_query->row['affiliate_id']);

            if ($affiliate_info) {
                $affiliate_firstname = $affiliate_info['firstname'];
                $affiliate_lastname = $affiliate_info['lastname'];
            } else {
                $affiliate_firstname = '';
                $affiliate_lastname = '';
            }

            $this->load->model('localisation/language');

            $language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

            if ($language_info) {
                $language_code = $language_info['code'];
            } else {
                $language_code = $this->config->get('config_language');
            }

            return array(
                'order_id' => $order_query->row['order_id'],
                'order_code' => $order_query->row['order_code'],
                'invoice_no' => $order_query->row['invoice_no'],
                'invoice_prefix' => $order_query->row['invoice_prefix'],
                'store_id' => $order_query->row['store_id'],
                'store_name' => $order_query->row['store_name'],
                'store_url' => $order_query->row['store_url'],
                'customer_id' => $order_query->row['customer_id'],
                'customer' => $order_query->row['customer'],
                'customer_group_id' => $order_query->row['customer_group_id'],
                'firstname' => $order_query->row['firstname'],
                'lastname' => $order_query->row['lastname'],
                'fullname' => $order_query->row['fullname'],
                'order_payment' => $order_query->row['order_payment'],
                'order_transfer' => $order_query->row['order_transfer'],
                'email' => $order_query->row['email'],
                'telephone' => $order_query->row['telephone'],
                'custom_field' => json_decode($order_query->row['custom_field'], true),
                'payment_firstname' => $order_query->row['payment_firstname'],
                'payment_lastname' => $order_query->row['payment_lastname'],
                'payment_company' => $order_query->row['payment_company'],
                'payment_address_1' => $order_query->row['payment_address_1'],
                'payment_address_2' => $order_query->row['payment_address_2'],
                'payment_postcode' => $order_query->row['payment_postcode'],
                'payment_city' => $order_query->row['payment_city'],
                'payment_zone_id' => $order_query->row['payment_zone_id'],
                'payment_zone' => $order_query->row['payment_zone'],
                'payment_zone_code' => $payment_zone_code,
                'payment_country_id' => $order_query->row['payment_country_id'],
                'payment_country' => $order_query->row['payment_country'],
                'payment_iso_code_2' => $payment_iso_code_2,
                'payment_iso_code_3' => $payment_iso_code_3,
                'payment_address_format' => $order_query->row['payment_address_format'],
                'payment_custom_field' => json_decode($order_query->row['payment_custom_field'], true),
                'payment_method' => $order_query->row['payment_method'],
                'payment_code' => $order_query->row['payment_code'],
                'payment_status' => $order_query->row['payment_status'],
                'shipping_firstname' => $order_query->row['shipping_firstname'],
                'shipping_lastname' => $order_query->row['shipping_lastname'],
                'shipping_company' => $order_query->row['shipping_company'],
                'shipping_address_1' => $order_query->row['shipping_address_1'],
                'shipping_address_2' => $order_query->row['shipping_address_2'],
                'shipping_postcode' => $order_query->row['shipping_postcode'],
                'shipping_city' => $order_query->row['shipping_city'],
                'shipping_province_code' => $order_query->row['shipping_province_code'],
                'shipping_district_code' => $order_query->row['shipping_district_code'],
                'shipping_ward_code' => $order_query->row['shipping_ward_code'],
                'shipping_zone_id' => $order_query->row['shipping_zone_id'],
                'shipping_zone' => $order_query->row['shipping_zone'],
                'shipping_zone_code' => $shipping_zone_code,
                'shipping_country_id' => $order_query->row['shipping_country_id'],
                'shipping_country' => $order_query->row['shipping_country'],
                'shipping_iso_code_2' => $shipping_iso_code_2,
                'shipping_iso_code_3' => $shipping_iso_code_3,
                'shipping_address_format' => $order_query->row['shipping_address_format'],
                'shipping_custom_field' => json_decode($order_query->row['shipping_custom_field'], true),
                'shipping_method' => $order_query->row['shipping_method'],
                'shipping_fee' => $order_query->row['shipping_fee'],
                'shipping_code' => $order_query->row['shipping_code'],
                'comment' => $order_query->row['comment'],
                'order_cancel_comment' => $order_query->row['order_cancel_comment'],
                'total' => $order_query->row['total'],
                'discount' => $order_query->row['discount'],
                'reward' => $reward,
                'order_status_id' => $order_query->row['order_status_id'],
                'order_status' => $order_query->row['order_status'],
                'previous_order_status_id' => $order_query->row['previous_order_status_id'],
                'affiliate_id' => $order_query->row['affiliate_id'],
                'affiliate_firstname' => $affiliate_firstname,
                'affiliate_lastname' => $affiliate_lastname,
                'commission' => $order_query->row['commission'],
                'language_id' => $order_query->row['language_id'],
                'language_code' => $language_code,
                'currency_id' => $order_query->row['currency_id'],
                'currency_code' => $order_query->row['currency_code'],
                'currency_value' => $order_query->row['currency_value'],
                'ip' => $order_query->row['ip'],
                'forwarded_ip' => $order_query->row['forwarded_ip'],
                'user_agent' => $order_query->row['user_agent'],
                'accept_language' => $order_query->row['accept_language'],
                'date_added' => $order_query->row['date_added'],
                'user_id' => $order_query->row['user_id'],
                'source' => $order_query->row['source'],
                'from_domain' => $order_query->row['from_domain'],
                'date_modified' => $order_query->row['date_modified']
            );
        } else {
            return;
        }
    }

    public function getOrders($data = array())
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "order` ";
        $statusArray = array_keys($this->getListStatus());
        $array = implode(",", $statusArray);
        // var_dump($array);die;
        if (!empty($data['order_status'])) {
            $sql .= "WHERE order_status = '" . (int)$data['order_status'] . "' ";
        } else {
            $sql .= "WHERE order_status IN (" . $array . ")";
        }

        if (!empty($data['order_payment'])) {
            $sql .= " AND order_payment = '" . (int)$data['order_payment'] . "' ";
        }
        if (!empty($data['order_transfer'])) {
            $sql .= " AND order_transfer = '" . (int)$data['order_transfer'] . "' ";
        }
        if (!empty($data['order_search_text'])) {
            $sql .= " AND order_id = '" . $data['order_search_text'] . "' ";
        }

        $sql .= "ORDER BY order_id";
        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getOrdersByCustomerId($customerId, $data = array())
    {
        $sql = "SELECT o.order_id as order_id, o.order_code, o.date_added, o.total,o.payment_method, o.payment_status, o.order_status_id, os.name, o.transport_order_code, o.transport_name, o.transport_status FROM " . DB_PREFIX . "order o ";
        $sql .= " left join " . DB_PREFIX . "order_status as os ON (o.order_status_id = os.order_status_id) ";
        $sql .= " WHERE customer_id = " . $customerId;
        $sql .= " AND  payment_status != 3";
        $statusArray = array_keys($this->getListStatus());
        $array = implode(",", $statusArray);
        // var_dump($array);die;

        if (!empty($data['order_payment'])) {
            $sql .= " AND order_payment = '" . (int)$data['order_payment'] . "' ";
        }
        if (!empty($data['order_transfer'])) {
            $sql .= " AND order_transfer = '" . (int)$data['order_transfer'] . "' ";
        }
        if (!empty($data['order_search_text'])) {
            $sql .= " AND order_id = '" . $data['order_search_text'] . "' ";
        }

        $sql .= " ORDER BY date_added";
        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " DESC";
        }
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getLastestOrders($data = ['start' => 0, 'limit' => 10])
    {
        $sql = "SELECT `order_id`, `fullname`, `order_status`, `total`, `date_added` FROM `" . DB_PREFIX . "order` ";
        $sql .= "ORDER BY date_added DESC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }


    public function getLastestOrdersBydate($data)
    {
        $data['filter_date_added_end'] = date("Y-m-d", strtotime($data['filter_date_added_end'] . ' + 1 days')); /// compare dateTime < date
        $sql = "SELECT `date_added`, `total` FROM `" . DB_PREFIX . "order` WHERE `date_added` >= DATE('" . $this->db->escape($data['filter_date_added_start']) . "') AND `date_added` < DATE('" . $this->db->escape($data['filter_date_added_end']) . "')";
        if (!empty($data['order_status'])) {
            $sql .= "AND order_status = '" . (int)$data['order_status'] . "' ";
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getOrderProducts($order_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

        return $query->rows;
    }

    public function getOrderOptions($order_id, $order_product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product_id . "'");

        return $query->rows;
    }

    public function getOrderVouchers($order_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

        return $query->rows;
    }

    public function getOrderVoucherByVoucherId($voucher_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_voucher` WHERE voucher_id = '" . (int)$voucher_id . "'");

        return $query->row;
    }

    public function getOrderTotals($order_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order");

        return $query->rows;
    }

    public function getTotalOrders($data = array())
    {
        $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order`";
        $statusArray = array_keys($this->getListStatus());
        $array = implode(",", $statusArray);
        if (!empty($data['order_status'])) {
            $sql .= "WHERE order_status = '" . (int)$data['order_status'] . "' ";
        } else {
            $sql .= "WHERE order_status IN (" . $array . ")";
        }

        if (!empty($data['order_payment'])) {
            $sql .= " AND order_payment = '" . (int)$data['order_payment'] . "' ";
        }
        if (!empty($data['order_transfer'])) {
            $sql .= " AND order_transfer = '" . (int)$data['order_transfer'] . "' ";
        }
        if (!empty($data['order_search_text'])) {
            $sql .= " AND order_id = '" . $data['order_search_text'] . "' ";
        }
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalOrdersByStoreId($store_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE store_id = '" . (int)$store_id . "'");

        return $query->row['total'];
    }

    public function getTotalOrdersByOrderStatusId($order_status_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE order_status_id = '" . (int)$order_status_id . "' AND order_status_id > '0'");

        return $query->row['total'];
    }

    public function getTotalOrdersByProcessingStatus()
    {
        $implode = array();

        $order_statuses = $this->config->get('config_processing_status');

        foreach ($order_statuses as $order_status_id) {
            $implode[] = "order_status_id = '" . (int)$order_status_id . "'";
        }

        if ($implode) {
            $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE " . implode(" OR ", $implode));

            return $query->row['total'];
        } else {
            return 0;
        }
    }

    public function getTotalOrdersByCompleteStatus()
    {
        $implode = array();

        $order_statuses = $this->config->get('config_complete_status');

        foreach ($order_statuses as $order_status_id) {
            $implode[] = "order_status_id = '" . (int)$order_status_id . "'";
        }

        if ($implode) {
            $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE " . implode(" OR ", $implode) . "");

            return $query->row['total'];
        } else {
            return 0;
        }
    }

    public function getTotalOrdersByLanguageId($language_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE language_id = '" . (int)$language_id . "' AND order_status_id > '0'");

        return $query->row['total'];
    }

    public function getTotalOrdersByCurrencyId($currency_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE currency_id = '" . (int)$currency_id . "' AND order_status_id > '0'");

        return $query->row['total'];
    }

    public function getTotalSales($data = array())
    {
        $sql = "SELECT SUM(total) AS total FROM `" . DB_PREFIX . "order`";

        if (!empty($data['filter_order_status'])) {
            $implode = array();

            $order_statuses = explode(',', $data['filter_order_status']);

            foreach ($order_statuses as $order_status_id) {
                $implode[] = "order_status_id = '" . (int)$order_status_id . "'";
            }

            if ($implode) {
                $sql .= " WHERE (" . implode(" OR ", $implode) . ")";
            }
        } elseif (isset($data['filter_order_status_id']) && $data['filter_order_status_id'] !== '') {
            $sql .= " WHERE order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
        } else {
            $sql .= " WHERE order_status_id > '0'";
        }

        if (!empty($data['filter_order_id'])) {
            $sql .= " AND order_id = '" . (int)$data['filter_order_id'] . "'";
        }

        if (!empty($data['filter_customer'])) {
            $sql .= " AND CONCAT(firstname, ' ', o.lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
        }

        if (!empty($data['filter_date_added'])) {
            $sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if (!empty($data['filter_date_modified'])) {
            $sql .= " AND DATE(date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
        }

        if (!empty($data['filter_total'])) {
            $sql .= " AND total = '" . (float)$data['filter_total'] . "'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function createInvoiceNo($order_id)
    {
        $order_info = $this->getOrder($order_id);

        if ($order_info && !$order_info['invoice_no']) {
            $query = $this->db->query("SELECT MAX(invoice_no) AS invoice_no FROM `" . DB_PREFIX . "order` WHERE invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "'");

            if ($query->row['invoice_no']) {
                $invoice_no = $query->row['invoice_no'] + 1;
            } else {
                $invoice_no = 1;
            }

            $this->db->query("UPDATE `" . DB_PREFIX . "order` SET invoice_no = '" . (int)$invoice_no . "', invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "' WHERE order_id = '" . (int)$order_id . "'");

            return $order_info['invoice_prefix'] . $invoice_no;
        }
    }

    public function getOrderHistories($order_id, $start = 0, $limit = 10)
    {
        if ($start < 0) {
            $start = 0;
        }

        if ($limit < 1) {
            $limit = 10;
        }

        $query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY oh.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

        return $query->rows;
    }

    public function getTotalOrderHistories($order_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_history WHERE order_id = '" . (int)$order_id . "'");

        return $query->row['total'];
    }

    public function getTotalOrderHistoriesByOrderStatusId($order_status_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_history WHERE order_status_id = '" . (int)$order_status_id . "'");

        return $query->row['total'];
    }

    public function getEmailsByProductsOrdered($products, $start, $end)
    {
        $implode = array();

        foreach ($products as $product_id) {
            $implode[] = "op.product_id = '" . (int)$product_id . "'";
        }

        $query = $this->db->query("SELECT DISTINCT email FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) WHERE (" . implode(" OR ", $implode) . ") AND o.order_status_id <> '0' LIMIT " . (int)$start . "," . (int)$end);

        return $query->rows;
    }

    public function getTotalEmailsByProductsOrdered($products)
    {
        $implode = array();

        foreach ($products as $product_id) {
            $implode[] = "op.product_id = '" . (int)$product_id . "'";
        }

        $query = $this->db->query("SELECT COUNT(DISTINCT email) AS total FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) WHERE (" . implode(" OR ", $implode) . ") AND o.order_status_id <> '0'");

        return $query->row['total'];
    }

    //tunglaso1
    public function getListStatus()
    {
        $data = array(
            1 => 'Nháp',
            2 => 'Hoàn thành',
            3 => 'Đang xử lý',
            4 => 'Không thành công',
            5 => 'Đã huỷ ',
        );
        return $data;
    }

    public function getListStatusExceptDraft()
    {
        $data = array(
            2 => 'Hoàn thành',
            3 => 'Đang xử lý',
            4 => 'Không thành công',
            5 => 'Đã huỷ ',
        );
        return $data;
    }

    public function getNameStatusOrder($status = null)
    {
        if (isset($status)) {
            $data = $this->getListStatus();
            if (isset($data[$status])) {
                return $data[$status];
            }
            return null;
        }
        return null;
    }

    public function getListPayment()
    {
        $data = array(
            1 => 'Đã thanh toán',
            2 => 'Thanh toán sau (COD)',
        );
        return $data;
    }

    public function getListShipment()
    {
        $data = array(
            1 => 'Chờ vận chuyển',
            2 => 'Đang vận chuyển',
            3 => 'Đang hoàn đơn ',
        );
        return $data;
    }

    public function getCustomerIdByPhone($data)
    {
        $phone = $data['telephone'];
        $sql = "SELECT * FROM `" . DB_PREFIX . "customer` WHERE telephone = '" . $phone . "'";
        $query = $this->db->query($sql);
        if (count($query->row) > 0) {
            return $query->row['customer_id'];
        }

        $customerData['firstname'] = $data['firstname'];
        $customerData['lastname'] = $data['lastname'];
        $customerData['email'] = $data['email'];
        $customerData['telephone'] = $data['telephone'];
        $customerData['customer_source'] = '';
        $customerData['customer_group_id'] = '';
        $customerData['note'] = '';
        $this->load->model('customer/customer');
        $customer_id = $this->model_customer_customer->addCustomer($customerData);

        return $customer_id;
    }

    public function addOrderCommon($data, $customerId = null)
    {
        $payment_method = isset($data['payment_method']) ? $data['payment_method'] : '';
        $pos_id_ref = isset($data['pos_id_ref']) ? $data['pos_id_ref'] : '';
        $store_id = isset($data['store_id']) ? $data['store_id'] : 0;
        $user_id = isset($data['user_id']) ? $data['user_id'] : 1;
        $now = new DateTime();
        $now = $now->format('Y-m-d H:i:s');
        $date_added = isset($data['created_date']) ? $data['created_date'] : $now;

        //$this->db->query("INSERT INTO " . DB_PREFIX . "order SET  firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', fullname = '" . $this->db->escape($data['fullname']) . "', telephone = '" . $data['telephone'] . "', order_status='" . $data['order_status'] . "', order_payment='" . $data['order_payment'] . "', order_transfer = '" . $data['order_transfer'] . "', comment = '" . $data['order_note'] . "',customer_id = '" . $customerId . "', total = '" . $data['total'] . "', date_added = NOW(), date_modified = NOW()");
        $this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET  
            customer_id = '" . (int)$customerId . "', 
            firstname = '" . $this->db->escape($data['firstname']) . "', 
            lastname = '" . $this->db->escape($data['lastname']) . "', 
            fullname = '" . $this->db->escape($data['fullname']) . "', 
            email = '" . $this->db->escape($data['email']) . "', 
            telephone = '" . $this->db->escape($data['telephone']) . "', 
            order_payment='" . $data['order_payment'] . "', 
            order_transfer = '" . $data['order_transfer'] . "', 
            comment = '" . $data['order_note'] . "', 
            payment_firstname = '" . $this->db->escape($data['payment_firstname']) . "', 
            payment_lastname = '" . $this->db->escape($data['payment_lastname']) . "', 
            payment_company = '" . $this->db->escape($data['payment_company']) . "', 
            payment_address_1 = '" . $this->db->escape($data['payment_address_1']) . "', 
            payment_address_2 = '" . $this->db->escape($data['payment_address_2']) . "', 
            payment_city = '" . $this->db->escape($data['payment_city']) . "', 
            payment_status = '" . $this->db->escape($data['payment_status']) . "', 
            payment_postcode = '" . $this->db->escape($data['payment_postcode']) . "', 
            payment_method = '" . $this->db->escape($payment_method) . "', 
            payment_code = '" . $this->db->escape($data['payment_code']) . "', 
            shipping_firstname = '" . $this->db->escape($data['shipping_firstname']) . "', 
            shipping_lastname = '" . $this->db->escape($data['shipping_lastname']) . "', 
            shipping_company = '" . $this->db->escape($data['shipping_company']) . "', 
            shipping_address_1 = '" . $this->db->escape($data['shipping_address_1']) . "', 
            shipping_address_2 = '" . $this->db->escape($data['shipping_address_2']) . "', 
            shipping_city = '" . $this->db->escape($data['shipping_city']) . "', 
            shipping_method = '" . $this->db->escape($data['shipping_method']) . "', 
            shipping_method_value = '" . $this->db->escape($data['shipping_method_value']) . "', 
            shipping_ward_code = '" . $this->db->escape($data['shipping_ward_code']) . "', 
            shipping_district_code = '" . $this->db->escape($data['shipping_district_code']) . "', 
            shipping_province_code = '" . $this->db->escape($data['shipping_province_code']) . "', 
            shipping_fee = '" . $this->db->escape($data['shipping_fee']) . "', 
            order_status_id = '" . $this->db->escape($data['order_status_id']) . "', 
            currency_code = '" . $this->db->escape($data['currency_code']) . "', 
            currency_value = '" . $this->currency->getValue($data['currency_code']) . "', 
            from_domain = '" . (isset($data['from_domain']) ? $this->db->escape($data['from_domain']) : '') . "', 
            currency_id = '" . $this->currency->getId($data['currency_code']) . "', 
            shipping_code = '" . $this->db->escape($data['shipping_code']) . "', 
            discount = '" . (isset($data['order-discount']) ? (float)(extract_number($data['order-discount'])) : 0) . "', 
            total = '" . (float)$data['total'] . "', 
            customer_ref_id = '" . (isset($data['customer_ref_id']) ? $this->db->escape($data['customer_ref_id']) : '') . "', 
            pos_id_ref = '" . $this->db->escape($pos_id_ref) . "', 
            store_id = '" . (int)$store_id . "', 
            user_id = '" . (int)$user_id . "', 
            date_added = '" . $this->db->escape($date_added) . "', 
            date_modified = NOW()"
        );

        // TODO: data from web contains discount value for order???

        $order_id = $this->db->getLastId();

        // Auto add or update receipt_voucher
        $customer_info = json_encode(['id' => (int)$customerId, 'name' => $data['fullname'] ]);

        $dataToSave = [
            'payment_status' =>   $data['payment_status'],
            'payment_method' =>   $payment_method,
            'total_pay'      =>   $data['total'],
            'store_id'       =>   (int)$store_id,
            'customer_info' => $customer_info
        ];
        $this->autoUpdateFromOrder($order_id, $dataToSave);
        // support tag
        if (isset($data['tags'])) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "order_tag WHERE order_id = '" . (int)$order_id . "'");

            /* notice: empty tags known as REMOVING ALL tags! */

            $tags = explode(',', $data['tags']);
            $tags = is_array($tags) ? $tags : [];
            $this->load->model('catalog/tag');
            foreach ($tags as $tag) {
                $tag = trim($tag);
                if (empty($tag)) {
                    continue;
                }

                $existing_tag = $this->model_catalog_tag->getTagByValue($tag);
                if (!$existing_tag) {
                    $sql_update = "INSERT INTO " . DB_PREFIX . "tag(`value`) 
                                   VALUES ('" . $this->db->escape($tag) . "')";
                    $this->db->query($sql_update);
                    $tag_id = $this->db->getLastId();
                } else {
                    $tag_id = $existing_tag['tag_id'];
                }

                $this->db->query("INSERT INTO " . DB_PREFIX . "order_tag 
                                      SET order_id = " . (int)$order_id . ", 
                                          tag_id = " . (int)$tag_id);
            }
        }

        // 20200420: support order utm
        if (isset($data['utm']) && !empty($data['utm'])) {
            $this->load->model('sale/order_utm');
            $this->model_sale_order_utm->addOrderUtm($order_id, $data['utm']);
        } elseif (isset($data['tracking_utm']) && !empty($data['tracking_utm'])) {
            $tracking_utm = $data['tracking_utm'];
            if (!empty($tracking_utm)) {
                parse_str(html_entity_decode($tracking_utm), $utmArr);

                $dataUtm = [];
                foreach ($utmArr as $key => $value) {
                    $dataUtm[] = [
                      'key' => $key,
                      'value' => $value
                    ];
                }

                $this->load->model('sale/order_utm');
                $this->model_sale_order_utm->addOrderUtm($order_id, $dataUtm);
            }
        }

        return $order_id;
    }

    public function addOrderCustom($data)
    {
        $order['fullname'] = $customer['fullname'] = $data['fullname'];
        $order['firstname'] = $customer['firstname'] = $data['firstname'];
        $order['lastname'] = $customer['lastname'] = $data['lastname'];
        $order['email'] = $customer['email'] = $data['email'];
        $order['telephone'] = $customer['telephone'] = $data['telephone'];
        $order['order_status'] = $data['order_status'];
        $customerId = $data['customer_id'];
        $orderId = $this->addOrderCommon($data, $customerId);
        return $orderId;
    }

    public function addOrderProduct($products, $orderId)
    {
        if (count($products) > 0) {
            foreach ($products as $product) {
                //insert order_product table
                $product_version_id = '';
                $discount_id = '';
                $listed_price = 0;
                $this->load->model('catalog/product');
                $product_detail = $this->model_catalog_product->getProduct($product['product_id']);
                if (isset($product['product_version'])) {
                    if ((int)$product['product_version']['price'] == 0) {
                        if (!empty($product['product_version']['discount_id'])) {
                            $product['product_version']['price'] = 0;
                            $discount_id = $product['product_version']['discount_id'];
                        } elseif (!empty($product['is_add_on_product'])) {
                            $product['product_version']['price'] = ((float)$product['product_version']['price'] == 0) ? 0 : (float)$product['product_version']['price'];
                        } else {
                            $product['product_version']['price'] = $product['product_version']['compare_price'];
                        }
                    }

                    $product['price'] = $product['product_version']['price'];
                    $product_version_id = $product['product_version']['product_version_id'];
                    $listed_price = $product['product_version']['compare_price'];
                } else {
                    if (!empty($product_detail['discount_id'])) {
                        $product['price'] = ((float)$product['price'] == 0) ? 0 : (float)$product['price'];
                        $discount_id = $product_detail['discount_id'];
                    } elseif (!empty($product['is_add_on_product'])) {
                        $product['price'] = ((float)$product['price'] == 0) ? 0 : (float)$product['price'];
                    } else {
                        $product['price'] = ((float)$product['price'] != 0) ? (float)$product['price'] : (float)$product_detail['compare_price_master'];
                    }

                    $listed_price = (float)$product_detail['compare_price_master'];
                }

                // TODO: data from web contains discount value for each product???
                $discount = 0;
                $campaign_voucher_id = isset($product['campaign_voucher_id']) ? (int)$product['campaign_voucher_id'] : null;

                // get deal_id in add-on deal
                $addOnDealId = null;
                if (!empty($product['is_add_on_product'])) {
                    $pv_id = !empty($product_version_id) ? $product_version_id : 0;
                    $p_id = $product['product_id'];
                    $addOnDealId = $this->getAddOnDealId($p_id, $pv_id);
                }
                // end

                $orderProductTotal = (int)$product['quantity'] * (float)$product['price'] - $discount;
                $sql = "
					INSERT INTO " . DB_PREFIX . "order_product 
					SET  order_id = '" . (int)$orderId . "', 
					product_id = '" . (int)$product['product_id'] . "', 
					product_version_id ='" . (int)$product_version_id . "', 
					name = '" . $this->db->escape($product['name']) . "', 
					model = '" . $product['model'] . "', 
					quantity = '" . (int)$product['quantity'] . "', 
					price = '" . $product['price'] . "', 
					discount = '" . $discount . "', 
					campaign_voucher_id = '" . $campaign_voucher_id . "', 
					total = '" . $orderProductTotal . "', 
					discount_id = '" . (int)$discount_id . "', 
					listed_price = '" . (float)$listed_price . "', 
					add_on_deal_id = '" . $addOnDealId . "' 
				";
                $this->db->query($sql);
                //reduce quantity product in product table

                $default_store_id = isset($product['default_store_id']) ? (int)$product['default_store_id'] : 0;

                $sqlProduct = "UPDATE " . DB_PREFIX . "product_to_store 
                              SET quantity = quantity - '" . (int)$product['quantity'] . "' 
                              WHERE product_id = '" . (int)$product['product_id'] . "' 
                              AND product_version_id = '" . (int)$product_version_id . "' 
                              AND store_id = '" . (int)$default_store_id . "'";

                $result = $this->db->query($sqlProduct);

                // TODO: review this new code...
                if (!$result){
                    $sqlProduct = "INSERT INTO " . DB_PREFIX . "product_to_store 
					               SET quantity = '-" . (int)$product['quantity'] . "', 
                                   product_id = '" . (int)$product['product_id'] . "', 
                                   product_version_id = '" . (int)$product_version_id . "',
                                   store_id = '" . (int)$default_store_id . "'";

                    $this->db->query($sqlProduct);
                }
            }

//            try {
//                $this->sendToRabbitMq($orderId);
//            } catch (Exception $exception) {
//                $this->log->write('Send message order error: ' . $exception->getMessage());
//            }

            return true;
        }
        return false;
    }

    public function addOrderAddOnProduct($addOnProducts, $orderId)
    {
        if (empty($addOnProducts) || empty($orderId)) {
            return;
        }

        $DB_PREFIX = DB_PREFIX;

        foreach ($addOnProducts as $addOnProduct) {
            if (empty($addOnProduct['main_product_id']) || empty($addOnProduct['product_id'])) {
                continue;
            }

            $product_version_id = !empty($addOnProduct['product_version']['product_version_id']) ? $addOnProduct['product_version']['product_version_id'] : 0;

            $sql = "INSERT INTO `{$DB_PREFIX}order_add_on_product` 
                        SET `order_id` = {$orderId}, 
                            `main_product_id` = {$addOnProduct['main_product_id']}, 
                            `main_product_version_id` = {$addOnProduct['main_product_version_id']}, 
                            `product_id` = {$addOnProduct['product_id']}, 
                            `product_version_id` = {$product_version_id}
                            ";

            $this->db->query($sql);

            // update limit quantity
            $this->db->query("UPDATE `{$DB_PREFIX}add_on_product` 
                                SET `current_quantity` = current_quantity - 1 
                                    WHERE `product_id` = {$addOnProduct['product_id']} AND `product_version_id` = {$product_version_id}");
        }

    }

    public function addOrderProductForPos($products, $orderId, $store_id = 0)
    {
        if (count($products) > 0) {
            foreach ($products as $product) {
                //insert order_product table
                $product_version_id = $product['product_version_id'];
                $this->load->model('catalog/product');

                $product['price'] = (float)$product['price'];
                // TODO: data from web contains discount value for each product???
                $discount = isset($product['product-discount']) ? (float)(extract_number($product['product-discount'])) : 0;
                $orderProductTotal = (int)$product['quantity'] * (float)$product['price'] - $discount;
                $sql = "
					INSERT INTO " . DB_PREFIX . "order_product 
					SET order_id = '" . (int)$orderId . "', 
					    product_id = '" . (int)$product['product_id'] . "', 
					    product_version_id ='" . (int)$product_version_id . "', 
					    `name` = '" . $product['name'] . "', 
					    model = '" . $product['model'] . "', 
					    quantity = '" . (int)$product['quantity'] . "', 
					    price = '" . $product['price'] . "', 
					    discount = '" . $discount . "', 
					    total = '" . $orderProductTotal . "'
				";

                $this->db->query($sql);
                //reduce quantity product in product table

                $default_store_id = $store_id;

                $sqlProduct = "UPDATE " . DB_PREFIX . "product_to_store 
                               SET quantity = quantity - '" . (int)$product['quantity'] . "' 
                               WHERE product_id = '" . (int)$product['product_id'] . "' 
                                 AND product_version_id = '" . (int)$product_version_id . "' 
                                 AND store_id = '" . (int)$default_store_id . "'";

                $result = $this->db->query($sqlProduct);

                // TODO: review this new code...
                if (!$result){
                    $sqlProduct = "INSERT INTO " . DB_PREFIX . "product_to_store 
					               SET quantity = '-" . (int)$product['quantity'] . "', 
                                       product_id = '" . (int)$product['product_id'] . "', 
                                       product_version_id = '" . (int)$product_version_id . "',
                                       store_id = '" . (int)$default_store_id . "'";

                    $this->db->query($sqlProduct);
                }
            }

            try {
                $this->sendToRabbitMq($orderId);
            } catch (Exception $exception) {
                $this->log->write('Send message order error: ' . $exception->getMessage());
            }

            return true;
        }
        return false;
    }

    public function putBackQuantityByProductId($product_id, $quantity)
    {
        $sqlProduct = "
			UPDATE " . DB_PREFIX . "product SET quantity = quantity + '" . (int)$quantity . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'
		";
        $this->db->query($sqlProduct);
    }

    public function deleteOrderById($orderId)
    {
        //put back quantity product
        $sql = "
			SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$orderId . "'
		";
        $data = $this->db->query($sql)->rows;
        foreach ($data as $key => $value) {
            $this->putBackQuantityByProductId($value['product_id'], $value['quantity']);
        }
        //delete order_product where order_id in order_product table
        $sqlOrderProduct = "
			DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$orderId . "'
		";
        $this->db->query($sqlOrderProduct);
        //delete order in order table
        $sqlOrder = "
			DELETE FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$orderId . "'
		";
        $this->db->query($sqlOrder);

        return true;
    }

    public function getOrderById($orderId)
    {
        $sqlOrder = "
			SELECT * FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$orderId . "'
		";
        $order = $this->db->query($sqlOrder)->row;

        return $order;
    }

    public function getOrderByIdAndPaymentMethod($orderId, $payment_method)
    {
        $DB_PREFIX = DB_PREFIX;
        $sqlOrder = "SELECT * FROM `{$DB_PREFIX}order` 
                     WHERE order_id = '" . (int)$orderId .  "' 
                       AND payment_method = '" . $this->db->escape($payment_method) . "'";
        $order = $this->db->query($sqlOrder)->row;

        return $order;
    }

    public function getListProductByArrayId($listId = array())
    {
        $listId = implode(',', $listId);

        if (count($listId) == 0) {
            return null;
        }
        $sql = "
			SELECT DISTINCT * FROM " . DB_PREFIX . "product p JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id IN (" . $listId . ") AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
		";

        $data = $this->db->query($sql)->rows;
    }

    public function getOrderDetailById($orderId)
    {
        $sql = "SELECT o.order_id AS order_id, o.fullname AS fullname, o.firstname AS firstname, o.lastname AS lastname, o.customer_id AS customer_id, o.order_transfer AS order_transfer, o.order_payment AS order_payment, o.order_status AS order_status, o.total AS total,p.product_id AS product_id, p.image AS image, p.quantity AS max_quantity, pd.name AS name, op.price AS price, op.quantity AS quantity, op.total AS total_product FROM " . DB_PREFIX . "order o JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) JOIN " . DB_PREFIX . "product p ON (op.product_id = p.product_id) JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
			WHERE o.order_id = '" . (int)$orderId . "' AND pd.language_id = '" . `` . "'";
        // var_dump($sql);die;
        $result = $this->db->query($sql)->rows;
        return $result;
    }

    public function getTopProducts($data = ['start' => 0, 'limit' => 10])
    {
        $sql = "SELECT op.name as name, op.product_id as product_id, p.quantity as quantity, p.image as image, SUM(op.total) as total FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "product p ON (op.product_id = p .product_id) GROUP BY op.product_id ORDER BY total DESC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 10;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function updateOrder($data, $order_id)
    {
        $sql = "UPDATE " . DB_PREFIX . "order  
                SET order_status = '" . (int)$data['order_status'] . "', 
                order_payment = '" . (int)$data['order_payment'] . "', 
                order_transfer = '" . (int)$data['order_transfer'] . "', 
                comment = '" . $data['order_note'] . "', 
                total = '" . $data['total'] . "', 
                customer_ref_id = '" . (isset($data['customer_ref_id']) ? $this->db->escape($data['customer_ref_id']) : '') . "', 
                date_modified = NOW() 
                WHERE order_id = '" . (int)$order_id . "'
		 ";
        $this->db->query($sql);
    }

    public function updateOrderPaymentStatus($order_id, $status, $order_status_id, $payment_code) {
        $sql = "UPDATE " . DB_PREFIX . "order  
                SET payment_status = '" . $status . "', 
                order_status_id = '" . $order_status_id . "', 
                payment_code = '" . $payment_code . "' 
                WHERE order_id = '" . (int)$order_id . "'
		 ";
        $this->db->query($sql);
    }

    public function updateOrderPaymentStatusFromAdmin($order_id, $status)
    {
        $sql = "UPDATE " . DB_PREFIX . "order SET payment_status = '" . (int)$status . "' WHERE order_id = '" . (int)$order_id . "'";
        $this->db->query($sql);
    }

    /**
     * NOTICE: keep $old_order_status_id for event listener
     *
     * @param array $data
     * @param int $old_order_status_id
     */
    public function updateStatus($data, $old_order_status_id)
    {
        if (isset($data['order_cancel_comment'])) {
            $sql = sprintf("UPDATE %s 
                            SET order_status_id = '%d', 
                                order_cancel_comment = '%s', 
                                comment = '%s', 
                                date_modified = NOW() 
                            WHERE order_id = '%d'",
                DB_PREFIX . "order",
                (int)$data['order_status_id'],
                $data['order_cancel_comment'],
                $data['order_cancel_comment'],
                (int)$data['order_id']
            );
        } else {
            $sql = sprintf("UPDATE %s 
                            SET order_status_id = '%d', 
                                date_modified = NOW() 
                            WHERE order_id = '%d'",
                DB_PREFIX . "order",
                (int)$data['order_status_id'],
                (int)$data['order_id']
            );
        }

        $this->db->query($sql);

        // update manual update status
        if ($data['order_status_id'] != $old_order_status_id) {
            $this->updateManualUpdateStatus($data['order_id']);
        }
    }

    public function updateOrderPaymentCode($order_id, $payment_code) {
        $sql = "UPDATE " . DB_PREFIX . "order  
                SET payment_code = '" . $payment_code . "' 
                WHERE order_id = '" . (int)$order_id . "'
		 ";
        $this->db->query($sql);
    }

    public function deleteProductOrder($order_id)
    {
        $sql = "
    		DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . $order_id . "' 
    	";
        $this->db->query($sql);
    }

    public function editOrder($order_id, $data)
    {
        // TODO move to event: event before edit
        // put back order quantity to stock
        if (isset($data['old_order_status_id'])) {
            $old_order_status_id = $data['old_order_status_id'];
        } else {
            $old_order_status_id = $this->getOrderStatusIdByOrderId($order_id);
        }
        if ($old_order_status_id == self::ORDER_STATUS_ID_PROCESSING) {
            $this->updateProductOrderChangeStatus($order_id, false);
        }

        // customer
        if (!isset($data['customer']) || $data['customer'] == '') {
            // if ($data['customer_phone'] == '' || $data['customer_email'] == '') { todo: not check email because email allows empty
            if ($data['customer_phone'] == '') {
                $customer_id = '';
            } else {
                if (isset($data['customer_full_name'])) {
                    $name = extract_name($data['customer_full_name']);
                    $data['customer_firstname'] = $name[0];
                    $data['customer_lastname'] = $name[1];
                }

                $this->db->query("INSERT INTO " . DB_PREFIX . "customer 
                                    SET firstname = '" . $this->db->escape($data['customer_firstname']) . "', 
                                        lastname = '" . $this->db->escape($data['customer_lastname']) . "', 
                                        email = '" . $this->db->escape($data['customer_email']) . "', 
                                        telephone = '" . $this->db->escape($data['customer_phone']) . "', 
                                        customer_group_id = '" . $this->db->escape($data['customer_group_id']) . "', 
                                        salt = '" . $this->db->escape($salt = token(9)) . "', 
                                        password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1(token(9))))) . "', 
                                        status = 1,  
                                        date_added = NOW()");
                $customer_id = $this->db->getLastId();

                // update customer code
                $prefix = 'KH';
                $code = $prefix . str_pad($customer_id, 6, "0", STR_PAD_LEFT);

                $this->db->query("UPDATE " . DB_PREFIX . "customer 
                          SET `code` = '" . $code . "' 
                          WHERE customer_id = '" . (int)$customer_id . "'");

                $this->db->query("INSERT INTO " . DB_PREFIX . "address 
                                    SET customer_id = '" . (int)$customer_id . "', 
                                        firstname = '" . $this->db->escape($data['customer_firstname']) . "', 
                                        lastname = '" . $this->db->escape($data['customer_lastname']) . "', 
                                        address = '" . $this->db->escape($data['customer_address']) . "', 
                                        city = '" . $this->db->escape($data['customer_province']) . "', 
                                        district = '" . $this->db->escape($data['customer_district']) . "', 
                                        wards = '" . $this->db->escape($data['customer_wards']) . "', 
                                        phone = '" . $this->db->escape($data['customer_phone']) . "'");

                $address_id = $this->db->getLastId();
                $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
            }
        } else {
            $customer_id = (int)$data['customer'];
        }
        $data['shipping_fullname'] = trim($data['shipping_fullname']) != '' ? $data['shipping_fullname'] : $data['customer_full_name'];

        $order_status_id = (int)$data['order_status'];
        if ($data['payment_status'] == 4 && $data['payment_method'] != 1) {// payment_method =1: thoanh toans quas vnpay, payment_status=4: thanh toán lỗi
            $data['payment_status'] = 0;
        }
        $payment_status = $data['payment_status'];
        $this->db->query("UPDATE `" . DB_PREFIX . "order` 
                          SET fullname = '" . $this->db->escape($data['shipping_fullname']) . "', 
                          shipping_province_code = '" . $this->db->escape($data['shipping_province']) . "', 
                          shipping_district_code = '" . $this->db->escape($data['shipping_district']) . "', 
                          shipping_ward_code = '" . $this->db->escape($data['shipping_wards']) . "', 
                          shipping_address_1 = '" . $this->db->escape($data['shipping_address']) . "', 
                          telephone = '" . $this->db->escape($data['shipping_phone']) . "', 
                          shipping_method = '" . $this->db->escape($data['shipping_method']) . "', 
                          shipping_method_value = '" . (isset($data['delivery']) ? $this->db->escape($data['delivery']) : $this->db->escape("0")) . "', 
                          shipping_fee = '" . $this->db->escape($data['shipping_fee']) . "', 
                          order_status_id = " . (int)$order_status_id . ", 
                          customer_id = " . (int)$customer_id . ", 
                          payment_method = '" . (array_key_exists('payment_method', $data) ? $this->db->escape($data['payment_method']) : '') . "', 
                          discount = '" . (isset($data['order-discount']) ? $this->db->escape(extract_number($data['order-discount'])) : 0) . "', 
                          total = '" . $this->db->escape($data['total_pay']) . "', 
                          customer_ref_id = " . (isset($data['customer_ref_id']) ? ("'" . $this->db->escape($data['customer_ref_id']) . "'") : '`customer_ref_id`') . ", 
                          comment = '" . $this->db->escape($data['note']) . "', 
                          user_id = " . (int)$data['user_id'] . ",
                          date_modified = NOW()" . ",  
                          payment_status = '" . $payment_status . "' WHERE order_id = " . (int)$order_id);

        // notice for above sql: keep old value customer_ref_id (for chatbot) if not set when edit order

        //tag
        $this->db->query("DELETE FROM " . DB_PREFIX . "order_tag WHERE order_id = '" . (int)$order_id . "'");
        if (isset($data['tag_list'])) {
            $tags = is_array($data['tag_list']) ? $data['tag_list'] : [];
            foreach ($tags as $tag) {
                if (strpos($tag, self::ORDER_TAG_NEW_PREFIX) !== false) {
                    $newTag = str_replace(self::ORDER_TAG_NEW_PREFIX, '', $tag);
                    $sql_update = "INSERT INTO " . DB_PREFIX . "tag(value) VALUES ('" . $this->db->escape($newTag) . "')";
                    $this->db->query($sql_update);
                    $tag_id = $this->db->getLastId();
                    $this->db->query("INSERT INTO " . DB_PREFIX . "order_tag SET order_id = " . (int)$order_id . ", tag_id = " . (int)$tag_id);
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "order_tag SET order_id = " . (int)$order_id . ", tag_id = " . (int)$tag);
                }
            }
        }

        // product_order
        if (isset($data['pids'])) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

            $product_ids = is_array($data['pids']) ? $data['pids'] : [];
            foreach ($product_ids as $key => $ids) {
                $ids = explode('-', $ids);
                $product_id = array_shift($ids);
                $product_version_id = array_shift($ids);
                $price = isset($data['prices'][$key]) ? $data['prices'][$key] : 0;
                $price = str_replace(',', '', $price);
                $quantity = isset($data['quantities'][$key]) ? (int)$data['quantities'][$key] : 0;
                $quantity = str_replace(',', '', $quantity);
                $discount = isset($data['product-discount'][$key]) ? (int)(extract_number($data['product-discount'][$key])) : 0;
                $total = $price * $quantity - $discount;
                $this->db->query("INSERT INTO " . DB_PREFIX . "order_product 
                                  SET order_id = " . (int)$order_id . ", 
                                  product_id = " . (int)$product_id . ", 
                                  product_version_id =" . (int)$product_version_id . ", 
                                  quantity = " . $quantity . ", 
                                  price = " . $price . ", 
                                  discount = '" . $discount . "', 
                                  total = '" . $total . "'");
            }
        }

        // update order date_modified
        $sql = "UPDATE " . DB_PREFIX . "order SET date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'";
        $this->db->query($sql);

        // update manual update status
        if (!(isset($data['is_create_order_delivery']) && $data['is_create_order_delivery']) && $order_status_id != $old_order_status_id) {
            $this->updateManualUpdateStatus($order_id);
        }

        if (isset($data['order_source']) && $data['order_source'] == 'shopee') {
            $this->updateOrderShopee($order_id);
        }

        if (isset($data['order_source']) && $data['order_source'] == 'lazada') {
            $this->updateOrderLazada($order_id);
        }

        return $order_id;
    }

    public function getOrderStatusIdByOrderId($order_id)
    {
        $query = $this->db->query("SELECT `order_status_id` FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id . "'");
        if (isset($query->row['order_status_id'])) {
            return (int)$query->row['order_status_id'];
        } else {
            return 0;
        }
    }

    public function updateProductOrderChangeStatus($order_id, $flag = true)
    {
        $condition = $flag ? '-' : '+';
        $products = $this->getOrderProductsDetail($order_id);
        foreach ($products as $product) {
            $default_store_id = isset($product['default_store_id']) ? (int)$product['default_store_id'] : 0;

            $sqlProduct = "UPDATE " . DB_PREFIX . "product_to_store 
					       SET quantity = quantity " . $condition . " '" . (int)$product['quantity'] . "' 
                           WHERE product_id = '" . (int)$product['product_id'] . "' 
                             AND product_version_id = '" . (int)$product['product_version_id'] . "' 
                             AND store_id = '" . (int)$default_store_id . "'";

            $result = $this->db->query($sqlProduct);

            // TODO: review this new code...
            if (!$result){
                $sqlProduct = "INSERT INTO " . DB_PREFIX . "product_to_store 
                               SET quantity = 0 " . $condition . " '" . (int)$product['quantity'] . "', 
                                   product_id = '" . (int)$product['product_id'] . "', 
                                   product_version_id = '" . (int)$product['product_version_id'] . "',
                                   store_id = '" . (int)$default_store_id . "'";

                $this->db->query($sqlProduct);
            }
        }
    }

    public function getOrderProductsDetail($order_id)
    {
        $sql = "SELECT DISTINCT(t.order_product_id) as odi, t.* FROM
                    (SELECT o.order_id, o.order_code, o.customer_id, 
                            od.order_product_id, od.quantity, od.price, od.total, 
                            p.product_id, p.default_store_id, p.image, pd.name, od.product_version_id 
                    FROM `" . DB_PREFIX . "order_product` od 
                    JOIN `" . DB_PREFIX . "order` o ON o.order_id = od.order_id 
                    JOIN `" . DB_PREFIX . "product` p ON od.product_id = p.product_id 
                    JOIN `" . DB_PREFIX . "product_description` pd ON p.product_id = pd.product_id 
                    WHERE od.order_id = " . (int)$order_id . ") t";

        $query = $this->db->query($sql);
        $product_orders = [];
        foreach ($query->rows as $row) {
            $product_orders[] = [
                'order_id' => $row['order_id'],
                'order_code' => $row['order_code'],
                'customer_id' => $row['customer_id'],
                'product_id' => $row['product_id'],
                'name' => $row['name'],
                'quantity' => $row['quantity'],
                'price' => $row['price'],
                'total' => $row['total'],
                'image' => $row['image'],
                'product_version_id' => $row['product_version_id'],
                'default_store_id' => $row['default_store_id'],
            ];
        }

        return $product_orders;
    }

    /**
     * @param $order_id
     * @param int $manual_update_status
     */
    public function updateManualUpdateStatus($order_id, $manual_update_status = 1)
    {
        // only update when not integrated with transport service yet
        $sql = "UPDATE " . DB_PREFIX . "order SET `manual_update_status` = $manual_update_status WHERE `order_id` = '" . (int)$order_id . "' AND `transport_order_code` IS NOT NULL";
        $this->db->query($sql);
    }

    public function updateOrderShopee($order_id)
    {
        // 1: is edited, 2 order is update by bestme
        $sql = "UPDATE " . DB_PREFIX . "shopee_order 
                    SET `is_edited` = 1, 
                        `sync_status` = 2 
                    WHERE `bestme_order_id` = '" . (int)$order_id . "'";
        $this->db->query($sql);
    }

    public function updateOrderLazada($order_id)
    {
        // 1: is edited, 2 order is update by bestme
        $sql = "UPDATE " . DB_PREFIX . "lazada_order 
                    SET `is_edited` = 1, 
                        `sync_status` = 2 
                    WHERE `bestme_order_id` = '" . (int)$order_id . "'";
        $this->db->query($sql);
    }

    public function editOrderCustom($data, $order_id)
    {
        // var_dump($data);die;
        //xac dinh order_status cua order truoc khi edit
        $order = $this->getOrderById($order_id);

        if (in_array($order['order_status'], [4, 5])) {
            $this->updateOrder($data, $order_id);
        }
        if (in_array($order['order_status'], [2, 3])) {
            if (in_array($data['order_status'], [4, 5])) {
                foreach ($data['product-choose'] as $key => $value) {
                    $product_id = $value;
                    $quantity = $data['product_order_quantity'][$key];
                    $this->putBackQuantityByProductId($product_id, $quantity);
                }
                $this->updateOrder($data, $order_id);
            } else {
                // var_dump($data);die;
                $this->updateOrder($data, $order_id);
            }
        }
        //neu la don nhap(order_status =1) thi co the chinh sua lai toan bo don hang tu sp so luong...
        if ($order['order_status'] == 1) {
            $order_products = $this->getOrderProducts($order_id);
            //cong lai cac sp cu vao kho
            foreach ($order_products as $order_product) {
                $product_id = $order_product['product_id'];
                $quantity = $order_product['quantity'];
                $this->putBackQuantityByProductId($product_id, $quantity);
            }
            //xoa cac ban ghi cu trong product_order
            $this->deleteProductOrder($order_id);
            //tao moi lai
            $this->addOrderProduct($data, $order_id);
            // add report for order
            //$this->addReport($order_id); // do not add report order for status draft!
            //update order
            $customerId = $this->getCustomerIdByPhone($data);
            // var_dump($data);
            $this->db->query("UPDATE " . DB_PREFIX . "order SET  firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', fullname = '" . $this->db->escape($data['fullname']) . "', telephone = '" . $data['telephone'] . "', order_status='" . $data['order_status'] . "', order_payment='" . $data['order_payment'] . "', order_transfer = '" . $data['order_transfer'] . "', comment = '" . $data['order_note'] . "',customer_id = '" . $customerId . "', total = '" . $data['order_total'] . "', date_added = NOW(), date_modified = NOW() WHERE order_id = '" . $order_id . "'");
        }
    }

    /**
     * now use by chatbot api only
     *
     * TODO: refactor function for reusing...
     *
     * @param int $order_id
     * @param array $data
     * @param null|int $customer_id
     * @return bool
     */
    public function editOrderCommon($order_id, $data, $customer_id = null)
    {
        if (empty($order_id)) {
            return false;
        }

        $payment_method = isset($data['payment_method']) ? $data['payment_method'] : '';
        $pos_id_ref = isset($data['pos_id_ref']) ? $data['pos_id_ref'] : '';
        $store_id = isset($data['store_id']) ? $data['store_id'] : 0;
        $user_id = isset($data['user_id']) ? $data['user_id'] : 1;

        $this->db->query("UPDATE `" . DB_PREFIX . "order` SET  
            customer_id = '" . (int)$customer_id . "', 
            firstname = '" . $this->db->escape($data['firstname']) . "', 
            lastname = '" . $this->db->escape($data['lastname']) . "', 
            fullname = '" . $this->db->escape($data['fullname']) . "', 
            email = '" . $this->db->escape($data['email']) . "', 
            telephone = '" . $this->db->escape($data['telephone']) . "', 
            order_payment='" . $data['order_payment'] . "', 
            order_transfer = '" . $data['order_transfer'] . "', 
            comment = '" . $data['order_note'] . "', 
            payment_firstname = '" . $this->db->escape($data['payment_firstname']) . "', 
            payment_lastname = '" . $this->db->escape($data['payment_lastname']) . "', 
            payment_company = '" . $this->db->escape($data['payment_company']) . "', 
            payment_address_1 = '" . $this->db->escape($data['payment_address_1']) . "', 
            payment_address_2 = '" . $this->db->escape($data['payment_address_2']) . "', 
            payment_city = '" . $this->db->escape($data['payment_city']) . "', 
            payment_status = '" . $this->db->escape($data['payment_status']) . "', 
            payment_postcode = '" . $this->db->escape($data['payment_postcode']) . "', 
            payment_method = '" . $this->db->escape($payment_method) . "', 
            payment_code = '" . $this->db->escape($data['payment_code']) . "', 
            shipping_firstname = '" . $this->db->escape($data['shipping_firstname']) . "', 
            shipping_lastname = '" . $this->db->escape($data['shipping_lastname']) . "', 
            shipping_company = '" . $this->db->escape($data['shipping_company']) . "', 
            shipping_address_1 = '" . $this->db->escape($data['shipping_address_1']) . "', 
            shipping_address_2 = '" . $this->db->escape($data['shipping_address_2']) . "', 
            shipping_city = '" . $this->db->escape($data['shipping_city']) . "', 
            shipping_method = '" . $this->db->escape($data['shipping_method']) . "', 
            shipping_method_value = '" . $this->db->escape($data['shipping_method_value']) . "', 
            shipping_ward_code = '" . $this->db->escape($data['shipping_ward_code']) . "', 
            shipping_district_code = '" . $this->db->escape($data['shipping_district_code']) . "', 
            shipping_province_code = '" . $this->db->escape($data['shipping_province_code']) . "', 
            shipping_fee = '" . $this->db->escape($data['shipping_fee']) . "', 
            order_status_id = '" . $this->db->escape($data['order_status_id']) . "', 
            currency_code = '" . $this->db->escape($data['currency_code']) . "', 
            currency_value = '" . $this->currency->getValue($data['currency_code']) . "', 
            from_domain = '" . (isset($data['from_domain']) ? $this->db->escape($data['from_domain']) : '') . "', 
            currency_id = '" . $this->currency->getId($data['currency_code']) . "', 
            shipping_code = '" . $this->db->escape($data['shipping_code']) . "', 
            discount = '" . (isset($data['order-discount']) ? (float)(extract_number($data['order-discount'])) : 0) . "', 
            total = '" . (float)$data['total'] . "', 
            customer_ref_id = '" . (isset($data['customer_ref_id']) ? $this->db->escape($data['customer_ref_id']) : '') . "', 
            pos_id_ref = '" . $this->db->escape($pos_id_ref) . "', 
            store_id = '" . (int)$store_id . "', 
            user_id = '" . (int)$user_id . "', 
            date_modified = NOW() 
            WHERE order_id = " . (int)$order_id
        );

        // TODO: data from web contains discount value for order???

        // support tag
        if (isset($data['tags'])) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "order_tag WHERE order_id = '" . (int)$order_id . "'");

            /* notice: empty tags known as REMOVING ALL tags! */

            $tags = explode(',', $data['tags']);
            $tags = is_array($tags) ? $tags : [];
            $this->load->model('catalog/tag');
            foreach ($tags as $tag) {
                $tag = trim($tag);
                if (empty($tag)) {
                    continue;
                }

                $existing_tag = $this->model_catalog_tag->getTagByValue($tag);
                if (!$existing_tag) {
                    $sql_update = "INSERT INTO " . DB_PREFIX . "tag(`value`) 
                                   VALUES ('" . $this->db->escape($tag) . "')";
                    $this->db->query($sql_update);
                    $tag_id = $this->db->getLastId();
                } else {
                    $tag_id = $existing_tag['tag_id'];
                }

                $this->db->query("INSERT INTO " . DB_PREFIX . "order_tag 
                                      SET order_id = " . (int)$order_id . ", 
                                          tag_id = " . (int)$tag_id);
            }
        }

        // 20200420: support order utm
        if (isset($data['utm']) && !empty($data['utm'])) {
            $this->load->model('sale/order_utm');
            $this->model_sale_order_utm->addOrderUtm($order_id,$data['utm']);
        }

        return $order_id;
    }

    public function copy($order_id)
    {
        //duplicate trong bang order
        $sqlCopy = "
    		INSERT INTO " . DB_PREFIX . "order (firstname, fullname, lastname, email, telephone, order_status, order_payment, order_transfer, comment, customer_id, total, date_added, date_modified) SELECT firstname, fullname, lastname, email, telephone, order_status, order_payment, order_transfer, comment, customer_id, total, date_added, date_modified FROM " . DB_PREFIX . "order WHERE order_id = '" . $order_id . "'
    	";
        $this->db->query($sqlCopy);
        $order_id_copy = $this->db->getLastId();
        //duplicate trong order_product
        $sqlOrderProduct = "
			SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'
		";
        $data = $this->db->query($sqlOrderProduct)->rows;

        foreach ($data as $key => $value) {
            //create new record order_product
            $sql = "
				INSERT INTO " . DB_PREFIX . "order_product SET  order_id = '" . (int)$order_id_copy . "', product_id = '" . (int)$value['product_id'] . "', quantity = '" . (int)$value['quantity'] . "', price = '" . $value['price'] . "', total = '" . $value['total'] . "'
			";
            $this->db->query($sql);
            //kiem tra quantity sp trong kho so voi quantity copy
            $quantity_store = $this->db->query("SELECT quantity FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$value['product_id'] . "'")->row['quantity'];
            if ($quantity_store < $value['quantity']) {
                //xoa order_id_copy
                $this->db->query("DELETE FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id_copy . "'");
                return false;
            }
            //reduce quantity product in product table
            $sqlProduct = "
				UPDATE " . DB_PREFIX . "product SET quantity = quantity - '" . (int)$value['quantity'] . "', date_modified = NOW() WHERE product_id = '" . (int)$value['product_id'] . "'
			";
            $this->db->query($sqlProduct);
        }
        return true;
    }

    public function getNameForOrder($orderId)
    {
        $sql = "SELECT op.name as name FROM " . DB_PREFIX . "order_product op WHERE op.order_id = " . (int)$orderId;
        $query = $this->db->query($sql);

        $names = array_map(function ($op) {
            return $op['name'];
        }, $query->rows);

        return implode(', ', $names);
    }

    public function updateStatusTransportDeliveryApi($transport_order_code, $transport_status)
    {
        $sql = "UPDATE " . DB_PREFIX . "order SET transport_status = '" . $transport_status . "' WHERE transport_order_code  = '" . $transport_order_code . "'";
        $this->db->query($sql);
    }

    public function updateStatusDeliveryApi($order_id, $transport_status)
    {
        $sql = "UPDATE " . DB_PREFIX . "order SET transport_status = '" . $transport_status . "' WHERE order_id  = '" . (int)$order_id . "'";
        $this->db->query($sql);
    }

    public function addReport($order_id)
    {
        $dataOrder = $this->model_sale_order->getOrder($order_id);

        /* validate order status: skip update order report if draft or cancelled */
        if (!isset($dataOrder['order_status_id']) || !in_array($dataOrder['order_status_id'], [
                ModelSaleOrder::ORDER_STATUS_ID_PROCESSING,
                ModelSaleOrder::ORDER_STATUS_ID_DELIVERING,
                ModelSaleOrder::ORDER_STATUS_ID_COMPLETED
            ])
        ) {
            return;
        }

        $products = $this->getOrderProductsWithCostPrice($order_id);

        $total_discount = 0;

        // order discount is divided by product
        $total_before_discount = 0;
        foreach ($products as $key => $product) {
            $total_before_discount += $product['quantity'] * $product['price'];
        }
        foreach ($products as $key => $product) {
            if($total_before_discount == 0) {
                $products[$key]['discount_with_order_discount'] = 0;
            } else {
                $percent_price = $product['quantity'] * $product['price'] / $total_before_discount;
                $products[$key]['discount_with_order_discount'] = $product['discount'] + $percent_price * $dataOrder['discount'];
                $total_discount += $product['discount'] + $percent_price * $dataOrder['discount'];
            }
        }

        /* do update report */
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $reportTime = new DateTime();
        $reportTime->setTime($reportTime->format('H'), 0, 0);
        $dataOrderReport = [
            'report_time' => $reportTime->format('Y-m-d H:i:s'),
            'report_date' => $reportTime->format('Y-m-d'),
            'order_id' => $order_id,
            'customer_id' => $dataOrder['customer_id'],
            'total_amount' => $dataOrder['total'],
            'order_status' => $dataOrder['order_status_id'],
            'store_id' => $dataOrder['store_id'],
            'user_id' => $dataOrder['user_id'],
            'source' => $dataOrder['source'],
            'discount' => $total_discount,
            'shipping_fee' => $dataOrder['shipping_fee']
        ];
        foreach ($products as $product) {
            $dataReportOrderProduct = [
                'report_time' => $reportTime->format('Y-m-d H:i:s'),
                'report_date' => $reportTime->format('Y-m-d'),
                'order_id' => $order_id,
                'product_id' => $product['product_id'],
                'product_version_id' => (int)$product['product_version_id'],
                'quantity' => $product['quantity'],
                'price' => $product['price'],
                'total_amount' => $product['total'],
                'store_id' => $dataOrder['store_id'],
                'user_id' => $dataOrder['user_id'],
                'cost_price' => $product['cost_price'],
                'discount' => $product['discount_with_order_discount'],
            ];
            $this->saveOrderProductReport($dataReportOrderProduct);
        }

        $this->saveOrderReport($dataOrderReport);
    }

    public function saveOrderReport($data)
    {
        /* validate order status: skip update order report if draft or cancelled */
        if (!isset($data['order_status']) || (self::VNP_APP_CODE != $data['payment_method'] && !in_array($data['order_status'], [
                ModelSaleOrder::ORDER_STATUS_ID_PROCESSING,
                ModelSaleOrder::ORDER_STATUS_ID_DELIVERING,
                ModelSaleOrder::ORDER_STATUS_ID_COMPLETED
            ])) || (self::VNP_APP_CODE == $data['payment_method'] && $data['order_status'] != ModelSaleOrder::ORDER_STATUS_ID_DRAFT)
        ) {
            return;
        }

        $sql = "INSERT INTO " . DB_PREFIX . "report_order SET report_time = '" . $data['report_time'] . "'";
        $sql .= ", report_date = '" . $data['report_date'] . "'";
        $sql .= ", order_id = '" . $data['order_id'] . "'";
        $sql .= ", customer_id = '" . $data['customer_id'] . "'";
        $sql .= ", total_amount = '" . $data['total_amount'] . "'";
        $sql .= ", order_status = '" . $data['order_status'] . "'";
        $sql .= ", store_id = '" . $data['store_id'] . "'";
        $sql .= ", user_id = '" . $data['user_id'] . "'";
        $sql .= ", source = '" . $data['source'] . "'";
        $sql .= ", discount = '" . $data['discount'] . "'";
        $sql .= ", shipping_fee = '" . $data['shipping_fee'] . "'";
        $this->db->query($sql);

        // sync shop order to welcom
        $this->load->model('setting/setting');
        $dataOrderReport = [
            'shop_name' =>  $this->model_setting_setting->getShopName(),
            'type' => 'add',
            'new_status' => $data['order_status'],
            'old_status' => null,
            'new_value' => $data['total_amount'],
            'old_value' => null,
            'time' => $data['report_time']
        ];
        $this->publishSyncShopOrder($dataOrderReport);
        // end sync
    }

    public function saveOrderProductReport($data)
    {
        $sql = "INSERT INTO " . DB_PREFIX . "report_product SET report_time = '" . $data['report_time'] . "'";
        $sql .= ", report_date = '" . $data['report_date'] . "'";
        $sql .= ", order_id = '" . $data['order_id'] . "'";
        $sql .= ", product_id = '" . $data['product_id'] . "'";
        $sql .= ", product_version_id = '" . $data['product_version_id'] . "'";
        $sql .= ", quantity = '" . $data['quantity'] . "'";
        $sql .= ", price = '" . $data['price'] . "'";
        $sql .= ", total_amount = '" . $data['total_amount'] . "'";
        $sql .= ", store_id = '" . $data['store_id'] . "'";
        $sql .= ", user_id = '" . $data['user_id'] . "'";
        $sql .= ", cost_price = '" . $data['cost_price'] . "'";
        $sql .= ", discount = '" . $data['discount'] . "'";
        $this->db->query($sql);
    }

    public function getOrderByPosRef($pos_ref_id)
    {
        if (empty($pos_ref_id)) {
            return [];
        }
        $sql = "SELECT * FROM " . DB_PREFIX . "order o WHERE o.pos_id_ref = '" . $this->db->escape($pos_ref_id) . "'";
        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getProductsDetailByOrder($order_id)
    {
        $this->load->model('tool/image');
        $image_null = $this->model_tool_image->resize('no_image.png', 100, 100);
        $sql = "SELECT p.sale_on_out_of_stock as p_sale_on_out_of_stock, 
                       p.status as p_status, 
                       pv.status as pv_status, 
                       p.quantity as p_quantity, 
                       pv.quantity as pv_quantity, 
                       CASE 
                           WHEN p.image IS NULL OR p.image='' 
                           THEN '" . $image_null . "' 
                           ELSE p.image 
                       END AS image_check, 
                       CASE 
                           WHEN op.product_version_id IS NULL 
                           THEN op.product_id 
                           ELSE CONCAT(op.product_id, '-', op.product_version_id) 
                       END AS id_version, op.product_version_id, op.product_id, p.image, 
                       CASE
                           WHEN op.product_version_id IS NULL OR op.product_version_id = 0 
                           THEN p.price 
                           ELSE pv.price 
                       END as p_price, 
                       CASE 
                           WHEN op.product_version_id IS NULL OR op.product_version_id = 0 
                           THEN p.compare_price 
                           ELSE pv.compare_price 
                       END as p_compare_price, 
                       pd.name, 
                       op.quantity, 
                       op.price, 
                       op.discount as order_product_discount, 
                       pv.version 
                       FROM " . DB_PREFIX . "order_product as op 
                       LEFT JOIN " . DB_PREFIX . "product_description as pd ON op.product_id=pd.product_id ";
        $sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (op.product_id = p.product_id)";
        $sql .= " LEFT JOIN " . DB_PREFIX . "product_version pv ON (op.product_version_id = pv.product_version_id)";
        $sql .= " WHERE order_id = '" . (int)$order_id . "' AND pd.language_id='" . (int)$this->config->get('config_language_id') . "'";

        $product_list = $this->db->query($sql)->rows;

        foreach ($product_list as $key => $product) {
            $flag = true;
            if (empty($product['product_version_id'])) {
                if ($product['p_status'] == 0) {
                    $flag = false;
                }

                if ($product['p_sale_on_out_of_stock'] == 0 && $product['p_quantity'] < 1) {
                    $flag = false;
                }
            } else {
                if ($product['pv_status'] == 0 || $product['p_status'] == 0) {
                    $flag = false;
                }

                if ($product['p_sale_on_out_of_stock'] == 0 && $product['pv_quantity'] < 1) {
                    $flag = false;
                }
            }

            // todo: not use
//            if ($product['p_price'] == 0) {
//                $product_list[$key]['p_price'] = $product_list[$key]['p_compare_price'];
//            }

            $product_list[$key]['flag'] = $flag;
        }

        return $product_list;
    }

    /**
     * get order info by order_code
     * @param $order_code
     * @return
     */
    public function getOrderByOrderCode($order_code)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}order` WHERE `order_code` = '$order_code' LIMIT 1";
        $query = $this->db->query($sql);

        return $query->row;
    }

    /**
     * check if order exists by transport_order_code
     * @param $transport_order_code
     * @return bool
     */
    public function checkOrderExistsByTransportOrderCode($transport_order_code)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT EXISTS (SELECT 1 FROM `{$DB_PREFIX}order` WHERE `transport_order_code` = '$transport_order_code' LIMIT 1) result";
        $query = $this->db->query($sql);

        return 1 == $query->row['result'];
    }

    /**
     * @param $transport_order_code
     * @param $order_status_id
     * @param $transport_status
     */
    public function updateOrderTransportStatus($transport_order_code, $order_status_id, $transport_status)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "UPDATE `{$DB_PREFIX}order`
                SET `order_status_id` = '{$order_status_id}',
                    `transport_status` = '{$transport_status}'
                WHERE `transport_order_code` = '$transport_order_code'";
        $this->db->query($sql);
    }

    /**
     * @param $order_id
     * @param $previous_order_status_id
     */
    public function updatePreviousOrderStatusId($order_id, $previous_order_status_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "UPDATE `{$DB_PREFIX}order` SET previous_order_status_id = '" . (int)$previous_order_status_id . "' WHERE `order_id` = '" . (int)$order_id . "'";
        $this->db->query($sql);
    }

    public function getOrdersListData(array $data)
    {
        $sql = "SELECT o.total, o.date_added, o.source, o.order_code, o.`payment_status`, o.`order_status_id`, o.`date_modified`, 
                        o.order_id as order_id_result, 
                        o.telephone as telephone_order, 
                        o.discount, o.`store_id`, o.`pos_id_ref`, 
                        CONCAT(customer.lastname, ' ', customer.firstname) as customer_full_name, 
                        o.fullname as order_full_name FROM `" . DB_PREFIX . "order` as o";

        $sql .= " LEFT JOIN `" . DB_PREFIX . "customer` as customer ON o.`customer_id` = customer.`customer_id` ";
        $sql .= " WHERE 1";
        // not show order with payment -status = 3
        $sql .= " AND o.`payment_status` != '3'";
        $sql .= " AND o.`order_status_id` = '9'";
        $sql .= " AND o.`source` NOT IN('shopee', 'lazada', 'tiki')";

        // filter order_code
        if (!empty($data['order_code'])) {
            $sql .= " AND o.`order_code` LIKE '%" . $this->db->escape($data['order_code']) . "%' ";
        }

        // filter order_source
        if (!empty($data['order_source'])) {
            $sql .= " AND o.`source` LIKE '" . $this->db->escape($data['order_source']) . "' ";
        }

        if (!empty($data['time_from']) && !empty($data['time_to'])) {
            $sql .= " AND o.date_added BETWEEN '" . $data['time_from'] . "' AND '" . $data['time_to'] . "'";
        }

        $sql .= " GROUP BY o.`order_id`";
        $sql .= " ORDER BY o.`date_modified` DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getOrdersListTotal(array $data)
    {
        $sql = "SELECT COUNT(DISTINCT o.order_id ) AS total FROM `" . DB_PREFIX . "order` as o ";

        $sql .= " LEFT JOIN `" . DB_PREFIX . "customer` as customer ON o.`customer_id` = customer.`customer_id` ";
        $sql .= " WHERE 1";
        // not show order with payment -status = 3
        $sql .= " AND o.`payment_status` != '3'";
        $sql .= " AND o.`order_status_id` = '9'";
        $sql .= " AND o.`source` NOT IN('shopee', 'lazada', 'tiki')";

        // filter order_code
        if (!empty($data['order_code'])) {
            $sql .= " AND o.`order_code` LIKE '%" . $this->db->escape($data['order_code']) . "%' ";
        }

        // filter order_source
        if (!empty($data['order_source'])) {
            $sql .= " AND o.`source` LIKE '" . $this->db->escape($data['order_source']) . "' ";
        }

        if (!empty($data['time_from']) && !empty($data['time_to'])) {
            $sql .= " AND o.date_added BETWEEN '" . $data['time_from'] . "' AND '" . $data['time_to'] . "'";
        }

        $sql .= " ORDER BY o.`date_modified` DESC";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    // v2.9.2
    public function getDataTotalReportByOrders(array $data)
    {
        $sql = "SELECT 
                    o.order_id as order_id_result, 
                    o.total, 
                    o.date_added, 
                    o.source, 
                    o.order_code, 
                    o.`store_id`, 
                    o.`pos_id_ref`, 
                    IF(rp.discount IS NULL, 0.0000, rp.discount) AS `rp_discount`, 
                    IF(rp.discount IS NULL, o.`total`, (o.`total` + rp.discount)) AS `total_amount`, 
                    CONCAT(customer.lastname, ' ', customer.firstname) AS customer_full_name, 
                    o.fullname AS order_full_name 
                FROM `" . DB_PREFIX . "order` AS o";

        $sql .= " LEFT JOIN `" . DB_PREFIX . "customer` as customer ON o.`customer_id` = customer.`customer_id` ";
        $sql .= " LEFT JOIN `" . DB_PREFIX . "report_order` as rp ON o.`order_id` = rp.`order_id` ";
        $sql .= " WHERE 1";
        // not show order with payment -status = 3
        $sql .= " AND o.`payment_status` != '3'";
        $sql .= " AND o.`order_status_id` = '9'";

        // filter order_source
        if (!empty($data['order_source'])) {
            $sql .= " AND o.`source` LIKE '" . $this->db->escape($data['order_source']) . "' ";
        }

        $sql .= " GROUP BY o.`order_id`";
        $sql .= " ORDER BY o.`date_modified` DESC";

        $query = $this->db->query($sql);

        return $query->rows;

    }
    // END v2.9.2

    private function getOrderProductsWithCostPrice($order_id) {
        $query = $this->db->query( "SELECT pts.cost_price AS cost_price, op.*
                FROM " . DB_PREFIX . "order_product op
                left join " . DB_PREFIX . "product p ON op.product_id = p.product_id   
                LEFT JOIN " . DB_PREFIX . "product_to_store pts ON p.product_id = pts.product_id AND p.default_store_id = pts.store_id 
                WHERE p.product_id=op.product_id
                AND if(p.multi_versions = 1, pts.product_version_id, 1) = if(p.multi_versions = 1, op.product_version_id, 1)
                AND op.order_id = ".(int)$order_id);
        return $query->rows;

    }

    /**
     * update Related Order Products due to product_id (price change, ...)
     *
     * @param $product_id
     */
    public function updateRelatedOrderProducts($product_id)
    {
        if (empty($product_id)) {
            return;
        }

        try {
            $this->load->model('catalog/product');

            $latest_product = $this->model_catalog_product->getProduct($product_id);
            $latest_product_versions = $this->model_catalog_product->getProductVersions($product_id);
            // remap product_version_id as key
            foreach ($latest_product_versions as $key => $lpv) {
                if (!isset($lpv['product_version_id'])) {
                    continue;
                }

                $latest_product_versions[$lpv['product_version_id']] = $lpv;
                unset($latest_product_versions[$key]);
            }

            // only for order if in some statuses
            $order_statuses = [
                ModelSaleOrder::ORDER_STATUS_ID_DRAFT
            ];
            $related_order_products = $this->model_sale_order->getRelatedOrderProductsByProductId($product_id, $order_statuses);

            foreach ($related_order_products as $related_order_product) {
                // single version
                $related_pv_id = $related_order_product['product_version_id'];
                if (!$related_pv_id) {
                    if (!array_key_exists('price', $latest_product) || !array_key_exists('compare_price', $latest_product)) {
                        continue;
                    }

                    $price_for_sale = !empty($latest_product['price']) && $latest_product['price'] > 0 ? $latest_product['price'] : $latest_product['compare_price'];
                } else { // multi versions
                    $latest_pv = isset($latest_product_versions[$related_pv_id]) ? $latest_product_versions[$related_pv_id] : [];
                    if (!array_key_exists('price', $latest_pv) || !array_key_exists('compare_price', $latest_pv)) {
                        continue;
                    }

                    $price_for_sale = !empty($latest_pv['price']) && $latest_pv['price'] > 0 ? $latest_pv['price'] : $latest_pv['compare_price'];
                }

                $related_order_product['price'] = $price_for_sale;
                //$related_order_product['discount'] = ?;
                $related_order_product['total'] = $price_for_sale * $related_order_product['quantity'];

                // product_order
                $DB_PREFIX = DB_PREFIX;
                $this->db->query("DELETE FROM {$DB_PREFIX}order_product 
                              WHERE order_id = '" . (int)$related_order_product['order_id'] . "' 
                              AND product_id = '" . (int)$related_order_product['product_id'] . "' 
                              AND product_version_id = '" . (int)$related_order_product['product_version_id'] . "'");

                $this->db->query("INSERT INTO {$DB_PREFIX}order_product 
                                  SET order_id = " . (int)$related_order_product['order_id'] . ", 
                                      product_id = " . (int)$related_order_product['product_id'] . ", 
                                      product_version_id =" . (int)$related_order_product['product_version_id'] . ", 
                                      quantity = " . (int)$related_order_product['quantity'] . ", 
                                      price = '{$related_order_product['price']}', 
                                      discount = '{$related_order_product['discount']}', 
                                      total = '{$related_order_product['total']}'");

                // update order info such as: total, discount, transport fee, ...
                $this->reCalculateOrderInfo($related_order_product['order_id']);
            }
        } catch (Exception $e) {
            // do something...
        }
    }

    public function getRelatedOrderProductsByProductId($product_id, $order_statuses = [])
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT op.* FROM `{$DB_PREFIX}order_product` op
                                   JOIN `{$DB_PREFIX}order` o ON (op.order_id = o.order_id)
                                   WHERE op.product_id = '" . (int)$product_id . "' AND o.`source` NOT IN ('shopee', 'lazada')";

        if (!empty($order_statuses)) {
            $order_statuses = array_map(function ($os) {
                return trim($os);
            }, $order_statuses);

            $sql .= " AND o.order_status_id IN (" . implode(',', $order_statuses) . ")";
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function reCalculateOrderInfo($order_id)
    {
        if (empty($order_id)) {
            return;
        }

        $order = $this->getOrder($order_id);
        if (empty($order)) {
            return;
        }

        $order_product = $this->getOrderProducts($order_id);
        if (empty($order_product)) {
            return;
        }

        $total_amount = 0;
        foreach ($order_product as $item) {
            $total_amount += $item['total'];
        }

        $DB_PREFIX = DB_PREFIX;
        $shipping_fee = $order['shipping_fee']; // TODO: re-calc shipping_fee
        $discount_order = $order['discount']; // TODO: re-calc discount
        $total_pay = (float)$total_amount - (float)$discount_order + (float)$shipping_fee;

        $this->db->query("UPDATE `{$DB_PREFIX}order` 
                          SET shipping_fee = {$shipping_fee}, 
                          discount = {$discount_order}, 
                          total = '" . $this->db->escape($total_pay) . "', 
                          date_modified = NOW() 
                          WHERE order_id = " . (int)$order_id);
    }

    public function getStaffIdsByProductIds(array $product_ids)
    {
        $DB_PREFIX = DB_PREFIX;
        $staff_ids = [];

        $sql = "SELECT user_create_id FROM `{$DB_PREFIX}product` WHERE product_id IN (".implode(",", $product_ids).")";
        $query = $this->db->query($sql);

        foreach ($query->rows as $item) {
            if (!in_array($item['user_create_id'], $staff_ids) && $item['user_create_id'] != 1) { // 1 is user_id of admin
                $staff_ids[] = $item['user_create_id'];
            }
        }

        return $staff_ids;
    }

    public function getStaffEmailsByStaffId(array $staff_ids)
    {
        $DB_PREFIX = DB_PREFIX;
        $mail_staff = [];

        $sql = "SELECT email, user_id FROM `{$DB_PREFIX}user` WHERE user_id IN (".implode(",", $staff_ids).")";
        $query = $this->db->query($sql);

        foreach ($query->rows as $item) {
            $mail_staff[$item['user_id']][] = $item['email'];
        }

        return $mail_staff;
    }

    public function updateTransportDeliveryApi($order_id, $order_code, $transport_method, $service_name, $money_total, $exchange_weight, $transport_status, $transport_current_warehouse)
    {
        $sql = "UPDATE " . DB_PREFIX . "order SET 
        transport_order_code  = '" . $this->db->escape($order_code) . "', 
        transport_name = '" . $this->db->escape($transport_method) . "', 
        transport_service_name = '" . $this->db->escape($service_name) . "', 
        transport_money_total = '" . $this->db->escape($money_total) . "', 
        transport_weight = '" . $this->db->escape($exchange_weight) . "',
        transport_status = '" . $this->db->escape($transport_status) . "',
        transport_current_warehouse = '" . $this->db->escape($transport_current_warehouse) . "'
        WHERE order_id = '" . (int)$order_id . "'";
        $this->db->query($sql);
    }

    public function updateCollectionAmount($order_id, $collection_amount)
    {
        $DB_PREFIX = DB_PREFIX;
        $this->db->query("UPDATE `{$DB_PREFIX}order` 
                          SET collection_amount = " . (float)$collection_amount . " 
                          WHERE order_id = " . (int)$order_id);
    }

    public function getCampaignId($order_id)
    {
        if (!isset($order_id)) {
            return null;
        }

        $DB_PREFIX = DB_PREFIX;
        $result = $this->db->query("SELECT `campaign_id` FROM `{$DB_PREFIX}order_campaign` WHERE `order_id` = {$order_id}");

        if (isset($result->row['campaign_id'])) {
            return $result->row['campaign_id'];
        }

        return null;
    }

    public function getChannelId($order_id)
    {
        if (!isset($order_id)) {
            return null;
        }

        $DB_PREFIX = DB_PREFIX;
        $result = $this->db->query("SELECT `channel_id` FROM `{$DB_PREFIX}order_channel` WHERE `order_id` = {$order_id}");

        if (isset($result->row['channel_id'])) {
            return $result->row['channel_id'];
        }

        return null;
    }

    public function getOrderByOrderId($order_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}order` WHERE order_id = " . (int)$order_id;
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getOrderProductByOrderId($order_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $products = [];
        $sql = "SELECT * FROM `{$DB_PREFIX}order_product` WHERE order_id = " . (int)$order_id;

        $query = $this->db->query($sql);

        foreach ($query->rows as $item) {
            if (empty($item['product_id'])) {
                continue;
            }

            $this->load->model('catalog/product');
            $product = $this->model_catalog_product->getProductBasic($item['product_id']);
            $product_version = $this->getProductVersionInfo($item['product_id'], $item['product_version_id']);
            $product_attributes = $this->model_catalog_product->getProductAttributes($item['product_id']);
            $products[] = [
                'name' => $product['name'],
                'sku' => $product['sku'],
                'common_sku' => $product['common_sku'],
                'source' => $product['source'],
                'brand_shop_name' => isset($product['brand_shop_name']) ? $product['brand_shop_name'] : null,
                'version' => isset($product_version['version']) ? $product_version['version'] : '',
                'version_sku' => isset($product_version['sku']) ? $product_version['sku'] : '',
                'quantity' => $item['quantity'],
                'price' => (float)$item['price'],
                'discount' => (float)$item['discount'],
                'total' => (float)$item['total'],
                'tax' => (float)$item['tax'],
                'weight' => (float)$product['weight'],
                'description' => $product['description'],
                'sub_description' => $product['sub_description'],
                'sale_on_out_of_stock' => $product['sale_on_out_of_stock'],
                'multi_versions' => $product['multi_versions'],
                'original_price' => $product['compare_price'],
                'promotion_price' => $product['price'],
                'version_original_price' => isset($product_version['compare_price']) ? $product_version['compare_price'] : '',
                'version_promotion_price' => isset($product_version['price']) ? $product_version['price'] : '',
                'product_attributes' => json_encode($product_attributes),
                'barcode' => $product['barcode'],
                'version_barcode' => isset($product_version['barcode']) ? $product_version['barcode'] : '',
            ];
        }

        return $products;
    }

    public function getProductVersionInfo($product_id, $version_id)
    {
        if (empty($product_id) || empty($version_id)) {
            return  [];
        }

        $sql = "SELECT version, sku, price, compare_price, barcode from " . DB_PREFIX . "product_version as pv WHERE pv.product_id = '" . (int)$product_id . "' AND pv.product_version_id ='" . (int)$version_id . "'";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function sendToRabbitMq($order_id)
    {
        try {
            if (!empty($order_id)) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'order' => []
                ];
                $order = $this->getOrderByOrderId($order_id);
                if (isset($order) && !empty($order) && !empty($order['order_status_id']) && in_array($order['order_status_id'], [7,8,9,10])) {
                    $message['order'] = [
                        'order_code' => $order['order_code'],
                        'order_status' => $order['order_status_id'],
                        'total' => (float)$order['total'],
                        'order_product' => $this->getOrderProductByOrderId($order_id),
                        'customer' => $this->dataCustomer($order)
                    ];

                    $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'order', '', 'order', json_encode($message));
                }
            }
        } catch (Exception $exception) {
            $this->log->write('Send message order error: ' . $exception->getMessage());
        }
    }

    public function dataCustomer($order_info)
    {
        try {
            $this->load->model('customer/customer');

            $customer = $this->model_customer_customer->getCustomer($order_info['customer_id']);
            $customer_address = $this->model_customer_customer->getAddressByCustomerId($order_info['customer_id']);

            $this->load->model('localisation/vietnam_administrative');

            $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($customer_address['city']);
            $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($customer_address['district']);
            $ward = $this->model_localisation_vietnam_administrative->getWardByCode($customer_address['wards']);

            return [
                'customer_full_name' => (isset($customer['lastname']) ? $customer['lastname'] : '') . ' ' . (isset($customer['firstname']) ? $customer['firstname'] : ''),
                'customer_email' => (isset($customer['email']) ? $customer['email'] : ''),
                'customer_telephone' => (isset($customer['telephone']) ? $customer['telephone'] : ''),
                'customer_address' => $customer_address['address'] . (!empty($ward['name']) ? ', ' . $ward['name'] : '') . (!empty($district['name']) ? ', ' . $district['name'] : '') . (!empty($province['name']) ? ', ' . $province['name'] : ''),
            ];
        } catch (Exception $exception) {
            $this->log->write('SendToRabbitMq dataCustomer for order got error : ' . $exception->getMessage());

            return [];
        }
    }

    public function getCouponInfoByCode($coupon_code)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}coupon` 
                    WHERE `code` = '" . $this->db->escape($coupon_code)."' 
                        AND (`limit_times` = -1 OR `limit_times` > 0)
                        AND (`never_expired` = 1 OR (`date_start` <= NOW() AND `date_end` >= NOW()))";

        $query = $this->db->query($sql);
        return $query->row;
    }

    public function checkCouponProduct($coupon_id, $products, $data = false)
    {
        if (empty($coupon_id) || empty($products)) {
            return false;
        }

        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}coupon_product` WHERE `coupon_id` = {$coupon_id} ";

        $sql .= " AND (";
        foreach ($products as $key => $product) {
            if (!empty($product['product_version']) && !empty($product['product_version']['product_version_id'])) {
                if ($key == 0) {
                    $sql .= " (`product_id` = {$product['product_id']} AND `product_version_id` = {$product['product_version']['product_version_id']})";
                } else {
                    $sql .= " OR (`product_id` = {$product['product_id']} AND `product_version_id` = {$product['product_version']['product_version_id']})";
                }
            } else {
                if ($key == 0) {
                    $sql .= " (`product_id` = {$product['product_id']})";
                } else {
                    $sql .= " OR (`product_id` = {$product['product_id']})";
                }
            }
        }
        $sql .= " )";

        $query = $this->db->query($sql);
        return $data ? $query->rows : count($query->rows) == 0 ;
    }

    public function addOrderVoucher($order_id, $coupon, $value)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "INSERT INTO {$DB_PREFIX}order_voucher 
                    SET order_id = {$order_id}, 
                        voucher_id = {$coupon['coupon_id']}, 
                        description = '', 
                        code = '', 
                        from_name = '', 
                        from_email = '', 
                        to_name = '', 
                        to_email = '', 
                        voucher_theme_id = 1, 
                        message = '', 
                        amount = {$value}, 
                        date_added = NOW() 
                        ";

        $this->db->query($sql);

        return $this->db->getLastId();
    }

    public function updateLimitTimesCoupon($data)
    {
        if (empty($data[0]) || empty($data[1])) {
            return;
        }

        if (isset($data[1]['limit_times']) && $data[1]['limit_times'] == -1) {
            return;
        }

        if (!isset($data[1]['coupon_id'])) {
            return;
        }

        $DB_PREFIX = DB_PREFIX;
        $sql = "UPDATE `{$DB_PREFIX}coupon` 
                    SET `limit_times` = limit_times - 1 
                        WHERE `coupon_id` = {$data[1]['coupon_id']}";
        $this->db->query($sql);
    }

    public function getAddOnDealId($product_id, $product_version_id = 0)
    {
        $this->load->model('discount/add_on_deal');
        $result = $this->model_discount_add_on_deal->getAddOnDealByAddOnProductId($product_id, $product_version_id);

        if (!empty($result['add_on_deal_id'])) {
            return $result['add_on_deal_id'];
        }

        return null;
    }
}
