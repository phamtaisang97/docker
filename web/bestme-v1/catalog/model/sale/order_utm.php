<?php

class ModelSaleOrderUtm extends Model
{
    /**
     * @param int $order_id
     * @param array $utm format as
     * [
     *     [
     *         "key" => "campaign",
     *         "value" => "Campaign 1",
     *     ],
     *     ...
     * ]
     */
    public function addOrderUtm($order_id, array $utm)
    {
        if (!$order_id || empty($utm)) {
            return;
        }

        // build list for updating
        $utm_for_sql = [];
        foreach ($utm as $u) {
            if (!isset($u['key']) || !isset($u['value'])) {
                continue;
            }

            $utm_for_sql[] = "('{$order_id}', '', '{$u['key']}', '{$u['value']}', NOW(), NOW())";
        }

        // remove all first
        $DB_PREFIX = DB_PREFIX;
        $this->db->query("DELETE FROM `{$DB_PREFIX}order_utm` WHERE `order_id` = " . (int)$order_id);

        // add new
        $this->db->query("INSERT INTO `{$DB_PREFIX}order_utm` (`order_id`, `code`, `key`, `value`, `date_added`, `date_modified`) 
                          VALUES " . implode(', ', $utm_for_sql));
    }
}
