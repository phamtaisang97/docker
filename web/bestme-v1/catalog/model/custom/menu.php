<?php

class ModelCustomMenu extends Model
{
    const MENU_TYPE_CATEGORY = 1;
    const MENU_TYPE_COLLECTION = 2;
    const MENU_TYPE_PRODUCT = 3;
    const MENU_TYPE_URL = 4;
    const MENU_TYPE_BUILT_IN_PAGE = 5;
    const MENU_TYPE_BLOG = 6;

    public function getMenuNameById($menu_id)
    {
        $sql = "SELECT md.name as name FROM " . DB_PREFIX . "menu as m LEFT JOIN " . DB_PREFIX . "menu_description as md ON (m.menu_id = md.menu_id) WHERE m.menu_id = " . (int)$menu_id . " AND md.language_id = " . (int)$this->config->get('config_language_id');
        $query = $this->db->query($sql);
        $name = isset($query->row['name']) ? $query->row['name'] : '';

        return $name;
    }

    public function getMenuById($menu_id)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "menu as m 
                LEFT JOIN " . DB_PREFIX . "menu_description as md ON (m.menu_id = md.menu_id) 
                WHERE m.parent_id = " . (int)$menu_id . " 
                AND md.language_id = " . (int)$this->config->get('config_language_id') . " 
                ORDER BY m.menu_id ASC";
        $query = $this->db->query($sql);

        $menu = [];
        foreach ($query->rows as $row) {
            $items = $this->getMenuById($row['menu_id']);
            $url = $this->buildMenuUrl($row['menu_type_id'], $row['target_value']);

            if ($url === false){
                continue;
            }

            if (empty($items) && $row['menu_type_id'] == self::MENU_TYPE_CATEGORY) {
                $items = $this->buildMenuTreeForCategory($row['target_value']);
            }

            $menu[] = [
                'menu_id' => $row['menu_id'],
                'menu_type_id' => $row['menu_type_id'],
                'target_value' => $row['target_value'],
                'name' => $row['name'],
                'items' => $items,
                'text' => $row['name'],
                'url' => $url
            ];

        }

        return $menu;
    }

    public function getAllMenu()
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "menu as m LEFT JOIN " . DB_PREFIX . "menu_description as md ON (m.menu_id = md.menu_id) WHERE m.parent_id IS NULL AND md.language_id = " . (int)$this->config->get('config_language_id') . " ORDER BY m.menu_id ASC";
        $query = $this->db->query($sql);

        $menu = array();
        foreach ($query->rows as $row) {
            $items = $this->getMenuById($row['menu_id']);
            $menu[] = array(
                'menu_id' => $row['menu_id'],
                'menu_type_id' => $row['menu_type_id'],
                'target_value' => $row['target_value'],
                'name' => $row['name'],
                'items' => $items
            );

        }

        return $menu;
    }

    private function buildMenuUrl($type_id, $value)
    {
        $url = '';

        if ($type_id == self::MENU_TYPE_COLLECTION) {
            $url = $this->url->link('common/shop', 'collection=' . $value . '&is_menu=1');
        } else if ($type_id == self::MENU_TYPE_CATEGORY) {
            $url = $this->url->link('common/shop', 'path=' . $value);
        } else if ($type_id == self::MENU_TYPE_PRODUCT) {
            $url = $this->url->link('product/product', 'product_id=' . $value);
        } else if ($type_id == self::MENU_TYPE_URL) {
            $url = $value;
        } else if ($type_id == self::MENU_TYPE_BUILT_IN_PAGE) {
            $target = str_replace('?route=', '', $value);
            $url = $this->url->link($target);
        } else if ($type_id == self::MENU_TYPE_BLOG) {
            // check show/hide blog category : if status == 0 : hide
            $this->load->model('blog/blog');
            $blog_category = $this->model_blog_blog->getBlogCategoryByBlogId($value);
            if (is_array($blog_category) && array_key_exists('status', $blog_category) && !$blog_category['status']){
                return false;
            }
            // end check

            $url = $this->url->link('blog/blog', 'blog_category_id=' . $value);
        }

        return $url;
    }

    /**
     * build Menu Tree For Category
     *
     * @param $category_id
     * @return array
     */
    private function buildMenuTreeForCategory($category_id)
    {
        $this->load->model('catalog/category');
        $category = $this->model_catalog_category->getCategory($category_id);
        if (empty($category)) {
            return [];
        }

        $categories = $this->model_catalog_category->getCategoriesRecursive($category_id, 3);
        if (empty($categories) || !is_array($categories)) {
            return [];
        }

        $categories = array_map(function ($cat) {
            $url = $this->buildMenuUrl(self::MENU_TYPE_CATEGORY, $cat['category_id']);

            return [
                'menu_id' => $cat['category_id'],
                'parent_menu_id' => $cat['parent_id'],
                'menu_type_id' => self::MENU_TYPE_CATEGORY,
                'target_value' => $cat['category_id'],
                'name' => $cat['name'],
                //'items' => [], // later
                'text' => $cat['name'],
                'url' => $url
            ];
        }, $categories);

        $items = pickChildrenFromFlatArray($categories, $category_id, $deep = 3,
            $ITEM_KEY = 'menu_id', $PARENT_KEY = 'parent_menu_id', $CHILDREN_KEY = 'items');

        return $items;
    }
}