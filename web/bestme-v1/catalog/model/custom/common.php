<?php
class ModelCustomCommon extends Model {

    public function getListMenu()
    {
        $sql = "SELECT m.id AS group_menu_id, md.name AS name FROM " . DB_PREFIX . "novaon_group_menu m LEFT JOIN " . DB_PREFIX . "novaon_group_menu_description md ON (m.id = md.group_menu_id) WHERE md.language_id = '" . (int)$this->config->get('config_language_id') . "'";
        $query = $this->db->query($sql);
        $group_menu = $query->rows;
        $menu_item = $this->db->query("SELECT m.id AS menu_item_id, md.name AS name FROM " . DB_PREFIX . "novaon_menu_item m LEFT JOIN " . DB_PREFIX . "novaon_menu_item_description md ON (m.id = md.menu_item_id) WHERE md.language_id = '" . (int)$this->config->get('config_language_id') . "'")->rows;
        // var_dump($menu_item);die;
        $arrayMenu = $arrayMenuItem = array();
        foreach ($menu_item as $key => $value) {
            $menu_item_id = $value['menu_item_id'];
            $menu_item_name = $value['name'];
            $arrayMenuItem[] = [
                'id' => 'menu_item-'.$menu_item_id,
                'name' => $menu_item_name,
            ];
        }
        foreach ($group_menu as $key => $value) {
            $group_menu_id = $value['group_menu_id'];
            $group_menu_name = $value['name'];
            $arrayMenu[] = [
                // 'group_menu-'.$group_menu_id => $group_menu_name,
                'id' => 'group_menu-'.$group_menu_id,
                'name' => $group_menu_name,
            ];
        }
        $data = array_merge($arrayMenu, $arrayMenuItem);
        // var_dump($data);die;
        return $data;
    }

    public function getCategoryMenu($category_id)
    {
        if ($category_id == '') {
            return array();
        }
        $query = $this->db->query("
            SELECT main_name, main_id FROM " . DB_PREFIX . "novaon_relation_table WHERE child_name = 'category' AND child_id = '".$category_id."'
        ");
        $data = array();
        foreach ($query->rows as $value) {
            $sql = "SELECT md.name AS name FROM " . DB_PREFIX . "novaon_".$value['main_name']." m LEFT JOIN " . DB_PREFIX . "novaon_".$value['main_name']."_description md ON (m.id = md.".$value['main_name']."_id) WHERE md.language_id = '" . (int)$this->config->get('config_language_id') . "' AND m.id = '".$value['main_id']."' ";
            $data[] = ['id' => $value['main_name'].'-'.$value['main_id'],
                'name' => $this->db->query($sql)->row['name']
            ];
        }
        return $data;
    }

    public function getSlugUnique($slug, $tableName = null)
    {
        //we only bother doing this if there is a conflicting slug already
        $sql = "
            SELECT keyword FROM ".DB_PREFIX."seo_url WHERE keyword  LIKE '".$slug."%'
        ";
        if (isset($tableName)) {
            $sql .= " AND table_name = '".$tableName."' ";
        }
        $query = $this->db->query($sql);
        $data = $query->rows;
        $slugs = [];
        foreach ($data as $value) {
            $slugs[] = $value['keyword'];
        }
        $count = count($data);

        if($count != 0 && in_array($slug, $slugs)){
            $max = 0;
            //keep incrementing $max until a space is found
            while(in_array( ($slug . '-' . ++$max ), $slugs) );
            //update $slug with the appendage
            $slug .= '-' . $max;
        }
        return $slug;
    }
    public function createSlug($str, $delimiter = '-'){

        $str = $this->to_slug($str);
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;

    }
    public function to_slug($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        return $str;
    }

    public function getListGroupMenu($language_id = null)
    {
        if (!$language_id) {
            $language_id = $this->config->get('config_language_id');
        }
        $sql = "SELECT m.id AS id, md.name AS name FROM " . DB_PREFIX . "novaon_group_menu m LEFT JOIN " . DB_PREFIX . "novaon_group_menu_description md ON (m.id = md.group_menu_id) WHERE md.language_id = '" . (int)$language_id . "'";
        $data = $this->db->query($sql)->rows;
        return $data;

    }
    public function getListProduct($language_id = null)
    {
        if (!$language_id) {
            $language_id = $this->config->get('config_language_id');
        }
        $query = $this->db->query("SELECT p.product_id AS id, pd.name AS name FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE pd.language_id = '" . (int)$language_id . "' GROUP BY p.product_id");
        $data = $query->rows;
        return $data;
    }

    public function getNameRecordById($id, $tableName, $field, $prefix = null, $name = null)
    {
        if (isset($prefix)) {
            $pre = $prefix;
        } else {
            $pre = null;
        }
        $language_id = $this->config->get('config_language_id');
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX .$pre. $tableName. "_description WHERE ".$field." = '" . (int)$id . "' AND language_id = '". $language_id ."'");
        if ($name) {
            return $query->row[$name];
        }
        if (isset($query->row['name'])) {
            return $query->row['name'];
        }
        return null;
    }

    public function getAppendMenuItem($menu_item_id, $category = null)
    {
        $sql = "
            SELECT * FROM " . DB_PREFIX . "novaon_relation_table WHERE main_name = 'menu_item' AND main_id = '".$menu_item_id."' AND redirect = 0
        ";
        $listCategory = $array = [];
        $result = $this->db->query($sql)->rows;
        foreach ($result as $key => $value) {
            if ($value['child_name'] == 'category') {
                $array[$key]['text'] = $value['title'];
                $listCategory[$key]['category_id'] = $value['child_id'];
                $listCategory[$key]['name'] = $this->getNameRecordById($value['child_id'], 'category', 'category_id');
                // $array[$key]['url'] = 'index.php?route=product/category&path=25_28_36';
                $array[$key]['url'] = $this->getUrlByTable('category', $value['child_id']);
                $array[$key]['id'] = $value['child_id'];
                $array[$key]['items'] = [];
            }
            elseif ($value['child_name'] == 'product') {
                $array[$key]['text'] = $value['title'];
                $array[$key]['url'] = $this->getUrlByTable('product', $value['child_id']);
                $array[$key]['id'] = $value['child_id'];
                $array[$key]['items'] = [];
            }
            elseif ($value['child_name'] == '') {
                $array[$key]['text'] = $value['title'];
                $array[$key]['url'] = $value['url'];
                $array[$key]['id'] = $value['child_id'];
                $array[$key]['items'] = [];
            }
            else{
                $array[$key]['text'] = $value['title'];
                $array[$key]['url'] = $value['url'];
                $array[$key]['id'] = $value['child_id'];
                $array[$key]['items'] = [];
            }
        }
        if ($category) {
            return $listCategory;
        }
        return $array;
    }

    public function getUrlByTable($tableName, $id)
    {
        if ($tableName == 'product') {
            $data = $this->url->link('product/product', '&product_id='.$id);
            return $data;
        }
        if ($tableName == 'category') {
            // $data = 'index.php?route=product/category&path=25_28_36';
            $data = $this->db->query("SELECT path_id FROM " . DB_PREFIX . "category_path WHERE category_id = '".$id."' ORDER BY level ASC")->rows;
            $result = '';
            foreach ($data as $key => $value) {
                if ($key == 0) {
                    $result = $value['path_id'];
                } else {
                    $result = $result.'_'.$value['path_id'];
                }
            }
            $data = $this->url->link('product/category', '&path='.$result);
            return $data;
        }

    }

    public function getListMenuItemByGroupMenuId($group_menu_id, $category = null)
    {
        $data = $this->db->query("SELECT * FROM " . DB_PREFIX . "novaon_relation_table WHERE main_name = 'group_menu' AND main_id = '".$group_menu_id."'")->rows;
        $listCategory = $array = [];
        foreach ($data as $key => $value) {
            if ($value['child_name'] == 'menu_item') {
                $sql = "
                    SELECT * FROM " . DB_PREFIX . "novaon_relation_table WHERE main_name = 'menu_item' AND main_id = '".$value['child_id']."' AND redirect = 1
                ";
                $result = $this->db->query($sql)->row;
                if (empty($result)){
                    continue;
                }
                if (!empty($result['url']) && strpos($result['url'], '?route=') > -1) {
                    $target = str_replace('?route=', '', $result['url']);
                    $url = $this->url->link($target);
                } else {
                    $url = $result['url'];
                }
                $array[$key]['text'] = $this->getNameRecordById($value['child_id'], 'menu_item', 'menu_item_id','novaon_');
                $array[$key]['url'] = $url;
                $array[$key]['id'] = $result['child_id'];
                $array[$key]['items'] = $this->getAppendMenuItem($value['child_id']);
                if ($category) {
                    $listCategory[$key]['categories'] = $this->getAppendMenuItem($value['child_id'], $category);
                }

            }
            elseif ($value['child_name'] == 'category') {
                $array[$key]['text'] = $this->getNameRecordById($value['child_id'], 'category', 'category_id');
                $array[$key]['url'] = $this->getUrlByTable('category', $value['child_id']);
                $array[$key]['id'] = $value['child_id'];
                $array[$key]['items'] = [];
            }
            else{
                $array[$key]['text'] = 'text';
                $array[$key]['url'] = 'url';
                $array[$key]['items'] = [];
                $array[$key]['id'] = null;
            }
        }
        if ($category) {
            return $listCategory;
        }
        return $array;
    }

    public function getAllByMainId($mainName, $mainId, $redirect = null)
    {
        $sql = "SELECT * FROM " .DB_PREFIX. "novaon_relation_table rt WHERE main_name = '".$mainName."' AND main_id = ". $mainId ." ";
        if (isset($redirect)) {
            $sql .= " AND redirect = '".$redirect."'";
        }
        $query = $this->db->query($sql);
        $result = $query->rows;
        $data = array();
        foreach ($result as $key => $value) {
            if ($value['child_name'] == 'menu_item') {
                $url_link = '';
                $data[$key]['name'] = $this->db->query(" SELECT * FROM " .DB_PREFIX. "novaon_menu_item_description WHERE menu_item_id = ". $value['child_id'] ." AND language_id = '" . (int)$this->config->get('config_language_id') . "'")->row['name'];
                $menu_item_data = $this->db->query("SELECT * FROM " .DB_PREFIX. "novaon_relation_table WHERE main_name = 'menu_item' AND main_id = '".(int)$value['child_id']."' AND redirect = 1");
                $data[$key]['type_id'] = isset($menu_item_data->row['type_id']) ? $menu_item_data->row['type_id'] : $value['type_id'];
                $child_append_name_table = isset($menu_item_data->row['child_name']) ? $menu_item_data->row['child_name'] : $value['child_name'];
                $child_append_name = '';
                $url_link = isset($menu_item_data->row['url']) ? $menu_item_data->row['url'] : $value['url'];
                $data[$key]['child_append_name'] = $child_append_name;
                $data[$key]['child_append_id'] = isset($menu_item_data->row['child_id']) ? $menu_item_data->row['child_id'] : $value['child_id'];
                $data[$key]['child_id'] = $value['child_id'];
                $data[$key]['url_link'] = $url_link;
                $data[$key]['relation_id'] = $value['id'];
                $data[$key]['title'] = $value['title'];
            }
        }

        return $data;
    }

}




