<?php

class ModelAccountAddress extends Model
{
    public function addAddress($customer_id, $data)
    {
        $DB_PREFIX = DB_PREFIX;
        $this->db->query("INSERT INTO {$DB_PREFIX}address 
                            SET customer_id = '" . (int)$customer_id . "', 
                                firstname = '" . $this->db->escape($data['firstname']) . "', 
                                lastname = '" . $this->db->escape($data['lastname']) . "', 
                                company = '" . $this->db->escape($data['company']) . "', 
                                address_1 = '" . $this->db->escape($data['address_1']) . "', 
                                address_2 = '" . $this->db->escape($data['address_2']) . "', 
                                postcode = '" . $this->db->escape($data['postcode']) . "', 
                                city = '" . $this->db->escape($data['city']) . "', 
                                zone_id = '" . (int)$data['zone_id'] . "', 
                                country_id = '" . (int)$data['country_id'] . "', 
                                custom_field = '" . $this->db->escape(isset($data['custom_field']['address']) ? json_encode($data['custom_field']['address']) : '') . "'");

        $address_id = $this->db->getLastId();

        if (!empty($data['default'])) {
            $this->db->query("UPDATE {$DB_PREFIX}customer 
                                SET address_id = '" . (int)$address_id . "' 
                                WHERE customer_id = '" . (int)$customer_id . "'");
        }

        return $address_id;
    }

    public function editAddress($address_id, $data)
    {
        $DB_PREFIX = DB_PREFIX;
        $this->db->query("UPDATE {$DB_PREFIX}address 
                            SET firstname = '" . $this->db->escape($data['firstname']) . "', 
                            lastname = '" . $this->db->escape($data['lastname']) . "', 
                            company = '" . $this->db->escape($data['company']) . "', 
                            address_1 = '" . $this->db->escape($data['address_1']) . "', 
                            address_2 = '" . $this->db->escape($data['address_2']) . "', 
                            postcode = '" . $this->db->escape($data['postcode']) . "', 
                            city = '" . $this->db->escape($data['city']) . "', 
                            zone_id = '" . (int)$data['zone_id'] . "', 
                            country_id = '" . (int)$data['country_id'] . "', 
                            custom_field = '" . $this->db->escape(isset($data['custom_field']['address']) ? json_encode($data['custom_field']['address']) : '') . "' 
                            WHERE address_id  = '" . (int)$address_id . "' 
                              AND customer_id = '" . (int)$this->customer->getId() . "'");

        if (!empty($data['default'])) {
            $this->db->query("UPDATE {$DB_PREFIX}customer 
                                SET address_id = '" . (int)$address_id . "' 
                                WHERE customer_id = '" . (int)$this->customer->getId() . "'");
        }
    }

    public function addAddressNew($customer_id, $data)
    {
        $DB_PREFIX = DB_PREFIX;
        $this->db->query("INSERT INTO {$DB_PREFIX}address 
		                    SET customer_id = '" . (int)$customer_id . "', 
		                        firstname = '" . $this->db->escape($data['firstname']) . "', 
		                        lastname = '" . $this->db->escape($data['lastname']) . "', 
		                        company = '" . $this->db->escape($data['company']) . "', 
		                        district = '" . $this->db->escape($data['district']) . "', 
		                        address = '" . $this->db->escape($data['address']) . "', 
		                        postcode = '" . $this->db->escape($data['postcode']) . "', 
		                        wards = '" . $this->db->escape($data['wards']) . "', 
		                        phone = '" . $this->db->escape($data['phone']) . "', 
		                        city = '" . $this->db->escape($data['city']) . "'");

        $address_id = $this->db->getLastId();

        if (!empty($data['default'])) {
            $this->db->query("UPDATE {$DB_PREFIX}customer 
                                SET address_id = '" . (int)$address_id . "' 
                                WHERE customer_id = '" . (int)$customer_id . "'");
        }

        return $address_id;
    }

    public function editAddressNew($address_id, $data)
    {
        $DB_PREFIX = DB_PREFIX;
        $this->db->query("UPDATE {$DB_PREFIX}address 
                            SET firstname = '" . $this->db->escape($data['firstname']) . "', 
                                lastname = '" . $this->db->escape($data['lastname']) . "', 
                                company = '" . $this->db->escape($data['company']) . "', 
                                address = '" . $this->db->escape($data['address']) . "', 
                                district = '" . $this->db->escape($data['district']) . "', 
                                postcode = '" . $this->db->escape($data['postcode']) . "', 
                                city = '" . $this->db->escape($data['city']) . "'  
                                WHERE address_id  = '" . (int)$address_id . "' 
                                  AND customer_id = '" . (int)$this->customer->getId() . "'");

        if (!empty($data['default'])) {
            $this->db->query("UPDATE {$DB_PREFIX}customer 
                                SET address_id = '" . (int)$address_id . "' 
                                WHERE customer_id = '" . (int)$this->customer->getId() . "'");
        }
    }

    public function deleteAddress($address_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "address 
                          WHERE address_id = '" . (int)$address_id . "' 
                          AND customer_id = '" . (int)$this->customer->getId() . "'");
    }

    public function getAddress($address_id)
    {
        $address_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address 
                                           WHERE address_id = '" . (int)$address_id . "' 
                                             AND customer_id = '" . (int)$this->customer->getId() . "'");

        if ($address_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` 
                                               WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

            if ($country_query->num_rows) {
                $country = $country_query->row['name'];
                $iso_code_2 = $country_query->row['iso_code_2'];
                $iso_code_3 = $country_query->row['iso_code_3'];
                $address_format = $country_query->row['address_format'];
            } else {
                $country = '';
                $iso_code_2 = '';
                $iso_code_3 = '';
                $address_format = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` 
                                            WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

            if ($zone_query->num_rows) {
                $zone = $zone_query->row['name'];
                $zone_code = $zone_query->row['code'];
            } else {
                $zone = '';
                $zone_code = '';
            }

            $address_data = array(
                'address_id' => $address_query->row['address_id'],
                'firstname' => $address_query->row['firstname'],
                'lastname' => $address_query->row['lastname'],
                'company' => $address_query->row['company'],
                'address' => $address_query->row['address'],
                'district' => $address_query->row['district'],
                'phone' => $address_query->row['phone'],
                'address_1' => isset($address_query->row['address_1']) ? $address_query->row['address_1'] : '',
                'address_2' => isset($address_query->row['address_2']) ? $address_query->row['address_2'] : '',
                'postcode' => $address_query->row['postcode'],
                'city' => $address_query->row['city'],
                'wards' => $address_query->row['wards'],
                'zone_id' => $address_query->row['zone_id'],
                'zone' => $zone,
                'zone_code' => $zone_code,
                'country_id' => $address_query->row['country_id'],
                'country' => $country,
                'iso_code_2' => $iso_code_2,
                'iso_code_3' => $iso_code_3,
                'address_format' => $address_format,
                'custom_field' => json_decode($address_query->row['custom_field'], true)
            );

            return $address_data;
        } else {
            return false;
        }
    }

    public function getCountAddessAccount()
    {
        $query = $this->db->query("SELECT COUNT(DISTINCT customer_id) AS total FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "'");
        if (isset($query->row['total'])) {
            return $query->row['total'];
        } else {
            return 0;
        }
    }

    public function getCountPhoneAccount($phone)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE telephone = '" . $phone . "'");
        if (isset($query->row['total'])) {
            return $query->row['total'];
        } else {
            return 0;
        }
    }

    public function getAddessAccount()
    {
        $sql = "SELECT ad.*, ad.address_id as address_id, ad.customer_id as customer_id FROM " . DB_PREFIX . "address ad left join " . DB_PREFIX . "customer as cu ON (ad.address_id = cu.address_id) WHERE ad.customer_id = '" . (int)$this->customer->getId() . "' order by cu.address_id DESC";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getAddessMultiAccount()
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "'";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function addAddressCustomer($customer_id, $data)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', address = '" . $this->db->escape($data['address']) . "', phone = '" . $this->db->escape($data['phone']) . "', city = '" . $this->db->escape($data['city_delivery']) . "', district = '" . $this->db->escape($data['district_delivery']) . "', wards = '" . $this->db->escape($data['ward_delivery']) . "'");

        $address_id = $this->db->getLastId();

        if (!empty($data['default'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
        }

        return $address_id;
    }

    public function editAddressCustomer($address_id, $data)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "address SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', address = '" . $this->db->escape($data['address']) . "', phone = '" . $this->db->escape($data['phone']) . "', city = '" . $this->db->escape($data['city_delivery']) . "', district = '" . $this->db->escape($data['district_delivery']) . "', wards = '" . $this->db->escape($data['ward_delivery']) . "' WHERE address_id = '" . (int)$address_id . "'");

        if (!empty($data['default'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
        }

        return $address_id;
    }

    public function getAddresses()
    {
        $address_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "'");

        foreach ($query->rows as $result) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$result['country_id'] . "'");

            if ($country_query->num_rows) {
                $country = $country_query->row['name'];
                $iso_code_2 = $country_query->row['iso_code_2'];
                $iso_code_3 = $country_query->row['iso_code_3'];
                $address_format = $country_query->row['address_format'];
            } else {
                $country = '';
                $iso_code_2 = '';
                $iso_code_3 = '';
                $address_format = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$result['zone_id'] . "'");

            if ($zone_query->num_rows) {
                $zone = $zone_query->row['name'];
                $zone_code = $zone_query->row['code'];
            } else {
                $zone = '';
                $zone_code = '';
            }

            $address_data[$result['address_id']] = array(
                'address_id' => $result['address_id'],
                'firstname' => $result['firstname'],
                'lastname' => $result['lastname'],
                'company' => $result['company'],
                'address_1' => isset($result['address_1']) ? $result['address_1'] : '',
                'address_2' => isset($result['address_2']) ? $result['address_2'] : '',
                'postcode' => $result['postcode'],
                'city' => $result['city'],
                'zone_id' => $result['zone_id'],
                'zone' => $zone,
                'zone_code' => $zone_code,
                'country_id' => $result['country_id'],
                'country' => $country,
                'iso_code_2' => $iso_code_2,
                'iso_code_3' => $iso_code_3,
                'address_format' => $address_format,
                'custom_field' => json_decode($result['custom_field'], true)

            );
        }

        return $address_data;
    }

    public function getAddressesCustom()
    {
        $address_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "'");

        foreach ($query->rows as $result) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$result['country_id'] . "'");

            if ($country_query->num_rows) {
                $country = $country_query->row['name'];
                $iso_code_2 = $country_query->row['iso_code_2'];
                $iso_code_3 = $country_query->row['iso_code_3'];
                $address_format = $country_query->row['address_format'];
            } else {
                $country = '';
                $iso_code_2 = '';
                $iso_code_3 = '';
                $address_format = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$result['zone_id'] . "'");

            if ($zone_query->num_rows) {
                $zone = $zone_query->row['name'];
                $zone_code = $zone_query->row['code'];
            } else {
                $zone = '';
                $zone_code = '';
            }

            //get address total
            $this->load->model('localisation/vietnam_administrative');
            $province = isset($result['city']) ? $this->model_localisation_vietnam_administrative->getProvinceByCode($result['city']) : '';
            $district = isset($result['district']) ? $this->model_localisation_vietnam_administrative->getDistrictByCode($result['district']) : '';
            $wards = isset($result['wards']) ? $this->model_localisation_vietnam_administrative->getWardByCode($result['wards']) : '';
            $address_total = convertAddressOrderList(isset($result['address']) ? $result['address'] : '', isset($province['name']) ? $province['name'] : '', isset($district['name_with_type']) ? $district['name_with_type'] : '', isset($wards['name_with_type']) ? $wards['name_with_type'] : '');
            $address_data[$result['address_id']] = array(
                'address_id' => $result['address_id'],
                'firstname' => $result['firstname'],
                'lastname' => $result['lastname'],
                'company' => $result['company'],
                'address' => $result['address'],
                'phone' => $result['phone'],
                'postcode' => $result['postcode'],
                'city' => $result['city'],
                'zone_id' => $result['zone_id'],
                'zone' => $zone,
                'zone_code' => $zone_code,
                'country_id' => $result['country_id'],
                'country' => $country,
                'iso_code_2' => $iso_code_2,
                'iso_code_3' => $iso_code_3,
                'address_format' => $address_format,
                'custom_field' => json_decode($result['custom_field'], true),
                'address_total' => $address_total,

            );
        }

        return $address_data;
    }

    public function getTotalAddresses()
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "'");

        return $query->row['total'];
    }
}
