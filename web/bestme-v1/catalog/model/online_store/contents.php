<?php

class ModelOnlineStoreContents extends Model
{
    use RabbitMq_Trait;

    public function getContent($content_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "page_contents WHERE page_id = " . (int)$content_id);

        return $query->row;
    }

    public function getContents($data = array())
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "page_contents";
        $sql .= " WHERE 1 ";

        $implode = array();

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $implode[] = "status = '" . (int)$data['filter_status'] . "'";
        }

        if (isset($data['filter_title']) && $data['filter_title'] !== '') {
            $implode[] = "title LIKE '" . $this->db->escape($data['filter_title']) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }
        $sort_data = array(
            'date_added',
            'status'
        );
        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY date_added";
        }

        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " DESC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalContents($data = array())
    {
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "page_contents";
        $sql .= " WHERE 1 ";

        $implode = array();

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $implode[] = "status = '" . (int)$data['filter_status'] . "'";
        }

        if (isset($data['filter_title']) && $data['filter_title'] !== '') {
            $implode[] = "title LIKE '" . $this->db->escape($data['filter_title']) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getContentsAPI($data = array())
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "page_contents";

        $sql .= " WHERE 1 ";

        $page = isset($data['page']) ? (int)$data['page'] : 1;
        if (isset($data['source']) && !empty($data['source'])) {
            $sql .= " AND source = '". $this->db->escape($data['source']) ."' ";
        }
        if (isset($data['page']) && !empty($data['page'])) {
            $data = [
                'start' => ((int)$data['page'] - 1) * ((int)isset($data['limit']) ? $data['limit'] : 15),
                'limit' => isset($data['limit']) ? $data['limit'] : 15
            ];
        }
        $sql .= " ORDER BY date_added DESC";
        // limit
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 15;
            }
            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);
        $result = [];
        foreach ($query->rows as $row) {
            $result [] = [
                "page_id" => $row['page_id'],
                "title" => $row['title'],
                "description" => $row['description'],
                "seo_title" => $row['seo_title'],
                "seo_description" => $row['seo_description'],
                "source" => $row['source'],
                "status" => $row['status'],
                "alias" => $this->getSeoUrlByQuery('page_id=' . $row['page_id']),
                "date_added" => $row['date_added'],

            ];
        }
        return [
            'total' => $this->getTotalContents($data),
            'page' => $page,
            'records' => $result
        ];
    }

    public function getSeoUrlByQuery($query) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "seo_url` WHERE query = '" . $this->db->escape($query) . "' AND language_id = " . (int)$this->config->get('config_language_id'));

        $data = $query->row;
        if (count($data) > 0) {
            return $data['keyword'];
        }

        return null;
    }

    public function addContent($data)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "page_contents SET title = '" . $this->db->escape($data['title']) . "', seo_title = '" . $this->db->escape($data['seo_title']) . "', description = '" . $this->db->escape($data['description']) . "', seo_description = '" . $this->db->escape($data['seo_description']) . "',source = '" . $this->db->escape(isset($data['source']) ? $data['source'] : '') . "', status = " . (int)$data['status'] . ", date_added = NOW()");

        $last_id = $this->db->getLastId();

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        // TODO check keyword unique
        // SEO URL
        if (isset($data['alias'])) {
            $keyword = $this->to_slug($data['alias']);
            $store_id = 0;
            $keyword = $this->getSlugUnique($keyword);
            foreach ($languages as $language) {
                $language_id = $language['language_id'];
                if (trim($keyword)) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'page_id=" . (int)$last_id . "', keyword = '" . $this->db->escape($keyword) . "'");
                }
            }
        }

        // send product to rabbit mq
        try {
            $this->sendToRabbitMq($last_id);
        } catch (Exception $exception) {
            $this->log->write('Send message add policy error: ' . $exception->getMessage());
        }

        return $last_id;
    }

    public function editContent($id, $data)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "page_contents SET title = '" . $this->db->escape($data['title']) . "', seo_title = '" . $this->db->escape($data['seo_title']) . "', description = '" . $this->db->escape($data['description']) . "', seo_description = '" . $this->db->escape($data['seo_description']) . "', status = " . (int)$data['status'] . " WHERE page_id = " . (int)$id);

        // TODO check keyword unique
        // SEO URL
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'page_id=" . (int)$id . "'");
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        if (isset($data['alias'])) {
            $keyword = $this->to_slug($data['alias']);
            $store_id = 0;
            $keyword = $this->getSlugUnique($keyword);
            foreach ($languages as $language) {
                $language_id = $language['language_id'];
                if (trim($keyword)) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'page_id=" . (int)$id . "', keyword = '" . $this->db->escape($keyword) . "'");
                }
            }
        }
        // send product to rabbit mq
        try {
            $this->sendToRabbitMq($id);
        } catch (Exception $exception) {
            $this->log->write('Send message add policy error: ' . $exception->getMessage());
        }

    }

    public function checkPolicyExist($title) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "page_contents` WHERE `title` = '" . $this->db->escape($title) . "'");
        if (isset($query->row['page_id'])) {
            return $query->row['page_id'];
        }

        return false;
    }

    public function getSlugUnique($slug)
    {
        //we only bother doing this if there is a conflicting slug already
        $sql = "
            SELECT keyword FROM " . DB_PREFIX . "seo_url WHERE keyword  LIKE '" . $slug . "%'
        ";
        $query = $this->db->query($sql);
        $data = $query->rows;
        $slugs = [];
        foreach ($data as $value) {
            $slugs[] = $value['keyword'];
        }
        $count = count($data);

        if ($count != 0 && in_array($slug, $slugs)) {
            $max = 0;
            //keep incrementing $max until a space is found
            while (in_array(($slug . '-' . ++$max), $slugs)) ;
            //update $slug with the appendage
            $slug .= '-' . $max;
        }
        return $slug;
    }

    public function to_slug($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        return $str;
    }

    public function getAliasById($page_id, $store_id = 0)
    {
        $query = $this->db->query("SELECT keyword FROM " . DB_PREFIX . "seo_url WHERE query = 'page_id=" . (int)$page_id . "' AND store_id = '" . (int)$store_id . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function buildPolicyForRabbitMq($policy_id)
    {
        try {
            $policy = $this->getContent($policy_id);
            $alias = $this->getAliasById($policy['page_id']);
            $record = [];
            $record = [
                "id" => $policy['page_id'],
                "title" => $policy['title'],
                "content" => $policy['description'],
                "seo_title" => $policy['seo_title'],
                "seo_description" => $policy['seo_description'],
                "alias" => $alias['keyword'],
                "status" => $policy['status'],
            ];
            return $record;
        } catch (Exception $e) {
            return [
                'code' => 500,
                'message' => $e->getMessage()
            ];
        }
    }

    public function sendToRabbitMq($policy_id)
    {
        try {
            if (!empty($policy_id)) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'policy' => []
                ];

                $policy = $this->buildPolicyForRabbitMq($policy_id);
                if (isset($policy['code']) && $policy['code'] == 500) {
                    $this->log->write('Build ploicy for rabbit_mq error: ' . json_encode($policy));
                    return;
                }

                $message['policy'] = $policy;

                $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'policy', '', 'policy', json_encode($message));
            }
        } catch (Exception $exception) {
            $this->log->write('Send message product error: ' . $exception->getMessage());
        }
    }

    public function deleteContent($id)
    {
        try {
            $this->modifyPolicyRabbitMq($id, 'delete');
        } catch (Exception $exception) {
            $this->log->write('Send message deleteContent error: ' . $exception->getMessage());
        }
        $this->db->query("DELETE FROM " . DB_PREFIX . "page_contents WHERE page_id = '" . (int)$id . "'");
    }

    public function modifyPolicyRabbitMq($policy_id, $action = 'delete')
    {
        try {
            if (empty($policy_id)) {
                return;
            }

            $policy = $this->getContent($policy_id);
            if (!empty($policy['title'])) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'policy' => [
                        'title' => isset($policy['title']) ? $policy['title'] : '',
                        'action' => $action,
                    ]
                ];

                $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'modify_policy', '', 'modify_policy', json_encode($message));
            }
        } catch (Exception $exception) {
            $this->log->write('Send message modifyPolicyRabbitMq error: ' . $exception->getMessage());
        }
    }
}
