<?php

class ModelOnlineStoreKeywordSync extends Model
{

    public function getTotalKeyword($data = array())
    {
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "keyword_sync";
        $sql .= " WHERE 1 ";

        $implode = array();

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $implode[] = "status = '" . (int)$data['filter_status'] . "'";
        }

        if (isset($data['filter_title']) && $data['filter_title'] !== '') {
            $implode[] = "title LIKE '" . $this->db->escape($data['filter_title']) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getkeywords($data = array())
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "keyword_sync";

        $sql .= " WHERE 1 ";

        $page = isset($data['page']) ? (int)$data['page'] : 1;

        if (isset($data['page']) && !empty($data['page'])) {
            $data = [
                'start' => ((int)$data['page'] - 1) * ((int)isset($data['limit']) ? $data['limit'] : 15),
                'limit' => isset($data['limit']) ? $data['limit'] : 15
            ];
        }
        $sql .= " ORDER BY date_added DESC";
        // limit
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 15;
            }
            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        $result =  $query->rows;
        return [
            'total' => $this->getTotalKeyword($data),
            'page' => $page,
            'records' => $result
        ];
    }

    public function createKeyword($data)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "keyword_sync SET keyword = '" . $this->db->escape($data['keyword']) . "'");
        return [
            'data' => "create done !",
        ];
    }

    public function checkKeywordExist($keyword) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "keyword_sync` WHERE `keyword` = '" . $this->db->escape($keyword) . "'");
        if (isset($query->row['keyword_id'])) {
            return true;
        }

        return false;
    }

    public function deleteAllKeyword() {
        $this->db->query("DELETE FROM " . DB_PREFIX . "keyword_sync");
        return [
            'data' => "delete done !",
        ];
    }

}
