<?php

class ModelUserUserGroup extends Model
{
    public function addUserGroup($data)
    {
        $DB_PREFIX = DB_PREFIX;
        $this->db->query("INSERT INTO {$DB_PREFIX}user_group 
                          SET name = '" . $this->db->escape($data['name']) . "', 
                              permission = '" . (isset($data['permission']) ? $this->db->escape(json_encode($data['permission'])) : '') . "'");

        return $this->db->getLastId();
    }

    public function editUserGroup($user_group_id, $data)
    {
        // skip edit admin group for security reason!
        if ($user_group_id == 1) {
            return;
        }

        $DB_PREFIX = DB_PREFIX;
        $this->db->query("UPDATE {$DB_PREFIX}user_group 
                          SET name = '" . $this->db->escape($data['name']) . "', 
                              permission = '" . (isset($data['permission']) ? $this->db->escape(json_encode($data['permission'])) : '') . "' 
                          WHERE user_group_id = '" . (int)$user_group_id . "'");
    }

    public function deleteUserGroup($user_group_id)
    {
        // skip delete admin group for security reason!
        if ($user_group_id == 1) {
            return;
        }

        $DB_PREFIX = DB_PREFIX;
        $this->db->query("DELETE FROM {$DB_PREFIX}user_group 
                          WHERE user_group_id = '" . (int)$user_group_id . "'");
    }

    public function getUserGroup($user_group_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "'");

        $user_group = array(
            'name'       => isset($query->row['name']) ? $query->row['name'] : '',
            'permission' => json_decode(isset($query->row['permission']) ? $query->row['permission'] : '', true),
            'access_all' =>isset($query->row['access_all']) ? $query->row['access_all'] : 0
        );

        return $user_group;
    }

    public function getUserGroupWithCountStaff() {
        $sql = "SELECT ug.name as name , u.total, ug.user_group_id as id
                    FROM " . DB_PREFIX . "user_group as ug 
                LEFT JOIN 
                (SELECT COUNT(*) as total, user_group_id 
                FROM " . DB_PREFIX . "user 
                GROUP BY user_group_id)  as u ON ug.user_group_id = u.user_group_id 
                WHERE ug.name != ''
                    AND ug.user_group_id not in (1, 10)
                Group by ug.user_group_id DESC
                ";
        $query = $this->db->query($sql);
        return $query->rows;
    }
}