<?php

class ModelUserUser extends Model
{
    public function getUsers($data = array())
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "user`";
        $sql .= " WHERE `status` = 1";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getUser($user_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $query = $this->db->query("SELECT *, 
                                          (SELECT ug.name FROM `{$DB_PREFIX}user_group` ug 
                                           WHERE ug.user_group_id = u.user_group_id) AS user_group 
                                   FROM `{$DB_PREFIX}user` u 
                                   WHERE u.user_id = '" . (int)$user_id . "'");

        return $query->row;
    }

    public function getUserByEmail($email)
    {
        $DB_PREFIX = DB_PREFIX;
        $query = $this->db->query("SELECT *, 
                                          (SELECT ug.name FROM `{$DB_PREFIX}user_group` ug 
                                           WHERE ug.user_group_id = u.user_group_id) AS user_group 
                                   FROM `{$DB_PREFIX}user` u 
                                   WHERE u.email = '" . $this->db->escape($email) . "'");

        return $query->row;
    }

    public function addUser($data)
    {
        if (!isset($data['type'])) {
            $data['type'] = 'Staff';
        }

        // force staff!
        if ($data['type'] != 'Staff') {
            $data['type'] = 'Staff';
        }

        if (!isset($data['info'])) {
            $data['info'] = '';
        }

        if (!isset($data['telephone'])) {
            $data['telephone'] = '';
        }

        if (!isset($data['permission'])) {
            $data['permission'] = [];
        }

        $DB_PREFIX = DB_PREFIX;
        $this->db->query("INSERT INTO `{$DB_PREFIX}user` 
                          SET username = '" . $this->db->escape($data['username']) . "', 
                              user_group_id = '" . (int)$data['user_group_id'] . "', 
                              salt = '" . $this->db->escape($salt = token(9)) . "', 
                              password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', 
                              firstname = '" . $this->db->escape($data['firstname']) . "', 
                              lastname = '" . $this->db->escape($data['lastname']) . "', 
                              email = '" . $this->db->escape($data['email']) . "' , 
                              telephone = '" . $this->db->escape($data['telephone']) . "', 
                              image = '" . $this->db->escape($data['image']) . "', 
                              status = '" . (int)$data['status'] . "', 
                              info = '" . $this->db->escape($data['info']) . "', 
                              type = '" . $this->db->escape($data['type']) . "', 
                              permission = '" . $this->db->escape(json_encode($data['permission'])) . "', 
                              date_added = NOW()");

        return $this->db->getLastId();
    }

    public function editUser($user_id, $data)
    {
        // skip edit admin for security reason!
        if ($user_id == 1) {
            return;
        }

        if (!isset($data['type'])) {
            $data['type'] = 'Staff';
        }

        // force staff for security reason!
        if ($data['type'] != 'Staff') {
            $data['type'] = 'Staff';
        }

        if (!isset($data['info'])) {
            $data['info'] = '';
        }

        if (!isset($data['telephone'])) {
            $data['telephone'] = '';
        }

        if (!isset($data['permission'])) {
            $data['permission'] = [];
        }

        $DB_PREFIX = DB_PREFIX;
        $this->db->query("UPDATE `{$DB_PREFIX}user` 
                          SET username = '" . $this->db->escape($data['username']) . "', 
                              user_group_id = '" . (int)$data['user_group_id'] . "', 
                              firstname = '" . $this->db->escape($data['firstname']) . "', 
                              lastname = '" . $this->db->escape($data['lastname']) . "', 
                              email = '" . $this->db->escape($data['email']) . "', 
                              image = '" . $this->db->escape($data['image']) . "', 
                              status = '" . (int)$data['status'] . "', 
                              telephone = '" . $this->db->escape($data['telephone']) . "', 
                              info = '" . $this->db->escape($data['info']) . "', 
                              type = '" . $this->db->escape($data['type']) . "', 
                              permission = '" . $this->db->escape(json_encode($data['permission'])) . "' 
                          WHERE user_id = '" . (int)$user_id . "'");

        if (isset($data['password']) && !empty($data['password'])) {
            $this->db->query("UPDATE `{$DB_PREFIX}user` 
                              SET salt = '" . $this->db->escape($salt = token(9)) . "', 
                                  password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' 
                              WHERE user_id = '" . (int)$user_id . "'");
        }
    }

    public function deleteUser($user_id)
    {
        // skip delete admin for security reason!
        if ($user_id == 1) {
            return;
        }

        $DB_PREFIX = DB_PREFIX;
        $this->db->query("DELETE FROM `{$DB_PREFIX}user` 
                          WHERE user_id = '" . (int)$user_id . "'");
    }

    public function getTotalStaff() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX ."user` WHERE `type` = 'Staff' ");

        return $query->row['total'];
    }

    public function addUserStore($data)
    {
        if (!isset($data['store_id'])) {
            $data['store_id'] = '';
        }
        if (!isset($data['user_id'])) {
            $data['user_id'] = '';
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "user_store` SET user_id = '" . $this->db->escape($data['user_id']) . "', store_id = '" . (int)$data['store_id'] . "' ");

        return $this->db->getLastId();
    }

    public function getUserStore($user_id)
    {
        $query = "SELECT us.store_id as id , s.name as title FROM " . DB_PREFIX . "user_store as us INNER JOIN " . DB_PREFIX . "store as s ON us.store_id = s.store_id WHERE us.user_id=" . (int)$user_id . " ORDER BY us.id ASC";
        $results = $this->db->query($query);
        return $results->rows;
    }
}