<?php
class ModelCatalogTag extends Model
{
    public function getAllTag()
    {
        $sql = "SELECT t.tag_id, t.value from " . DB_PREFIX . "tag as t ORDER BY t.value ASC";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTagByValue($value)
    {
        $DB_PREFIX = DB_PREFIX;
        $value = trim($value);
        $sql = "SELECT t.`tag_id`, t.`value` FROM `{$DB_PREFIX}tag` as t 
                WHERE t.`value` = '{$this->db->escape($value)}'";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getTagsByProductId($product_id) {
        $query = "SELECT t.tag_id as id , t.value as title FROM ". DB_PREFIX ."product_tag as pt INNER JOIN ". DB_PREFIX ."tag as t ON t.tag_id = pt.tag_id WHERE pt.product_id=".(int)$product_id." ORDER BY t.value ASC";
        $results = $this->db->query($query);
        return $results->rows;
    }

    /**
     * get tag list from their values
     *
     * @param array $tag_values
     * @return array
     */
    public function getTagsByValues(array $tag_values = [])
    {
        if (empty($tag_values)) {
            return [];
        }

        $tag_values_standardize = array_map(function ($value) {
            return sprintf("'%s'", $this->db->escape(mb_strtoupper($value)));
        }, $tag_values);

        $tag_values_sql = implode(',', $tag_values_standardize);

        $sql = "SELECT * FROM " . DB_PREFIX . "tag 
                WHERE 1=1 
                  AND UPPER(`value`) IN ({$tag_values_sql})";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * add tag use only value
     *
     * @param $text
     * @return mixed
     */
    public function addTagFast($text) {
        $sql_update = "INSERT INTO ". DB_PREFIX . "tag(value) VALUES ('". $this->db->escape($text) ."')";
        $this->db->query($sql_update);
        return $this->db->getLastId();
    }
}