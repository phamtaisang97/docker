<?php

class ModelCatalogCollection extends Model
{
    public function getAllcollection($data = array())
    {

        $sql = "SELECT * FROM " . DB_PREFIX . "collection c LEFT JOIN " . DB_PREFIX . "collection_description pc ON (c.collection_id = pc.collection_id) WHERE 1=1 ";
        $sql .= " AND c.`collection_id` NOT IN (SELECT `collection_id` FROM `" . DB_PREFIX . "flash_sale` WHERE CURDATE() >= `starting_date` AND CURDATE() <= `ending_date` AND CURRENT_TIME() >= `starting_time` AND CURRENT_TIME() <= `ending_time`)";

        if (!empty($data['type']) && 'product' == $data['type']) {
            $sql .= " AND (c.type = 0 OR c.type IS NULL) ";
        }

        $sql .= "  AND c.status = 1 ORDER BY c.collection_id";

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getCountProductCollections($collection_id)
    {
        $sql = "SELECT COUNT(product_collection_id) AS total FROM " . DB_PREFIX . "product_collection WHERE collection_id = $collection_id";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getProductCollections($collection_id)
    {
        $sql = "SELECT cd.category_id,cd.name ";
        $sql .= " FROM " . DB_PREFIX . "product_collection pc ";
        $sql .= " LEFT JOIN " . DB_PREFIX . "product as p ON (pc.product_id = p.product_id) ";
        $sql .= " LEFT JOIN " . DB_PREFIX . "product_description as pd ON (p.product_id = pd.product_id) ";
        $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category as ptc ON (p.product_id = ptc.product_id) ";
        $sql .= " LEFT JOIN " . DB_PREFIX . "category_description as cd ON (ptc.category_id = cd.category_id) ";
        $sql .= " WHERE 1=1 ";
        $sql .= ' AND pc.collection_id = "' . $collection_id . '" ';
        $sql .= ' GROUP BY ptc.category_id  ORDER BY pc.product_collection_id ASC';
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * get collections by filter
     *
     * @param array $filter support keys:
     * - filter_collection_id: id of collection, then get all sub-collections of it if it's type is 1 (CONTAINS COLLECTION)
     * @return array
     */
    public function getCollections($filter)
    {
        if (!isset($filter['filter_collection_id'])) {
            return [];
        }

        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM ${DB_PREFIX}collection c 
                LEFT JOIN ${DB_PREFIX}collection_description cd ON (c.collection_id = cd.collection_id) 
                WHERE 1=1 
                  AND c.status = 1 
                  AND c.collection_id = '". (int)$filter['filter_collection_id'] ."'";
        $query = $this->db->query($sql);
        $data = $query->row;

        if (!isset($data['type']) || $data['type'] != 1) {
            return [];
        }

        $sql = "SELECT * FROM ${DB_PREFIX}collection_to_collection ctc 
                LEFT JOIN ${DB_PREFIX}collection_description as cd ON (ctc.collection_id = cd.collection_id) 
                LEFT JOIN ${DB_PREFIX}collection as c ON (ctc.collection_id = c.collection_id) 
                WHERE 1=1 
                AND ctc.parent_collection_id = '". (int)$filter['filter_collection_id'] . "' 
                ORDER BY ctc.sort_order ASC ";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * get Collection by filter
     *
     * @param array $filter support keys:
     * - filter_collection_id: id of collection, then get all sub-collections of it if it's type is 1 (CONTAINS COLLECTION)
     * @return mixed
     */
    public function getCollection($filter)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM ${DB_PREFIX}collection c 
                LEFT JOIN ${DB_PREFIX}collection_description cd ON (c.collection_id = cd.collection_id) WHERE 1=1 
                AND c.status = 1 AND c.collection_id = '". (int)$filter['filter_collection_id'] ."'";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getCollectionByName($filter) {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM ${DB_PREFIX}collection c 
                LEFT JOIN ${DB_PREFIX}collection_description cd ON (c.collection_id = cd.collection_id) WHERE 1=1 
                AND c.status = 1";
        if (!empty($filter['name'])) {
            $name_search = mb_strtoupper($filter['name']);
            $sql .= " AND hex(UPPER(title)) = hex('". $this->db->escape($name_search) ."')";
        }
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getCountCollections($filter)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "collection c WHERE 1=1 ";
        $sql .= "  AND c.status = 1 AND c.collection_id = '". (int)$filter['filter_collection_id'] ."'";
        $query = $this->db->query($sql);
        $data = $query->row;
        if (!isset($data['type']) || $data['type'] != 1) {
            return 0;
        }
        $sql = "SELECT COUNT(1) total FROM ". DB_PREFIX ."collection_to_collection ctc WHERE 1=1 ";
        $sql .= ' AND ctc.parent_collection_id = "'. (int)$filter['filter_collection_id'] .'" ';

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    /**
     * get collection list from their titles
     *
     * @param array $collection_titles
     * @return array
     */
    public function getCollectionsByTitles(array $collection_titles = [])
    {
        if (empty($collection_titles)) {
            return [];
        }

        $collection_titles_standardize = array_map(function ($title) {
            return sprintf("'%s'", $this->db->escape(mb_strtoupper($title)));
        }, $collection_titles);

        $collection_titles_sql = implode(',', $collection_titles_standardize);

        $sql = "SELECT * FROM " . DB_PREFIX . "collection 
                WHERE 1=1 
                  AND UPPER(`title`) IN ({$collection_titles_sql})";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * add collection use only title
     *
     * @param $title
     * @return false
     */
    public function addCollectionTitleOnly($title)
    {
        if (trim($title) == '') {
            return false;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "collection` SET `title` = '" . $this->db->escape($title) . "', `status` = 1");
        $collection_id = $this->db->getLastId();
        $this->db->query("INSERT INTO " . DB_PREFIX . "collection_description SET  collection_id = '" . $collection_id . "', image = '', description = '', meta_title = '', meta_description = '', alias = ''");

        return $collection_id;
    }

    public function getListDataCollections($data = array())
    {
        $collections = $this->getListCollections($data);
        $count_collection = $this->countTotalCollections($data);

        $json = [
            'total' => $count_collection,
            'page' => (isset($this->request->get['page']) && !empty($this->request->get['page'])) ? $this->request->get['page'] : '',
            'records' => $collections
        ];

        return $json;
    }

    public function getListCollections($data = array())
    {
        $sql = $this->getCollectionSql($data);
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function countTotalCollections($data = array())
    {
        $sqlAll = $this->getCollectionSql($data);
        $query = $this->db->query("SELECT COUNT(1) AS `total` FROM (" . $sqlAll . ") as collection");

        return $query->row['total'];
    }

    private function getCollectionSql($data = array())
    {
        $sql = "SELECT c.*, cd.image, cd.description, cd.meta_title, cd.meta_description, cd.alias 
                    FROM " . DB_PREFIX . "collection c 
                        LEFT JOIN " . DB_PREFIX . "collection_description cd ON (c.collection_id = cd.collection_id) 
                            WHERE 1=1";

        if (!empty($data['filter_name'])) {
            $sql .= " AND c.title LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['type'])) {
            $sql .= " AND (c.type = 0 OR c.type IS NULL) ";
        }

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $sql .= " AND c.status = '" . (int)$data['filter_status'] . "'";
        }
        $sql .= " ORDER BY c.collection_id";

        return $sql;
    }

    public function check_collection($collection_id = '', $name = '')
    {
        $sql = "SELECT COUNT(collection_id) AS total FROM " . DB_PREFIX . "collection 
                WHERE title = '" . $this->db->escape(trim($name)) . "'";

        if(!empty($collection_id)) {
            $sql .= " AND `collection_id` NOT IN(".$collection_id.")";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getCollectionByIds($ids)
    {
        $tmp_ids = [];
        foreach ($ids as $id) {
            $tmp_ids[] = (int)$id;
        }

        $str_ids = implode(",", $tmp_ids);

        $sql = "SELECT COUNT(collection_id) AS total FROM " . DB_PREFIX . "collection 
                WHERE collection_id IN(" . $str_ids . ")";

        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function getProductByIds($ids)
    {
        $tmp_ids = [];
        foreach ($ids as $id) {
            $tmp_ids[] = (int)$id;
        }

        $str_ids = implode(",", $tmp_ids);

        $sql = "SELECT COUNT(product_id) AS total FROM " . DB_PREFIX . "product 
                WHERE product_id IN(" . $str_ids . ")";

        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function addCollection($data)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "collection SET user_create_id = 1, title = '" . $this->db->escape($data['title']) . "', status = " . $this->db->escape($data['status']) . ", type = '". $this->db->escape($data['type_select']) ."', product_type_sort = '". $this->db->escape($data['cat_list']) ."'");
        $collection_id = $this->db->getLastId();
        $this->db->query("INSERT INTO " . DB_PREFIX . "collection_description SET  collection_id = '" . $collection_id . "', image = '" . $this->db->escape($data['image']) . "', description = '" . $this->db->escape($data['description']) . "', meta_title = '" . $this->db->escape($data['seo_name']) . "', meta_description = '" . $this->db->escape($data['body']) . "', alias = '" . $this->db->escape($data['alias']) . "'");

        // create seo
        $this->load->model('custom/common');

        $collection_alias = $data['title'];
        if (isset($data['alias']) && $data['alias'] != '') {
            $collection_alias = $data['alias'];
        }
        $slug = $this->model_custom_common->createSlug($collection_alias, '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);
        $language_id = $this->config->get('config_language_id');
        $alias = 'collection='.$collection_id;
        $sql = "INSERT INTO " . DB_PREFIX . "seo_url SET language_id = '" . (int)$language_id . "', query = '".$alias."', keyword = '".$slug_custom."', table_name = 'collection'";
        $this->db->query($sql);
        // Luu vao bo suu tap
        if(isset($data['collection_list']) != '' && $data['type_select'] != 1){
            $this->db->query("DELETE FROM " . DB_PREFIX . "collection_to_collection WHERE collection_id IN (" . implode(',',$data['collection_list']) . ") and parent_collection_id = '".$collection_id."'");
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_collection WHERE product_id IN (" . implode(',',$data['collection_list']) . ") and collection_id = '".$collection_id."'");
            foreach ($data['collection_list'] as $value){
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_collection SET product_id = '" . (int)$value . "', collection_id = '".$collection_id."' ");
            }
        } else {
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_collection WHERE product_id IN (" . implode(',',$data['collection_list']) . ") and collection_id = '".$collection_id."'");
            $this->db->query("DELETE FROM " . DB_PREFIX . "collection_to_collection WHERE collection_id IN (" . implode(',',$data['collection_list']) . ") and parent_collection_id = '".$collection_id."'");
            foreach ($data['collection_list'] as $key => $value){
                $this->db->query("INSERT INTO " . DB_PREFIX . "collection_to_collection SET collection_id = '" . (int)$value . "', parent_collection_id = '". (int)$collection_id."', sort_order = '". (int)($key + 1) ."'");
            }
        }

        return $collection_id;
    }

    public function editCollection($collection_id, $data)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "collection SET  title = '" . $this->db->escape($data['title']) . "',status = " . $data['status'] . ", product_type_sort = '" . $this->db->escape($data['cat_list']) . "', type= '" . $this->db->escape($data['type_select']) . "'  WHERE collection_id = '" . (int)$collection_id . "'");
        $this->db->query("UPDATE " . DB_PREFIX . "collection_description SET  image = '" . $this->db->escape($data['image']) . "',description = '" . $this->db->escape($data['description']) . "',meta_title = '" . $this->db->escape($data['seo_name']) . "',meta_description = '" . $this->db->escape($data['body']) . "',alias = '" . $this->db->escape($data['alias']) . "' WHERE collection_id = " . (int)$collection_id . "");

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_collection WHERE collection_id = '" . $collection_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "collection_to_collection WHERE parent_collection_id = '" . $collection_id . "'");
        // collection contains products
        if ($data['type_select'] != 1) {
            foreach ($data['collection_list'] as $key => $value) {
                //$addOrderBy = $this->addOrderBy();
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_collection SET product_id = '" . (int)$value . "', collection_id = '" . $collection_id . "' , sort_order = '" . ($key + 1) . "'");
            }
        } else { // collection contains sub-collections
            foreach ($data['collection_list'] as $key => $value) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "collection_to_collection SET collection_id = '" . (int)$value . "', parent_collection_id = '" . (int)$collection_id . "', sort_order = '" . ($key + 1) . "'");
            }
        }

        //seo
        //delete seo product
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'collection=" . (int)$collection_id . "'");

        $this->load->model('custom/common');
        $tmp_alias = (isset($data['alias']) && $data['alias'] != '') ? $data['alias'] : ((isset($data['title']) && $data['title'] != '') ? $data['title'] : '');
        $data['alias'] = $tmp_alias;

        if (isset($data['alias']) && $data['alias'] != '') {
            $slug = $this->model_custom_common->createSlug($data['alias'], '-');
            $slug_custom = $this->model_custom_common->getSlugUnique($slug);
            //create new seo product
            $language_id = $this->config->get('config_language_id');
            $alias = 'collection=' . $collection_id;
            $sqlseo = "INSERT INTO " . DB_PREFIX . "seo_url SET language_id = '" . (int)$language_id . "', query = '" . $alias . "', keyword = '" . $slug_custom . "', table_name = 'collection'";
            $this->db->query($sqlseo);
        }

        $this->deleteProductCaches();
    }

    public function deleteCollection($collection_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "collection WHERE collection_id = '" . (int)$collection_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'collection=" . (int)$collection_id . "'");
        //delete tag, collection
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_collection WHERE collection_id = '" . (int)$collection_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "collection_to_collection WHERE parent_collection_id = '" . (int)$collection_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "collection_description WHERE collection_id = '" . (int)$collection_id . "'");
        $this->deleteProductCaches();
    }

    /**
     * delete product caches
     */
    private function deleteProductCaches() {
        $product_key_redis =  DB_PREFIX . 'product';
        $this->cache->delete($product_key_redis);
    }
}