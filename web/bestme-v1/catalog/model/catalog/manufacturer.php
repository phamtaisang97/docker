<?php
class ModelCatalogManufacturer extends Model {
    use RabbitMq_Trait;

    private $MANUFACTURE_STATUS_DRAFT = 99;
    const TYPE_DELETE_TRANSACTION = 99;

	public function getManufacturer($manufacturer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer m LEFT JOIN " . DB_PREFIX . "manufacturer_to_store m2s ON (m.manufacturer_id = m2s.manufacturer_id) WHERE m.manufacturer_id = '" . (int)$manufacturer_id . "' AND m2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		return $query->row;
	}

	public function getManufacturers($data = array(), $get_draft = true) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "manufacturer m LEFT JOIN " . DB_PREFIX . "manufacturer_to_store m2s ON (m.manufacturer_id = m2s.manufacturer_id) WHERE m.status <> 99 AND m2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

			$sort_data = array(
				'name',
				'sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY name";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
            $sql = "SELECT * FROM " . DB_PREFIX . "manufacturer m
                    LEFT JOIN " . DB_PREFIX . "manufacturer_to_store m2s ON (m.manufacturer_id = m2s.manufacturer_id)
                    WHERE m2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
                    ". ($get_draft ? '' : ' AND m.status <> ' . $this->MANUFACTURE_STATUS_DRAFT . ' ') ."
                    ORDER BY name";
            $query = $this->db->query($sql);

            $manufacturer_data = $query->rows;

            foreach ($manufacturer_data as $key => $value) {
                if ($value['name'] == null) {
                    $value['name'] = 'Other';
                    $manufacturer_data['Other'] = $value;
                    unset($manufacturer_data[$key]);
                }
            }

			return $manufacturer_data;
		}
	}

	public function getManufacturersHadProduct()
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM {$DB_PREFIX}manufacturer m 
                LEFT JOIN {$DB_PREFIX}manufacturer_to_store m2s ON (m.manufacturer_id = m2s.manufacturer_id) 
                LEFT JOIN {$DB_PREFIX}product p ON (m.manufacturer_id = p.manufacturer_id) 
                WHERE m2s.store_id = '" . (int)$this->config->get('config_store_id'). "' AND p.product_id IS NOT NULL 
                AND m.status <> {$this->MANUFACTURE_STATUS_DRAFT} 
                GROUP BY m.manufacturer_id 
                ORDER BY name";
        $query = $this->db->query($sql);

        $manufacturer_data = $query->rows;

        return $manufacturer_data;
    }

    public function getManufacturerNameById($manufacturer_id){
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer WHERE manufacturer_id = " . (int)$manufacturer_id);
        if ($query->row['name']) {
            return $query->row['name'];
        }
        return null;
    }

    public function getManufacturerIdByName($manufacturer_name){
//        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer WHERE name = '". $this->db->escape($manufacturer_name) ."' .);
//        if ($query->row['manufacturer_id']) {
//            return $query->row['manufacturer_id'];
//        }
//        return 0;

        $sql = "SELECT * FROM ". DB_PREFIX . "manufacturer WHERE 1=1 ";

        $name_search = mb_strtoupper($manufacturer_name);
        $sql .= " AND hex(UPPER(name)) = hex('". $this->db->escape($name_search) ."')";
        $query = $this->db->query($sql);

        if ($query->row['manufacturer_id']) {
            return $query->row['manufacturer_id'];
        }
        return 0;
    }

    /**
     * getManufactureByName, may except one id
     *
     * @param array $filter, format as
     * [
     *     'name' => 'man 1',
     *     'id' => 123
     * ]
     * where "id" is need to be except
     * @return mixed
     */
    public function getManufacturerByName(array $filter = [])
    {
        if (empty($filter)) {
            return [];
        }

        $sql = "SELECT * FROM " . DB_PREFIX . "manufacturer WHERE 1=1 ";

        if (isset($filter['name'])) {
            $sql .= " AND UPPER(`name`) = '" . $this->db->escape(mb_strtoupper($filter['name'])) . "'";
        }

        if (isset($filter['id']) && $filter['id'] != '') {
            $sql .= " AND manufacturer_id != '" . (int)$filter['id'] . "'";
        }

        $query = $this->db->query($sql);

        return $query->row;
    }

    public function addManufacturerFast($name, $source = '')
    {
        $data = [
            "name" => $name,
            "sort_order" => 0,
            "manufacturer_store" => [0],
            "image" => "",
            "source" => $source,
            "manufacturer_seo_url" => [[
                1 => str_replace(" ", "-", $this->db->escape($name)),
            ]]
        ];

        $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer 
                          SET name = '" . $this->db->escape($data['name']) . "', 
                          sort_order = '" . (int)$data['sort_order'] . "',`status` = 1 , `date_added` = NOW(), `date_modified` = NOW()");

        $manufacturer_id = $this->db->getLastId();

        $prefix = 'NCC';
        $code = $prefix . str_pad($manufacturer_id, 6, "0", STR_PAD_LEFT);

        $this->db->query("UPDATE " . DB_PREFIX . "manufacturer 
                          SET `manufacturer_code` = '" . $code . "' 
                          WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET image = '" . $this->db->escape($data['image']) . "' WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
        }

        if (isset($data['manufacturer_store'])) {
            foreach ($data['manufacturer_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '" . (int)$store_id . "'");
            }
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = 0");
        }

        // SEO URL
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        $manufacturer_alias = $data['name'];

        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($manufacturer_alias, '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);
        foreach ($languages as $key => $language) {
            $language_id = $language['language_id'];
            $alias = 'manufacturer_id=' . $manufacturer_id;
            $sql = "INSERT INTO " . DB_PREFIX . "seo_url SET language_id = '" . (int)$language_id . "', query = '" . $alias . "', keyword = '" . $slug_custom . "', table_name = 'manufacturer'";
            $this->db->query($sql);
        }

        if (!empty($data['source'])) {
            $this->updateSourceManufacture($manufacturer_id, $data['source']);
        }

        //$this->cache->delete('manufacturer');
        // send product to rabbit mq
        try {
            $this->sendToRabbitMq($manufacturer_id, $this->db->escape($data['name']));
        } catch (Exception $exception) {
            $this->log->write('Send message add new manufacture fast error: ' . $exception->getMessage());
        }

        return $manufacturer_id;
    }

    public function updateManufacturerFast($manufacturer_id, $name, $source = '')
    {
        $data = [
            "name" => $name,
            "sort_order" => 0,
            "manufacturer_store" => [0],
            "image" => "",
            "source" => $source,
            "manufacturer_seo_url" => [[
                1 => str_replace(" ", "-", $this->db->escape($name)),
            ]]
        ];

        // get old_name
        $old_name = $this->getManufacturerNameById($manufacturer_id);

        // get update_name
        $this->db->query("UPDATE " . DB_PREFIX . "manufacturer 
                          SET `name` = '" . $this->db->escape($data['name']) . "' 
                          WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET image = '" . $this->db->escape($data['image']) . "' WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer_to_store WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

        if (isset($data['manufacturer_store'])) {
            foreach ($data['manufacturer_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '" . (int)$store_id . "'");
            }
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = 0");
        }

        // SEO URL
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        $manufacturer_alias = $data['name'];

        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($manufacturer_alias, '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);

        $this->db->query("DELETE FROM `" . DB_PREFIX . "seo_url` WHERE query = 'manufacturer_id=" . (int)$manufacturer_id . "'");

        foreach ($languages as $key => $language) {
            $language_id = $language['language_id'];
            $alias = 'manufacturer_id=' . $manufacturer_id;
            $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                    SET language_id = '" . (int)$language_id . "', 
                        query = '" . $alias . "', 
                        keyword = '" . $slug_custom . "', 
                        table_name = 'manufacturer'";
            $this->db->query($sql);
        }

        if (!empty($data['source'])) {
            $this->updateSourceManufacture($manufacturer_id, $data['source']);
        }

        //$this->cache->delete('manufacturer');
        // send product to rabbit mq
        if ($old_name != $data['name']) {
            try {
                $this->sendToRabbitMq($manufacturer_id, $this->db->escape($data['name']),  $this->db->escape($old_name));
            } catch (Exception $exception) {
                $this->log->write('Send message edit manufacture fast error: ' . $exception->getMessage());
            }
        }

        return $manufacturer_id;
    }

    public function updateSourceManufacture($manufacturer_id, $source = null)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET 
                                   `source` = '" . $this->db->escape($source) . "' 
                                    WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
    }

    /**
     * @param int $manufacturer_id
     * @return mixed
     */
    public function getManufacturerById($manufacturer_id)
    {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "manufacturer WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

        return $query->row;
    }

    public function sendToRabbitMq($manufacture_id, $manufacture_name, $manufacture_old_name = "")
    {
        try {
            if (!empty($manufacture_id)) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'manufacture' => [
                        'name' => $manufacture_name,
                        'old_name' => $manufacture_old_name
                    ]
                ];

                $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'manufacture', '', 'manufacture', json_encode($message));
            }
        } catch (Exception $exception) {
            $this->log->write('Send message product error: ' . $exception->getMessage());
        }
    }

    public function updateManufacturerStatus($manufacturer_id, $status)
    {
        try {
            $db_prefix = DB_PREFIX;
            $sql = "UPDATE {$db_prefix}manufacturer SET `status` = '{$status}'";

            $sql .= " WHERE manufacturer_id = '" . (int)$manufacturer_id . "'";

            $this->db->query($sql);

            switch ($status) {
                case self::TYPE_DELETE_TRANSACTION:
                    $this->modifyManufactureRabbitMq($manufacturer_id, 'delete');
                    break;
            }
        } catch (Exception $exception) {
            $this->log->write('updateManufacturerStatus error: ' . $exception->getMessage());
        }
    }

    public function modifyManufactureRabbitMq($manufacture_id, $action = 'delete')
    {
        try {
            if (empty($manufacture_id)) {
                return;
            }

            $manufacturer = $this->getManufacturerById($manufacture_id);
            if (!empty($manufacturer['name'])) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'manufacturer' => [
                        'name' => $manufacturer['name'],
                        'action' => $action,
                    ]
                ];

                $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'modify_manufacturer', '', 'modify_manufacturer', json_encode($message));
            }
        } catch (Exception $exception) {
            $this->log->write('Send message modifyManufactureRabbitMq error: ' . $exception->getMessage());
        }
    }
}