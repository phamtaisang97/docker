<?php

class ModelCatalogProductFlashsale extends Model
{

    public function getFlashsale()
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "flash_sale 
        WHERE CURDATE() >= DATE(starting_date)
        AND CURDATE() <= DATE(ending_date)";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getProductCollections($collection_id)
    {
        $sql = "SELECT * ";
        $sql .= " FROM " . DB_PREFIX . "product_collection pc ";
        $sql .= " LEFT JOIN " . DB_PREFIX . "product as p ON (pc.product_id = p.product_id) ";
        $sql .= " WHERE 1 ";
        $sql .= ' AND pc.collection_id = "' . $collection_id . '" ';
        $query = $this->db->query($sql);

        return $query->rows;
    }
}
