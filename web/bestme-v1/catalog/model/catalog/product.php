<?php

class ModelCatalogProduct extends Model
{
    use RabbitMq_Trait;

    const CHANNEL_WEB = 0;
    const CHANNEL_POS = 1;
    const CHANNEL_WEB_AND_POS = 2;

    public static $CHANNELS_FOR_WEB = [
        self::CHANNEL_WEB,
        self::CHANNEL_WEB_AND_POS,
    ];

    public static $CHANNELS_FOR_POS = [
        self::CHANNEL_POS,
        self::CHANNEL_WEB_AND_POS,
    ];

    public function updateViewed($product_id)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET viewed = (viewed + 1) WHERE product_id = '" . (int)$product_id . "'");

        $this->deleteProductCaches();
    }

    /**
     * get product detail instead of function getProduct (faster old function)
     * TODO: make sure getProductOptimize() return output same getProduct() with same input,
     * then replace full usage of getProduct(), then may remove getProduct() function
     *
     * @param int|array $product_id if multi product ids, provide array
     * @param bool $flag_status
     * @param bool $flag_deleted
     * @param bool $keep_product_id_sorted_as_input
     * @return array|bool
     */
    public function getProductOptimize($product_id, $flag_status = true, $flag_deleted = true, $keep_product_id_sorted_as_input = false)
    {
        $is_mul_products = is_array($product_id);
        $product_ids = $is_mul_products ? $product_id : [(int)$product_id];
        if ($is_mul_products && empty($product_ids)) {
            return [];
        }

        $condition_p0_status = $flag_status ? " p0.status = '1' " : " 1=1 ";
        $condition_p0_delete = $flag_deleted ? " p0.`deleted` IS NULL " : " 1=1 ";


        $DB_PREFIX = DB_PREFIX;
        // VERY IMPORTANT: DO NOT use DISTINCT *, please list all fields need to get if using JOIN!
        $sql = "SELECT p.*,
                  -- pd.*,
                  pd.`name` as name,
                  pd.`description`,
                  pd.`sub_description`,
                  pd.`seo_title`,
                  pd.`seo_description`,
                  pd.`tag`,
                  pd.`meta_title`,
                  pd.`meta_description`,
                  pd.`meta_keyword`,
                  
                  p2s.`quantity`,
                  p2s.`cost_price`,

                  m.`name` AS manufacturer,
                  
                  pv.`product_version_id`,

                  -- custom output
                  p.sku as sku_custom, 
                  p.status as p_status, 
                  (CASE WHEN pv.product_version_id IS NULL THEN p2s.quantity ELSE p2s.quantity END) AS product_master_quantity,
                  p.product_id as product_id_master, 
                  p.p_price_discount as price_master, 
                  p.compare_price as compare_price_master, 
                  p.discount_id as p_discount_id, 
                  
                  pvq.product_version_quantity,
                  pvq.min_price_version,
                  pvq.max_price_version,
                  pvq.price_version_check_null,
                  pvq.min_compare_price_version,
                  pvq.max_compare_price_version,
                  pvq.product_version_status_max,
                  pvq.max_percent_pv, 
                  product_version_percent.discount_id as pv_discount_id, 
                  product_version_percent.pv_price_discount as pv_price_discount, 
                  
                  product_version_percent.max_percent_pv max_percent_pv_2, 
                  product_version_percent.product_version_id product_version_id_2,
                  product_version_percent.pv_price_discount price_2,
                  product_version_percent.compare_price compare_price_2,
                  product_version_percent.image pv_image,
                  
                  -- product images
                  pim.image as second_image, 
                  
                  -- discount
                  0 AS discount, 
                  
                  -- special price
                  0 AS special,
                  
                  -- reward
                  0 AS reward,
                  
                  -- stock_status
                  0 AS stock_status,
                  
                  -- weight_class
                  0 AS weight_class,
                  
                  -- length_class 
                  0 AS length_class,
                  
                  -- rating
                  0 AS rating, 
                  
                  -- reviews
                  0 AS reviews, 
                  
                  -- p.sort_order, 
                  p.deleted as p_deleted 
                  
                  FROM (SELECT p0.*, IFNULL( dp.price_discount, 0 ) as p_price_discount, dp.discount_id FROM {$DB_PREFIX}product p0 
                                 LEFT JOIN (SELECT m.* 
                                                FROM
                                                    (SELECT dp_1.* 
                                                        FROM {$DB_PREFIX}discount_product AS dp_1 
                                                        JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                            WHERE d_1.date_started <= NOW() 
                                                                AND d_1.date_ended >= NOW() 
                                                                AND d_1.status = 1 
                                                                AND dp_1.status = 1 
                                                                AND dp_1.product_id IN (" . implode(',', $product_ids) . ")) AS m
                                                    LEFT JOIN (SELECT dp_1.price_discount, dp_1.product_id 
                                                        FROM {$DB_PREFIX}discount_product AS dp_1 
                                                        JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                            WHERE d_1.date_started <= NOW() 
                                                                AND d_1.date_ended >= NOW() 
                                                                AND d_1.status = 1 
                                                                AND dp_1.status = 1 
                                                                AND dp_1.product_id IN (" . implode(',', $product_ids) . ")) AS b                                                                                              
                                                    ON m.product_id = b.product_id 
                                                    AND m.price_discount > b.price_discount 
                                                    WHERE b.price_discount IS NULL 
                                                    GROUP BY m.product_id) AS dp  
                                 ON CASE WHEN p0.multi_versions = 0 THEN p0.product_id ELSE -1 END = dp.product_id
                                 WHERE 1 
                                   AND p0.product_id IN (" . implode(',', $product_ids) . ") 
                                   AND {$condition_p0_status} 
                                   AND {$condition_p0_delete} 
                       ) AS p 
                  LEFT JOIN {$DB_PREFIX}product_description pd ON (p.product_id = pd.product_id) 
                  LEFT JOIN (
                    SELECT * 
                    FROM {$DB_PREFIX}product_to_store
                    GROUP BY product_id, store_id
                  ) p2s ON (p.product_id = p2s.product_id AND p.default_store_id = p2s.store_id) 
                  LEFT JOIN {$DB_PREFIX}manufacturer m ON (p.manufacturer_id = m.manufacturer_id) 
                  LEFT JOIN (
                      SELECT * 
                      FROM {$DB_PREFIX}product_version
                      GROUP BY product_id
                  )  pv ON (
                             CASE WHEN p.multi_versions = 1 THEN p.product_id
                             ELSE -1
                             END = pv.product_id
                         ) 
                  LEFT JOIN (SELECT MAX(product_image_id) last_image_id, image, product_id  
                                FROM {$DB_PREFIX}product_image pim2 GROUP BY product_id) 
                            pim ON (pim.product_id = p.product_id) 
                            
                  LEFT JOIN (SELECT m.product_version_id, m.price, m.compare_price, min(m.percent_pv) max_percent_pv, m.product_id, m.deleted, m.image, 
                                IFNULL( m.price_discount, 0 ) as pv_price_discount, m.discount_id 
                    FROM (SELECT pv_m.*, dp.price_discount, dp.discount_id, 
                            (CASE WHEN dp.price_discount IS NULL THEN 0 
                                ELSE 
                                    (CASE WHEN dp.price_discount = 0 THEN -100 
                                    ELSE ((dp.price_discount - pv_m.compare_price)/pv_m.compare_price*100) END)
                                END) as percent_pv 
                            FROM {$DB_PREFIX}product_version pv_m 
                                LEFT JOIN (SELECT dp_1.* FROM {$DB_PREFIX}discount_product AS dp_1 
                                            LEFT JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                WHERE d_1.date_started <= NOW() 
                                                    AND d_1.date_ended >= NOW() 
                                                    AND d_1.status = 1 
                                                    AND dp_1.status = 1 
                                                    AND dp_1.product_id IN (" . implode(',', $product_ids) . ") 
                                                    ) dp  
                                    ON (pv_m.product_id = dp.product_id AND pv_m.product_version_id = dp.product_version_id) 
                                    WHERE pv_m.deleted IS NULL) m        
                            LEFT JOIN (SELECT pv_b.product_version_id, pv_b.compare_price, pv_b.product_id, dp.price_discount, dp.discount_id, 
                                        (CASE WHEN dp.price_discount IS NULL THEN 0 
                                            ELSE 
                                                (CASE WHEN dp.price_discount = 0 THEN -100 
                                                    ELSE ((dp.price_discount - pv_b.compare_price)/pv_b.compare_price*100) END) 
                                         END) as percent_pv  
                                FROM {$DB_PREFIX}product_version pv_b 
                                    LEFT JOIN (SELECT dp_1.* FROM {$DB_PREFIX}discount_product AS dp_1 
                                                LEFT JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                    WHERE d_1.date_started <= NOW() 
                                                        AND d_1.date_ended >= NOW() 
                                                        AND d_1.status = 1 
                                                        AND dp_1.status = 1 
                                                        AND dp_1.product_id IN (" . implode(',', $product_ids) . ") 
                                                        ) dp  
                                    ON (pv_b.product_id = dp.product_id AND pv_b.product_version_id = dp.product_version_id) 
                                    WHERE pv_b.deleted IS NULL) b 
                        ON m.product_id = b.product_id 
                        AND (m.percent_pv > b.percent_pv OR (m.percent_pv = b.percent_pv AND m.compare_price > b.compare_price) )
                        WHERE m.deleted IS NULL AND b.percent_pv IS NULL 
                        GROUP BY m.product_id) product_version_percent 
                            ON CASE WHEN p.multi_versions = 1 THEN p.product_id ELSE -1 END = product_version_percent.product_id
                    
                  INNER JOIN (
                          SELECT DISTINCT p2.product_id, p2.discount_id, 
                              SUM(case when p2.status = 1 then p2.quantity else 0 end) as product_version_quantity,
                              MIN(case when p2.price_discount > 0 then p2.price_discount else p2.compare_price end) as min_price_version,
                              MAX(case when p2.price_discount > 0 then p2.price_discount else p2.compare_price end) as max_price_version,
                              MAX(p2.price_discount) as price_version_check_null,
                              MIN(p2.compare_price) as min_compare_price_version,
                              MAX(p2.compare_price) as max_compare_price_version,
                              MAX(p2.status) as product_version_status_max,
                              MIN(CASE WHEN p2.price_discount IS NULL THEN 0 
                                    ELSE 
                                        (CASE WHEN p2.price_discount = 0 THEN -100 
                                        ELSE ((p2.price_discount - p2.compare_price)/p2.compare_price*100) END) 
                                    END) as max_percent_pv
                          FROM (
                              SELECT ps.*, p.compare_price, p.price, p.status, IFNULL( dp.price_discount, 0 ) as price_discount, dp.discount_id 
                              FROM {$DB_PREFIX}product_to_store ps
                              LEFT JOIN {$DB_PREFIX}product p ON p.product_id = ps.product_id 
                              LEFT JOIN (SELECT m.* 
                                                FROM
                                                    (SELECT dp_1.* 
                                                        FROM {$DB_PREFIX}discount_product AS dp_1 
                                                        JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                            WHERE d_1.date_started <= NOW() 
                                                                AND d_1.date_ended >= NOW() 
                                                                AND d_1.status = 1 
                                                                AND dp_1.status = 1 ) AS m
                                                    LEFT JOIN (SELECT dp_1.price_discount, dp_1.product_id 
                                                        FROM {$DB_PREFIX}discount_product AS dp_1 
                                                        JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                            WHERE d_1.date_started <= NOW() 
                                                                AND d_1.date_ended >= NOW() 
                                                                AND d_1.status = 1 
                                                                AND dp_1.status = 1 ) AS b                                                                                              
                                                    ON m.product_id = b.product_id 
                                                    AND m.price_discount > b.price_discount 
                                                    WHERE b.price_discount IS NULL 
                                                    GROUP BY m.product_id) AS dp
                                     ON CASE WHEN p.multi_versions = 0 THEN p.product_id ELSE -1 END = dp.product_id
                              WHERE p.multi_versions = 0 
                                AND p.product_id IN (" . implode(',', $product_ids) . ") 
                              UNION
                              SELECT ps.*, pv.compare_price, pv.price, pv.status, IFNULL( dp.price_discount, 0 ) as price_discount, dp.discount_id 
                              FROM {$DB_PREFIX}product_to_store ps
                              -- note: AND pv.product_id = ps.product_id to avoid wrong data (same product_version_id but different from product_id, ...)
                              LEFT JOIN {$DB_PREFIX}product_version pv ON pv.product_version_id = ps.product_version_id AND pv.product_id = ps.product_id 
                              LEFT JOIN (SELECT m.* 
                                                FROM
                                                    (SELECT dp_1.* 
                                                        FROM {$DB_PREFIX}discount_product AS dp_1 
                                                        JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                            WHERE d_1.date_started <= NOW() 
                                                                AND d_1.date_ended >= NOW() 
                                                                AND d_1.status = 1 
                                                                AND dp_1.status = 1 
                                                                AND dp_1.product_id IN (" . implode(',', $product_ids) . ")) AS m
                                                    LEFT JOIN (SELECT dp_1.price_discount, dp_1.product_id 
                                                        FROM {$DB_PREFIX}discount_product AS dp_1 
                                                        JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                            WHERE d_1.date_started <= NOW() 
                                                                AND d_1.date_ended >= NOW() 
                                                                AND d_1.status = 1 
                                                                AND dp_1.status = 1 
                                                                AND dp_1.product_id IN (" . implode(',', $product_ids) . ")) AS b                                                                                              
                                                    ON m.product_id = b.product_id 
                                                    AND m.price_discount > b.price_discount 
                                                    WHERE b.price_discount IS NULL 
                                                    GROUP BY m.product_id) AS dp   
                                    ON (pv.product_id = dp.product_id AND pv.product_version_id = dp.product_version_id) 
                              WHERE ps.product_version_id <> 0
                                  AND ps.product_id IN (" . implode(',', $product_ids) . ") 
                                  -- note: AND pv.product_id IN... to avoid wrong data (same product_version_id but different from product_id, ...)
                                  AND pv.product_id IN (" . implode(',', $product_ids) . ") 
                          ) p2
                          GROUP BY p2.product_id 
                     ) AS pvq ON (pvq.product_id = p.product_id) 
                  WHERE 1 
                    AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
                    AND p.date_available <= NOW() "; // No need use: AND p.channel IN (" . $this->db->escape(implode(',', $channel)) . ") "; TODO: remove...

        if ($keep_product_id_sorted_as_input) {
            $sql .= " ORDER BY FIELD(p.product_id," . implode(',', $product_ids) . ") ";
        }

        // get result from cache first
        $cache_key = DB_PREFIX . 'product:getProductOptimize_' . md5($sql);
        $cache_products = $this->cache->get($cache_key);
        if ($cache_products) {
            $products = $cache_products;
        } else {
            // get result from db
            $query = $this->db->query($sql);
            if ($query->num_rows) {
                $products = $query->rows;
                $this->cache->set($cache_key, $products);
            }
        }

        $product_id_to_categories = [];
        $categories = $this->getCategoriesCustom($product_ids);
        if ($categories) {
            foreach ($categories as $category) {
                if (!isset($category['product_id'])) {
                    continue;
                }

                $p_id = $category['product_id'];
                if (!array_key_exists($p_id, $product_id_to_categories)) {
                    $product_id_to_categories[$p_id] = [];
                }

                $product_id_to_categories[$p_id][] = [
                    'id' => $category['category_id'],
                    'name' => $category['name'],
                    'image' => $category['image'],
                    'href' => $this->url->link('common/shop', 'category=' . $category['category_id'], true),
                ];
            }
        }

        if (!empty($products)) {
            $result = [];
            foreach ($products as $product) {
                $data['categories'] = isset($product_id_to_categories[$product['product_id_master']])
                    ? $product_id_to_categories[$product['product_id_master']]
                    : [];

                $p_image = '';
                if ($product['multi_versions']) {
                    if(!empty($product['pv_image']))  {
                        $p_image = $product['pv_image'];
                    } else {
                        $p_image = $product['image'];
                    }
                } else {
                    $p_image = $product['image'];
                }

                $result[] = [
                    'product_id' => $product['product_id_master'],
                    'product_version_id' => $product['product_version_id_2'],
                    'name' => $product['name'],
                    'description' => $product['description'],
                    'sub_description' => $product['sub_description'],
                    'meta_title' => $product['meta_title'],
                    'meta_description' => $product['meta_description'],
                    'meta_keyword' => $product['meta_keyword'],
                    'tag' => $product['tag'],
                    'model' => $product['model'],
                    'sku' => $product['sku_custom'],
                    'upc' => $product['upc'],
                    'ean' => $product['ean'],
                    'jan' => $product['jan'],
                    'isbn' => $product['isbn'],
                    'mpn' => $product['mpn'],
                    'location' => $product['location'],
                    'quantity' => $product['quantity'],
                    'stock_status' => $product['stock_status'],
                    'image' => $p_image,
                    'second_image' => $product['second_image'],
                    'manufacturer_id' => $product['manufacturer_id'],
                    'manufacturer' => $product['manufacturer'],
                    'price' => ($product['discount'] ? $product['discount'] : $product['price_master']),
                    'special' => $product['special'],
                    'reward' => $product['reward'],
                    'points' => $product['points'],
                    'tax_class_id' => $product['tax_class_id'],
                    'date_available' => $product['date_available'],
                    'weight' => $product['weight'],
                    'weight_class_id' => $product['weight_class_id'],
                    'length' => $product['length'],
                    'width' => $product['width'],
                    'height' => $product['height'],
                    'length_class_id' => $product['length_class_id'],
                    'subtract' => $product['subtract'],
                    'rating' => round($product['rating']),
                    'reviews' => $product['reviews'] ? $product['reviews'] : 0,
                    'minimum' => $product['minimum'],
                    'sort_order' => $product['sort_order'],
                    'status' => $product['p_status'],
                    'date_added' => $product['date_added'],
                    'date_modified' => $product['date_modified'],
                    'viewed' => $product['viewed'],
                    'min_price_version' => $product['min_price_version'],
                    'max_price_version' => $product['max_price_version'],
                    'min_compare_price_version' => $product['min_compare_price_version'],
                    'max_compare_price_version' => $product['max_compare_price_version'],
                    'price_master' => $product['price_master'],
                    'compare_price_master' => $product['compare_price_master'],
                    'product_version_quantity' => $product['product_version_quantity'],
                    'product_master_quantity' => $product['product_master_quantity'],
                    'categories' => $data['categories'],
                    'sale_on_out_stock' => $product['sale_on_out_of_stock'],
                    'price_version_check_null' => $product['price_version_check_null'],
                    'multi_versions' => $product['multi_versions'],
                    'product_version_status_max' => $product['product_version_status_max'],
                    'max_percent_pv' => $product['multi_versions'] ? $product['max_percent_pv_2'] : $product['max_percent_pv'],
                    'default_store_id' => $product['default_store_id'],
                    'price_2' => $product['price_2'],
                    'compare_price_2' => $product['compare_price_2'],
                    'deleted' => $product['p_deleted'],
                    'price_discount' => $product['multi_versions'] ? $product['pv_price_discount'] : $product['p_price_discount'],
                    'discount_id' => $product['multi_versions'] ? $product['pv_discount_id'] : $product['p_discount_id'],
                ];

                // get first if single
                if (!$is_mul_products) {
                    break;
                }
            }

            return $is_mul_products ? $result : $result[0];
        } else {
            return false;
        }
    }

    /**
     * get product detail
     *
     * @param int|array $product_id if multi product ids, provide array
     * @param bool $flag_status
     * @param bool $flag_deleted
     * @param bool $keep_product_id_sorted_as_input
     * @return array|bool
     */
    public function getProduct($product_id, $flag_status = true, $flag_deleted = true, $keep_product_id_sorted_as_input = false)
    {
        $is_mul_products = is_array($product_id);
        $product_ids = $is_mul_products ? $product_id : [(int)$product_id];
        if ($is_mul_products && empty($product_ids)) {
            return [];
        }
        $DB_PREFIX = DB_PREFIX;
        if ($is_mul_products) {
            $sql_product_version = " LEFT JOIN (SELECT m.product_version_id, m.price, m.compare_price, min(m.percent_pv) max_percent_pv,
                    m.product_id, m.deleted, m.image, m.sku, IFNULL( m.price_discount, 0 ) as pv_price_discount, m.discount_id  
                    FROM (SELECT pv_m.*, dp.price_discount, dp.discount_id, 
                            (CASE WHEN dp.price_discount IS NULL THEN 0 ELSE ((dp.price_discount - pv_m.compare_price)/pv_m.compare_price*100) END) as percent_pv 
                            FROM " . DB_PREFIX . "product_version pv_m
                                LEFT JOIN (SELECT dp_1.product_id, dp_1.product_version_id, 
                                                 dp_1.price_discount, dp_1.discount_id 
                                            FROM {$DB_PREFIX}discount_product AS dp_1 
                                            JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                WHERE d_1.date_started <= NOW() 
                                                AND d_1.date_ended >= NOW() 
                                                AND d_1.status = 1 
                                                AND dp_1.status = 1 
                                                AND dp_1.product_id IN (" . implode(',', $product_ids) . ") 
                                            ) AS dp  
                                    ON (pv_m.product_id = dp.product_id AND pv_m.product_version_id = dp.product_version_id) 
                                    WHERE pv_m.product_id IN (" . implode(',', $product_ids) . ") AND pv_m.deleted IS NULL) m  
                    
                    LEFT JOIN (SELECT pv_b.product_version_id, pv_b.product_id, pv_b.compare_price,
                                (CASE WHEN dp.price_discount IS NULL THEN 0 ELSE ((dp.price_discount - pv_b.compare_price)/pv_b.compare_price*100) END) as percent_pv 
                                FROM {$DB_PREFIX}product_version pv_b 
                                    LEFT JOIN (SELECT dp_1.product_id, dp_1.product_version_id, 
                                                      dp_1.price_discount, dp_1.discount_id 
                                                FROM {$DB_PREFIX}discount_product AS dp_1 
                                                JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                WHERE d_1.date_started <= NOW() 
                                                    AND d_1.date_ended >= NOW() 
                                                    AND d_1.status = 1 
                                                    AND dp_1.status = 1 
                                                    AND dp_1.product_id IN (" . implode(',', $product_ids) . ")   
                                                ) AS dp  
                                    ON (pv_b.product_id = dp.product_id AND pv_b.product_version_id = dp.product_version_id)  
                                    WHERE pv_b.product_id IN (" . implode(',', $product_ids) . ") AND pv_b.deleted IS NULL) b 
                    ON m.product_id = b.product_id 
                    AND (m.percent_pv > b.percent_pv OR (m.percent_pv = b.percent_pv AND m.compare_price > b.compare_price) )
                    WHERE m.deleted IS NULL AND b.percent_pv IS NULL 
                    GROUP BY m.product_id) product_version_percent ON CASE WHEN p.multi_versions = 1 THEN p.product_id ELSE -1 END = product_version_percent.product_id
             ";
        } else {
            $sql_product_version = " LEFT JOIN (SELECT m.product_version_id, m.price, m.compare_price, min(m.percent_pv) max_percent_pv,
                    m.product_id, m.deleted, m.image, m.sku, IFNULL( m.price_discount, 0 ) as pv_price_discount, m.discount_id 
                    FROM (SELECT pv_m.*, dp.price_discount, dp.discount_id, 
                            (CASE WHEN dp.price_discount IS NULL THEN 0 ELSE ((dp.price_discount - pv_m.compare_price)/pv_m.compare_price*100) END) as percent_pv 
                            FROM " . DB_PREFIX . "product_version pv_m
                                LEFT JOIN (SELECT dp_1.product_id, dp_1.product_version_id, 
                                                  dp_1.price_discount, dp_1.discount_id 
                                            FROM {$DB_PREFIX}discount_product AS dp_1 
                                                JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                WHERE d_1.date_started <= NOW() 
                                                    AND d_1.date_ended >= NOW() 
                                                    AND d_1.status = 1 
                                                    AND dp_1.status = 1 
                                                    AND dp_1.product_id = {$product_id} 
                                                    ) AS dp  
                                    ON (pv_m.product_id = dp.product_id AND pv_m.product_version_id = dp.product_version_id) 
                                    WHERE pv_m.product_id = {$product_id} AND pv_m.deleted IS NULL) m  
                    
                    LEFT JOIN (SELECT pv_b.product_version_id, pv_b.product_id, pv_b.compare_price, 
                                (CASE WHEN dp.price_discount IS NULL THEN 0 ELSE ((dp.price_discount - pv_b.compare_price)/pv_b.compare_price*100) END) as percent_pv 
                                FROM {$DB_PREFIX}product_version pv_b 
                                    LEFT JOIN (SELECT dp_1.product_id, dp_1.product_version_id, 
                                                      dp_1.price_discount, dp_1.discount_id 
                                                FROM {$DB_PREFIX}discount_product AS dp_1 
                                                    JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                    WHERE d_1.date_started <= NOW() 
                                                        AND d_1.date_ended >= NOW() 
                                                        AND d_1.status = 1 
                                                        AND dp_1.status = 1 
                                                        AND dp_1.product_id = {$product_id} 
                                                        ) AS dp   
                                    ON (pv_b.product_id = dp.product_id AND pv_b.product_version_id = dp.product_version_id)  
                                    WHERE pv_b.product_id = {$product_id} AND pv_b.deleted IS NULL) b 
                    ON m.product_id = b.product_id 
                    AND (m.percent_pv > b.percent_pv OR (m.percent_pv = b.percent_pv AND m.compare_price > b.compare_price) )
                    WHERE m.deleted IS NULL AND b.percent_pv IS NULL 
                    GROUP BY m.product_id) product_version_percent ON CASE WHEN p.multi_versions = 1 THEN p.product_id ELSE -1 END = product_version_percent.product_id
             ";
        }

        $condition_p0_status = $flag_status ? " p0.status = '1' " : " 1=1 ";
        $condition_p20_status = $flag_status ? " p20.status = '1' " : " 1=1 ";
        $condition_p0_delete = $flag_deleted ? " p0.`deleted` IS NULL " : " 1=1 ";
        $condition_p20_delete = $flag_deleted ? " p20.`deleted` IS NULL " : " 1=1 ";
        $condition_pv2_delete = $flag_deleted ? " pv2.`deleted` IS NULL OR pv2.`deleted` = 0 " : " 1=1 ";

        $DB_PREFIX = DB_PREFIX;
        // VERY IMPORTANT: DO NOT use DISTINCT *, please list all fields need to get if using JOIN!
        $sql = "SELECT p.*,
                  -- pd.*,
                  pd.`name` as name,
                  pd.`description`,
                  pd.`sub_description`,
                  pd.`seo_title`,
                  pd.`seo_description`,
                  pd.`tag`,
                  pd.`meta_title`,
                  pd.`meta_description`,
                  pd.`meta_keyword`,
                  pd.`noindex`,
                  pd.`nofollow`,
                  pd.`noarchive`,
                  pd.`noimageindex`,
                  pd.`nosnippet`,
                  
                  -- p2s.*,
                  p2s.`quantity`,
                  
                  -- m.*,
                  m.`name` AS manufacturer,
                  
                  -- pv.*,
                  pv.`product_version_id`,
                  
                  -- custom output
                  p.sku as sku_custom, 
                  p.status as p_status, 
                  (CASE WHEN pv.product_version_id IS NULL THEN p2s.quantity ELSE p2s.quantity END) AS product_master_quantity,
                  p.product_id as product_id_master, 
                  p.p_price_discount as price_master, 
                  p.compare_price as compare_price_master, 
                  p.discount_id as p_discount_id, 
                  
                  pvq.product_version_quantity,
                  pvq.min_price_version,
                  pvq.max_price_version,
                  pvq.price_version_check_null,
                  pvq.min_compare_price_version,
                  pvq.max_compare_price_version,
                  pvq.product_version_status_max,
                  pvq.max_percent_pv, 
                  product_version_percent.discount_id as pv_discount_id, 
                  product_version_percent.pv_price_discount as pv_price_discount, 
                  
                  product_version_percent.max_percent_pv max_percent_pv_2, 
                  product_version_percent.product_version_id product_version_id_2,
                  product_version_percent.pv_price_discount price_2,
                  product_version_percent.compare_price compare_price_2,
                  product_version_percent.image pv_image,
                  
                  -- product images
                  pim.image as second_image, 
                  
                  -- discount
                  0 AS discount, 
                  
                  -- special price
                  0 AS special,
                  
                  -- reward
                  0 AS reward,
                  
                  -- stock_status
                  0 AS stock_status,
                  
                  -- weight_class
                  0 AS weight_class,
                  
                  -- length_class
                  0 AS length_class,
                  
                  -- rating
                  0 AS rating, 
                  
                  -- reviews
                  0 AS reviews, 
                  
                  -- p.sort_order, 
                  p.deleted as p_deleted 
                  
                  FROM (SELECT p0.*, IFNULL( dp.price_discount, 0 ) as p_price_discount, dp.discount_id 
                            FROM {$DB_PREFIX}product p0
                                 LEFT JOIN (SELECT m.* 
                                                FROM
                                                    (SELECT dp_1.* 
                                                        FROM {$DB_PREFIX}discount_product AS dp_1 
                                                        JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                            WHERE d_1.date_started <= NOW() 
                                                                AND d_1.date_ended >= NOW() 
                                                                AND d_1.status = 1 
                                                                AND dp_1.status = 1 
                                                                AND dp_1.product_id IN (" . implode(',', $product_ids) . ")) AS m
                                                    LEFT JOIN (SELECT dp_1.price_discount, dp_1.product_id 
                                                        FROM {$DB_PREFIX}discount_product AS dp_1 
                                                        JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                            WHERE d_1.date_started <= NOW() 
                                                                AND d_1.date_ended >= NOW() 
                                                                AND d_1.status = 1 
                                                                AND dp_1.status = 1 
                                                                AND dp_1.product_id IN (" . implode(',', $product_ids) . ")) AS b                                                                                              
                                                    ON m.product_id = b.product_id 
                                                    AND m.price_discount > b.price_discount 
                                                    WHERE b.price_discount IS NULL 
                                                    GROUP BY m.product_id) AS dp 
                                                ON CASE WHEN p0.multi_versions = 0 THEN p0.product_id ELSE -1 END = dp.product_id
                        WHERE 1 
                          AND p0.product_id IN (" . implode(',', $product_ids) . ") 
                          AND {$condition_p0_status} 
                          AND {$condition_p0_delete} 
                       ) AS p 
                  LEFT JOIN {$DB_PREFIX}product_description pd ON (p.product_id = pd.product_id) 
                  LEFT JOIN {$DB_PREFIX}product_to_store p2s ON (p.product_id = p2s.product_id AND p.default_store_id = p2s.store_id) 
                  LEFT JOIN {$DB_PREFIX}manufacturer m ON (p.manufacturer_id = m.manufacturer_id) 
                  LEFT JOIN {$DB_PREFIX}product_version as pv ON (
                                                                     CASE WHEN p.multi_versions = 1 THEN p.product_id
                                                                     ELSE -1
                                                                     END = pv.product_id
                                                                 ) 
                  LEFT JOIN (SELECT MAX(product_image_id) last_image_id, image, product_id  
                                FROM {$DB_PREFIX}product_image pim2 GROUP BY product_id) 
                            pim ON (pim.product_id = p.product_id) 
                            
                  {$sql_product_version}
       
                  INNER JOIN (
                                  SELECT DISTINCT p2.product_id, dp.discount_id, 
                                  SUM(case when pv2.status = 1 then p2s2.quantity else 0 end) as product_version_quantity, 
                                  MIN(case when dp.price_discount > 0 then dp.price_discount else pv2.compare_price end) as min_price_version,
                                  MAX(case when dp.price_discount > 0 then dp.price_discount else pv2.compare_price end) as max_price_version,
                                  MAX(dp.price_discount) as price_version_check_null, 
                                  MIN(pv2.compare_price) as min_compare_price_version,
                                  MAX(pv2.compare_price) as max_compare_price_version, 
                                  MAX(pv2.status) as product_version_status_max, 
                                  MIN(case when dp.price_discount > 0 then ((dp.price_discount - pv2.compare_price)/pv2.compare_price*100) end) as max_percent_pv 
                                  FROM (SELECT p20.*, IFNULL( dp.price_discount, 0 ) as p_price_discount, dp.discount_id 
                                    FROM {$DB_PREFIX}product p20 
                                    LEFT JOIN (SELECT m.* 
                                                FROM
                                                    (SELECT dp_1.* 
                                                        FROM {$DB_PREFIX}discount_product AS dp_1 
                                                        JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                            WHERE d_1.date_started <= NOW() 
                                                                AND d_1.date_ended >= NOW() 
                                                                AND d_1.status = 1 
                                                                AND dp_1.status = 1 
                                                                AND dp_1.product_id IN (" . implode(',', $product_ids) . ")) AS m
                                                    LEFT JOIN (SELECT dp_1.price_discount, dp_1.product_id 
                                                        FROM {$DB_PREFIX}discount_product AS dp_1 
                                                        JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                            WHERE d_1.date_started <= NOW() 
                                                                AND d_1.date_ended >= NOW() 
                                                                AND d_1.status = 1 
                                                                AND dp_1.status = 1 
                                                                AND dp_1.product_id IN (" . implode(',', $product_ids) . ")) AS b                                                                                              
                                                    ON m.product_id = b.product_id 
                                                    AND m.price_discount > b.price_discount 
                                                    WHERE b.price_discount IS NULL 
                                                    GROUP BY m.product_id) AS dp 
                                                    ON CASE WHEN p20.multi_versions = 0 THEN p20.product_id ELSE -1 END = dp.product_id
                                         WHERE 1 
                                           AND p20.product_id IN (" . implode(',', $product_ids) . ") 
                                           AND {$condition_p20_status} 
                                           AND {$condition_p20_delete} 
                                       ) AS p2 
                                  LEFT JOIN {$DB_PREFIX}product_to_store p2s2 ON (
                                                                                     p2.product_id = p2s2.product_id 
                                                                                     AND p2.default_store_id = p2s2.store_id 
                                                                                 ) 
                                  LEFT JOIN {$DB_PREFIX}product_version as pv2 ON (
                                                                                       CASE WHEN p2.multi_versions = 1 THEN p2.product_id
                                                                                       ELSE -1
                                                                                       END = pv2.product_id
                                                                                       AND (
                                                                                           CASE WHEN p2.multi_versions = 1 THEN 
                                                                                              {$condition_pv2_delete}  
                                                                                           ELSE pv2.product_id > -1
                                                                                           END
                                                                                       )
                                                                                  )
                                  LEFT JOIN (SELECT m.* 
                                                FROM
                                                    (SELECT dp_1.* 
                                                        FROM {$DB_PREFIX}discount_product AS dp_1 
                                                        JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                            WHERE d_1.date_started <= NOW() 
                                                                AND d_1.date_ended >= NOW() 
                                                                AND d_1.status = 1 
                                                                AND dp_1.status = 1 
                                                                AND dp_1.product_id IN (" . implode(',', $product_ids) . ")) AS m
                                                    LEFT JOIN (SELECT dp_1.price_discount, dp_1.product_id 
                                                        FROM {$DB_PREFIX}discount_product AS dp_1 
                                                        JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                            WHERE d_1.date_started <= NOW() 
                                                                AND d_1.date_ended >= NOW() 
                                                                AND d_1.status = 1 
                                                                AND dp_1.status = 1 
                                                                AND dp_1.product_id IN (" . implode(',', $product_ids) . ")) AS b                                                                                              
                                                    ON m.product_id = b.product_id 
                                                    AND m.price_discount > b.price_discount 
                                                    WHERE b.price_discount IS NULL 
                                                    GROUP BY m.product_id) AS dp 
                                    ON (pv2.product_id = dp.product_id AND pv2.product_version_id = dp.product_version_id)  
                                  WHERE 1 
                                  GROUP BY p2.product_id 
                             ) AS pvq ON (pvq.product_id = p.product_id) 
                  WHERE 1 
                    AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
                    AND p.date_available <= NOW() 
                  GROUP BY p.product_id, pv.product_version_id "; // No need use: AND p.channel IN (" . $this->db->escape(implode(',', $channel)) . ") "; TODO: remove...

        if ($keep_product_id_sorted_as_input) {
            $sql .= " ORDER BY FIELD(p.product_id," . implode(',', $product_ids) . ") ";
        }

        // get result from cache first
        $cache_key = DB_PREFIX . 'product:getProduct_' . md5($sql);
        $cache_products = $this->cache->get($cache_key);
        if ($cache_products) {
            $products = $cache_products;
        } else {
            // get result from db
            $query = $this->db->query($sql);
            if ($query->num_rows) {
                $products = $query->rows;
                $this->cache->set($cache_key, $products);
            }
        }

        $product_id_to_categories = [];
        $categories = $this->getCategoriesCustom($product_ids);
        if ($categories) {
            foreach ($categories as $category) {
                if (!isset($category['product_id'])) {
                    continue;
                }

                $p_id = $category['product_id'];
                if (!array_key_exists($p_id, $product_id_to_categories)) {
                    $product_id_to_categories[$p_id] = [];
                }

                if (!empty($category['keyword'])) {
                    $href = $this->url->link('common/shop') .'/'. $category['keyword'];
                } else {
                    $href = $this->url->link('common/shop', 'category=' . $category['category_id'], true);
                }

                $product_id_to_categories[$p_id][] = [
                    'id' => $category['category_id'],
                    'name' => $category['name'],
                    'image' => $category['image'],
                    'href' => $href,
                ];
            }
        }

        if (!empty($products)) {
            $result = [];
            foreach ($products as $product) {
                $data['categories'] = isset($product_id_to_categories[$product['product_id_master']])
                    ? $product_id_to_categories[$product['product_id_master']]
                    : [];

                $p_image = '';
                if ($product['multi_versions']) {
                    if(!empty($product['pv_image']))  {
                        $p_image = $product['pv_image'];
                    } else {
                        $p_image = $product['image'];
                    }
                } else {
                    $p_image = $product['image'];
                }

                $result[] = [
                    'product_id' => $product['product_id_master'],
                    'name' => $product['name'],
                    'description' => $product['description'],
                    'sub_description' => $product['sub_description'],
                    'meta_title' => $product['meta_title'],
                    'meta_description' => $product['meta_description'],
                    'meta_keyword' => $product['meta_keyword'],
                    'noindex' => $product['noindex'],
                    'nofollow' => $product['nofollow'],
                    'noarchive' => $product['noarchive'],
                    'noimageindex' => $product['noimageindex'],
                    'nosnippet' => $product['nosnippet'],
                    'tag' => $product['tag'],
                    'model' => $product['model'],
                    'sku' => $product['sku_custom'],
                    'upc' => $product['upc'],
                    'ean' => $product['ean'],
                    'jan' => $product['jan'],
                    'isbn' => $product['isbn'],
                    'mpn' => $product['mpn'],
                    'location' => $product['location'],
                    'quantity' => $product['quantity'],
                    'stock_status' => $product['stock_status'],
                    'image' => $p_image,
                    'second_image' => $product['second_image'],
                    'manufacturer_id' => $product['manufacturer_id'],
                    'manufacturer' => $product['manufacturer'],
                    'price' => $product['price_master'],
                    'special' => $product['special'],
                    'reward' => $product['reward'],
                    'points' => $product['points'],
                    'tax_class_id' => $product['tax_class_id'],
                    'date_available' => $product['date_available'],
                    'weight' => $product['weight'],
                    'weight_class_id' => $product['weight_class_id'],
                    'length' => $product['length'],
                    'width' => $product['width'],
                    'height' => $product['height'],
                    'length_class_id' => $product['length_class_id'],
                    'subtract' => $product['subtract'],
                    'rating' => round($product['rating']),
                    'reviews' => $product['reviews'] ? $product['reviews'] : 0,
                    'minimum' => $product['minimum'],
                    'sort_order' => $product['sort_order'],
                    'status' => $product['p_status'],
                    'date_added' => $product['date_added'],
                    'date_modified' => $product['date_modified'],
                    'viewed' => $product['viewed'],
                    'min_price_version' => $product['min_price_version'],
                    'max_price_version' => $product['max_price_version'],
                    'min_compare_price_version' => $product['min_compare_price_version'],
                    'max_compare_price_version' => $product['max_compare_price_version'],
                    'price_master' => $product['price_master'],
                    'price_2' => $product['price_2'],
                    'compare_price_2' => $product['compare_price_2'],
                    'compare_price_master' => $product['compare_price_master'],
                    'product_version_quantity' => $product['product_version_quantity'],
                    'product_master_quantity' => $product['product_master_quantity'],
                    'categories' => $data['categories'],
                    'product_version_id' => $product['product_version_id_2'],
                    'sale_on_out_stock' => $product['sale_on_out_of_stock'],
                    'price_version_check_null' => $product['price_version_check_null'],
                    'multi_versions' => $product['multi_versions'],
                    'product_version_status_max' => $product['product_version_status_max'],
                    'max_percent_pv' => $product['multi_versions'] ? $product['max_percent_pv_2'] : $product['max_percent_pv'],
                    'default_store_id' => $product['default_store_id'],
                    'deleted' => $product['p_deleted'],
                    'price_discount' => $product['multi_versions'] ? $product['pv_price_discount'] : $product['p_price_discount'],
                    'discount_id' => $product['multi_versions'] ? $product['pv_discount_id'] : $product['p_discount_id'],
                ];

                // get first if single
                if (!$is_mul_products) {
                    break;
                }
            }

            return $is_mul_products ? $result : $result[0];
        } else {
            return false;
        }
    }

    public function getHotlineValue($store_id = 0)
    {
        $query = $this->db->query("SELECT `value` FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = 'config_hotline'");

        return $query->row;
    }

    public function getProductsByCategoryId($category_id)
    {
        $sql = "SELECT * 
                FROM " . DB_PREFIX . "product p 
                    LEFT JOIN " . DB_PREFIX . "product_description pd  ON (p.product_id = pd.product_id) 
                    LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) 
                    LEFT JOIN " . DB_PREFIX . "category c ON (c.category_id = p2c.category_id) 
                    WHERE 
                        pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND
                        (p2c.category_id = ".(int)$category_id." OR c.parent_id = ".(int)$category_id.") AND
                        p.deleted IS NULL 
                    ORDER BY pd.name ASC";

        // get result from cache first (temporary comment)
//        $cache_key = DB_PREFIX . 'product:getProductsByCategoryId_' . md5($sql);
//        $cache_products = $this->cache->get($cache_key);
//        if ($cache_products) {
//            $products = $cache_products;
//        } else {
            // get result from db
            $query = $this->db->query($sql);
//            if ($query->num_rows) {
                $products = $query->rows;
//                $this->cache->set($cache_key, $products);
//            }
//        }

        if (empty($products)) {
            return [];
        }

        $product_ids = array_map(function ($product) {
            return $product['product_id'];
        }, $products);

        $product_data = $this->getProducts([
            'filter_product_ids' => array_unique($product_ids)
        ]);

        return $product_data;
    }

    public function getProducts($data = array())
    {
        $sql = "SELECT p.product_id, (SELECT AVG(rating) AS total 
                FROM " . DB_PREFIX . "review r1 
                WHERE r1.product_id = p.product_id 
                AND r1.status = '1' 
                GROUP BY r1.product_id) AS rating, 
                ( SELECT price FROM " . DB_PREFIX . "product_discount pd2 
                  WHERE pd2.product_id = p.product_id 
                  AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' 
                  AND pd2.quantity = '1' 
                  AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) 
                  AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) 
                  ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, 
                  ( SELECT price FROM " . DB_PREFIX . "product_special ps 
                    WHERE ps.product_id = p.product_id 
                    AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' 
                    AND ((ps.date_start = '0000-00-00' 
                    OR ps.date_start < NOW()) 
                    AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())
                   ) 
                  ORDER BY ps.priority ASC, ps.price ASC LIMIT 1
                ) AS special";

        /* filter category */
        if (!empty($data['filter_category_id'])) {
            if (!empty($data['filter_sub_category'])) {
                $sql .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";
            } else {
                $sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
            }

            if (!empty($data['filter_filter'])) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) 
                          LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
            } else {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
            }
        } else {
            $sql .= " FROM " . DB_PREFIX . "product p";
        }

        $sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) 
                  LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id AND p.default_store_id = p2s.store_id) 
                  WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
                  AND p.status = '1' 
                  AND p.date_available <= NOW() ";

        if (!empty($data['filter_category_id'])) {
            if (!empty($data['filter_category_ids'])) {
                $categories = is_array($data['filter_category_ids']) ? $data['filter_category_ids'] : [$data['filter_category_ids']];
                $categories = implode(",", $categories);
                $sql .= " AND p2c.category_id IN (" . $categories . ")";
            } elseif (!empty($data['filter_sub_category'])) {
                $sql .= " AND cp.path_id = '" . (int)$data['filter_category_id'] . "'";
            } else {
                $sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
            }

            if (!empty($data['filter_filter'])) {
                $implode = array();

                $filters = explode(',', $data['filter_filter']);

                foreach ($filters as $filter_id) {
                    $implode[] = (int)$filter_id;
                }

                $sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
            }
        }

        /* filter by product ids */
        if (isset($data['filter_product_ids']) && is_array($data['filter_product_ids'])) {
            $product_ids_query = implode(', ', $data['filter_product_ids']);
            $sql .= " AND (p.product_id IN ($product_ids_query)) ";
        }

        /* filter by product name, tag */
        if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
            $sql .= " AND (";

            if (!empty($data['filter_name'])) {
                $implode = array();

                $words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_name'])));

                foreach ($words as $word) {
                    $implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
                }

                if ($implode) {
                    $sql .= " " . implode(" AND ", $implode) . "";
                }

                if (!empty($data['filter_description'])) {
                    $sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
                }
            }

            if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
                $sql .= " OR ";
            }

            if (!empty($data['filter_tag'])) {
                $implode = array();

                $words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_tag'])));

                foreach ($words as $word) {
                    $implode[] = "pd.tag LIKE '%" . $this->db->escape($word) . "%'";
                }

                if ($implode) {
                    $sql .= " " . implode(" AND ", $implode) . "";
                }
            }

            if (!empty($data['filter_name'])) {
                $sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
            }

            $sql .= ")";
        }

        /* filter by manufacturer */
        if (!empty($data['filter_manufacturer_id'])) {
            $sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer_id'] . "'";
        } else if (isset($data['filter_manufacturer_id']) && $data['filter_manufacturer_id'] === '0') {
            $sql .= " AND p.manufacturer_id = '0'";
        }

        /* filter by price min */
        if (!empty($data['price_min'])) {
            $sql .= " AND p.price >= '" . $data['price_min'] . "'";
        }

        /* filter by price max */
        if (!empty($data['price_max'])) {
            $sql .= " AND p.price <= '" . $data['price_max'] . "'";
        }

        $sql .= " GROUP BY p.product_id";

        /* sort */
        $sort_data = array(
            'pd.name',
            'p.model',
            'p.quantity',
            'p.price',
            'rating',
            'p.sort_order',
            'p.date_added',
            'input_product_ids',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
                $sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
            } elseif ($data['sort'] == 'p.price') {
                $sql .= " ORDER BY (CASE WHEN special IS NOT NULL THEN special WHEN discount IS NOT NULL THEN discount ELSE p.price END)";
            } elseif ($data['sort'] == 'input_product_ids') {
                if (isset($data['filter_product_ids']) && is_array($data['filter_product_ids'])) {
                    $sql .= " ORDER BY FIELD(p.product_id," . implode(',', $data['filter_product_ids']) . ") ";
                }
            } else {
                $sql .= " ORDER BY " . $data['sort'];
            }
        } else {
            $sql .= " ORDER BY p.sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC, LCASE(pd.name) DESC";
        } else {
            $sql .= " ASC, LCASE(pd.name) ASC";
        }

        /* pagination */
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        // get result from cache first
        $cache_key = DB_PREFIX . 'product:getProducts_' . md5($sql);
        $cache_products = $this->cache->get($cache_key);
        if ($cache_products) {
            $products_result = $cache_products;
        } else {
            // get result from db
            $query = $this->db->query($sql);
            $products_result = $query->rows;

            if ($query->num_rows) {
                $this->cache->set($cache_key, $products_result);
            }
        }

        $product_data = [];

        // TODO: use query IN... read getLatestProduct() for example
        $product_ids = [];
        foreach ($products_result as $result) {
            if (!isset($result['product_id'])) {
                continue;
            }

            $product_ids[] = $result['product_id'];
        }

        $keep_product_id_sorted_as_input = (isset($data['sort']) && $data['sort'] == 'input_product_ids');

        $products = $this->getProductOptimize($product_ids, $flag_status = true, $flag_deleted = true, $keep_product_id_sorted_as_input);
        if (!is_array($products)) {
            return $product_data;
        }

        foreach ($products as $p) {
            if (!isset($p['product_id'])) {
                continue;
            }

            $product_data[$p['product_id']] = $p;
        }

        return $product_data;
    }

    /**
     * get list product with filter instead of function getProduct (faster than old function)
     * TODO: make sure getProductsCustomOptimize() return output same getProductsCustom() with same input,
     * TODO: then replace full usage of getProductsCustom(),
     * TODO: then may remove getProductsCustom() function
     *
     * @param $filter_data
     * @return mixed
     */
    public function getProductsCustomOptimize($filter_data)
    {
        /*
         * Notice:
         * FULLTEXT by default has a minimum word length, usually 4 chars (see mysql environment variable 'ft_min_word_length')
         * FULLTEXT indexes on MyISAM tables must be rebuilt after changing this variable. Use REPAIR TABLE tbl_name QUICK.
         * See:
         * - https://stackoverflow.com/questions/13130068/mysql-fulltext-search-query-missing-results
         * - https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_ft_min_word_len
         * Temp disabled. TODO: implement later or remove...
         */

        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT DISTINCT *, 
        p.sku as sku_custom,
        pv.product_version_quantity, 
        p.quantity as product_master_quantity, 
        p.product_id as product_id_master, 
        pd.name AS name, 
        -- FULLTEXT search score
        -- MATCH(pd.`name`) AGAINST('". $this->db->escape(isset($filter_data['search']) ? trim($filter_data['search']) : '') ."') as score, 
        p.image,     
        p.sku,
        p.barcode,
        p.product_id AS product_id,    
        m.name AS manufacturer, 
        0 AS special,
        0 AS discount, 
        0 AS rating, 
        dp.price_discount as price_master, 
        p.compare_price as compare_price_master, 
        pv.min_price_version,
        pv.max_price_version,
        pv.price_version_check_null,
        pv.min_compare_price_version,
        pv.product_version_status_max ,
        pv.max_percent_pv,
        product_version_percent.max_percent_pv max_percent_pv_2, 
        product_version_percent.product_version_id product_version_id_2,
        product_version_percent.pv_price_discount price_2,
        product_version_percent.compare_price compare_price_2,
        product_version_percent.sku product_version_percent_sku, 
        product_version_percent.image pv_image,
        -- discount
        dp.price_discount as p_price_discount,
        dp.discount_id as p_discount_id,
        product_version_percent.pv_discount_id AS pv_discount_id 
         ";

        $sql .= " FROM " . DB_PREFIX . "product p 
                    left join " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id)";

        $sql .= " LEFT JOIN (SELECT m.* 
                                FROM
                                    (SELECT dp_1.* 
                                        FROM {$DB_PREFIX}discount_product AS dp_1 
                                        JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                            WHERE d_1.date_started <= NOW() 
                                                AND d_1.date_ended >= NOW() 
                                                AND d_1.status = 1 
                                                AND dp_1.status = 1 ) AS m
                                    LEFT JOIN (SELECT dp_1.price_discount, dp_1.product_id 
                                        FROM {$DB_PREFIX}discount_product AS dp_1 
                                        JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                            WHERE d_1.date_started <= NOW() 
                                                AND d_1.date_ended >= NOW() 
                                                AND d_1.status = 1 
                                                AND dp_1.status = 1 ) AS b                                                                                              
                                    ON m.product_id = b.product_id 
                                    AND m.price_discount > b.price_discount 
                                    WHERE b.price_discount IS NULL 
                                    GROUP BY m.product_id) AS dp 
                 ON CASE WHEN p.multi_versions = 0 THEN p.product_id ELSE -1 END = dp.product_id";

        $sql .= " LEFT JOIN (SELECT 
                                IFNULL( dp.price_discount, 0 ) as pv_price_discount, dp.discount_id as pv_discount_id, 
                                MIN(case when IFNULL( dp.price_discount, 0 ) > 0 then dp.price_discount else pv.compare_price end) as min_price_version,
                                MAX(case when IFNULL( dp.price_discount, 0 ) > 0 then dp.price_discount else pv.compare_price end) as max_price_version,
                                MAX(IFNULL( dp.price_discount, 0 )) as price_version_check_null,
                                MIN(pv.compare_price) as min_compare_price_version,
                                MAX(pv.compare_price) as max_compare_price_version, MAX(pv.status) as product_version_status_max,
                                MIN(CASE WHEN dp.price_discount IS NULL THEN 0 
                                        ELSE 
                                            (CASE WHEN dp.price_discount = 0 THEN -100 
                                            ELSE ((dp.price_discount - pv.compare_price)/pv.compare_price*100) END) 
                                        END) as max_percent_pv, 
                                SUM(case when pv.status = 1 then pv.quantity else 0 end) as product_version_quantity,
                                pv.deleted,
                                pv.product_id
                            FROM {$DB_PREFIX}product_version as pv
                            LEFT JOIN (SELECT dp_1.* FROM {$DB_PREFIX}discount_product AS dp_1 
                                    JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                    WHERE d_1.date_started <= NOW() 
                                        AND d_1.date_ended >= NOW() 
                                        AND d_1.status = 1 
                                        AND dp_1.status = 1 
                                        ) dp  
                            ON (pv.product_id = dp.product_id AND pv.product_version_id = dp.product_version_id)
                            WHERE deleted IS null
                            GROUP BY pv.product_id) pv 
                            ON CASE WHEN p.multi_versions = 1 THEN p.product_id ELSE -1 END = pv.product_id ";

        $sql .= " LEFT JOIN (SELECT m.product_version_id, m.price, m.compare_price, min(m.percent_pv) max_percent_pv, m.product_id, m.deleted, m.image, m.sku, 
                                IFNULL( m.price_discount, 0 ) as pv_price_discount, m.discount_id as pv_discount_id 
                    FROM (SELECT pv_m.*, dp.price_discount, dp.discount_id, 
                        (CASE WHEN dp.price_discount IS NULL THEN 0 
                                ELSE 
                                    (CASE WHEN dp.price_discount = 0 THEN -100 
                                    ELSE ((dp.price_discount - pv_m.compare_price)/pv_m.compare_price*100) END)
                                END) as percent_pv 
                            FROM {$DB_PREFIX}product_version pv_m 
                                LEFT JOIN (SELECT dp_1.* FROM {$DB_PREFIX}discount_product AS dp_1 
                                                JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                WHERE d_1.date_started <= NOW() 
                                                    AND d_1.date_ended >= NOW() 
                                                    AND d_1.status = 1 
                                                    AND dp_1.status = 1 
                                                    ) dp  
                                    ON (pv_m.product_id = dp.product_id AND pv_m.product_version_id = dp.product_version_id) 
                                    WHERE pv_m.deleted IS NULL) m        
                    LEFT JOIN (SELECT pv_b.product_version_id, pv_b.product_id, pv_b.compare_price, dp.price_discount, dp.discount_id, 
                                (CASE WHEN dp.price_discount IS NULL THEN 0 
                                ELSE 
                                    (CASE WHEN dp.price_discount = 0 THEN -100 
                                    ELSE ((dp.price_discount - pv_b.compare_price)/pv_b.compare_price*100) END)
                                END) as percent_pv 
                                FROM {$DB_PREFIX}product_version pv_b 
                                    LEFT JOIN (SELECT dp_1.* FROM {$DB_PREFIX}discount_product AS dp_1 
                                                    JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                                    WHERE d_1.date_started <= NOW() 
                                                        AND d_1.date_ended >= NOW() 
                                                        AND d_1.status = 1 
                                                        AND dp_1.status = 1 
                                                        ) dp  
                                    ON (pv_b.product_id = dp.product_id AND pv_b.product_version_id = dp.product_version_id) 
                                    WHERE pv_b.deleted IS NULL) b 
                        ON m.product_id = b.product_id 
                        AND (m.percent_pv > b.percent_pv OR (m.percent_pv = b.percent_pv AND m.compare_price > b.compare_price) )
                        WHERE m.deleted IS NULL AND b.percent_pv IS NULL 
                        GROUP BY m.product_id) product_version_percent 
                            ON CASE WHEN p.multi_versions = 1 THEN p.product_id ELSE -1 END = product_version_percent.product_id ";

        $sql .= " left join " . DB_PREFIX . "manufacturer m ON (m.manufacturer_id = p.manufacturer_id)";

        if (isset($filter_data['store_id'])) {
            $sql .= " left join " . DB_PREFIX . "product_to_store pts ON (pts.product_id = p.product_id)";
        }

        if (isset($filter_data['filter_category_id']) || !empty($filter_data['categories_ids'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category c ON (p.product_id = c.product_id)";
        }

        if (isset($filter_data['filter_collection_id'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_collection pc ON (p.product_id = pc.product_id)";
            $sql .= " LEFT JOIN " . DB_PREFIX . "collection cl ON (cl.collection_id = pc.collection_id)";
        }

        if (isset($filter_data['products_block_name'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_collection pc ON (p.product_id = pc.product_id)";
        }

        if (isset($filter_data['attribute_value']) && !empty($filter_data['attribute_value'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_attribute_filter paf ON (p.product_id = paf.product_id)";
        }

        $sql .= " LEFT JOIN " . DB_PREFIX . "product_tag pt ON (pt.product_id = p.product_id)";
        $sql .= " LEFT JOIN " . DB_PREFIX . "tag t ON (t.tag_id = pt.tag_id)";

        $sql .= " WHERE pd.language_id = " . (int)$this->config->get('config_language_id') . " and p.status = 1 AND p.`deleted` IS NULL ";
        $sql .= " AND  pv.`deleted` IS NULL ";

        if (isset($filter_data['filter_nametag']) && !empty(($filter_data['filter_nametag']))) {
            $sql .= ' AND t.tag_id IN (' . $this->db->escape($filter_data['filter_nametag']) . ')';
        }

        if (isset($filter_data['search']) && !empty($filter_data['search'])) {
            // FULLTEXT search
            //$sql .= " AND ( MATCH(pd.`name`) AGAINST('" . $this->db->escape(trim($filter_data['search'])) . "')";
            //$sql .= " OR t.`value` LIKE '%" . $this->db->escape(trim($filter_data['search'])) . "%' ) ";

            $n_searches = [];

            // add each word for searching
            $name_search = $filter_data['search'];
            $names = explode(' ', $name_search);
            foreach ($names as $ns) {
                $n_searches[] = "pd.`name` LIKE '%$ns%'";
            }

            $n_searches_sql = implode(' OR ', $n_searches);

            $sql .= " AND ( $n_searches_sql 
                            OR t.`value` LIKE '%" . $this->db->escape(trim($filter_data['search'])) . "%' 
                    ) ";
        }

        if (isset($filter_data['channel'])) {
            $sql .= ' AND p.channel IN (' . $this->db->escape($filter_data['channel']) . ')';
        } else {
            // default for web call
            $sql.= "AND p.channel IN (" . $this->db->escape(implode(',', self::$CHANNELS_FOR_WEB)) . ") ";
        }

        if (isset($filter_data['store_id'])) {
            $sql .= " AND pts.store_id = '" . $this->db->escape(trim($filter_data['store_id'])) . "'";
        }

        if (isset($filter_data['filter_manufacturer_id']) && !empty(($filter_data['filter_manufacturer_id']))) {
            $sql .= ' AND m.manufacturer_id IN (' . $this->db->escape($filter_data['filter_manufacturer_id']) . ')';
        }

        if (isset($filter_data['filter_category_id']) && !empty($filter_data['filter_category_id'])) {
            $sub_category_ids = $this->getAllCategoriesByParent($filter_data['filter_category_id']);
            $sql .= ' AND c.category_id IN (' . implode(',', $sub_category_ids) . ')';
        }

        if (!empty($filter_data['categories_ids'])) {
            $sql .= " AND c.category_id IN (". implode(',', $filter_data['categories_ids']) .") ";
        }

        if (isset($filter_data['filter_collection_id']) && !empty($filter_data['filter_collection_id'])) {
            $sql .= ' AND pc.collection_id IN (' . $this->db->escape($filter_data['filter_collection_id']) . ') AND cl.`status` = 1 ';
        }

        if (isset($filter_data['products_block_name']) && !empty($filter_data['products_block_name'])) {
            $sql .= ' AND pc.collection_id IN (' . $this->db->escape($filter_data['products_block_name']) . ')';
        }

        if (isset($filter_data['attribute_value']) && !empty($filter_data['attribute_value'])) {
            $sql .= " AND paf.attribute_filter_value = '{$filter_data['attribute_value']}'";
        }

        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '') {
            $sql .= " AND pd.`name` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' ";
        }

        if (isset($filter_data['show_gg_product_feed']) && $filter_data['show_gg_product_feed'] !== '') {
            $sql .= " AND p.show_gg_product_feed = '{$filter_data['show_gg_product_feed']}'";
        }

        if (!empty($filter_data['price_min'])) {
            $sql .= " AND CASE p.multi_versions 
                        WHEN 0 
                        THEN (case when p.price > 0 then p.price >= '".$filter_data['price_min']."' else p.compare_price >= '".$filter_data['price_min']."' end) 
                        ELSE (case when pv.min_price_version > 0 then pv.min_price_version >= '".$filter_data['price_min']."' else pv.min_compare_price_version >= '".$filter_data['price_min']."' end) END";
//            $sql .= " AND CASE p.multi_versions WHEN 0 THEN (case when p.price > 0 then p.price >= '" . $filter_data['price_min'] . "' else p.compare_price >= '" . $filter_data['price_min'] . "' end) ELSE (SELECT IF(min(pvc.price) > 0, min(pvc.price), min(pvc.compare_price))  from " . DB_PREFIX . "product_version as pvc where p.product_id = pvc.product_id) >= '" . $filter_data['price_min'] . "' END";
            //$sql .= " AND compare_price_product >= '" . $filter_data['price_min'] . "'";
        }

        if (!empty($filter_data['price_max'])) {
            $sql .= " AND CASE p.multi_versions 
                        WHEN 0 
                        THEN (case when p.price > 0 then p.price <= '".$filter_data['price_max']."' else p.compare_price <= '".$filter_data['price_max']."' end) 
                        ELSE (case when pv.max_price_version > 0 then pv.max_price_version <= '".$filter_data['price_max']."' else pv.max_compare_price_version <= '".$filter_data['price_max']."' end) END";
//            $sql .= " AND CASE p.multi_versions WHEN 0 THEN (case when p.price > 0 then p.price <= '" . $filter_data['price_max'] . "' else p.compare_price <= '" . $filter_data['price_max'] . "' end) ELSE (SELECT IF(min(pvc.price) > 0, min(pvc.price), min(pvc.compare_price)) from " . DB_PREFIX . "product_version as pvc where p.product_id = pvc.product_id) <= '" . $filter_data['price_max'] . "' END";
            //$sql .= " AND compare_price_product <= '" . $filter_data['price_max'] . "'";
        }

        $sql = $sql . " group by p.product_id";

        if (isset($filter_data['valueoptiton']) && $filter_data['valueoptiton']) {
            if ($filter_data['valueoptiton'] == 1) {
                $sql = $sql . " ORDER BY CONVERT(pd.name USING utf8) COLLATE utf8_vietnamese_ci ASC";
            }

            if ($filter_data['valueoptiton'] == 2) {
                $sql = $sql . " ORDER BY CONVERT(pd.name USING utf8) COLLATE utf8_vietnamese_ci DESC";
            }

            if ($filter_data['valueoptiton'] == 3) {
                $sql .= " ORDER BY case when p.multi_versions = 0 
                            then case when p.price = 0 then p.compare_price 
                                  else p.price 
                                  END 
                            else case when pv.min_price_version = 0 then pv.min_compare_price_version
                                  else pv.min_price_version
                                  END 
                        end DESC";
            }

            if ($filter_data['valueoptiton'] == 4) {
                $sql .= " ORDER BY case when p.multi_versions = 0 
                            then case when p.price = 0 then p.compare_price 
                                  else p.price 
                                  END 
                            else case when pv.min_price_version = 0 then pv.min_compare_price_version
                                  else pv.min_price_version
                                  END 
                        end ASC";
            }

        } elseif (isset($filter_data['search']) && !empty($filter_data['search'])) {
            // FULLTEXT search
            //$sql = $sql . " ORDER BY score DESC";

            // Order with full search more priority
            // TODO: advance with words combination. E.g: for searching "A B C" then order by:
            // A B C
            // A B
            // A C
            // B C
            // A
            // B
            // C
            $sql .= " ORDER BY CASE 
                                   WHEN pd.`name` LIKE '%" . $this->db->escape(trim($filter_data['search'])) . "%' THEN 1 
                                   ELSE 2 
                               END ";
        } else {
            $sql = $sql . " ORDER BY p.date_modified DESC";
        }

        if (isset($filter_data['start']) || isset($filter_data['limit'])) {
            if ($filter_data['start'] < 0) {
                $filter_data['start'] = 0;
            }

            if ($filter_data['limit'] < 1) {
                $filter_data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
        }

        // get result from cache first
        $cache_key = DB_PREFIX . 'product:getProductsCustomOptimize_' . md5($sql);
        $cache_products = $this->cache->get($cache_key);
        if ($cache_products) {
            return $cache_products;
        } else {
            // get result from db
            $query = $this->db->query($sql);
            $products = $query->rows;

            if ($query->num_rows) {
                $this->cache->set($cache_key, $products);
            }

            return $products;
        }
    }

    public function getProductDescriptionTab($product_id)
    {
        $db_prefix = DB_PREFIX;
        $sql = "SELECT * FROM `{$db_prefix}product_description_tab`
                WHERE product_id = '" . $product_id . "'";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getIngredientOfProductByProductId($product_id)
    {
        try {
            $db_prefix = DB_PREFIX;
            $sql = "SELECT pi.*, i.name, i.code, i.active_ingredient, i.unit, i.basic_description 
                        FROM `{$db_prefix}product_ingredient` as pi 
                            LEFT JOIN `{$db_prefix}ingredient` as i ON (pi.ingredient_id = i.ingredient_id) 
                                WHERE `product_id` = {$product_id}";

            $query = $this->db->query($sql);
            return $query->rows;
        } catch (Exception $exception) {
            $this->log->write('[getIngredientOfProductByProductId] error : ' . $exception->getMessage());
            return [];
        }
    }

    public function getProductsCustom($filter_data)
    {
        /*
         * Notice:
         * FULLTEXT by default has a minimum word length, usually 4 chars (see mysql environment variable 'ft_min_word_length')
         * FULLTEXT indexes on MyISAM tables must be rebuilt after changing this variable. Use REPAIR TABLE tbl_name QUICK.
         * See:
         * - https://stackoverflow.com/questions/13130068/mysql-fulltext-search-query-missing-results
         * - https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_ft_min_word_len
         * Temp disabled. TODO: implement later or remove...
         */

        $sql = "SELECT DISTINCT *, 
        p.sku as sku_custom,
        SUM(case when pv.status = 1 then pv.quantity else 0 end) as product_version_quantity, 
        p.quantity as product_master_quantity, 
        p.product_id as product_id_master, 
        pd.name AS name, 
        -- FULLTEXT search score
        -- MATCH(pd.`name`) AGAINST('". $this->db->escape(isset($filter_data['search']) ? trim($filter_data['search']) : '') ."') as score, 
        p.image,     
        p.sku,
        p.barcode,
        p.product_id AS product_id,    
        m.name AS manufacturer, 
        0 AS special,
        0 AS discount, 
        0 AS rating, 
        p.price as price_master, 
        p.compare_price as compare_price_master, 
        MIN(case when pv.price > 0 then pv.price else pv.compare_price end) as min_price_version, 
        MAX(case when pv.price > 0 then pv.price else pv.compare_price end) as max_price_version, 
        MAX(pv.price) as price_version_check_null, 
        MIN(pv.compare_price) as min_compare_price_version, 
        MAX(pv.compare_price) as max_compare_price_version, MAX(pv.status) as product_version_status_max ,
        MIN(case when pv.price > 0 then ((pv.price - pv.compare_price)/pv.compare_price*100) end) as max_percent_pv ";

        $sql .= " from " . DB_PREFIX . "product p left join " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id)";
        $sql .= " left join " . DB_PREFIX . "product_version pv ON CASE WHEN p.multi_versions = 1 THEN p.product_id ELSE -1 END = pv.product_id ";
        $sql .= "left join " . DB_PREFIX . "manufacturer m ON (m.manufacturer_id = p.manufacturer_id)";

        if (isset($filter_data['store_id'])) {
            $sql .= "left join " . DB_PREFIX . "product_to_store pts ON (pts.product_id = p.product_id)";
        }

        if (isset($filter_data['filter_category_id'])) {
            $sql .= "LEFT JOIN " . DB_PREFIX . "product_to_category c ON (p.product_id = c.product_id)";
        }

        if (isset($filter_data['filter_collection_id'])) {
            $sql .= "LEFT JOIN " . DB_PREFIX . "product_collection pc ON (p.product_id = pc.product_id)";
            $sql .= "LEFT JOIN " . DB_PREFIX . "collection cl ON (cl.collection_id = pc.collection_id)";
        }

        if (isset($filter_data['products_block_name'])) {
            $sql .= "LEFT JOIN " . DB_PREFIX . "product_collection pc ON (p.product_id = pc.product_id)";
        }

        $sql .= "LEFT JOIN " . DB_PREFIX . "product_tag pt ON (pt.product_id = p.product_id)";
        $sql .= "LEFT JOIN " . DB_PREFIX . "tag t ON (t.tag_id = pt.tag_id)";

        $sql .= " WHERE pd.language_id = " . (int)$this->config->get('config_language_id') . " and p.status = 1 AND p.`deleted` IS NULL ";
        $sql .= " AND  pv.`deleted` IS NULL ";

        if (isset($filter_data['filter_nametag']) && !empty(($filter_data['filter_nametag']))) {
            $sql .= ' AND t.tag_id IN (' . $this->db->escape($filter_data['filter_nametag']) . ')';
        }

        if (isset($filter_data['search']) && !empty($filter_data['search'])) {
            // FULLTEXT search
            //$sql .= " AND ( MATCH(pd.`name`) AGAINST('" . $this->db->escape(trim($filter_data['search'])) . "')";
            //$sql .= " OR t.`value` LIKE '%" . $this->db->escape(trim($filter_data['search'])) . "%' ) ";

            $n_searches = [];

            // add each word for searching
            $name_search = $filter_data['search'];
            $names = explode(' ', $name_search);
            foreach ($names as $ns) {
                $n_searches[] = "pd.`name` LIKE '%$ns%'";
            }

            $n_searches_sql = implode(' OR ', $n_searches);

            $sql .= " AND ( $n_searches_sql 
                            OR t.`value` LIKE '%" . $this->db->escape(trim($filter_data['search'])) . "%' 
                    ) ";
        }

        if (isset($filter_data['channel'])) {
            $sql .= ' AND p.channel IN (' . $this->db->escape($filter_data['channel']) . ')';
        } else {
            // default for web call
            $sql.= "AND p.channel IN (" . $this->db->escape(implode(',', self::$CHANNELS_FOR_WEB)) . ") ";
        }

        if (isset($filter_data['store_id'])) {
            $sql .= " AND pts.store_id = '" . $this->db->escape(trim($filter_data['store_id'])) . "'";
        }

        if (isset($filter_data['filter_manufacturer_id']) && !empty(($filter_data['filter_manufacturer_id']))) {
            $sql .= ' AND m.manufacturer_id IN (' . $this->db->escape($filter_data['filter_manufacturer_id']) . ')';
        }

        if (isset($filter_data['filter_category_id']) && !empty($filter_data['filter_category_id'])) {
            $sub_category_ids = $this->getAllCategoriesByParent($filter_data['filter_category_id']);
            $sql .= ' AND c.category_id IN (' . implode(',', $sub_category_ids) . ')';
        }

        if (isset($filter_data['filter_collection_id']) && !empty($filter_data['filter_collection_id'])) {
            $sql .= ' AND pc.collection_id IN (' . $this->db->escape($filter_data['filter_collection_id']) . ') AND cl.`status` = 1 ';
        }

        if (isset($filter_data['products_block_name']) && !empty($filter_data['products_block_name'])) {
            $sql .= ' AND pc.collection_id IN (' . $this->db->escape($filter_data['products_block_name']) . ')';
        }

        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '') {
            $sql .= " AND pd.`name` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' ";
        }

        if (!empty($filter_data['price_min'])) {
            $sql .= " AND CASE p.multi_versions WHEN 0 THEN (case when p.price > 0 then p.price >= '" . $filter_data['price_min'] . "' else p.compare_price >= '" . $filter_data['price_min'] . "' end) ELSE (SELECT IF(min(pvc.price) > 0, min(pvc.price), min(pvc.compare_price))  from " . DB_PREFIX . "product_version as pvc where p.product_id = pvc.product_id) >= '" . $filter_data['price_min'] . "' END";
            //$sql .= " AND compare_price_product >= '" . $filter_data['price_min'] . "'";
        }

        if (!empty($filter_data['price_max'])) {
            $sql .= " AND CASE p.multi_versions WHEN 0 THEN (case when p.price > 0 then p.price <= '" . $filter_data['price_max'] . "' else p.compare_price <= '" . $filter_data['price_max'] . "' end) ELSE (SELECT IF(min(pvc.price) > 0, min(pvc.price), min(pvc.compare_price)) from " . DB_PREFIX . "product_version as pvc where p.product_id = pvc.product_id) <= '" . $filter_data['price_max'] . "' END";
            //$sql .= " AND compare_price_product <= '" . $filter_data['price_max'] . "'";
        }

        $sql = $sql . " group by p.product_id";

        if (isset($filter_data['valueoptiton']) && $filter_data['valueoptiton']) {
            if ($filter_data['valueoptiton'] == 1) {
                $sql = $sql . " ORDER BY CONVERT(pd.name USING utf8) COLLATE utf8_vietnamese_ci ASC";
            }

            if ($filter_data['valueoptiton'] == 2) {
                $sql = $sql . " ORDER BY CONVERT(pd.name USING utf8) COLLATE utf8_vietnamese_ci DESC";
            }

            if ($filter_data['valueoptiton'] == 3) {
//                $sql = $sql . " ORDER BY case when p.multi_versions = 0 then case when p.price = 0 then p.compare_price else p.price end else (SELECT case when min(pvc.price) = 0 then min(pvc.compare_price) else min(pvc.price) end from " . DB_PREFIX . "product_version as pvc where p.product_id = pvc.product_id) end DESC";
                $sql = $sql . " ORDER BY case when p.multi_versions = 0 then case when p.price = 0 then p.compare_price else p.price end 
                                         else 
                                         (SELECT 
                                                case when min(if(pvc.price = 0, pvc.compare_price, pvc.price)) = 0 then
                                                    case when min(pvc.price) = 0 then min(pvc.compare_price) else min(pvc.price) end 
                                                else
                                                    max(if(pvc.price = 0, pvc.compare_price, pvc.price)) 
                                                end 
                                                from " . DB_PREFIX . "product_version as pvc where p.product_id = pvc.product_id) 
                                         end DESC,
                                         case when p.multi_versions = 0 then case when p.price = 0 then p.compare_price else p.price end 
                                         else 
                                         (SELECT 
                                                case when min(if(pvc.price = 0, pvc.compare_price, pvc.price)) = 0 then
                                                    case when min(pvc.price) = 0 then min(pvc.compare_price) else min(pvc.price) end 
                                                else
                                                    min(if(pvc.price = 0, pvc.compare_price, pvc.price)) 
                                                end 
                                                from " . DB_PREFIX . "product_version as pvc where p.product_id = pvc.product_id) 
                                         end DESC
                                         ";
            }

            if ($filter_data['valueoptiton'] == 4) {
//                $sql = $sql . " ORDER BY case when p.multi_versions = 0 then case when p.price = 0 then p.compare_price else p.price end else (SELECT case when min(pvc.price) = 0 then min(pvc.compare_price) else min(pvc.price) end from " . DB_PREFIX . "product_version as pvc where p.product_id = pvc.product_id) end ASC";
                $sql = $sql . " ORDER BY case when p.multi_versions = 0 then case when p.price = 0 then p.compare_price else p.price end 
                                         else 
                                         (SELECT 
                                                case when min(if(pvc.price = 0, pvc.compare_price, pvc.price)) = 0 then
                                                    case when min(pvc.price) = 0 then min(pvc.compare_price) else min(pvc.price) end 
                                                else
                                                    max(if(pvc.price = 0, pvc.compare_price, pvc.price)) 
                                                end 
                                                from " . DB_PREFIX . "product_version as pvc where p.product_id = pvc.product_id) 
                                         end ASC,
                                         case when p.multi_versions = 0 then case when p.price = 0 then p.compare_price else p.price end 
                                         else 
                                         (SELECT 
                                                case when min(if(pvc.price = 0, pvc.compare_price, pvc.price)) = 0 then
                                                    case when min(pvc.price) = 0 then min(pvc.compare_price) else min(pvc.price) end 
                                                else
                                                    min(if(pvc.price = 0, pvc.compare_price, pvc.price)) 
                                                end 
                                                from " . DB_PREFIX . "product_version as pvc where p.product_id = pvc.product_id) 
                                         end ASC
                                         ";
            }

        } elseif (isset($filter_data['search']) && !empty($filter_data['search'])) {
            // FULLTEXT search
            //$sql = $sql . " ORDER BY score DESC";

            // Order with full search more priority
            // TODO: advance with words combination. E.g: for searching "A B C" then order by:
            // A B C
            // A B
            // A C
            // B C
            // A
            // B
            // C
            $sql .= " ORDER BY CASE 
                                   WHEN pd.`name` LIKE '%" . $this->db->escape(trim($filter_data['search'])) . "%' THEN 1 
                                   ELSE 2 
                               END ";
        } else {
            $sql = $sql . " ORDER BY p.date_added DESC";
        }

        if (isset($filter_data['start']) || isset($filter_data['limit'])) {
            if ($filter_data['start'] < 0) {
                $filter_data['start'] = 0;
            }

            if ($filter_data['limit'] < 1) {
                $filter_data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
        }

        // get result from cache first
        $cache_key = DB_PREFIX . 'product:getProductsCustom_' . md5($sql);
        $products_list = $this->cache->get($cache_key);
        if ($products_list) {
            return $products_list;
        }

        // get result from db
        $query = $this->db->query($sql);
        $products_list = $query->rows;
        $this->cache->set($cache_key, $products_list);

        return $products_list;
    }


    /**
     * get total product in list with filter instead of function getTotalProductsCustom (faster old function)
     * TODO: make sure getTotalProductsCustomOptimize() return output same getTotalProductsCustom() with same input,
     * TODO: then replace full usage of getTotalProductsCustom(),
     * TODO: then may remove getTotalProductsCustom() function
     *
     * @param $filter_data
     * @return mixed
     */
    public function getTotalProductsCustomOptimize($filter_data)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM {$DB_PREFIX}product p 
                LEFT JOIN {$DB_PREFIX}product_description pd ON (pd.product_id = p.product_id) 
                LEFT JOIN (SELECT MIN(case when price > 0 then price else compare_price end) as min_price_version,
                            MAX(case when price > 0 then price else compare_price end) as max_price_version,
                            MAX(price) as price_version_check_null,
                            MIN(compare_price) as min_compare_price_version,
                            MAX(compare_price) as max_compare_price_version, MAX(status) as product_version_status_max,
                            MIN(case when price > 0 then ((price - compare_price)/compare_price*100) end) as max_percent_pv,
                            SUM(case when status = 1 then quantity else 0 end) as product_version_quantity,
                            deleted,
                            product_id
                            FROM {$DB_PREFIX}product_version
                            WHERE deleted IS null 
                            GROUP BY product_id) pv ON CASE WHEN p.multi_versions = 1 
                                                                 THEN p.product_id 
                                                                 ELSE -1 
                                                            END = pv.product_id ";

        if (isset($filter_data['filter_manufacturer_id'])) {
            $sql .= "LEFT JOIN {$DB_PREFIX}manufacturer m ON (m.manufacturer_id = p.manufacturer_id) ";
        }

        if (isset($filter_data['filter_category_id'])) {
            $sql .= "LEFT JOIN {$DB_PREFIX}product_to_category c ON (p.product_id = c.product_id) ";
        }

        if (isset($filter_data['filter_collection_id'])) {
            $sql .= "LEFT JOIN {$DB_PREFIX}product_collection pc ON (p.product_id = pc.product_id)";
            $sql .= "LEFT JOIN " . DB_PREFIX . "collection cl ON (cl.collection_id = pc.collection_id)";
        }

        if (isset($filter_data['attribute_value']) && !empty($filter_data['attribute_value'])) {
            $sql .= "LEFT JOIN " . DB_PREFIX . "product_attribute_filter paf ON (p.product_id = paf.product_id)";
        }

        $sql .= "LEFT JOIN {$DB_PREFIX}product_tag pt ON (pt.product_id = p.product_id) ";
        $sql .= "LEFT JOIN {$DB_PREFIX}tag t ON (t.tag_id = pt.tag_id) ";

        $sql .= "WHERE pd.language_id = " . (int)$this->config->get('config_language_id') . " and p.status = 1 AND p.`deleted` IS NULL ";

        if (isset($filter_data['channel'])) {
            $sql .= ' AND p.channel IN (' . $this->db->escape($filter_data['channel']) . ') ';
        } else {
            // default for web call
            $sql.= " AND p.channel IN (" . $this->db->escape(implode(',', self::$CHANNELS_FOR_WEB)) . ") ";
        }

        if (isset($filter_data['filter_nametag']) && !empty(isset($filter_data['filter_nametag']))) {
            $sql .= ' AND t.tag_id IN (' . $this->db->escape($filter_data['filter_nametag']) . ') ';
        }

        if (isset($filter_data['search']) && !empty($filter_data['search'])) {
            // FULLTEXT search
            //$sql .= " AND ( MATCH(pd.`name`) AGAINST('" . $this->db->escape(trim($filter_data['search'])) . "')";
            //$sql .= " OR t.`value` LIKE '%" . $this->db->escape(trim($filter_data['search'])) . "%' ) ";

            $n_searches = [];

            // add each word for searching
            $name_search = $filter_data['search'];
            $names = explode(' ', $name_search);
            foreach ($names as $ns) {
                $n_searches[] = "pd.`name` LIKE '%$ns%'";
            }

            $n_searches_sql = implode(' OR ', $n_searches);

            $sql .= " AND ( $n_searches_sql 
                            OR t.`value` LIKE '%" . $this->db->escape(trim($filter_data['search'])) . "%' 
                    ) ";
        }

        if (isset($filter_data['filter_manufacturer_id']) && !empty($filter_data['filter_manufacturer_id'])) {
            $sql .= ' AND m.manufacturer_id IN (' . $this->db->escape($filter_data['filter_manufacturer_id']) . ') ';
        }

        if (isset($filter_data['filter_category_id']) && !empty($filter_data['filter_category_id'])) {
            $sub_category_ids = $this->getAllCategoriesByParent($filter_data['filter_category_id']);
            $sql .= ' AND c.category_id IN (' . implode(',', $sub_category_ids) . ') ';
        }

        if (isset($filter_data['filter_collection_id']) && !empty($filter_data['filter_collection_id'])) {
            $sql .= ' AND pc.collection_id IN (' . $this->db->escape($filter_data['filter_collection_id']) . ') AND cl.`status` = 1 ';
        }

        if (isset($filter_data['attribute_value']) && !empty($filter_data['attribute_value'])) {
            $sql .= " AND paf.attribute_filter_value = '{$filter_data['attribute_value']}'";
        }

        if (isset($filter_data['filter_name']) && !empty($filter_data['filter_name'])) {
            $sql .= " AND pd.`name` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' ";
        }

        if (!empty($filter_data['price_min'])) {
            $sql .= " AND CASE p.multi_versions 
                        WHEN 0 
                        THEN (case when p.price > 0 then p.price >= '".$filter_data['price_min']."' else p.compare_price >= '".$filter_data['price_min']."' end) 
                        ELSE (case when pv.min_price_version > 0 then pv.min_price_version >= '".$filter_data['price_min']."' else pv.min_compare_price_version >= '".$filter_data['price_min']."' end) END";
        }

        if (!empty($filter_data['price_max'])) {
            $sql .= " AND CASE p.multi_versions 
                        WHEN 0 
                        THEN (case when p.price > 0 then p.price <= '".$filter_data['price_max']."' else p.compare_price <= '".$filter_data['price_max']."' end) 
                        ELSE (case when pv.max_price_version > 0 then pv.max_price_version <= '".$filter_data['price_max']."' else pv.max_compare_price_version <= '".$filter_data['price_max']."' end) END";
        }

        // get result from cache first
        $cache_key = DB_PREFIX . 'product:getTotalProductsCustomOptimize_' . md5($sql);
        $product_total = $this->cache->get($cache_key);
        if($product_total) {
            return $product_total;
        }

        // get result from db
        $query = $this->db->query($sql);
        $product_total = $query->row['total'];
        $this->cache->set($cache_key, $product_total);

        return $product_total;
    }

    public function getTotalProductsCustom($filter_data)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM {$DB_PREFIX}product p 
                LEFT JOIN {$DB_PREFIX}product_description pd ON (pd.product_id = p.product_id) 
                LEFT JOIN {$DB_PREFIX}product_version pv ON CASE WHEN p.multi_versions = 1 
                                                                 THEN p.product_id 
                                                                 ELSE -1 
                                                            END = pv.product_id ";

        if (isset($filter_data['filter_manufacturer_id'])) {
            $sql .= "LEFT JOIN {$DB_PREFIX}manufacturer m ON (m.manufacturer_id = p.manufacturer_id) ";
        }

        if (isset($filter_data['filter_category_id'])) {
            $sql .= "LEFT JOIN {$DB_PREFIX}product_to_category c ON (p.product_id = c.product_id) ";
        }

        if (isset($filter_data['filter_collection_id'])) {
            $sql .= "LEFT JOIN {$DB_PREFIX}product_collection pc ON (p.product_id = pc.product_id)";
            $sql .= "LEFT JOIN " . DB_PREFIX . "collection cl ON (cl.collection_id = pc.collection_id)";
        }

        $sql .= "LEFT JOIN {$DB_PREFIX}product_tag pt ON (pt.product_id = p.product_id) ";
        $sql .= "LEFT JOIN {$DB_PREFIX}tag t ON (t.tag_id = pt.tag_id) ";

        $sql .= "WHERE pd.language_id = " . (int)$this->config->get('config_language_id') . " and p.status = 1 AND p.`deleted` IS NULL ";

        if (isset($filter_data['channel'])) {
            $sql .= ' AND p.channel IN (' . $this->db->escape($filter_data['channel']) . ') ';
        } else {
            // default for web call
            $sql.= " AND p.channel IN (" . $this->db->escape(implode(',', self::$CHANNELS_FOR_WEB)) . ") ";
        }

        if (isset($filter_data['filter_nametag']) && !empty(isset($filter_data['filter_nametag']))) {
            $sql .= ' AND t.tag_id IN (' . $this->db->escape($filter_data['filter_nametag']) . ') ';
        }

        if (isset($filter_data['search']) && !empty($filter_data['search'])) {
            // FULLTEXT search
            //$sql .= " AND ( MATCH(pd.`name`) AGAINST('" . $this->db->escape(trim($filter_data['search'])) . "')";
            //$sql .= " OR t.`value` LIKE '%" . $this->db->escape(trim($filter_data['search'])) . "%' ) ";

            $n_searches = [];

            // add each word for searching
            $name_search = $filter_data['search'];
            $names = explode(' ', $name_search);
            foreach ($names as $ns) {
                $n_searches[] = "pd.`name` LIKE '%$ns%'";
            }

            $n_searches_sql = implode(' OR ', $n_searches);

            $sql .= " AND ( $n_searches_sql 
                            OR t.`value` LIKE '%" . $this->db->escape(trim($filter_data['search'])) . "%' 
                    ) ";
        }

        if (isset($filter_data['filter_manufacturer_id']) && !empty($filter_data['filter_manufacturer_id'])) {
            $sql .= ' AND m.manufacturer_id IN (' . $this->db->escape($filter_data['filter_manufacturer_id']) . ') ';
        }

        if (isset($filter_data['filter_category_id']) && !empty($filter_data['filter_category_id'])) {
            $sub_category_ids = $this->getAllCategoriesByParent($filter_data['filter_category_id']);
            $sql .= ' AND c.category_id IN (' . implode(',', $sub_category_ids) . ') ';
        }

        if (isset($filter_data['filter_collection_id']) && !empty($filter_data['filter_collection_id'])) {
            $sql .= ' AND pc.collection_id IN (' . $this->db->escape($filter_data['filter_collection_id']) . ') AND cl.`status` = 1 ';
        }

        if (isset($filter_data['filter_name']) && !empty($filter_data['filter_name'])) {
            $sql .= " AND pd.`name` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' ";
        }

        if (!empty($filter_data['price_min'])) {
            $sql .= " AND CASE p.multi_versions WHEN 0 
                                                THEN p.price >= '" . $filter_data['price_min'] . "' 
                                                ELSE (SELECT min(pvc.price) FROM {$DB_PREFIX}product_version as pvc 
                                                      WHERE p.product_id = pvc.product_id) >= '" . $filter_data['price_min'] . "' 
                                                END ";
        }

        if (!empty($filter_data['price_max'])) {
            $sql .= " AND CASE p.multi_versions WHEN 0 
                                                THEN p.price <= '" . $filter_data['price_max'] . "' 
                                                ELSE (SELECT max(pvc.price) FROM {$DB_PREFIX}product_version as pvc 
                                                      WHERE p.product_id = pvc.product_id) <= '" . $filter_data['price_max'] . "' 
                                                END ";
        }

        // get result from cache first
        $cache_key = DB_PREFIX . 'product:getTotalProductsCustom_' . md5($sql);
        $products_total = $this->cache->get($cache_key);
        if($products_total) {
            return $products_total;
        }

        // get result from db
        $query = $this->db->query($sql);
        $products_total = $query->row['total'];
        $this->cache->set($cache_key, $products_total);

        return $products_total;
    }

    /* error: loop 4ever? TODO: FIX... */
    private function getAllCategoriesByParent($category_id)
    {
        if (!$category_id) {
            return [];
        }

        $result = [$category_id];
        $query = $this->db->query("SELECT `category_id` FROM `" . DB_PREFIX . "category` 
                                   WHERE `parent_id` = " . (int)$category_id);
        foreach ($query->rows as $row) {
            // IMPORTANT: avoid infinitive loop if wrong data such as category has category_id = parent_id
            if ($row['category_id'] == $category_id) {
                continue;
            }

            $sub_ids = $this->getAllCategoriesByParent($row['category_id']);
            foreach ($sub_ids as $cate_id) {
                if (!in_array($cate_id, $result)) {
                    $result[] = (int)$cate_id;
                }
            }
        }

        return $result;
    }

    public function getSumProduct_version($pro_id)
    {
        $sql = " select sum(quantity) as total from " . DB_PREFIX . "product_version WHERE product_id = $pro_id";
        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function getProductSpecials($data = array())
    {
        $sql = "SELECT DISTINCT ps.product_id, 
                       (
                           SELECT AVG(rating) FROM " . DB_PREFIX . "review r1 
                           WHERE r1.product_id = ps.product_id 
                             AND r1.status = '1' 
                           GROUP BY r1.product_id) AS rating 
                FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id) 
                LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) 
                LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) 
                WHERE p.status = '1' 
                  AND p.date_available <= NOW() 
                  AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' 
                  AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' 
                  AND (
                          (ps.date_start = '0000-00-00' OR ps.date_start < NOW()) 
                      AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())
                  ) GROUP BY ps.product_id";

        $sort_data = array(
            'pd.name',
            'p.model',
            'ps.price',
            'rating',
            'p.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
                $sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
            } else {
                $sql .= " ORDER BY " . $data['sort'];
            }
        } else {
            $sql .= " ORDER BY p.sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC, LCASE(pd.name) DESC";
        } else {
            $sql .= " ASC, LCASE(pd.name) ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $product_data = array();
        $product_result = array();
        // get result from cache first
        $cache_key = DB_PREFIX . 'product:getProductSpecials' . md5($sql);
        $cache_products = $this->cache->get($cache_key);

        if ($cache_products) {
            $product_result = $cache_products;
        } else {
            // get result from db
            $query = $this->db->query($sql);
            $product_result = $query->rows;

            if ($query->num_rows) {
                $this->cache->set($cache_key, $product_result);
            }
        }

        // TODO: use query IN... read getLatestProduct() for example
        foreach ($product_result as $result) {
            $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
        }
        return $product_data;
    }

    public function getLatestProducts($limit, $return_product_detail = true)
    {
        // temp disable cache due to error: remove all products from BE but FE still got...
        // $product_data = $this->cache->get('product.latest.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int)$limit);
        // hardcode. TODO: remove and fix bug...
        $product_data = [];
        $timezone = ini_get('date.timezone') ? ini_get('date.timezone') : date_default_timezone_get();
        $now = new DateTime('now', new DateTimeZone($timezone));
        $start_time = $now->modify('- 30 day')->format('Y-m-d');
        $sql = "SELECT p.product_id FROM " . DB_PREFIX . "product p 
                    LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id AND p.default_store_id = p2s.store_id) 
                    WHERE p.status = '1' 
                      AND p.date_added <= NOW() 
                      AND p.date_added >= '" . $start_time ."' 
                      AND p.deleted is NULL 
                    ORDER BY p.date_added DESC LIMIT " . (int)$limit;

        $query = $this->db->query($sql);

        // TODO: remove...
        /*foreach ($query->rows as $result) {
            if ($return_product_detail) {
                $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
            } else {
                $product_data[$result['product_id']] = $result['product_id'];
            }

        }*/

        $new_products = $query->rows;
        if (empty($new_products)) {
            return [];
        }

        if ($return_product_detail) {
            $product_ids = array_map(function ($row) {
                return $row['product_id'];
            }, $new_products);

            $product_data = $this->getProducts([
                'filter_product_ids' => array_unique($product_ids)
            ]);
        } else {
            foreach ($new_products as $np) {
                $product_data[$np['product_id']] = $np['product_id'];
            }
        }

        return $product_data;
    }

    public function getPopularProducts($limit)
    {
        //TODO if had limit check again home
        $query = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product p 
                                    LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id AND p.default_store_id = p2s.store_id) 
                                    WHERE  p.status = '1' 
                                      AND p.date_available <= NOW() 
                                      AND p.deleted is NULL 
                                    ORDER BY p.viewed DESC, p.date_added DESC LIMIT " . (int)$limit);

        // TODO: use query IN... read getLatestProduct() for example
        foreach ($query->rows as $result) {
            $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
        }

        return $product_data;
    }

    public function getHotProducts($limit)
    {
        //TODO if had limit check again home
        $query = $this->db->query("SELECT DISTINCT p.product_id FROM " . DB_PREFIX . "product p 
                                    LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id AND p.default_store_id = p2s.store_id)
                                    LEFT JOIN (SELECT pv2.product_id, max(pv2.price > 0 AND pv2.price < pv2.compare_price) is_promotion_product_version 
                                                FROM " . DB_PREFIX . "product_version pv2 inner JOIN " . DB_PREFIX . "product p ON pv2.product_id = p.product_id 
                                                WHERE p.multi_versions = 1 GROUP BY pv2.product_id) pv ON p.product_id = pv.product_id 
                                    WHERE  p.status = '1' 
                                      AND p.date_available <= NOW() 
                                      AND p.deleted is NULL 
                                      AND (CASE WHEN p.multi_versions = 0 THEN p.price > 0 AND p.price < p.compare_price ELSE is_promotion_product_version END)
                                    ORDER BY p.date_added DESC LIMIT " . (int)$limit);

        if (empty($query->rows)) {
            return [];
        }

        $product_ids = array_map(function ($row) {
            return $row['product_id'];
        }, $query->rows);

        $product_data = $this->getProducts([
            'filter_product_ids' => array_unique($product_ids)
        ]);

        return $product_data;
    }

    /**
     * getBestSellerProducts
     *
     * TODO: support getting best sale product version...
     *
     * @param null|int $limit
     * @param bool $return_product_detail default true, true for return product detail, false for return product id only!
     * @return array|null
     */
    public function getBestSellerProducts($limit, $return_product_detail = true)
    {
        $product_data = [];

        //TODO if had limit check again home
        $sql = "SELECT op.product_id, SUM(op.quantity) AS total FROM " . DB_PREFIX . "order_product op 
                    LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id) 
                    LEFT JOIN `" . DB_PREFIX . "product` p ON (op.product_id = p.product_id) 
                    LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id AND p.default_store_id = p2s.store_id) 
                    WHERE o.order_status_id > '6' 
                      AND p.status = '1' 
                      AND p.date_available <= NOW() 
                      AND p.deleted is NULL 
                    GROUP BY op.product_id ORDER BY total DESC LIMIT " . (int)$limit;
        $query = $this->db->query($sql);

        // TODO: remove...
        /*foreach ($query->rows as $result) {
            if ($return_product_detail) {
                // DO NOT exe too many query like this. TODO: use one query with "IN" operator
                $product_data[$result['product_id']] = $this->getProduct($result['product_id']);

                // include total sold
                $product_data[$result['product_id']]['total'] = $result['total'];
            } else {
                $product_data[$result['product_id']] = $result['product_id'];
            }
        }*/

        $best_sale_products = $query->rows;
        if (empty($best_sale_products)) {
            return [];
        }

        if ($return_product_detail) {
            $product_ids = array_map(function ($row) {
                return $row['product_id'];
            }, $best_sale_products);

            $product_data = $this->getProducts([
                'filter_product_ids' => array_unique($product_ids)
            ]);

            // include total sold
            $product_id_to_total_sold = [];
            foreach ($best_sale_products as $bsp) {
                $product_id_to_total_sold[$bsp['product_id']] = $bsp['total'];
            }

            foreach ($product_data as &$product) {
                if (!array_key_exists('product_id', $product) ||
                    !array_key_exists($product['product_id'], $product_id_to_total_sold)
                ) {
                    continue;
                }

                $product['total'] = $product_id_to_total_sold[$product['product_id']];
            }
            unset($product);
        } else {
            foreach ($best_sale_products as $bsp) {
                $product_data[$bsp['product_id']] = $bsp['product_id'];
            }
        }

        return $product_data;
    }

    public function getProductAttributes($product_id)
    {
        $product_attribute_data = array();

        $product_attribute_query = $this->db->query("SELECT attribute_id FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' GROUP BY attribute_id");

        foreach ($product_attribute_query->rows as $product_attribute) {
            $product_attribute_description_data = '';

            $product_attribute_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
            $result = $product_attribute_description_query->row;
            if (count($result) > 0) {
                $product_attribute_description_data = $result;
            }
            $product_attribute_data[] = array(
                'attribute_id' => $product_attribute['attribute_id'],
                'product_attribute_description' => $product_attribute_description_data
            );
        }

        return $product_attribute_data;
    }

    public function getProductOptions($product_id)
    {
        $product_option_data = array();

        $product_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY o.sort_order");

        foreach ($product_option_query->rows as $product_option) {
            $product_option_value_data = array();

            $product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_id = '" . (int)$product_id . "' AND pov.product_option_id = '" . (int)$product_option['product_option_id'] . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY ov.sort_order");

            foreach ($product_option_value_query->rows as $product_option_value) {
                $product_option_value_data[] = array(
                    'product_option_value_id' => $product_option_value['product_option_value_id'],
                    'option_value_id' => $product_option_value['option_value_id'],
                    'name' => $product_option_value['name'],
                    'image' => $product_option_value['image'],
                    'quantity' => $product_option_value['quantity'],
                    'subtract' => $product_option_value['subtract'],
                    'price' => $product_option_value['price'],
                    'price_prefix' => $product_option_value['price_prefix'],
                    'weight' => $product_option_value['weight'],
                    'weight_prefix' => $product_option_value['weight_prefix']
                );
            }

            $product_option_data[] = array(
                'product_option_id' => $product_option['product_option_id'],
                'product_option_value' => $product_option_value_data,
                'option_id' => $product_option['option_id'],
                'name' => $product_option['name'],
                'type' => $product_option['type'],
                'value' => $product_option['value'],
                'required' => $product_option['required']
            );
        }

        return $product_option_data;
    }

    public function getProductDiscounts($product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND quantity > 1 AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity ASC, priority ASC, price ASC");

        return $query->rows;
    }

    public function getProductImages($product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getDataProductImages($product_id)
    {
        $query = $this->db->query("SELECT image, image_alt FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getProductRelated($product_id)
    {
        $product_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related pr LEFT JOIN " . DB_PREFIX . "product p ON (pr.related_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pr.product_id = '" . (int)$product_id . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

        // TODO: use query IN... read getLatestProduct() for example
        foreach ($query->rows as $result) {
            $product_data[$result['related_id']] = $this->getProduct($result['related_id']);
        }

        return $product_data;
    }

    public function getProductLayoutId($product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

        if ($query->num_rows) {
            return (int)$query->row['layout_id'];
        } else {
            return 0;
        }
    }

    public function getCategories($product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

        return $query->rows;
    }

    /**
     * @param int|array $product_id
     * @return array
     */
    public function getCategoriesCustom($product_id)
    {
        $is_mul_products = is_array($product_id);
        $product_ids = $is_mul_products ? $product_id : [(int)$product_id];
        if ($is_mul_products && empty($product_ids)) {
            return [];
        }

        $query = $this->db->query("SELECT * FROM (SELECT *, CONCAT('category_id=', category_id) as product_to_category_query FROM " . DB_PREFIX . "product_to_category ) as ptc 
                                   LEFT JOIN " . DB_PREFIX . "category as c ON c.category_id = ptc.category_id 
                                   LEFT JOIN " . DB_PREFIX . "category_description as cd ON cd.category_id = ptc.category_id 
                                   LEFT JOIN `" . DB_PREFIX . "seo_url` seo ON (ptc.product_to_category_query = seo.query) 
                                   WHERE ptc.product_id IN (" . implode(',', $product_ids) . ") 
                                     AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
                                     AND seo.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->rows;
    }

    public function getCategoriesCustomForPos($product_id)
    {
        $result = [];
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category as ptc 
                                   LEFT JOIN " . DB_PREFIX . "category_description as cd ON cd.category_id = ptc.category_id 
                                   WHERE product_id = '" . (int)$product_id . "' 
                                   AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        if ($query->num_rows <= 0){
            return $result;
        }

        foreach ($query->rows as $row) {
            $result[] = $this->getAllParentsCategory($row['category_id']);
        }


        return $result;
    }

    public function getCategoriesAndParents($product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category as ptc 
                                   LEFT JOIN " . DB_PREFIX . "category as c 
                                          ON c.category_id = ptc.category_id 
                                   LEFT JOIN " . DB_PREFIX . "category_description as cd 
                                          ON cd.category_id = ptc.category_id 
                                   WHERE product_id = '" . (int)$product_id . "' 
                                     AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        $list_categories = [];
        foreach ($query->rows as $row) {
            $parent_categories = $this->getAllParentsCategory($row['category_id']);
            foreach ($parent_categories as $k => $category){
                $list_categories[$k] = $category;
            }
        }

        $result = [];
        foreach ($list_categories as $category){
            $result[] = [
                'id' => $category['category_id'],
                'name' => $category['name'],
                'image' => $category['image'],
                'href' => $this->url->link('common/shop', 'category=' . $category['category_id'], true),
            ];
        }

        return $result;
    }

    public function getCategoriesAndParentsForBradcrumd($product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category as ptc 
                                   LEFT JOIN " . DB_PREFIX . "category as c 
                                          ON c.category_id = ptc.category_id 
                                   LEFT JOIN " . DB_PREFIX . "category_description as cd 
                                          ON cd.category_id = ptc.category_id 
                                   WHERE product_id = '" . (int)$product_id . "' 
                                     AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        $list_categories = [];

        if (!empty($query->row['category_id'])) {
            $parent_categories = $this->getAllParentsCategory($query->row['category_id']);
            foreach ($parent_categories as $k => $category){
                $list_categories[$k] = $category;
            }
        }


        $result = [];
        foreach ($list_categories as $category){
            $result[] = [
                'id' => $category['category_id'],
                'name' => $category['name'],
                'image' => $category['image'],
                'href' => $this->url->link('common/shop', 'path=' . $category['category_id'], true),
            ];
        }

        return $result;
    }

    private function getAllParentsCategory($category_id)
    {
        $result = [];
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category` as c 
                                   LEFT JOIN `" . DB_PREFIX . "category_description` as cd ON c.`category_id` = cd.`category_id` 
                                   WHERE c.`category_id` = " . (int)$category_id);

        if ($query->num_rows <= 0) {
            return $result;
        }

        $result[$query->row['category_id']] = $query->row;
        if ($query->row['parent_id'] != 0 &&
            // IMPORTANT: avoid infinitive loop if wrong data such as category has category_id = parent_id
            $query->row['parent_id'] != $category_id
        ) {
            $parents = $this->getAllParentsCategory($query->row['parent_id']);
            foreach ($parents as $k => $v) {
                $result[$k] = $v;
            }
        }

        return $result;
    }


    public function getCategoryIdsByProductId($product_id)
    {
        $query = $this->db->query("SELECT pct.category_id FROM " . DB_PREFIX . "product_to_category as pct 
                                   INNER JOIN " . DB_PREFIX . "category_description as cd ON pct.category_id = cd.category_id 
                                   WHERE pct.product_id = '" . (int)$product_id . "' 
                                   AND cd.language_id='" . (int)$this->config->get('config_language_id') . "'");

        return $query->rows;
    }

    /**
     * get StoreIds By ProductId
     * TODO: correct function name as "getProductToStoresByProductId", support also get by version id, remove duplicate result!...
     *
     * @param $product_id
     * @return mixed
     */
    public function getStoreIdsByProductId($product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store as pts 
                                   WHERE product_id = '" . (int)$product_id . "'");

        return $query->rows;
    }

    public function getTotalProducts($data = array())
    {
        $sql = "SELECT COUNT(DISTINCT p.product_id) AS total";

        if (!empty($data['filter_category_id'])) {
            if (!empty($data['filter_sub_category'])) {
                $sql .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";
            } else {
                $sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
            }

            if (!empty($data['filter_filter'])) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
            } else {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
            }
        } else {
            $sql .= " FROM " . DB_PREFIX . "product p";
        }

        $sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

        if (!empty($data['filter_category_id'])) {
            if (!empty($data['filter_category_ids'])) {
                $categories = is_array($data['filter_category_ids']) ? $data['filter_category_ids'] : [$data['filter_category_ids']];
                $categories = implode(",", $categories);
                $sql .= " AND p2c.category_id IN (" . $categories . ")";
            } elseif (!empty($data['filter_sub_category'])) {
                $sql .= " AND cp.path_id = '" . (int)$data['filter_category_id'] . "'";
            } else {
                $sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
            }

            if (!empty($data['filter_filter'])) {
                $implode = array();

                $filters = explode(',', $data['filter_filter']);

                foreach ($filters as $filter_id) {
                    $implode[] = (int)$filter_id;
                }

                $sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
            }
        }

        if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
            $sql .= " AND (";

            if (!empty($data['filter_name'])) {
                $implode = array();

                $words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_name'])));

                foreach ($words as $word) {
                    $implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
                }

                if ($implode) {
                    $sql .= " " . implode(" AND ", $implode) . "";
                }

                if (!empty($data['filter_description'])) {
                    $sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
                }
            }

            if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
                $sql .= " OR ";
            }

            if (!empty($data['filter_tag'])) {
                $implode = array();

                $words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_tag'])));

                foreach ($words as $word) {
                    $implode[] = "pd.tag LIKE '%" . $this->db->escape($word) . "%'";
                }

                if ($implode) {
                    $sql .= " " . implode(" AND ", $implode) . "";
                }
            }

            if (!empty($data['filter_name'])) {
                $sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
            }

            $sql .= ")";
        }

        if (!empty($data['filter_manufacturer_id'])) {
            $sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer_id'] . "'";
        } else if (isset($data['filter_manufacturer_id']) && $data['filter_manufacturer_id'] === '0') {
            $sql .= " AND p.manufacturer_id = '0'";
        }

        if (!empty($data['price_min'])) {
            $sql .= " AND p.price >= '" . $data['price_min'] . "'";
        }

        if (!empty($data['price_max'])) {
            $sql .= " AND p.price <= '" . $data['price_max'] . "'";
        }

        // get result from cache first
        $cache_key = DB_PREFIX . 'product:getTotalProducts_' . md5($sql);
        $product_total = $this->cache->get($cache_key);
        if ($product_total) {
            return $product_total;
        }

        // get result from db
        $query = $this->db->query($sql);
        $product_total = $query->row['total'];
        $this->cache->set($cache_key, $product_total);

        return $product_total;
    }

    public function getProfile($product_id, $recurring_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recurring r JOIN " . DB_PREFIX . "product_recurring pr ON (pr.recurring_id = r.recurring_id AND pr.product_id = '" . (int)$product_id . "') WHERE pr.recurring_id = '" . (int)$recurring_id . "' AND status = '1' AND pr.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'");

        return $query->row;
    }

    public function getProfiles($product_id)
    {
        $query = $this->db->query("SELECT rd.* FROM " . DB_PREFIX . "product_recurring pr JOIN " . DB_PREFIX . "recurring_description rd ON (rd.language_id = " . (int)$this->config->get('config_language_id') . " AND rd.recurring_id = pr.recurring_id) JOIN " . DB_PREFIX . "recurring r ON r.recurring_id = rd.recurring_id WHERE pr.product_id = " . (int)$product_id . " AND status = '1' AND pr.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getTotalProductSpecials()
    {
        $query = $this->db->query("SELECT COUNT(DISTINCT ps.product_id) AS total FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW()))");

        if (isset($query->row['total'])) {
            return $query->row['total'];
        } else {
            return 0;
        }
    }

    public function getProductByIds($ids)
    {
        $ids = is_array($ids) ? $ids : [$ids];
        $ids = implode(",", $ids);
        $query = $this->db->query("SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT points FROM " . DB_PREFIX . "product_reward pr WHERE pr.product_id = p.product_id AND pr.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "') AS reward, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int)$this->config->get('config_language_id') . "') AS stock_status, (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS weight_class, (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS length_class, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews, p.sort_order FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id IN (" . $ids . ") AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");
        if ($query->num_rows) {
            return $query->rows;
        } else {
            return false;
        }
    }

    public function getManufacturer()
    {
        $query = $this->db->query("SELECT p.manufacturer_id as manufacturer_id, m.name, COUNT(p.product_id) as count 
                                    FROM " . DB_PREFIX . "product p 
                                    LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) 
                                    LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) 
                                    LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) 
                                    WHERE p.status = 1 
                                      AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
                                      AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' 
                                    GROUP BY p.manufacturer_id 
                                    ORDER BY m.sort_order ASC, m.name ASC");

        $result = $query->rows;
        foreach ($result as $key => $value) {
            if ($value['name'] == null) {
                $value['name'] = 'Other';
                $result['Other'] = $value;
                unset($result[$key]);
            }
        }

        return $result;
    }

    public function countProductPriceInRange($range, $type)
    {
        if ($type == 'range') {
            $query = $this->db->query("SELECT COUNT(p.product_id) as count FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = 1 AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND p.price >= " . $range['from'] . " AND p.price <= " . $range['to']);
        } else if ($type == 'max') {
            $query = $this->db->query("SELECT COUNT(p.product_id) as count FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = 1 AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND p.price <= " . $range['to']);
        } else {
            $query = $this->db->query("SELECT COUNT(p.product_id) as count FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = 1 AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND p.price >= " . $range['from']);
        }

        $result = $query->rows;

        if (isset($result[0]['count'])) {
            return $result[0]['count'];
        } else {
            return false;
        }
    }

    public function countProductByCategoryIds($categories = array())
    {
        $categories = is_array($categories) ? $categories : [$categories];
        $categories = implode(",", $categories);
        $query = $this->db->query("SELECT COUNT(DISTINCT (p.product_id)) as count FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category ptc ON (p.product_id = ptc.product_id) WHERE p.status = 1 AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND ptc.category_id IN (" . $categories . ")");

        $result = $query->rows;

        if (isset($result[0]['count'])) {
            return $result[0]['count'];
        } else {
            return false;
        }
    }

    public function getProductAttributeCustom($product_id)
    {
        $sql = "SELECT * from " . DB_PREFIX . "product_attribute as pa LEFT JOIN " . DB_PREFIX . "attribute_description as ad ON pa.attribute_id = ad.attribute_id WHERE pa.product_id ='" . (int)$product_id . "' ";
        $sql .= " AND pa.language_id = '" . (int)$this->config->get('config_language_id') . "'";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getCollectionList($collection_id) {
        $collection_list = [];
        $sql = "SELECT * FROM " . DB_PREFIX . "collection WHERE collection_id = '". (int)$collection_id ."'";
        $query = $this->db->query($sql);
        $result = $query->row;

        $collection_list[] = $collection_id;
        if (isset($result['type']) && $result['type'] == 1) {
            $sql = "SELECT collection_id FROM " . DB_PREFIX . "collection_to_collection WHERE parent_collection_id = '". (int)$collection_id ."'";
            $query = $this->db->query($sql);
            foreach ($query->rows as $result) {
                $collection_list[] = $result['collection_id'];
            }
        }
        return implode(',', $collection_list);
    }

    public function getProductsByColection($filter, $is_return_product_detail = true)
    {
        $collection_list = [];

        $DB_PREFIX = DB_PREFIX;

        /* get collection by collection_id */
        $sql = "SELECT * FROM {$DB_PREFIX}collection 
                WHERE collection_id = '" . (int)$filter['filter_collection_id'] . "' 
                AND `status` = 1";
        $query = $this->db->query($sql);
        $result = $query->row;

        /* return no result if not existed */
        if (empty($result)) {
            return [];
        }

        if (isset($filter['filter_collection_id']) && !empty($filter['filter_collection_id'])) {
            $collection_list[] = $filter['filter_collection_id'];
        }

        /* trying get all sub-collections */
        if (isset($result['type']) && $result['type'] == 1) {
            $sql = "SELECT collection_id FROM {$DB_PREFIX}collection_to_collection 
                    WHERE parent_collection_id = '" . (int)$filter['filter_collection_id'] . "'";
            $query = $this->db->query($sql);
            foreach ($query->rows as $result) {
                $collection_list[] = $result['collection_id'];
            }
        }

        /* get all product ids belong to collection and its sub-collections */
        $sql = "SELECT DISTINCT p.product_id FROM " . DB_PREFIX . "product_collection AS pc 
                LEFT JOIN " . DB_PREFIX . "product AS p ON p.product_id = pc.product_id 
                WHERE 1 
                AND pc.collection_id IN (" . implode(',', $collection_list) . ") 
                AND p.status = 1 AND p.deleted is NULL ";

        if (isset($filter['filter_product_id'])) {
            $sql .= " AND p.product_id != " . (int)$filter['filter_product_id'];
        }

        $sql .= " ORDER BY p.date_modified DESC ";

        if (isset($filter['limit'])) {
            $sql .= " LIMIT " . (int)$filter['limit'];
        }

        $query = $this->db->query($sql);

        $product_data = [];

        if (empty($query->rows)) {
            return [];
        }

        /* get all product detail if need */
        if ($is_return_product_detail) {
            $product_ids = array_map(function ($row) {
                return $row['product_id'];
            }, $query->rows);

            $product_data = $this->getProducts([
                'filter_product_ids' => $product_ids,
                'sort' => 'input_product_ids',
            ]);
        } else {
            foreach ($query->rows as $row) {
                $product_data[$row['product_id']] = $row['product_id'];
            }
        }

        return $product_data;
    }

    public function getProductsByIdsSameCategory($filter)
    {
        $sql = "SELECT ptc.product_id FROM " . DB_PREFIX . "product_to_category AS ptc 
                LEFT JOIN " . DB_PREFIX . "product AS p ON p.product_id = ptc.product_id 
                WHERE ptc.category_id IN (SELECT category_id 
                                          FROM " . DB_PREFIX . "product_to_category AS ptc 
                                          WHERE ptc.product_id = '" . (int)$filter['filter_product_id'] . "'
                                         ) 
                  AND p.status = 1 
                  AND ptc.product_id != '" . (int)$filter['filter_product_id'] . "' ";

        if (isset($filter['start']) && isset($filter['limit'])) {
            $sql .= " LIMIT " . (int)$filter['start'] . "," . (int)$filter['limit'];
        }

        $query = $this->db->query($sql);

        $product_data = [];
        // TODO: use query IN... read getLatestProduct() for example
        foreach ($query->rows as $result) {
            $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
        }

        return $product_data;
    }

    // correct function name from getProductVersionId to getProductByVersionValue
    // TODO: remove below line...
    // public function getProductVersionId($product_id, $version)
    /**
     * get product by version value (multi version values)
     * @param int $product_id
     * @param array $version
     * @param bool $include_deleted default false for ONLY active version
     * @param int $product_version_id
     * @return mixed
     */
    public function getProductByVersionValue($product_id, $version, $include_deleted = false)
    {
        $DB_PREFIX = DB_PREFIX;

        // get product_version_id
        $sql = "SELECT product_version_id 
                    FROM {$DB_PREFIX}product_version 
                        WHERE product_id = '" . (int)$product_id . "' 
                            AND version ='" . $this->db->escape(implode(',', $version)) . "' 
                            AND " . ($include_deleted ? '1=1' : 'deleted IS NULL');

        $query = $this->db->query($sql);

        if (!empty($query->row) && !empty($query->row['product_version_id'])) {
            return $this->getProductVersionById($product_id, $query->row['product_version_id']);
        }

        return [];
    }

    public function getProductVersionById($product_id, $product_version_id = 0)
    {
        $DB_PREFIX = DB_PREFIX;

        // get product_version_id
        $sql_get_pv = "SELECT pv.product_version_id, 
                              pv.product_id, 
                              pv.version, 
                              pv.compare_price, 
                              pv.sku, 
                              pv.barcode, 
                              pv.quantity, 
                              pv.status, 
                              pv.deleted, 
                              pv.image, 
                              pv.image_alt, 
                              dp.discount_id, 
                              IFNULL( dp.price_discount, 0 ) as price 
                FROM {$DB_PREFIX}product_version as pv 
                LEFT JOIN (SELECT dp_1.product_id, dp_1.product_version_id, 
                                  dp_1.price_discount, 
                                  dp_1.discount_id 
                            FROM {$DB_PREFIX}discount_product AS dp_1 
                                JOIN {$DB_PREFIX}discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                WHERE d_1.date_started <= NOW() 
                                    AND d_1.date_ended >= NOW() 
                                    AND d_1.status = 1 
                                    AND dp_1.status = 1 
                                    AND dp_1.product_id = '" . (int)$product_id . "' 
                                    AND dp_1.product_version_id = '" . (int)$product_version_id . "' 
                                    ORDER BY dp_1.price_discount ASC LIMIT 1 ) AS dp 
                ON (pv.product_id = dp.product_id AND pv.product_version_id = dp.product_version_id) 
                WHERE pv.product_id = '" . (int)$product_id . "' 
                  AND pv.product_version_id = '" . (int)$product_version_id . "' 
                  AND pv.deleted IS NULL";

        // select product quantity in stock
        // could not combine into one sql, this will return duplicate result and not exactly for quantity in stock!
        $sql = "SELECT found_pv.*, pts.quantity as quantity_in_stock, 
                  (CASE WHEN found_pv.product_version_id IS NULL THEN pts.quantity ELSE pts.quantity END) AS quantity_in_stock  
                FROM ({$sql_get_pv}) as found_pv 
                LEFT JOIN {$DB_PREFIX}product p ON p.product_id = found_pv.product_id
                LEFT JOIN {$DB_PREFIX}product_to_store pts ON (
                   pts.product_id = found_pv.product_id
                   AND pts.product_version_id = found_pv.product_version_id 
                   AND pts.store_id = p.default_store_id
                ) 
                WHERE 1=1";
        $query = $this->db->query($sql);

        $product = $query->row;
        // override quantity
        $product['quantity'] = isset($product['quantity_in_stock']) ? (int)$product['quantity_in_stock'] : 0;
        unset($product['quantity_in_stock']);

        return $product;
    }

    public function getProductByVersionId($product_id, $version_id)
    {
        // TODO: need check deleted IS NULL?...

        $sql = "SELECT * from " . DB_PREFIX . "product_version as pv 
                WHERE pv.product_id = '" . (int)$product_id . "' 
                  AND pv.product_version_id ='" . (int)$version_id . "'";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getSaleOnOutOfStockProduct($product_id)
    {
        $sql = "SELECT sale_on_out_of_stock from " . DB_PREFIX . "product as p WHERE p.product_id ='" . (int)$product_id . "'";
        $query = $this->db->query($sql);
        return $query->row['sale_on_out_of_stock'];
    }

    public function getSeoProduct($product_id)
    {
        $sql = "SELECT seo_title,seo_description from " . DB_PREFIX . "product_description  WHERE product_id ='" . (int)$product_id . "' and  language_id = " . (int)$this->config->get('config_language_id') . "";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getProductName($product_id)
    {
        $sql = "SELECT name from " . DB_PREFIX . "product_description  WHERE product_id =" . (int)$product_id . " and  language_id = " . (int)$this->config->get('config_language_id') . "";
        $query = $this->db->query($sql);
        return $query->row['name'];
    }

    public function getProductUserCreateId($product_id)
    {
        $sql = "SELECT user_create_id FROM " . DB_PREFIX . "product  WHERE product_id =" . (int)$product_id;
        $query = $this->db->query($sql);
        return isset($query->row['user_create_id']) ? $query->row['user_create_id'] : 1; // 1 is user_id of admin
    }

    public function getProductNews($limit = null, $return_product_detail = false)
    {
        $this->load->model('extension/module/theme_builder_config');
        $new_product_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_NEW_PRODUCT);
        $new_product_config = json_decode($new_product_config, true);

        $limit = empty($limit) || (int)$limit < 0 ? 1e4 : (int)$limit;

        if ($new_product_config['setting']['auto_retrieve_data'] == 0) {
            $filter['filter_collection_id'] = $new_product_config['setting']['collection_id'];
            $new_products = $this->getProductsByColection($filter, $return_product_detail);
        } else {
            $new_products = $this->getLatestProducts($limit, $return_product_detail);
        }

        $new_products = is_array($new_products) ? $new_products : [];

        return $new_products;
    }

    public function getProductBestSales($limit = null, $return_product_detail = false)
    {
        $this->load->model('extension/module/theme_builder_config');
        $best_sales_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_BEST_SALES_PRODUCT);
        $best_sales_config = json_decode($best_sales_config, true);

        $limit = empty($limit) || (int)$limit < 0 ? 1e4 : (int)$limit;

        if ($best_sales_config['setting']['auto_retrieve_data'] == 0) {
            $filter['filter_collection_id'] = $best_sales_config['setting']['collection_id'];
            $filter['limit'] = $limit;
            $best_sales_product = $this->getProductsByColection($filter, $return_product_detail);
        } else {
            $best_sales_product = $this->getBestSellerProducts($limit, $return_product_detail);
        }

        $best_sales_product = is_array($best_sales_product) ? $best_sales_product : [];

        return $best_sales_product;
    }

    public function getBaseImageAlt($product_id)
    {
        $sql = "SELECT image_alt from " . DB_PREFIX . "product  WHERE product_id =" . (int)$product_id;
        $query = $this->db->query($sql);
        return $query->row['image_alt'];
    }

    public function getCollectionByProductId($product_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT collection_id as id from {$DB_PREFIX}product_collection as pc 
                WHERE pc.product_id ='" . (int)$product_id . "'";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @param array $data
     * @return array
     */
    public function getAPIProducts($data = array())
    {
        $sql = $this->getAllWarehousesSql($data);

        /* pagination */
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = $this->config->get('config_limit_admin');
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getAPIProducts_' . md5($sql);
        $products = $this->cache->get($cache_key);
        if ($products) {
            return $products;
        }

        // get result from db
        $query = $this->db->query($sql);
        $products = $query->rows;
        $this->cache->set($cache_key, $products);

        return $products;
    }

    /**
     * @param array $data
     * @return int
     */
    public function getAPIProductsTotal($data = [])
    {
        $sqlAll = $this->getAllWarehousesSql($data);
        $sql = "SELECT COUNT(t.`product_id`) AS `total` FROM (" . $sqlAll . ") as t";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getAPIProducts_' . md5($sql);
        $product_total = $this->cache->get($cache_key);
        if ($product_total) {
            return $product_total;
        }

        // get result from db
        $query = $this->db->query($sql);
        $product_total = $query->row['total'];
        $this->cache->set($cache_key, $product_total);

        return $product_total;
    }

    /**
     * @param array $data
     * TODO: check usage and rename...
     * @param bool $for_default_store_only
     * @param bool $include_deleted
     * @return string
     */
    public function getAllWarehousesSql($data = [], $for_default_store_only = false, $include_deleted = false)
    {
        $DB_PREFIX = DB_PREFIX;

        /* sub-sql */
        $sql = "SELECT p.`product_id`, 
                    (CASE WHEN p.multi_versions = 0 THEN p.sku ELSE pv.sku END) as sku, 
                    p.`common_barcode`, 
                    p.`common_sku`, 
                    p.`common_compare_price`, 
                    p.`common_price`, 
                    (CASE WHEN p.multi_versions = 0 THEN p.barcode ELSE pv.barcode END) as barcode, 
                    /* quantity query later, not work in this sql, why? */
                    p.`sale_on_out_of_stock`, 
                    p.`stock_status_id`, 
                    p.`image`, 
                    p.`image_alt`, 
                    p.`multi_versions`, 
                    p.`manufacturer_id`, 
                    (CASE WHEN p.multi_versions = 0 
                          THEN IF(p.price=0, p.compare_price, p.price) 
                          ELSE IF(pv.price=0, pv.compare_price, pv.price) END) AS price, 
                    p.`price` as price_master, 
                    (CASE WHEN p.multi_versions = 0 
                          THEN IF(p.compare_price=0, p.price, p.compare_price) 
                          ELSE IF(pv.compare_price=0, pv.price, pv.compare_price) END) AS compare_price, 
                    p.`compare_price` as compare_price_master, 
                    p.`weight`, 
                    p.`status`, 
                    p.`channel`, 
                    p.`demo`, 
                    p.`deleted`, 
                    p.`default_store_id`, 
                    p.`date_added`, 
                    p.`date_modified`, 
                    pd.`language_id`, 
                    pd.`name`, 
                    pd.`description`, 
                    pd.`sub_description`,
                    (CASE WHEN p.multi_versions = 0 THEN NULL ELSE pv.version END) AS name_version,  
                    p.product_id as product_id_master, 
                    m.name AS manufacturer, 
                    MIN(case when pv.price > 0 then pv.price else pv.compare_price end) as min_price_version, 
                    MAX(case when pv.price > 0 then pv.price else pv.compare_price end) as max_price_version, 
                    MAX(pv.price) as price_version_check_null, 
                    MIN(pv.compare_price) as min_compare_price_version, 
                    MAX(pv.compare_price) as max_compare_price_version, MAX(pv.status) as product_version_status_max ,
                    MIN(case when pv.price > 0 then ((pv.price - pv.compare_price)/pv.compare_price*100) end) as max_percent_pv, 
                    (CASE WHEN p.multi_versions = 0 
                          THEN IF(p.price=0, p.compare_price, p.price) 
                          ELSE IF(pv.price=0, pv.compare_price, pv.price) END) AS price_version, 
                    (CASE WHEN p.multi_versions = 0 
                          THEN IF(p.compare_price=0, p.price, p.compare_price) 
                          ELSE IF(pv.compare_price=0, pv.price, pv.compare_price) END) AS compare_price_version, 
                    IF(p.multi_versions = 0, 0, pv.product_version_id) AS product_version_id 
                FROM  {$DB_PREFIX}product p 
                LEFT JOIN {$DB_PREFIX}product_description pd ON (pd.product_id = p.product_id) 
                LEFT JOIN {$DB_PREFIX}product_version pv ON (pv.product_id = p.product_id) 
                LEFT JOIN {$DB_PREFIX}manufacturer m ON (m.manufacturer_id = p.manufacturer_id) ";

        if ($for_default_store_only) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_to_store pts ON (pts.product_id = p.product_id AND pts.store_id = p.default_store_id)"; // for default store only
        } else {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_to_store pts ON (pts.product_id = p.product_id)";
        }

        if (isset($data['filter_category']) && $data['filter_category'] != 0) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_to_category c ON (p.product_id = c.product_id)";
        }

        if (isset($data['filter_collection']) && $data['filter_collection'] != 0) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_collection pc ON (p.product_id = pc.product_id)";
        }

        if ((isset($data['filter_tag']) && $data['filter_tag'] != '') ||
            (isset($data['filter_name']) && $data['filter_name'] !== '')
        ) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_tag pt ON (pt.product_id = p.product_id) ";
            $sql .= " LEFT JOIN {$DB_PREFIX}tag t ON (t.tag_id = pt.tag_id) ";
        }

        $sql .= " WHERE 1=1 
                  AND p.`channel` IN (1, 2) ";

        $is_all_statuses = isset($data['filter_is_all_statuses']) ? $data['filter_is_all_statuses'] : false;

        // exclude deleted if set
        if (!$is_all_statuses && !$include_deleted) {
            $sql .= " AND p.`deleted` IS NULL ";
            $sql .= " AND (CASE WHEN p.multi_versions = 0 THEN 1=1 ELSE pv.`deleted` IS NULL END)";
        }

        /* except original product if multi versions */
        //$sql .= " AND (CASE WHEN p.multi_versions = 0 THEN pv.product_id IS NULL ELSE pv.product_version_id IS NOT NULL END)";

        if (!$is_all_statuses) {
            $sql .= " AND p.status = 1 AND (CASE WHEN p.multi_versions = 0 THEN 1=1 ELSE pv.status = 1 END)";
        }

        if (!$is_all_statuses) {
            $sql .= " AND (CASE WHEN p.multi_versions = 0 AND p.sale_on_out_of_stock = 0 THEN pts.quantity > 0 ELSE 1=1 END)";
            $sql .= " AND (CASE WHEN p.multi_versions = 1 AND p.sale_on_out_of_stock = 0 THEN pts.quantity > 0 ELSE 1=1 END)";
        }

        $sql .= " AND pd.language_id = " . (int)$this->config->get('config_language_id') . "";

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $sql .= ' AND p.status=' . $data['filter_status'];
        }

        if (isset($data['filter_manufacturer']) && $data['filter_manufacturer'] != 0) {
            $sql .= ' AND m.manufacturer_id IN (' . $data['filter_manufacturer'] . ')';
        }

        if (isset($data['filter_category']) && $data['filter_category'] != 0) {
            $value = implode(", ", array_map('intval', $data['filter_category']));
            $sql .= " AND c.category_id IN(" . $value . ") ";
        }

        if (isset($data['filter_tag']) && $data['filter_tag'] != '') {
            $sql .= " AND t.`value` RLIKE '" . $data['filter_tag'] . "'";
        }

        if (isset($data['filter_collection']) && $data['filter_collection'] != 0) {
            $sql .= ' AND pc.collection_id IN (' . $data['filter_collection'] . ')';
        }

        if (isset($data['filter_name']) && $data['filter_name'] !== '') {
            $sql .= " AND (pd.`name` LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%' ";
            $sql .= " OR t.`value` LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%' ) ";
        }

        /*
         * format: 12,13_1, 13_3:
         * - product: 12 (single version or all versions if has)
         * - product: 13 + version: 1, 3
         */
        if (isset($data['filter_except_product_ids']) && $data['filter_except_product_ids']) {
            $except_product_ids = explode(',', $data['filter_except_product_ids']);
            $except_product_ids_query = [];
            $except_product_version_ids_query = [];
            foreach ($except_product_ids as $except_pid) {
                if (!$except_pid) {
                    continue;
                }

                // support version id
                if (strpos($except_pid, '_') > 0) {
                    $p_and_ver = explode('_', $except_pid);
                    if (!is_array($p_and_ver) || count($p_and_ver) < 2) {
                        continue;
                    }

                    // add version only, DO NOT add product id!
                    $except_product_version_ids_query[] = $p_and_ver[1];
                } else {
                    $except_product_ids_query[] = $except_pid;
                }
            }
            $except_product_ids_query = implode(',', $except_product_ids_query);
            $except_product_version_ids_query = implode(',', $except_product_version_ids_query);

            if (!empty($except_product_ids_query)) {
                $sql .= " AND p.product_id NOT IN ({$except_product_ids_query})";
            }

            if (!empty($except_product_version_ids_query)) {
                $sql .= " AND (CASE WHEN pv.product_version_id IS NOT NULL AND pv.product_version_id > 0 THEN pv.product_version_id NOT IN ({$except_product_version_ids_query}) ELSE 1=1 END)";
            }
        }

        if (isset($data['filter_product_id']) && !empty($data['filter_product_id'])) {
            $sql .= " AND p.product_id = '" . (int)$data['filter_product_id'] . "'";
        }

        if (isset($data['filter_product_version_id']) && !empty($data['filter_product_version_id'])) {
            $sql .= " AND pv.product_version_id = '" . (int)$data['filter_product_version_id'] . "'";
        }

        if (isset($data['filter_amount']) && $data['filter_amount']) {
            $sql .= " AND ( ";
            foreach ($data['filter_amount'] as $amounts) {
                $amount = explode('_', $amounts);
                $quantity = isset($amount[1]) ? $amount[1] : 0;

                if ($amount[0] == 2) {
                    $sql .= " pts.quantity <'" . (int)$quantity . "'";
                }
            }
            $sql .= " ) ";
        }

        if (isset($data['filter_store']) && $data['filter_store'] != null) {
            $sql .= " AND pts.store_id = " .$data['filter_store']. " ";
        }

        $sql .= " GROUP BY p.product_id, pv.product_version_id ";

        /* final sql for correcting quantity */
        $final_sql = "SELECT DISTINCT p2.*, 
                             IF(p2.multi_versions = 1 AND p2.product_version_id = pts2.product_version_id, pts2.quantity, 0) AS product_version_quantity, 
                             IF(p2.multi_versions = 0 AND pts2.product_version_id = 0, pts2.quantity, 0) AS product_master_quantity 
                      FROM ($sql) AS p2 
                      LEFT JOIN {$DB_PREFIX}product_to_store pts2 ON (pts2.product_id = p2.product_id AND pts2.product_version_id = p2.product_version_id) ";

        $final_sql .= " GROUP BY p2.product_id, p2.product_version_id ";

        $sort_data = [];
        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $final_sql .= " ORDER BY " . $data['sort'];
        } else {
            $final_sql .= " ORDER BY p2.date_modified";
        }

        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $final_sql .= " ASC";
        } else {
            $final_sql .= " DESC";
        }

        return $final_sql;
    }

    public function getManufactureIdByProductId($product_id)
    {
        if (!$product_id){
            return false;
        }

        $sql = "SELECT `manufacturer_id` FROM `" . DB_PREFIX . "product` WHERE `product_id` =" . (int)$product_id;

        $query = $this->db->query($sql);
        if ($query->num_rows){
            return (int)$query->row['manufacturer_id'];
        }else{
            return false;
        }
    }

    public function getProductVersionsFullName($product_id, $product_version_ids = [])
    {
        $product_name = '';
        $query = $this->db->query("SELECT `name` FROM `" . DB_PREFIX . "product_description` WHERE `product_id` =" . (int)$product_id);
        if ($query->num_rows > 0) {
            $product_name = isset($query->row['name']) ? $query->row['name'] : '';
        }

        if ($product_version_ids && $product_name){
            foreach ($product_version_ids as $version_id){
                if ($version_id){
                    $query = $this->db->query("SELECT `version` as name FROM `" . DB_PREFIX . "product_version` WHERE `product_version_id` = " . (int)$version_id . " AND `product_id` = ". (int)$product_id);
                    if ($query->num_rows > 0) {
                        $version_name = isset($query->row['name']) ? $query->row['name'] : '';
                        if ($version_name){
                            $product_name .= '(' .$version_name .')';
                        }
                    }
                }
            }
        }

        return $product_name;
    }

    /**
     * check product is in stock
     *
     * @param $product_id
     * @param null|int $product_version_id null if product not multi versions or check for all versions
     * @return bool
     */
    public function isStock($product_id, $product_version_id = null)
    {
        $sql = "SELECT p.sale_on_out_of_stock, 
                       pts.quantity, 
                       pts.product_version_id 
                FROM " . DB_PREFIX . "product_to_store pts 
                LEFT JOIN `" . DB_PREFIX . "product` p ON p.`product_id` = pts.`product_id` 
                WHERE pts.`product_id` = " . (int)$product_id . " 
                  AND pts.`store_id` = p.`default_store_id`";

        if (!is_null($product_version_id)) {
            $sql .= "AND pts.`product_version_id` = " . (int)$product_version_id;
        }

        $query = $this->db->query($sql);
        if (!isset($query->rows) || empty($query->rows)) {
            return false;
        }

        $in_stock_count = 0;
        $not_selected_store = [];
        $not_selected_store_sql = [];
        foreach ($query->rows as $row) {
            /* notice: if product not select any store for quantity, known as quantity = 0, then check sale_on_out_of_stock only! */
            if (!isset($row['quantity'])) {
                $not_selected_store[] = (int)$row['product_version_id'];
                $not_selected_store_sql[] = 'pts.`product_version_id` = ' . (int)$row['product_version_id'];
            } else {
                $in_stock = ($row['quantity'] > 0) || (isset($row['sale_on_out_of_stock']) && $row['sale_on_out_of_stock'] == 1);
                if ($in_stock) {
                    $in_stock_count++;
                }

                if ($in_stock_count > 0) {
                    return true;
                }
            }
        }

        if (!empty($not_selected_store)) {
            $sql = "SELECT p.sale_on_out_of_stock 
                        FROM " . DB_PREFIX . "product p 
                        LEFT JOIN `" . DB_PREFIX . "product_to_store` pts ON p.`product_id` = pts.`product_id` 
                        WHERE p.`product_id` = " . (int)$product_id . "
                          AND (" . implode(' OR ', $not_selected_store_sql) . ")";
            $query = $this->db->query($sql);

            foreach ($query->rows as $row) {
                $in_stock = isset($row['sale_on_out_of_stock']) && $row['sale_on_out_of_stock'] == 1;
                if ($in_stock) {
                    $in_stock_count++;
                }

                if ($in_stock_count > 0) {
                    return true;
                }
            }
        }

        // old code. TODO: remove...
        // foreach ($query->rows as $row) {
        //     /* notice: if product not select any store for quantity, known as quantity = 0, then check sale_on_out_of_stock only! */
        //     if (!isset($row['quantity'])) {
        //         $sql = "SELECT p.sale_on_out_of_stock
        //                 FROM " . DB_PREFIX . "product p
        //                 WHERE p.`product_id` = " . (int)$product_id;
        //         $query = $this->db->query($sql);
        //
        //         $in_stock = isset($query->row['sale_on_out_of_stock']) && $query->row['sale_on_out_of_stock'] == 1;
        //     } else {
        //         $in_stock = ($row['quantity'] > 0) || (isset($row['sale_on_out_of_stock']) && $row['sale_on_out_of_stock'] == 1);
        //     }
        //
        //     if ($in_stock) {
        //         $in_stock_count++;
        //     }
        // }

        /* notice: if product multi versions: out of stock means as all versions out of stock! */
        return $in_stock_count > 0;
    }

    /**
     * get Product Quantity By Store
     *
     * @param int $product_id
     * @param null|int $product_version_id null for product not multi versions
     * @param null|int $store_id null for default store
     * @return null|int null if no store
     */
    public function getProductQuantityByStore($product_id, $product_version_id = null, $store_id = null)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT pts.quantity 
                FROM `{$DB_PREFIX}product_to_store` pts 
                LEFT JOIN `{$DB_PREFIX}product` p 
                       ON p.`product_id` = pts.`product_id` 
                WHERE pts.`product_id` = " . (int)$product_id . " 
                  AND pts.`product_version_id` = " . (int)$product_version_id . " ";

        if (!is_null($store_id)) {
            $sql .= "AND pts.`store_id` = " . (int)$store_id . " ";
        } else {
            $sql .= "AND pts.`store_id` = p.`default_store_id`";
        }

        $query = $this->db->query($sql);

        /* notice: if product not select any store for quantity, known as quantity = null, then check sale_on_out_of_stock only! */
        return (!isset($query->row['quantity'])) ? null : $query->row['quantity'];
    }

    /**
     * get product version by id
     *
     * @param $product_version_id
     * @return mixed
     */
    public function getProductVersion($product_version_id) {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}product_version` WHERE `product_version_id` = " . (int)$product_version_id . " LIMIT 1";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getProductVersionByProductId($product_id) {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}product_version` WHERE `product_id` = " . (int)$product_id ." AND deleted is null";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @param array $data, format
     * - single product:
     * {
     *     "name": "test social 1 - 1PB",
     *     "description": "&lt;p&gt;test 1 - desc&lt;\/p&gt;\r\n\r\n&lt;p&gt;test 1 - desc&lt;\/p&gt;\r\n\r\n&lt;p&gt;test 1 - desc&lt;\/p&gt;",
     *     "summary": "&lt;p&gt;test 1 - short&lt;\/p&gt;\r\n\r\n&lt;p&gt;test 1 - short&lt;\/p&gt;",
     *     "product_version": "0",
     *     "price": "10,000",
     *     "promotion_price": "9,000",
     *     "weight": "123",
     *     "sku": "SOCIAL001",
     *     "barcode": "82574258001",
     *     "stores_default_id": "0",
     *     "sale_on_out_of_stock": "1",
     *     "common_compare_price": "",
     *     "common_price": "",
     *     "common_weight": "0",
     *     "common_sku": "",
     *     "common_barcode": "",
     *     "stores_default_id_mul": "0",
     *     "common_cost_price": "0",
     *     "common_sale_on_out_of_stock": "1",
     *     "attribute_name": [
     *         ""
     *     ],
     *     "stores": [
     *         [
     *             "0"
     *         ]
     *     ],
     *     "original_inventory": [
     *         [
     *         "50"
     *         ]
     *     ],
     *     "cost_price": [
     *         [
     *         "5,000"
     *         ]
     *     ],
     *     "seo_title": "",
     *     "seo_description": "",
     *     "alias": "",
     *     "meta_keyword": "",
     *     "images": [
     *         "https:\/\/res.cloudinary.com\/bestme-test\/image\/upload\/v1587983715\/x2.bestme.test\/cf714c11ee1dab3ee6fbac9e7bea41eb.jpg",
     *         "https:\/\/res.cloudinary.com\/bestme-test\/image\/upload\/v1587983704\/x2.bestme.test\/942706098598332177618186301982977727397888n.jpg"
     *     ],
     *     "image-alts": [
     *         "",
     *         ""
     *     ],
     *     "status": "1",
     *     "category": [
     *         "add_new_social",
     *         "add_new_cat-social"
     *     ],
     *     "manufacturer": "add_new_man-social",
     *     "collection_list": [
     *         "35"
     *     ],
     *     "tag_list": [
     *         "25"
     *     ],
     *     "channel": "0"
     * }
     *
     * - multi versions:
     *
     * {
     *     "name": "test social 2 - NPB",
     *     "description": "&lt;p&gt;test social 2 - NPB - desc&lt;\/p&gt;\r\n\r\n&lt;p&gt;test social 2 - NPB - desc&lt;\/p&gt;\r\n\r\n&lt;p&gt;test social 2 - NPB\u00a0- desc&lt;\/p&gt;",
     *     "summary": "&lt;p&gt;test social 1 - NPB - short&lt;\/p&gt;\r\n\r\n&lt;p&gt;test social 1 - NPB - short&lt;\/p&gt;\r\n\r\n&lt;p&gt;test social 1 - NPB - short&lt;\/p&gt;",
     *     "product_version": "1",
     *     "price": "0",
     *     "promotion_price": "",
     *     "weight": "0",
     *     "sku": "",
     *     "barcode": "",
     *     "stores_default_id": "0",
     *     "sale_on_out_of_stock": "1",
     *     "common_compare_price": "20,000",
     *     "common_price": "18,000",
     *     "common_weight": "150",
     *     "common_sku": "SOCIALNPB2",
     *     "common_barcode": "123456789002",
     *     "stores_default_id_mul": "0",
     *     "common_cost_price": "16,000",
     *     "common_sale_on_out_of_stock": "1",
     *     "attribute_name": [
     *         "M\u00e0u s\u1eafc"
     *     ],
     *     "attribute_values": [
     *         [
     *             "XANH",
     *             "CAM"
     *         ]
     *     ],
     *     "product_display": [
     *         "XANH",
     *         "CAM"
     *     ],
     *     "product_version_names": [
     *         "XANH",
     *         "CAM"
     *     ],
     *     "product_version_ids": [
     *         "",
     *         ""
     *     ],
     *     "product_prices": [
     *         "20,000",
     *         "20,000"
     *     ],
     *     "product_promotion_prices": [
     *         "17,000",
     *         "18,000"
     *     ],
     *     "product_quantities": [
     *         "",
     *         ""
     *     ],
     *     "product_skus": [
     *         "SOCIALNPB21",
     *         "SOCIALNPB22"
     *     ],
     *     "product_barcodes": [
     *         "123456789002",
     *         "123456789002"
     *     ],
     *     "seo_title": "test social 2 - NPB seo title",
     *     "seo_description": "test social 2 - NPB seo desc",
     *     "alias": "test social 2 - NPB",
     *     "meta_keyword": "test social 2 - NPB,bestme-social",
     *     "images": [
     *         "https:\/\/res.cloudinary.com\/bestme-test\/image\/upload\/v1587983715\/x2.bestme.test\/cf714c11ee1dab3ee6fbac9e7bea41eb.jpg",
     *         "https:\/\/res.cloudinary.com\/bestme-test\/image\/upload\/v1587983704\/x2.bestme.test\/942706098598332177618186301982977727397888n.jpg"
     *     ],
     *     "image-alts": [
     *         "",
     *         ""
     *     ],
     *     "status": "1",
     *     "category": [
     *         "183"
     *     ],
     *     "manufacturer": "48",
     *     "collection_list": [
     *         "35"
     *     ],
     *     "tag_list": [
     *         "25"
     *     ],
     *     "channel": "0"
     *     }
     *
     * @return mixed
     */
    public function addProduct($data)
    {
        /* quickly add manufacturer if has */
        $manufacturer_id = '';
        if (isset($data['manufacturer']) && $data['manufacturer'] != null) {
            $this->load->model('catalog/manufacturer');
            if (strpos($data['manufacturer'], 'add_new_') !== false) {
//                $manufacturer_id = $this->model_catalog_manufacturer->getManufacturerIdByName(str_replace('add_new_', '', $data['manufacturer']));
//
//                if (!$manufacturer_id) {
                    $manufacturer_id = $this->model_catalog_manufacturer->addManufacturerFast(str_replace('add_new_', '', $data['manufacturer']));
                //}
            } else {
                $manufacturer_id = $data['manufacturer'];
            }

            if (!empty($data['source'])) {
                $manufacturer = $this->model_catalog_manufacturer->getManufacturerById($manufacturer_id);
                if (isset($manufacturer['source']) && $manufacturer['source'] != 'omihub') {
                    $this->model_catalog_manufacturer->updateSourceManufacture($manufacturer_id, $data['source']);
                }
            }
        }
        // get user_store
        $user_id = $data['created_user_id'];
        $this->load->model('user/user');
        $user_stores = $this->model_user_user->getUserStore($user_id);
        $default_store = !empty($user_stores) ? $user_stores[0]['id'] : 0;

        /* update default stores */
        if ((int)$data['product_version'] == 1) {
            $sale_on_out_of_stock = isset($data['common_sale_on_out_of_stock']) ? $data['common_sale_on_out_of_stock'] : 0;
            $weight = isset($data['common_weight']) ? extract_number($data['common_weight']) : 0;
            $default_store_id = isset($data['stores_default_id_mul']) ? $data['stores_default_id_mul'] : $default_store;
        } else {
            $sale_on_out_of_stock = isset($data['sale_on_out_of_stock']) ? $data['sale_on_out_of_stock'] : 0;
            $weight = isset($data['weight']) ? extract_number($data['weight']) : 0;
            $default_store_id = isset($data['stores_default_id']) ? $data['stores_default_id'] : $default_store;
        }

        /* create product */
        $this->db->query("INSERT INTO " . DB_PREFIX . "product 
                          SET demo = 0, 
                              sku = '" . $this->db->escape($data['sku']) . "', 
                              quantity = '" . (int)0 . "', 
                              price = '" . (float)str_replace(',', '', $data['promotion_price']) . "', 
                              price_currency_id = 1, 
                              compare_price = '" . (float)str_replace(',', '', $data['price']) . "', 
                              c_price_currency_id = 1, 
                              weight = '" . (float)extract_number($weight) . "', 
                              channel = '" . (isset($data['channel']) ? (int)$data['channel'] : 2) . "', 
                              weight_class_id = 1, 
                              status = '" . (int)$data['status'] . "', 
                              manufacturer_id = '" . (int)$manufacturer_id . "', 
                              barcode = '" . $data['barcode'] . "', 
                              sale_on_out_of_stock = '" . (int)$sale_on_out_of_stock . "', 
                              common_barcode = '" . $this->db->escape(getValueByKey($data, 'common_barcode')) . "', 
                              common_sku = '" . $this->db->escape(getValueByKey($data, 'common_sku')) . "', 
                              common_price = '" . (float)str_replace(',', '', getValueByKey($data, 'common_price')) . "', 
                              common_cost_price = '" . (float)str_replace(',', '', getValueByKey($data, 'common_cost_price')) . "', 
                              common_compare_price = '" . (float)str_replace(',', '', getValueByKey($data, 'common_compare_price')) . "', 
                              multi_versions = '" . (int)$data['product_version'] . "', 
                              `default_store_id` = '" . (int)$default_store_id . "',
                              user_create_id = '". (int)$user_id ."',
                              date_added = NOW(), 
                              date_modified = NOW()");

        $product_id = $this->db->getLastId();
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        // update source
        if (!empty($data['source'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET `source` = '" . $this->db->escape($data['source']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }

        // update brand_shop_name
        if (!empty($data['brand_shop_name'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET `brand_shop_name` = '" . $this->db->escape($data['brand_shop_name']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }

        /* update main image */
        if (!empty($data['images'])) {
            if (!empty($data['images'][0])) {
                $alt = '';
                if (!empty($data['image-alts'][0])) {
                    $alt = $data['image-alts'][0];
                    array_shift($data['image-alts']);
                }
                $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['images'][0]) . "', image_alt = '" . $this->db->escape($alt) . "' WHERE product_id = '" . (int)$product_id . "'");
            }
            array_shift($data['images']);
        }

        /* update description */
        if (isset($data['name'])) {
            $language_id = (int)$this->config->get('config_language_id');
            $data['description'] = isset($data['description']) ? $data['description'] : '';
            $data['meta_keyword'] = isset($data['meta_keyword']) ? $data['meta_keyword'] : '';
            $data['seo_title'] = isset($data['seo_title']) ? $data['seo_title'] : '';
            $data['seo_description'] = isset($data['seo_description']) ? $data['seo_description'] : '';
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', sub_description = '" . $this->db->escape($data['summary']) . "', seo_title = '" . $this->db->escape($data['seo_title']) . "', seo_description = '" . $this->db->escape($data['seo_description']) . "', meta_keyword = '" . $this->db->escape($data['meta_keyword']) . "'");
        }

        /* update description tab */
        if (isset($data['title_tab']) && isset($data['description_tab'])) {
            foreach ($data['title_tab'] as $key => $title_tab) {
                $description_tab = $data['description_tab'][$key];
                if ($title_tab != null) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_description_tab SET product_id = '" . (int)$product_id . "', title = '" . $this->db->escape($title_tab) . "', description = '" . $this->db->escape($description_tab) . "'");
                }
            }
        }

        /* update product version (attribute) values */
        if (isset($data['attribute_name']) && (int)$data['product_version'] == 1) {
            foreach ($data['attribute_name'] as $k => $attribute) {
                $attribute = trim($attribute);
                if ($attribute != '') {
                    $language_id = (int)$this->config->get('config_language_id'); //TV
                    $this->db->query("INSERT INTO " . DB_PREFIX . "attribute SET attribute_group_id = 0, sort_order = 0 ");
                    $attribute_id = $this->db->getLastId();
                    $this->db->query("INSERT INTO " . DB_PREFIX . "attribute_description SET attribute_id = '" . (int)$attribute_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($attribute) . "'");
                    $attribute_description = implode(",", $data['attribute_values'][$k]);
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$attribute_id . "', language_id = '" . (int)$language_id . "', text = '" . $this->db->escape($attribute_description) . "'");
                }
            }

            // add attribute filter
            $this->load->model('catalog/attribute_filter');
            $this->model_catalog_attribute_filter->updateAttributeFilterByProduct($product_id);
        }

        /* update product version */
        $product_version_ids = [];

        if (isset($data['product_version_names']) && (int)$data['product_version'] == 1) {
            foreach ($data['product_version_names'] as $k => $version_name) {
                /* TODO: remove...
                $sku = isset($data['product_skus'][$k]) ? $data['product_skus'][$k] : '';
                if ($sku == '') {
                    continue;
                }*/
                if (isset($data['product_display'])) {
                    $display = in_array($version_name, $data['product_display']) ? 1 : 0;
                } else {
                    $display = 0;
                }

                $product_version_image = isset($data['product_version_images'][$k]) ? $data['product_version_images'][$k] : '';
                $product_version_image_alt = isset($data['product_version_image_alts'][$k]) ? $data['product_version_image_alts'][$k] : '';

                $new_key = $k; // $this->replaceVersionValues($version_name); (not use new unique generated key)
                $version_name = implode(',', explode(' • ', $version_name));
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_version 
                                    SET product_id = '" . (int)$product_id . "',
                                        version = '" . $this->db->escape($version_name) . "',
                                        price = '" . $this->db->escape(str_replace(',', '', $data['product_promotion_prices'][$k])) . "', compare_price = '" . $this->db->escape(str_replace(',', '', $data['product_prices'][$k])) . "',
                                        sku = '" . $this->db->escape($data['product_skus'][$k]) . "',
                                        barcode = '" . $this->db->escape($data['product_barcodes'][$k]) . "',
                                        quantity = '" . $this->db->escape($data['product_quantities'][$k]) . "',
                                        status = '" . (int)$display . "',
                                        `image` = '" . $this->db->escape($product_version_image) . "',
                                        `image_alt` = '" . $this->db->escape($product_version_image_alt) . "'");
                $product_version_id = $this->db->getLastId();
                $product_version_ids[$new_key] = $product_version_id;
            }
        }

        /* update additional images */
        if (isset($data['images'])) {
            foreach ($data['images'] as $k => $image) {
                $alt = isset($data['image-alts'][$k]) ? $data['image-alts'][$k] : '';
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($image) . "', image_alt = '" . $this->db->escape($alt) . "', sort_order = " . (int)$k);
            }
        }

        /* update product-category */
        if (isset($data['category'])) {
            $this->load->model('catalog/category');
            foreach ($data['category'] as $category_id) {
                if (strpos($category_id, 'add_new_') !== false) {
//                    $category_id = $this->model_catalog_category->getCategoryIdByName(str_replace('add_new_', '', $category_id));
//                    if (!$category_id) {
                        $category_id = $this->model_catalog_category->addCategoryFast(str_replace('add_new_', '', $category_id));
//                    }
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");

                if (!empty($data['source'])) {
                    $category = $this->model_catalog_category->getCategory($category_id);
                    if (isset($category['source']) && $category['source'] != 'omihub') {
                        $this->model_catalog_category->updateSourceCategory($category_id, $data['source']);
                    }
                }
            }
        }

        /* update product-collection */
        if (isset($data['collection_list'])) {
            foreach ($data['collection_list'] as $collection_id) {
                if (strpos($collection_id, 'add_new_') !== false) {
                    $this->load->model('catalog/collection');
                    $collection_id = $this->model_catalog_collection->addCollectionTitleOnly(str_replace('add_new_', '', $collection_id));
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_collection SET product_id = '" . (int)$product_id . "', collection_id = '" . (int)$collection_id . "'");
            }
        }

        /* update product-tag */
        if (isset($data['tag_list'])) {
            foreach ($data['tag_list'] as $tag_id) {
                if (strpos($tag_id, 'add_new_') !== false) {
                    $this->load->model('catalog/tag');
                    $tag_id = $this->model_catalog_tag->addTagFast(str_replace('add_new_', '', $tag_id));
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_tag SET product_id = '" . (int)$product_id . "', tag_id = '" . (int)$tag_id . "'");
            }
        }

        /* update product to store */
        if (isset($data['stores'])) {
            if (!empty($product_version_ids)) {
                foreach ($product_version_ids as $key => $pv_id) {
                    if (!isset($data['stores'][$key]) || !is_array($data['stores'][$key])) {
                        continue;
                    }

                    foreach ($data['stores'][$key] as $store_key => $store_id) {
                        $item_quantity = isset($data['original_inventory'][$key][$store_key]) ? (int)$data['original_inventory'][$key][$store_key] : 0;
                        $item_cost_price = isset($data['cost_price'][$key][$store_key]) ? (float)extract_number($data['cost_price'][$key][$store_key]) : 0;
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store 
                                          SET `product_id` = '" . (int)$product_id . "', 
                                              `store_id` = '" . (int)$store_id . "',
                                              `product_version_id` = '" . (int)$pv_id . "', 
                                              `quantity` = '" . $item_quantity . "', 
                                              `cost_price` = '" . $item_cost_price . "'");
                    }
                }
            } else {
                foreach ($data['stores'] as $key => $store) {
                    // get user_store
                    $user_stores = $this->model_user_user->getUserStore($user_id);
                    $default_store = !empty($user_stores) ? $user_stores[0]['id'] : 0;

                    $store_id = (isset($store[0])) ? $store[0] : $default_store;
                    $original_inventory = (isset($data['original_inventory'][$key][0])) ? $data['original_inventory'][$key][0] : 0;
                    $cost_price = (isset($data['cost_price'][$key][0])) ? $data['cost_price'][$key][0] : 0;

                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store 
                                      SET `product_id` = '" . (int)$product_id . "', 
                                          `store_id` = '" . (int)$store_id . "', 
                                          `product_version_id` = '" . (int)0 . "', 
                                          `quantity` = '" . (int)$original_inventory . "', 
                                          `cost_price` = '" . (float)extract_number($cost_price) . "'");
                }
            }
        }

        /* create seo */
        $product_alias = $data['name'];
        if (isset($data['alias']) && $data['alias'] != '') {
            $product_alias = $data['alias'];
        }

        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($product_alias, '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);
        foreach ($languages as $key => $language) {
            $language_id = $language['language_id'];
            $alias = 'product_id=' . $product_id;
            $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                    SET language_id = '" . (int)$language_id . "', 
                        query = '" . $alias . "', 
                        keyword = '" . $slug_custom . "', 
                        table_name = 'product'";
            $this->db->query($sql);
        }

        $this->deleteProductCaches();

        /* update product warehouse */
        // original product
        // TODO: remove warehouse
        /*$this->db->query("INSERT INTO " . DB_PREFIX . "warehouse SET product_id = '" . (int)$product_id . "', product_version_id = NULL");

        // multi versions of product
        if (!empty($product_version_ids)) {
            foreach ($product_version_ids as $pv_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "warehouse SET product_id = '" . (int)$product_id . "', product_version_id = '" . (int)$pv_id . "'");
            }
        }*/

        // send product to rabbit mq
        try {
            $this->sendToRabbitMq($product_id);
        } catch (Exception $exception) {
            $this->log->write('Send message add product catalog error: ' . $exception->getMessage());
        }

        return $product_id;
    }

    function replaceVersionValues($str)
    {
        // unescape version value due to version may contains special char that was escaped
        // e.g BLACK & WHITE => BLACK &apm; WHITE => BLACK__apm__WHITE instead of our expect is BLACK___WHITE
        $str = html_entity_decode($str);

        return preg_replace('/[^0-9a-zA-Z\-_]+/', '_', $str) . '_' . md5($str);
    }

    public function checkExistingProductByName($name, $exclude_product_id)
    {
        $product_id = $this->searchProductByName($name);

        if ($product_id && $product_id != $exclude_product_id) {
            return true;
        }

        return false;
    }

    public function searchProductByName($name)
    {
        $sql = "SELECT p.`product_id` FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

        $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND pd.name = '" . $this->db->escape($name) . "'";

        $sql .= " AND p.`deleted` IS NULL ";

        $query = $this->db->query($sql);
        if (isset($query->row['product_id'])) {
            return $query->row['product_id'];
        } else {
            return false;
        }
    }

    public function searchProductAndVersionBySKU($sku, $product_id)
    {
        $sql = "SELECT product_id FROM " . DB_PREFIX . "product WHERE sku = '" . $this->db->escape($sku) . "' AND deleted IS NULL";
        if (!empty($product_id)) {
            $sql .= " AND product_id != '" . (int)$product_id . "'";
        }
        $query = $this->db->query($sql);
        if (isset($query->row['product_id'])) {
            return false;
        }

        $sql = "SELECT product_id FROM " . DB_PREFIX . "product_version WHERE sku = '" . $this->db->escape($sku) . "' AND deleted IS NULL";
        if (!empty($product_id)) {
            $sql .= " AND product_id != '" . (int)$product_id . "'";
        }
        $query = $this->db->query($sql);
        if (isset($query->row['product_id'])) {
            return false;
        }

        return true;
    }

    public function searchProductAndVersionBySkuForSync($sku)
    {
        $sql = "SELECT product_id FROM " . DB_PREFIX . "product WHERE sku = '" . $this->db->escape($sku) . "' AND deleted IS NULL";

        $query = $this->db->query($sql);
        if (isset($query->row['product_id'])) {
            return $query->row['product_id'];
        }

        $sql = "SELECT product_id FROM " . DB_PREFIX . "product_version WHERE sku = '" . $this->db->escape($sku) . "' AND deleted IS NULL";

        $query = $this->db->query($sql);
        if (isset($query->row['product_id'])) {
            return $query->row['product_id'];
        }

        return 0;
    }

    /**
     * edit product
     *
     * Too many duplicated code with function add()
     * TODO: re-use code (common function, extract function, ...), do not duplicate code...
     * @param int $product_id
     * @param array $data
     */
    public function editProduct($product_id, $data)
    {
        /* quickly add manufacturer if has */
        $manufacturer_id = '';
        if (isset($data['manufacturer']) && $data['manufacturer'] != null) {
            $this->load->model('catalog/manufacturer');
            if (strpos($data['manufacturer'], 'add_new_') !== false) {
//                $manufacturer_id = $this->model_catalog_manufacturer->getManufacturerIdByName(str_replace('add_new_', '', $data['manufacturer']));
//
//                if (!$manufacturer_id) {
                    $manufacturer_id = $this->model_catalog_manufacturer->addManufacturerFast(str_replace('add_new_', '', $data['manufacturer']));
                //}
            } else {
                $manufacturer_id = $data['manufacturer'];
            }

            if (!empty($data['source'])) {
                $manufacturer = $this->model_catalog_manufacturer->getManufacturerById($manufacturer_id);
                if (isset($manufacturer['source']) && $manufacturer['source'] != 'omihub') {
                    $this->model_catalog_manufacturer->updateSourceManufacture($manufacturer_id, $data['source']);
                }
            }
        }

        /* update default stores */
        $sale_on_out_of_stock = isset($data['sale_on_out_of_stock']) ? $data['sale_on_out_of_stock'] : 0;
        $weight = isset($data['weight']) ? extract_number($data['weight']) : 0;
        $default_store_id = isset($data['stores_default_id']) ? $data['stores_default_id'] : 0;

        $old_product = $this->getProductForLazada($product_id);
        $old_product_version = $this->getProductVersions($product_id);

        /* update product */
        $this->db->query("UPDATE " . DB_PREFIX . "product 
                          SET demo = 0, 
                              sku = '" . $this->db->escape($data['sku']) . "', 
                              quantity = '" . 0 . "', 
                              price = '" . (float)str_replace(',', '', $data['promotion_price']) . "', 
                              price_currency_id = 1, 
                              compare_price = '" . (float)str_replace(',', '', $data['price']) . "', 
                              c_price_currency_id = 1, 
                              weight = '" . (float)$weight . "', 
                              weight_class_id = 1, 
                              status = '" . (int)$data['status'] . "', 
                              channel = '" . (isset($data['channel']) ? (int)$data['channel'] : 2) . "', 
                              manufacturer_id = '" . (int)$manufacturer_id . "', 
                              barcode = '" . $data['barcode'] . "', 
                              common_barcode = '" . $this->db->escape(getValueByKey($data, 'common_barcode')) . "', 
                              common_sku = '" . $this->db->escape(getValueByKey($data, 'common_sku')) . "', 
                              common_price = '" . (float)str_replace(',', '', getValueByKey($data, 'common_price')) . "', 
                              common_cost_price = '" . (float)str_replace(',', '', getValueByKey($data, 'common_cost_price')) . "', 
                              common_compare_price = '" . (float)str_replace(',', '', getValueByKey($data, 'common_compare_price')) . "', 
                              sale_on_out_of_stock = '" . (int)$sale_on_out_of_stock . "', 
                              multi_versions = '" . (int)$data['product_version'] . "', 
                              default_store_id = '" . (int)$default_store_id . "', 
                              date_modified = NOW() 
                              WHERE product_id = '" . (int)$product_id . "'");

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        // update source
        if (!empty($data['source'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET `source` = '" . $this->db->escape($data['source']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }

        // update brand_shop_name
        if (!empty($data['brand_shop_name'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET `brand_shop_name` = '" . $this->db->escape($data['brand_shop_name']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }

        $DB_PREFIX = DB_PREFIX;
        $sql = "DELETE FROM `{$DB_PREFIX}product_description_tab` WHERE product_id = " . (int)$product_id;
        $this->db->query($sql);

        /* update product description tab */
        if (isset($data['title_tab']) && isset($data['description_tab'])) {
            foreach ($data['title_tab'] as $key => $title_tab) {
                $description_tab = $data['description_tab'][$key];
                if ($title_tab != null) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_description_tab
                    SET product_id = '" . (int)$product_id . "', title = '" . $this->db->escape($title_tab) . "', description = '" . $this->db->escape($description_tab) . "'");
                }
            }
        }

        /* update main image */
        if (!empty($data['images'])) {
            if (!empty($data['images'][0])) {
                $alt = '';
                if (isset($data['image-alts'][0])) {
                    $alt = $data['image-alts'][0];
                    array_shift($data['image-alts']);
                }
                $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['images'][0]) . "', image_alt = '" . $this->db->escape($alt) . "' WHERE product_id = '" . (int)$product_id . "'");
            }
            array_shift($data['images']);
        } else {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '', image_alt = '' WHERE product_id = '" . (int)$product_id . "'");
        }

        /* update description */
        // remove first
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

        // update
        if (isset($data['name'])) {
            $data['description'] = isset($data['description']) ? $data['description'] : '';
            $data['meta_keyword'] = isset($data['meta_keyword']) ? $data['meta_keyword'] : '';
            $language_id = (int)$this->config->get('config_language_id');
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description 
                              SET product_id = '" . (int)$product_id . "', 
                                  language_id = '" . (int)$language_id . "', 
                                  name = '" . $this->db->escape($data['name']) . "', 
                                  description = '" . $this->db->escape($data['description']) . "', 
                                  sub_description = '" . $this->db->escape($data['summary']) . "', 
                                  seo_title = '" . $this->db->escape($data['seo_title']) . "', 
                                  seo_description = '" . $this->db->escape($data['seo_description']) . "',
                                  meta_keyword = '" . $this->db->escape($data['meta_keyword']) . "'");
        }

        /* update product to store - old code. TODO: remove... */
        /*// remove first
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store
                          WHERE product_id = '" . (int)$product_id . "'");

        // update
        if (isset($data['store_list'])) {
            foreach ($data['store_list'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store
                                  SET product_id = '" . (int)$product_id . "',
                                      store_id = '" . (int)$store_id . "'");
            }
        }*/

        /* update product version (attribute) values */
        if (!array_key_exists('shopee_update_product', $data) || $data['shopee_update_product'] != 'not_update') {
            // get all product attributes
            $this->load->model('catalog/attribute');
            $old_attributes = $this->model_catalog_attribute->getAttributes(['product_id' => $product_id]);
            $old_attributes = array_column($old_attributes, 'name');
            $old_attributes = array_map('mb_strtolower', $old_attributes);

            // remove first
            $this->deleteProductAttributes($product_id);

            // update
            if (isset($data['attribute_name']) && (int)$data['product_version'] == 1) {
                foreach ($data['attribute_name'] as $k => $attribute) {
                    $attribute = trim($attribute);
                    if ($attribute != '') {
                        $language_id = (int)$this->config->get('config_language_id'); //TV
                        $this->db->query("INSERT INTO " . DB_PREFIX . "attribute 
                                      SET attribute_group_id = 0, 
                                          sort_order = 0 ");

                        $attribute_id = $this->db->getLastId();
                        $this->db->query("INSERT INTO " . DB_PREFIX . "attribute_description 
                                      SET attribute_id = '" . (int)$attribute_id . "', 
                                          language_id = '" . (int)$language_id . "', 
                                          name = '" . $this->db->escape($attribute) . "'");

                        $attribute_description = implode(",", $data['attribute_values'][$k]);
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute 
                                      SET product_id = '" . (int)$product_id . "', 
                                          attribute_id = '" . (int)$attribute_id . "', 
                                          language_id = '" . (int)$language_id . "', 
                                          text = '" . $this->db->escape($attribute_description) . "'");

                        // remove old attribute from array to update attribute filter if it has not change (update when calling by product)
                        $old_attribute_key = array_search(mb_strtolower($attribute), $old_attributes);
                        if (false !== $old_attribute_key) {
                            unset($old_attributes[$old_attribute_key]);
                        }
                    }
                }
            }

            // update attribute filter vs old product attribute
            $this->load->model('catalog/attribute_filter');
            foreach ($old_attributes as $old_attribute) {
                $this->model_catalog_attribute_filter->updateAttributeFilterByName($old_attribute);
            }

            // add attribute filter
            $this->model_catalog_attribute_filter->updateAttributeFilterByProduct($product_id);

            /* update product version */
            // soft-delete first
            $this->db->query("UPDATE " . DB_PREFIX . "product_version 
                          SET deleted = 1 
                          WHERE product_id = '" . (int)$product_id . "'");
            $product_version_ids = [];
            $product_version_ids_api = [];
            if (isset($data['product_version_names']) && (int)$data['product_version'] == 1) {
                foreach ($data['product_version_names'] as $k => $version_name) {
                    /* TODO: remove...
                    $sku = isset($data['product_skus'][$k]) ? $data['product_skus'][$k] : '';
                    if ($sku == '') {
                        continue;
                    }
                    $display = in_array($version_name, $data['product_display']) ? 1 : 0;*/
                    if (isset($data['product_display'])) {
                        $display = in_array($version_name, $data['product_display']) ? 1 : 0;
                    } else {
                        $display = 0;
                    }

                    $product_version_image = isset($data['product_version_images'][$k]) ? $data['product_version_images'][$k] : '';
                    $product_version_image_alt = isset($data['product_version_image_alts'][$k]) ? $data['product_version_image_alts'][$k] : '';

                    $new_key = $this->replaceVersionValues($version_name);
                    $version_name = implode(',', explode(' • ', $version_name));
                    if (isset($data['product_version_ids'][$k]) && !empty($data['product_version_ids'][$k])) {
                        $this->db->query("DELETE FROM " . DB_PREFIX . "product_version WHERE product_id = '" . (int)$product_id . "' AND product_version_id = '" . (int)$data['product_version_ids'][$k] . "'");
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_version
                                            SET product_version_id = '" . (int)$data['product_version_ids'][$k] . "',
                                                product_id = '" . (int)$product_id . "',
                                                version = '" . $this->db->escape($version_name) . "',
                                                price = '" . $this->db->escape(str_replace(',', '', $data['product_promotion_prices'][$k])) . "',
                                                compare_price = '" . $this->db->escape(str_replace(',', '', $data['product_prices'][$k])) . "',
                                                sku = '" . $this->db->escape($data['product_skus'][$k]) . "',
                                                barcode = '" . $this->db->escape($data['product_barcodes'][$k]) . "',
                                                quantity = '" . $this->db->escape($data['product_quantities'][$k]) . "',
                                                status = '" . (int)$display . "',
                                                `image` = '" . $this->db->escape($product_version_image) . "',
                                                `image_alt` = '" . $this->db->escape($product_version_image_alt) . "'");
                        $product_version_id = $this->db->getLastId();
                    } else {
                        $product_version_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_version WHERE product_id = '" . (int)$product_id . "' AND version ='" . $version_name . "'");
                        $product_version = $product_version_query->row;
                        if ($product_version) {
                            $this->db->query("UPDATE " . DB_PREFIX . "product_version SET product_id = '" . (int)$product_id . "', version = '" . $this->db->escape($version_name) . "', price = '" . $this->db->escape(str_replace(',', '', $data['product_promotion_prices'][$k])) . "', compare_price = '" . $this->db->escape(str_replace(',', '', $data['product_prices'][$k])) . "', sku = '" . $this->db->escape($data['product_skus'][$k]) . "', barcode = '" . $this->db->escape($data['product_barcodes'][$k]) . "', quantity = '" . $this->db->escape($data['product_quantities'][$k]) . "', status = '" . (int)$display . "', deleted = NULL WHERE product_version_id = '" . (int)$product_version['product_version_id'] . "'");
                            $product_version_id = $product_version['product_version_id'];
                        } else {
                            $this->db->query("INSERT INTO " . DB_PREFIX . "product_version 
                                                SET product_id = '" . (int)$product_id . "',
                                                    version = '" . $this->db->escape($version_name) . "',
                                                    price = '" . $this->db->escape(str_replace(',', '', $data['product_promotion_prices'][$k])) . "',
                                                    compare_price = '" . $this->db->escape(str_replace(',', '', $data['product_prices'][$k])) . "',
                                                    sku = '" . $this->db->escape($data['product_skus'][$k]) . "',
                                                    barcode = '" . $this->db->escape($data['product_barcodes'][$k]) . "',
                                                    quantity = '" . $this->db->escape($data['product_quantities'][$k]) . "',
                                                    status = '" . (int)$display . "',
                                                    `image` = '" . $this->db->escape($product_version_image) . "',
                                                    `image_alt` = '" . $this->db->escape($product_version_image_alt) . "'");
                            $product_version_id = $this->db->getLastId();
                        }
                    }
                    $product_version_ids[$new_key] = $product_version_id;
                    $product_version_ids_api[] = $product_version_id;
                }
            }
        }

        /* update additional images */
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
        if (isset($data['images'])) {
            foreach ($data['images'] as $k => $image) {
                $alt = isset($data['image-alts'][$k]) ? $data['image-alts'][$k] : '';
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($image) . "', image_alt = '" . $this->db->escape($alt) . "', sort_order = " . (int)$k);
            }
        }

        /* update product-category */
        if (!array_key_exists('shopee_update_product', $data) || $data['shopee_update_product'] != 'not_update') {
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
            if (isset($data['category'])) {
                $this->load->model('catalog/category');
                foreach ($data['category'] as $category_id) {
                    if (strpos($category_id, 'add_new_') !== false) {
//                        $category_id = $this->model_catalog_category->getCategoryIdByName(str_replace('add_new_', '', $category_id));
//                        if (!$category_id) {
                            $category_id = $this->model_catalog_category->addCategoryFast(str_replace('add_new_', '', $category_id));
//                        }
                    }
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");

                    if (!empty($data['source'])) {
                        $category = $this->model_catalog_category->getCategory($category_id);
                        if (isset($category['source']) && $category['source'] != 'omihub') {
                            $this->model_catalog_category->updateSourceCategory($category_id, $data['source']);
                        }
                    }
                }
            }
        }

        /* update product-collection */
        // IMPORTANT: if delete collection here, product may lost collection if no collection set!
        if (isset($data['collection_list'])) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_collection WHERE product_id = '" . (int)$product_id . "'");
            foreach ($data['collection_list'] as $collection_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_collection SET product_id = '" . (int)$product_id . "', collection_id = '" . (int)$collection_id . "'");
            }
        }

        /* update product-tag */
        // IMPORTANT: if delete tag here, product may lost tag if no collection set!
        if (isset($data['tag_list'])) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_tag WHERE product_id = '" . (int)$product_id . "'");
            foreach ($data['tag_list'] as $tag_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_tag SET product_id = '" . (int)$product_id . "', tag_id = '" . (int)$tag_id . "'");
            }
        }

        /* update product to store */
        if (!array_key_exists('shopee_update_product', $data) || $data['shopee_update_product'] != 'not_update') {
            // remove first
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store 
                          WHERE product_id = '" . (int)$product_id . "'");

            // update
            if (isset($data['stores'])) {
                if (!empty($product_version_ids)) {
                    foreach ($product_version_ids as $key => $pv_id) {
                        if (!isset($data['stores'][$key]) || !is_array($data['stores'][$key])) {
                            continue;
                        }

                        foreach ($data['stores'][$key] as $store_key => $store_id) {
                            $item_quantity = isset($data['original_inventory'][$key][$store_key]) ? (int)$data['original_inventory'][$key][$store_key] : 0;
                            $item_cost_price = isset($data['cost_price'][$key][$store_key]) ? (float)extract_number($data['cost_price'][$key][$store_key]) : 0;
                            $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store 
                                          SET `product_id` = '" . (int)$product_id . "', 
                                              `store_id` = '" . (int)$store_id . "',
                                              `product_version_id` = '" . (int)$pv_id . "', 
                                              `quantity` = '" . $item_quantity . "', 
                                              `cost_price` = '" . $item_cost_price . "'");
                        }
                    }
                } else {
                    $list_used = [];
                    foreach ($data['stores'] as $key => $store) {
                        $store_id = (isset($store[0])) ? $store[0] : 0;
                        $original_inventory = (isset($data['original_inventory'][$key][0])) ? $data['original_inventory'][$key][0] : 0;
                        $cost_price = (isset($data['cost_price'][$key][0])) ? $data['cost_price'][$key][0] : 0;

                        if (in_array($store_id, $list_used)) {
                            continue;
                        }

                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store 
                                      SET `product_id` = '" . (int)$product_id . "', 
                                          `store_id` = '" . (int)$store_id . "', 
                                          `product_version_id` = '" . (int)0 . "', 
                                          `quantity` = '" . (int)$original_inventory . "', 
                                          `cost_price` = '" . (float)extract_number($cost_price) . "'");
                        $list_used[] = $store_id;
                    }
                }

                if (!empty($product_version_ids_api) && $data['product_version_ids_api']) {
                    // remove first
                    $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
                    foreach ($product_version_ids_api as $key => $pv_id) {
                        if (!isset($data['stores'][$key]) || !is_array($data['stores'][$key])) {
                            continue;
                        }

                        foreach ($data['stores'][$key] as $store_key => $store_id) {
                            $item_quantity = isset($data['original_inventory'][$key][$store_key]) ? (int)$data['original_inventory'][$key][$store_key] : 0;
                            $item_cost_price = isset($data['cost_price'][$key][$store_key]) ? (float)extract_number($data['cost_price'][$key][$store_key]) : 0;
                            $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store 
                                          SET `product_id` = '" . (int)$product_id . "', 
                                              `store_id` = '" . (int)$store_id . "',
                                              `product_version_id` = '" . (int)$pv_id . "', 
                                              `quantity` = '" . $item_quantity . "', 
                                              `cost_price` = '" . $item_cost_price . "'");
                        }
                    }
                }
            }
        }

        /* update seo */
        // IMPORTANT: if delete seo here, product may lost seo if no alias set!
        if (isset($data['alias'])) {
            // delete first. IMPORTANT: only delete if after this always re-creating new seo!
            $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url 
                          WHERE query = 'product_id=" . (int)$product_id . "'");

            // update (auto if seo alias empty)
            if (trim($data['alias']) == '') {
                $data['alias'] = $data['name'];
            }

            $this->load->model('custom/common');
            $slug = $this->model_custom_common->createSlug($data['alias'], '-');
            $slug_custom = $this->model_custom_common->getSlugUnique($slug);

            // create new seo product
            foreach ($languages as $key => $language) {
                $language_id = $language['language_id'];
                $alias = 'product_id=' . $product_id;
                $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                        SET language_id = '" . (int)$language_id . "', 
                            query = '" . $alias . "', 
                            keyword = '" . $slug_custom . "', 
                            table_name = 'product'";
                $this->db->query($sql);
            }
        }

        /* update product warehouse */
        // onlny if multi versions of product
        // TODO: remove warehouse
        /*if (!empty($product_version_ids)) {
            // remove old
            $this->db->query("DELETE FROM " . DB_PREFIX . "warehouse WHERE product_id = '" . (int)$product_id . "' AND product_version_id IS NOT NULL");

            // insert new
            foreach ($product_version_ids as $pv_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "warehouse SET product_id = '" . (int)$product_id . "', product_version_id = '" . (int)$pv_id . "'");
            }
        }*/

        /* update related order_products and order */
        $this->load->model('sale/order');
        $this->model_sale_order->updateRelatedOrderProducts($product_id);

        // update connection product and product_lazada
        $new_product = $this->getProductForLazada($product_id);
        $new_product_version = $this->getProductVersions($product_id);

        $this->load->model('catalog/lazada');
        $this->model_catalog_lazada->disconnectWhenProductChange($old_product, $new_product, $old_product_version, $new_product_version);

        $this->load->model('catalog/shopee');
        $this->model_catalog_shopee->disconnectWhenProductChange($old_product, $new_product, $old_product_version, $new_product_version);

        $this->deleteProductCaches();

        // send product to rabbit mq
        try {
            $this->sendToRabbitMq($product_id);
        } catch (Exception $exception) {
            $this->log->write('Send message edit catalog product error: ' . $exception->getMessage());
        }
    }

    private function getProductForLazada($product_id)
    {
        $sql = "SELECT `multi_versions`, `product_id`  
                  FROM `" . DB_PREFIX . "product` 
                      WHERE `product_id` = " . (int)$product_id . " 
                          AND `deleted` IS NULL";

        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getProductVersions($product_id, $product_version_id_as_key = false)
    {
        $product_versions_data = array();
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_version WHERE product_id = '" . (int)$product_id . "' AND deleted IS NULL");

        foreach ($query->rows as $result) {
            $product_versions_data[$result['product_version_id']] = [
                'version' => $result['version'],
                'price' => $result['price'],
                'compare_price' => $result['compare_price'],
                'sku' => $result['sku'],
                'barcode' => $result['barcode'],
                'quantity' => $result['quantity'],
                'status' => $result['status'],
                'product_version_id' => $result['product_version_id'],
                'image' => $result['image'],
                'image_alt' => $result['image_alt']
            ];
        }

        return $product_version_id_as_key ? $product_versions_data : array_values($product_versions_data);
    }

    public function getProductVersionIds($product_id)
    {
        $product_version_ids = [];
        $query = $this->db->query("SELECT product_version_id FROM " . DB_PREFIX . "product_version WHERE product_id = '" . (int)$product_id . "' AND deleted IS NULL");

        foreach ($query->rows as $result) {
            if (empty($result['product_version_id'])) {
                continue;
            }

            $product_version_ids[] = $result['product_version_id'];
        }

        return $product_version_ids;
    }

    private function deleteProductAttributes($product_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "DELETE FROM `{$DB_PREFIX}product_attribute` WHERE product_id = " . (int)$product_id;
        $this->db->query($sql);

        // delete attribute description
        $sql = "DELETE FROM `{$DB_PREFIX}attribute_description` WHERE `attribute_id` IN (SELECT `attribute_id` FROM `{$DB_PREFIX}product_attribute` WHERE product_id = '" . (int)$product_id . "')";
        $this->db->query($sql);

        // delete attribute
        $sql = "DELETE FROM `{$DB_PREFIX}attribute` WHERE `attribute_id` IN (SELECT `attribute_id` FROM `{$DB_PREFIX}product_attribute` WHERE product_id = '" . (int)$product_id . "')";
        $this->db->query($sql);

        $this->deleteProductCaches();
    }

    public function deleteProduct($product_id, $remove_from_db = false)
    {
        $this->sendToRabbitMqModifyProduct($product_id, 'delete');
        /* update attribute filter by attribute names */
        // get all product attributes
        $this->load->model('catalog/attribute');
        $attributes = $this->model_catalog_attribute->getAttributes(['product_id' => $product_id]);

        if ($remove_from_db) {
            $this->db->query("DELETE FROM `" . DB_PREFIX . "product` WHERE product_id = '" . (int)$product_id . "'");
            $this->db->query("DELETE FROM `" . DB_PREFIX . "product_version` WHERE product_id = '" . (int)$product_id . "'");
            // delete product attribute
            $this->deleteProductAttributes($product_id);
        } else {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET `deleted` = 1 WHERE product_id = '" . (int)$product_id . "'");
            $this->db->query("UPDATE `" . DB_PREFIX . "product_version` SET `deleted` = 1 WHERE product_id = '" . (int)$product_id . "'");
            // delete product attribute
            $this->model_catalog_attribute->removeUnusedProductAttributes($product_id);
        }
        // update attribute filters of product after deleting product attributes
        if (!empty($attributes)) {
            $this->load->model('catalog/attribute_filter');
            foreach ($attributes as $attribute) {
                $this->model_catalog_attribute_filter->updateAttributeFilterByName($attribute['name']);
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_recurring WHERE product_id = " . (int)$product_id);
        $this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'product_id=" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "coupon_product WHERE product_id = '" . (int)$product_id . "'");
        // delete in relation table
        $this->db->query("DELETE FROM " . DB_PREFIX . "novaon_relation_table WHERE child_id = '" . (int)$product_id . "' AND child_name = 'product'");

        // delete in warehouse table, include multi versions
        // TODO: remove warehouse
        // $this->db->query("DELETE FROM " . DB_PREFIX . "warehouse WHERE product_id = '" . (int)$product_id . "'");
        // NO NEED, we have soft-delete product, and query products already check product IS NOT NULL
        //$this->db->query("DELETE FROM " . DB_PREFIX . "warehouse WHERE product_id = '" . (int)$product_id . "'");

        // delete tag
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_tag WHERE product_id = '" . (int)$product_id . "'");

        // delete collection
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_collection WHERE product_id = '" . (int)$product_id . "'");

        $this->load->model('catalog/lazada');
        $this->model_catalog_lazada->disconnectWhenProductDeleted($product_id);

        $this->load->model('catalog/shopee');
        $this->model_catalog_shopee->disconnectWhenProductDeleted($product_id);

        $this->deleteProductCaches();
    }

    /**
     * delete product caches
     */
    private function deleteProductCaches() {
        $product_key_redis =  DB_PREFIX . 'product';
        $this->cache->delete($product_key_redis);
    }
    public function getCountVersionProduct($product_id)
    {
        $sql = "SELECT COUNT(pv.product_id) as total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_version pv ON (p.product_id = pv.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)  WHERE 1=1 AND pv.product_id = " . $product_id . " AND pd.language_id = " . (int)$this->config->get('config_language_id') . " AND pv.`deleted` IS NULL";
        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    /**
     * @param $product_id
     * @return array array of category names, NOT ARRAY OF CATEGORY OBJECTS
     */
    public function getProductCategoryName($product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
        $this->load->model('catalog/category');
        $results = $query->rows;
        $category = array();
        if (!empty($results)) {
            foreach ($results as $result) {
                $cat = $this->model_catalog_category->getCategory($result['category_id']);
                if (!isset($cat['name'])) {
                    continue;
                }

                $category[] = $cat['name'];
            }
        }

        return $category;
    }

    public function countProductInStoreDefault($product_id)
    {
        $sql = "SELECT SUM(pts.quantity) as qty FROM `" . DB_PREFIX . "product_to_store` pts LEFT JOIN `" . DB_PREFIX . "product` p ON p.`product_id` = pts.`product_id` WHERE pts.`store_id` = p.`default_store_id` AND pts.`product_id` = " . (int)$product_id;
        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {
            return (int)$query->row['qty'];
        }

        return 0;
    }

    /**
     * @param $sku
     * @return mixed
     */
    public function getProductOneVersionBySKU($sku)
    {
        $query = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "product WHERE sku = '" . $this->db->escape($sku) . "' AND multi_versions = 0");

        return $query->row;
    }

    /**
     * @param $skues
     * @return mixed
     */
    public function getProductMultiVersionByVersionSKUES($skues, $get_product_version_id = false)
    {
        $str_skues = "";
        foreach ($skues as $k => $sku) {
            if ($k != 0) {
                $str_skues = $str_skues . ", ";
            }
            $str_skues = $str_skues . "'$sku'";
        }

        $get_product_version_id_query = $get_product_version_id ? "`product_version_id`, `product_id`" : "DISTINCT `product_id`";

        $query = $this->db->query("SELECT {$get_product_version_id_query} FROM " . DB_PREFIX . "product_version WHERE sku IN (" . $str_skues . ")");

        return $query->rows;
    }

    /**
     * get product by id
     *
     * @param $product_id
     * @param bool|null $flag_deleted true=only deleted, false=only not deleted, null=all. Default null for all products
     * @param int|null $status 0=hidden, 1=visible, null=all statuses. Default null for all statuses
     * @return mixed
     */
    public function getProductBasic($product_id, $flag_deleted = false, $status = null)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT DISTINCT * FROM {$DB_PREFIX}product p 
                LEFT JOIN {$DB_PREFIX}product_description pd 
                       ON (p.product_id = pd.product_id) 
                WHERE p.product_id = '" . (int)$product_id . "' 
                  AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' ";

        if ($flag_deleted !== null) {
            $sql .= " AND p.`deleted` IS " . ($flag_deleted ? "NOT NULL" : "NULL");
        }

        if ($status !== null) {
            $sql .= " AND p.`status` = " . (int)$status;
        }

        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getCostPriceForProductByStore($store_id, $product_id, $product_version_id = 0)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_to_store` WHERE `store_id` = " . (int)$store_id . " AND `product_id` = " . (int)$product_id . " AND `product_version_id` = " . (int)$product_version_id);

        if (isset($query->row['cost_price'])) {
            return (float)$query->row['cost_price'];
        }

        return 0;
    }

    /**
     *
     * @param int|string $product_id
     * @return array
     */
    public function buildProductForRabbitMq($product_id)
    {
        try {
            $this->load->model('catalog/attribute');
            $this->load->model('catalog/manufacturer');
            $product = $this->getProductForRabbitMq($product_id);
            $record = [];

            if (isset($product['multi_versions']) && $product['multi_versions'] == 1) {
                $version_quantity = $this->getCountVersionProduct($product['product_id']);
            } else {
                $version_quantity = 0;
            }

            $category_names = $this->getProductCategoryName($product['product_id']);
            $category_name_product = is_array($category_names) ? implode(',', $category_names) : '';
            $name_cate = (!empty($category_name_product) ? $category_name_product : '');

            $product_versions = $this->getProductVersions($product['product_id']);
            foreach ($product_versions as &$version) {
                $version['version'] = implode(' • ', explode(',', $version['version']));
                $version['version_for_store'] = $this->replaceVersionValues($version['version']);

                $version['price'] = (int)str_replace(',', '', $version['price']);
                $version['compare_price'] = (int)str_replace(',', '', $version['compare_price']);
                $version['cost_price'] = $this->getCostPriceForProductByStore($product['default_store_id'], $product['product_id'], $version['product_version_id']);
                $version['quantity'] = $this->getQuantityForProductByStore($product['default_store_id'], $product['product_id'], $version['product_version_id']);
            }
            unset($version);

            // $description_tab
            $title_tab = [];
            $description_tab = [];
            $product_description_tabs = $this->getProductDescriptionTab($product['product_id']);
            foreach ($product_description_tabs as $product_description_tab) {
                $title_tab[] = html_entity_decode(trim($product_description_tab['title']));
                $description_tab[] = html_entity_decode($product_description_tab['description']);
            }

            $array_manufacturer = $this->model_catalog_manufacturer->getManufacturer($product['manufacturer_id']);
            $name_manufacturer = (isset($array_manufacturer['name']) ? $array_manufacturer['name'] : '');

            $product_cost_price = $this->getCostPriceForProductByStore($product['default_store_id'], $product['product_id']);
            $product_quantity_of_store = $this->getQuantityForProductByStore($product['default_store_id'], $product['product_id']);

            $record = [
                "id" => $product['product_id'],
                "name" => html_entity_decode($product['name']),
                "original_price" => $product['compare_price'],
                "sale_price" => $product['price'],
                "common_compare_price" => $product['common_compare_price'],
                "common_price" => $product['common_price'],
                "common_cost_price" => $product['common_cost_price'],
                "cost_price" => $product_cost_price,
                "image" => $product['image'],
                "quantity" => $product_quantity_of_store,
                "sku" => $product['multi_versions'] ? $product['common_sku'] : $product['sku'],
                "weight" => $product["weight"],
                "in_stock" => ($product["sale_on_out_of_stock"] == 1 || $product_quantity_of_store > 0) ? 1 : 0,
                "sale_on_out_of_stock" => $product["sale_on_out_of_stock"],
                "version_quantity" => $version_quantity,
                "version" => $product_versions,
                'nameCategory' => isset($name_cate) ? $name_cate : '',
                'manufacturer' => $name_manufacturer,
                'activeStatus' => $product['status'],
                'user_name' => $product['fullname'],
                'desc' => html_entity_decode($product['sub_description']),
                'content' => html_entity_decode($product['description']),
                'product_attributes' => [],
                'barcode' => $product['multi_versions'] ? $product['common_barcode'] : $product['barcode'],
                'seo_title' => isset($product['seo_title']) ? $product['seo_title'] : '',
                'seo_description' => isset($product['seo_description']) ? $product['seo_description'] : '',
                'meta_keyword' => isset($product['meta_keyword']) ? $product['meta_keyword'] : '',
                'alias' => html_entity_decode($product['name']),
                'title_tab' => $title_tab,
                'description_tab' => $description_tab,
            ];

            if (empty($query_params['product_version_id'])) {
                $product_attributes = $this->getProductAttributes($product['product_id']);
                foreach ($product_attributes as $product_attribute) {
                    $attribute_info = $this->model_catalog_attribute->getAttribute($product_attribute['attribute_id']);

                    if ($attribute_info) {
                        $record['product_attributes'][] = array(
                            'attribute_id' => $product_attribute['attribute_id'],
                            'name' => $attribute_info['name'],
                            'detail' => explode(",", $product_attribute['product_attribute_description']['text'])
                        );
                    }
                }
            }

            return $record;
        } catch (Exception $e) {
            return [
                'code' => 500,
                'message' => $e->getMessage()
            ];
        }
    }

    public function sendToRabbitMq($product_id)
    {
        try {
            if (!empty($product_id)) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'product' => []
                ];

                $product = $this->buildProductForRabbitMq($product_id);
                if (isset($product['code']) && $product['code'] == 500) {
                    $this->log->write('Build product for rabbit_mq error: ' . json_encode($product));
                    return;
                }

                $message['product'] = $product;

                $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'product', '', 'product', json_encode($message));
            }
        } catch (Exception $exception) {
            $this->log->write('Send message product error: ' . $exception->getMessage());
        }
    }

    public function getQuantityForProductByStore($store_id, $product_id, $product_version_id = 0)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_to_store` WHERE `store_id` = " . (int)$store_id . " AND `product_id` = " . (int)$product_id . " AND `product_version_id` = " . (int)$product_version_id);

        if (isset($query->row['quantity'])) {
            return ((float)$query->row['quantity'] > 0) ? (float)$query->row['quantity'] : 0;
        }

        return 0;
    }

    public function getProductForRabbitMq($product_id, $flag_deleted = false, $status = null)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT DISTINCT p.*, pd.*, CONCAT(u.lastname, ' ', u.firstname) as fullname FROM {$DB_PREFIX}product p  
                LEFT JOIN {$DB_PREFIX}product_description pd 
                       ON (p.product_id = pd.product_id) 
                LEFT JOIN {$DB_PREFIX}user u ON (u.user_id = p.user_create_id) 
                WHERE p.product_id = '" . (int)$product_id . "' 
                  AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' ";

        if ($flag_deleted !== null) {
            $sql .= " AND p.`deleted` IS " . ($flag_deleted ? "NOT NULL" : "NULL");
        }

        if ($status !== null) {
            $sql .= " AND p.`status` = " . (int)$status;
        }

        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getProductIdBySku($product_sku)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT `product_id` FROM `{$DB_PREFIX}product` 
                        WHERE sku = '" . $this->db->escape($product_sku) . "' 
                        OR common_sku = '" . $this->db->escape($product_sku) . "'";

        $query = $this->db->query($sql);

        if (isset($query->row['product_id'])) {
            return $query->row['product_id'];
        }

        return 0;
    }

    public function getProductBySku($product_sku)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}product_version` 
                        WHERE sku = '" . $this->db->escape($product_sku) . "' 
                        AND deleted is null";
        $query = $this->db->query($sql);
        if (!empty($query->row) && !empty($query->row['product_version_id'])) {
            $product_discount = $this->getProductVersionById($query->row['product_id'], $query->row['product_version_id']);
            $product = $this->getProduct($query->row['product_id']);
            $product['price_2'] = $product_discount['price'];
            $product['compare_price_2'] = $product_discount['compare_price'];
            $product['price'] = $query->row['price'];
            $product['compare_price'] = $query->row['compare_price'];
            $product['version'] = $query->row['version'];
        }
        else {
            $sql = "SELECT * FROM `{$DB_PREFIX}product` 
                        WHERE sku = '" . $this->db->escape($product_sku) . "' 
                        AND deleted is null";
            $query = $this->db->query($sql);
            if (isset($query->row['product_id'])) {
                $product = $this->getProduct($query->row['product_id']);
            }
        }

        if (isset($product)) {
            return $product;
        }
    }

    public function getDataAddOnDealProductById($product_id, $product_version_id = 0)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT aop.*, p.user_create_id, 
                            p.sku as p_sku, p.multi_versions, 
                            p.compare_price as p_compare_price, 
                            p.weight as weight, 
                            p.tax_class_id, aod.limit_quantity_product,  
                            aop.limit_quantity,  
                            pd.name, p.image, 
                            pts.quantity as pts_quantity, 
                            pv.version, pv.compare_price as pv_compare_price, 
                            pv.sku as pv_sku  
                            FROM `{$DB_PREFIX}add_on_product` as aop 
                                JOIN `{$DB_PREFIX}add_on_deal` as aod ON (aop.add_on_deal_id = aod.add_on_deal_id) 
                                INNER JOIN `{$DB_PREFIX}product` as p ON (p.product_id = aop.product_id) 
                                INNER JOIN `{$DB_PREFIX}product_description` as pd ON (pd.product_id = aop.product_id) 
                                LEFT JOIN `{$DB_PREFIX}product_version` as pv ON (pv.product_id = aop.product_id AND 
                                                                        CASE WHEN aop.product_version_id <> 0 
                                                                            THEN aop.product_version_id ELSE -1 END = pv.product_version_id) 
                                INNER JOIN `{$DB_PREFIX}product_to_store` as pts ON (aop.product_id = pts.product_id AND 
                                                                        CASE WHEN aop.product_version_id <> 0 
                                                                            THEN aop.product_version_id ELSE 0 END = pts.product_version_id) 
                            WHERE aop.product_id = {$product_id} 
                                AND aop.product_version_id = {$product_version_id}
                                AND aop.status = 1  
                                AND aod.date_started <= NOW() 
                                AND aod.date_ended >= NOW() 
                                AND aod.status = 1 
                                AND aod.deleted IS NULL";

        $query = $this->db->query($sql);
        return $query->row;
    }

    public function sendToRabbitMqModifyProduct($product_id, $action = 'delete')
    {
        try {
            if (!empty($product_id)) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'product_sku' => '',
                    'action' => $action
                ];

                $infoProduct = $this->getProductForRabbitMq($product_id);
                $sku = !empty($infoProduct['common_sku']) ? $infoProduct['common_sku'] : $infoProduct['sku'];

                if (!empty($sku)) {
                    $message['product_sku'] = $sku;

                    $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'modify_product', '', 'modify_product', json_encode($message));
                }
            }
        } catch (Exception $exception) {
            $this->log->write('Send message delete product error: ' . $exception->getMessage());
        }
    }
}
