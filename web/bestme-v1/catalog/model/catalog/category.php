<?php
class ModelCatalogCategory extends Model {
    use RabbitMq_Trait;
    
	public function getCategory($category_id) {
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getCategory_' . md5($sql);
        $category = $this->cache->get($cache_key);
        if ($category) {
            return $category;
        }

        // get result from db
        $query = $this->db->query($sql);
        $category = $query->row;
        $this->cache->set($cache_key, $category);

		return $category;
	}

    public function getMultiSettingValue($keys, $store_id = 0)
    {
        $keys = "'" . implode("','", $keys) . "'";
        $sql = "SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` IN (" . $keys . ")";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getMultiSettingValue_' . md5($sql);
        $setting_value = $this->cache->get($cache_key);
        if ($setting_value) {
            return $setting_value;
        }

        // get result from db
        $query = $this->db->query($sql);
        $setting_value = $query->rows;
        $this->cache->set($cache_key, $setting_value);

        return $setting_value;
    }

	public function getCategories($parent_id = 0) {
		$sql = "SELECT * FROM " . DB_PREFIX . "category c 
		                           LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) 
		                           LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) 
		                           WHERE c.parent_id = '" . (int)$parent_id . "' 
		                           AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
		                           AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  
		                           AND c.status = '1' 
		                           ORDER BY c.sort_order = 0, c.sort_order, LCASE(cd.name)";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getCategories_' . md5($sql);
        $categories = $this->cache->get($cache_key);
        if ($categories) {
            return $categories;
        }

        // get result from db
        $query = $this->db->query($sql);
        $categories = $query->rows;
        $this->cache->set($cache_key, $categories);

        return $categories;
	}

    /**
     * @param int|array $parent_id one or array of category id
     * @param int $deep
     * @return array
     */
    public function getCategoriesRecursive($parent_id = 0, $deep = 3)
    {
        if (!is_array($parent_id)) {
            $parent_id = [(int)$parent_id];
        }

        if (count($parent_id) < 1) {
            return [];
        }

        $sql = "SELECT * FROM " . DB_PREFIX . "category c 
		                           LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) 
		                           LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) 
		                           WHERE c.parent_id IN (" . implode(',', $parent_id) . ") 
		                           AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
		                           AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  
		                           AND c.status = '1' 
		                           ORDER BY c.sort_order = 0, c.sort_order, LCASE(cd.name)";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getCategoriesRecursive_' . md5($sql);
        $result = $this->cache->get($cache_key);
        if (!$result) {
            // get result from db
            $query = $this->db->query($sql);
            $result = $query->rows;
            $this->cache->set($cache_key, $result);
        }

        if ($deep > 1) {
            $more_parent_ids = array_map(function ($item) {
                return $item['category_id'];

            }, $result);

            $result = array_merge($result, $this->getCategoriesRecursive($more_parent_ids, $deep - 1));
        }

        return $result;
    }

    /**
     * @param int|array $parent_id one or array of category id
     * @param int $deep
     * @return array
     */
    public function getCategoryIdsRecursive($parent_id = 0, $deep = 3)
    {
        $result = $this->getCategoriesRecursive($parent_id, $deep);

        $result = array_map(function ($item) {
            return $item['category_id'];

        }, $result);

        return $result;
    }

    /**
     * @param int $parent_id
     * @param int $deep
     * @return array categories array with additional key "sub_categories"
     */
    public function getCategoriesTree($parent_id = 0, $deep = 3)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "category c 
		                           LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) 
		                           LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) 
		                           WHERE c.parent_id = '" . (int)$parent_id . "' 
		                           AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
		                           AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  
		                           AND c.status = '1' 
		                           ORDER BY c.sort_order = 0, c.sort_order, LCASE(cd.name)";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getCategoriesTree_' . md5($sql);
        $result = $this->cache->get($cache_key);
        if (!$result) {
            // get result from db
            $query = $this->db->query($sql);
            $result = $query->rows;
            $this->cache->set($cache_key, $result);
        }

        if ($deep > 1) {
            foreach ($result as &$item) {
                $item['sub_categories'] = $this->getCategoriesTree($item['category_id'], $deep - 1);
            }
            unset($item);
        }

        return $result;
    }

	public function getAllCategories() {
		$sql = "SELECT * FROM " . DB_PREFIX . "category c 
		                           LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) 
		                           LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) 
		                           WHERE 1=1 
		                           AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
		                           AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  
		                           AND c.status = '1' 
		                           ORDER BY c.sort_order = 0, c.sort_order, cd.name COLLATE utf8_unicode_ci;"; // sort alphabet, supported Utf-8

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getAllCategories_' . md5($sql);
        $categories = $this->cache->get($cache_key);
        if ($categories) {
            return $categories;
        }

        // get result from db
        $query = $this->db->query($sql);
        $categories = $query->rows;
        $this->cache->set($cache_key, $categories);

        return $categories;
	}

    public function getFullChildCategories($data, $parentId = 0)
    {
        $result = [];
        $sql = "SELECT * FROM `" . DB_PREFIX . "category` c LEFT JOIN `" . DB_PREFIX . "category_description` cd ON (c.category_id = cd.category_id) 
                                            WHERE c.`parent_id` = " . (int)$parentId . " AND cd.`language_id` = " . (int)$this->config->get('config_language_id');

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " ORDER BY c.`date_added` DESC";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getFullChildCategories_' . md5($sql);
        $categories = $this->cache->get($cache_key);
        if (!$categories) {
            // get result from db
            $query = $this->db->query($sql);
            $categories = $query->rows;
            $this->cache->set($cache_key, $categories);
        }

        foreach ($categories as $row) {
            $children = $this->getFullChildCategories([], $row['category_id']);
            $sub_ids = [];
            foreach ($children as $child) {
                $sub_ids[] = $child['id'];
                if (isset($child['sub_ids']) && is_array($child['sub_ids'])){
                    $sub_ids = array_merge($sub_ids, $child['sub_ids']);
                }
            }

            $result[] = [
                'id' => $row['category_id'],
                'text' => html_entity_decode($row['name']), // return a&b instead of a&amp;b
                'hasChildren' => count($children) > 0 ? true : false,
                'sub_ids' => $sub_ids,
                'children' => $children
            ];
        }

        return $result;
    }

    public function getFullChildCategoryIds($parentId)
    {
        if (empty($parentId)) {
            return [];
        }

        $allChildren = $this->getFullChildCategories($data = [], $parentId);
        if (!is_array($allChildren)) {
            return [];
        }

        $result = [];
        foreach ($allChildren as $child) {
            if (!isset($child['id'])) {
                continue;
            }

            $result[] = $child['id'];
            if (isset($child['sub_ids']) && is_array($child['sub_ids'])) {
                $result = array_merge($result, $child['sub_ids']);
            }
        }

        return $result;
    }

    public function getCategoriesHadProduct()
    {
        // Temp return all categories. TODO: correct...
        return $this->getAllCategories();

        $sql = "SELECT * FROM " . DB_PREFIX . "category c 
                LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) 
                LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) 
                LEFT JOIN " . DB_PREFIX . "product_to_category ptc ON (c.category_id = ptc.category_id) 
                WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
                  AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  
                  AND c.status = '1' 
                  AND ptc.product_id IS NOT NULL 
                  GROUP BY c.category_id  
                  ORDER BY c.sort_order, LCASE(cd.name)";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getFullCategoriesForColumnLeft($filter_data, $parentId = 0, $level = 0)
    {
        $result = [];

        if (!isset($filter_data['filter_name'])) {
            $filter_data['filter_name'] = '';
        } else {
            $filter_data['filter_name'] = trim($filter_data['filter_name'], ' ');
        }

        $filter_name_1 = html_entity_decode($filter_data['filter_name']); // support new value a&b
        $filter_name_2 = htmlentities($filter_name_1); // support old value a&amp;b

        $sql = "SELECT *, IF(cd.`name` LIKE '%" . $this->db->escape($filter_name_1) . "%' OR cd.`name` LIKE '%" . $this->db->escape($filter_name_2) . "%', 'YES', 'NO') AS `filter_name` 
                      FROM `" . DB_PREFIX . "category` c 
                      LEFT JOIN `" . DB_PREFIX . "category_description` cd ON (c.category_id = cd.category_id) 
                      WHERE c.`parent_id` = " . (int)$parentId . " 
                      AND cd.`language_id` = " . (int)$this->config->get('config_language_id');

        if (!empty($filter_data['filter_status'])) {
            $sql .= " AND c.`status` = '1'";
        }

        $sql .= " ORDER BY c.`sort_order` = 0, c.`sort_order` ASC, cd.`name` ASC";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getFullCategoriesForColumnLeft_' . md5($sql);
        $categories = $this->cache->get($cache_key);
        if (!$categories) {
            // get result from db
            $query = $this->db->query($sql);
            $categories = $query->rows;
            $this->cache->set($cache_key, $categories);
        }

        foreach ($categories as $row) {
            $children = $this->getFullCategoriesForColumnLeft($filter_data, $row['category_id'], $level + 1);
            $sub_ids = [];
            foreach ($children as $child) {
                $sub_ids[] = $child['id'];
                if (isset($child['sub_ids']) && is_array($child['sub_ids'])){
                    $sub_ids = array_merge($sub_ids, $child['sub_ids']);
                }
            }

            $result[] = [
                'id' => $row['category_id'],
                'category_id' => $row['category_id'],
                'name' => html_entity_decode($row['name']), // return a&b instead of a&amp;b
                'image' => $row['image'],
                'hasChildren' => count($children) > 0 ? true : false,
                'sub_ids' => $sub_ids,
                'list_sub' => $children,
                'filter_name' => $row['filter_name'],
                'level' => $level,
                'parent_id' => $parentId,
                'sort_order' => $row['sort_order'],
                'source' => $row['source'],
                'url' => str_replace(' ', '', $this->url->link('common/shop', '&path=' . $row['category_id'], true)),
            ];
        }

        return $result;
    }

	public function getCategoryFilters($category_id) {
		$implode = array();

		$query = $this->db->query("SELECT filter_id FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$implode[] = (int)$result['filter_id'];
		}

		$filter_group_data = array();

		if ($implode) {
			$filter_group_query = $this->db->query("SELECT DISTINCT f.filter_group_id, fgd.name, fg.sort_order FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_group fg ON (f.filter_group_id = fg.filter_group_id) LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND fgd.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY f.filter_group_id ORDER BY fg.sort_order, LCASE(fgd.name)");

			foreach ($filter_group_query->rows as $filter_group) {
				$filter_data = array();

				$filter_query = $this->db->query("SELECT DISTINCT f.filter_id, fd.name FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND f.filter_group_id = '" . (int)$filter_group['filter_group_id'] . "' AND fd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY f.sort_order, LCASE(fd.name)");

				foreach ($filter_query->rows as $filter) {
					$filter_data[] = array(
						'filter_id' => $filter['filter_id'],
						'name'      => $filter['name']
					);
				}

				if ($filter_data) {
					$filter_group_data[] = array(
						'filter_group_id' => $filter_group['filter_group_id'],
						'name'            => $filter_group['name'],
						'filter'          => $filter_data
					);
				}
			}
		}

		return $filter_group_data;
	}

	public function getCategoryLayoutId($category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return (int)$query->row['layout_id'];
		} else {
			return 0;
		}
	}

	public function getTotalCategoriesByCategoryId($parent_id = 0) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");

		return $query->row['total'];
	}

    public function getCategoriesIdByProductId($product_id)
    {
        if (!$product_id) {
            return [];
        }
        $sql = "SELECT * FROM " . DB_PREFIX . "product_to_category as ptc LEFT JOIN " . DB_PREFIX . "category_description as cd ON cd.category_id = ptc.category_id WHERE product_id = '" . (int)$product_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getCategoriesIdByProductId_' . md5($sql);
        $categories = $this->cache->get($cache_key);
        if (!$categories) {
            // get result from db
            $query = $this->db->query($sql);
            $categories = $query->rows;
            $this->cache->set($cache_key, $categories);
        }

        $list_categories = [];
        foreach ($categories as $row) {
            $parent_categories = $this->getAllParentsCategory($row['category_id']);
            foreach ($parent_categories as $k => $category){
                $list_categories[$k] = $category;
            }
        }

        $result = [];
        foreach ($list_categories as $category){
            $result[] = $category['category_id'];
        }

        return $result;
    }

    private function getAllParentsCategory($category_id)
    {
        $result = [];
        $sql = "SELECT * FROM `" . DB_PREFIX . "category` as c LEFT JOIN `" . DB_PREFIX . "category_description` as cd ON c.`category_id` = cd.`category_id` WHERE c.`category_id` = " . (int)$category_id;

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getAllParentsCategory_' . md5($sql);
        $category = $this->cache->get($cache_key);
        if (!$category) {
            // get result from db
            $query = $this->db->query($sql);
            $category = $query->row;
            $this->cache->set($cache_key, $category);
        }

        if (empty($category)){
            return $result;
        }
        $result[$category['category_id']] = $category;
        if ($category['parent_id'] != 0){
            $parents = $this->getAllParentsCategory($category['parent_id']);
            foreach ($parents as $k => $v){
                $result[$k] = $v;
            }
        }

        return $result;
    }

    public function getNameRecordById($categoryId, $tableName, $name = null)
    {
        $language_id = $this->config->get('config_language_id');
        $sql = "SELECT * FROM " . DB_PREFIX . $tableName . "_description WHERE category_id = '" . (int)$categoryId . "' AND language_id = '" . $language_id . "'";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getNameRecordById_' . md5($sql);
        $category = $this->cache->get($cache_key);
        if (!$category) {
            // get result from db
            $query = $this->db->query($sql);
            $category = $query->row;
            $this->cache->set($cache_key, $category);
        }

        if ($name) {
            if ($category[$name]) {
                return $category[$name];
            }
            return null;
        }
        if ($category['name']) {
            return $category['name'];
        }
        return null;
    }

    public function getCategoriesForClassify($filter_data)
    {
        $result = $this->getFullCategoriesNotFilterClassify($filter_data);

        $result_filter =  $this->getCategoryContainFilterNameClassify($result);

        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : 5;
        if (isset($filter_data['page'])) {
            $start = ($filter_data['page'] - 1) * $limit;
            $result_filter = array_slice($result_filter, $start, $limit);
        }

        return $result_filter;
    }

    public function getCategoriesByNames(array $cat_names = [])
    {
        if (empty($cat_names)) {
            return [];
        }

        $cat_names_standardize = array_map(function ($cat_name) {
            return sprintf("'%s'", $this->db->escape(mb_strtoupper($cat_name)));
        }, $cat_names);

        $cat_names_sql = implode(',', $cat_names_standardize);

        $sql = "SELECT * FROM " . DB_PREFIX . "category_description 
                WHERE 1=1 
                  AND language_id = '" . (int)$this->config->get('config_language_id') . "' 
                  AND UPPER(`name`) IN ({$cat_names_sql})";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getCategoryIdByName($category_name){
        $sql = "SELECT * FROM ". DB_PREFIX . "category_description WHERE 1=1 AND language_id = '". (int)$this->config->get('config_language_id') ."'";

        $name_search = mb_strtoupper($category_name);
        $sql .= " AND hex(UPPER(name)) = hex('". $this->db->escape($name_search) ."')";
        $query = $this->db->query($sql);

        if (isset($query->row['category_id'])) {
            return $query->row['category_id'];
        }
        return 0;
    }

    /**
     * @param $filter
     * @return mixed
     */
    public function getCategoryByName($filter) {
        $sql = "SELECT * FROM ". DB_PREFIX . "category_description WHERE 1=1 AND language_id = '". (int)$this->config->get('config_language_id') ."'";
        if (isset($filter['name'])) {
            $name_search = mb_strtoupper($filter['name']);
            $sql .= " AND hex(UPPER(name)) = hex('". $this->db->escape($name_search) ."')";
        }
        if (isset($filter['id']) && $filter['id'] != '') {
            $sql .= " AND category_id != '". (int)$filter['id'] ."'";
        }
        $query = $this->db->query($sql);
        return $query->row;
    }

    public function addCategoryFast($name, $source = '')
    {
        $name = htmlspecialchars_decode($name);
        $data = [
            "parent_id" => 0,
            "status" => 1,
            "category_description" => trim($name),
            "name" => trim($name),
            "source" => trim($source),
        ];
        $this->db->query("INSERT INTO " . DB_PREFIX . "category SET parent_id = '" . (int)$data['parent_id'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

        $category_id = $this->db->getLastId();
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        if (isset($data['category_description']) && isset($data['name'])) {
            // add with all languages
            foreach ($languages as $language) {
                $language_id = $language['language_id'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['category_description']) . "'");
            }
        }
        //tung
        //thêm mới product-choose
        if (isset($data['product-choose'])) {
            foreach ($data['product-choose'] as $key => $product_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
            }
        }
        //them mới menu
        if (isset($data['menu'])) {
            foreach ($data['menu'] as $menu) {
                $result = explode('-', $menu);
                $menuName = $result[0];
                $menuId = $result[1];
                $this->db->query("
                    INSERT INTO " . DB_PREFIX . "novaon_relation_table SET main_name = '" . $menuName . "', main_id = '" . $menuId . "', child_name = 'category', child_id = '" . $category_id . "', type_id = 2, redirect = 0
                ");
            }
        }
        //save into path and store
        $level = 0;

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

            $level++;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', `level` = '" . (int)$level . "'");

        if (isset($data['category_filter'])) {
            foreach ($data['category_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        // if (isset($data['category_store'])) {
        //     foreach ($data['category_store'] as $store_id) {
        $store_id = 0;
        $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
        //     }
        // }

        // create seo
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        $category_alias = $data['name'];

        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($category_alias, '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);
        foreach ($languages as $key => $language) {
            $language_id = $language['language_id'];
            $alias = 'category_id=' . $category_id;
            $sql = "INSERT INTO " . DB_PREFIX . "seo_url SET language_id = '" . (int)$language_id . "', query = '" . $alias . "', keyword = '" . $slug_custom . "', table_name = 'category'";
            $this->db->query($sql);
        }

        //$this->cache->delete('category');
        if (!empty($data['source'])) {
            $this->updateSourceCategory($category_id, $data['source']);
        }

        $this->deleteProductCaches();

        // send product to rabbit mq
        try {
            $this->sendToRabbitMq($category_id, $this->db->escape(trim($data['name'])));
        } catch (Exception $exception) {
            $this->log->write('Send message add category error: ' . $exception->getMessage());
        }

        return $category_id;
    }


    public function updateCategoryFast($category_id, $name, $source = '')
    {
        $name = htmlspecialchars_decode($name);
        $data = [
            "category_description" => trim($name),
            "name" => trim($name),
            "source" => trim($source),
        ];

        // get old_name
        $result = $this->getCategory($category_id);
        $old_name = isset($result['name']) ? $result['name'] : '';

        $this->db->query("UPDATE " . DB_PREFIX . "category 
                          SET date_modified = NOW() 
                          WHERE category_id = '" . (int)$category_id . "'");


        $this->db->query("DELETE FROM " . DB_PREFIX . "category_description 
                          WHERE category_id = '" . (int)$category_id . "'");

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        if (isset($data['category_description']) && isset($data['name'])) {
            // add with all languages
            foreach ($languages as $language) {
                $language_id = $language['language_id'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_description 
                                  SET category_id = '" . (int)$category_id . "', 
                                  language_id = '" . (int)$language_id . "', 
                                  `name` = '" . $this->db->escape(trim($data['name'])) . "', 
                                  description = '" . $this->db->escape(trim($data['category_description'])) . "'");
            }
        }

        //seo
        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($data['name'], '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);
        //delete seo product
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'category_id=" . (int)$category_id . "'");
        //create new seo product
        foreach ($languages as $key => $language) {
            $language_id = $language['language_id'];
            $alias = 'category_id=' . $category_id;
            $sql = "INSERT INTO " . DB_PREFIX . "seo_url SET language_id = '" . (int)$language_id . "', query = '" . $alias . "', keyword = '" . $slug_custom . "', table_name = 'category'
            ";
            $this->db->query($sql);
        }

        if (!empty($data['source'])) {
            $this->updateSourceCategory($category_id, $data['source']);
        }

        $this->deleteProductCaches();

        // send product to rabbit mq
        if ($old_name != $data['name']) {
            try {
                $this->sendToRabbitMq($category_id, $this->db->escape(trim($data['name'])), $this->db->escape($old_name));
            } catch (Exception $exception) {
                $this->log->write('Send message update category error: ' . $exception->getMessage());
            }
        }

        return $category_id;
    }

    public function updateSourceCategory($category_id, $source = null)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "category SET 
                                   `source` = '" . $this->db->escape($source) . "' 
                                    WHERE category_id = '" . (int)$category_id . "'");

        $this->deleteProductCaches();
    }

    private function getCategoryContainFilterNameClassify($tree)
    {
        $result_filter = [];
        foreach ($tree as $k => $item) {
            if ($item['filter_name'] == 'YES'){
                $result_filter[] = $item;
            }else{
                $result_filter = array_merge($result_filter, $this->getCategoryContainFilterNameClassify($item['children']));
            }
        }

        return $result_filter;
    }

    private function getFullCategoriesNotFilterClassify($filter_data, $parentId = 0)
    {
        $result = [];

        if (!isset($filter_data['filter_name'])) {
            $filter_data['filter_name'] = '';
        } else {
            $filter_data['filter_name'] = trim($filter_data['filter_name'], ' ');
        }

        $sql = "SELECT *, IF(cd.`name` LIKE '%" . $filter_data['filter_name'] . "%', 'YES', 'NO') AS `filter_name` 
                      FROM `" . DB_PREFIX . "category` c 
                      LEFT JOIN `" . DB_PREFIX . "category_description` cd ON (c.category_id = cd.category_id) 
                      WHERE c.`parent_id` = " . (int)$parentId . " 
                      AND cd.`language_id` = " . (int)$this->config->get('config_language_id');

        $sql .= " ORDER BY c.`date_modified` DESC";

        $query = $this->db->query($sql);

        foreach ($query->rows as $row) {
            $children = $this->getFullCategoriesNotFilterClassify($filter_data, $row['category_id']);
            $sub_ids = [];
            foreach ($children as $child) {
                $sub_ids[] = $child['id'];
                if (isset($child['sub_ids']) && is_array($child['sub_ids'])) {
                    $sub_ids = array_merge($sub_ids, $child['sub_ids']);
                }
            }

            $result[] = [
                'id' => $row['category_id'],
                'parent_id' => $row['parent_id'],
                'text' => $row['name'],
                'name' => $row['name'],
                'description' => $row['description'],
                'sort_order' => $row['sort_order'],
                'total_product' => $this->getTotalProductByCategoryId($row['category_id']),
                'hasChildren' => count($children) > 0 ? true : false,
                'sub_ids' => $sub_ids,
                'children' => $children,
                'image' => $row['image'],
                'filter_name' => $row['filter_name']
            ];
        }

        return $result;
    }

    //v3.5.1 open api
    public function getTotalProductByCategoryId($category_id)
    {
        $sql = "SELECT COUNT(*) as count_product FROM `" . DB_PREFIX . "product_to_category` ptc
                                            INNER JOIN `" . DB_PREFIX . "product` p ON (p.`product_id` = ptc.`product_id`)
                                            WHERE ptc.`category_id` = ".(int)$category_id."   
                                            AND p.`deleted` is NULL";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getTotalProductByCategoryId_' . md5($sql);
        $category = $this->cache->get($cache_key);
        if (!$category) {
            // get result from db
            $query = $this->db->query($sql);
            $category = $query->row;
            $this->cache->set($cache_key, $category);
        }

        if (isset($category['count_product'])) {
            return (int)$category['count_product'];
        }

        return 0;
    }

    private function getFullCategoriesNotFilterClassifyss($filter_data, $parentId = 0)
    {
        $result = [];

        if (!isset($filter_data['filter_name'])) {
            $filter_data['filter_name'] = '';
        } else {
            $filter_data['filter_name'] = trim($filter_data['filter_name'], ' ');
        }

        $sql = "SELECT *, IF(cd.`name` LIKE '%" . $filter_data['filter_name'] . "%', 'YES', 'NO') AS `filter_name` 
                      FROM `" . DB_PREFIX . "category` c 
                      LEFT JOIN `" . DB_PREFIX . "category_description` cd ON (c.category_id = cd.category_id) 
                      WHERE c.`parent_id` = " . (int)$parentId . " 
                      AND cd.`language_id` = " . (int)$this->config->get('config_language_id');

        $sql .= " ORDER BY c.`date_modified` DESC";

        $query = $this->db->query($sql);

        foreach ($query->rows as $row) {
            $children = $this->getFullCategoriesNotFilterClassify($filter_data, $row['category_id']);
            $sub_ids = [];
            foreach ($children as $child) {
                $sub_ids[] = $child['id'];
                if (isset($child['sub_ids']) && is_array($child['sub_ids'])) {
                    $sub_ids = array_merge($sub_ids, $child['sub_ids']);
                }
            }

            $result[] = [
                'id' => $row['category_id'],
                'parent_id' => $row['parent_id'],
                'text' => $row['name'],
                'name' => $row['name'],
                'sort_order' => $row['sort_order'],
                'total_product' => $this->getTotalProductByCategoryId($row['category_id']),
                'description' => $row['description'],
                'hasChildren' => count($children) > 0 ? true : false,
//                'sub_ids' => $sub_ids,
                'children' => $children,
                'image' => $row['image'],
                'filter_name' => $row['filter_name']
            ];
        }

        return $result;
    }
    /**
     * @param $category_id
     * @return mixed
     */
    public function getCategoryCustom($category_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");

        $row =  $query->row;

        $children = $this->getFullCategoriesNotFilterClassifyss(null, $row['category_id']);
        $sub_ids = [];
        foreach ($children as $child) {
            $sub_ids[] = $child['id'];
            if (isset($child['sub_ids']) && is_array($child['sub_ids'])) {
                $sub_ids = array_merge($sub_ids, $child['sub_ids']);
            }
        }

        $result[] = [
            'id' => $row['category_id'],
            'parent_id' => $row['parent_id'],
            'text' => $row['name'],
            'name' => $row['name'],
            'sort_order' => $row['sort_order'],
            'description' => $row['description'],
            'image' => $row['image'],
            'hasChildren' => count($children) > 0 ? true : false,
//            'sub_ids' => $children['sub_ids'],
            'children' => $children,
        ];
        return $result;
    }

    /**
     * @param $filter_data
     * @return array
     */
    public function getCategoriesForClassifyss($filter_data)
    {
        $result = $this->getFullCategoriesNotFilterClassifyss($filter_data);

        $result_filter =  $this->getCategoryContainFilterNameClassify($result);

        $limit = isset($filter_data['limit']) ? $filter_data['limit'] : 5;
        $filter_data['page'] = isset($filter_data['page']) ? $filter_data['page'] : 1;
        if (isset($filter_data['page'])) {
            $start = ($filter_data['page'] - 1) * $limit;
            $result_filter = array_slice($result_filter, $start, $limit);
        }
        $result = [];
        foreach ($result_filter as $value) {
            $result[] = [
                "id" => $value['id'],
                "parent_id" => $value['parent_id'],
                "name" => $value['name'],
                "sort_order" => $value['sort_order'],
                "total_product" => $this->getTotalProductByCategoryId($value['id']),
                "description" => $value['description'],
                "hasChildren" => $value['hasChildren'],
                "children" => $value['children'],
            ];
            $total = $this->getTotalCategories($filter_data, $value['parent_id']);
        }
        return [
            'total' => $total,
            'page' => $filter_data['page'],
            'records' => $result
        ];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function addCategory($data)
    {
        /*
         * $data sample:
         *
         * [
         *    "parent_id" => 0,
         *    "status" => 1,
         *    "category_description" => $name,
         *    "name" => $name,
         *    "image" => '',
         *    "sub_categories" => [
         *        "cat-1",
         *        "cat-2"
         *    ],
         * ];
         */

        /* TODO: check if category existed... */

        /* Create new category */
        $cateogry_image = isset($data['image']) ? trim($data['image']) : '';
        $this->db->query("INSERT INTO " . DB_PREFIX . "category 
                          SET parent_id = '" . (int)$data['parent_id'] . "', 
                          status = '1', 
                          image = '" . $this->db->escape($cateogry_image) . "', 
                          date_modified = NOW(), 
                          date_added = NOW()");

        $category_id = $this->db->getLastId();
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        if (isset($data['category_description']) && isset($data['name'])) {
            // add with all languages
            foreach ($languages as $language) {
                $language_id = $language['language_id'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_description 
                                  SET category_id = '" . (int)$category_id . "', 
                                  language_id = '" . (int)$language_id . "', 
                                  `name` = '" . $this->db->escape(trim($data['name'])) . "', 
                                  description = '" . $this->db->escape(trim($data['name'])) . "'");
            }
        }

        // add product_to_category
        if (isset($data['product-choose'])) {
            foreach ($data['product-choose'] as $key => $product_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category 
                                  SET product_id = '" . (int)$product_id . "', 
                                  category_id = '" . (int)$category_id . "'");
            }
        }

        /* category_path */
        $level = 0;

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` 
                                   WHERE category_id = '" . (int)$data['parent_id'] . "' 
                                   ORDER BY `level` ASC");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` 
                              SET `category_id` = '" . (int)$category_id . "', 
                              `path_id` = '" . (int)$result['path_id'] . "', 
                              `level` = '" . (int)$level . "'");

            $level++;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` 
                          SET `category_id` = '" . (int)$category_id . "', 
                          `path_id` = '" . (int)$category_id . "', 
                          `level` = '" . (int)$level . "'");

        if (isset($data['category_filter'])) {
            foreach ($data['category_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter 
                                  SET category_id = '" . (int)$category_id . "', 
                                  filter_id = '" . (int)$filter_id . "'");
            }
        }

        /*
         * add category_to_store
         * TODO: support multi stores?...
         */
        $store_id = 0;
        $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store 
                          SET category_id = '" . (int)$category_id . "', 
                          store_id = '" . (int)$store_id . "'");

        $this->cache->delete('category');

        /* add relation ship with sub_categories */
        // create for new
        $sub_category_names = isset($data['sub_categories']) && is_array($data['sub_categories']) ? $data['sub_categories'] : [];
        $existing_sub_categories = $this->getCategoriesByNamess($sub_category_names);

        $new_sub_categories = array_udiff($sub_category_names, $existing_sub_categories, 'strcasecmp');
        $new_sub_categories = array_unique($new_sub_categories);

        foreach ($new_sub_categories as $new_sub_category) {
            $data_sub_cat = [
                "parent_id" => $category_id,
                "status" => 1,
                "category_description" => $new_sub_category,
                "name" => $new_sub_category,
                "sub_categories" => '[]'
            ];

            $this->addCategory($data_sub_cat);
        }

        // update for existing
        foreach ($existing_sub_categories as $existing_sub_category) {
            if ($existing_sub_category == $data['name']) {
                continue;
            }

            $existing_sub_category = trim($existing_sub_category);
            $filter_name_1 = html_entity_decode($existing_sub_category); // support new value a&b
            $filter_name_2 = htmlentities($filter_name_1); // support old value a&amp;b

            $this->db->query("UPDATE " . DB_PREFIX . "category c 
                              LEFT JOIN " . DB_PREFIX . "category_description cd ON c.category_id = cd.category_id
                              SET c.parent_id = '" . (int)$category_id . "', 
                              c.date_modified = NOW() 
                              WHERE (cd.`name` = '" . $filter_name_1 . "' OR cd.`name` = '" . $filter_name_2 . "') 
                              AND cd.`language_id` = " . (int)$this->config->get('config_language_id'));
        }

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        // seo
        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($data['name'], '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);

        foreach ($languages as $key => $language) {
            $language_id = $language['language_id'];
            $alias = 'category_id=' . $category_id;
            $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                    SET language_id = '" . (int)$language_id . "', 
                        query = '" . $alias . "', 
                        keyword = '" . $slug_custom . "', 
                        table_name = 'category'
            ";
            $this->db->query($sql);
        }

        $this->deleteProductCaches();

        // send product to rabbit mq
        try {
            $this->sendToRabbitMq($category_id, $this->db->escape(trim($data['name'])));
        } catch (Exception $exception) {
            $this->log->write('Send message add category error: ' . $exception->getMessage());
        }
        
        return $category_id;
    }

    /**
     * @param $name
     * @return false|mixed
     */
    public function checkNameCategoryExist($name) {
//        $name_search = mb_strtoupper($filter['name']);
//        $sql .= " AND hex(UPPER(name)) = hex('". $this->db->escape($name_search) ."')";
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_description` WHERE hex(UPPER(name)) = hex('" . $this->db->escape(mb_strtoupper($name)) . "')");
        if (isset($query->row['category_id'])) {
            return $query->row['category_id'];
        }

        return false;
    }

    /**
     * @param $category_id
     * @param $data
     */
    public function editCategory($category_id, $data)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "category 
                          SET parent_id = '" . (int)$data['parent_id'] . "', 
                          date_modified = NOW() 
                          WHERE category_id = '" . (int)$category_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "category  
                          SET image = '" . $this->db->escape(trim($data['image'])) . "' 
                          WHERE category_id = '" . (int)$category_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category_description 
                          WHERE category_id = '" . (int)$category_id . "'");

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        if (isset($data['category_description']) && isset($data['name'])) {
            // add with all languages
            foreach ($languages as $language) {
                $language_id = $language['language_id'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_description 
                                  SET category_id = '" . (int)$category_id . "', 
                                  language_id = '" . (int)$language_id . "', 
                                  `name` = '" . $this->db->escape(trim($data['name'])) . "', 
                                  description = '" . $this->db->escape(trim($data['category_description'])) . "'");
            }
        }

        //thêm mới product-choose
        if (isset($data['product-choose'])) {
            //xoá record trogn table product-category với product_id va category_id
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE category_id = '" . (int)$category_id . "'");

            foreach ($data['product-choose'] as $key => $product_id) {

                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
            }
        }

        // TODO: remove or reuse...
        //xoá category trong bảng relation_table
        /*if (isset($data['menu'])) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "novaon_relation_table WHERE child_id = '" . (int)$category_id . "' AND child_name='category'");
            //thêm mới category vả menu trong bảng relation_table 
            foreach ($data['menu'] as $key => $menu) {
                $result = explode('-', $menu);
                $menuName = $result[0];
                $menuId = $result[1];
                $this->db->query("
                    INSERT INTO " . DB_PREFIX . "novaon_relation_table SET main_name = '" . $menuName . "', main_id = '" . $menuId . "', child_name = 'category', child_id = '" . $category_id . "', type_id = 2, redirect = 0
                ");
            }

        }*/
        //category path
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE path_id = '" . (int)$category_id . "' ORDER BY level ASC");

        if ($query->rows) {
            foreach ($query->rows as $category_path) {
                // Delete the path below the current one
                $this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_path['category_id'] . "' AND level < '" . (int)$category_path['level'] . "'");

                $path = array();

                // Get the nodes new parents
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Get whats left of the nodes current path
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_path['category_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Combine the paths with a new level
                $level = 0;

                foreach ($path as $path_id) {
                    $this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_path['category_id'] . "', `path_id` = '" . (int)$path_id . "', level = '" . (int)$level . "'");

                    $level++;
                }
            }
        } else {
            // Delete the path below the current one
            $this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_id . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

                $level++;
            }

            $this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', level = '" . (int)$level . "'");
        }

        /* add relation ship with sub_categories */
        // create for new

        // remove first (parent id = 0)
        // a,b,c,d

        // update
        // a,b,c,d => risk: loss all data if could not edit...


        // $sub_category_names, e.g: old: a,b, submit: b,c,d => find:
        // names no change: b
        // names to be added: c,d <= array_diff(submit, old);
        // name to be removed: a <= array_diff(old, submit);

        $sub_category_names = isset($data['sub_categories']) && is_array($data['sub_categories']) ? $data['sub_categories'] : [];
        $existing_sub_categories = $this->getCategoriesByNamess($sub_category_names);

        $old_childs = $this->getChildCategory($category_id);
        $olds = [];

        foreach ($old_childs as $child) {
            if (!isset($child['name'])) {
                continue;
            }

            $olds[] = $child['name'];
        }

        $new_sub_categories = array_udiff($sub_category_names, $existing_sub_categories,'strcasecmp');
        $remove_sub_categories = array_udiff($olds, $sub_category_names,'strcasecmp');
        $new_sub_categories = array_unique($new_sub_categories);

        foreach ($new_sub_categories as $new_sub_category) {
            $data_sub_cat = [
                "parent_id" => $category_id,
                "status" => 1,
                "category_description" => $new_sub_category,
                "name" => $new_sub_category,
                "sub_categories" => '[]'
            ];

            $this->addCategory($data_sub_cat);
        }

        // update for existing
        foreach ($existing_sub_categories as $existing_sub_category) {
            if ($existing_sub_category == $data['name']) {
                continue;
            }

            $existing_sub_category = trim($existing_sub_category);
            $filter_name_1 = html_entity_decode($existing_sub_category); // support new value a&b
            $filter_name_2 = htmlentities($filter_name_1); // support old value a&amp;b

            $this->db->query("UPDATE " . DB_PREFIX . "category c 
                          LEFT JOIN " . DB_PREFIX . "category_description cd ON c.category_id = cd.category_id
                          SET c.parent_id = '" . (int)$category_id . "', 
                          c.date_modified = NOW() 
                          WHERE (cd.`name` = '" . $filter_name_1 . "' OR cd.`name` = '" . $filter_name_2 . "') 
                          AND cd.`language_id` = " . (int)$this->config->get('config_language_id'));
        }

        // rm parent_id
        foreach ($remove_sub_categories as $remove_sub_category) {
            if (!array_search($remove_sub_category, $sub_category_names)) {
                $remove_sub_category = trim($remove_sub_category);
                $filter_name_1 = html_entity_decode($remove_sub_category); // support new value a&b
                $filter_name_2 = htmlentities($filter_name_1); // support old value a&amp;b

                $this->db->query("UPDATE " . DB_PREFIX . "category c 
                                  LEFT JOIN " . DB_PREFIX . "category_description cd ON c.category_id = cd.category_id
                                  SET c.parent_id = '0', 
                                  c.date_modified = NOW() 
                                  WHERE (cd.`name` = '" . $filter_name_1 . "' OR cd.`name` = '" . $filter_name_2 . "') 
                                  AND cd.`language_id` = " . (int)$this->config->get('config_language_id'));
            }
        }

        //seo
        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($data['name'], '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);
        //delete seo product
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'category_id=" . (int)$category_id . "'");
        //create new seo product
        foreach ($languages as $key => $language) {
            $language_id = $language['language_id'];
            $alias = 'category_id=' . $category_id;
            $sql = "INSERT INTO " . DB_PREFIX . "seo_url SET language_id = '" . (int)$language_id . "', query = '" . $alias . "', keyword = '" . $slug_custom . "', table_name = 'category'
            ";
            $this->db->query($sql);
        }

        $this->deleteProductCaches();

        // send product to rabbit mq
        $old_name_manufacture = !empty($data['old_name']) ? $data['old_name'] : '';
        if ($data['name'] != $old_name_manufacture) {
            try {
                $this->sendToRabbitMq($category_id, $this->db->escape($data['name']), $data['old_name']);
            } catch (Exception $exception) {
                $this->log->write('Send message edit category error: ' . $exception->getMessage());
            }
        }

        //$this->cache->delete('category');
    }

    /**
     * @param $category_id
     * @return mixed
     */
    public function getChildCategory($category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c 
                          LEFT JOIN " . DB_PREFIX . "category_description cd 
                          ON c.category_id = cd.category_id 
                          WHERE c.`parent_id` = '" . $category_id . "' 
                          AND cd.`language_id` = " . (int)$this->config->get('config_language_id'));


        return $query->rows;
    }

    /**
     * @param $category_id
     * @return bool
     */
    public function deleteCategory($category_id)
    {
        // TODO: remove because check category has children not remove
        /*$count = $this->db->query("SELECT COUNT(*) AS count FROM " . DB_PREFIX . "category WHERE parent_id = '" . (int)$category_id . "'")->row;
        if ($count['count'] > 0) {
            return false;
        }*/

        try {
            $this->modifyCategoryRabbitMq($category_id, 'delete');
        } catch (Exception $exception) {
            $this->log->write('Send message product error: ' . $exception->getMessage());
        }

        // remove category_path
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_path WHERE category_id = '" . (int)$category_id . "'");

        // remove all sub-categories
        $categories = $this->db->query("SELECT category_id, parent_id FROM " . DB_PREFIX . "category WHERE parent_id = '" . (int)$category_id . "'");
        foreach ($categories->rows as $result) {
            // avoid infinitive loop when parent id = sub id
            if ($result['category_id'] == $category_id) {
                continue;
            }

            $this->deleteCategory($result['category_id']);
        }

        //delete in relationtable
        $this->db->query("DELETE FROM " . DB_PREFIX . "novaon_relation_table WHERE child_id = '" . (int)$category_id . "' AND child_name = 'category'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'category_id=" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "coupon_category WHERE category_id = '" . (int)$category_id . "'");

        $this->cache->delete('category');
        $this->deleteProductCaches();

        return true;
    }

    public function modifyCategoryRabbitMq($category_id, $action = 'delete')
    {
        try {
            if (empty($category_id)) {
                return;
            }

            $category = $this->getCategory($category_id);
            if (!empty($category['name'])) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'category' => [
                        'name' => $category['name'],
                        'action' => $action,
                    ]
                ];

                $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'modify_category', '', 'modify_category', json_encode($message));
            }
        } catch (Exception $exception) {
            $this->log->write('Send message modifyCategoryRabbitMq error: ' . $exception->getMessage());
        }
    }

    /**
     * @param array $sub_category_names
     * @return array
     */
    public function getCategoriesByNamess(array $sub_category_names)
    {
        if (!is_array($sub_category_names) || empty($sub_category_names)) {
            return [];
        }
        $str_sub_category_name = "";
        foreach ($sub_category_names as $key => $sub_name) {
            if ($key > 0) {
                $str_sub_category_name .= ",";
            }
            $str_sub_category_name .= "'" . $sub_name . "'";
        }
//        if (isset($str_sub_category_name) {
//            $name_search = mb_strtoupper($str_sub_category_name);
//            $sql .= " AND hex(UPPER(name)) = hex('". $this->db->escape($name_search) ."')";
//        }
        $sql = "SELECT `name`  
                                   FROM `" . DB_PREFIX . "category_description` 
                                   WHERE `name` IN ( " . $str_sub_category_name . " ) 
                                   AND `language_id` = " . (int)$this->config->get('config_language_id');
        $query = $this->db->query($sql);
        $result = [];
        if ($query->num_rows > 0) {
            foreach ($query->rows as $row) {
                $result[] = $row['name'];
            }
        }

        return $result;
    }

    /**
     * @param $filter_data
     * @param int $categoryId
     * @return mixed
     */
    public function getTotalCategories($filter_data, $categoryId = 0)
    {
        $sql = "SELECT COUNT(DISTINCT c.category_id) AS total FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c.parent_id = '" . $categoryId . "'";
        if (isset($filter_data['filter_status']) && $filter_data['filter_status'] != '') {
            $sql = $sql . " AND c.status = '" . $filter_data['filter_status'] . "'";
        }
        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] != '') {
            $sql = $sql . " AND cd.name LIKE '%" . $this->db->escape($filter_data['filter_name']) . "%'";
        }
        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function sendToRabbitMq($category_id, $category_name, $category_old_name = "")
    {
        try {
            if (!empty($category_id)) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'category' => [
                        'name' => $category_name,
                        'old_name' => $category_old_name
                    ]
                ];

                $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'category', '', 'category', json_encode($message));
            }
        } catch (Exception $exception) {
            $this->log->write('Send message category error: ' . $exception->getMessage());
        }
    }

    public function getAllParentsCategoryById($category_id)
    {
        $result = [];
        $sql = "SELECT * FROM `" . DB_PREFIX . "category` c 
                                   LEFT JOIN `" . DB_PREFIX . "category_description` as cd ON c.`category_id` = cd.`category_id` 
                                   WHERE c.`category_id` = " . (int)$category_id . " 
                                   AND cd.`language_id` = " . (int)$this->config->get('config_language_id');

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getAllParentsCategoryById_' . md5($sql);
        $category = $this->cache->get($cache_key);
        if (!$category) {
            // get result from db
            $query = $this->db->query($sql);
            $category = $query->row;
            $this->cache->set($cache_key, $category);
        }

        if (empty($category)) {
            return $result;
        }

        $result[] = [
            'id' => $category['category_id'],
            'name' =>  html_entity_decode($category['name']),
            'href' => $this->url->link('common/shop', 'path=' . $category['category_id'], true),
            'text' => html_entity_decode($category['name']), // return a&b instead of a&amp;b
            'image' => $category['image'],
            'meta_title' => html_entity_decode($category['meta_title']),
            'meta_keyword' => html_entity_decode($category['meta_keyword']),
            'meta_description' => html_entity_decode($category['meta_description'])
        ];
        if ($category['parent_id'] != 0 &&
            // IMPORTANT: avoid infinitive loop if wrong data such as category has category_id = parent_id
            $category['parent_id'] != $category_id
        ) {
            $parents = $this->getAllParentsCategoryById($category['parent_id']);
            foreach ($parents as $v) {
                $result[] = [
                    'id' => $v['id'],
                    'name' =>  html_entity_decode($v['name']),
                    'href' => $this->url->link('common/shop', 'path=' . $v['id'], true),
                    'text' => html_entity_decode($v['name']), // return a&b instead of a&amp;b
                    'image' => $v['image'],
                    'meta_title' => html_entity_decode($v['meta_title']),
                    'meta_keyword' => html_entity_decode($v['meta_keyword']),
                    'meta_description' => html_entity_decode($v['meta_description'])
                ];
            }
        }

        return $result;
    }

    /**
     * delete product caches
     */
    private function deleteProductCaches() {
        $product_key_redis =  DB_PREFIX . 'product';
        $this->cache->delete($product_key_redis);
    }
}