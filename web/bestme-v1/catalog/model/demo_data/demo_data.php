<?php

class ModelDemoDataDemoData extends Controller
{
    const VNPAY_METHOD = 'paym_ons_vnpay';
    const COD_METHOD = '198535319739A';
    const BANK_TRANSFER_METHOD = '888888888888';

    /**
     * delete all demo data except product, category, collection
     */
    public function removeAllDataDemo()
    {
        $this->removeOrderAndReturnReceipt();
        $this->removeReport();
        $this->removeStoreAndReceipt();
        $this->removeCustomer();
        $this->removeBankTransferMethod();
        $this->removeCashFlow();
        $this->removeManufacturer();
        $this->removePromotion();
    }

    private function removePromotion() {
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "discount");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "discount_description");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "discount_to_store");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "order_to_discount");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "product_discount");
    }

    private function removeManufacturer()
    {
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "manufacturer");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "manufacturer_to_store");
    }

    private function removeBankTransferMethod()
    {
        $this->load->model('setting/setting');

        $payment_methods = $this->model_setting_setting->getSettingValue('payment_methods');
        $payment_methods = json_decode($payment_methods, true);

        $new_payment_method = [];
        foreach ($payment_methods as $payment_method) {
            if ($payment_method['payment_id'] !== self::BANK_TRANSFER_METHOD) {
                $new_payment_method[] = $payment_method;
            }
        }

        if ($new_payment_method == []) {
            $new_payment_method = json_decode('[{"payment_id":"198535319739A","name":"Thanh toán khi nhận hàng (COD)","guide":"Bạn sẽ thanh toán toàn bộ giá trị đơn hàng khi nhận hàng","status":"1"}]');
        }

        $this->model_setting_setting->editSettingValue('payment', 'payment_methods', $new_payment_method);
    }

    private function removeOrderAndReturnReceipt()
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "order WHERE 1");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "order_history");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "order_product");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "return_receipt");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "return_product");
    }

    private function removeReport()
    {
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "report_order");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "report_product");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "report");
    }

    private function removeStoreAndReceipt()
    {
        $this->db->query("DELETE from " . DB_PREFIX . "store WHERE `store_id` != 0");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "store_receipt");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "store_receipt_to_product");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "store_take_receipt");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "store_take_receipt_to_product");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "store_transfer_receipt");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "store_transfer_receipt_to_product");
    }

    private function removeCustomer()
    {
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "customer");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "customer_group");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "customer_group_description");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "address");
    }

    private function removeCashFlow()
    {
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "payment_voucher");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "receipt_voucher");
    }
}