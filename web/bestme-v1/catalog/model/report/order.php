<?php
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;

class ModelReportOrder extends Model {

    public function saveOrderReport($data)
    {
        $user_id = $this->user->getId();
        $store_id = 0;
        $sql = "INSERT INTO ". DB_PREFIX . "report_order SET report_time = '". $data['report_time'] ."'";
        $sql .= ", report_date = '" . $data['report_date'] . "'";
        $sql .= ", order_id = '" . $data['order_id'] . "'";
        $sql .= ", customer_id = '" . $data['customer_id'] . "'";
        $sql .= ", total_amount = '" . $data['total_amount'] . "'";
        $sql .= ", order_status = '" . $data['order_status'] . "'";
        $sql .= ", store_id = '" . $store_id . "'";
        $sql .= ", source = '" . $data['source'] . "'";
        $sql .= ", discount = '" . $data['discount'] . "'";
        $sql .= ", shipping_fee = '" . $data['shipping_fee'] . "'";
        $sql .= ", user_id = '" . (int)$user_id . "'";
        $this->db->query($sql);
    }

    public function updateTotalReturnOrderReport($order_id, $total_return) {
        $sql = "UPDATE ". DB_PREFIX . "report_order 
                SET total_return = total_return + '" . $total_return . "'
                WHERE order_id = '" . (int)$order_id . "'";
        $this->db->query($sql);
    }

    public function updateOrderReport($data)
    {
        $sql = "UPDATE ". DB_PREFIX . "report_order SET ";
        $sql .= "customer_id = '" . $data['customer_id'] . "'";
        $sql .= ", total_amount = '" . $data['total_amount'] . "'";
        $sql .= ", order_status = '" . $data['order_status'] . "'";
        $sql .= ", shipping_fee = '" . $data['shipping_fee'] . "'";
        $sql .= ", discount = '" . $data['discount'] . "'";
        $sql .= " WHERE order_id = '" . $data['order_id'] . "'";
        $this->db->query($sql);
    }

    public function updateStatusOrderReport($data)
    {
        $sql = "UPDATE ". DB_PREFIX . "report_order SET ";
        $sql .= "order_status = '" . $data['order_status'] . "'";
        $sql .= " WHERE order_id = '" . $data['order_id'] . "'";
        $this->db->query($sql);
    }

    public function getReportByOrderId($order_id)
    {
        if (!$order_id){
            return [];
        }

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "report_order` WHERE order_id = " . (int)$order_id);

        return $query->row;
    }

    public function saveCASOrderReport($data)
    {
        $sql = "UPDATE ". DB_PREFIX . "report_order SET ";
        $sql .= "customer_id = '" . $data['customer_id'] . "'";
        $sql .= ", total_amount = '" . $data['total_amount'] . "'";
        $sql .= ", order_status = '" . $data['order_status'] . "'";
        $sql .= ", shipping_fee = '" . $data['shipping_fee'] . "'";
        $sql .= ", discount = '" . $data['discount'] . "'";
        $sql .= " WHERE order_id = '" . $data['order_id'] . "'";
        $this->db->query($sql);
    }

    /* Get total orders by period */
    public function getTotalOrderOnDashBoard($filter = null)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT COUNT(DISTINCT R.order_id ) AS total FROM `{$DB_PREFIX}report_order` as R ";
        if (!empty($filter['source'])) {
            $sql .= " JOIN `{$DB_PREFIX}order` O ON R.`order_id` = O.`order_id` AND O.`source` = '{$filter['source']}' ";
        }
        $sql .= " WHERE R.order_status IN('7', '8', '9', '10') AND R.`report_time` BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "' ";
        if (isset($filter['user_id'])){
            $sql .= " AND R.`user_id` = " . (int)$filter['user_id'];
        }
        if (isset($filter['store_id'])){
            $sql .= " AND R.`store_id` = " . (int)$filter['store_id'];
        }
        if (!empty($filter['order_status'])) {
            $sql .= " AND R.`order_status` = " . (int)$filter['order_status'];
        }
        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    /* Get total customer by period */
    public function getTotalCustomerOnDashBoard($filter = null)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT COUNT(DISTINCT ro.customer_id ) AS total FROM `{$DB_PREFIX}report_order` as ro ";
        if (!empty($filter['source'])) {
            $sql .= " JOIN `{$DB_PREFIX}order` O ON ro.`order_id` = O.`order_id` AND O.`source` = '{$filter['source']}' ";
        }
        $sql .= " WHERE ro.order_status IN('7', '8', '9', '10') AND ro.`report_time` BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "' ";
        if (isset($filter['user_id'])){
            $sql .= " AND ro.`user_id` = " . (int)$filter['user_id'];
        }
        if (isset($filter['store_id'])){
            $sql .= " AND ro.`store_id` = " . (int)$filter['store_id'];
        }

        $query = $this->db->query($sql);
        return $query->row['total'];
    }


    // v2.9.2
    public function getReportByOrders(array $filter)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT SUM(total_amount + discount - shipping_fee) as `total_amount`, 
                       SUM(shipping_fee) as `shipping_fee`, 
                       SUM(discount) as `discount`, 
                       COUNT(DISTINCT order_id) as `total_order`, 
                       `report_date`, 
                       '0' as order_return, 
                       '0' as total_return, 
                       `report_date`, 
                       `store_id` 
                  FROM `{$DB_PREFIX}report_order` WHERE `order_status` NOT IN(6, 10)";

        if (!empty($filter['start_time']) && !empty($filter['end_time'])) {
            $sql .= " AND `report_time` BETWEEN '{$filter['start_time']}' AND '{$filter['end_time']}'";
        }

        if (isset($filter['store_id']) && $filter['store_id'] !== '' && $filter['store_id'] > -1) {
            $sql .= " AND `store_id` = " . (int)$filter['store_id'];
        }

        $sql .= " GROUP BY `report_date`";
        $sql .= " ORDER BY `report_date` DESC";

        $query = $this->db->query($sql);

        return $query->rows;

    }

    public function getDataTotalReportByOrders(array $filter)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT SUM(total_amount + discount - shipping_fee) as `total_amount`, 
                       SUM(shipping_fee) as `shipping_fee`, 
                       SUM(discount) as `discount`, 
                       COUNT(DISTINCT order_id) as `total_order`, 
                       `report_date`, 
                       '0' as order_return, 
                       '0' as total_return, 
                       `store_id` 
                   FROM `{$DB_PREFIX}report_order` WHERE `order_status` NOT IN(6, 10)";

        if (!empty($filter['start_time']) && !empty($filter['end_time'])) {
            $sql .= " AND `report_time` BETWEEN '{$filter['start_time']}' AND '{$filter['end_time']}'";
        }

//        if(!$this->user->isAdmin() && !$this->user->canAccessAll()) {
//            $sql .= " AND `user_id` = '" . (int)$this->user->getId() . "'";
//        }
//        if (isset($filter['store_id']) && $filter['store_id'] !== '' && $filter['store_id'] > -1
//            && ($this->user->isUserInStore($filter['store']) || $this->user->isAdmin() || $this->user->canAccessAll())
//        ) {
//            $sql .= " AND `store_id` = " . (int)$filter['store_id'];
//        }  else {
//            $sql .= " AND `store_id` in (0,". implode(',', $this->user->getUserStores()) .")";
//        }

        if (isset($filter['store_id']) && $filter['store_id'] !== '') {
            $sql .= " AND `store_id` = '{$filter['store_id']}'";
        }

        $sql .= " GROUP BY `report_date`";

        $query = $this->db->query($sql);

        return $query->rows;

    }

    public function getTotalReportByOrders(array $filter)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT COUNT(DISTINCT report_date) as total  FROM `{$DB_PREFIX}report_order` WHERE `order_status` NOT IN(6, 10)";

        if (!empty($filter['start_time']) && !empty($filter['end_time'])) {
            $sql .= " AND `report_time` BETWEEN '{$filter['start_time']}' AND '{$filter['end_time']}'";
        }

        if (isset($filter['store_id']) && $filter['store_id'] !== '') {
            $sql .= " AND `store_id` = '{$filter['store_id']}'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function export($data)
    {
        $this->load->language('report/order');
        $report_by_orders = $data['data_report'];
        foreach ($report_by_orders as $value){
            $result[] = array(
                "{$this->language->get('text_column_date')}"                    => $value['report_date'],
                "{$this->language->get('text_column_order_buy')}"               => $value['total_order'],
                "{$this->language->get('text_column_total_amount')}"            => $value['total_amount'],
                "{$this->language->get('text_column_discount')}"                => $value['discount'],
                "{$this->language->get('text_column_shipping_fee')}"            => $value['shipping_fee'],
                "{$this->language->get('text_column_order_return')}"            => $value['order_return'],
                "{$this->language->get('text_column_money_back')}"              => $value['total_return'],
                "{$this->language->get('text_column_revenue')}"                 => $value['revenue'],
            );
        }

        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->setTitle("{$this->language->get('heading_title')}");
        $activeSheet->setCellValue('C1', "{$this->language->get('heading_title')}");
        $activeSheet->setCellValue('D2', "{$this->language->get('text_excel_store')}");
        $activeSheet->setCellValue('E2', $data['store_name']);
        $activeSheet->setCellValue('D3', "{$this->language->get('text_excel_time')}");
        $activeSheet->setCellValue('E3', $data['time']);
        $activeSheet->getStyle("C1")->getFont()->setSize(16);
        $activeSheet->getStyle("D3")->getFont()->setBold(true);
        $activeSheet->getStyle("D2")->getFont()->setBold(true);

        //output headers
        $activeSheet->fromArray(array_keys($result[0]), NULL, 'A4');

        $styleArrayHeader = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'cfe2f3',
                ],
                'endColor' => [
                    'argb' => 'cfe2f3',
                ],
            ],
        ];
        $styleColumn      = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
        ];
        $styleArrayFooter = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'fce5cd',
                ],
                'endColor' => [
                    'argb' => 'fce5cd',
                ],
            ],
        ];

        $style_align_right = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ],
        ];
        $style_align_left  = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ],
        ];

        $activeSheet->getStyle('A4:H4')->applyFromArray($styleArrayHeader);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(50);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);

        $spreadsheet->getActiveSheet()->getRowDimension('4')->setRowHeight(40);

        $rowCount = 5;
        foreach ($result as $key => $row) {
            $activeSheet->setCellValue('A' . $rowCount, $row["{$this->language->get('text_column_date')}"]);
            $activeSheet->SetCellValue('B' . $rowCount, $row["{$this->language->get('text_column_order_buy')}"]);
            $activeSheet->SetCellValue('C' . $rowCount, $row["{$this->language->get('text_column_total_amount')}"]);
            $activeSheet->SetCellValue('D' . $rowCount, $row["{$this->language->get('text_column_discount')}"]);
            $activeSheet->SetCellValue('E' . $rowCount, $row["{$this->language->get('text_column_shipping_fee')}"]);
            $activeSheet->SetCellValue('F' . $rowCount, $row["{$this->language->get('text_column_order_return')}"]);
            $activeSheet->SetCellValue('G' . $rowCount, $row["{$this->language->get('text_column_money_back')}"]);
            $activeSheet->SetCellValue('H' . $rowCount, $row["{$this->language->get('text_column_revenue')}"]);
            $activeSheet->getStyle('A' . $rowCount . ':B' . $rowCount)->applyFromArray(array_merge($styleColumn, $style_align_left));
            $activeSheet->getStyle('C' . $rowCount . ':H' . $rowCount)->applyFromArray(array_merge($styleColumn, $style_align_right));
            $rowCount++;
        }


        $activeSheet->setCellValue('A' . $rowCount, "{$this->language->get('text_total')}");
        $activeSheet->SetCellValue('B' . $rowCount, $data['total_all']['total_order']);
        $activeSheet->SetCellValue('C' . $rowCount, $data['total_all']['total_amount']);
        $activeSheet->SetCellValue('D' . $rowCount, $data['total_all']['total_discount']);
        $activeSheet->SetCellValue('E' . $rowCount, $data['total_all']['total_shipping_fee']);
        $activeSheet->SetCellValue('F' . $rowCount, $data['total_all']['total_order_return']);
        $activeSheet->SetCellValue('G' . $rowCount, $data['total_all']['total_return']);
        $activeSheet->SetCellValue('H' . $rowCount, $data['total_all']['total_revenue']);
        $activeSheet->getStyle('A' . $rowCount . ':B' . $rowCount)->applyFromArray(array_merge($styleArrayFooter, $style_align_left));
        $activeSheet->getStyle('C' . $rowCount . ':H' . $rowCount)->applyFromArray(array_merge($styleArrayFooter, $style_align_right));

        /*
         * write output for downloading...
         * https://stackoverflow.com/questions/27701981/phpexcel-download-using-ajax-call
         */
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        ob_start();
        $writer->save("php://output"); // for test: DIR_APPLICATION . "products.xlsx"
        $xlsxData = ob_get_contents();
        ob_end_clean();

        return 'data:application/vnd.ms-excel;base64,' . base64_encode($xlsxData);
    }

    public function dataExport(array $filter)
    {
        $this->load->model('sale/return_receipt');

        $data_excel = [
            'data_report' => [],
            'total_all' => [
                'total_order' => 0,
                'total_amount' => 0,
                'total_discount' => 0,
                'total_shipping_fee' => 0,
                'total_order_return' => 0,
                'total_return' => 0,
                'total_revenue' => 0,
            ],
        ];

        try {
            $start_time_req_obj = date_create($filter['start_time']);
            if (!$start_time_req_obj instanceof DateTime) {
                throw new Exception('Excel invalid "start_time"');
            }

            $end_time_req_obj = date_create($filter['end_time']);
            if (!$end_time_req_obj instanceof DateTime) {
                throw new Exception('Excel invalid "end_time"');
            }

            $order_reports = [];
            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($start_time_req_obj, $interval, $end_time_req_obj);

            foreach ($period as $dt) {
                /** @var DateTime $dt */
                $r_date = $dt->format('Y-m-d');

                $order_reports[$r_date] = [
                    'total_amount' => 0,
                    'shipping_fee' => 0,
                    'discount' => 0,
                    'total_order' => 0,
                    'report_date' => $r_date,
                    'store_id' => 0,
                    'revenue' => 0,
                    'total_return' => 0, // later
                    'order_return' => 0, // later
                ];
            }

            // get return receive report by date range
            $order_return = $this->model_sale_return_receipt->totalOrderReturnByDateGroupByDate($filter['start_time'], $filter['end_time']);
            foreach ($order_return as $ort) {
                if (!is_array($ort) ||
                    !array_key_exists('date_added', $ort) ||
                    !array_key_exists('order_return', $ort) ||
                    !array_key_exists('total_return', $ort)
                ) {
                    continue;
                }

                $order_reports[$ort['date_added']]['order_return'] = (int)$ort['order_return'];
                $order_reports[$ort['date_added']]['total_return'] = (float)$ort['total_return'];
            }

            $data_report = $this->getDataTotalReportByOrders($filter);
            foreach ($data_report as $rp_order) {
                // data table
                $order_reports[$rp_order['report_date']]['total_amount'] = (float)$rp_order['total_amount'];
                $order_reports[$rp_order['report_date']]['shipping_fee'] = (float)$rp_order['shipping_fee'];
                $order_reports[$rp_order['report_date']]['discount'] = (float)$rp_order['discount'];
                $order_reports[$rp_order['report_date']]['total_order'] = (int)$rp_order['total_order'];
                $order_reports[$rp_order['report_date']]['store_id'] = (float)$rp_order['store_id'];

                // data total
                $data_excel['total_all']['total_order'] += (float)$rp_order['total_order'];
                $data_excel['total_all']['total_amount'] += (float)$rp_order['total_amount'];
                $data_excel['total_all']['total_discount'] += (float)$rp_order['discount'];
                $data_excel['total_all']['total_shipping_fee'] += (float)$rp_order['shipping_fee'];
            }

            // calculate "revenue" after got full reports with return receive and order report detail
            foreach ($order_reports as &$or) {
                $or['revenue'] = (float)$or['total_amount'] - (float)$or['discount'] + (float)$or['shipping_fee'] - (float)$order_reports[$or['report_date']]['total_return'];

                $data_excel['total_all']['total_order_return'] += $or['order_return'];
                $data_excel['total_all']['total_return'] += $or['total_return'];
                $data_excel['total_all']['total_revenue'] += $or['revenue'];
            }
            unset($or);

            $data_excel['data_report'] = $order_reports;
        } catch (Exception $e) {
            //
        }

        return $data_excel;
    }

    /**
     * get order info by date range
     *
     * @param $filter
     * @return array
     * @throws Exception
     */
    public function getOrdersByDate($filter) {
        $this->load->model('sale/order');
        $this->load->model('report/overview');

        $date1 = new DateTime($filter['start']);
        $date2 = new DateTime($filter['end']);
        $interval = $date1->diff($date2);
        $filter_order_status = [
            ModelSaleOrder::ORDER_STATUS_ID_PROCESSING,
            ModelSaleOrder::ORDER_STATUS_ID_DELIVERING,
            ModelSaleOrder::ORDER_STATUS_ID_COMPLETED,
            ModelSaleOrder::ORDER_STATUS_ID_CANCELLED
        ];

        $DB_PREFIX = DB_PREFIX;
        $source_query = empty($filter['source']) ? '' : " AND O.`source` = '{$filter['source']}' ";
        $sqlContainer = "SELECT DISTINCT R.* FROM `{$DB_PREFIX}report_order` R JOIN `{$DB_PREFIX}order` O ON R.`order_id` = O.`order_id` {$source_query}
                         WHERE R.report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "' 
                            AND R.`order_status` IN (" . implode(',', $filter_order_status) . ")";
        if (isset($filter['user_id'])){
            $sqlContainer .= " AND R.`user_id` = " . $filter['user_id'];
        }
        if (isset($filter['store_id'])){
            $sqlContainer .= " AND R.`store_id` = " . $filter['store_id'];
        }

        if ($interval->days < 1) {
            $type_format_time = 1;
            $result_label_query = "DISTINCT report_time";
            $group_by_field = $order_by_field = 'report_time';
            $list_times = $this->model_report_overview->getListHours($filter);
        } elseif ($interval->days >= 1 && $interval->days <= 120) {
            $type_format_time = 2;
            $result_label_query = "report_date";
            $group_by_field = $order_by_field = 'report_date';
            $list_times = $this->model_report_overview->getListDay($filter);
        } else {
            $type_format_time = 3;
            $result_label_query = "LEFT(report_date, 7)";
            $group_by_field = 'result_label';
            $order_by_field = 'report_date';
            $list_times = $this->model_report_overview->getListMonth($filter);
        }

        $sql = "SELECT {$result_label_query} as result_label, COUNT(1) as total_value 
                FROM ({$sqlContainer}) as report_data 
                WHERE report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "' 
                GROUP BY {$group_by_field} 
                ORDER BY {$order_by_field} ASC";

        $query = $this->db->query($sql);
        $results = $query->rows;

        // get data
        foreach ($list_times as $list_time) {
            $json_data['content'][] = [
                'label' => $this->model_report_overview->formatDate($list_time, $type_format_time),
                'value' => $this->model_report_overview->getValueData($results, $list_time)
            ];
        }

        // get total
        $json_data['total'] = $this->getTotalOrderOnDashBoard($filter);

        // get total completed orders
        $filter['order_status'] = ModelSaleOrder::ORDER_STATUS_ID_COMPLETED;
        $json_data['total_completed'] = $this->getTotalOrderOnDashBoard($filter);

        return $json_data;
    }

    /**
     * get customer info by date range
     *
     * @param $filter
     * @return array
     * @throws Exception
     */
    public function getCustomersByDate($filter) {
        $this->load->model('sale/order');
        $this->load->model('report/overview');

        $date1 = new DateTime($filter['start']);
        $date2 = new DateTime($filter['end']);
        $interval = $date1->diff($date2);
        $filter_order_status = [
            ModelSaleOrder::ORDER_STATUS_ID_PROCESSING,
            ModelSaleOrder::ORDER_STATUS_ID_DELIVERING,
            ModelSaleOrder::ORDER_STATUS_ID_COMPLETED,
            ModelSaleOrder::ORDER_STATUS_ID_CANCELLED
        ];

        $DB_PREFIX = DB_PREFIX;
        $sqlContainer = "SELECT DISTINCT R.* FROM {$DB_PREFIX}report_order R ";

        if (!empty($filter['source'])) {
            $sqlContainer .= " JOIN `{$DB_PREFIX}order` O ON R.`order_id` = O.`order_id` AND O.`source` = '{$filter['source']}' ";
        }
        $sqlContainer .= " WHERE R.report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "' AND R.`order_status` IN (" . implode(',', $filter_order_status) . ") ";
        if (isset($filter['user_id'])){
            $sqlContainer .= " AND R.`user_id` = " . $filter['user_id'];
        }
        if (isset($filter['store_id'])){
            $sqlContainer .= " AND R.`store_id` = " . $filter['store_id'];
        }

        if ($interval->days < 1) {
            $type_format_time = 1;
            $result_label_query = "report_time";
            $group_by_field = $order_by_field = 'report_time';
            $list_times = $this->model_report_overview->getListHours($filter);
        } elseif ($interval->days >= 1 && $interval->days <= 120) {
            $type_format_time = 2;
            $result_label_query = "report_date";
            $group_by_field = $order_by_field = 'report_date';
            $list_times = $this->model_report_overview->getListDay($filter);
        } else {
            $type_format_time = 3;
            $result_label_query = "LEFT(report_date, 7)";
            $group_by_field = 'result_label';
            $order_by_field = 'report_date';
            $list_times = $this->model_report_overview->getListMonth($filter);
        }

        $sql = "SELECT {$result_label_query} as result_label, COUNT(DISTINCT customer_id) as total_value 
                FROM ({$sqlContainer}) as report_data 
                WHERE report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "' 
                GROUP BY {$group_by_field}
                ORDER BY {$order_by_field} ASC";

        $query = $this->db->query($sql);
        $results = $query->rows;

        // get data
        foreach ($list_times as $list_time) {
            $json_data['content'][] = [
                'label' => $this->model_report_overview->formatDate($list_time, $type_format_time),
                'value' => $this->model_report_overview->getValueData($results, $list_time)
            ];
        }

        // get total
        $json_data['total'] = $this->getTotalCustomerOnDashBoard($filter);

        return $json_data;
    }
}