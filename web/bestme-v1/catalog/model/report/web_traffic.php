<?php
class ModelReportWebTraffic extends Model {

    public function increaseWebTraffic($data)
    {
        try {
            $sql = "SELECT * FROM ". DB_PREFIX ."report_web_traffic WHERE `report_time` = '" . $data['report_time'] . "'";
            $query = $this->db->query($sql);
            if ($query->row){
                $sql = "UPDATE `" . DB_PREFIX . "report_web_traffic` SET `value` = `value` + 1, `report_time` = '". $data['report_time'] ."', `last_updated` = NOW() WHERE `report_time` = '" . $data['report_time'] . "'";
            } else {
                $sql = "INSERT INTO `" . DB_PREFIX . "report_web_traffic` SET `report_time` = '". $data['report_time'] ."', `value`= 1, `last_updated` = NOW(), `report_date` = '". $data['report_date'] ."'";
            }

            $this->db->query($sql);
        } catch (Exception $e) {
            return false;
        }

        return true;
    }
}