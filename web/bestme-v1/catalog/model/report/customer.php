<?php

class ModelReportCustomer extends Model {

    public function createCustomerReport($customer_id)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $reportTime = new DateTime();
        $reportTime->setTime($reportTime->format('H'), 0, 0);
        $sql = "INSERT INTO ". DB_PREFIX . "report_customer SET report_time = '". $reportTime->format('Y-m-d H:i:s') ."'";
        $sql .= ", report_date = '" . $reportTime->format('Y-m-d') . "'";
        $sql .= ", customer_id = '" . (int)$customer_id . "'";
        $this->db->query($sql);
    }

}