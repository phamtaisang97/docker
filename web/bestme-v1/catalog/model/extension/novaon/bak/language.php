<?php
class ModelExtensionNovaonLanguage extends Model {
	public function getLanguage($language_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "language WHERE language_id = '" . (int)$language_id . "'");
		return $query->row;
	}
	public function getLanguages() {
	    $sql = "SELECT * FROM " . DB_PREFIX . "language WHERE status = '1' ORDER BY sort_order, name";

        $cache_key = DB_PREFIX . 'language_catalog:getLanguages_' . md5($sql);
        $cache_language = $this->cache->get($cache_key);

        if ($cache_language) {
            $language_data = $cache_language;
        } else {
            $query = $this->db->query($sql);
            $language_data = $query->rows;
            $this->cache->set($cache_key, $language_data);
        }

        $result = [];
        foreach ($language_data as $language) {
            $result[] = array(
                'language_id' => $language['language_id'],
                'name'        => $language['name'],
                'code'        => $language['code'],
                'locale'      => $language['locale'],
                'image'       => $language['image'],
                'sort_order'  => $language['sort_order'],
                'status'      => $language['status']
            );
        }

		return $result;
	}
}



?>