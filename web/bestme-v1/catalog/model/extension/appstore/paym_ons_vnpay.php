<?php
class ModelExtensionAppstorePaymOnsVnpay extends Model
{
    function getAccountPayment() {
        $sql = "SELECT * FROM " . DB_PREFIX . "db_app_payment_vn_pay_account";
        $accountPayment = $this->db->query($sql);
        return $accountPayment;
    }
    function checkConnect(){
        $sql = "SELECT COUNT(id) AS total FROM " . DB_PREFIX . "db_app_payment_vn_pay_account";
        $accountPayment = $this->db->query($sql)->row;
        if($accountPayment['total']){
            return true;
        }
        return false;
    }
}
