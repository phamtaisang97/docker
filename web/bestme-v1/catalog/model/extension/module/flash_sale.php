<?php

class ModelExtensionModuleFlashSale extends Model
{
    function getProductSales()
    {
        $sql = "SELECT product_id FROM " . DB_PREFIX . "app_flash_sale_product";
        $query = $this->db->query($sql);

        return $query->rows;
    }
}