<?php

class ModelExtensionModuleThemeBuilderConfig extends Model
{
    const TABLE_NAME = DB_PREFIX . 'theme_builder_config';

    /* config key for theme */
    const CONFIG_KEY_THEME_COLOR = 'config_theme_color';
    const CONFIG_KEY_THEME_TEXT = 'config_theme_text';
    const CONFIG_KEY_THEME_FAVICON = 'config_theme_favicon';
    const CONFIG_KEY_THEME_SOCIAL = 'config_theme_social';
    const CONFIG_KEY_THEME_ECOMMERCE = 'config_theme_ecommerce';
    const CONFIG_KEY_THEME_OVERRIDE_CSS = 'config_theme_override_css';
    const CONFIG_KEY_THEME_CHECKOUT_PAGE = 'config_theme_checkout_page';

    /* config key for section */
    // homepage
    const CONFIG_KEY_SECTION_SECTIONS = 'config_section_sections';
    const CONFIG_KEY_SECTION_BANNER = 'config_section_banner';
    const CONFIG_KEY_SECTION_BEST_SALES_PRODUCT = 'config_section_best_sales_product';
    const CONFIG_KEY_SECTION_NEW_PRODUCT = 'config_section_new_product';
    const CONFIG_KEY_SECTION_BEST_VIEWS_PRODUCT = 'config_section_best_views_product';
    const CONFIG_KEY_SECTION_BLOG = 'config_section_blog';
    const CONFIG_KEY_SECTION_DETAIL_PRODUCT = 'config_section_detail_product';
    const CONFIG_KEY_SECTION_FOOTER = 'config_section_footer';
    const CONFIG_KEY_SECTION_HEADER = 'config_section_header';
    const CONFIG_KEY_SECTION_HOT_PRODUCT = 'config_section_hot_product';
    const CONFIG_KEY_SECTION_LIST_PRODUCT = 'config_section_list_product';
    const CONFIG_KEY_SECTION_PARTNER = 'config_section_partner';
    const CONFIG_KEY_SECTION_PRODUCT_GROUPS = 'config_section_product_groups';
    const CONFIG_KEY_SECTION_SLIDESHOW = 'config_section_slideshow';
    const CONFIG_KEY_SECTION_CONTENT_CUSTOMIZE = 'config_section_content_customize';
    const CONFIG_KEY_SECTION_CUSTOMIZE_LAYOUT = 'config_section_customize_layout';

    // category
    const CONFIG_KEY_SECTION_CATEGORY_SECTIONS = 'config_section_category_sections';
    const CONFIG_KEY_SECTION_CATEGORY_PRODUCT_CATEGORY = 'config_section_category_product_category';
    const CONFIG_KEY_SECTION_CATEGORY_PRODUCT_LIST = 'config_section_category_product_list';
    const CONFIG_KEY_SECTION_CATEGORY_BANNER = 'config_section_category_banner';
    const CONFIG_KEY_SECTION_CATEGORY_FILTER = 'config_section_category_filter';
    // product_detail
    const CONFIG_KEY_SECTION_PRODUCT_DETAIL_SECTIONS = 'config_section_product_detail_sections';
    const CONFIG_KEY_SECTION_PRODUCT_DETAIL_RELATED_PRODUCT = 'config_section_product_detail_related_product';
    const CONFIG_KEY_SECTION_PRODUCT_DETAIL_TEMPLATE = 'config_section_product_detail_template';
    // blog
    const CONFIG_KEY_SECTION_BLOG_SECTIONS = 'config_section_blog_sections';
    const CONFIG_KEY_SECTION_BLOG_BLOG_CATEGORY = 'config_section_blog_blog_category';
    const CONFIG_KEY_SECTION_BLOG_BLOG_LIST = 'config_section_blog_blog_list';
    const CONFIG_KEY_SECTION_BLOG_LATEST_BLOG = 'config_section_blog_latest_blog';
    // contact
    const CONFIG_KEY_SECTION_CONTACT_SECTIONS = 'config_section_contact_sections';
    const CONFIG_KEY_SECTION_CONTACT_MAP = 'config_section_contact_map';
    const CONFIG_KEY_SECTION_CONTACT_CONTACT = 'config_section_contact_contact';
    const CONFIG_KEY_SECTION_CONTACT_FORM = 'config_section_contact_form';
    const CONFIG_KEY_SECTION_RATE = 'config_section_rate';

    public static $CONFIG_KEYS_DEFAULT_CONFIG_MAPPING = [
        // theme
        self::CONFIG_KEY_THEME_COLOR => 'theme/color',
        self::CONFIG_KEY_THEME_TEXT => 'theme/text',
        self::CONFIG_KEY_THEME_FAVICON => 'theme/favicon',
        self::CONFIG_KEY_THEME_SOCIAL => 'theme/social',
        self::CONFIG_KEY_THEME_ECOMMERCE => 'theme/ecommerce',
        self::CONFIG_KEY_THEME_OVERRIDE_CSS => 'theme/override_css',
        self::CONFIG_KEY_THEME_CHECKOUT_PAGE => 'theme/checkout-page',
        // section
        //// homepage
        self::CONFIG_KEY_SECTION_SECTIONS => 'section/sections',
        self::CONFIG_KEY_SECTION_BANNER => 'section/banner',
        self::CONFIG_KEY_SECTION_BEST_SALES_PRODUCT => 'section/best_sales_product',
        self::CONFIG_KEY_SECTION_NEW_PRODUCT => 'section/new_product',
        self::CONFIG_KEY_SECTION_BEST_VIEWS_PRODUCT => 'section/best_views_product',
        self::CONFIG_KEY_SECTION_BLOG => 'section/blog',
        self::CONFIG_KEY_SECTION_DETAIL_PRODUCT => 'section/detail_product',
        self::CONFIG_KEY_SECTION_FOOTER => 'section/footer',
        self::CONFIG_KEY_SECTION_HEADER => 'section/header',
        self::CONFIG_KEY_SECTION_HOT_PRODUCT => 'section/hot_product',
        self::CONFIG_KEY_SECTION_LIST_PRODUCT => 'section/list_product',
        self::CONFIG_KEY_SECTION_PRODUCT_GROUPS => 'section/product_groups',
        self::CONFIG_KEY_SECTION_PARTNER => 'section/partner',
        self::CONFIG_KEY_SECTION_SLIDESHOW => 'section/slideshow',
        self::CONFIG_KEY_SECTION_CONTENT_CUSTOMIZE => 'section/content_customize',
        self::CONFIG_KEY_SECTION_CUSTOMIZE_LAYOUT => 'section/customize_layout',
        //// category
        self::CONFIG_KEY_SECTION_CATEGORY_SECTIONS => 'section/category/sections',
        self::CONFIG_KEY_SECTION_CATEGORY_BANNER => 'section/category/banner',
        self::CONFIG_KEY_SECTION_CATEGORY_FILTER => 'section/category/filter',
        self::CONFIG_KEY_SECTION_CATEGORY_PRODUCT_CATEGORY => 'section/category/product_category',
        self::CONFIG_KEY_SECTION_CATEGORY_PRODUCT_LIST => 'section/category/product_list',
        //// product_detail
        self::CONFIG_KEY_SECTION_PRODUCT_DETAIL_SECTIONS => 'section/product_detail/sections',
        self::CONFIG_KEY_SECTION_PRODUCT_DETAIL_RELATED_PRODUCT => 'section/product_detail/related_product',
        self::CONFIG_KEY_SECTION_PRODUCT_DETAIL_TEMPLATE => 'section/product_detail/template',
        //// blog
        self::CONFIG_KEY_SECTION_BLOG_SECTIONS => 'section/blog/sections',
        self::CONFIG_KEY_SECTION_BLOG_BLOG_CATEGORY => 'section/blog/blog_category',
        self::CONFIG_KEY_SECTION_BLOG_BLOG_LIST => 'section/blog/blog_list',
        self::CONFIG_KEY_SECTION_BLOG_LATEST_BLOG => 'section/blog/latest_blog',
        /// rate
        self::CONFIG_KEY_SECTION_RATE => 'section/rate',
        //// contact
        self::CONFIG_KEY_SECTION_CONTACT_SECTIONS => 'section/contact/sections',
        self::CONFIG_KEY_SECTION_CONTACT_MAP => 'section/contact/map',
        self::CONFIG_KEY_SECTION_CONTACT_CONTACT => 'section/contact/contact',
        self::CONFIG_KEY_SECTION_CONTACT_FORM => 'section/contact/form',
    ];

    /* default config */
    const THEME_DEFAULT_CONFIG_PATH = DIR_SYSTEM . 'library/theme_config/default_config/';
    private static $THEME_DEFAULT_CONFIG_SECTION_SUB_DIRECTORY = [
        // blog
        'config_section_blog_blog_category' => self::THEME_DEFAULT_CONFIG_PATH . 'section/blog/blog_category.json',
        'config_section_blog_blog_list' => self::THEME_DEFAULT_CONFIG_PATH . 'section/blog/blog_list.json',
        'config_section_blog_latest_blog' => self::THEME_DEFAULT_CONFIG_PATH . 'section/blog/latest_blog.json',
        'config_section_blog_sections' => self::THEME_DEFAULT_CONFIG_PATH . 'section/blog/sections.json',
        // category
        'config_section_category_banner' => self::THEME_DEFAULT_CONFIG_PATH . 'section/category/banner.json',
        'config_section_category_filter' => self::THEME_DEFAULT_CONFIG_PATH . 'section/category/filter.json',
        'config_section_category_product_category' => self::THEME_DEFAULT_CONFIG_PATH . 'section/category/product_category.json',
        'config_section_category_product_list' => self::THEME_DEFAULT_CONFIG_PATH . 'section/category/product_list.json',
        'config_section_category_sections' => self::THEME_DEFAULT_CONFIG_PATH . 'section/category/sections.json',
        // contact
        'config_section_contact_sections' => self::THEME_DEFAULT_CONFIG_PATH . 'section/contact/sections.json',
        // product detail
        'config_section_product_detail_related_product' => self::THEME_DEFAULT_CONFIG_PATH . 'section/product_detail/related_product.json',
        'config_section_product_detail_sections' => self::THEME_DEFAULT_CONFIG_PATH . 'section/product_detail/sections.json',
        'config_section_product_template' => self::THEME_DEFAULT_CONFIG_PATH . 'section/product_detail/template.json',
        // rate
        'config_section_rate_sections' => self::THEME_DEFAULT_CONFIG_PATH . 'section/rate.json'
    ];

    public function isSupportKey($key)
    {
        return array_key_exists($key, self::$CONFIG_KEYS_DEFAULT_CONFIG_MAPPING);
    }

    public function getConfig($id)
    {
        $sql = sprintf("SELECT * FROM `%s` 
            WHERE id = '%s'",
            /* TABLE NAME */
            self::TABLE_NAME,
            /* WHERE */
            $this->db->escape($id)
        );

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'theme_builder_config:getConfig_' . md5($sql);
        $config = $this->cache->get($cache_key);
        if (!$config) {
            // get result from db
            $query = $this->db->query($sql);
            $config = $query->row;
            $this->cache->set($cache_key, $config);
        }

        return $config['config'];
    }

    public function getConfigByKeyForStoreId($store_id, $key, $code = 'config')
    {
        if (!$this->isSupportKey($key)) {
            return null;
        }

        $sql = sprintf("SELECT * FROM `%s` 
            WHERE store_id = '%s' AND `config_theme` = '%s' AND `key` = '%s' AND code = '%s'",
            /* TABLE NAME */
            self::TABLE_NAME,
            /* WHERE */
            $this->db->escape($store_id),
            $this->db->escape($this->getCurrentTheme()),
            $this->db->escape($key),
            $this->db->escape($code)
        );

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'theme_builder_config:getConfigByKeyForStoreId_' . md5($sql);
        $row = $this->cache->get($cache_key);
        if (!$row) {
            // get result from db
            $query = $this->db->query($sql);
            $row = $query->row;
            $this->cache->set($cache_key, $row);
        }

        $data_result = (!is_array($row) || empty($row)) ? null : $row['config'];
        /* if in preview get data session get current element */
        if (isset($this->session->data['theme_builder_config'][$this->getCurrentTheme()][$key]['data']) && isset($this->request->get['config'])) {
            $index_element_config = $this->session->data['theme_builder_config'][$this->getCurrentTheme()][$key]['index'];
            $data_result = $this->session->data['theme_builder_config'][$this->getCurrentTheme()][$key]['data'][$index_element_config];
        }

        // patch default config to current config for old/new key removed/added
        // this current makes many errors. TODO: fix later or remove if no need...
        //if ('config' == $code && !is_null($data_result)) {
        //    /* replace default config with result */
        //    $data_result = $this->mergeDefaultConfig($key, $data_result);
        //}

        return $data_result;
    }

    private function getCurrentTheme()
    {
        $current_theme = $this->config->get('config_theme');

        return isset($current_theme) ? $current_theme : 'default';
    }

    /**
     * merge result with default config
     * @param string $key
     * @param string $data_result
     * @return false|string
     */
    private function mergeDefaultConfig($key, $data_result)
    {
        $key_arr = explode('_', $key); // eg. config_theme_override_css
        if (!empty($key_arr) && count($key_arr) > 2) {
            if (isset(static::$THEME_DEFAULT_CONFIG_SECTION_SUB_DIRECTORY[$key])) {
                // case: file default config in sub directory
                $default_config_path = static::$THEME_DEFAULT_CONFIG_SECTION_SUB_DIRECTORY[$key];
            } else {
                $config_key_type = $key_arr[1];
                $config_key_name = substr($key, strlen($key_arr[0] . '_' . $key_arr[1] . '_'));
                $default_config_path = self::THEME_DEFAULT_CONFIG_PATH . $config_key_type . '/' . $config_key_name . '.json';
            }

            if (file_exists($default_config_path) && is_readable($default_config_path)) {
                $default_config = file_get_contents($default_config_path);
                if (false !== $default_config) {
                    $default_config = json_decode($default_config, true);
                    if (is_array($default_config) && !empty($default_config)) {
                        $data_result_decode = json_decode($data_result, true);
                        if (!is_null($data_result_decode)) {
                            $data_result = json_encode(array_replace_recursive($default_config, $data_result_decode));
                        }
                    }
                }
            }
        }

        return $data_result;
    }
}