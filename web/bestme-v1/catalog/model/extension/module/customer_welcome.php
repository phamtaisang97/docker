<?php

class ModelExtensionModuleCustomerWelcome extends Model
{
    public function addConfig($customer_id, $data)
    {
        $this->db->query("INSERT INTO `" . DB_PREFIX . "customer_welcome_config` SET `customer_id` = '" . $this->db->escape($customer_id) . "', `is_shown_welcome_message` = '" . $this->db->escape($data['is_shown_welcome_message']) . "'");
    }

    public function editConfig($customer_id, $data)
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "customer_welcome_config` SET `is_shown_welcome_message` = '" . $this->db->escape($data['is_shown_welcome_message']) . "' WHERE `customer_id` = '" . $customer_id . "'");
    }

    public function deleteConfig($customer_id)
    {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "customer_welcome_config` WHERE `customer_id` = '" . $customer_id . "'");
    }

    public function getConfig($customer_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_welcome_config` WHERE `customer_id` = '" . $customer_id . "'");

        return $query->row;
    }

    public function getConfigs()
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_welcome_config` ORDER BY `customer_id`");

        return $query->rows;
    }
}