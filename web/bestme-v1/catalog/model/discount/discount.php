<?php

class ModelDiscountDiscount extends Model
{
    use RabbitMq_Trait;

    const INPUT_DATETIME_FORMAT = 'Y-m-d H:i:s';
    const DISCOUNT_DATETIME_FORMAT = 'Y-m-d H:i';

    public static $SOURCE_WEBSITE = 'website';
    public static $SOURCE_ADMIN = 'admin';
    public static $SOURCE_POS = 'pos';
    public static $SOURCE_CHATBOT = 'chatbot';
    public static $SOURCE_OTHER = 'other';

    public function checkDiscountValidById($discount_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}discount` 
                    WHERE `discount_id` = {$discount_id} 
                        AND `date_started` <= NOW() 
                        AND `date_ended` >= NOW() 
                        AND status = 1";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getDiscountsForProductActiveNow() // only get discounts with status 2: Activated; AND with type = product, category, $manufacturer
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "discount` d LEFT JOIN `" . DB_PREFIX . "discount_description` dd ON d.`discount_id` = dd.`discount_id` 
                    WHERE (d.`times_used` < d.`usage_limit` OR d.`usage_limit` = 0) 
                    AND d.`discount_type_id` IN (2, 3, 4) 
                    AND d.`start_at` <= NOW() AND 
                        (d.`end_at` IS NULL OR d.`end_at` = '0000-00-00 00:00:00' OR d.`end_at` >= NOW()) 
                    AND d.`discount_status_id` = 2";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getDiscountsForOrderActiveNow() // only get discounts with status 2: Activated; AND with type = order
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "discount` d LEFT JOIN `" . DB_PREFIX . "discount_description` dd ON d.`discount_id` = dd.`discount_id`
                    WHERE (d.`times_used` < d.`usage_limit` OR d.`usage_limit` = 0) 
                    AND d.`discount_type_id` = 1 
                    AND d.`start_at` <= NOW() AND 
                        (d.`end_at` IS NULL OR d.`end_at` = '0000-00-00 00:00:00' OR d.`end_at` >= NOW()) 
                    AND d.`discount_status_id` = 2";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getDiscountNameByDiscountId($discount_id)
    {
        $sql = "SELECT `name` FROM `" . DB_PREFIX ."discount_description` WHERE `discount_id` = " . (int)$discount_id . " AND `language_id` = " .(int)$this->config->get('config_language_id');
        $query = $this->db->query($sql);

        if ($query->num_rows){
            return $query->row['name'];
        }

        return '';
    }

    public function getDiscountsByStatus($discount_status_id)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "discount` d 
                LEFT JOIN `" . DB_PREFIX . "discount_description` dd ON d.`discount_id` = dd.`discount_id`
                WHERE `discount_status_id` = " . (int)$discount_status_id;
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getActivatedDiscounts()
    {
        return $this->getDiscountsByStatus($type_id = 2); //ModelDiscountDiscountStatus::STATUS_ACTIVATED);
    }

    public function getStoreIdsByDiscountId($discount_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $query = $this->db->query("SELECT * FROM {$DB_PREFIX}discount_to_store as dts 
                                   WHERE discount_id = '" . (int)$discount_id . "'");

        return $query->rows;
    }

    public function addOrderDiscount($order_id, $discount_ids)
    {
        if (!$order_id || !is_array($discount_ids) || empty($discount_ids)){
            return;
        }

        $this->db->query("DELETE FROM `" . DB_PREFIX ."order_to_discount` WHERE `order_id` = " . (int)$order_id);
        foreach ($discount_ids as $discount_id) {
            if (!$discount_id){
                continue;
            }

            $this->db->query("INSERT INTO `" . DB_PREFIX . "order_to_discount` SET `order_id` = " . (int)$order_id . ", `discount_id` = " .(int)$discount_id);

            // update used_times. Notice: support parallel updating using COUNT() instead of increase by 1
            $this->updateUsedTimesByDiscountId($discount_id);
        }
    }

    public function countDiscountsUsedTimesByDiscountId($discount_id)
    {
        $query = $this->db->query($this->getCountUsedTimesByDiscountIdSql($discount_id));

        return $query->rows;
    }

    public function getDiscountById($discount_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "discount` WHERE `discount_id` = " . (int)$discount_id);

        return $query->row;
    }

    public function updateUsedTimesByDiscountId($discount_id)
    {
        $discount_id = (int)$discount_id;
        $count_sql = $this->getCountUsedTimesByDiscountIdSql($discount_id);
        if (empty($count_sql)) {
            return 0;
        }

        $DB_PREFIX = DB_PREFIX;
        $sql = "UPDATE `{$DB_PREFIX}discount` d SET d.`times_used` = ({$count_sql}) 
                WHERE d.`discount_id` = {$discount_id}";

        return $this->db->query($sql);
    }

    public function getDiscountsResult($data)
    {
        $this->load->language('discount/discount');

        $data_discounts = [];
        $discounts = $this->getDiscounts($data);
        $count_total_discount = $this->countTotalDiscounts($data);

        $DISCOUNT_STATUS = [
            // Scheduled
            1 => [
                'status' => $this->language->get('txt_scheduled')
            ],
            //Activated
            2 => [
                'status' => $this->language->get('txt_active')
            ],
            // Paused
            3 => [
                'status' => $this->language->get('txt_paused')
            ],
            // Deleted
            4 => [
                'status' => $this->language->get('txt_deleted')
            ]
        ];

        foreach ($discounts as $result) {
            // modify config add identify fields
            $configs = json_decode($result['config'], true);
            switch ($result['discount_type_id']) {
                case 2:
                    $configs = $this->modifyProductConfig($configs);
                    break;
                case 3:
                    $configs = $this->modifyCategoryConfig($configs);
                    break;
                case 4:
                    $configs = $this->modifyManufacturerConfig($configs);
                    break;
            }

            $configs = json_encode($configs);

            $data_discounts[] = array(
                'discount_id' => $result['discount_id'],
                'name' => $result['discount_name'],
                'code' => $result['code'],
                'status' => $DISCOUNT_STATUS[$result['discount_status_id']]['status'],
                'usage_limit' => $result['usage_limit'],
                'times_used' => $result['times_used'],
                'start_at' => $result['start_at'],
                'end_at' => $result['end_at'],
                'discount_type_id' => $result['discount_type_id'],
                'discount_status_id' => $result['discount_status_id'],
                'config' => $configs,
                'order_source' => $result['order_source'],
                'customer_group' => $result['customer_group'],
                'description' => $result['description']
            );
        }

        $json = [
            'total' => $count_total_discount,
            'page' => (isset($this->request->get['page']) && !empty($this->request->get['page'])) ? $this->request->get['page'] : '',
            'records' => $data_discounts
        ];

        return $json;
    }

    public function getDiscounts($data)
    {
        $sql = $this->getDiscountsSql($data);
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function countTotalDiscounts($data = array())
    {
        $sqlAll = $this->getDiscountsSql($data);
        $query = $this->db->query("SELECT COUNT(1) AS `total` FROM (" . $sqlAll . ") as discount");

        return $query->row['total'];
    }

    /**
     * @param $data
     */
    public function addDiscount($data)
    {
        $start_at = $data['start_at'];
        $end_at = '';
        if (isset($data['discount_time_has_end']) && $data['discount_time_has_end'] == 1) {
            $end_at = $data['end_at'];
        }

        $end_at = isset($data['end_at']) && !empty($data['end_at']) ? $data['end_at'] : '';

        // auto start_at
        $discount_status_id = isset($data['discount_status_id']) ? $data['discount_status_id'] : 1;
        $data['discount_status_id'] = $discount_status_id = $this->autoChangeDiscountStatusId($discount_status_id, $start_at, $end_at);

        if (isset($data['all_sources']) && $data['all_sources'] == 1) {
            $order_source = 'ALL';
        } elseif (!empty($data['source_list'])) {
            $order_source = json_encode($data['source_list']);
        } else {
            $order_source = empty($data['order_source']) ? '' : $data['order_source'];
        }

        if (isset($data['usage_limitless']) && $data['usage_limitless'] == 1) {
            $usage_limit = 0;
        } else {
            $usage_limit = isset($data['usage_limit']) ? $data['usage_limit'] : 0;
        }

        $discount_type = isset($data['discount_type_id']) ? (int)$data['discount_type_id'] : 0;
        $user_create_id = isset($data['user_create_id']) ? (int)$data['user_create_id'] : 1;

        $sql = "INSERT INTO `" . DB_PREFIX . "discount` SET `code` = '" . $this->db->escape($data['discount_code']) . "',
                                                            `discount_type_id` = '" . (int)$discount_type . "',
                                                            `discount_status_id` = '" . (int)$discount_status_id . "',
                                                            `usage_limit` = '" . (int)$usage_limit . "', 
                                                            `times_used` = 0, 
                                                            `config` = '" . $data['config'] . "',
                                                            `order_source` = '" . $this->db->escape($order_source) . "',
                                                            `start_at` = '" . $this->db->escape($start_at) . "',
                                                            `end_at` = '" . $this->db->escape($end_at) . "', 
                                                            `user_create_id` = '" . $this->db->escape($user_create_id) . "', 
                                                            `source` = '" . $this->db->escape($data['source']) . "', 
                                                            `created_at` = NOW()
                                                            ";

        $this->db->query($sql);
        $discount_id = $this->db->getLastId();

        if (isset($data['name'])) {
            $language_id = (int)$this->config->get('config_language_id');
            $data['description'] = isset($data['description']) ? $data['description'] : '';
            $this->db->query("INSERT INTO " . DB_PREFIX . "discount_description SET `discount_id` = '" . (int)$discount_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "'");
        }

        if (isset($data['all_stores']) && $data['all_stores'] == 1) {
            $store_list = $this->db->query("SELECT `store_id` FROM `" . DB_PREFIX . "store`");

            $store_list = array_map(function ($item) {
                return $item['store_id'];
            }, $store_list->rows);
        } else {
            $store_list = isset($data['store_list']) && is_array($data['store_list']) ? $data['store_list'] : [];
        }
        foreach ($store_list as $store) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "discount_to_store SET `discount_id` = '" . (int)$discount_id . "', store_id = '" . (int)$store . "'");
        }

        $this->sendDiscountBrokerMessage($discount_id);
    }

    /**
     * @param $discount_id
     * @param $data
     */
    public function editDiscount($discount_id, $data)
    {
        // get old discount info
        $old_discount = $this->getDiscountInfo($discount_id);

        $start_at = $data['start_at'];
        $end_at = '';
        if (isset($data['discount_time_has_end']) && $data['discount_time_has_end'] == 1) {
            $end_at = $data['end_at'];
        }

        $end_at = isset($data['end_at']) && !empty($data['end_at']) ? $data['end_at'] : '';

        // auto start_at
        $discount_status_id = isset($data['discount_status_id']) ? $data['discount_status_id'] : 1;
        $data['discount_status_id'] = $discount_status_id = $this->autoChangeDiscountStatusId($discount_status_id, $start_at, $end_at);

        if (isset($data['all_sources']) && $data['all_sources'] == 1) {
            $order_source = 'ALL';
        } elseif (!empty($data['source_list'])) {
            $order_source = json_encode($data['source_list']);
        } else {
            $order_source = empty($data['order_source']) ? '' : $data['order_source'];
        }

        if (isset($data['usage_limitless']) && $data['usage_limitless'] == 1) {
            $usage_limit = 0;
        } else {
            $usage_limit = isset($data['usage_limit']) ? $data['usage_limit'] : 0;
        }

        $discount_type = isset($data['discount_type_id']) ? (int)$data['discount_type_id'] : 0;
        $source_query = empty($data['source']) ? '' : " `source` = '{$this->db->escape($data['source'])}', ";
        $sql = "UPDATE `" . DB_PREFIX . "discount` SET `code` = '" . $this->db->escape($data['discount_code']) . "',
                                                            `discount_type_id` = '" . $discount_type . "', 
                                                            `discount_status_id` = '" . (int)$discount_status_id . "', 
                                                            `usage_limit` = '" . (int)$usage_limit . "', 
                                                            `times_used` = 0, 
                                                            `config` = '" . $data['config'] . "',
                                                            `order_source` = '" . $this->db->escape($order_source) . "',
                                                            `start_at` = '" . $this->db->escape($start_at) . "',
                                                            {$source_query}
                                                            `end_at` = '" . $this->db->escape($end_at) . "'
                                                             WHERE `discount_id` = " . (int)$discount_id . "
                                                            ";

        $this->db->query($sql);

        $this->db->query("DELETE FROM " . DB_PREFIX . "discount_description WHERE discount_id = '" . (int)$discount_id . "'");
        if (isset($data['name'])) {
            $language_id = (int)$this->config->get('config_language_id');
            $data['description'] = isset($data['description']) ? $data['description'] : '';
            $this->db->query("INSERT INTO " . DB_PREFIX . "discount_description SET `discount_id` = '" . (int)$discount_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "discount_to_store WHERE discount_id = '" . (int)$discount_id . "'");
        if (isset($data['all_stores']) && $data['all_stores'] == 1) {
            $store_list = $this->db->query("SELECT `store_id` FROM `" . DB_PREFIX . "store`");

            $store_list = array_map(function ($item) {
                return $item['store_id'];
            }, $store_list->rows);
        } else {
            $store_list = isset($data['store_list']) && is_array($data['store_list']) ? $data['store_list'] : [];
        }

        foreach ($store_list as $store) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "discount_to_store SET `discount_id` = '" . (int)$discount_id . "', store_id = '" . (int)$store . "'");
        }

        $this->sendDiscountBrokerMessage($discount_id, $old_discount['code']);
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getDiscountByName($name)
    {
        $sql = "select dd.name as discount_name, d.code as discount_code, d.discount_id  from  " . DB_PREFIX . "discount d 
                left join " . DB_PREFIX . "discount_description dd ON (dd.discount_id = d.discount_id)";
        $sql .= "WHERE dd.language_id = " . (int)$this->config->get('config_language_id') . "
                 AND d.discount_status_id <> '4'";

        if (isset($name) && $name !== '') {
            $sql .= " AND (dd.`name` LIKE '%" . $this->db->escape(trim($name)) . "%' 
                      OR d.`code` LIKE '%" . $this->db->escape(trim($name)) . "%')";
        }
        $sql = $sql . " group by d.discount_id";
        $sql .= " ORDER BY d.discount_status_id = '3'
                           , d.discount_status_id = '1'
                           , d.discount_status_id = '2'
                           , d.start_at DESC";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @param $code
     * @param $discount_id
     * @return bool
     */
    public function validateCodeExist($code, $discount_id)
    {
        $sql = "SELECT COUNT(DISTINCT discount_id) as total FROM `" . DB_PREFIX . "discount` WHERE `code` = '" . $this->db->escape($code) . "'";
        if (!empty($discount_id)) {
            $sql .= " AND `discount_id` != " . (int)$discount_id;
        }

        $query = $this->db->query($sql);
        if ($query->row['total'] != 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $discount_id
     * @return mixed
     */
    public function getDiscountInfo($discount_id)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "discount` d LEFT JOIN `" . DB_PREFIX . "discount_description` dd ON dd.`discount_id` = d.`discount_id` 
                                WHERE d.`discount_id` = " . (int)$discount_id . " AND dd.`language_id` = " . (int)$this->config->get('config_language_id');

        $query = $this->db->query($sql);

        return $query->row;
    }

    /**
     * @param $type_id
     * @param $start
     * @param $end
     * @param $discount_id
     * @return mixed
     */
    public function getDiscountsWithTypeAndDate($type_id, $start, $end, $discount_id) // only get discounts with status 1: Scheduled; 2: Activated;
    {
        if (!$end || $end == '0000-00-00 00:00:00'){
            $sql = "SELECT * FROM `" . DB_PREFIX . "discount` d WHERE `discount_type_id` = " . (int)$type_id . " AND `discount_id` != " . (int)$discount_id . " AND 
                        (
                            (`start_at` >= '" . $this->db->escape($start) . "') 
                            OR (`start_at` < '" . $this->db->escape($start) . "' 
                                AND (`end_at` >= '" . $this->db->escape($start) . "' 
                                    OR `end_at` IS NULL OR `end_at` = '0000-00-00 00:00:00'
                                )
                            )
                        )";
        }else{
            $sql = "SELECT * FROM `" . DB_PREFIX . "discount` d WHERE `discount_type_id` = " . (int)$type_id . " AND `discount_id` != " . (int)$discount_id . " AND 
                        (
                            (`start_at` >= '" . $this->db->escape($start) . "' 
                                AND `start_at` <= '" . $this->db->escape($end) . "'
                            ) 
                            OR (`end_at` >= '" . $this->db->escape($start) . "' 
                                AND `end_at` <= '" . $this->db->escape($end) . "'
                            ) 
                            OR (`start_at` <= '" . $this->db->escape($start) . "' 
                                AND `end_at` >= '" . $this->db->escape($end) . "' 
                                ) 
                            OR (
                                (`end_at` IS NULL OR `end_at` = '0000-00-00 00:00:00')
                                AND (`start_at` <= '" . $this->db->escape($end) ."')
                            )
                        )";
        }

        $sql .= " AND discount_status_id IN (1, 2)";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    private function autoChangeDiscountStatusId($discount_status_id, $start_at, $end_at)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $now = new DateTime();
        $start_at = $start_at ? new DateTime($start_at) : '';
        $end_at = $end_at ? new DateTime($end_at) : '';

        //if (in_array($discount_status_id, [ModelDiscountDiscountStatus::STATUS_PAUSED, ModelDiscountDiscountStatus::STATUS_DELETED])) {
        if (in_array($discount_status_id, [4])) {
            return $discount_status_id;
        }

        if ($start_at <= $now && ((!$end_at instanceof DateTime) || $end_at >= $now)) {
            //return ModelDiscountDiscountStatus::STATUS_ACTIVATED;
            return 2;
        }

        if ($start_at > $now && ((!$end_at instanceof DateTime) || $end_at >= $now)) {
            //return ModelDiscountDiscountStatus::STATUS_SCHEDULED;
            return 1;
        }

        if ($start_at < $now && (($end_at instanceof DateTime) && $end_at < $now)) {
            //return ModelDiscountDiscountStatus::STATUS_PAUSED;
            return 3;
        }

        return $discount_status_id;
    }

    private function getDiscountsSql($data)
    {
        $sql = "select d.*, dd.name as discount_name, ds.name as discount_status, dd.`description`  from  " . DB_PREFIX . "discount d 
                left join " . DB_PREFIX . "discount_description dd ON (dd.discount_id = d.discount_id)
                left join " . DB_PREFIX . "discount_type dt ON (dt.discount_type_id = d.discount_type_id)
                right join " . DB_PREFIX . "discount_status ds ON (d.discount_status_id = ds.discount_status_id) ";

        $sql .= " WHERE dd.language_id = '" . (int)$this->config->get('config_language_id') . "'
                    AND ds.language_id = '" . (int)$this->config->get('config_language_id') . "'
                    AND d.discount_status_id <> '4' ";

        if (!empty($data['filter_discount_id'])) {
            $sql .= " AND d.discount_id = ". $data['filter_discount_id'] ;
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND (dd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'
            OR d.`code` LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%')";
        }

        if (isset($data['discount_type_id']) && $data['discount_type_id'] != '') {
            $sql .= ' AND d.discount_type_id IN (' . $data['discount_type_id'] . ')';
        }

        if (isset($data['source']) && !empty($data['source'])) {
            $sql .= " AND d.source = '" . $this->db->escape($data['source']) . "' ";
        }

        $sql .= " GROUP BY d.discount_id";
        $sql .= " ORDER BY d.discount_status_id = '3'
                           , d.discount_status_id = '1'
                           , d.discount_status_id = '2'
                           , d.created_at DESC";

        return $sql;
    }

    private function getCountUsedTimesByDiscountIdSql($discount_id)
    {
        if (!$discount_id) {
            return '';
        }

        $DB_PREFIX = DB_PREFIX;
        $discount_id = (int)$discount_id;
        $sql = "SELECT COUNT(otd.`order_id`) as used_times 
                FROM `{$DB_PREFIX}order_to_discount` otd 
                LEFT JOIN `{$DB_PREFIX}order` o ON otd.`order_id` = o.`order_id` 
                WHERE o.`order_status_id` IN (7, 8, 9) 
                AND otd.`discount_id` = {$discount_id}";

        return $sql;
    }

    /**
     * @param $configs
     * @return array
     */
    private function modifyProductConfig($configs) {
        $this->load->model('catalog/product');
        foreach ($configs as $key => $config) {
            if (0 == $config['all_product']) {
                // add product sku
                $product = $this->model_catalog_product->getProductOptimize($config['product_id']);
                if (!empty($product['sku'])) {
                    $configs[$key]['product_sku'] = $product['sku'];
                }

                // add product version sku
                $product_versions = $this->model_catalog_product->getProductVersions($config['product_id'], true);
                $product_version_skus = [];
                foreach ($config['product_version_ids'] as $product_version_id) {
                    if (!empty($product_versions[$product_version_id]['sku'])) {
                        $product_version_skus[] = $product_versions[$product_version_id]['sku'];
                    }
                }
                $configs[$key]['product_version_skus'] = $product_version_skus;
            }
        }

        return $configs;
    }

    /**
     * @param $configs
     * @return array
     */
    private function modifyCategoryConfig($configs) {
        $this->load->model('catalog/category');

        foreach ($configs as $key => $config) {
            if (0 == $config['all_category']) {
                // add category name
                $category = $this->model_catalog_category->getCategory($config['category_id']);
                if (!empty($category['name'])) {
                    $configs[$key]['category_name'] = $category['name'];
                }
            }
        }

        return $configs;
    }

    /**
     * @param $configs
     * @return array
     */
    private function modifyManufacturerConfig($configs) {
        $this->load->model('catalog/manufacturer');

        foreach ($configs as $key => $config) {
            if (0 == $config['all_manufacturer']) {
                // add manufacturer name
                $manufacturer = $this->model_catalog_manufacturer->getManufacturer($config['manufacturer_id']);
                if (!empty($manufacturer['name'])) {
                    $configs[$key]['manufacturer_name'] = $manufacturer['name'];
                }
            }
        }

        return $configs;
    }

    /**
     * send discount message to RabbitMQ
     *
     * @param $discount_id
     * @param string $old_discount_code
     */
    private function sendDiscountBrokerMessage($discount_id, $old_discount_code = '')
    {
        try {
            if (!empty($discount_id)) {
                $discount = $this->getDiscountInfo($discount_id);
                if (!empty($discount)) {
                    $message = [
                        'shop_name' => $this->config->get('shop_name'),
                        'discount' => [
                            'name' => $discount['name'],
                            'description' => $discount['description'],
                            'code' => $discount['code'],
                            'old_code' => $old_discount_code,
                            'discount_type_id' => $discount['discount_type_id'],
                            'discount_status_id' => $discount['discount_status_id'],
                            'usage_limit' => $discount['usage_limit'],
                            'times_used' => $discount['times_used'],
                            'config' => $discount['config'],
                            'order_source' => $discount['order_source'],
                            'customer_group' => $discount['customer_group'],
                            'start_at' => $discount['start_at'],
                            'end_at' => $discount['end_at']
                        ]
                    ];

                    $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'discount', '', 'discount', json_encode($message));
                } else {
                    $this->log->write('Send message discount error: Could not get discount data for blog id ' . $discount_id);
                }
            } else {
                $this->log->write('Send message discount error: Missing discount_id ');
            }
        } catch (Exception $exception) {
            $this->log->write('Send message discount error: ' . $exception->getMessage());
        }
    }
}