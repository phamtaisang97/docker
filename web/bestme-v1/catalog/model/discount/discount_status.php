<?php

class ModelDiscountDiscountStatus extends Model
{
    const STATUS_SCHEDULED = 1;
    const STATUS_ACTIVATED = 2;
    const STATUS_PAUSED = 3;
    const STATUS_DELETED = 4;

    public function getDiscountStatus($data = array())
    {
        $language_id = (int)$this->config->get('config_language_id');
        $sql = "SELECT * FROM " . DB_PREFIX . "discount_status WHERE language_id = $language_id ";

        if (!empty($data['filter_name'])) {
            $sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }


        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }
}
