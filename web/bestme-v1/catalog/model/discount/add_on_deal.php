<?php

class ModelDiscountAddOnDeal extends Model
{
    public function getAddOnDealNameByProductId($product_id, $product_version_id = 0)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT aod.name FROM `{$DB_PREFIX}add_on_deal_main_product` AS aodmp 
                    JOIN `{$DB_PREFIX}add_on_deal` AS aod ON aodmp.add_on_deal_id = aod.add_on_deal_id 
                        WHERE aodmp.product_id = {$product_id} 
                            AND aodmp.status = 1 
                            AND aod.date_started <= NOW() 
                            AND aod.date_ended >= NOW() 
                            AND aod.status = 1 
                            AND aod.deleted IS NULL";

        $query = $this->db->query($sql);
        if (isset($query->row['name'])) {
            return $query->row['name'];
        }

        return false;
    }

    public function getAddOnDealByProductId($product_id, $product_version_id = 0)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT aod.* FROM `{$DB_PREFIX}add_on_deal_main_product` AS aodmp 
                    JOIN `{$DB_PREFIX}add_on_deal` AS aod ON aodmp.add_on_deal_id = aod.add_on_deal_id 
                        WHERE aodmp.product_id = {$product_id} 
                            AND aodmp.product_version_id = {$product_version_id} 
                            AND aodmp.status = 1 
                            AND aod.date_started <= NOW() 
                            AND aod.date_ended >= NOW() 
                            AND aod.status = 1 
                            AND aod.deleted IS NULL";

        $query = $this->db->query($sql);
        if (isset($query->row)) {
            return $query->row;
        }

        return [];
    }

    public function getFullAddOnProductById($deal_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT aop.*, p.user_create_id, 
                            p.sku as p_sku, p.multi_versions, 
                            p.compare_price as p_compare_price, 
                            p.weight as weight, 
                            p.tax_class_id, 
                            pd.name, p.image, 
                            pts.quantity as pts_quantity, 
                            pv.version, pv.compare_price as pv_compare_price, 
                            pv.sku as pv_sku  
                            FROM `{$DB_PREFIX}add_on_product` as aop 
                            INNER JOIN `{$DB_PREFIX}product` as p ON (p.product_id = aop.product_id) 
                            INNER JOIN `{$DB_PREFIX}product_description` as pd ON (pd.product_id = aop.product_id) 
                            LEFT JOIN `{$DB_PREFIX}product_version` as pv ON (pv.product_id = aop.product_id AND 
                                                                        CASE WHEN aop.product_version_id <> 0 
                                                                            THEN aop.product_version_id ELSE -1 END = pv.product_version_id) 
                            INNER JOIN `{$DB_PREFIX}product_to_store` as pts ON (aop.product_id = pts.product_id AND 
                                                                        CASE WHEN aop.product_version_id <> 0 
                                                                            THEN aop.product_version_id ELSE 0 END = pts.product_version_id) 
                 WHERE aop.add_on_deal_id = {$deal_id}";


        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getAddOnDealByAddOnProductId($product_id, $product_version_id = 0)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT aod.* FROM `{$DB_PREFIX}add_on_product` AS aop 
                    JOIN `{$DB_PREFIX}add_on_deal` AS aod ON aop.add_on_deal_id = aod.add_on_deal_id 
                        WHERE aop.product_id = {$product_id} 
                            AND aop.product_version_id = {$product_version_id} 
                            AND aod.date_started <= NOW() 
                            AND aod.date_ended >= NOW() 
                            AND aod.status = 1 
                            AND aod.deleted IS NULL";

        $query = $this->db->query($sql);
        if (isset($query->row)) {
            return $query->row;
        }

        return [];
    }
}