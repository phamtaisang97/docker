<?php
class ModelBlogBlog extends Model {
    use RabbitMq_Trait;

    public function getAllBlogTag()
    {
        $sql = "SELECT * FROM ". DB_PREFIX ."blog_tag as bt INNER JOIN ". DB_PREFIX ."tag as t ON (t.tag_id = bt.tag_id) WHERE 1=1";
        $sql .= " GROUP BY t.tag_id";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getALlBlogCategory()
    {
        $sql = "SELECT * FROM ". DB_PREFIX ."blog_category as bc INNER JOIN ". DB_PREFIX ."blog_category_description as bcd ON (bcd.blog_category_id = bc.blog_category_id) WHERE 1=1 AND bc.status = 1";
        $sql .= " AND bcd.language_id = '". (int)$this->config->get('config_language_id') ."'";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getFullBlogCategories($parentId = 0, $level = 0)
    {
        $result = [];
        $sql = "SELECT * 
                      FROM ". DB_PREFIX ."blog_category as bc INNER JOIN ". DB_PREFIX ."blog_category_description as bcd ON (bcd.blog_category_id = bc.blog_category_id)
                      WHERE bc.`parent_id` = " . (int)$parentId . " 
                      AND bcd.`language_id` = " . (int)$this->config->get('config_language_id');
        $query = $this->db->query($sql);

        foreach ($query->rows as $row) {
            $children = $this->getFullBlogCategories($row['blog_category_id'], $level + 1);
            $sub_ids = [];
            foreach ($children as $child) {
                $sub_ids[] = $child['blog_category_id'];
                if (isset($child['sub_ids']) && is_array($child['sub_ids'])){
                    $sub_ids = array_merge($sub_ids, $child['sub_ids']);
                }
            }

            $result[] = [
                'blog_category_id' => $row['blog_category_id'],
                'title' => html_entity_decode($row['title']), // return a&b instead of a&amp;b
                'name' => html_entity_decode($row['title']), // return a&b instead of a&amp;b
                'hasChildren' => count($children) > 0 ? true : false,
                'sub_ids' => $sub_ids,
                'list_sub' => $children,
                'level' => $level,
                'parent_id' => $parentId,
                'url' => $this->url->link('blog/blog', 'blog_category_id=' . $row['blog_category_id'], true),
                'href' => $this->url->link('blog/blog', 'blog_category_id=' . $row['blog_category_id'], true),
            ];
        }

        return $result;
    }

    // TODO: correct name to getBlogCategoryByBlogCategoryId or remove this due to duplicate function...
    public function getTitleCategory($blog_category)
    {
        $sql = "SELECT * FROM ". DB_PREFIX ."blog_category as bc INNER JOIN ". DB_PREFIX ."blog_category_description as bcd ON (bcd.blog_category_id = bc.blog_category_id) WHERE 1=1 AND bc.status = 1";
        $sql .= " AND bc.blog_category_id = '" . (int)$blog_category. "'";
        $sql .= " AND bcd.language_id = '". (int)$this->config->get('config_language_id') ."'";
        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getBlogList($filter)
    {
        $db_prefix = DB_PREFIX;
        $sql = "SELECT b.*, bd.*, bcd.`title` as blog_category_title, bcd.`blog_category_id`, u.`firstname`, u.`lastname`";
        $tag_condition = empty($filter['not_load_tag']);
        if ($tag_condition) {
            $sql .= " , `t`.*";
        }
        $sql .= " FROM `{$db_prefix}blog` b 
                LEFT JOIN `{$db_prefix}blog_description` bd ON (b.`blog_id` = bd.`blog_id`) 
                LEFT JOIN `{$db_prefix}user` u ON (b.`author` = u.`user_id`)
                LEFT JOIN `{$db_prefix}blog_to_blog_category` btbc ON (b.`blog_id` = btbc.`blog_id`)
                LEFT JOIN `{$db_prefix}blog_category_description` bcd ON (bcd.`blog_category_id` = btbc.`blog_category_id`) ";

        if ($tag_condition) {
            $sql .= " LEFT JOIN `{$db_prefix}blog_tag` bt ON (bt.`blog_id` = b.`blog_id`) LEFT JOIN `{$db_prefix}tag` t ON (t.`tag_id` = bt.`tag_id`)";
        }

        $sql .= " WHERE 1=1";
        $sql .= " AND b.status = 1";
        $sql .= " AND bd.language_id = '". (int)$this->config->get('config_language_id') ."'";
        $sql .= " AND bcd.language_id = '". (int)$this->config->get('config_language_id') ."'";

        if (isset($filter['blog_category_id'])) {
            $sql .= " AND btbc.blog_category_id = '". (int)$filter['blog_category_id'] ."' ";
        }

        if (!empty($filter['categories_ids'])) {
            $sql .= " AND btbc.blog_category_id IN (". implode(',', $filter['categories_ids']) .") ";
        }

        if (isset($filter['blog_category_title'])) {
            $sql .= " AND bcd.title = '". $filter['blog_category_title'] ."' ";
        }

        if (isset($filter['tag_id'])) {
            $sql .= " AND t.tag_id = '". (int)$filter['tag_id'] ."' ";
        }

        if (isset($filter['in_home'])) {
            $sql .= " AND b.in_home = '". (int)$filter['in_home'] ."' ";
        }

        if (isset($filter['blog_id'])) {
            $sql .= " AND b.blog_id = '". (int)$filter['blog_id'] ."' ";
        }

        if (isset($filter['out_blog_id']) &&
            !empty($filter['out_blog_id']) &&
            isset($filter['out_blog_id']['blog_id']) &&
            isset($filter['out_blog_id']['blog_categories'])
        ) {
            $sql .= " AND b.blog_id != '". (int)$filter['out_blog_id']['blog_id'] ."' ";
            $category = [];
            foreach ($filter['out_blog_id']['blog_categories'] as $blog_category) {
                $category[] = $blog_category['blog_category_id'];
            }
            if (!empty($category)) {
                $sql .= " AND bcd.blog_category_id IN (". implode(',', $category) .") ";
            }
        }

        if (isset($filter['date_time'])) {
            $timezone = ini_get('date.timezone') ? ini_get('date.timezone') : date_default_timezone_get();
            $now = new DateTime('now', new DateTimeZone($timezone));
            $start_time = $now->modify('- 30 day')->format('Y-m-d');
            $sql .= " AND b.date_modified <= NOW() AND b.date_modified >= '" . $start_time ."'";
        }

        /* group by */
        $sql .= " GROUP BY b.blog_id";

        /* sort by */
        $sort_data = [
            'b.blog_id',
            'b.date_added'
        ];

        if (isset($filter['sort'])) {
            $sql .= " ORDER BY " . $filter['sort'];
        } else {
            $sql .= " ORDER BY b.date_publish";
        }

        /* sort direction */
        if (isset($filter['order']) && ($filter['order'] == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " DESC";
        }

        if (isset($filter['start']) || isset($filter['limit'])) {
            if (!isset($filter['start']) || $filter['start'] < 0) {
                $filter['start'] = 0;
            }

            if (!isset($filter['limit']) || $filter['limit'] < 1) {
                $filter['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$filter['start'] . "," . (int)$filter['limit'];
        }

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'blog:getBlogList' . md5($sql);
        $blogs_list = $this->cache->get($cache_key);
        if ($blogs_list) {
            return $blogs_list;
        }

        $query = $this->db->query($sql);
        $this->cache->set($cache_key, $query->rows);
        return $query->rows;
    }

    public function getBlogListCount($filter)
    {
        $db_prefix = DB_PREFIX;
        $sql = "SELECT b.*, bd.*, bcd.`title` as blog_category_title, u.`firstname`, u.`lastname` ";
        $tag_condition = empty($filter['not_load_tag']);
        if ($tag_condition) {
            $sql .= " , `t`.*";
        }
        $sql .= " FROM `{$db_prefix}blog` b 
                LEFT JOIN `{$db_prefix}blog_description` bd ON (b.`blog_id` = bd.`blog_id`) 
                LEFT JOIN `{$db_prefix}user` u ON (b.`author` = u.`user_id`)
                LEFT JOIN `{$db_prefix}blog_to_blog_category` btbc ON (b.`blog_id` = btbc.`blog_id`)
                LEFT JOIN `{$db_prefix}blog_category_description` bcd ON (bcd.`blog_category_id` = btbc.`blog_category_id`) ";

        if ($tag_condition) {
            $sql .= " LEFT JOIN `{$db_prefix}blog_tag` bt ON (bt.`blog_id` = b.`blog_id`) LEFT JOIN `{$db_prefix}tag` t ON (t.`tag_id` = bt.`tag_id`) ";
        }

        $sql .= " WHERE 1=1";
        $sql .= " AND b.status = 1";

        $sql .= " AND bd.language_id = '". (int)$this->config->get('config_language_id') ."'";

        if (isset($filter['blog_category_id'])) {
            $sql .= " AND bcd.blog_category_id = '". (int)$filter['blog_category_id'] ."' ";
        }

        if (!empty($filter['categories_ids'])) {
            $sql .= " AND btbc.blog_category_id IN (". implode(',', $filter['categories_ids']) .") ";
        }

        if (isset($filter['tag_id'])) {
            $sql .= " AND t.tag_id = '". (int)$filter['tag_id'] ."' ";
        }

        if (isset($filter['blog_id'])) {
            $sql .= " AND b.blog_id = '". (int)$filter['blog_id'] ."' ";
        }

        $sql .= " GROUP BY b.blog_id";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'blog:getBlogListCount' . md5($sql);
        $blog_count = $this->cache->get($cache_key);
        if ($blog_count) {
            return $blog_count;
        }
        $query = $this->db->query($sql);
        $this->cache->set($cache_key, count($query->rows));
        return count($query->rows);
    }

    public function getCategoriesByBlog($blog_id)
    {
        $sql = "SELECT * FROM ". DB_PREFIX ."blog_to_blog_category as btbc LEFT JOIN ". DB_PREFIX ."blog_category_description as bcd ON (btbc.blog_category_id = bcd.blog_category_id) ";
        $sql .= "LEFT JOIN ". DB_PREFIX . "blog_category as bc ON (bcd.blog_category_id = bc.blog_category_id)";
        $sql .= "WHERE bc.status = 1 AND btbc.blog_id = '". (int)$blog_id ."'";
        $sql .= " AND bcd.language_id = '". (int)$this->config->get('config_language_id') ."'";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTagsByBlog($blog_id)
    {
        $sql = "SELECT * FROM ". DB_PREFIX ."blog_tag as bt INNER JOIN ". DB_PREFIX ."tag as t ON (t.tag_id = bt.tag_id) WHERE 1=1";
        $sql .= " AND bt.blog_id = '". (int)$blog_id ."'";
        $sql .= " GROUP BY t.tag_id";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function countBlogOfCategory($category_id)
    {
        $db_prefix = DB_PREFIX;
        $sql = "SELECT COUNT(*) as blog_count FROM `{$db_prefix}blog` b 
                LEFT JOIN `{$db_prefix}blog_to_blog_category` btbc ON (b.`blog_id` = btbc.`blog_id`)
                WHERE 1=1
                ";

        if (isset($category_id) && $category_id) {
            $sql .= " AND btbc.blog_category_id = '". (int)$category_id."' ";
        }

        $query = $this->db->query($sql);

        return isset($query->row['blog_count']) ? $query->row['blog_count'] : 0;
    }

    public function getBlogById($blog_id)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "blog b 
                LEFT JOIN " . DB_PREFIX . "blog_description bd ON (b.blog_id = bd.blog_id) 
                WHERE b.blog_id = $blog_id 
        ";
        $query = $this->db->query($sql);
        return $query->row;
    }

    /**
     * @param $title
     * @return mixed
     */
    public function getBlogByTitle($title)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "blog b 
                LEFT JOIN " . DB_PREFIX . "blog_description bd ON (b.blog_id = bd.blog_id) 
                WHERE bd.`title` = '$title'";
        $query = $this->db->query($sql);
        return $query->row;
    }

    // TODO: correct name to getBlogCategoryByBlogCategoryId
    public function getBlogCategoryByBlogId($blog_category_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX ."blog_category` WHERE `blog_category_id` = " . (int)$blog_category_id);

        if (count($query->rows) > 0) {
            return $query->row;
        }

        return null;
    }

    /**
     * get Blog by id
     *
     * @param int $blog_id
     * @return array
     */
    public function getBlog($blog_id)
    {
        $language_id = (int)$this->config->get('config_language_id');
        $sql = "SELECT b.*, bd.*, bcd.`title` as blog_category_title, u.`firstname`, u.`lastname`,t.* FROM " . DB_PREFIX . "blog b 
                LEFT JOIN `" . DB_PREFIX . "blog_description` bd ON (b.blog_id = bd.blog_id) 
                LEFT JOIN `" . DB_PREFIX . "user` u ON (b.`author` = u.`user_id`) 
                LEFT JOIN `" . DB_PREFIX . "blog_to_blog_category` btbc ON (b.`blog_id` = btbc.`blog_id`) 
                LEFT JOIN `" . DB_PREFIX . "blog_category_description` bcd ON (bcd.`blog_category_id` = btbc.`blog_category_id`) 
                LEFT JOIN `" . DB_PREFIX . "blog_tag` bt ON (bt.`blog_id` = b.`blog_id`) 
                LEFT JOIN `" . DB_PREFIX . "tag` t ON (t.`tag_id` = bt.`tag_id`) 
                WHERE 1=1 AND b.blog_id = '" . (int)$blog_id . "' AND bd.language_id = '" . $language_id . "'";
        $query = $this->db->query($sql);

        $blog = $query->row;

        // add more info blog_categories to blog
        $blog_category = $this->getCategoriesByBlog($blog_id);
        $blog['blog_categories'] = $blog_category ? $blog_category : [];

        return $blog;
    }

    /**
     * get Category Id By Blog id
     *
     * @param $blog_id
     * @return string|int empty if not found
     */
    public function getCategoryIdByBlog($blog_id)
    {
        $sql = "SELECT btbc.blog_category_id FROM ". DB_PREFIX ."blog_to_blog_category as btbc ";
        $sql .= "WHERE btbc.blog_id = '". (int)$blog_id ."'";
        $query = $this->db->query($sql);

        // TODO: support the blog belongs to many blog categories in future? ...
        return isset($query->row['blog_category_id']) ? $query->row['blog_category_id'] : '';
    }

    //v3.5.1

    /**
     * @param array $data
     * @return array
     */
    public function getAllBlogs($data = array())
    {
        $sql = $this->getAllBlogsSql($data);
        $products_total = $this->getTotalBlogs($data);
        $page = isset($data['page']) ? (int)$data['page'] : 1;
        if (isset($data['page']) && !empty($data['page'])) {
            $data = [
                'start' => ((int)$data['page'] - 1) * ((int)isset($data['limit']) ? $data['limit'] : 15),
                'limit' => isset($data['limit']) ? $data['limit'] : 15
            ];
        }

        // limit
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 15;
            }
            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        $result = [];
        $this->db->query($sql);
        foreach ($query->rows as $blog) {
            $result[] = [
                "blog_id" => $blog['blog_id'],
                "author" => $blog['author'],
                "status" => $blog['status'],
                "date_publish" => $blog['date_publish'],
                "date_added" => $blog['date_added'],
                "date_modified" => $blog['date_modified'],
                "title" => $blog['title'],
                "content" => $blog['content'],
                "short_content" => $blog['short_content'],
                "image" => $blog['image'],
                "meta_title" => $blog['meta_title'],
                "meta_description" => $blog['meta_description'],
                "seo_keywords" => $blog['seo_keywords'],
                "alias" => $blog['alias'],
                "alt" => $blog['alt'],
                "type" => $blog['type'],
                "video_url" => $blog['video_url'],
                "category" => [
                    "id" => $blog['blog_category_id'],
                    'status' => $blog['blog_category_status'],
                    'title' => $blog['blog_category_title'],
                    'meta_title' => $blog['blog_category_meta_title'],
                    'meta_description' => $blog['blog_category_meta_description'],
                    'alias' => $blog['blog_category_alias'],
                    'source' => $blog['blog_category_source']
                ],
                "firstname" => $blog['firstname'],
                "lastname" => $blog['lastname'],
                "author_email" => $blog['email'],
                "tags" => $this->getTagsByBlog($blog['blog_id']),
                "source" => $blog['source'],
                "source_author_name" => $blog['source_author_name']
            ];
        }
        return [
            'total' => $products_total,
            'page' => $page,
            'records' => $result
        ];
    }

    /**
     * @param array $data
     * @return string
     */
    private function getAllBlogsSql($data = array())
    {
        $db_prefix = DB_PREFIX;
        $language_id = (int)$this->config->get('config_language_id');
        $sql = "SELECT b.*, bd.`language_id`, bd.`title`, bd.`content`, bd.`short_content`,  bd.`image`, bd.`meta_title`, bd.`meta_description`,
       bd.`seo_keywords`, bd.`alias`, bd.`alt`, bd.`type`, bd.`video_url`, bd.`source_author_name`, bc.`blog_category_id`, bc.`status` as blog_category_status, bc.`source` blog_category_source, bcd.`title` as blog_category_title, bcd.`meta_title` as blog_category_meta_title,
       bcd.`meta_description` as blog_category_meta_description, bcd.`alias` as blog_category_alias, u.`firstname`, u.`lastname`, u.`email` FROM `{$db_prefix}blog` b 
                LEFT JOIN `{$db_prefix}blog_description` bd ON (b.`blog_id` = bd.`blog_id`) 
                LEFT JOIN `{$db_prefix}user` u ON (b.`author` = u.`user_id`)
                LEFT JOIN `{$db_prefix}blog_to_blog_category` btbc ON (b.`blog_id` = btbc.`blog_id`)
                LEFT JOIN `{$db_prefix}blog_category` bc ON (bc.`blog_category_id` = btbc.`blog_category_id`)
                LEFT JOIN `{$db_prefix}blog_category_description` bcd ON (bc.`blog_category_id` = bcd.`blog_category_id`)
                ";

        if (isset($data['filter_tag']) && $data['filter_tag'] != '') {
            $sql .= " LEFT JOIN `{$db_prefix}blog_tag` bt ON (bt.`blog_id` = b.`blog_id`)";
            $sql .= " LEFT JOIN `{$db_prefix}tag` t ON (t.`tag_id` = bt.`tag_id`)";
        }

        $sql .= " WHERE 1=1 
                  AND bd.language_id = '" . $language_id . "' 
                  AND bcd.language_id = '" . $language_id . "' ";

        if (!empty($data['filter_name'])) {
            $sql .= " AND bd.`title` LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (isset($data['source']) && !empty($data['source'])) {
            $sql .= " 
                AND b.source = '". $this->db->escape($data['source']) ."' 
                AND bc.source = '". $this->db->escape($data['source']) ."' 
            ";
        }

        // TODO: use array, do not set '' for multiple statuses...
        if (!empty($data['filter_status'])) {
            $order_statuses = explode(',', $data['filter_status']);
            $order_statuses = array_map(function ($blog_status_id) {
                return "'" . (int)$blog_status_id . "'";

            }, $order_statuses);

            $sql .= " AND b.`status` IN (" . implode(",", $order_statuses) . ")";
        } elseif (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $sql .= " AND b.`status` = '" . (int)$data['filter_status'] . "'";
        }

        if (!empty($data['filter_author']) && is_array($data['filter_author'])) {
            $blog_authors = array_map(function ($blog_author_id) {
                return "'" . (int)$blog_author_id . "'";

            }, $data['filter_author']);

            $sql .= " AND b.`author` IN (" . implode(", ", $blog_authors) . ")";
        }

        if (!empty($data['filter_category']) && is_array($data['filter_category'])) {
            $blog_categories = array_map(function ($blog_catrgory_id) {
                return "'" . (int)$blog_catrgory_id . "'";

            }, $data['filter_category']);

            $sql .= " AND bc.`blog_category_id` IN (" . implode(", ", $blog_categories) . ")";
        }

        if (!empty($data['filter_tag']) && is_array($data['filter_tag'])) {
            $blog_tags = array_map(function ($blog_tag_value) {
                return "'" . $blog_tag_value . "'";

            }, $data['filter_tag']);

            $sql .= " AND t.`value` IN (" . implode(", ", $blog_tags) . ")";
        }

        $sql .= " ORDER BY b.`date_modified` DESC";

        // IMPORTANT: do not apply LIMIT here

        return $sql;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function addBlog($data)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "blog 
                          SET demo = 0,
                          author = '" . $this->db->escape($data['author']) . "', 
                          status = '" . $this->db->escape($data['status']) . "', 
                          `source` = '" . $this->db->escape($data['source']) . "', 
                          date_publish = NOW(), 
                          date_added = NOW(), 
                          date_modified = NOW()"
        );
        $blog_id = $this->db->getLastId();

        $language_id = (int)$this->config->get('config_language_id');
        $this->db->query("INSERT INTO " . DB_PREFIX . "blog_description 
                          SET blog_id = '" . $blog_id . "', 
                          language_id = '" . (int)$language_id . "', 
                          title = '" . $this->db->escape($data['title']) . "', 
                          content = '" . $this->db->escape($data['content']) . "', 
                          short_content = '" . $this->db->escape($data['short_content']) . "', 
                          image = '" . $this->db->escape($data['image']) . "',
                          alt = '" . $this->db->escape($data['alt']) . "', 
                          meta_title = '" . $this->db->escape($data['seo_name']) . "', 
                          meta_description = '" . $this->db->escape($data['body']) . "', 
                          alias = '" . $this->db->escape($data['alias']) . "',
                          seo_keywords = '" . $this->db->escape($data['seo_keywords']) . "',
                          type = '" . $this->db->escape($data['type']) . "',
                          video_url = '" . $this->db->escape($data['video_url']) . "',
                          source_author_name = '" . $this->db->escape($data['source_author_name']) . "'"
        );

        $this->db->query("INSERT INTO " . DB_PREFIX . "blog_to_blog_category 
                          SET  blog_id = '" . $blog_id . "', 
                          blog_category_id = '" . $this->db->escape($data['blog_category']) . "'"
        );

        // create seo
        $this->load->model('custom/common');

        $blog_alias = $data['title'];
        if (isset($data['alias']) && $data['alias'] != '') {
            $blog_alias = $data['alias'];
        }
        $slug = $this->model_custom_common->createSlug($blog_alias, '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);
        $alias = 'blog=' . $blog_id;
        $this->db->query("UPDATE " . DB_PREFIX . "blog_description SET alias = '" . $this->db->escape($slug_custom) . "' WHERE blog_id = '" . (int)$blog_id . "'");
        $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                SET language_id = '" . (int)$language_id . "', 
                `query` = '" . $alias . "', 
                `keyword` = '" . $slug_custom . "', 
                `table_name` = 'blog'";
        $this->db->query($sql);

        if (isset($data['tag_list'])) {
            $tags_list = [];
            if (is_string($data['tag_list'])) {
                $tags_list = explode(',', $data['tag_list']);
            } elseif (is_array($data['tag_list'])) {
                $tags_list = $data['tag_list'];
            }

            foreach ($tags_list as $tag_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "blog_tag 
                                  SET blog_id = '" . (int)$blog_id . "', 
                                  tag_id = '" . (int)$tag_id . "'");
            }
        }

        $this->deleteBlogCaches();

        $this->sendBlogBrokerMessage($blog_id);

        return $blog_id;
    }

    /**
     * @param $blog_id
     */
    public function deleteBlog($blog_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "blog WHERE blog_id = '" . (int)$blog_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'blog=" . (int)$blog_id . "'");
        //delete tag, collection
        $this->db->query("DELETE FROM " . DB_PREFIX . "blog_description WHERE blog_id = '" . (int)$blog_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "blog_tag WHERE blog_id = '" . (int)$blog_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "blog_to_blog_category WHERE blog_id = '" . (int)$blog_id . "'");
        $this->deleteBlogCaches();
        //$this->cache->delete('blog');
    }

    /**
     * @param $blog_category_id
     * @return false|void
     */
    public function deleteCategory($blog_category_id)
    {
        // get old blog category
        $old_blog_category = $this->getBlogCategory($blog_category_id);
        if (!$old_blog_category || !isset($old_blog_category['blog_category_id'])) {
            return false;
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "blog_category 
                          WHERE blog_category_id = '" . (int)$blog_category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url 
                          WHERE query = 'blog_category_id=" . (int)$blog_category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "blog_to_blog_category 
                          WHERE blog_category_id = '" . (int)$blog_category_id . "'");
        //delete tag, collection
        $this->db->query("DELETE FROM " . DB_PREFIX . "blog_category_description 
                          WHERE blog_category_id = '" . (int)$blog_category_id . "'");

        $this->deleteBlogCaches();
    }

    /**
     * @param $blog_id
     * @param $data
     */
    public function editBlog($blog_id, $data)
    {
        // get old blog
        $old_blog = $this->getBlog($blog_id);
        if (!$old_blog || !isset($old_blog['blog_id'])) {
            return;
        }

        // update
        $language_id = $this->config->get('config_language_id');
        $source_query = empty($data['source']) ? '' : " `source` = '{$this->db->escape($data['source'])}', ";
        $this->db->query("UPDATE " . DB_PREFIX . "blog 
                          SET demo = 0,
                          author = '" . $this->db->escape($data['author']) . "', 
                          status = " . $data['status'] . ", 
                          {$source_query}
                          date_modified = NOW() 
                          WHERE blog_id = '" . (int)$blog_id . "'");
        $this->db->query("UPDATE " . DB_PREFIX . "blog_to_blog_category 
                          SET blog_category_id = '" . $this->db->escape($data['blog_category']) . "' 
                          WHERE blog_id = " . (int)$blog_id . "");

        $this->db->query("DELETE FROM " . DB_PREFIX . "blog_tag WHERE blog_id = '" . (int)$blog_id . "'");
        if (isset($data['tag_list'])) {
            $tags_list = [];
            if (is_string($data['tag_list'])) {
                $tags_list = explode(',', $data['tag_list']);
            } elseif (is_array($data['tag_list'])) {
                $tags_list = $data['tag_list'];
            }

            foreach ($tags_list as $tag_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "blog_tag 
                                  SET blog_id = '" . (int)$blog_id . "', 
                                      tag_id = '" . (int)$tag_id . "'");
            }
        }

        // update seo
        $this->load->model('custom/common');
        $old_blog_alias = isset($old_blog['alias']) ? $old_blog['alias'] : '';
        $blog_alias = $old_blog_alias;
        if (isset($data['alias']) && $data['alias'] != '' && $data['alias'] != $old_blog_alias) {
            $slug = $this->model_custom_common->createSlug($data['alias'], '-');
            $slug_custom = $this->model_custom_common->getSlugUnique($slug);
            $blog_alias = $slug_custom;

            // delete seo if existing
            $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'blog=" . (int)$blog_id . "'");

            // create new seo
            $alias = 'blog=' . $blog_id;

            $this->db->query("UPDATE " . DB_PREFIX . "blog_description 
                              SET alias = '" . $this->db->escape($slug_custom) . "' 
                              WHERE blog_id = '" . (int)$blog_id . "'");

            $sqlseo = "INSERT INTO `" . DB_PREFIX . "seo_url` 
                       SET `language_id` = '" . (int)$language_id . "', 
                       `query` = '" . $alias . "', 
                       `keyword` = '" . $slug_custom . "', 
                       `table_name` = 'blog'";

            $this->db->query($sqlseo);
        }

        $this->db->query("UPDATE " . DB_PREFIX . "blog_description 
                          SET `title` = '" . $this->db->escape($data['title']) . "', 
                          `image` = '" . $this->db->escape($data['image']) . "', 
                          `alt` = '" . $this->db->escape($data['alt']) . "', 
                          `content` = '" . $this->db->escape($data['content']) . "', 
                          `short_content` = '" . $this->db->escape($data['short_content']) . "', 
                          `meta_title` = '" . $this->db->escape($data['seo_name']) . "', 
                          `meta_description` = '" . $this->db->escape($data['body']) . "', 
                          `alias` = '" . $this->db->escape($blog_alias) . "', 
                          `seo_keywords` = '" . $this->db->escape($data['seo_keywords']) . "', 
                          `type` = '" . $this->db->escape($data['type']) . "', 
                          `video_url` = '" . $this->db->escape($data['video_url']) . "',
                          `source_author_name` = '" . $this->db->escape($data['source_author_name']) . "'
                          WHERE `blog_id` = " . (int)$blog_id . "");

        $this->deleteBlogCaches();

        $this->sendBlogBrokerMessage($blog_id, $old_blog['title']);
    }

    /**
     * delete blog caches
     */
    private function deleteBlogCaches() {
        $product_key_redis =  DB_PREFIX . 'blog';
        $this->cache->delete($product_key_redis);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function getTotalBlogs($data = [])
    {
        $sql = $this->getAllBlogsSql($data);

        $sql = "SELECT COUNT(1) as `total` FROM ($sql) as ab";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    /**
     * @param $category_title
     * @return mixed
     */
    public function getBlogCategoryByTitle($category_title) {
        $DB_PREFIX = DB_PREFIX;
        $language_id = (int)$this->config->get('config_language_id');

        $sql = "SELECT * FROM `{$DB_PREFIX}blog_category_description` WHERE `language_id` = {$language_id} AND `title` = '{$category_title}' LIMIT 1";
        $query = $this->db->query($sql);

        return $query->row;
    }

    /**
     * @param $category_id
     * @return mixed
     */
    public function getBlogCategory($category_id)
    {
        $language_id = (int)$this->config->get('config_language_id');
        $sql = "SELECT * FROM " . DB_PREFIX . "blog_category as bc 
                INNER JOIN " . DB_PREFIX . "blog_category_description bcd ON (bc.blog_category_id = bcd.blog_category_id) 
                WHERE bc.blog_category_id = '" . (int)$category_id . "' 
                AND bcd.language_id = '" . $language_id . "'";
        $query = $this->db->query($sql);

        return $query->row;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createBlogCategory($data)
    {
        $language_id = (int)$this->config->get('config_language_id');
        $status = isset($data['status']) ? $this->db->escape($data['status']) : 1;
        $this->db->query("INSERT INTO " . DB_PREFIX . "blog_category 
                          SET status = " . $status . ",
                          `source` = '{$this->db->escape($data['source'])}',
                          `date_added` = NOW(), 
                          `date_modified` = NOW()"
        );
        $category_id = $this->db->getLastId();

        //seo url
        $this->load->model('custom/common');
        $category_alias = $data['title'];
        if (isset($data['alias']) && $data['alias'] != '') {
            $category_alias = $data['alias'];
        }
        $slug = $this->model_custom_common->createSlug($category_alias, '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);
        $alias = 'blog_category_id=' . $category_id;
        $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                SET `language_id` = '" . (int)$language_id . "', 
                    `query` = '" . $alias . "', 
                    `keyword` = '" . $slug_custom . "', 
                    `table_name` = 'blog_category'";
        $this->db->query($sql);

        //add description
        $this->db->query("INSERT INTO " . DB_PREFIX . "blog_category_description 
                          SET `blog_category_id` = '" . (int)$category_id . "', 
                          `language_id` = '" . (int)$language_id . "', 
                          `title` = '" . $this->db->escape($data['title']) . "',
                          `meta_title` = '" . $this->db->escape($data['meta_title']) . "', 
                          `meta_description` = '" . $this->db->escape($data['meta_description']) . "', 
                          `alias` = '" . $this->db->escape($slug_custom) . "'"
        );

        $this->sendBlogCategoryBrokerMessage($category_id);

        return $category_id;
    }

    /**
     * @param $data
     */
    public function editBlogCategory($data)
    {
        // get old blog category
        $blog_category_id = $data['blog_category_id'];
        $old_blog_category = $this->getBlogCategory($blog_category_id);
        if (!$old_blog_category || !isset($old_blog_category['blog_category_id'])) {
            return;
        }

        // update
        $language_id = $this->config->get('config_language_id');
        $status = isset($data['status']) ? $this->db->escape($data['status']) : 1;
        $source_query = empty($data['source']) ? '' : " `source` = '{$this->db->escape($data['source'])}', ";
        $this->db->query("UPDATE " . DB_PREFIX . "blog_category SET 
                         `status` = " . $status . ", 
                         {$source_query}
                         `date_modified` = NOW() 
                         WHERE blog_category_id = '" . $data['blog_category_id'] . "'");

        // update seo url
        $this->load->model('custom/common');
        $old_blog_category_alias = isset($old_blog_category['alias']) ? $old_blog_category['alias'] : '';
        $blog_category_alias = $old_blog_category_alias;
        if (isset($data['alias']) && $data['alias'] != '' && $data['alias'] != $old_blog_category_alias) {
            $alias = 'blog_category_id=' . $blog_category_id;
            $slug = $this->model_custom_common->createSlug($data['alias'], '-');
            $slug_custom = $this->model_custom_common->getSlugUnique($slug);
            $blog_category_alias = $slug_custom;

            // delete seo if existing
            $sql = "DELETE FROM " . DB_PREFIX . "seo_url WHERE query = '" . $alias . "'";
            $this->db->query($sql);

            // create
            $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                    SET language_id = '" . (int)$language_id . "', 
                    `query` = '" . $alias . "', 
                    `keyword` = '" . $slug_custom . "', 
                    `table_name` = 'blog_category'";
            $this->db->query($sql);
        }

        $this->db->query("UPDATE " . DB_PREFIX . "blog_category_description 
                          SET `title` = '" . $this->db->escape($data['title']) . "',
                          `meta_title` = '" . $this->db->escape($data['meta_title']) . "', 
                          `meta_description` = '" . $this->db->escape($data['meta_description']) . "', 
                          `alias` = '" . $this->db->escape($blog_category_alias) . "'
                          WHERE language_id = '" . (int)$language_id . "' 
                          AND blog_category_id = '" . (int)$blog_category_id . "'");

        $this->sendBlogCategoryBrokerMessage($blog_category_id, $old_blog_category['title']);
    }

    public function getAllParentsBlogCategoryById($blog_category_id)
    {
        $result = [];
        $query = $this->db->query("SELECT * FROM  `" . DB_PREFIX . "blog_category` bc 
                                   LEFT JOIN `" . DB_PREFIX . "blog_category_description` as bcd ON bc.`blog_category_id` = bcd.`blog_category_id` 
                                
                                   WHERE bc.`blog_category_id` = " . (int)$blog_category_id . " 
                                   AND bcd.`language_id` = " . (int)$this->config->get('config_language_id'));

        if ($query->num_rows <= 0) {
            return $result;
        }

        $result[] = [
            'id' => $query->row['blog_category_id'],
            'name' =>  html_entity_decode($query->row['title']),
            'href' =>  $this->url->link('blog/blog', 'blog_category_id=' . $query->row['blog_category_id'], true),
            'text' => html_entity_decode($query->row['title']), // return a&b instead of a&amp;b
            'meta_title' => html_entity_decode($query->row['meta_title']),
            'meta_description' => html_entity_decode($query->row['meta_description'])
        ];
        if ($query->row['parent_id'] != 0 &&
            // IMPORTANT: avoid infinitive loop if wrong data such as category has category_id = parent_id
            $query->row['parent_id'] != $blog_category_id
        ) {
            $parents = $this->getAllParentsBlogCategoryById($query->row['parent_id']);
            foreach ($parents as $v) {
                $result[] = [
                    'id' => $v['id'],
                    'name' =>  html_entity_decode($v['text']),
                    'href' =>  $this->url->link('blog/blog', 'blog_category_id=' . $v['id'], true),
                    'text' => html_entity_decode($v['text']), // return a&b instead of a&amp;b
                    'meta_title' => html_entity_decode($v['meta_title']),
                    'meta_description' => html_entity_decode($v['meta_description'])
                ];
            }
        }

        return $result;
    }

    public function getCategoriesAndParents($blog_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "blog_to_blog_category as ptc LEFT JOIN " . DB_PREFIX . "blog_category_description as cd ON cd.blog_category_id = ptc.blog_category_id WHERE blog_id = '" . (int)$blog_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        $list_categories = [];
        foreach ($query->rows as $row) {
            $parent_categories = $this->getAllParentsCategory($row['blog_category_id']);
            foreach ($parent_categories as $k => $category) {
                $list_categories[$k] = $category;
            }
        }

        $result = [];
        foreach ($list_categories as $category) {
            $result[] = [
                'id' => $category['blog_category_id'],
                'title' => $category['title'],
                'parent_id' => isset($category['parent_id']) ? $category['parent_id'] : 0,
                'alias' => $category['alias']
            ];
        }

        return $result;
    }

    private function getAllParentsCategory($blog_id)
    {
        $result = [];
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "blog_category` as c LEFT JOIN `" . DB_PREFIX . "blog_category_description` as cd ON c.`blog_category_id` = cd.`blog_category_id` WHERE c.`blog_category_id` = " . (int)$blog_id
            . " AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        if ($query->num_rows <= 0) {
            return $result;
        }

        $result[$query->row['blog_category_id']] = $query->row;
        if (isset($query->row['parent_id']) && $query->row['parent_id'] != 0) {
            $parents = $this->getAllParentsCategory($query->row['parent_id']);
            foreach ($parents as $k => $v) {
                $result[$k] = $v;
            }
        }

        return $result;
    }

    /**
     * @param $blog_id
     * @return mixed
     */
    private function getBlogData($blog_id) {
        $language_id = (int)$this->config->get('config_language_id');
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT b.date_modified, bd.*, u.firstname, u.lastname, u.email, bcd.title category_title, bcd.meta_title category_meta_title, bcd.meta_description category_meta_description, bcd.alias category_alias
                FROM {$DB_PREFIX}blog b 
                LEFT JOIN {$DB_PREFIX}blog_description bd ON (b.blog_id = bd.blog_id) 
                LEFT JOIN {$DB_PREFIX}user u ON (b.author = u.user_id) 
                LEFT JOIN {$DB_PREFIX}blog_to_blog_category bc ON (b.blog_id = bc.blog_id) 
                LEFT JOIN {$DB_PREFIX}blog_category_description bcd ON (bc.blog_category_id = bcd.blog_category_id) 
                WHERE b.blog_id = '" . (int)$blog_id . "' AND bd.language_id = '" . $language_id . "' LIMIT 1";
        $query = $this->db->query($sql);

        return $query->row;
    }

    /**
     * send blog message to RabbitMQ
     *
     * @param $blog_id
     * @param string $old_blog_title
     * @param string $old_blog_category_title
     */
    private function sendBlogBrokerMessage($blog_id, $old_blog_title = '', $old_blog_category_title = '')
    {
        try {
            if (!empty($blog_id)) {
                $blog = $this->getBlogData($blog_id);
                if (!empty($blog)) {
                    $message = [
                        'shop_name' => $this->config->get('shop_name'),
                        'blog' => [
                            'title' => $blog['title'],
                            'old_title' => $old_blog_title,
                            'content' => $blog['content'],
                            'short_content' => $blog['short_content'],
                            'author' => [
                                'firstname' => isset($blog['firstname']) ? $blog['firstname'] : '',
                                'lastname' => isset($blog['lastname']) ? $blog['lastname'] : '',
                                'email' => isset($blog['email']) ? $blog['email'] : ''
                            ],
                            'source_author_name' => $blog['source_author_name'],
                            'image' => $blog['image'],
                            'meta_title' => $blog['meta_title'],
                            'meta_description' => $blog['meta_description'],
                            'seo_keywords' => $blog['seo_keywords'],
                            'alias' => $blog['alias'],
                            'alt' => $blog['alt'],
                            'type' => $blog['type'],
                            'video_url' => $blog['video_url'],
                            'blog_date_modify' => $blog['date_modified'],
                            'category' => [
                                'title' => $blog['category_title'],
                                'old_title' => $old_blog_category_title,
                                'meta_title' => $blog['category_meta_title'],
                                'meta_description' => $blog['category_meta_description'],
                                'alias' => $blog['category_alias']
                            ]
                        ]
                    ];

                    $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'blog', '', 'blog', json_encode($message));
                } else {
                    $this->log->write('Send message blog error: Could not get blog data for blog id ' . $blog_id);
                }
            } else {
                $this->log->write('Send message blog error: Missing blog_id');
            }
        } catch (Exception $exception) {
            $this->log->write('Send message blog error: ' . $exception->getMessage());
        }
    }

    /**
     * send blog category message to RabbitMQ
     *
     * @param $blog_category_id
     * @param $old_blog_category_title
     */
    private function sendBlogCategoryBrokerMessage($blog_category_id, $old_blog_category_title = '')
    {
        try {
            if (!empty($blog_category_id)) {
                $blog_category = $this->getBlogCategory($blog_category_id);
                if (!empty($blog_category)) {
                    $message = [
                        'shop_name' => $this->config->get('shop_name'),
                        'blog_category' => [
                            'title' => $blog_category['title'],
                            'old_title' => $old_blog_category_title,
                            'meta_title' => $blog_category['meta_title'],
                            'meta_description' => $blog_category['meta_description'],
                            'alias' => $blog_category['alias']
                        ]
                    ];

                    $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'blog_category', '', 'blog_category', json_encode($message));
                } else {
                    $this->log->write('Send message blog category error: Could not get blog category data for blog category id ' . $blog_category_id);
                }
            } else {
                $this->log->write('Send message blog category error: Missing blog_category_id ');
            }
        } catch (Exception $exception) {
            $this->log->write('Send message blog category error: ' . $exception->getMessage());
        }
    }
}
