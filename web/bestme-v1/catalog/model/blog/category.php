<?php

class ModelBlogCategory extends Model
{
    use RabbitMq_Trait;

    public function getAllCategories($data = array())
    {
        $sql = $this->getAllCategorySql($data);

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'blog:getAllCategories' . md5($sql);
        $blog_categories_list = $this->cache->get($cache_key);
        if ($blog_categories_list) {
            return $blog_categories_list;
        }

        $query = $this->db->query($sql);
        $this->cache->set($cache_key, $query->rows);

        return $query->rows;
    }

    public function createBlogCategory($data)
    {
        $language_id = (int)$this->config->get('config_language_id');
        $this->db->query("INSERT INTO " . DB_PREFIX . "blog_category 
                          SET status = " . $data['status'] . ", 
                          `date_added` = NOW(), 
                          `date_modified` = NOW()"
        );
        $category_id = $this->db->getLastId();

        //seo url
        $this->load->model('custom/common');
        $category_alias = $data['title'];
        if (isset($data['alias']) && $data['alias'] != '') {
            $category_alias = $data['alias'];
        }
        $slug = $this->model_custom_common->createSlug($category_alias, '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);
        $alias = 'blog_category_id=' . $category_id;
        $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                SET `language_id` = '" . (int)$language_id . "', 
                    `query` = '" . $alias . "', 
                    `keyword` = '" . $slug_custom . "', 
                    `table_name` = 'blog_category'";
        $this->db->query($sql);

        //add description
        $this->db->query("INSERT INTO " . DB_PREFIX . "blog_category_description 
                          SET `blog_category_id` = '" . (int)$category_id . "', 
                          `language_id` = '" . (int)$language_id . "', 
                          `title` = '" . $this->db->escape($data['title']) . "',
                          `meta_title` = '" . $this->db->escape($data['seo_title']) . "', 
                          `meta_description` = '" . $this->db->escape($data['seo_description']) . "', 
                          `alias` = '" . $this->db->escape($slug_custom) . "'"
        );

        $this->deleteBlogCaches();

        $this->sendBlogCategoryBrokerMessage($category_id);

        // update parent_id
        foreach ($data['category'] as $category) {
            $sql = "UPDATE " . DB_PREFIX . "blog_category SET 
                         `parent_id` = " . $category_id . ", 
                         `date_modified` = NOW() 
                         WHERE blog_category_id = '" . $category . "'";
            $this->db->query($sql);
        }
        return $category_id;
    }

    public function getBlogCategory($category_id)
    {
        $language_id = (int)$this->config->get('config_language_id');
        $sql = "SELECT * FROM " . DB_PREFIX . "blog_category as bc 
                INNER JOIN " . DB_PREFIX . "blog_category_description bcd ON (bc.blog_category_id = bcd.blog_category_id) 
                WHERE bc.blog_category_id = '" . (int)$category_id . "' 
                AND bcd.language_id = '" . $language_id . "'";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'blog:getBlogCategory' . md5($sql);
        $blog_category = $this->cache->get($cache_key);
        if ($blog_category) {
            return $blog_category;
        }

        $query = $this->db->query($sql);
        $this->cache->set($cache_key, $query->row);

        return $query->row;
    }

    public function getBlogCategoryByParent($id)
    {
        $language_id = (int)$this->config->get('config_language_id');
        $sql = "SELECT * FROM " . DB_PREFIX . "blog_category as bc 
                INNER JOIN " . DB_PREFIX . "blog_category_description bcd ON (bc.blog_category_id = bcd.blog_category_id) 
                WHERE bc.parent_id = '" . (int)$id . "' 
                AND bcd.language_id = '" . $language_id . "'";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'blog:getBlogCategoryByParent' . md5($sql);
        $blog_categories_list = $this->cache->get($cache_key);
        if ($blog_categories_list) {
            return $blog_categories_list;
        }

        $query = $this->db->query($sql);
        $this->cache->set($cache_key, $query->rows);

        return $query->rows;
    }

    public function getBlogCategoryNameById($category_id)
    {
        $language_id = (int)$this->config->get('config_language_id');
        $sql = "SELECT bcd.title FROM " . DB_PREFIX . "blog_category as bc 
                INNER JOIN " . DB_PREFIX . "blog_category_description bcd ON (bc.blog_category_id = bcd.blog_category_id) 
                WHERE bc.blog_category_id = '" . (int)$category_id . "' 
                AND bcd.language_id = '" . $language_id . "'";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'blog:getBlogCategoryNameById' . md5($sql);
        $blog_category = $this->cache->get($cache_key);
        if ($blog_category) {
            return $blog_category;
        }

        $query = $this->db->query($sql);
        $this->cache->set($cache_key, $query->rows);

        return $query->row;
    }

    public function editBlogCategory($data)
    {
        // get old blog category
        $blog_category_id = $data['blog_category_id'];
        $old_blog_category = $this->getBlogCategory($blog_category_id);
        if (!$old_blog_category || !isset($old_blog_category['blog_category_id']) || 'omihub' == $old_blog_category['source']) {
            return;
        }

        // update
        $language_id = $this->config->get('config_language_id');
        $this->db->query("UPDATE " . DB_PREFIX . "blog_category SET 
                         `status` = " . $data['status'] . ", 
                         `date_modified` = NOW() 
                         WHERE blog_category_id = '" . $data['blog_category_id'] . "'");

        // update seo url
        $this->load->model('custom/common');
        $old_blog_category_alias = isset($old_blog_category['alias']) ? $old_blog_category['alias'] : '';
        $blog_category_alias = $old_blog_category_alias;
        if (isset($data['alias']) && $data['alias'] != '' && $data['alias'] != $old_blog_category_alias) {
            $alias = 'blog_category_id=' . $blog_category_id;
            $slug = $this->model_custom_common->createSlug($data['alias'], '-');
            $slug_custom = $this->model_custom_common->getSlugUnique($slug);
            $blog_category_alias = $slug_custom;

            // delete seo if existing
            $sql = "DELETE FROM " . DB_PREFIX . "seo_url WHERE query = '" . $alias . "'";
            $this->db->query($sql);

            // create
            $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                    SET language_id = '" . (int)$language_id . "', 
                    `query` = '" . $alias . "', 
                    `keyword` = '" . $slug_custom . "', 
                    `table_name` = 'blog_category'";
            $this->db->query($sql);
        }

        $this->db->query("UPDATE " . DB_PREFIX . "blog_category_description 
                          SET `title` = '" . $this->db->escape($data['title']) . "',
                          `meta_title` = '" . $this->db->escape($data['seo_title']) . "', 
                          `meta_description` = '" . $this->db->escape($data['seo_description']) . "', 
                          `alias` = '" . $this->db->escape($blog_category_alias) . "'
                          WHERE language_id = '" . (int)$language_id . "' 
                          AND blog_category_id = '" . (int)$blog_category_id . "'");

        $this->deleteBlogCaches();

        $this->sendBlogCategoryBrokerMessage($blog_category_id, $old_blog_category['title']);
    }

    public function getBlogCategoryByName($name, $category_id)
    {
        $language_id = (int)$this->config->get('config_language_id');
        $sql = "SELECT * FROM " . DB_PREFIX . "blog_category_description 
                WHERE title = '" . $this->db->escape($name) . "' 
                AND language_id = '" . $language_id . "'";
        if (!empty($category_id)) {
            $sql .= " AND blog_category_id != '" . (int)$category_id . "'";
        }

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'blog:getBlogCategoryByName' . md5($sql);
        $blog_category = $this->cache->get($cache_key);
        if ($blog_category) {
            return $blog_category;
        }

        $query = $this->db->query($sql);
        $this->cache->set($cache_key, $query->rows);

        return $query->row;
    }

    public function getTotalBlogCategories($data = [])
    {
        $sql = $this->getAllCategorySql($data);

        $sql = "SELECT COUNT(1) as `total` FROM ($sql) as abc";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'blog:getTotalBlogCategories' . md5($sql);
        $blog_categories_count = $this->cache->get($cache_key);
        if ($blog_categories_count) {
            return $blog_categories_count;
        }

        $query = $this->db->query($sql);
        $this->cache->set($cache_key, $query->rows);

        return $query->row['total'];
    }

    public function getTotalBlogToCategorys($collection_id)
    {
        $sql = "SELECT COUNT(blog_id) AS total FROM " . DB_PREFIX . "blog_to_blog_category 
                WHERE `blog_category_id` = $collection_id";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'blog:getTotalBlogToCategorys' . md5($sql);
        $blog_category_count = $this->cache->get($cache_key);
        if ($blog_category_count) {
            return $blog_category_count;
        }

        $query = $this->db->query($sql);
        $this->cache->set($cache_key, $query->rows);

        return $query->row['total'];
    }

    public function getBlogCategoryPaginator($data = array())
    {
        $language_id = (int)$this->config->get('config_language_id');
        $sql = "SELECT bc.blog_category_id, bcd.title 
                FROM " . DB_PREFIX . "blog_category as bc 
                INNER JOIN " . DB_PREFIX . "blog_category_description bcd ON (bc.blog_category_id = bcd.blog_category_id) 
                WHERE 1=1 AND bcd.language_id = '" . $language_id . "'";
        if (!empty($data['filter_name'])) {
            $sql .= " AND bcd.title LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }
        $sql .= " ORDER BY bcd.title ASC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function countBlogCategory($data = array())
    {
        $language_id = (int)$this->config->get('config_language_id');
        $sql = "SELECT bc.blog_category_id, bcd.title 
                FROM " . DB_PREFIX . "blog_category as bc 
                INNER JOIN " . DB_PREFIX . "blog_category_description bcd ON (bc.blog_category_id = bcd.blog_category_id) 
                WHERE 1=1 AND bcd.language_id = '" . $language_id . "'";
        if (!empty($data['filter_name'])) {
            $sql .= " AND bcd.title LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }
        $sql .= " ORDER BY bcd.title ASC";
        $query = $this->db->query($sql);

        return count($query->rows);
    }

    public function updateStatus($id, $status)
    {
        // get old blog category
        $old_blog_category = $this->getBlogCategory($id);
        if (!$old_blog_category || !isset($old_blog_category['blog_category_id']) || 'omihub' == $old_blog_category['source']) {
            return false;
        }

        $this->db->query("UPDATE " . DB_PREFIX . "blog_category 
                          SET status = " . $status . " 
                          WHERE blog_category_id = " . (int)$id . "");

        $this->deleteBlogCaches();

        return true;
    }

    public function updateParent($id, $parent_id)
    {
        $sql = "UPDATE " . DB_PREFIX . "blog_category 
                          SET parent_id = " . (int)$parent_id . " 
                          WHERE blog_category_id = " . (int)$id . "";
        $this->db->query($sql);
        $this->deleteBlogCaches();
        return true;
    }

    public function removeCategory($id)
    {
        $sql = "UPDATE " . DB_PREFIX . "blog_category 
                          SET parent_id = 0
                          WHERE blog_category_id = " . (int)$id . "";
        $this->db->query($sql);
        $this->deleteBlogCaches();
    }

    public function deleteCategory($blog_category_id)
    {
        // get old blog category
        $old_blog_category = $this->getBlogCategory($blog_category_id);
        if (!$old_blog_category || !isset($old_blog_category['blog_category_id']) || 'omihub' == $old_blog_category['source']) {
            return false;
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "blog_category 
                          WHERE blog_category_id = '" . (int)$blog_category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url 
                          WHERE query = 'blog_category_id=" . (int)$blog_category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "blog_to_blog_category 
                          WHERE blog_category_id = '" . (int)$blog_category_id . "'");
        //delete tag, collection
        $this->db->query("DELETE FROM " . DB_PREFIX . "blog_category_description 
                          WHERE blog_category_id = '" . (int)$blog_category_id . "'");

        $this->deleteBlogCaches();

        $this->sendBlogCategoryDeleteBrokerMessage($old_blog_category['title']);
    }

    public function getCategoryByBlogId($blog_id)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "blog_to_blog_category bbg 
                LEFT JOIN " . DB_PREFIX . "blog_category_description bcd ON (bbg.blog_category_id = bcd.blog_category_id) 
                WHERE blog_id = $blog_id 
        ";
        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getFullChildCategoryIds($parentId)
    {
        if (empty($parentId)) {
            return [];
        }

        $allChildren = $this->getFullChildCategories($data = [], $parentId);
        if (!is_array($allChildren)) {
            return [];
        }

        $result = [];
        foreach ($allChildren as $child) {
            if (!isset($child['id'])) {
                continue;
            }

            $result[] = $child['id'];
            if (isset($child['sub_ids']) && is_array($child['sub_ids'])) {
                $result = array_merge($result, $child['sub_ids']);
            }
        }

        return $result;
    }

    public function getFullChildCategories($data, $parentId = 0)
    {
        $result = [];
        $sql = "SELECT * FROM `" . DB_PREFIX . "blog_category` c LEFT JOIN `" . DB_PREFIX . "blog_category_description` cd ON (c.blog_category_id = cd.blog_category_id) 
                                            WHERE c.`parent_id` = " . (int)$parentId . " AND cd.`language_id` = " . (int)$this->config->get('config_language_id');

        if (!empty($data['filter_title'])) {
            $sql .= " AND cd.title LIKE '%" . $this->db->escape($data['filter_title']) . "%'";
        }

        $sql .= " ORDER BY c.`date_added` DESC";

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'blog:getFullChildCategories' . md5($sql);
        $blog_categories_list = $this->cache->get($cache_key);
        if ($blog_categories_list) {
            $blog_categories = $blog_categories_list;
        } else {
            $query = $this->db->query($sql);
            $this->cache->set($cache_key, $query->rows);

            $blog_categories = $query->rows;
        }

        foreach ($blog_categories as $row) {
            $children = $this->getFullChildCategories([], $row['blog_category_id']);
            $sub_ids = [];
            foreach ($children as $child) {
                $sub_ids[] = $child['id'];
                if (isset($child['sub_ids']) && is_array($child['sub_ids'])){
                    $sub_ids = array_merge($sub_ids, $child['sub_ids']);
                }
            }

            $result[] = [
                'id' => $row['blog_category_id'],
                'text' => html_entity_decode($row['title']), // return a&b instead of a&amp;b
                'hasChildren' => count($children) > 0 ? true : false,
                'sub_ids' => $sub_ids,
                'children' => $children
            ];
        }

        return $result;
    }

    private function getAllCategorySql($data = array())
    {
        $language_id = (int)$this->config->get('config_language_id');
        $sql = "SELECT bc.*, bcd.`language_id`, bcd.`title`, bcd.`meta_title`, bcd.`meta_description`, bcd.`alias` FROM " . DB_PREFIX . "blog_category bc 
                LEFT JOIN " . DB_PREFIX . "blog_category_description bcd ON (bc.blog_category_id = bcd.blog_category_id) 
                WHERE 1=1 AND bcd.language_id = '" . $language_id . "'";


        if (isset($data['get_bc_detail']) && $data['get_bc_detail']) {
            $sql .= " AND bc.parent_id = '" . 0 . "'";
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND bcd.title LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $sql .= " AND bc.status = '" . (int)$data['filter_status'] . "'";
        }
        $sql .= " ORDER BY bc.date_modified DESC";

        return $sql;
    }

    /**
     * delete blog caches
     */
    private function deleteBlogCaches() {
        $cache_key_redis =  DB_PREFIX . 'blog';
        $this->cache->delete($cache_key_redis);
    }

    /**
     * send blog category message to RabbitMQ
     *
     * @param $blog_category_id
     * @param string $old_blog_category_title
     */
    private function sendBlogCategoryBrokerMessage($blog_category_id, $old_blog_category_title = '')
    {
        try {
            if (!empty($blog_category_id)) {
                $blog_category = $this->getBlogCategory($blog_category_id);
                if (!empty($blog_category)) {
                    $message = [
                        'shop_name' => $this->config->get('shop_name'),
                        'blog_category' => [
                            'title' => $blog_category['title'],
                            'old_title' => $old_blog_category_title,
                            'meta_title' => $blog_category['meta_title'],
                            'meta_description' => $blog_category['meta_description'],
                            'alias' => $blog_category['alias']
                        ]
                    ];

                    $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'blog_category', '', 'blog_category', json_encode($message));
                } else {
                    $this->log->write('Send message blog category error: Could not get blog category data for blog category id ' . $blog_category_id);
                }
            } else {
                $this->log->write('Send message blog category error: Missing blog_category_id ');
            }
        } catch (Exception $exception) {
            $this->log->write('Send message blog category error: ' . $exception->getMessage());
        }
    }

    /**
     * Send blog category delete message to RabbitMQ
     *
     * @param $title
     * @param $action
     * @return void
     */
    private function sendBlogCategoryDeleteBrokerMessage($title, $action = 'delete') {
        try {
            if ($title) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'title' => $title,
                    'action' => $action
                ];
                $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'modify_blog_category', '', 'modify_blog_category', json_encode($message));
            }
        }catch (Exception $exception) {
            $this->log->write('Send message blog category delete error: ' . $exception->getMessage());
        }
    }
}