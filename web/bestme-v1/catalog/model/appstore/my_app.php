<?php


class ModelAppstoreMyApp extends Model
{
    // define some free app code
    const APP_CHAT_BOT = 'mar_ons_chatbot';
    const APP_MAX_LEAD = 'mar_ons_maxlead';
    const APP_LIVE_CHAT_X9 = 'mar_ons_livechatx9';
    const APP_GHN = 'trans_ons_ghn';
    const APP_VIETTEL_POST = 'trans_ons_vtpost';

    public function checkAppActive($app_code)
    {
        $sqlCheck = "SELECT COUNT(app.`id`) AS `total` FROM `" . DB_PREFIX . "my_app` app 
                WHERE app.`app_code` = '" . $this->db->escape($app_code) . "'
                AND `installed` = 1 AND `status` = 1";

        $queryCheck = $this->db->query($sqlCheck);

        if ($queryCheck->row['total'] > 0) {
            return true;
        }
        return false;
    }

    public function checkModuleActive($app_code)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "appstore_setting` 
                WHERE `code` = '" . $this->db->escape($app_code) . "' ";
        $query = $this->db->query($sql);
        $module = $query->row;
        if (empty($module)) {
            return false;
        }
        $setting = json_decode($module['setting'], true);
        if ($setting['status'] == 1) {
            return true;
        } else {
            return false;
        }
    }
}