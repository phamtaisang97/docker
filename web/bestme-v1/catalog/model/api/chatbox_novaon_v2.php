<?php

class ModelApiChatboxNovaonV2 extends Model
{
    const __ERR_MESSAGE_MAX_LENGTH = 500;
    const APP_CODE_PAYMENT_VNPAY = 'paym_ons_vnpay';

    /**
     * get products list data (include one product), old version, for Further reading
     *
     * @param array $query_params
     * @param bool $is_all_statuses if true, includes status hide, not_sale_out_of_stock, deleted, ...
     * @param array $more_return_fields
     * @return array|mixed
     */
    public function getProductsListData_v1_0(array $query_params, $is_all_statuses = false, $more_return_fields = [])
    {
        $this->load->language('api/chatbot_novaon');
        $this->load->model('catalog/product');
        $this->load->model('catalog/attribute');

        $data_filter = [];
        if (isset($query_params['page']) && !empty($query_params['page'])) {
            $data_filter = [
                'start' => ((int)$query_params['page'] - 1) * ((int)isset($query_params['limit']) ? $query_params['limit'] : 15),
                'limit' => isset($query_params['limit']) ? $query_params['limit'] : 15
            ];
        }

        if (isset($query_params['searchKey']) && isset($query_params['searchField'])) {
            $data_filter = array_merge($data_filter, [
                'filter_' . $query_params['searchField'] => $query_params['searchKey'],
            ]);
        }

        if (isset($query_params['except_product_ids'])) {
            $data_filter = array_merge($data_filter, [
                'filter_except_product_ids' => $query_params['except_product_ids']
            ]);
        }

        if (isset($query_params['id'])) {
            $data_filter = [
                'filter_product_id' => $query_params['id'],
                'filter_product_version_id' => isset($query_params['product_version_id']) ? $query_params['product_version_id'] : '',
            ];
        }

        if ($is_all_statuses) {
            $data_filter = array_merge($data_filter, [
                'filter_is_all_statuses' => true
            ]);
        }

        if (isset($query_params['is_all'])) {
            $data_filter = array_merge($data_filter, [
                'filter_is_all' => $query_params['is_all']
            ]);
        }

        try {
            $this->load->model('api/chatbox_novaon');
            $products = $this->model_api_chatbox_novaon->getAPIProducts($data_filter);
            $products_total = $this->model_api_chatbox_novaon->getAPIProductsTotal($data_filter);
            $records = [];
            foreach ($products as $product) {
                $category_names = $this->model_catalog_product->getProductCategoryName($product['product_id']);
                $category_name_product = is_array($category_names) ? implode(',', $category_names) : '';
                $name_cate = (!empty($category_name_product) ? $category_name_product : '');
                $product_images = $this->model_catalog_product->getDataProductImages($product['product_id']);

                $r = [
                    "id" => $product['product_id'],
                    "name" => $product['name'],
                    "original_price" => $product['compare_price_version'],
                    "sale_price" => $product['price_version'],
                    "image" => $product['image'],
                    "url" => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                    "version" => implode(' • ', explode(',', $product["name_version"])),
                    "product_version_id" => $product["product_version_id"],
                    "quantity" => $product["quantity_in_stock"],
                    "weight" => $product["weight"],
                    "sale_on_out_of_stock" => $product["sale_on_out_of_stock"],
                    'nameCategory' => isset($name_cate) ? $name_cate : '',
                    'manufacturer' => is_null($product['manufacturer']) ? "" : $product['manufacturer'],
                    'activeStatus' => $product['status'],
                    'product_images' => $product_images
                ];

                if (empty($query_params['product_version_id'])) {
                    $product_attributes = $this->model_catalog_product->getProductAttributes($product['product_id']);
                    foreach ($product_attributes as $product_attribute) {
                        $attribute_info = $this->model_catalog_attribute->getAttribute($product_attribute['attribute_id']);

                        if ($attribute_info) {
                            $r['product_attributes'][] = array(
                                'attribute_id' => $product_attribute['attribute_id'],
                                'name' => $attribute_info['name'],
                                'detail' => explode(",", $product_attribute['product_attribute_description']['text'])
                            );
                        }
                    }
                }

                if (!empty($more_return_fields)) {
                    foreach ($more_return_fields as $mr_field) {
                        if (array_key_exists($mr_field, $product)) {
                            $r[$mr_field] = $product[$mr_field];
                        }
                    }
                }

                $records[] = $r;

                if (!empty($query_params['id'])) {
                    // only get first product
                    break;
                }
            }

            // get list
            if (!isset($query_params['id'])) {
                return [
                    'total' => $products_total,
                    'page' => (isset($query_params['page']) && !empty($query_params['page'])) ? $query_params['page'] : '',
                    'records' => $records,
                ];
            } else {
                // get one
                if ($records) {
                    if (empty($query_params['product_version_id'])) {
                        unset($records[0]['product_version_id']);
                    }
                    return $records[0];
                }

                return [
                    'code' => 102,
                    'message' => 'products info invalid'
                ];
            }
        } catch (Exception $e) {
            $len = mb_strlen($e->getMessage()) > self::__ERR_MESSAGE_MAX_LENGTH ? self::__ERR_MESSAGE_MAX_LENGTH : null; // null for sub string to the end

            return [
                'code' => 201,
                'message' => 'unknown error',
                '__error' => mb_substr($e->getMessage(), 0, $len)
            ];
        }
    }

    /**
     * get products list data (include one product)
     * @param array $query_params
     * @param bool $is_all_statuses if true, includes status hide, not_sale_out_of_stock, deleted, ...
     * @param array $more_return_fields
     * @return array|mixed
     */
    public function getProductsListData_v1_1(array $query_params, $is_all_statuses = false, $more_return_fields = [])
    {
        $this->load->language('api/chatbot_novaon');

        $data_filter = [];
        if (isset($query_params['page']) && !empty($query_params['page'])) {
            $data_filter = [
                'start' => ((int)$query_params['page'] - 1) * ((int)isset($query_params['limit']) ? $query_params['limit'] : 15),
                'limit' => isset($query_params['limit']) ? $query_params['limit'] : 15
            ];
        }

        if (isset($query_params['searchKey']) && isset($query_params['searchField'])) {
            $data_filter = array_merge($data_filter, [
                'filter_' . $query_params['searchField'] => $query_params['searchKey'],
            ]);
        }

        if (isset($query_params['quantity'])) {
            $data_filter = array_merge($data_filter, [
                'quantity' => $query_params['quantity']
            ]);
        }

        if (isset($query_params['category'])) {
            $data_filter = array_merge($data_filter, [
                'category_product' => $query_params['category']
            ]);
        }

        if (isset($query_params['manufacturer'])) {
            $data_filter = array_merge($data_filter, [
                'manufacturer' => $query_params['manufacturer']
            ]);
        }

        if (isset($query_params['product_ids'])) {
            $data_filter = array_merge($data_filter, [
                'filter_product_ids' => $query_params['product_ids']
            ]);
        }

        if (isset($query_params['except_product_ids'])) {
            $data_filter = array_merge($data_filter, [
                'filter_except_product_ids' => $query_params['except_product_ids']
            ]);
        }

        if (isset($query_params['is_all'])) {
            $data_filter = array_merge($data_filter, [
                'filter_is_all' => $query_params['is_all']
            ]);
        }

        if (isset($query_params['id'])) {
            $data_filter = [
                'filter_product_id' => $query_params['id'],
                'filter_product_version_id' => isset($query_params['product_version_id']) ? $query_params['product_version_id'] : '',
            ];
        }

        if ($is_all_statuses) {
            $data_filter = array_merge($data_filter, [
                'filter_is_all_statuses' => true
            ]);
        }

        if (isset($query_params['status'])) {
            $data_filter = array_merge($data_filter, [
                'filter_status' => $query_params['status']
            ]);
        }

        if (isset($query_params['source'])) {
            $data_filter = array_merge($data_filter, [
                'filter_source' => $query_params['source']
            ]);
        }

        try {
            $this->load->model('api/chatbox_novaon');
            $this->load->model('catalog/product');
            $this->load->model('catalog/attribute');
            $this->load->model('catalog/manufacturer');

            $products_total = $this->getTotalProductsCustom($data_filter);
            $products = $this->getProductsCustom($data_filter);

            $records = [];
            foreach ($products as $product) {
                if (isset($product['multi_versions']) && $product['multi_versions'] == 1) {
                    $version_quantity = $this->model_catalog_product->getCountVersionProduct($product['product_id']);
                } else {
                    $version_quantity = 0;
                }

                $category_names = $this->model_catalog_product->getProductCategoryName($product['product_id']);
                $category_name_product = is_array($category_names) ? implode(',', $category_names) : '';
                $name_cate = (!empty($category_name_product) ? $category_name_product : '');

                $product_versions = $this->model_catalog_product->getProductVersions($product['product_id']);

                foreach ($product_versions as &$version) {
                    $version['version'] = implode(' • ', explode(',', $version['version']));
                    $version['version_for_store'] = $this->replaceVersionValues($version['version']);

                    $version['price'] = (int)str_replace(',', '', $version['price']);
                    $version['compare_price'] = (int)str_replace(',', '', $version['compare_price']);
                    $version['cost_price'] = $this->model_catalog_product->getCostPriceForProductByStore($product['default_store_id'], $product['product_id'], $version['product_version_id']);
                }
                unset($version);

                // $description_tab
                $title_tab = [];
                $description_tab = [];
                $product_description_tabs = $this->model_catalog_product->getProductDescriptionTab($product['product_id']);
                foreach ($product_description_tabs as $product_description_tab) {
                    $title_tab[] = html_entity_decode(trim($product_description_tab['title']));
                    $description_tab[] = html_entity_decode($product_description_tab['description']);
                }

                $array_manufacturer = $this->model_catalog_manufacturer->getManufacturer($product['manufacturer_id']);
                $name_manufacturer = (isset($array_manufacturer['name']) ? $array_manufacturer['name'] : '');

                $product_cost_price = $this->model_catalog_product->getCostPriceForProductByStore($product['default_store_id'], $product['product_id']);
                $product_images = $this->model_catalog_product->getDataProductImages($product['product_id']);

                $r = [
                    "id" => $product['product_id'],
                    "name" => html_entity_decode($product['product_name']),
                    "original_price" => $product['compare_price'],
                    "sale_price" => $product['price'],
                    "common_compare_price" => $product['common_compare_price'],
                    "common_price" => $product['common_price'],
                    "common_cost_price" => $product['common_cost_price'],
                    "cost_price" => $product_cost_price,
                    "image" => $product['product_image'],
                    "url" => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                    "quantity" => $product['total_quantity'],
                    "sku" => $product['multi_versions'] ? $product['common_sku'] : $product['sku'],
                    "weight" => $product["weight"],
                    "in_stock" => ($product["sale_on_out_of_stock"] == 1 || $product['total_quantity'] > 0) ? 1 : 0,
                    "sale_on_out_of_stock" => $product["sale_on_out_of_stock"],
                    "version_quantity" => $version_quantity,
                    "version" => $product_versions,
                    'nameCategory' => isset($name_cate) ? $name_cate : '',
                    'manufacturer' => $name_manufacturer,
                    'activeStatus' => $product['status'],
                    'user_name' => $product['fullname'],
                    'desc' => html_entity_decode($product['product_sub_description']),
                    'content' => html_entity_decode($product['product_description']),
                    'product_attributes' => [],
                    'barcode' => $product['multi_versions'] ? $product['common_barcode'] : $product['barcode'],
                    'product_images' => $product_images,
                    'seo_title' => isset($product['seo_title']) ? $product['seo_title'] : '',
                    'seo_description' => isset($product['seo_description']) ? $product['seo_description'] : '',
                    'meta_keyword' => isset($product['meta_keyword']) ? $product['meta_keyword'] : '',
                    'alias' => html_entity_decode($product['product_name']),
                    'title_tab' => $title_tab,
                    'description_tab' => $description_tab,
                ];

                /*'desc' => filter_var(html_entity_decode($product['product_sub_description']), FILTER_SANITIZE_STRING),
                'content' => filter_var(html_entity_decode($product['product_description']), FILTER_SANITIZE_STRING),*/
                if (empty($query_params['product_version_id'])) {
                    $product_attributes = $this->model_catalog_product->getProductAttributes($product['product_id']);
                    foreach ($product_attributes as $product_attribute) {
                        $attribute_info = $this->model_catalog_attribute->getAttribute($product_attribute['attribute_id']);

                        if ($attribute_info) {
                            $r['product_attributes'][] = array(
                                'attribute_id' => $product_attribute['attribute_id'],
                                'name' => $attribute_info['name'],
                                'detail' => explode(",", $product_attribute['product_attribute_description']['text'])
                            );
                        }
                    }
                }

                if (!empty($more_return_fields)) {
                    foreach ($more_return_fields as $mr_field) {
                        if (array_key_exists($mr_field, $product)) {
                            $r[$mr_field] = $product[$mr_field];
                        }
                    }
                }

                $records[] = $r;
            }

            // get list
            if (!isset($query_params['id'])) {
                return [
                    'total' => $products_total,
                    'page' => (isset($query_params['page']) && !empty($query_params['page'])) ? $query_params['page'] : '',
                    'records' => $records
                ];
            } else {
                // get one
                if (!empty($records)) {
                    if (empty($query_params['product_version_id'])) {
                        unset($records[0]['product_version_id']);
                    }
                    return $records[0];
                }

                return [
                    'code' => 102,
                    'message' => 'products info invalid'
                ];
            }
        } catch (Exception $e) {
            $len = mb_strlen($e->getMessage()) > self::__ERR_MESSAGE_MAX_LENGTH ? self::__ERR_MESSAGE_MAX_LENGTH : null; // null for sub string to the end

            return [
                'code' => 201,
                'message' => 'unknown error',
                '__error' => mb_substr($e->getMessage(), 0, $len)
            ];
        }
    }

    /**
     * getOrdersList
     * @param array $data_filter
     * @return array
     */
    public function getOrdersListData(array $data_filter)
    {
        try {
            $this->load->model('api/chatbox_novaon');
            $this->load->model('sale/order');
            $this->load->model('localisation/vietnam_administrative');

            $this->load->language('sale/order');

            $orders = $this->model_api_chatbox_novaon->getOrderList($data_filter);
            $orders_total = $this->model_api_chatbox_novaon->getOrdersListTotal($data_filter);
            $orders_total_revenue = $this->model_api_chatbox_novaon->getOrderTotalRevenue($data_filter);

            $CHANGEABLE_ORDER_STATUS = [
                // draft
                ModelSaleOrder::ORDER_STATUS_ID_DRAFT => [
                    [
                        'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_PROCESSING,
                        'order_status_description' => $this->language->get('order_status_processing')
                    ]
                ],
                // processing
                ModelSaleOrder::ORDER_STATUS_ID_PROCESSING => [
                    [
                        'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_DELIVERING,
                        'order_status_description' => $this->language->get('order_status_delivering')
                    ],
                    [
                        'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_COMPLETED,
                        'order_status_description' => $this->language->get('order_status_complete')
                    ],
                    [
                        'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_CANCELLED,
                        'order_status_description' => $this->language->get('order_status_canceled')
                    ]
                ],
                // delivering
                ModelSaleOrder::ORDER_STATUS_ID_DELIVERING => [
                    [
                        'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_COMPLETED,
                        'order_status_description' => $this->language->get('order_status_complete')
                    ],
                    [
                        'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_CANCELLED,
                        'order_status_description' => $this->language->get('order_status_canceled')
                    ]
                ],
                // complete
                ModelSaleOrder::ORDER_STATUS_ID_COMPLETED => [
                    [
                        'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_CANCELLED,
                        'order_status_description' => $this->language->get('order_status_canceled')
                    ]
                ],
                // cancelled
                ModelSaleOrder::ORDER_STATUS_ID_CANCELLED => []
            ];

            $records = [];
            foreach ($orders as $order) {
                $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($order['shipping_province_code']);
                $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($order['shipping_district_code']);
                $ward = $this->model_localisation_vietnam_administrative->getWardByCode($order['shipping_ward_code']);

                $products_of_order = $this->model_sale_order->getProductsDetailByOrder($order['order_id']);
                $products_info = [];
                foreach ($products_of_order as $product) {
                    $product_id = $product['product_id'];
                    $product_version_id = isset($product['product_version_id']) && !empty($product['product_version_id']) ? $product['product_version_id'] : null;

                    // get product info same as api get product detail
                    $product_info = $this->getProductsListData_v1_0([
                        'id' => $product_id,
                        'product_version_id' => $product_version_id,
                        'is_all' => 1 /* always get product regardless out of stock or not */
                    ], $all_statuses = true);

                    // patch quantity in order
                    $product_info['inventory'] = $product_info['quantity'];
                    $product_info['quantity'] = $product['quantity'];

                    // patch sale price, original_price at buy time as in order_product
                    $product_info['original_price'] = isset($product['p_compare_price']) ? $product['p_compare_price'] : 0;
                    $product_info['sale_price'] = isset($product['p_price']) ? $product['p_price'] : 0;
                    $product_info['discount'] = isset($product['order_product_discount']) ? $product['order_product_discount'] : 0;

                    $products_info[] = $product_info;
                }

                $tags = getValueByKey($order, 'tag_value', '');
                $tags = empty($tags) ? [] : explode(',', $tags);
                $tags = array_values(array_unique($tags));

                $data_trans['customer_id'] = $order['customer_id'];
                $data_trans['firstname'] = $order['firstname'];
                $data_trans['lastname'] = $order['lastname'];
                $data_trans['fullname'] = $order['fullname'];
                $data_trans['email'] = $order['email'];
                $data_trans['telephone'] = $order['telephone'];

                $data_trans['transport_order_code'] = $order['transport_order_code'];
                $data_trans['transport_name'] = $order['transport_name'];
                $data_trans['transport_service_name'] = $order['transport_service_name'];
                $data_trans['transport_display_name'] = ''; // Notice: only has value if use one transport (GHN, VTPOST, GHTK, ...)
                $data_trans['transport_collection_amount'] = $order['collection_amount'];

                $transport_order_detail_from_db = [];

                if (isset($data_trans['transport_name']) && !empty($data_trans['transport_name'])) {
                    $transport = new Transport($data_trans['transport_name'], $data_trans, $this);
                    $is_validate_transport = $transport->getAdaptor() instanceof \Transport\Abstract_Transport;

                    if ($is_validate_transport) {
                        $data_trans['transport_display_name'] = $transport->getAdaptor()->getDisplayName();
                    } else {
                        $data_trans['transport_display_name'] = '';
                    }

                    $data_trans['transport_order_active'] = true;

                    try {
                        $data_for_api = [];
                        $data_for_api = array_merge($data_for_api, $transport_order_detail_from_db);
                        $data_for_api['token'] = $this->config->get('config_' . strtoupper($data_trans['transport_name']) . '_token');
                        $data_for_api['transport_order_code'] = $data_trans['transport_order_code'];
                        $data_trans['transport_order_detail']['PickWarehouseName'] = $order['transport_current_warehouse'];
                        $data_trans['transport_order_detail']['OrderCode'] = $data_trans['transport_order_code'];
                        $data_trans['transport_order_detail']['CurrentStatus'] = $order['transport_status'];
                        $data_trans['transport_order_detail']['CollectionAmount'] = $order['collection_amount'];

                        // always get transport status code from transport adaptor
                        $data_trans['transport_order_detail']['CurrentStatusLanguage'] = $transport->getAdaptor()->getTransportStatusMessage($order['transport_status']);

                        $data_trans['transport_order_detail']['Cancelable'] = 1;
                        if ($is_validate_transport) {
                            $data_trans['transport_order_detail']['Cancelable'] = $transport->getAdaptor()->isCancelableOrder($data_trans['transport_order_detail']['CurrentStatus']) ? 1 : 0;
                        }
                    } catch (Exception $e) {
                    }
                } else {
                    $data_trans['transport_order_active'] = false;
                }
                $data_trans_result = $data_trans;
                unset($data_trans);

                $records[] = [
                    "customer" => [
                        "name" => $order['order_full_name'],
                        "phone_number" => $order['telephone_order'],
                        "email" => $order['email'],
                        "delivery_addr" => $order['shipping_address_1'],
                        "full_delivery_addr" => sprintf('%s%s%s%s',
                            $order['shipping_address_1'],
                            isset($ward['name_with_type']) ? (', ' . $ward['name_with_type']) : '',
                            isset($district['name_with_type']) ? (', ' . $district['name_with_type']) : '',
                            isset($province['name_with_type']) ? (', ' . $province['name_with_type']) : ''
                        ),
                        "ward" => isset($ward['code']) ? $ward['code'] : '',
                        "district" => isset($district['code']) ? $district['code'] : '',
                        "city" => isset($province['code']) ? $province['code'] : '',
                        "note" => $order['comment'],
                        "customer_id" => $order['customer_customer_id'],
                        "subscriber_id" => $order['customer_subscriber_id'],
                        "customer_group_id" => $order['customer_customer_group_id'],
                    ],
                    "status" => $order['order_status_id'],
                    "customer_ref_id" => $order['customer_ref_id'],
                    "order_id" => $order['order_id'],
                    "order_code" => isset($order['order_code']) ? $order['order_code'] : '',
                    "tags" => $tags,
                    "total" => isset($order['total']) ? $order['total'] : '',
                    "date_added" => isset($order['date_added']) ? $order['date_added'] : '',
                    "date_modified" => isset($order['date_modified']) ? $order['date_modified'] : '',
                    "products" => $products_info,
                    "payment_trans" => $order['shipping_method'],
                    "payment_trans_custom_name" => $order['shipping_method'] == '-3' ? $order['shipping_method_value'] : '',
                    "payment_trans_custom_fee" => $order['shipping_method'] == '-3' ? $order['shipping_fee'] : '',
                    "payment_status" => $order['payment_status'] == 1 ? 1 : 0,
                    "delivery_fee" => $order['shipping_fee'],
                    "changable_order_statuses" => $CHANGEABLE_ORDER_STATUS[(int)$order['order_status_id']],
                    'from_district_id_ghn' => $this->config->get('config_GHN_hub'),
                    'payment_method' => isset($order['payment_method']) ? $order['payment_method'] : '',
                    'user_id' => isset($order['user_id']) ? $order['user_id'] : '',
                    'order_source' => isset($order['source']) ? $order['source'] : 'chatbot', // for chatbot: default text 'chatbot'
                    'transports' => $data_trans_result,
                    'text_pick_address' => $this->config->get('config_address'),
                    'campaign_id' => $this->model_sale_order->getCampaignId($order['order_id']),
                    'channel_id' => $this->model_sale_order->getChannelId($order['order_id'])
                ];
            }

            $json = [
                'total' => $orders_total,
                'page' => (isset($this->request->get['page']) && !empty($this->request->get['page'])) ? $this->request->get['page'] : '',
                'total_revenue' => $orders_total_revenue,
                'records' => $records
            ];
        } catch (Exception $e) {
            $len = mb_strlen($e->getMessage()) > self::__ERR_MESSAGE_MAX_LENGTH ? self::__ERR_MESSAGE_MAX_LENGTH : null; // null for sub string to the end

            $json = [
                'code' => 201,
                'message' => 'unknown error',
                '__error' => mb_substr($e->getMessage(), 0, $len)
            ];
        }

        return $json;
    }

    /**
     * getStaffsList
     * @param array $data_filter
     * @return array
     */
    public function getStaffsListData(array $data_filter)
    {
        try {
            $this->load->model('user/user_group');

            $staffs = $this->getStaffsList($data_filter);
            $staffs_total = $this->getStaffsListTotal($data_filter);

            $records = [];
            foreach ($staffs as $staff) {
                $user_group =  $this->model_user_user_group->getUserGroup($staff['user_group_id']);

                $records[] = array(
                    'id' => $staff['user_id'],
                    'avatar' => $staff['image'],
                    'username' => $staff['username'],
                    'firstname' => $staff['firstname'],
                    'lastname' => $staff['lastname'],
                    'user_group_name' => $user_group ? $user_group['name'] : ''
                );
            }

            $json = [
                'total' => (int)$staffs_total,
                'page' => (isset($this->request->get['page']) && !empty($this->request->get['page'])) ? (int)$this->request->get['page'] : 1,
                'records' => $records
            ];
        } catch (Exception $e) {
            $len = mb_strlen($e->getMessage()) > self::__ERR_MESSAGE_MAX_LENGTH ? self::__ERR_MESSAGE_MAX_LENGTH : null; // null for sub string to the end

            $json = [
                'code' => 500,
                'message' => 'unknown error',
                '__error' => mb_substr($e->getMessage(), 0, $len)
            ];
        }

        return $json;
    }

    /**
     * getStaffsList
     *
     * @param $data
     * @return mixed
     */
    public function getStaffsList($data)
    {
        $sql = $this->getAllStaffsSql($data);

        /* pagination */
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = $this->config->get('config_limit_admin');
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * getStaffsListTotal
     *
     * @param $data
     * @return mixed
     */
    public function getStaffsListTotal($data)
    {
        $sqlAll = $this->getAllStaffsSql($data);
        $query = $this->db->query("SELECT COUNT(t.`user_id`) AS `total` FROM (" . $sqlAll . ") as t");

        return $query->row['total'];
    }

    /**
     * getAllStaffsSql
     *
     * @param $data
     * @return string sql with alias "s" for staff
     */
    public function getAllStaffsSql($data)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT s.user_id, 
                       s.username, 
                       s.firstname, 
                       s.lastname, 
                       s.email, 
                       s.telephone, 
                       s.image, 
                       s.status, 
                       s.type, 
                       s.permission, 
                       s.info, 
                       s.user_group_id 
                FROM `{$DB_PREFIX}user` as s 
                WHERE 1=1 
                AND s.type = 'Staff'";

        // filter "staff_ids"
        if (isset($data['staff_ids']) && !empty($data['staff_ids'])) {
            $staff_ids = explode(',', $data['staff_ids']);
            $staff_ids_query = [];
            foreach ($staff_ids as $s_id) {
                if (empty($s_id)) {
                    continue;
                }

                $staff_ids_query[] = (int)$s_id;
            }
            $staff_ids_query = implode(',', $staff_ids_query);

            if (!empty($staff_ids_query)) {
                $sql .= " AND s.`user_id` IN ({$staff_ids_query}) ";
            }
        }

        // ORDER BY
        $sort_data = array(
            'username',
            'status',
            'date_added'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY username";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        return $sql;
    }

    /**
     * getCategoriesListData
     * @param array $data_filter
     * @return array
     */
    public function getCategoriesListData(array $data_filter)
    {
        try {
            $this->load->model('catalog/category');

            $categories = $this->getCategoriesList($data_filter);
            $categories_total = $this->getCategoriesListTotal($data_filter);

            $records = [];
            foreach ($categories as $category) {
                if ($category['status'] != '1') {
                    continue;
                }

                $records[] = [
                    "category_id" => $category['category_id'],
                    "name" => $category['name'],
                    "image" => $category['image'],
                ];
            }

            $json = [
                'total' => $categories_total,
                'page' => (isset($this->request->get['page']) && !empty($this->request->get['page'])) ? $this->request->get['page'] : '',
                'records' => $records
            ];
        } catch (Exception $e) {
            $len = mb_strlen($e->getMessage()) > self::__ERR_MESSAGE_MAX_LENGTH ? self::__ERR_MESSAGE_MAX_LENGTH : null; // null for sub string to the end

            $json = [
                'code' => 201,
                'message' => 'unknown error',
                '__error' => mb_substr($e->getMessage(), 0, $len)
            ];
        }

        return $json;
    }

    /**
     * getCategoriesList
     *
     * @param $data
     * @return mixed
     */
    public function getCategoriesList($data)
    {
        $sql = $this->getAllCategoriesSql($data);

        /* pagination */
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = $this->config->get('config_limit_admin');
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * getStaffsListTotal
     *
     * @param $data
     * @return mixed
     */
    public function getCategoriesListTotal($data)
    {
        $sqlAll = $this->getAllCategoriesSql($data);
        $query = $this->db->query("SELECT COUNT(t.`category_id`) AS `total` FROM (" . $sqlAll . ") as t");

        return $query->row['total'];
    }

    /**
     * getAllStaffsSql
     *
     * @param $data
     * @return string sql with alias "s" for staff
     */
    public function getAllCategoriesSql($data)
    {
        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT c.category_id, 
                       cd.name, 
                       c.image, 
                       c.status 
                FROM {$DB_PREFIX}category c 
                LEFT JOIN {$DB_PREFIX}category_description cd ON (c.category_id = cd.category_id) 
                LEFT JOIN {$DB_PREFIX}category_to_store c2s ON (c.category_id = c2s.category_id) 
                WHERE 1=1
                  AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "'
                  AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
                  AND c.status = '1'";

        // filter "category_ids"
        if (isset($data['category_ids']) && !empty($data['category_ids'])) {
            $category_ids = explode(',', $data['category_ids']);
            $category_ids_query = [];
            foreach ($category_ids as $s_id) {
                if (empty($s_id)) {
                    continue;
                }

                $category_ids_query[] = (int)$s_id;
            }
            $category_ids_query = implode(',', $category_ids_query);

            if (!empty($category_ids_query)) {
                $sql .= " AND c.`category_id` IN ({$category_ids_query}) ";
            }
        }

        // ORDER BY
        $sql .= " ORDER BY c.`category_id`";
        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " DESC";
        }

        return $sql;
    }

    /**
     * getManufacturersList
     * @param array $data_filter
     * @return array
     */
    public function getManufacturersListData(array $data_filter)
    {
        try {
            $this->load->model('catalog/manufacturer');

            $staffs = $this->getManufacturersList($data_filter);
            $staffs_total = $this->getManufacturersListTotal($data_filter);

            $records = [];
            foreach ($staffs as $staff) {
                if ($staff['status'] !== '1') {
                    continue;
                }

                $records[] = [
                    "manufacturer_id" => $staff['manufacturer_id'],
                    "name" => $staff['name'],
                ];
            }

            $json = [
                'total' => $staffs_total,
                'page' => (isset($this->request->get['page']) && !empty($this->request->get['page'])) ? $this->request->get['page'] : '',
                'records' => $records
            ];
        } catch (Exception $e) {
            $len = mb_strlen($e->getMessage()) > self::__ERR_MESSAGE_MAX_LENGTH ? self::__ERR_MESSAGE_MAX_LENGTH : null; // null for sub string to the end

            $json = [
                'code' => 201,
                'message' => 'unknown error',
                '__error' => mb_substr($e->getMessage(), 0, $len)
            ];
        }

        return $json;
    }

    /**
     * getManufacturersList
     *
     * @param $data
     * @return mixed
     */
    public function getManufacturersList($data)
    {
        $sql = $this->getAllManufacturersSql($data);

        /* pagination */
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = $this->config->get('config_limit_admin');
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * getManufacturersListTotal
     *
     * @param $data
     * @return mixed
     */
    public function getManufacturersListTotal($data)
    {
        $sqlAll = $this->getAllManufacturersSql($data);
        $query = $this->db->query("SELECT COUNT(t.`manufacturer_id`) AS `total` FROM (" . $sqlAll . ") as t");

        return $query->row['total'];
    }

    /**
     * get customers list data
     *
     * @param array $query_params
     * @param bool $is_all_statuses if true, includes status hide, deleted, ...
     * @param array $more_return_fields
     * @return array|mixed
     */
    public function getCustomersListData(array $query_params)
    {
        if (isset($query_params['filter_search'])) {
            $filter_search = $query_params['filter_search'];
        } else {
            $filter_search = '';
        }

        if (isset($query_params['phone'])) {
            $phone = $query_params['phone'];
        } else {
            $phone = '';
        }

        if (isset($query_params['phone_operator'])) {
            $phone_operator = $query_params['phone_operator'];
        } else {
            $phone_operator = '';
        }

        if (isset($query_params['phone_value'])) {
            $phone_value = $query_params['phone_value'];
        } else {
            $phone_value = '';
        }

        if (isset($query_params['order'])) {
            $order = $query_params['order'];
        } else {
            $order = '';
        }

        if (isset($query_params['order_operator'])) {
            $order_operator = $query_params['order_operator'];
        } else {
            $order_operator = '';
        }

        if (isset($query_params['order_value'])) {
            $order_value = $query_params['order_value'];
        } else {
            $order_value = '';
        }

        if (isset($query_params['order_latest'])) {
            $order_latest = $query_params['order_latest'];
        } else {
            $order_latest = '';
        }

        if (isset($query_params['order_latest_operator'])) {
            $order_latest_operator = $query_params['order_latest_operator'];
        } else {
            $order_latest_operator = '';
        }

        if (isset($query_params['order_latest_value'])) {
            $order_latest_value = $query_params['order_latest_value'];
        } else {
            $order_latest_value = '';
        }

        if (isset($query_params['amount'])) {
            $amount = $query_params['amount'];
        } else {
            $amount = '';
        }

        if (isset($query_params['amount_operator'])) {
            $amount_operator = $query_params['amount_operator'];
        } else {
            $amount_operator = '';
        }

        if (isset($query_params['amount_value'])) {
            $amount_value = $query_params['amount_value'];
        } else {
            $amount_value = '';
        }

        if (isset($query_params['source'])) {
            $source = $query_params['source'];
        } else {
            $source = '';
        }

        if (isset($query_params['source_value'])) {
            $source_value = $query_params['source_value'];
        } else {
            $source_value = '';
        }

        $subscriber_id = isset($query_params['subscriber_id']) ? $query_params['subscriber_id'] : '';

        if (isset($query_params['page'])) {
            $page = $query_params['page'];
        } else {
            $page = 1;
        }

        if (isset($query_params['limit'])) {
            $limit = $query_params['limit'];
        } else {
            $limit = $this->config->get('config_limit_admin');
        }

        $data['customers'] = array();
        $filter_data = array(
            'filter_search' => trim($filter_search),
            'phone' => $phone,
            'phone_operator' => $phone_operator,
            'phone_value' => $phone_value,
            'order' => $order,
            'order_operator' => $order_operator,
            'order_value' => $order_value,
            'order_latest' => $order_latest,
            'order_latest_operator' => $order_latest_operator,
            'order_latest_value' => $order_latest_value,
            'amount' => $amount,
            'amount_operator' => $amount_operator,
            'amount_value' => $amount_value,
            'source' => $source,
            'source_value' => $source_value,
            'subscriber_id' => $subscriber_id,
            'start' => ($page - 1) * $limit,
            'limit' => $limit
        );
        $customer_total = $this->getTotalCustomers($filter_data);

        $results = $this->getCustomers($filter_data);

        foreach ($results as $result) {
            $customer_source = "";
            $is_authenticated = "";
            if ($result['customer_source'] == 1) {
                $customer_source = "NovaonX Social";
            }
            if ($result['customer_source'] == 2) {
                $customer_source = "Facebook";
            }
            if ($result['customer_source'] == 3) {
                $customer_source = "Bestme";
            }
            if ($result['customer_source'] == 1) {
                $is_authenticated = "Đã xác thực";
            }
            if ($result['customer_source'] == 0) {
                $is_authenticated = "Chưa xác thực";
            }
            $data['customers'][] = array(
                'customer_id' => $result['customer_id'],
                'customer_name' => $result['name'],
                'telephone' => $result['telephone'],
                'customer_source' => $customer_source,
                'is_authenticated' => $is_authenticated,
                'count_order' => $result['count_order'],
                'order_total' => isset($result['order_total']) ? number_format($result['order_total'], 0, '', ',') : '',
                'order_latest' => $result['order_latest'],
                'subscriber_id' => $result['subscriber_id'],
                'addresses' => $this->getAddressByCustomerId($result['customer_id'], $result['address_id'])
            );
        }
        return [
            'total' => $customer_total,
            'page' => (isset($query_params['page']) && !empty($query_params['page'])) ? $query_params['page'] : '',
            'records' => $data['customers'],
        ];
    }

    public function getTotalCustomers($data = array()) {
        $sqlAll = $this->getAllCustomersSql($data, true);
        $query = $this->db->query("SELECT COUNT(c.`customer_id`) AS `total` FROM (" . $sqlAll . ") as c");
        return $query->row['total'];
    }

    public function getCustomers($data = array()) {
        $sql = $this->getAllCustomersSql($data);
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getAllCustomersSql($data) {
        $language_id = (int)$this->config->get('config_language_id');
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT c.*, 
                       CONCAT(c.lastname, ' ', c.firstname) AS name, 
                       IFNULL(COUNT(od.order_id), 0) AS count_order, 
                       IFNULL(SUM(od.total), 0) AS order_total, 
                       MAX(od.date_added) as order_latest 
                FROM {$DB_PREFIX}customer c 
                LEFT JOIN {$DB_PREFIX}order od 
                       ON (c.customer_id = od.customer_id AND od.order_status_id NOT IN (6,10))";

        $sql .= " WHERE 1 ";

        if (!empty($data['phone']) && !empty($data['phone_operator'])) {
            if ($data['phone_operator'] == "AT") {
                $sql = $sql . " AND is_authenticated = 1";
            }
            else if ($data['phone_operator'] == "NAT") {
                $sql = $sql . " AND is_authenticated = 0";
            }
        }

        if (!empty($data['source']) && !empty($data['source_value'])) {
            if ($data['source_value'] == 1) {
                $sql = $sql . " AND customer_source = 1";
            }
            else if ($data['source_value'] == 2) {
                $sql = $sql . " AND customer_source = 2";
            }
            else if ($data['source_value'] == 3) {
                $sql = $sql . " AND customer_source = 3";
            }
        }

        $implode = array();

        $filter = $data;
        if(isset($data['user_create_id'])) {
            $sql .= " AND c.user_create_id = ". $data['user_create_id'];
        }

        if (!empty($data['filter_search'])) {
            $implode[] = "(c.code LIKE '%" . $this->db->escape($data['filter_search']) . "%' OR CONCAT(c.lastname, ' ', c.firstname) LIKE '%" . $this->db->escape($data['filter_search']) . "%')";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sql = $sql . " group by c.customer_id";

        if (!empty($data['order']) && !empty($data['order_operator']) && !empty($data['order_value'])) {
            if ($data['order_operator'] == "LT") {
                $sql = $sql . " HAVING count_order < ".$data['order_value'];
            }
            else if ($data['order_operator'] == "GT") {
                $sql = $sql . " HAVING count_order > ".$data['order_value'];
            }
            else if ($data['order_operator'] == "EQ"){
                $sql = $sql . " HAVING count_order = ".$data['order_value'];
            }
        }else {
            $sql = $sql . " HAVING 1 ";
        }

        if (!empty($data['order_latest']) && !empty($data['order_latest_operator']) && !empty($data['order_latest_value'])) {
            if ($data['order_latest_operator'] == "AT") {
                $sql = $sql . " AND order_latest > ".'"'.$data['order_latest_value'].'"';
            }
            else if ($data['order_latest_operator'] == "BF") {
                $sql = $sql . " AND order_latest < ".'"'.$data['order_latest_value'].'"';
            }
            else if ($data['order_latest_operator'] == "EQ"){
                $sql = $sql . " AND order_latest = ".'"'.$data['order_latest_value'].'"';
            }
        }

        if (!empty($data['amount']) && !empty($data['amount_operator']) && !empty($data['amount_value'])) {
            if ($data['amount_operator'] == "LT") {
                $sql = $sql . " AND order_total < ".'"'.$data['amount_value'].'"';
            }
            else if ($data['amount_operator'] == "GT") {
                $sql = $sql . " AND order_total > ".'"'.$data['amount_value'].'"';
            }
            else if ($data['amount_operator'] == "EQ"){
                $sql = $sql . " AND order_total = ".'"'.$data['amount_value'].'"';
            }
        }

        if (!empty($data['subscriber_id'])) {
            $sql .= " AND c.subscriber_id = '" . $data['subscriber_id'] . "'";
        }

        $sql .= " ORDER BY c.date_added DESC";

        return $sql;
    }

    public function checkPhoneExist($phone)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer` WHERE `telephone` = '" . $this->db->escape($phone) . "'");
        if (isset($query->row['customer_id'])) {
            return $query->row['customer_id'];
        }

        return false;
    }

    public function checkEmailExist($email)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer` WHERE `email` = '" . $this->db->escape($email) . "'");
        if (isset($query->row['customer_id'])) {
            return $query->row['customer_id'];
        }

        return false;
    }
    /**
     * getAllManufacturersSql
     *
     * @param $data
     * @return string sql with alias "s" for Manufacturer
     */
    public function getAllManufacturersSql($data)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT m.manufacturer_id, 
                       m.name, 
                       m.status 
                FROM `{$DB_PREFIX}manufacturer` as m 
                WHERE 1=1 
                AND m.status = '1'";

        // filter "manufacturer_ids"
        if (isset($data['manufacturer_ids']) && !empty($data['manufacturer_ids'])) {
            $manufacturer_ids = explode(',', $data['manufacturer_ids']);
            $manufacturer_ids_query = [];
            foreach ($manufacturer_ids as $s_id) {
                if (empty($s_id)) {
                    continue;
                }

                $manufacturer_ids_query[] = (int)$s_id;
            }
            $manufacturer_ids_query = implode(',', $manufacturer_ids_query);

            if (!empty($manufacturer_ids_query)) {
                $sql .= " AND m.`manufacturer_id` IN ({$manufacturer_ids_query}) ";
            }
        }

        // ORDER BY
        $sql .= " ORDER BY m.`manufacturer_id`";
        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " DESC";
        }

        return $sql;
    }

    private function checkIsFullPermission($permission)
    {
        if (!is_array($permission)) {
            return false;
        }

        foreach (ControllerApiChatbotNovaonV2::SUPPORTED_PERMISSIONS as $SUPPORTED_PERMISSION) {
            if (!in_array($SUPPORTED_PERMISSION, $permission)) {
                return false;
            }
        }

        return true;
    }

    /**
     * get list of payment methods
     * @param $data
     * @return array|mixed
     */
    public function getPaymentMethods() {
        $payment_methods = $this->model_setting_setting->getSettingValue('payment_methods');
        $payment_methods = json_decode($payment_methods, true);
        $payment_methods = is_array($payment_methods) ? $payment_methods : [];
        // add vnpay payment method
        $this->load->model('appstore/my_app');
        if ($this->model_appstore_my_app->checkAppActive(self::APP_CODE_PAYMENT_VNPAY)) {
            $payment_methods[] = [
                'payment_id' => self::APP_CODE_PAYMENT_VNPAY,
                'name' => $this->language->get('text_vnpay_payment'),
                'guide' => '',
                'status' => 1
            ];
        }

        return $payment_methods;
    }

    /**
     * getProductsCustom
     *
     * @param array $filter_data
     * @return mixed
     */
    public function getProductsCustom($filter_data)
    {
        $sql = $this->getProductsCustomSql($filter_data);

        /* pagination */
        if (isset($filter_data['start']) || isset($filter_data['limit'])) {
            if ($filter_data['start'] < 0) {
                $filter_data['start'] = 0;
            }

            if ($filter_data['limit'] < 1) {
                $data['limit'] = $this->config->get('config_limit_admin');
            }

            $sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * getProductsCustomSql
     *
     * @param $filter_data
     * @return string
     */
    private function getProductsCustomSql($filter_data)
    {
        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT p.*, CONCAT(u.lastname, ' ', u.firstname) as fullname , 
                       pd.name as product_name, 
                       pd.description as product_description, 
                       pd.sub_description as product_sub_description, 
                       pd.seo_title as seo_title, 
                       pd.seo_description as seo_description, 
                       pd.meta_keyword as meta_keyword, 
                       p.image as product_image, 
                       SUM(pts.quantity) total_quantity 
                FROM  {$DB_PREFIX}product p 
                LEFT JOIN {$DB_PREFIX}product_description pd 
                       ON (pd.product_id = p.product_id) 
                LEFT JOIN " . DB_PREFIX ."user u ON (u.user_id = p.user_create_id) ";

        $for_default_store_only = true;
        if ($for_default_store_only) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_to_store pts ON (pts.product_id = p.product_id AND pts.store_id = p.default_store_id)"; // for default store only
        } else {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_to_store pts ON (pts.product_id = p.product_id)";
        }

        $sql .= " LEFT JOIN {$DB_PREFIX}store s ON (pts.store_id = s.store_id) ";

        if (isset($filter_data['manufacturer']) && $filter_data['manufacturer'] != 0) {
            $sql .= " LEFT JOIN {$DB_PREFIX}manufacturer m ON (m.manufacturer_id = p.manufacturer_id) ";
        }

        if (!empty($filter_data['category_product'])) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_to_category c ON (p.product_id = c.product_id) ";
            $sql .= " LEFT JOIN {$DB_PREFIX}category_description cd ON (c.category_id = cd.category_id) ";
        }

        if (isset($filter_data['collection']) && $filter_data['collection'] != 0) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_collection pc ON (p.product_id = pc.product_id) ";
        }

        if ((isset($filter_data['filter_product_ids']) && $filter_data['filter_product_ids'])
            || (isset($filter_data['filter_except_product_ids']) && $filter_data['filter_except_product_ids'])
            || (isset($filter_data['filter_product_version_id']) && !empty($filter_data['filter_product_version_id']))) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_version pv ON (p.product_id = pv.product_id) ";
        }

        $sql .= " LEFT JOIN {$DB_PREFIX}product_tag pt ON (pt.product_id = p.product_id) ";
        $sql .= " LEFT JOIN {$DB_PREFIX}tag t ON (t.tag_id = pt.tag_id) ";
        $sql .= " WHERE pd.language_id = " . (int)$this->config->get('config_language_id') . " ";

        if(isset($filter_data['user_create_id']) && $filter_data['user_create_id'] !== '') {
            $sql .= ' AND p.user_create_id=' . $filter_data['user_create_id'];
        }

        if (isset($filter_data['filter_staff']) && $filter_data['filter_staff'] !== '') {
            $sql .= " AND p.user_create_id IN (" . $filter_data['filter_staff'] . ")";
        }

        if (isset($filter_data['status_product']) && $filter_data['status_product'] !== '') {
            $sql .= ' AND p.status=' . $filter_data['status_product'];
        }

        if (isset($filter_data['manufacturer']) && $filter_data['manufacturer'] != 0) {
            $sql .= ' AND m.name LIKE "%' . $filter_data['manufacturer'] . '%"';
        }

        if (isset($filter_data['nametag']) && $filter_data['nametag'] != '') {
            $sql .= " AND t.value RLIKE '" . $filter_data['nametag'] . "'";
        }

        if (!empty($filter_data['category_product'])) {
            $sql .= ' AND cd.name LIKE "%' . $filter_data['category_product'] . '%" ';
        }

        if (isset($filter_data['collection']) && $filter_data['collection'] != 0) {
            $sql .= ' AND pc.collection_id IN (' . $filter_data['collection'] . ')';
        }

        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '') {
            $sql .= " AND (pd.`name` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' ";
            $sql .= " OR t.`value` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' ) ";
        }

        if (isset($filter_data['filter_product_ids']) && $filter_data['filter_product_ids']) {
            $include_product_ids = explode(',', $filter_data['filter_product_ids']);
            $include_product_ids_query = [];
            $include_product_version_ids_query = [];
            foreach ($include_product_ids as $include_pid) {
                if (!$include_pid) {
                    continue;
                }

                // support version id
                if (strpos($include_pid, '_') > 0) {
                    $p_and_ver = explode('_', $include_pid);
                    if (!is_array($p_and_ver) || count($p_and_ver) < 2) {
                        continue;
                    }

                    // add version only, DO NOT add product id!
                    $include_product_version_ids_query[] = $p_and_ver[1];
                } else {
                    $include_product_ids_query[] = $include_pid;
                }
            }
            $include_product_ids_query = implode(',', $include_product_ids_query);
            $include_product_version_ids_query = implode(',', $include_product_version_ids_query);

            if (!empty($include_product_ids_query)) {
                $sql .= " AND p.product_id IN ({$include_product_ids_query})";
            }

            if (!empty($include_product_version_ids_query)) {
                $sql .= " AND (CASE WHEN p.multi_versions = 1 THEN pv.product_version_id IN ({$include_product_version_ids_query}) ELSE 1=1 END)";
            }
        }

        if (isset($filter_data['filter_except_product_ids']) && $filter_data['filter_except_product_ids']) {
            $except_product_ids = explode(',', $filter_data['filter_except_product_ids']);
            $except_product_ids_query = [];
            $except_product_version_ids_query = [];
            foreach ($except_product_ids as $except_pid) {
                if (!$except_pid) {
                    continue;
                }

                // support version id
                if (strpos($except_pid, '_') > 0) {
                    $p_and_ver = explode('_', $except_pid);
                    if (!is_array($p_and_ver) || count($p_and_ver) < 2) {
                        continue;
                    }

                    // add version only, DO NOT add product id!
                    $except_product_version_ids_query[] = $p_and_ver[1];
                } else {
                    $except_product_ids_query[] = $except_pid;
                }
            }
            $except_product_ids_query = implode(',', $except_product_ids_query);
            $except_product_version_ids_query = implode(',', $except_product_version_ids_query);

            if (!empty($except_product_ids_query)) {
                $sql .= " AND p.product_id NOT IN ({$except_product_ids_query})";
            }

            if (!empty($except_product_version_ids_query)) {
                $sql .= " AND (CASE WHEN p.multi_versions = 1 THEN pv.product_version_id NOT IN ({$except_product_version_ids_query}) ELSE 1=1 END)";
            }
        }

        if (isset($filter_data['filter_product_id']) && !empty($filter_data['filter_product_id'])) {
            $sql .= " AND p.product_id = '" . (int)$filter_data['filter_product_id'] . "'";
        }

        if (isset($filter_data['filter_product_version_id']) && !empty($filter_data['filter_product_version_id'])) {
            $sql .= " AND pv.product_version_id = '" . (int)$filter_data['filter_product_version_id'] . "'";
        }

        if (isset($filter_data['filter_store']) && $filter_data['filter_store'] != null) {
            $sql .= " AND pts.store_id = '" . $this->db->escape($filter_data['filter_store']) . "' ";
        }

        if (isset($filter_data['filter_status']) && $filter_data['filter_status'] != null) {
            $sql .= " AND p.status = '" . $this->db->escape($filter_data['filter_status']) . "' ";
        }

        if (isset($filter_data['filter_source']) && $filter_data['filter_source'] != null) {
            $sql .= " AND p.source = '" . $this->db->escape($filter_data['filter_source']) . "' ";
        }

        $sql .= " AND p.`deleted` IS NULL ";
        $sql .= " group by p.product_id";

        if (!empty($filter_data['quantity'])) {
            $filter_quantity = explode('_', $filter_data['quantity']);
            $operator_text = isset($filter_quantity[0]) ? $filter_quantity[0] : 'eq';
            $quantity = isset($filter_quantity[1]) ? $filter_quantity[1] : 0;
            $operator = $this->getOperatorFromText($operator_text);

            $sql .= " HAVING total_quantity " . $operator . " " . (int)$quantity . "";
        }

        $sql .= " ORDER BY p.date_modified, p.product_id DESC";

        return $sql;
    }

    /**
     * getTotalProductsCustom
     *
     * @param array $filter_data
     * @return mixed
     */
    public function getTotalProductsCustom($filter_data)
    {
        $sqlAll = $this->getProductsCustomSql($filter_data);
        $query = $this->db->query("SELECT COUNT(t.`product_id`) AS `total` FROM (" . $sqlAll . ") as t");

        return $query->row['total'];
    }

    /**
     * unescape version value due to version may contains special char that was escaped
     * e.g BLACK & WHITE => BLACK &apm; WHITE => BLACK__apm__WHITE instead of our expect is BLACK___WHITE
     * @param $str
     * @return string
     */
    private function replaceVersionValues($str) {
        $str = html_entity_decode($str);

        return preg_replace('/[^0-9a-zA-Z\-_]+/', '_', $str) . '_' . md5($str);
    }

    /**
     * get operator from text
     * @param $operator_text
     * @return string
     */
    private function getOperatorFromText($operator_text) {
        switch ($operator_text) {
            case 'lt':
                return '<';
            case 'gt':
                return '>';
            default:
                return '=';
        }
    }

    /**
     * @param $customer_id
     * @param $default_address_id
     * @return array
     */
    private function getAddressByCustomerId($customer_id, $default_address_id)
    {
        if (!isset($customer_id) || !isset($default_address_id)) {
            return [];
        }

        $result = [];

        $address_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");

        if ($address_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

            if ($country_query->num_rows) {
                $country = $country_query->row['name'];
                $iso_code_2 = $country_query->row['iso_code_2'];
                $iso_code_3 = $country_query->row['iso_code_3'];
                $address_format = $country_query->row['address_format'];
            } else {
                $country = '';
                $iso_code_2 = '';
                $iso_code_3 = '';
                $address_format = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

            if ($zone_query->num_rows) {
                $zone = $zone_query->row['name'];
                $zone_code = $zone_query->row['code'];
            } else {
                $zone = '';
                $zone_code = '';
            }
            $this->load->model('localisation/vietnam_administrative');
            foreach ($address_query->rows as $address) {
                $city = $this->model_localisation_vietnam_administrative->getProvinceByCode($address['city']);
                $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($address['district']);
                $ward = $this->model_localisation_vietnam_administrative->getWardByCode($address['wards']);
                $delivery_addr = $ward['name_with_type']." ".$district['name_with_type']." ".$city['name_with_type'];
                $result[] = array(
                    'customer_id' => $address['customer_id'],
                    'firstname' => $address['firstname'],
                    'lastname' => $address['lastname'],
                    'address' => $address['address'],
                    'city' => $address['city'],
                    'district' => $address['district'],
                    'wards' => $address['wards'],
                    'delivery_addr' => $delivery_addr,
                    'phone' => $address['phone'],
                    'default_address' => $default_address_id == $address['address_id'] ? true : false,
                );
            }
        }

        return $result;
    }
}
