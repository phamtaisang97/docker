<?php

class ModelApiChatboxNovaon extends Model
{
    /**
     * @param array $data
     * @return array
     */
    public function getAPIProducts($data = array())
    {
        $sql = $this->getAPIProductsSql($data);

        /* pagination */
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = $this->config->get('config_limit_admin');
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @param array $data
     * @return int
     */
    public function getAPIProductsTotal($data = [])
    {
        $sqlAll = $this->getAPIProductsSql($data, true);
        $query = $this->db->query("SELECT COUNT(t.`product_id`) AS `total` FROM (" . $sqlAll . ") as t");

        return $query->row['total'];
    }


    /**
     * @param array $data
     * TODO: check usage and rename...
     * @param bool $ware_house_only
     * @return string
     */
    public function getAPIProductsSql($data = [], $ware_house_only = false)
    {
        $DB_PREFIX = DB_PREFIX;

        /* sub-sql */
        $sql = "SELECT p.`product_id`, 
                    (CASE WHEN p.multi_versions = 0 THEN p.sku ELSE pv.sku END) as sku, 
                    p.`common_barcode`, 
                    p.`common_sku`, 
                    p.`common_compare_price`, 
                    p.`common_price`, 
                    (CASE WHEN p.multi_versions = 0 THEN p.barcode ELSE pv.barcode END) as barcode, 
                    /* quantity query later, not work in this sql, why? */
                    p.`sale_on_out_of_stock`, 
                    p.`stock_status_id`, 
                    p.`image`, 
                    p.`image_alt`, 
                    p.`multi_versions`, 
                    p.`manufacturer_id`, 
                    (CASE WHEN p.multi_versions = 0 
                          THEN IF(p.price=0, p.compare_price, p.price) 
                          ELSE IF(pv.price=0, pv.compare_price, pv.price) END) AS price, 
                    p.`price` as price_master, 
                    (CASE WHEN p.multi_versions = 0 
                          THEN IF(p.compare_price=0, p.price, p.compare_price) 
                          ELSE IF(pv.compare_price=0, pv.price, pv.compare_price) END) AS compare_price, 
                    p.`compare_price` as compare_price_master, 
                    p.`weight`, 
                    p.`status`, 
                    p.`channel`, 
                    p.`demo`, 
                    p.`deleted`, 
                    p.`default_store_id`, 
                    p.`date_added`, 
                    p.`date_modified`, 
                    pd.`language_id`, 
                    pd.`name`, 
                    pd.`description`, 
                    pd.`sub_description`,
                    (CASE WHEN p.multi_versions = 0 THEN NULL ELSE pv.version END) AS name_version,  
                    p.product_id as product_id_master, 
                    m.name AS manufacturer, 
                    MIN(case when pv.price > 0 then pv.price else pv.compare_price end) as min_price_version, 
                    MAX(case when pv.price > 0 then pv.price else pv.compare_price end) as max_price_version, 
                    MAX(pv.price) as price_version_check_null, 
                    MIN(pv.compare_price) as min_compare_price_version, 
                    MAX(pv.compare_price) as max_compare_price_version, MAX(pv.status) as product_version_status_max ,
                    MIN(case when pv.price > 0 then ((pv.price - pv.compare_price)/pv.compare_price*100) end) as max_percent_pv, 
                    (CASE WHEN p.multi_versions = 0 
                          THEN IF(p.price=0, p.compare_price, p.price) 
                          ELSE IF(pv.price=0, pv.compare_price, pv.price) END) AS price_version, 
                    (CASE WHEN p.multi_versions = 0 
                          THEN IF(p.compare_price=0, p.price, p.compare_price) 
                          ELSE IF(pv.compare_price=0, pv.price, pv.compare_price) END) AS compare_price_version, 
                    (CASE WHEN p.multi_versions = 0 
                          THEN p.sku 
                          ELSE pv.sku END) AS sku_version, 
                    IF(p.multi_versions = 0, 0, pv.product_version_id) AS product_version_id, 
                    s.store_id, 
                    s.name as store_name 
                FROM  {$DB_PREFIX}product p 
                LEFT JOIN {$DB_PREFIX}product_description pd ON (pd.product_id = p.product_id) 
                LEFT JOIN {$DB_PREFIX}product_version pv ON (pv.product_id = p.product_id) 
                LEFT JOIN {$DB_PREFIX}manufacturer m ON (m.manufacturer_id = p.manufacturer_id) ";

        $for_default_store_only = true;
        if ($for_default_store_only) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_to_store pts ON (pts.product_id = p.product_id AND pts.store_id = p.default_store_id)"; // for default store only
        } else {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_to_store pts ON (pts.product_id = p.product_id)";
        }

        $sql .= " LEFT JOIN {$DB_PREFIX}store s ON (pts.store_id = s.store_id) ";

        if (isset($data['filter_category']) && $data['filter_category'] != 0) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_to_category c ON (p.product_id = c.product_id)";
        }

        if (isset($data['filter_collection']) && $data['filter_collection'] != 0) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_collection pc ON (p.product_id = pc.product_id)";
        }

        if ((isset($data['filter_tag']) && $data['filter_tag'] != '') ||
            (isset($data['filter_name']) && $data['filter_name'] !== '')
        ) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_tag pt ON (pt.product_id = p.product_id) ";
            $sql .= " LEFT JOIN {$DB_PREFIX}tag t ON (t.tag_id = pt.tag_id) ";
        }

        $sql .= " WHERE 1=1 ";

        $is_all_statuses = isset($data['filter_is_all_statuses']) ? $data['filter_is_all_statuses'] : false;

        // exclude deleted if set
        $include_deleted = false;
        if (!$is_all_statuses && !$include_deleted) {
            $sql .= " AND p.`deleted` IS NULL ";
            $sql .= " AND (CASE WHEN p.multi_versions = 0 THEN 1=1 ELSE pv.`deleted` IS NULL END)";
        }

        /* except original product if multi versions */
        // TODO: use or remove?...
        //$sql .= " AND (CASE WHEN p.multi_versions = 0 THEN pv.product_id IS NULL ELSE pv.product_version_id IS NOT NULL AND pv.product_version_id <> 0 END)";

        if (!$is_all_statuses) {
            $sql .= " AND p.status = 1 AND (CASE WHEN p.multi_versions = 0 THEN 1=1 ELSE pv.status = 1 END)";
        }

        $is_all = getValueByKey($data, 'filter_is_all', 0) == 1;
        if (!$is_all) {
            $sql .= " AND (CASE WHEN p.multi_versions = 0 AND p.sale_on_out_of_stock = 0 THEN pts.quantity > 0 ELSE 1=1 END)";
            $sql .= " AND (CASE WHEN p.multi_versions = 1 AND p.sale_on_out_of_stock = 0 THEN pts.quantity > 0 ELSE 1=1 END)";
        }

        $sql .= " AND pd.language_id = " . (int)$this->config->get('config_language_id') . "";

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $sql .= ' AND p.status=' . $data['filter_status'];
        }

        if (isset($data['filter_manufacturer']) && $data['filter_manufacturer'] != 0) {
            $sql .= ' AND m.manufacturer_id IN (' . $data['filter_manufacturer'] . ')';
        }

        if (isset($data['filter_category']) && $data['filter_category'] != 0) {
            $value = implode(", ", array_map('intval', $data['filter_category']));
            $sql .= " AND c.category_id IN(" . $value . ") ";
        }

        if (isset($data['filter_tag']) && $data['filter_tag'] != '') {
            $sql .= " AND t.`value` RLIKE '" . $data['filter_tag'] . "'";
        }

        if (isset($data['filter_collection']) && $data['filter_collection'] != 0) {
            $sql .= ' AND pc.collection_id IN (' . $data['filter_collection'] . ')';
        }

        if (isset($data['filter_name']) && $data['filter_name'] !== '') {
            $sql .= " AND (pd.`name` LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%' ";
            $sql .= " OR t.`value` LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%' ) ";
        }

        /*
         * format: 12,13_1, 13_3:
         * - product: 12 (single version or all versions if has)
         * - product: 13 + version: 1, 3
         */
        if (isset($data['filter_product_ids']) && $data['filter_product_ids']) {
            $include_product_ids = explode(',', $data['filter_product_ids']);
            $include_product_ids_query = [];
            $include_product_version_ids_query = [];
            foreach ($include_product_ids as $include_pid) {
                if (!$include_pid) {
                    continue;
                }

                // support version id
                if (strpos($include_pid, '_') > 0) {
                    $p_and_ver = explode('_', $include_pid);
                    if (!is_array($p_and_ver) || count($p_and_ver) < 2) {
                        continue;
                    }

                    // add version only, DO NOT add product id!
                    $include_product_version_ids_query[] = $p_and_ver[1];
                } else {
                    $include_product_ids_query[] = $include_pid;
                }
            }
            $include_product_ids_query = implode(',', $include_product_ids_query);
            $include_product_version_ids_query = implode(',', $include_product_version_ids_query);

            if (!empty($include_product_ids_query)) {
                $sql .= " AND p.product_id IN ({$include_product_ids_query})";
            }

            if (!empty($include_product_version_ids_query)) {
                $sql .= " AND (CASE WHEN p.multi_versions = 1 THEN pv.product_version_id IN ({$include_product_version_ids_query}) ELSE 1=1 END)";
            }
        }

        /*
         * format: 12,13_1, 13_3:
         * - product: 12 (single version or all versions if has)
         * - product: 13 + version: 1, 3
         */
        if (isset($data['filter_except_product_ids']) && $data['filter_except_product_ids']) {
            $except_product_ids = explode(',', $data['filter_except_product_ids']);
            $except_product_ids_query = [];
            $except_product_version_ids_query = [];
            foreach ($except_product_ids as $except_pid) {
                if (!$except_pid) {
                    continue;
                }

                // support version id
                if (strpos($except_pid, '_') > 0) {
                    $p_and_ver = explode('_', $except_pid);
                    if (!is_array($p_and_ver) || count($p_and_ver) < 2) {
                        continue;
                    }

                    // add version only, DO NOT add product id!
                    $except_product_version_ids_query[] = $p_and_ver[1];
                } else {
                    $except_product_ids_query[] = $except_pid;
                }
            }
            $except_product_ids_query = implode(',', $except_product_ids_query);
            $except_product_version_ids_query = implode(',', $except_product_version_ids_query);

            if (!empty($except_product_ids_query)) {
                $sql .= " AND p.product_id NOT IN ({$except_product_ids_query})";
            }

            if (!empty($except_product_version_ids_query)) {
                $sql .= " AND (CASE WHEN p.multi_versions = 1 THEN pv.product_version_id NOT IN ({$except_product_version_ids_query}) ELSE 1=1 END)";
            }
        }

        if (isset($data['filter_product_id']) && !empty($data['filter_product_id'])) {
            $sql .= " AND p.product_id = '" . (int)$data['filter_product_id'] . "'";
        }

        if (isset($data['filter_product_version_id']) && !empty($data['filter_product_version_id'])) {
            $sql .= " AND pv.product_version_id = '" . (int)$data['filter_product_version_id'] . "'";
        }

        if (isset($data['filter_amount']) && $data['filter_amount']) {
            $sql .= " AND ( ";
            foreach ($data['filter_amount'] as $amounts) {
                $amount = explode('_', $amounts);
                $quantity = isset($amount[1]) ? $amount[1] : 0;

                if ($amount[0] == 2) {
                    $sql .= " pts.quantity <'" . (int)$quantity . "'";
                }
            }
            $sql .= " ) ";
        }

        if (isset($data['filter_store']) && $data['filter_store'] != null) {
            $sql .= " AND pts.store_id = " . $data['filter_store'] . " ";
        }

        $sql .= " GROUP BY p.product_id, pv.product_version_id ";

        /* correct the quantity of product */
        $is_all_condition = ($is_all || (isset($data['filter_product_id']) && !empty($data['filter_product_id'])))
            ? "1=1"
            : "CASE
                  WHEN p2.sale_on_out_of_stock = 0
                  THEN pts2.quantity > 0
                  ELSE 1=1
               END";

        $final_sql = "SELECT p2.*, 
                             IF(p2.multi_versions = 1 AND p2.product_version_id = pts2.product_version_id, pts2.quantity, 0) AS product_version_quantity, 
                             IF(p2.multi_versions = 0 AND pts2.product_version_id = 0, pts2.quantity, 0) AS product_master_quantity, 
                             pts2.quantity AS quantity_in_stock 
                      FROM ($sql) AS p2 
                      LEFT JOIN {$DB_PREFIX}product_to_store pts2 
                             ON (pts2.product_id = p2.product_id 
                                 AND pts2.product_version_id = p2.product_version_id 
                                 AND pts2.store_id = p2.store_id) 
                      WHERE {$is_all_condition}";

        $final_sql .= " GROUP BY p2.product_id, p2.product_version_id ";

        $sort_data = [];
        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $final_sql .= " ORDER BY " . $data['sort'];
        } else {
            $final_sql .= " ORDER BY p2.date_modified";
        }

        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $final_sql .= " ASC";
        } else {
            $final_sql .= " DESC";
        }

        return $final_sql;
    }

    public function getProductSpecials($product_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $query = $this->db->query("SELECT * FROM `{$DB_PREFIX}product_special` 
                                   WHERE product_id = '" . (int)$product_id . "' 
                                   ORDER BY priority, price");

        return $query->rows;
    }

    /**
     * getOrderList
     *
     * @param $data
     * @return mixed
     */
    public function getOrderList($data)
    {
        $sql = $this->getAllOrdersSql($data);

        /* pagination */
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = $this->config->get('config_limit_admin');
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * getOrdersListTotal
     *
     * @param $data
     * @return mixed
     */
    public function getOrdersListTotal($data)
    {
        $sqlAll = $this->getAllOrdersSql($data, true);
        $query = $this->db->query("SELECT COUNT(t.`order_id`) AS `total` FROM (" . $sqlAll . ") as t");

        return $query->row['total'];
    }

    /**
     * getOrderTotalRevenue
     *
     * @param $data
     * @return mixed
     */
    public function getOrderTotalRevenue($data)
    {
        $sqlAll = $this->getAllOrdersSql($data, true);
        $query = $this->db->query("SELECT SUM(t.`total_revenue`) AS `total_revenue` FROM (" . $sqlAll . ") as t");

        return isset($query->row['total_revenue']) ? (float)$query->row['total_revenue'] : 0;
    }

    /**
     * getOrderDetail
     *
     * @param $order_id
     */
    public function getOrderDetail($order_id)
    {

    }

    /* local functions */
    /**
     * getOrderListSql
     *
     * @param $data
     * @param bool $for_total or for listing
     * @return string
     */
    public function getAllOrdersSql($data, $for_total = false)
    {
        $DB_PREFIX = DB_PREFIX;
        $select_all_cols = ($for_total ? '' : '*,');
        $sql = "SELECT {$select_all_cols}
                       o.order_id as order_id, 
                       address.customer_id as address_customer_id, 
                       customer.customer_id as customer_customer_id, 
                       customer.date_added as customer_date_added, 
                       customer.subscriber_id as customer_subscriber_id, 
                       customer.customer_group_id as customer_customer_group_id, 
                       o.date_added as date_added, 
                       o.date_modified as date_modified, 
                       o.telephone as telephone_order, 
                       CONCAT(customer.lastname, ' ', customer.firstname) as customer_full_name, 
                       o.fullname as order_full_name, 
                       o.total as total_revenue, 
                       group_concat(t.`value` separator ',') as tag_value 
                FROM `{$DB_PREFIX}order` as o 
                LEFT JOIN `{$DB_PREFIX}address` as address ON o.`customer_id` = address.`customer_id` 
                LEFT JOIN `{$DB_PREFIX}customer` as customer ON o.`customer_id` = customer.`customer_id` 
                LEFT JOIN `{$DB_PREFIX}order_tag` ot ON (o.`order_id` = ot.`order_id`) 
                LEFT JOIN `{$DB_PREFIX}tag` t ON (ot.`tag_id` = t.`tag_id`) ";

        // join for filtering order utm
        if (isset($data['order_utm_key']) && !empty($data['order_utm_key'])) {
            $sql .= " LEFT JOIN `{$DB_PREFIX}order_utm` as ou ON o.`order_id` = ou.`order_id` ";
        }

        // filter order campaign
        if (isset($data['order_campaign']) && !empty($data['order_campaign'])) {
            $sql .= " LEFT JOIN `{$DB_PREFIX}order_campaign` as o_campaign ON o.`order_id` = o_campaign.`order_id` ";
        }

        // filter order chanel
        if (isset($data['order_channel']) && !empty($data['order_channel'])) {
            $sql .= " LEFT JOIN `{$DB_PREFIX}order_channel` as o_channel ON o.`order_id` = o_channel.`order_id` ";
        }

        // filter order product
        if (isset($data['order_product']) && !empty($data['order_product'])) {
            $sql .= " LEFT JOIN `{$DB_PREFIX}order_product` as o_product ON o.`order_id` = o_product.`order_id` ";
            $sql .= " JOIN `{$DB_PREFIX}product_description` as p_des ON (o_product.`product_id` = p_des.`product_id` AND p_des.`name` LIKE '%{$this->db->escape($data['order_product'])}%') ";
        }

        // filter campaign_voucher
        if (isset($data['order_promotion_code']) && !empty($data['order_promotion_code'])) {
            $sql .= " LEFT JOIN `{$DB_PREFIX}campaign_voucher` as o_campaign_voucher ON o.`order_id` = o_campaign_voucher.`order_id` ";
        }

        $sql .= " WHERE 1=1 AND `source` = 'chatbot' ";

        // filter order_status backward compatibility
        // removed!

        // filter order_status new
        if (isset($data['order_status']) && !empty($data['order_status'])) {
//            $order_status_array = implode(",", $data['order_status']);
//            if (!empty($order_status_array)) {
//                $sql .= " AND o.`order_status_id` IN (" . $order_status_array . ") ";
//            }

            $order_status_array = explode(',', $data['order_status']);
            $order_status_query = [];
            foreach ($order_status_array as $s_id) {
                if (empty($s_id)) {
                    continue;
                }

                $order_status_query[] = (int)$s_id;
            }
            $order_status_query = implode(',', $order_status_query);

            if (!empty($order_status_query)) {
                $sql .= " AND o.`order_status_id` IN ({$order_status_query}) ";
            }
        }

        // filter order campaign
        if (isset($data['order_campaign']) && !empty($data['order_campaign'])) {
            $order_campaign = explode(",", $data['order_campaign']);
            if (!empty($order_campaign)) {
                $count = 0;
                // build query
                $sql_campaign = " AND (";
                foreach ($order_campaign as $campaign) {
                    if (!empty($campaign)) {
                        if ($count == 0) {
                            $sql_campaign .= " o_campaign.`campaign_id` LIKE '%{$this->db->escape($campaign)}%' ";
                        } else {
                            $sql_campaign .= " OR o_campaign.`campaign_id` LIKE '%{$this->db->escape($campaign)}%' ";
                        }
                        $count++;
                    }
                }
                $sql_campaign .= " )";

                if ($count > 0) {
                    $sql .= $sql_campaign;
                }
            }
        }

        // filter order channel
        if (isset($data['order_channel']) && !empty($data['order_channel']) && is_array($data['order_channel'])) {
            $order_channel = explode(",", $data['order_channel']);
            if (!empty($order_channel)) {
                $count = 0;
                // build query
                $sql_channel = " AND (";
                foreach ($order_channel as $channel) {
                    if (!empty($campaign)) {
                        if ($count == 0) {
                            $sql_channel .= " o_channel.`channel_id` LIKE '%{$this->db->escape($channel)}%' ";
                        } else {
                            $sql_channel .= " OR o_channel.`channel_id` LIKE '%{$this->db->escape($channel)}%' ";
                        }
                        $count++;
                    }
                }
                $sql_channel .= " )";

                if ($count > 0) {
                    $sql .= $sql_channel;
                }
            }
        }

        // filter order_payment status
        if (isset($data['order_payment']) && $data['order_payment'] != '') {
            $data['order_payment'] = explode(",", $data['order_payment']);

            if (in_array("2", $data['order_payment'])) {
                $sql .= "AND o.previous_order_status_id = '" . (int)ModelSaleOrder::ORDER_STATUS_ID_COMPLETED . "' AND o.order_status_id ='" . (int)ModelSaleOrder::ORDER_STATUS_ID_CANCELLED . "' ";
                $remain = array_diff($data['order_payment'], ['2']);
                if (!empty($remain)) {
                    $payment_status_array = implode(",", $remain);
                    $sql .= " OR o.`payment_status` IN (" . $payment_status_array . ") ";
                    $sql .= " AND NOT (o.previous_order_status_id = '" . (int)ModelSaleOrder::ORDER_STATUS_ID_COMPLETED . "' AND o.order_status_id ='" . (int)ModelSaleOrder::ORDER_STATUS_ID_CANCELLED . "')";
                }
            } else {
                $payment_status_array = implode(",", $data['order_payment']);
                $sql .= " AND o.`payment_status` IN (" . $payment_status_array . ") ";
                $sql .= " AND NOT (o.previous_order_status_id = '" . (int)ModelSaleOrder::ORDER_STATUS_ID_COMPLETED . "' AND o.order_status_id ='" . (int)ModelSaleOrder::ORDER_STATUS_ID_CANCELLED . "')";
            }
        }

        // filter order_tag todo : not use
        /*$count = 0;
        $sql_tag = "";
        if (isset($data['order_tag']) && !empty($data['order_tag'])) {
            // extract to array
            $order_tags = explode(',', $data['order_tag']);
            $order_tags_query = [];
            foreach ($order_tags as $o_t) {
                $o_t = trim($o_t);
                if (empty($o_t)) {
                    continue;
                }

                $order_tags_query[] = $o_t;
            }

            // build query
            $sql_tag = " AND (";
            foreach ($order_tags_query as $tag) {
                if (!empty($tag)) {
                    if ($count == 0) {
                        $sql_tag .= " t.`value` LIKE '%{$this->db->escape($tag)}%' ";
                    } else {
                        $sql_tag .= " OR t.`value` LIKE '%{$this->db->escape($tag)}%' ";
                    }
                    $count++;
                }
            }
            $sql_tag .= " )";
        }

        if ($count > 0) {
            $sql .= $sql_tag;
        }*/

        // filter order_date_modified todo: not use
        /*if (isset($data['order_updated_time_type']) && is_array($data['order_updated_time_type'])) {
            $sql_order = 'AND ( ';
            foreach ($data['order_updated_time_type'] as $idx => $order_updated_time_type) {
                $compare = explode('_', $order_updated_time_type);
                $compare_type = html_entity_decode($compare[0]);

                if (isset($compare[1])) {
                    $compare_value = $compare[1];
                } else {
                    break;
                }

                if (!in_array($compare_type, ['>=', '<=', '>=<='])) {
                    continue;
                }

                try {
                    $order_date_modified = date_create_from_format('d/m/Y', $compare_value);
                } catch (Exception $e) {
                    continue;
                }

                switch ($compare_type) {
                    case '>=':
                        $order_date_modified = $order_date_modified->format('Y-m-d 00:00:00');
                        break;

                    case '<=':
                        $order_date_modified = $order_date_modified->format('Y-m-d 23:59:59');
                        break;
                }

                if ($idx == 0) {
                    $sql_order .= "o.`date_modified` " . $compare_type . "'" . $order_date_modified . "'";
                } else {
                    $sql_order .= " OR o.`date_modified` " . $compare_type . " '" . $order_date_modified . "'";
                }
            }

            $sql = $sql . $sql_order . ' )';
        }*/

        // filter order_total
        if (isset($data['order_total_type']) && !empty($data['order_total_type'])) {
            $data['order_total_type'] = explode(",", $data['order_total_type']);
            /*
             * Nhỏ hơn (NH), Lớn hơn (LH)
             * Nếu NH < LH => query: (o.`total` < NH OR o.`total` > NH). Ví dụ: (o.`total` < 10.000 OR o.`total` > 20.000)
             * Nếu NH > LH => query: (o.`total` > LH AND o.`total` < NH). Ví dụ: (o.`total` < 25.000 AND o.`total` > 15.000)
             * Nếu có điều kiện Bằng ('=', B) => luôn thêm OR. Ví dụ:
             *   - Nếu NH < LH, B => query: ((o.`total` < NH OR o.`total` > NH) OR B). Ví dụ: ((o.`total` < 10.000 OR o.`total` > 20.000) OR o.`total` = 15000 OR o.`total` = 16000)
             *   - Nếu NH > LH => query: ((o.`total` > LH AND o.`total` < NH) OR B). Ví dụ: ((o.`total` < 25.000 AND o.`total` > 15.000) OR o.`total` = 35000)
             */
            $compare_lt = ''; // <
            $compare_gt = ''; // >
            $compare_eqs = []; // =
            foreach ($data['order_total_type'] as $idx => $order_total_type) {
                $compare = explode('_', $order_total_type);
                $compare_type = html_entity_decode($compare[0]);
                $compare_total = isset($compare[1]) ? $compare[1] : 0;

                if ($compare_type == 'lt' && ($compare_lt === '' || $compare_lt > $compare_total)) {
                    $compare_lt = $compare_total; // get min
                }

                if ($compare_type == 'gt' && ($compare_gt === '' || $compare_gt < $compare_total)) {
                    $compare_gt = $compare_total; // get max
                }

                if ($compare_type == 'eq') {
                    $compare_eqs[] = $compare_total; // allow multi values
                }
            }
            // build expression
            $sql_total = '';
            if ($compare_lt <= $compare_gt && $compare_gt !== '' && $compare_lt !== '') {
                $sql_total = "(o.`total` < $compare_lt OR o.`total` > $compare_gt)";
            } elseif ($compare_lt >= $compare_gt && $compare_gt !== '' && $compare_lt !== '') {
                $sql_total = "(o.`total` < $compare_lt AND o.`total` > $compare_gt)";
            } elseif ($compare_lt !== '') {
                $sql_total = "(o.`total` < $compare_lt)";
            } elseif ($compare_gt !== '') {
                $sql_total = "(o.`total` > $compare_gt)";
            }

            foreach ($compare_eqs as $idx => $compare_eq) {
                if (!empty($sql_total)) {
                    $sql_total .= " OR o.`total` = $compare_eq";
                } else {
                    if ($idx === 0) {
                        $sql_total .= "o.`total` = $compare_eq";
                    } else {
                        $sql_total .= " OR o.`total` = $compare_eq";
                    }
                }
            }

            $sql .= " AND ($sql_total)";
        }

        // filter order_code
        if (isset($data['order_code']) && !empty($data['order_code'])) {
            $sql .= " AND o.`order_code` LIKE '%" . $this->db->escape($data['order_code']) . "%' ";
        }

        // TODO: remove...
       /* if (isset($data['order_transfer']) && !empty($data['order_transfer'])) {
            $sql .= " AND o.`order_transfer` = '" . (int)$data['order_transfer'] . "' ";
        }*/

        /*if (isset($data['order_search_text']) && !empty($data['order_search_text'])) {
            $sql .= " AND o.`fullname` LIKE '%" . $this->db->escape($data['order_search_text']) . "%' ";
        }*/

        // filter customer_ref_id
        /*if (isset($data['customer_ref_id']) && !empty($data['customer_ref_id'])) {
            $sql .= " AND o.`customer_ref_id` = '" . $this->db->escape($data['customer_ref_id']) . "' ";
        }*/

        // filter campaign_voucher
        if (isset($data['order_customer_ids']) && !empty($data['order_customer_ids'])) {
            $customer_ids = explode(',', $data['order_customer_ids']);
            $customer_ids_query = [];
            foreach ($customer_ids as $c_id) {
                if (empty($c_id)) {
                    continue;
                }

                $customer_ids_query[] = (int)$c_id;
            }

            $customer_ids_query = implode(',', $customer_ids_query);

            if (!empty($customer_ids_query)) {
                $sql .= " AND customer.`customer_id` IN ({$customer_ids_query}) ";
            }
        }

        // filter campaign_voucher
        if (isset($data['order_promotion_code']) && !empty($data['order_promotion_code'])) {
            $order_promotion_code = explode(",", $data['order_promotion_code']);
            if (!empty($order_promotion_code)) {
                $count = 0;
                // build query
                $sql_promotion_code = " AND (";
                foreach ($order_promotion_code as $o_p_code) {
                    if (!empty($o_p_code)) {
                        if ($count == 0) {
                            $sql_promotion_code .= " o_campaign_voucher.`voucher_code` LIKE '%{$this->db->escape($o_p_code)}%' ";
                        } else {
                            $sql_promotion_code .= " OR o_campaign_voucher.`voucher_code` LIKE '%{$this->db->escape($o_p_code)}%' ";
                        }
                        $count++;
                    }
                }
                $sql_promotion_code .= " )";

                if ($count > 0) {
                    $sql .= $sql_promotion_code;
                }
            }
        }

        // filter order_subscriber_ids
        if (isset($data['order_subscriber_ids']) && !empty($data['order_subscriber_ids'])) {
            $order_subscriber_ids = explode(",", $data['order_subscriber_ids']);
            if (!empty($order_subscriber_ids)) {
                $count = 0;
                // build query
                $sql_order_subscriber = " AND (";
                foreach ($order_subscriber_ids as $subscriber_id) {
                    if (!empty($subscriber_id)) {
                        if ($count == 0) {
                            $sql_order_subscriber .= " customer.`subscriber_id` LIKE '%{$this->db->escape($subscriber_id)}%' ";
                        } else {
                            $sql_order_subscriber .= " OR customer.`subscriber_id` LIKE '%{$this->db->escape($subscriber_id)}%' ";
                        }
                        $count++;
                    }
                }
                $sql_order_subscriber .= " )";

                if ($count > 0) {
                    $sql .= $sql_order_subscriber;
                }
            }
        }

        // filter "order_ids"
        if (isset($data['order_ids']) && !empty($data['order_ids'])) {
            $order_ids = explode(',', $data['order_ids']);
            $order_ids_query = [];
            foreach ($order_ids as $o_id) {
                if (empty($o_id)) {
                    continue;
                }

                $order_ids_query[] = (int)$o_id;
            }
            $order_ids_query = implode(',', $order_ids_query);

            if (!empty($order_ids_query)) {
                $sql .= " AND o.`order_id` IN ({$order_ids_query}) ";
            }
        }

        // filter order_utm todo: not use
        /*if (isset($data['order_utm_key']) && !empty($data['order_utm_key'])) {
            $sql .= " AND ou.`key` = '{$this->db->escape($data['order_utm_key'])}' ";

            $count = 0;
            $sql_utm = '';
            if (isset($data['order_utm_value']) && !empty($data['order_utm_value'])) {
                // extract to array
                $order_utm_values = explode(',', $data['order_utm_value']);
                $order_utm_values_query = [];
                foreach ($order_utm_values as $o_uv) {
                    $o_uv = trim($o_uv);
                    if (empty($o_uv)) {
                        continue;
                    }

                    $order_utm_values_query[] = $o_uv;
                }

                // build query
                $count = 0;
                $sql_utm .= " AND (";
                foreach ($order_utm_values_query as $utm) {
                    if (!empty($utm)) {
                        if ($count == 0) {
                            $sql_utm .= " ou.`value` = '{$this->db->escape($utm)}' ";
                        } else {
                            $sql_utm .= " OR ou.`value` = '{$this->db->escape($utm)}' ";
                        }
                        $count++;
                    }
                }
                $sql_utm .= " )";
            }

            if ($count > 0) {
                $sql .= $sql_utm;
            }
        }*/

        // GROUP
        $sql .= "GROUP BY o.`order_id`";

        // ORDER
        $sql .= " ORDER BY o.`order_id`";
        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " DESC";
        }

        return $sql;
    }
}
