<?php

class ModelRateRate extends Model
{
    CONST RATE_IS_SHOW   = 1;
    CONST RATE_IS_HIDDEN = 0;

    public function getAllRates($data = array())
    {
        $db_prefix = DB_PREFIX;
        $sql = "SELECT * FROM `{$db_prefix}rate_website` where `status` = '1'";
        $query = $this->db->query($sql);

        return $query->rows;
    }
}
