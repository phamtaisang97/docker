<?php

class ModelCustomerCustomer extends Model
{
    public function getAllCustomerGroups()
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "customer_group cg 
		        LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) 
		        WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getAllCustomers()
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "customer`";
        $sql .= " WHERE `status` = 1";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function addCustomer($data)
    {
        // set random password for customer
        $this->db->query("INSERT INTO " . DB_PREFIX . "customer SET  firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape(trim($data['email'])) . "', customer_source = '" . $this->db->escape($data['customer_source']) . "', note = '" . $this->db->escape($data['note']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1(token(9))))) . "', status = 1,  date_added = NOW()");

        $customer_id = $this->db->getLastId();

        // update customer code
        $prefix = 'KH';
        $code = $prefix . str_pad($customer_id, 6, "0", STR_PAD_LEFT);

        $this->db->query("UPDATE " . DB_PREFIX . "customer 
                          SET `code` = '" . $code . "' 
                          WHERE customer_id = '" . (int)$customer_id . "'");

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        //set group_id
        if (isset($data['customer_group_id']) && strlen($data['customer_group_id']) == strlen((int)$data['customer_group_id'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$data['customer_group_id'] . "' WHERE customer_id = '" . (int)$customer_id . "'");
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_group SET approval = 0, sort_order = 1");
            $customer_group_id = $this->db->getLastId();
            $this->db->query("UPDATE " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$customer_group_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
            foreach ($languages as $language) {
                $language_id = $language['language_id'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "customer_group_description SET customer_group_id = '" . (int)$customer_group_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($data['customer_group_id']) . "', description = ''");
            }

        }

        if (isset($data['addr'])) {
            foreach ($data['addr'] as $key => $address) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($address['firstname']) . "', lastname = '" . $this->db->escape($address['lastname']) . "', address = '" . $this->db->escape($address['address']) . "', city = '" . $this->db->escape($address['city']) . "', district = '" . $this->db->escape($address['district']) . "', phone = '" . $this->db->escape($address['phone']) . "'");

                if ($key == 0) {
                    $address_id = $this->db->getLastId();

                    $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
                }
            }
        }

        return $customer_id;
    }

    public function editCustomer($customer_id, $data)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "customer SET  firstname = '" .
            $this->db->escape($data['firstname']) .
            "', lastname = '" . $this->db->escape($data['lastname']) .
            "', email = '" . $this->db->escape(trim($data['email'])) .
            "', customer_source = '" . $this->db->escape($data['customer_source']) .
            "', note = '" . $this->db->escape(isset($data['note']) ? $data['note'] : '') .
            "', telephone = '" . $this->db->escape($data['telephone']) .
            "' WHERE customer_id = '" . (int)$customer_id . "'");

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        //set group_id
        if (isset($data['customer_group_id']) && strlen($data['customer_group_id']) == strlen((int)$data['customer_group_id'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$data['customer_group_id'] . "' WHERE customer_id = '" . (int)$customer_id . "'");
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_group SET approval = 0, sort_order = 1");
            $customer_group_id = $this->db->getLastId();
            $this->db->query("UPDATE " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$customer_group_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
            foreach ($languages as $language) {
                $language_id = $language['language_id'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "customer_group_description SET customer_group_id = '" . (int)$customer_group_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($data['customer_group_id']) . "', description = ''");
            }

        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");

        if (isset($data['addr'])) {
            foreach ($data['addr'] as $key => $address) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($address['firstname']) . "', lastname = '" . $this->db->escape($address['lastname']) . "', address = '" . $this->db->escape($address['address']) . "', city = '" . $this->db->escape($address['city']) . "', district = '" . $this->db->escape($address['district']) . "', phone = '" . $this->db->escape($address['phone']) . "'");

                if ($key == 0) {
                    $address_id = $this->db->getLastId();

                    $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
                }
            }
        }
    }

    public function editToken($customer_id, $token)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "customer SET token = '" . $this->db->escape($token) . "' WHERE customer_id = '" . (int)$customer_id . "'");
    }

    public function deleteCustomer($customer_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
        //$this->db->query("DELETE FROM " . DB_PREFIX . "customer_activity WHERE customer_id = '" . (int)$customer_id . "'");
        //$this->db->query("DELETE FROM " . DB_PREFIX . "customer_affiliate WHERE customer_id = '" . (int)$customer_id . "'");
        //$this->db->query("DELETE FROM " . DB_PREFIX . "customer_approval WHERE customer_id = '" . (int)$customer_id . "'");
        //$this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");
        //$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "'");
        //$this->db->query("DELETE FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int)$customer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");
    }

    public function getCustomer($customer_id)
    {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row;
    }

    public function getCustomerByEmail($email)
    {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

        return $query->row;
    }

    public function getCustomerByTelephone($telephone)
    {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE LCASE(telephone) = '" . $this->db->escape(utf8_strtolower($telephone)) . "'");

        return $query->row;
    }

    public function getCustomers($data = array())
    {
        $sql = "SELECT c.*, CONCAT(c.firstname, ' ', c.lastname) AS name, cgd.name AS customer_group, IFNULL(COUNT(od.order_id), 0) AS count_order, MAX(od.date_added)as order_lastest FROM " . DB_PREFIX . "customer c LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (c.customer_group_id = cgd.customer_group_id) LEFT JOIN " . DB_PREFIX . "order od ON (c.customer_id = od.customer_id)";
        //echo $sql; die();
        if (!empty($data['filter_affiliate'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "customer_affiliate ca ON (c.customer_id = ca.customer_id)";
        }

        $sql .= " WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_email'])) {
            $implode[] = "c.email LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
        }

        if (isset($data['filter_newsletter']) && !is_null($data['filter_newsletter'])) {
            $implode[] = "c.newsletter = '" . (int)$data['filter_newsletter'] . "'";
        }

        if (!empty($data['filter_customer_group_id'])) {
            $implode[] = "c.customer_group_id = '" . (int)$data['filter_customer_group_id'] . "'";
        }

        if (!empty($data['filter_affiliate'])) {
            $implode[] = "ca.status = '" . (int)$data['filter_affiliate'] . "'";
        }

        if (!empty($data['filter_ip'])) {
            $implode[] = "c.customer_id IN (SELECT customer_id FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($data['filter_ip']) . "')";
        }

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $implode[] = "c.status = '" . (int)$data['filter_status'] . "'";
        }

        if (!empty($data['filter_date_added'])) {
            $implode[] = "DATE(c.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'name',
            'c.email',
            'customer_group',
            'c.status',
            'c.ip',
            'c.date_added'
        );

        $sql = $sql . " group by c.customer_id";

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getAddress($address_id)
    {
        $address_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "'");

        if ($address_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

            if ($country_query->num_rows) {
                $country = $country_query->row['name'];
                $iso_code_2 = $country_query->row['iso_code_2'];
                $iso_code_3 = $country_query->row['iso_code_3'];
                $address_format = $country_query->row['address_format'];
            } else {
                $country = '';
                $iso_code_2 = '';
                $iso_code_3 = '';
                $address_format = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

            if ($zone_query->num_rows) {
                $zone = $zone_query->row['name'];
                $zone_code = $zone_query->row['code'];
            } else {
                $zone = '';
                $zone_code = '';
            }

            return array(
                'address_id' => $address_query->row['address_id'],
                'customer_id' => $address_query->row['customer_id'],
                'firstname' => $address_query->row['firstname'],
                'lastname' => $address_query->row['lastname'],
                'company' => $address_query->row['company'],
                'address' => $address_query->row['address'],
                'postcode' => $address_query->row['postcode'],
                'city' => $address_query->row['city'],
                'district' => $address_query->row['district'],
                'wards' => $address_query->row['wards'],
                'phone' => $address_query->row['phone'],
                'country_id' => $address_query->row['country_id'],
                'country' => $country,
                'address_format' => $address_format,
                'custom_field' => json_decode($address_query->row['custom_field'], true)
            );
        }
    }

    public function getAddressByCustomerId($customer_id)
    {
        $address_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");

        if ($address_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

            if ($country_query->num_rows) {
                $country = $country_query->row['name'];
                $iso_code_2 = $country_query->row['iso_code_2'];
                $iso_code_3 = $country_query->row['iso_code_3'];
                $address_format = $country_query->row['address_format'];
            } else {
                $country = '';
                $iso_code_2 = '';
                $iso_code_3 = '';
                $address_format = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

            if ($zone_query->num_rows) {
                $zone = $zone_query->row['name'];
                $zone_code = $zone_query->row['code'];
            } else {
                $zone = '';
                $zone_code = '';
            }

            return array(
                'address_id' => $address_query->row['address_id'],
                'customer_id' => $address_query->row['customer_id'],
                'firstname' => $address_query->row['firstname'],
                'lastname' => $address_query->row['lastname'],
                'company' => $address_query->row['company'],
                'address' => $address_query->row['address'],
                'postcode' => $address_query->row['postcode'],
                'city' => $address_query->row['city'],
                'district' => $address_query->row['district'],
                'phone' => $address_query->row['phone'],
                'country_id' => $address_query->row['country_id'],
                'country' => $country,
                'address_format' => $address_format,
                'custom_field' => json_decode($address_query->row['custom_field'], true),
                'wards' => $address_query->row['wards']
            );
        }
    }

    public function getAddresses($customer_id)
    {
        $address_data = array();

        $query = $this->db->query("SELECT address_id FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");

        foreach ($query->rows as $result) {
            $address_info = $this->getAddress($result['address_id']);

            if ($address_info) {
                $address_data[$result['address_id']] = $address_info;
            }
        }

        return $address_data;
    }

    public function getTotalCustomers($data = array())
    {
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "CONCAT(firstname, ' ', lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_email'])) {
            $implode[] = "email LIKE '" . $this->db->escape($data['filter_email']) . "%'";
        }

        if (isset($data['filter_newsletter']) && !is_null($data['filter_newsletter'])) {
            $implode[] = "newsletter = '" . (int)$data['filter_newsletter'] . "'";
        }

        if (!empty($data['filter_customer_group_id'])) {
            $implode[] = "customer_group_id = '" . (int)$data['filter_customer_group_id'] . "'";
        }

        if (!empty($data['filter_ip'])) {
            $implode[] = "customer_id IN (SELECT customer_id FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($data['filter_ip']) . "')";
        }

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $implode[] = "status = '" . (int)$data['filter_status'] . "'";
        }

        if (!empty($data['filter_date_added'])) {
            $implode[] = "DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if ($implode) {
            $sql .= " WHERE " . implode(" AND ", $implode);
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalCustomersByDate($data = array())
    {
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer";

        $implode = array();


        if (!empty($data['filter_date_added'])) {
            $implode[] = "DATE(date_added) <= DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if ($implode) {
            $sql .= " WHERE " . implode(" AND ", $implode);
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getAffliateByTracking($tracking)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_affiliate WHERE tracking = '" . $this->db->escape($tracking) . "'");

        return $query->row;
    }

    public function getAffiliate($customer_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_affiliate WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row;
    }

    public function getAffiliates($data = array())
    {
        $sql = "SELECT DISTINCT *, CONCAT(c.firstname, ' ', c.lastname) AS name FROM " . DB_PREFIX . "customer_affiliate ca LEFT JOIN " . DB_PREFIX . "customer c ON (ca.customer_id = c.customer_id)";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if ($implode) {
            $sql .= " WHERE " . implode(" AND ", $implode);
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql . "ORDER BY name");

        return $query->rows;
    }

    public function getTotalAffiliates($data = array())
    {
        $sql = "SELECT DISTINCT COUNT(*) AS total FROM " . DB_PREFIX . "customer_affiliate ca LEFT JOIN " . DB_PREFIX . "customer c ON (ca.customer_id = c.customer_id)";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if ($implode) {
            $sql .= " WHERE " . implode(" AND ", $implode);
        }
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalAddressesByCustomerId($customer_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row['total'];
    }

    public function getTotalAddressesByCountryId($country_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE country_id = '" . (int)$country_id . "'");

        return $query->row['total'];
    }

    public function getTotalAddressesByZoneId($zone_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE zone_id = '" . (int)$zone_id . "'");

        return $query->row['total'];
    }

    public function getTotalCustomersByCustomerGroupId($customer_group_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE customer_group_id = '" . (int)$customer_group_id . "'");

        return $query->row['total'];
    }

    public function addHistory($customer_id, $comment)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "customer_history SET customer_id = '" . (int)$customer_id . "', comment = '" . $this->db->escape(strip_tags($comment)) . "', date_added = NOW()");
    }

    public function getHistories($customer_id, $start = 0, $limit = 10)
    {
        if ($start < 0) {
            $start = 0;
        }

        if ($limit < 1) {
            $limit = 10;
        }

        $query = $this->db->query("SELECT comment, date_added FROM " . DB_PREFIX . "customer_history WHERE customer_id = '" . (int)$customer_id . "' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

        return $query->rows;
    }

    public function getTotalHistories($customer_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_history WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row['total'];
    }

    public function addTransaction($customer_id, $description = '', $amount = '', $order_id = 0)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$customer_id . "', order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($description) . "', amount = '" . (float)$amount . "', date_added = NOW()");
    }

    public function deleteTransactionByOrderId($order_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");
    }

    public function getTransactions($customer_id, $start = 0, $limit = 10)
    {
        if ($start < 0) {
            $start = 0;
        }

        if ($limit < 1) {
            $limit = 10;
        }

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

        return $query->rows;
    }

    public function getTotalTransactions($customer_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total  FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row['total'];
    }

    public function getTransactionTotal($customer_id)
    {
        $query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row['total'];
    }

    public function getTotalTransactionsByOrderId($order_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");

        return $query->row['total'];
    }

    public function addReward($customer_id, $description = '', $points = '', $order_id = 0)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "customer_reward SET customer_id = '" . (int)$customer_id . "', order_id = '" . (int)$order_id . "', points = '" . (int)$points . "', description = '" . $this->db->escape($description) . "', date_added = NOW()");
    }

    public function deleteReward($order_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order_id . "' AND points > 0");
    }

    public function getRewards($customer_id, $start = 0, $limit = 10)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

        return $query->rows;
    }

    public function getTotalRewards($customer_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row['total'];
    }

    public function getRewardTotal($customer_id)
    {
        $query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row['total'];
    }

    public function getTotalCustomerRewardsByOrderId($order_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order_id . "' AND points > 0");

        return $query->row['total'];
    }

    public function getIps($customer_id, $start = 0, $limit = 10)
    {
        if ($start < 0) {
            $start = 0;
        }
        if ($limit < 1) {
            $limit = 10;
        }

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int)$customer_id . "' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

        return $query->rows;
    }

    public function getTotalIps($customer_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row['total'];
    }

    public function getTotalCustomersByIp($ip)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($ip) . "'");

        return $query->row['total'];
    }

    public function getTotalLoginAttempts($email)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_login` WHERE `email` = '" . $this->db->escape($email) . "'");

        return $query->row;
    }

    public function deleteLoginAttempts($email)
    {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "customer_login` WHERE `email` = '" . $this->db->escape($email) . "'");
    }

    //v3.5.1 open api

    /**
     * @param $customer_id
     * @return array
     */
    public function getCustomerById($customer_id) {
        $result = $this->getCustomer($customer_id);
        $customer_source = "";
        $is_authenticated = "";
        if ($result['customer_source'] == 1) {
            $customer_source = "NovaonX Social";
        }
        if ($result['customer_source'] == 2) {
            $customer_source = "Facebook";
        }
        if ($result['customer_source'] == 3) {
            $customer_source = "Bestme";
        }
        if ($result['customer_source'] == 1) {
            $is_authenticated = "Đã xác thực";
        }
        if ($result['customer_source'] == 0) {
            $is_authenticated = "Chưa xác thực";
        }
        $data['customers'][] = array(
            'customer_id' => $result['customer_id'],
            'customer_name' => $result['firstname'].$result['lastname'],
            'telephone' => $result['telephone'],
            'customer_source' => $customer_source,
            'is_authenticated' => $is_authenticated,
            'order_total' => isset($result['order_total']) ? number_format($result['order_total'], 0, '', ',') : '',
            'addresses' => $this->getListAddressByCustomerId($result['customer_id'], $result['address_id'])
        );
        return $data;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function getOrderByCustomer($data = array())
    {
        $this->load->model("sale/order");
        $order_status_canceled = ModelSaleOrder::ORDER_STATUS_ID_CANCELLED;
        $sql = "SELECT od.order_status_id, od.order_id, od.order_code, od.date_added, od.total, rr.total as return_receipt
         FROM " . DB_PREFIX . "order od 
         LEFT JOIN " . DB_PREFIX . "return_receipt rr ON (od.order_id = rr.order_id)
         WHERE od.customer_id = '"
            . (int)$data['customer_id'] . "' 
            AND od.order_status_id != $order_status_canceled
            ORDER BY od.date_added DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }

    /**
     * @param $customer_id
     * @param $default_address_id
     * @return array
     */
    public function getListAddressByCustomerId($customer_id, $default_address_id)
    {
        if (!isset($customer_id) || !isset($default_address_id)) {
            return [];
        }

        $result = [];

        $address_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");

        if ($address_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

            if ($country_query->num_rows) {
                $country = $country_query->row['name'];
                $iso_code_2 = $country_query->row['iso_code_2'];
                $iso_code_3 = $country_query->row['iso_code_3'];
                $address_format = $country_query->row['address_format'];
            } else {
                $country = '';
                $iso_code_2 = '';
                $iso_code_3 = '';
                $address_format = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

            if ($zone_query->num_rows) {
                $zone = $zone_query->row['name'];
                $zone_code = $zone_query->row['code'];
            } else {
                $zone = '';
                $zone_code = '';
            }
            $this->load->model('localisation/vietnam_administrative');
            foreach ($address_query->rows as $address) {
                $city = $this->model_localisation_vietnam_administrative->getProvinceByCode($address['city']);
                $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($address['district']);
                $ward = $this->model_localisation_vietnam_administrative->getWardByCode($address['wards']);
                $delivery_addr = $ward['name_with_type']." ".$district['name_with_type']." ".$city['name_with_type'];
                $result[] = array(
                    'customer_id' => $address['customer_id'],
                    'firstname' => $address['firstname'],
                    'lastname' => $address['lastname'],
                    'address' => $address['address'],
                    'city' => $address['city'],
                    'district' => $address['district'],
                    'wards' => $address['wards'],
                    'delivery_addr' => $delivery_addr,
                    'phone' => $address['phone'],
                    'default_address' => $default_address_id == $address['address_id'] ? true : false,
                );
            }
        }

        return $result;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function getCustomerGroups($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "customer_group cg 
		        LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) 
		        WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        $this->load->model('user/user');
        if(isset($data['user_create_id']) && $data['user_create_id'] !== '') {
            $sql .= ' AND cg.user_create_id=' . $data['user_create_id'];
        }

        $sort_data = array(
            'cgd.name',
            'cg.sort_order'
        );

        if (!empty($data['filter_name'])) {
            $key_search = trim($this->db->escape($data['filter_name']));
            $sql .= " AND ( cgd.name LIKE '%" . $key_search . "%' 
             OR cg.customer_group_code LIKE '%" . $key_search . "%')
             ";
        }

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY cgd.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }
        $products_total = $this->getTotalCustomerGroups($data);
        $page = isset($data['page']) ? (int)$data['page'] : 1;
        if (isset($data['page']) && !empty($data['page'])) {
            $data = [
                'start' => ((int)$data['page'] - 1) * ((int)isset($data['limit']) ? $data['limit'] : 15),
                'limit' => isset($data['limit']) ? $data['limit'] : 15
            ];
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        $result = $query->rows;
        return [
            'total' => $products_total,
            'page' => $page,
            'records' => $result
        ];
    }

    /**
     * @param $customer_group_id
     * @return mixed
     */
    public function getCustomerGroup($customer_group_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer_group cg LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) WHERE cg.customer_group_id = '" . (int)$customer_group_id . "' AND cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function addCustomerGroup($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "customer_group SET approval = '" . 1 . "',sort_order = '" . 1 . "'");
        $customer_group_id = $this->db->getLastId();
        // update  id. e.g id = 123 => code(length=9) = NKH000123
        $prefix = 'NKH';
        $code = $prefix . str_pad($customer_group_id, 6, "0", STR_PAD_LEFT);

        $this->db->query("UPDATE " . DB_PREFIX . "customer_group 
                          SET `customer_group_code` = '" . $code . "' 
                          WHERE customer_group_id = '" . (int)$customer_group_id . "'");

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        foreach ($languages as $language) {
            $language_id = $language['language_id'];
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_group_description SET customer_group_id = '" . (int)$customer_group_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($data['name_group']) . "', description = '" . $this->db->escape($data['description']) . "'");
        }

        return $customer_group_id;
    }

    /**
     * @param $customer_group_id
     * @param $data
     */
    public function editCustomerGroup($customer_group_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "customer_group 
		                    SET approval = 1, 
		                        sort_order = 1 
		                        WHERE customer_group_id = '" . (int)$customer_group_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_group_description 
		                    WHERE customer_group_id = '" . (int)$customer_group_id . "'");

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        foreach ($languages as $language) {
            $language_id = $language['language_id'];
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_group_description 
                                SET customer_group_id = '" . (int)$customer_group_id . "', 
                                language_id = '" . (int)$language_id . "', 
                                `name` = '" . $this->db->escape($data['name_group']) . "', 
                                description = '" . $this->db->escape($data['description']) . "'");
        }
    }

    /**
     * @param $customer_group_id
     */
    public function deleteCustomerGroup($customer_group_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_group WHERE customer_group_id = '" . (int)$customer_group_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_group_description WHERE customer_group_id = '" . (int)$customer_group_id . "'");
//		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE customer_group_id = '" . (int)$customer_group_id . "'");
//		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE customer_group_id = '" . (int)$customer_group_id . "'");
//		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE customer_group_id = '" . (int)$customer_group_id . "'");
//		$this->db->query("DELETE FROM " . DB_PREFIX . "tax_rate_to_customer_group WHERE customer_group_id = '" . (int)$customer_group_id . "'");
    }

    /**
     * @param $name
     * @return false|mixed
     */
    public function checkNameGroupExist($name) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_group_description` WHERE `name` = '" . $this->db->escape($name) . "'");
        if (isset($query->row['customer_group_id'])) {
            return $query->row['customer_group_id'];
        }

        return false;
    }

    public function getTotalCustomerGroups($data = array()) {
        $sql = "SELECT COUNT(DISTINCT cg.customer_group_id) AS total FROM " . DB_PREFIX . "customer_group cg 
		        LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) 
		        WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if(isset($data['user_create_id']) && $data['user_create_id'] !== '') {
            $sql .= ' AND cg.user_create_id=' . $data['user_create_id'];
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND cgd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'
                OR cg.customer_group_code LIKE '%" . $this->db->escape($data['filter_name']) . "%'
            ";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function addEmailSubscribers($email)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "email_subscribers SET email = '" . $this->db->escape($email) . "'");
        return true;
    }

    public function checkEmailSubcribersExist($email) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "email_subscribers` WHERE `email` = '" . $this->db->escape($email) . "'");
        if (isset($query->row['email'])) {
            return true;
        }
        return false;
    }
}
