<?php
class ModelSettingAppstoreSetting extends Model {
    public function getModule($module_id) {
        // Lấy ra thông tin module nếu: module status = 1 và app có status = 1

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "appstore_setting WHERE module_id = '" . (int)$module_id . "'");

        if ($query->row) {
            return json_decode($query->row['setting'], true);
        } else {
            return array();
        }
    }
    public  function getModuleCode($module_id){
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "appstore_setting` WHERE `module_id` = '" . (int)$module_id . "'");

        if ($query->row) {
            return $query->row['code'];
        } else {
            return "";
        }
    }
}