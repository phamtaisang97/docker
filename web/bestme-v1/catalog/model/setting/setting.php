<?php
class ModelSettingSetting extends Model {
	public function getSetting($code, $store_id = 0) {
		$data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "'");

		foreach ($query->rows as $result) {
			if (!$result['serialized']) {
				$data[$result['key']] = $result['value'];
			} else {
				$data[$result['key']] = json_decode($result['value'], true);
			}
		}

		return $data;
	}
	
	public function getSettingValue($key, $store_id = 0) {
		$query = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = '" . $this->db->escape($key) . "'");

		if ($query->num_rows) {
			return $query->row['value'];
		} else {
			return null;	
		}
	}

    /**
     * @param $code
     * @param $key
     * @param int $store_id
     * @return mixed|null
     */
	public function getSettingValueByCodeAndKey($code, $key, $store_id = 0) {
        $query = $this->db->query("SELECT `value`, `serialized` FROM " . DB_PREFIX . "setting WHERE `code` = '{$this->db->escape($code)}' AND store_id = '" . (int)$store_id . "' AND `key` = '" . $this->db->escape($key) . "'");

        if ($query->num_rows) {
            return $query->row['serialized'] ? json_decode($query->row['value'], true) : $query->row['value'];
        } else {
            return null;
        }
    }

    public function getSettingModuleValue($key, $store_id = 0) {
        $query = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = '" . $this->db->escape($key) . "'");

        if ($query->num_rows) {
            return $query->row['value'];
        } else {
            return 1;
        }
    }

    public function getSettingByKey($key, $store_id = 0){
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = '" . $this->db->escape($key) . "'");

        return $query->row;
    }

    public function editSettingValue($code = '', $key = '', $value = '', $store_id = 0)
    {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "' AND `key` = '" . $this->db->escape($key) . "'");

        if (!is_array($value)) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value, true)) . "', serialized = '1'");
        }
    }

    /**
     * Get shopname for common use
     *
     * @return mixed
     */
    public function getShopName()
    {
        $query = $this->db->query("SELECT `value` FROM `" . DB_PREFIX . "setting` WHERE `key`= 'shop_name'");
        return $query->row['value'];
    }

    /**
     * Get shop url for common use
     * @return mixed
     */
    public function getShopUrl()
    {
        // do not hardcode. TODO: config http or https
        return sprintf("https://%s%s", $this->getShopName(), SHOP_NAME_SUFFIX);
    }
}