jQueryBestme(function () {
    addAsset();
    var page = window.location.pathname;
    switch (page) {
        case "/" : page = "home_page";
            break;
        case "/san-pham" : page = "product";
            break;
        default : page = "default";
    }
    var data = {page: page}
    jQueryBestme.ajax({
        type: 'post',
        url: 'index.php?route=design/app_config_theme/getConfigApp',
        dataType: 'json',
        data: data,
        beforeSend: function () {

        },
        success: function (data) {
            if (data['error_code'] == 0) {
                var app_config = data['message'];
                for (var i = 0; i < app_config.length; i++) {
                    addAppToPage(app_config[i]);
                }
            }
        }
    });
});

function addAppToPage(config) {
    var url = 'index.php?route=extension/appstore/' + config['code'] + '&module_id=' + config['module_id'];
    jQueryBestme.ajax({
        url: url,
        dataType: 'html',
        success: function (htmlText) {
            jQueryBestme('.' + config['position']).append(htmlText);
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });
}

function addAsset(){
    var url = "index.php?route=design/app_config_theme/getAsset";
    jQueryBestme.ajax({
        url: url,
        dataType: 'json',
        success: function (data) {

            var asset = data['result'];

            for(var i = 0; i < asset.length; i++){
                var asset_app = asset[i];
                if(asset_app['css']){
                    for(var k = 0 ; k < asset_app['css'].length ; k++){
                        addStyleSheet(asset_app['css'][k]);
                    }
                }
                if(asset_app['js']){
                    for(var j = 0 ; j < asset_app['js'].length ; j++){
                        addJavascript(asset_app['js'][j]);

                    }
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });
}
function addJavascript(file){
    var script = document.createElement('script');
    script.src = file;
    script.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(script);
}
function addStyleSheet(file) {
    var element = document.createElement("link");
    element.setAttribute("rel", "stylesheet");
    element.setAttribute("type", "text/css");
    element.setAttribute("href", file);
    document.getElementsByTagName("head")[0].appendChild(element);
}