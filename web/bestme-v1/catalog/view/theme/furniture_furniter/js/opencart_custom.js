function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

// Cart add remove functions
var cart = {
    'add': function(product_id, quantity) {
        var valid = validateSerialize();
        if (!valid) {
            $('input[name="quantity"]')[0].reportValidity();
            return false;
        }
        if (jQuery('#add-to-cart button[type="submit"]').attr('data-status') == "disabled") {
            return false;
        }
        $.ajax({
            url: 'index.php?route=checkout/my_orders/addCart',
            type: 'post',
            data: $('#add-to-cart').serialize(),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('.count-cart').html(json['total_product_cart']);

                    $('#add_cart_notify').modal('show');

                    // ga4 add_to_cart event
                    if (json['ga4_add_to_cart']) {
                        dataLayer.push({ ecommerce: null });  // Clear the previous ecommerce object.
                        dataLayer.push(json['ga4_add_to_cart']);
                    }
                }

                ajaxGetOrderInfo();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'update': function(key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'addDeal': function () {
        $.ajax({
            url: 'index.php?route=checkout/my_orders/addCart',
            type: 'post',
            data: $('#add-on-deal-cart').serialize(),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('.count-cart').html(json['total_product_cart']);

                    $('#add_cart_notify').modal('show');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var voucher = {
    'add': function() {

    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var wishlist = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);

                $('html, body').animate({ scrollTop: 0 }, 'slow');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}

var compare = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#compare-total').html(json['total']);

                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
    e.preventDefault();

    $('#modal-agree').remove();

    var element = this;

    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function(data) {
            html  = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div>';
            html += '  </div>';
            html += '</div>';

            $('body').append(html);

            $('#modal-agree').modal('show');
        }
    });
});
/// update product detail when choose version
$(document).ready(function () {
    if (jQuery('#add-to-cart .form-check-input').length) {
        ajaxChangeVersion();
    }
    jQuery('#add-to-cart .form-check-input').on('click', function () {
        ajaxChangeVersion();
    });

    ajaxGetOrderInfo();

    $(document).on('click', '.dropdown-cart > a', function (e) {
        event.preventDefault();
        ajaxGetOrderInfo();
    });
});

function ajaxChangeVersion() {
    $.ajax({
        url: 'index.php?route=product/product/updateCartWhenChangeVersion',
        type: 'post',
        data: $('#add-to-cart').serialize(),
        dataType: 'json',
        beforeSend: function() {
            //TODO loading
        },
        complete: function() {
            //TODO loaded
        },
        success: function(json) {
            if (json['success']) {
                if (json.sale_on_out_of_stock == 1 && 1 == json.product_version.status){
                    jQuery('button.button_buying').attr('data-status', '');
                    jQuery('button.btn-addcart-bestme').attr('data-status', '');
                    jQuery('a.btn-buynow-bestme').attr('data-status', '');
                    jQuery('a.add-cart-bestme').attr('data-status', '');
                }
                else {
                    jQuery('button.button_buying').attr('data-status', 'disabled');
                    jQuery('button.btn-addcart-bestme').attr('data-status', 'disabled');
                    jQuery('a.btn-buynow-bestme').attr('data-status', 'disabled');
                    jQuery('a.add-cart-bestme').attr('data-status', 'disabled');
                }
                // ga4 view_item event
                if (json['ga4_view_item_detail']) {
                    dataLayer.push({ ecommerce: null });  // Clear the previous ecommerce object.
                    dataLayer.push(json['ga4_view_item_detail']);
                }

                updateHtmlVersion(json['sale_on_out_of_stock'], json['product_version'], json['deal_data'], json['add_on_products']);
            } else {
                jQuery('button.button_buying').attr('data-status', 'disabled');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}

function updateHtmlVersion(sale_on_out_of_stock, product, deal_data = [], add_on_products = []) {
    updateDealByVersion(product, deal_data, add_on_products);
    if (deal_data && deal_data.name) {
        $("#buy-now").attr('is-deal','1');
        $(".btn-addcart-bestme.btn-action").attr('is-deal','1');
    }
    else {
        $("#buy-now").attr('is-deal','0');
        $(".btn-addcart-bestme.btn-action").attr('is-deal','0');
    }
    // version
    if (product['version']) {
        // form mua tra gop
        var version = product['version'].replace(",", " - ")
        $('input#pAttribute').val(version)
    }

    //update product version
    if (product['version']) {
        if (parseInt(product['compare_price_format']) === 0) {
            jQuery('#buy-now .product-version').text(' - ' + product['version']);
            jQuery('#buy-now button').hide();
        } else {
            jQuery('#buy-now .product-version').text(' - ' + product['version'] + ' - ' + ((product['price'] == 0) ? product['compare_price_format'] : product['price_format']));
            jQuery('#buy-now button').show();
        }
    } else {
        jQuery('#buy-now .product-version').text(' - ' + ((product['price'] == 0) ? product['compare_price_format'] : product['price_format']));
    }

    //update price
    var compare_price = parseInt(product['compare_price_format']);
    if (product['price'] == 0 && !isEmpty(product['discount_id'])) {
        jQuery('.gia-san-pham').html('<div class="fw-bold fz-36 d-flex text-primary">' +
            '<span>'+ product['price_format'].replace('VND', 'đ') +'</span></div>' +
            '<p class="text-second fz-18 mb-3 old-price-js"><del>'+ product['compare_price_format'].replace('VND', 'đ') +'</del></p>'
            +'<div class="pro-sticky">\n' +
            '<span id="product-detail-percent-sale" class="sale-off fz-12"> </span>\n' +
            '</div>'
        );
        jQuery('.gia-san-pham-header').html('<span class="product-version-header">' + product['version'].replace('VND', 'đ') + ' - </span><span class="gia-header">'+ product['price_format'].replace('VND', 'đ') +'</span>');
    } else if (product['price'] == 0) {
        jQuery('.gia-san-pham').html('<span class="new-price">'+ (compare_price === 0
            ? '<div class="fw-bold fz-36 d-flex text-primary">' +
            '<a class="fw-bold fz-36 mb-3 d-block text-primary" data-bs-toggle="modal" data-bs-target="#contactPriceModal" href="#contactPriceModal">Liên hệ</a>' + '</div>' +
              '    <p class="text-second fz-18 mb-3 old-price-js" style="float:left;">' +
              '        <del></del>' +
              '    </p>' + '<div class="pro-sticky">\n' +
            '<span id="product-detail-percent-sale" class="sale-off fz-12"> </span>\n' +
            '</div>'
            : '<div class="fw-bold fz-36 d-flex text-primary">' + '<span>'+ product['compare_price_format'].replace('VND', 'đ') + '</span>')
        );
        jQuery('.gia-san-pham .old-price').html('');
        // jQuery('p.old-price-after').html('<del><span class="old-price fz-18 text-second mt-n3 mb-3 text-left">'+ product['compare_price_format'] +'</span></del>');
        // giá sản phẩm header
        jQuery('.gia-san-pham-header').html('<span class="product-version-header">' + product['version'] + ' </span><span class="ms-3 gia-header">'+ (compare_price === 0 ?
            '<a data-bs-toggle="modal" data-bs-target="#contactPriceModal" class="btn btn-buy contact-header" href="#contactPriceModal">\n'
            + '<span class="gia-header ms-3">' + 'Liên hệ' + '</span></a>'
            : '<span class="gia-header ms-3">' + product['compare_price_format'].replace('VND', 'đ') + '</span>'));
    } else {
        if (parseInt(product['price_format']) === 0) {
            jQuery('.gia-san-pham').html(comparePrice === 0
                ? '<a data-bs-toggle="modal" data-bs-target="#contactPriceModal" href="#contactPriceModal" class="btn btn-buy">\n'
                + '<div class="fw-bold fz-36 d-flex text-primary">' + '<span class="fw-bold fz-36 mb-3 d-block text-primary">Liên hệ</span>' + '</div></a>'
                : '<div class="fw-bold fz-36 d-flex text-primary">' + '<span>'+ product['compare_price_format'].replace('VND', 'đ') + '</span></div>'
            );
        } else {
            jQuery('.gia-san-pham').html('<div class="fw-bold fz-36 d-flex text-primary">' +
                '<span>'+ product['price_format'].replace('VND', 'đ') +'</span></div>' +
                '<p class="text-second fz-18 mb-3 old-price-js"><del>'+ product['compare_price_format'].replace('VND', 'đ') +'</del></p>'
                +'<div class="pro-sticky">\n' +
                '<span id="product-detail-percent-sale" class="sale-off fz-12"> </span>\n' +
                '</div>'
            );
            jQuery('.gia-san-pham-header').html('<span class="product-version-header">' + product['version'].replace('VND', 'đ') + ' - </span><span class="gia-header">'+ product['price_format'].replace('VND', 'đ') +'</span>');

        }
    }

    if (parseInt(product['compare_price_format']) === 0) {
        $('.pro-action').css('visibility','hidden');
        $('.button_buying').css('visibility','hidden');
        $('.add-cart-bestme').css('visibility','hidden');
    }
    else{
        $('.pro-action').css('visibility','visible');
        $('.button_buying').css('visibility','visible');
        $('.add-cart-bestme').css('visibility','visible');
    }
    //update status
    jQuery('.code-product').html(product['sku']);

    //update thương hiệu
    jQuery('.thuong-hieu').html(product['manufacturer']);

    //active button add cart
    //TODO : remove if check is_stock done
    // if (product['status'] == 1 && (sale_on_out_of_stock == 1 || (sale_on_out_of_stock == 0 && product['quantity'] > 0))) {
    if (product['status'] == 1 && (sale_on_out_of_stock == 1)) {
        jQuery('.tinh-trang').html('<span class="true"><i class="fa fa-check-square-o" aria-hidden="true"></i>Còn hàng</span>');
        jQuery('.button_buying').prop( "disabled", false );
        jQuery('.btn-addcart-bestme').prop( "disabled", false );
    } else {
        jQuery('.tinh-trang').html('<span class="false">Hết hàng</span>');
        jQuery('.button_buying').prop( "disabled", true );
        jQuery('.btn-addcart-bestme').prop( "disabled", true );
    }

    /* update product version image */
    // set default values
    product.image = product.image ? product.image : jQuery('#product-image-default-src').val();
    product.image_alt = product.image_alt ? product.image_alt : '';
    let swiperSlide2 = jQuery('.mySwiper2 .swiper-wrapper .swiper-slide.swiper-slide-active');
    const swiperSlide = jQuery('.mySwiper .swiper-wrapper .swiper-slide img');
    if (product.image) {
        swiperSlide2.html("<img src="+product.image+" class='lazyload' alt='empty image'>");
    }
    swiperSlide.click(function () {
        let image = $(this).attr('src');
        swiperSlide2.html("<img src="+image+" class='lazyload' alt='empty image'>");
    })

    // update percent sale
    if('' === product['percent_sale']) {
        if (product['price'] == 0 && !isEmpty(product['discount_id'])) {
            jQuery('#product-detail-percent-sale').removeClass('d-none').text('-100%');
        } else {
            jQuery('#product-detail-percent-sale').addClass('d-none').text(product['percent_sale']);
        }
    } else {
        jQuery('#product-detail-percent-sale').removeClass('d-none').text(product['percent_sale']);
    }
}

function updateDealByVersion(product, deal_data = [], add_on_products = []) {
    let html_name_deal = `<span class="lable">Mua kèm deal sốc:</span> <span class="btn">${ deal_data.name } 
        <img src="/catalog/view/theme/furniture_furniter/img/vuesax_outline_arrow-right.svg">
        </span>`;
    let product_name = $('h1.page-pro-title span').text();
    let html_product_deal = `
    <div class="col-12 col-lg-8 col-md-12 product-deal">
        <h3 class="title-deal-name" data-limit-quantity-product="${deal_data.limit_quantity_product}">
               <div>
                  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.4" d="M19.9707 10V18C19.9707 21 18.9707 22 15.9707 22H7.9707C4.9707 22 3.9707 21 3.9707 18V10H19.9707Z" fill="#EE3C96"/>
                    <path d="M21.5 7V8C21.5 9.1 20.97 10 19.5 10H4.5C2.97 10 2.5 9.1 2.5 8V7C2.5 5.9 2.97 5 4.5 5H19.5C20.97 5 21.5 5.9 21.5 7Z" fill="#EE3C96"/>
                    <path opacity="0.4" d="M11.6408 5.00141H6.12076C5.78076 4.63141 5.79076 4.06141 6.15076 3.70141L7.57076 2.28141C7.94076 1.91141 8.55076 1.91141 8.92076 2.28141L11.6408 5.00141Z" fill="#EE3C96"/>
                    <path opacity="0.4" d="M17.8696 5.00141H12.3496L15.0696 2.28141C15.4396 1.91141 16.0496 1.91141 16.4196 2.28141L17.8396 3.70141C18.1996 4.06141 18.2096 4.63141 17.8696 5.00141Z" fill="#EE3C96"/>
                    <path opacity="0.6" d="M8.93945 10V15.14C8.93945 15.94 9.81945 16.41 10.4895 15.98L11.4295 15.36C11.7695 15.14 12.1995 15.14 12.5295 15.36L13.4195 15.96C14.0795 16.4 14.9695 15.93 14.9695 15.13V10H8.93945Z" fill="#EE3C96"/>
                  </svg>
                  ${ deal_data.name }
               </div>
               <svg class="d-none-desktop" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5.93974 13.781C5.81307 13.781 5.68641 13.7343 5.58641 13.6343C5.39307 13.441 5.39307 13.121 5.58641 12.9277L9.93307 8.58099C10.2531 8.26099 10.2531 7.74099 9.93307 7.42099L5.58641 3.07432C5.39307 2.88099 5.39307 2.56099 5.58641 2.36766C5.77974 2.17432 6.09974 2.17432 6.29307 2.36766L10.6397 6.71432C10.9797 7.05432 11.1731 7.51432 11.1731 8.00099C11.1731 8.48766 10.9864 8.94766 10.6397 9.28766L6.29307 13.6343C6.19307 13.7277 6.06641 13.781 5.93974 13.781Z" fill="#263A7B"/>
               </svg>
        </h3>
          <div id="deal-view" class="d-none-desktop">
            <div class="main-pro">
                <img class="product-deal-image" width="104px" height="104px"
                     src="${ product.image }" alt="${ product.image_alt }">
                <p class="font-14 font-weight-600 mb-0 text-center price-deal">
                    ${ product.price == 0 ? product.compare_price_format.replace('VND', 'đ') : product.price_format.replace('VND', 'đ') }
                </p>
                <p class="text-secondary font-12 mb-8px text-center price-old-deal">
                    <span class="text-decoration-line-through">${ product.compare_price_format.replace('VND', 'đ') }</span>
                </p>
                <svg width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M7.87381 0.079999V12.488H5.16181V0.079999H7.87381ZM12.4578 5.024V7.544H0.553813V5.024H12.4578Z" fill="#17204D"/>
                </svg>
            </div>
          ${deal_pro(add_on_products)}
        </div>
        
        <div id="main-product">
            <div class="d-flex justify-space-between">
                <div class="image">
                    <p class="title">Sản phẩm</p>
                    <img class="product-deal-image" width="88px" height="88px"
                             src="${ product.image }" alt="${ product.image_alt }">
                </div>
                <div class="info-main-deal">
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="name-main-product">
                                <p>${ product_name }</p> <span>${product.version}</span>
                            </div>
                        </div>
                        <div class="col-md-4 text-xs-left text-center col-sm-12">
                            <p class="quantity-deal">Số lượng: </p>
                            <div class="custom-number-input-wrapper" style="background: none">
                                <button class="decrease">-</button>
                                <input data-original-price="${product.compare_price}" 
                                data-sale-price="${product.price}" class="deal custom-number-input" type="number" oninput="this.setCustomValidity('')"
                                       oninvalid="this.setCustomValidity('Bạn nhập sai giá trị')" required=""
                                       name="quantity" pattern="(([0-9]{9})\\b)" min="1" max="9999" step="1" value="1">
                                <button class="increase">+</button>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 text-xs-left text-right price-main-deal">
                            <p class="title d-none-mobile">Đơn giá</p>
                            <p class="text-secondary font-12 mb-8px">
                                <span class="text-decoration-line-through price-old-index-1" data-price-old-index-1="${product.compare_price}">${ product.compare_price_format.replace('VND', 'đ') }</span>
                                <span class="pro-sticky">
                                    <span class="sale-off fz-12">${ product.percent_sale ? product.percent_sale : '0 %' }</span>
                                </span>
                            </p>
                            <p class="font-14 font-weight-600 mb-0 price-index-1 add-on-price-sale" data-price-index-1="${product.price}">${ product.price == 0 ? product.compare_price_format.replace('VND', 'đ') : product.price_format.replace('VND', 'đ') }</p>
                        </div>
                    </div>
                </div>
            </div>

            <input type="hidden" name="product_id" value="${ product.product_id }">
            <input type="hidden" name="product_name_add_cart" value="${ product_name }">
            ${genderAttribute(product.product_attribute)}
        </div>
    `;

    let html_add_on_product = `
        <div id="add-on-product">
            `+ele_ao_product(add_on_products)+`
        </div>
        </div>
    `;

    let html_total = `
       <div class="col-12 col-lg-4 col-md-12" id="block-add-deal-to-cart">
            <div class="info">
                <p>Đã chọn <span class="checked-add-deal">1</span>/${ add_on_products.length + 1 } sản phẩm</p>
                <p>
                    <span>Tổng cộng</span>
                    <span id="total-original-price"
                          data-total-original-price="${ product.compare_price }"
                          class="ml-3 text-secondary text-decoration-line-through font-12 mb-8px">
                        ${ product.compare_price_format.replace( 'VND', 'đ' ) }
                    </span>
                    <br>
                    <span id="total-sale-price"
                          data-total-sale-price="${ product.price }"
                          class="ml-89px font-14 font-weight-600 mb-0 add-on-price-sale">
                        ${ product.price_format.replace( 'VND', 'đ' ) }
                    </span>
                </p>
                <p class="d-flex">
                    <span>Tiết kiệm</span> 
                    <span id="price-save-abc" data-price-save="${ product.compare_price-product.price }" class="price-save ml-3"></span>
                </p>
                
                <p class="show-message-limit-quantity"></p>
                
                <button id="add-deal-to-cart"
                        type="button"
                        class="btn text-primary d-flex justify-content-center p-2 h-100 ms-lg-5"
                        onclick="cart.addDeal('')">
                        <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M9.625 5.03402C9.38583 5.03402 9.1875 4.83568 9.1875 4.59652V3.79152C9.1875 3.17902 8.925 2.58402 8.47 2.16985C8.00917 1.74985 7.41417 1.55735 6.78417 1.61568C5.73417 1.71485 4.8125 2.78818 4.8125 3.90818V4.47402C4.8125 4.71318 4.61417 4.91152 4.375 4.91152C4.13583 4.91152 3.9375 4.71318 3.9375 4.47402V3.90235C3.9375 2.33318 5.20333 0.886516 6.7025 0.740682C7.5775 0.659016 8.4175 0.933182 9.05917 1.52235C9.695 2.09985 10.0625 2.92818 10.0625 3.79152V4.59652C10.0625 4.83568 9.86417 5.03402 9.625 5.03402Z" fill="white"/>
                            <path d="M8.75087 13.2721H5.25087C2.55587 13.2721 2.0542 12.018 1.92587 10.7988L1.48837 7.30464C1.4242 6.67464 1.40087 5.77047 2.01337 5.0938C2.53837 4.51047 3.40754 4.23047 4.66754 4.23047H9.3342C10.6 4.23047 11.4692 4.5163 11.9884 5.0938C12.595 5.77047 12.5775 6.67464 12.5134 7.29297L12.0759 10.7988C11.9475 12.018 11.4459 13.2721 8.75087 13.2721ZM4.66754 5.10547C3.6817 5.10547 3.00504 5.29797 2.66087 5.68297C2.37504 5.99797 2.2817 6.48214 2.35754 7.20547L2.79504 10.6996C2.8942 11.633 3.15087 12.403 5.25087 12.403H8.75087C10.8509 12.403 11.1075 11.6388 11.2067 10.7113L11.6442 7.20547C11.72 6.4938 11.6267 6.00964 11.3409 5.6888C10.9967 5.29797 10.32 5.10547 9.3342 5.10547H4.66754Z" fill="white"/>
                            <path d="M8.99542 7.67057C8.66875 7.67057 8.40625 7.40807 8.40625 7.08724C8.40625 6.76641 8.66875 6.50391 8.98958 6.50391C9.31042 6.50391 9.57292 6.76641 9.57292 7.08724C9.57292 7.40807 9.31625 7.67057 8.99542 7.67057Z" fill="white"/>
                            <path d="M4.91143 7.67057C4.58477 7.67057 4.32227 7.40807 4.32227 7.08724C4.32227 6.76641 4.58477 6.50391 4.9056 6.50391C5.22643 6.50391 5.48893 6.76641 5.48893 7.08724C5.48893 7.40807 5.23227 7.67057 4.91143 7.67057Z" fill="#EE3C96"/>
                        </svg>
                    <span class="fz-12 fz-lg-16 align-self-center">Thêm Deal vào giỏ</span>
                </button>
            </div>
        </div>
    `
    let html = `
        <div id="add-on-deal" class="for-version" data-is-load="${product.product_version_id}">
            <form action="#" method="POST" id="add-on-deal-cart">
                <div class="row">
                    ${html_product_deal}
                    ${html_add_on_product}
                    ${html_total}
                </div>
            </form>
        </div>
    `;

    if (!isEmpty(deal_data)) {
        $("#deal-name").html(html_name_deal);
        $('.section-add-on-deal .row-form').html(html);
        initMainProductPrice();
        updateWhenCheckBoxDeal();
        actionChangeQuantity();
        $('.section-add-on-deal').css("display","block");
    }
    else {
        $("#deal-name").html('');
        $('.section-add-on-deal .row-form').html('');
        $('.section-add-on-deal').css("display","none");
    }
}

function ele_ao_product(add_on_products) {
    if (add_on_products) {
        let html = ``;
        let icon = `
             <svg style="position: absolute;margin: 8px 4px;" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path opacity="0.4" d="M13.3151 6.66663V12C13.3151 14 12.6484 14.6666 10.6484 14.6666H5.3151C3.3151 14.6666 2.64844 14 2.64844 12V6.66663H13.3151Z" fill="#EE3C96"/>
                <path d="M14.3346 4.66671V5.33337C14.3346 6.06671 13.9813 6.66671 13.0013 6.66671H3.0013C1.9813 6.66671 1.66797 6.06671 1.66797 5.33337V4.66671C1.66797 3.93337 1.9813 3.33337 3.0013 3.33337H13.0013C13.9813 3.33337 14.3346 3.93337 14.3346 4.66671Z" fill="#EE3C96"/>
                <path opacity="0.4" d="M7.75921 3.33329H4.07921C3.85254 3.08663 3.85921 2.70663 4.09921 2.46663L5.04587 1.51996C5.29254 1.27329 5.69921 1.27329 5.94587 1.51996L7.75921 3.33329Z" fill="#EE3C96"/>
                <path opacity="0.4" d="M11.9144 3.33329H8.23438L10.0477 1.51996C10.2944 1.27329 10.701 1.27329 10.9477 1.51996L11.8944 2.46663C12.1344 2.70663 12.141 3.08663 11.9144 3.33329Z" fill="#EE3C96"/>
                <path opacity="0.6" d="M5.96094 6.66663V10.0933C5.96094 10.6266 6.5476 10.94 6.99427 10.6533L7.62094 10.24C7.8476 10.0933 8.13427 10.0933 8.35427 10.24L8.9476 10.64C9.3876 10.9333 9.98094 10.62 9.98094 10.0866V6.66663H5.96094Z" fill="#EE3C96"/>
            </svg>
        `;
        for (let i = 0; i < add_on_products.length; i++) {
            html = html + `
            <label for="flexCheckDefault${ add_on_products[i].id }">
                 <div class="row mb-3 row-add-on-product ${ add_on_products[i].is_stock ? '' : 'not-is-stock' }">
                    <div class="col-md-7">
                        <div class="d-flex w-100">
                            <div class="form-check my-auto">
                                <input ${ add_on_products[i].is_stock ? '' : 'disabled' } 
                                       class="" type="checkbox" name="chooseAddOnProduct[]"
                                       value="${ add_on_products[i].id }"
                                       data-original-price="${ add_on_products[i].original_price }"
                                       data-sale-price="${ add_on_products[i].sale_price }"
                                       ${add_on_products[i].checked ? 'checked' : ''}
                                       id="flexCheckDefault${ add_on_products[i].id }">
                            </div>
                            <div class="d-flex ">
                                ${ add_on_products[i].sale_price == 0 ? icon : '' }
                                <img class="product-deal-image" width="88px" height="88px"
                                     src="${ add_on_products[i].image }"
                                     alt="1">
            
                                <div class="w-75">
                                    <label class="product-deal-notice">
                                        ${ add_on_products[i].sale_price == 0 ? 'Quà tặng' : 'Deal sốc' }
                                    </label>
                                    <p class="mb-0 product-name-deal">${ add_on_products[i].product_name }</p>
                                    <p class="font-weight-600 mb-0 version-deal">${add_on_products[i].version}</p>
                                     <span class="is-stock-deal" ${ add_on_products[i].is_stock ? 'style="display:none"' : '' }>
                                        ${ add_on_products[i].is_stock ? '' : 'Hết hàng' }
                                     </span>
                                    
                                    <div class="d-flex justify-space-between">
                                        <div class="d-none-desktop col-md-3 text-right price-on-deal">
                                            <p class="text-secondary font-12 mb-8px">
                                                <span class=" text-decoration-line-through">
                                                    ${ add_on_products[i].original_price_format.replace('VND', 'đ') }</span>
                                                    <span class="sale-off">${ '-' + add_on_products[i].percent_reduce + '%' }</span>
                                            </p>
                                            <p class="font-14 font-weight-600 mb-0 add-on-price-sale">
                                                ${ add_on_products[i].sale_price_format.replace( 'VND', 'đ' ) }</p>
                                        </div>
                                        
                                        <div class="d-none-desktop quantity-deal">
                                            <span>SL: </span> 
                                            <span>1</span>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="d-none-mobile col-md-2 text-center">
                        <span>1</span>
                    </div>
                    
                    <div class="col-md-3 text-right price-on-deal d-none-mobile">
                        <p class="text-secondary font-12 mb-8px">
                            <span class=" text-decoration-line-through">
                                ${ add_on_products[i].original_price_format.replace('VND', 'đ') }</span>
                                <span class="sale-off">${ '-' + add_on_products[i].percent_reduce + '%' }</span>
                        </p>
                        <p class="font-14 font-weight-600 mb-0 add-on-price-sale">
                            ${ add_on_products[i].sale_price_format.replace( 'VND', 'đ' ) }</p>
                    </div>
                </div>
                 <input type="hidden" name="">
             </label>
            `
        }
        return html;
    }
}

function deal_pro(add_on_products) {
    if (add_on_products) {
        let html = ``;
        const maxProductsLenght = Math.min(add_on_products.length, 2);
        for (let i = 0; i < maxProductsLenght; i++) {
            html = html + `
                 <div class="deal-pro">
                    <img src="${ add_on_products[i] && add_on_products[i].image ? add_on_products[i].image : '' }" alt="" width="104px">
                        <p class="font-14 font-weight-600 mb-0 text-center price-deal">
                            ${ add_on_products[i] && add_on_products[i].sale_price_format.replace( 'VND', 'đ' ) }
                        </p>
                        <p class="text-secondary font-12 mb-8px text-center price-old-deal">
                            <span class="text-decoration-line-through">
                                ${ add_on_products[i] && add_on_products[i].original_price_format.replace('VND', 'đ') }
                            </span>
                        </p>
                </div>
            `
        }
        return html;
    }
}

function genderAttribute(attributes) {
    let html = "";
    if (!isEmpty(attributes)) {
        for (const [key, value] of Object.entries(attributes)) {
            html = html +
                `<input type="hidden" name="attribute[${key}]" value="${ value }">`
        }
    }
    return html;
}

var checked = [];
var mainProductOriginalPrice = 0;
var mainProductSalePrice = 0;
var mainProductQuantity = 1;
var countChecked = 1;
var limitQuantityProduct = 0;

actionChangeQuantity();
initMainProductPrice();
updateWhenCheckBoxDeal();

function initMainProductPrice() {
    let quantity = parseInt($(".deal.custom-number-input").val());
    let original_price = parseInt($('.deal.custom-number-input').data('original-price'));
    let sale_price = parseInt($('.deal.custom-number-input').data('sale-price'));
    mainProductOriginalPrice = original_price ? original_price : 0;
    mainProductSalePrice = sale_price ? sale_price : 0;
    mainProductQuantity = quantity ? quantity : 1;

    checked = [];
    if ($('h3.title-deal-name').data('limit-quantity-product')) {
        limitQuantityProduct = $('h3.title-deal-name').data('limit-quantity-product');
    }

    $('#add-on-product input[name="chooseAddOnProduct[]"').each(function (i) {
        if (i < limitQuantityProduct) {
            $(this).prop( "checked", true );
        }
        if (this.checked) {
            checked.push({
                "id": $(this).val(),
                "original_price": $(this).data('original-price'),
                "sale_price": $(this).data('sale-price'),
                "save_price": parseInt($(this).data('original-price')) - parseInt($(this).data('sale-price'))
            })
        }
    })
    if (checked.length > 0) {
        countChecked = checked.length + 1;
    }

    $(".checked-add-deal").text(countChecked);
    calAllPrice();

    jQuery('#deal-name .btn').on('click', function () {
        $('html,body').animate({
                scrollTop: $(".section-add-on-deal").offset().top - 80},
            'slow');
    });

    jQuery('#deal-view').on('click', function () {
        $(this).css("display", "none");
        $("#main-product").addClass("show");
        $("#add-on-product").addClass("show");
    });

    jQuery('.section-add-on-deal').on('click', function () {
        $("#main-product").addClass("show");
        $("#add-on-product").addClass("show");
        $("#deal-view").css("display", "none");
    });

    if ((countChecked - 1) == limitQuantityProduct) {
        $("input[type='checkbox']:not(:checked)").prop( "disabled", true );
        // $(".show-message-limit-quantity").text("Bạn chỉ được chọn tối đa "+limitQuantityProduct+" sản phẩm mua kèm");
        $("input[type='checkbox']:not(:checked)").parents('.row-add-on-product').addClass('not-is-stock');
    }
    else {
        $("input[type='checkbox']:not(:checked)").prop( "disabled", false );
        $(".show-message-limit-quantity").text("");
        $(".row-add-on-product").removeClass('not-is-stock');
    }
}
function updateWhenCheckBoxDeal() {
    let checkbox = countChecked;
    $('#add-on-product').on('click', 'input[name="chooseAddOnProduct[]"]', function () {
        if (this.checked) {
            checked.push({
                "id": $(this).val(),
                "original_price": $(this).data('original-price'),
                "sale_price": $(this).data('sale-price'),
                "save_price": parseInt($(this).data('original-price')) - parseInt($(this).data('sale-price'))
            })
            checkbox++;
        } else {
            let index = checked.findIndex(({id}) => id == $(this).val());
            checked.splice(index, 1);
            checkbox--;
        }
        $(".checked-add-deal").text(checkbox);
        if (checkbox === 1) {
            $("#add-deal-to-cart").prop( "disabled", true );
        }
        else {
            $("#add-deal-to-cart").prop( "disabled", false );
        }

        if ((checkbox - 1) == limitQuantityProduct) {
            $("input[type='checkbox']:not(:checked)").prop( "disabled", true );
            // $(".show-message-limit-quantity").text("Bạn chỉ được chọn tối đa "+limitQuantityProduct+" sản phẩm mua kèm");
            $("input[type='checkbox']:not(:checked)").parents('.row-add-on-product').addClass('not-is-stock');
        }
        else {
            $("input[type='checkbox']:not(:checked)").prop( "disabled", false );
            $(".show-message-limit-quantity").text("");
            $(".row-add-on-product").removeClass('not-is-stock');
        }
        calAllPrice();
      });
}

function number_format(number, decimals, dec_point, thousands_sep) {
    decimals = parseFloat(decimals);
    var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
    var d = dec_point == undefined ? "," : dec_point;
    var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
    var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function actionChangeQuantity() {
    let price_save = parseInt($(".section-add-on-deal #price-save").data('price-save'));
    $("#price-save").text(number_format(price_save, 0, ',', '.') + ' đ');
    $('#add-on-deal').on('change', '.deal.custom-number-input', function () {
        let quantity = parseInt($(this).val());
        let original_price = parseInt($(this).data('original-price'));
        let sale_price =  parseInt($(this).data('sale-price'));

        mainProductOriginalPrice = original_price;
        mainProductSalePrice = sale_price;
        mainProductQuantity = quantity;

        calAllPrice();
    });
}

function displayTotalSavePrice(total_save_price) {
    $(".section-add-on-deal #price-save-abc").text(number_format(total_save_price, 0, ',', '.') + ' đ');
}

function calculateTotalCheckBox(checked = []) {
    let totalOriginalPrice = 0;
    let totalSalePrice = 0;
    let totalSavePrice = 0;
    for (let i = 0; i < checked.length; i++) {
        totalOriginalPrice += checked[i].original_price;
        totalSalePrice += checked[i].sale_price;
        totalSavePrice += checked[i].save_price;
    }

    return {
        'total_original_price': totalOriginalPrice,
        'total_sale_price': totalSalePrice,
        'total_save_price': totalSavePrice,
    }
}

function calAllPrice() {
    let dataPriceCheckBox = calculateTotalCheckBox(checked);
    let totalOriginalPriceCheckBox = dataPriceCheckBox.total_original_price;
    let totalSalePriceCheckBox = dataPriceCheckBox.total_sale_price;
    let totalSavePriceCheckBox = dataPriceCheckBox.total_save_price;
    let total_original_price = 0;
    let total_sale_price = 0;
    let total_save_price = 0;

    if (mainProductSalePrice === 0) {
        total_original_price = mainProductOriginalPrice * mainProductQuantity + totalOriginalPriceCheckBox;
        total_sale_price = mainProductOriginalPrice * mainProductQuantity + totalSalePriceCheckBox;
        total_save_price = totalSavePriceCheckBox;
    } else {
        total_original_price = mainProductOriginalPrice * mainProductQuantity + totalOriginalPriceCheckBox;
        total_sale_price = mainProductSalePrice * mainProductQuantity + totalSalePriceCheckBox;
        total_save_price = total_original_price - total_sale_price;
    }

    $("#total-original-price").text(number_format(total_original_price, 0, ',', '.') + ' đ');
    $("#total-sale-price").text(number_format(total_sale_price, 0, ',', '.') + ' đ');
    displayTotalSavePrice(total_save_price);
}

function validateSerialize() {
    var invalidFields = jQuery('#add-to-cart').find( ":invalid" );
    if (invalidFields.length > 0) return false;
    return true;
}

function ajaxGetOrderInfo() {
    $.ajax({
        url: 'index.php?route=checkout/my_orders/ajaxGetOrderInfo',
        type: 'get',
        data: $('#add-to-cart').serialize(),
        dataType: 'json',
        beforeSend: function() {
            //TODO loading
        },
        complete: function() {
            //TODO loaded
        },
        success: function(data) {
            //TODO: why not use json parse
            if (data && data.order_products && data.order && data.order_products.length) {
                let orderProductListHtml = '';
                $.each(data.order_products, function (index, product) {
                    let versionInfo = '';
                    let isPriceDisplayNone = 'd-block';
                    if (product.attributes && Object.keys(product.attributes).length > 0) {
                        $.each(product.attributes, function (attrName, attrValue) {
                            versionInfo = versionInfo +
                                '    <p>\n' +
                                '        <span>' + attrName + ':</span>\n' +
                                '        <span>' + attrValue + '</span>\n' +
                                '    </p>\n';
                        });
                    }

                    if (product.price === product.price_compare) {
                        isPriceDisplayNone = 'd-none';
                    }

                    orderProductListHtml += `<div class="cart-item align-items-center mb-3 d-flex">
                                                <a href="${ product.url }" class="me-3">
                                                    <img src="${ product.image_url }" width="60px" height="60px">
                                                </a>
                                                <div class="fz-12 flex-fill pl-3">
                                                    <a href="${ product.url }" class="text-dark fz-14 mb-1 d-block fw-bold">
                                                        ${ product.name }
                                                        <span class="text-fourth d-block fw-400 fz-12">${ product.attribute }</span>
                                                    </a>
                                                    <span class="d-block mb-1">Số lượng: ${ product.amount }</span>
                                                    <span class="fw-500 text-line-through ${ isPriceDisplayNone }">${ product.price_compare_formart }</span>
                                                    <strong>${ product.price_formart }</strong>
                                                </div>
                                            </div>`;
                });

                /* update order product list */
                $('.dropdown-cart .cart-item-list').html(orderProductListHtml);

                /* update total amount */
                const totalAmountHtml = (data && data.order && data.order.into_money_formart) ? data.order.into_money_formart : '0đ';

                /* update total amount in cart */
                $('.dropdown-cart .cart-total .total-price').text(totalAmountHtml);

                /* update total products in cart */
                $('.dropdown-cart .count-cart').text(data.order.amount > 99 ? '99+' : data.order.amount);
            } else {
                const htmlCartEmpty = '';

                /* update order empty */
                $('.dropdown-cart .cart-item-list').html(htmlCartEmpty);
                $('.dropdown-cart .cart-total .total-price').text('0');
                $('.dropdown-cart .count-cart').text('0');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}