// Product Carousel
function owlInit(elements, numItem, nav = true, dots = false, margin = 0, responsive = null, loop = true, resizeHotProductDiv = false) {
    if (elements.length) {
        elements.each(function () {
            const element = $(this);
            const interval = element.data('interval');
            const autoplayTime = undefined === element.data('autoplay_time') ? interval : element.data('autoplay_time') * 1e3

            if (resizeHotProductDiv) {
                element.on("initialized.owl.carousel", function (e) {
                    /* resize hot product div */
                    // get container div
                    const containers = element.find('.slide-item > .row');
                    containers.each(function (index) {
                        // get col height
                        const colHeight = $(this).find(' > div:last-child > div > div:first-child').height();
                        if (!isNaN(colHeight)) {
                            // set first hot product height
                            $(this).find(' > div:first-child > div:first-child').height(colHeight * 2);
                        }
                    });
                });
            }

            element.owlCarousel({
                items: numItem,
                autoplay: undefined === element.data('autoplay') ? typeof interval !== 'undefined' : 1 == element.data('autoplay'),
                autoplayTimeout: autoplayTime,
                loop: undefined === element.data('loop') ? loop : 1 == element.data('loop'),
                responsiveClass: true,
                lazyLoad: true,
                dots: dots,
                nav: nav,
                margin: margin,
                navText: ['<i class="icon icon-arrow-left"></i>', '<i class="icon icon-arrow-right"></i>'],
                responsive: false
            });
        });
    }
}

function isEmpty(variable) {
    return undefined === variable || '' === variable || (Array.isArray(variable) && 0 === variable.length) || false === variable || null === variable;
}

$(document).ready(function () {

    owlInit($('.owl-product').not('.owl-collection-product'), 1, true);

    /*Set an element Full width*/
    const fullWidtElm = '.full-width';

    function sectionFullWidth() {
        let bodyWidth = $('body').width();
        let originWidth = $('.container').width();
        $(fullWidtElm).css({
            'width': bodyWidth + 'px',
            'max-width': bodyWidth + 'px',
            'margin-left': -(bodyWidth - originWidth) / 2 + 'px'
        });
    }

    sectionFullWidth();
    $(window).resize(function (e) {
        sectionFullWidth();
    });
    /*End Set an element Full width*/

    // Check star rate by user
    var rate = 0;
    $('.user-star-rate input[type="radio"]').change(function () {
        rate = $(this).val();
        $(".form-rate form").find('.btn[type="submit"]').attr('data-original-title', '');
    });

    $(".form-rate form").submit(function (event) {
        if (rate === 0) {
            event.preventDefault();
            $(this).find('.btn[type="submit"]').tooltip('show');
            return;
        }
    });

    // Select dropdown when click dropdown item
    $('#dropdown-select').on('click', '.dropdown-item', function (event) {
        event.preventDefault();
        var key_txt = $(this).text();
        $(this).parents('#dropdown-select').find('button').html('').append(key_txt);
    });

    // input quantity calculate
    $('input[type="number"].custom-number-input').each(function (index, el) {
        var wrapper = document.createElement("div");
        wrapper.classList = "custom-number-input-wrapper";

        var increase = document.createElement("button");
        increase.innerHTML = '+';
        increase.classList = "increase";
        var decrease = document.createElement("button");
        decrease.innerHTML = '-';
        decrease.classList = "decrease";

        $(wrapper).append(decrease);
        $(wrapper).append($(this).clone());
        $(wrapper).append(increase);
        $(this).replaceWith($(wrapper));
    });

    $(document).on('click', '.custom-number-input-wrapper >button', function (e) {
        e.preventDefault();
        var input = $(this).parent().find('input');
        var input_val = (input.val()) ? parseInt(input.val()) : 0;
        var min = input.attr('min');
        var max = input.attr('max');
        var step = typeof input.attr('step') !== "undefined" ? input.attr('step') : 1;
        if ($(this).hasClass('increase')) {
            input_val = input_val + 1;
        } else {
            input_val = input_val - step;
        }
        if (input_val < min && typeof min !== "undefined") input_val = min;
        if (input_val > max && typeof max !== "undefined") input_val = max;
        input.val(input_val).change();
    })


    // Show/Hide password
    $(document).on('click', '.input-password-group button.show-password', function (e) {
        var input = $(this).parent().find('>input');
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            $(this).find('i').removeClass('icon-eye').addClass('icon-no-eye');
            input.attr('type', 'text');
        } else {
            $(this).find('i').removeClass('icon-no-eye').addClass('icon-eye');
            input.attr('type', 'password');
        }
    });

    // Close menu mobile
    if ($('#navbarMainMenu').length > 0) {
        $('#navbarMainMenu').parent().append('<div id="overlay"></div>');
        $('button.navbar-toggler').click(function () {
            $('#overlay').toggleClass('show');
        });

        $('#navbarMainMenu i.close').click(function () {
            $('#overlay').removeClass('show');
            $(this).parent('#navbarMainMenu').removeClass('show');
        });

        $(window).click(function (e) {
            if ($(e.target).attr('id') == 'overlay' && $('#navbarMainMenu').hasClass('show') && $(e.target).parents('#navbarMainMenu').length == 0) {
                $('#overlay').removeClass('show');
                $('#navbarMainMenu').removeClass('show');
            }
        });
    }

    // Menu responsive
    $('ul.nav > li > a>.toggle').on('click', function (e) {
        $(this).closest('li').toggleClass('active');
        e.preventDefault();
        return false;
    });

    // Fix content block
    const contentFixBlock = '.scroll-fix';
    const blockOffsetTop = $(contentFixBlock).length ? $(contentFixBlock).offset().top : 0;
    function fixContentBlock(disableMobile = true) {
        if($(contentFixBlock).length === 0) return;
        if($(window).width() <= 991 && disableMobile) {
            $(contentFixBlock).removeClass('fixed').css('top', 'auto');
            return;
        }
        let windowTop = (window.pageYOffset || document.scrollTop) - (document.clientTop || 0);
        if(windowTop <= blockOffsetTop) {
            $(contentFixBlock).removeClass('fixed').css('top', 'auto');
        }
        if(windowTop >= blockOffsetTop && $('header.fixed .fix-content').length) {
            let menuHeight = $('header.fixed .fix-content').outerHeight();
            $(contentFixBlock).addClass('fixed').css('top', menuHeight + 'px');
        }
    }
    $(document).resize(function (e) {
        fixContentBlock();
    })
    //////////// End Fix content block ///////////
    // Header menu fix scroll
    let topHeaderHeight = 500;
    let headerHeight = $('header.header').outerHeight();
    $(document).scroll(function (e) {
        var windowScrollTop = (window.pageYOffset || document.scrollTop) - (document.clientTop || 0);
        if (windowScrollTop >= 100 && !$('.header-mobile').hasClass('fixed')) {
            $('.header-mobile').addClass('fixed');
        } else if (windowScrollTop <= 100 && $('.header-mobile').hasClass('fixed')) {
            $('.header-mobile').removeClass('fixed');
        }

        if ($('.block-header').length == 0 | $('body').height() < $(window).height() * 1.5) return;
        var windowScrollTop = (window.pageYOffset || document.scrollTop) - (document.clientTop || 0);

        if (windowScrollTop >= topHeaderHeight && !$('.block-header').hasClass('fixed')) {
            $('.block-header').addClass('fixed');
            $('.block-header').css('min-height', headerHeight + "px");
        } else if (windowScrollTop <= topHeaderHeight && $('.block-header').hasClass('fixed')) {
            $('.block-header').removeClass('fixed');
            $('.block-header').css('min-height', "auto");
        }

        // Box buy-product in product detail page
        if ($('.product-teaser-fix').length > 0) {
            let menuHeight = $('header.fixed .fix-content').length ? $('header.fixed .fix-content').outerHeight() : 98;
            if ($('.block-header').hasClass('fixed') && windowScrollTop >= topHeaderHeight + 700) {
                $('.product-teaser-fix').slideDown(300).css('top', menuHeight + "px");
            } else {
                $('.product-teaser-fix').slideUp(300, function() {
                    $(this).css('top', "auto");
                });
            }
        }

        // back to top button
        let bodyHeight = $('body').height() - window.innerHeight * 2 - $('footer#footer').height();
        if (windowScrollTop > window.innerHeight * 1.5 && bodyHeight > 0) {
            $('a.back-to-top').fadeIn(500);
        } else {
            $('a.back-to-top').fadeOut(500);
        }
    });

    $('a.back-to-top').click(function () {
        $('html,body').animate({scrollTop: 0}, 200);
        return false;
    });

    // Address setting page collapse
    $('.address-setting-panel .left a[data-toggle="collapse"]').on('click', function () {
        $('.form-collapse .collapse.show').collapse('hide');
    })

    // // Menu responsive
    // $(document).on('click', '.block-header .main-menu ul.nav > li.has-child > a>.toggle', function (e) {
    //     $(this).closest('li').toggleClass('active');
    //     e.preventDefault();
    //     return false;
    // });
    // $(document).on('click', '.block-header .main-menu ul.nav > li.has-child > .toggle', function (e) {
    //     $(this).closest('li').toggleClass('active');
    //     e.preventDefault();
    //     return false;
    // });
    $(document).on('click', 'header.header-mobile li.nav-item.has-child span.toggle', function (e) {
        $(this).closest('li').toggleClass('active');
        e.preventDefault();
        return false;
    });

    // add children class to category menu
    const is_on_mobile = $('.cat-menu-in-body').data('is_on_mobile');
    const categories = document.querySelectorAll('.cat-menu-in-body .nav-item');

    for(let i = 0; i < categories.length; i++) {
        const ul_child = categories[i].lastElementChild;
        if(1 === is_on_mobile) {
            if('UL' === ul_child.tagName) {
                // category has ul childrens
                if('' === ul_child.innerHTML.trim()) {
                    // remove ul_child
                    categories[i].removeChild(ul_child);
                } else {
                    // append class to category element (li)
                    categories[i].classList.add('has-child');

                    // append toggle element
                    const a_child = categories[i].firstElementChild;
                    if('A' === a_child.tagName) {
                        const spanElement = document.createElement('span');
                        spanElement.classList.add('toggle');
                        categories[i].insertBefore(spanElement, categories[i].lastElementChild);
                    }
                }
            }
        } else {
            if('UL' === ul_child.tagName) {
                // category has ul childrens
                const div_grandchild = ul_child.lastElementChild;

                if('' === div_grandchild.innerHTML.trim()) {
                    // remove ul_child
                    categories[i].removeChild(ul_child);
                } else {
                    // append class to category element (li)
                    categories[i].classList.add('has-child');

                    // append toggle element
                    const a_child = categories[i].firstElementChild;

                    if('A' === a_child.tagName) {
                        a_child.insertAdjacentHTML('beforeend', '<span class="toggle">');
                    }
                }
            }
        }
    }
});