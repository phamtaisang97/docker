$(document).ready(function() {
    
    const tab_product_detail = $('#product-detail-page .product-detail #myTabContent .tab-pane .tab-content-description');
    // const producdetailtpage_add_to_cart_action_checkout = $(".add-to-cart-action-checkout");
    const footer_logo = $("footer .logo-footer");
    // const productpage_dropdown_filter_price_product = $("#product-page #dropdown-filter-price-product");
    // const homepage_collections = $("#homepage-collections");
    
    if(footer_logo.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_common/logo',
            success: function(response) {
                var image_logo = footer_logo.find(".image");
                image_logo.attr("style", "background-image: url(" + response.logo + ")");
            }
        });
    }
    
    // if(tab_product_detail.length > 0) {
    //     var collapse_tab_product_detail = $('#product-detail-page .product-detail #myTabContent .dv-collapse .btn-collapse');
    //     var getheight_tab_product_detail = tab_product_detail.height();
        
    //     if (getheight_tab_product_detail > 215) {
    //         tab_product_detail.addClass("hide");
    //     } else {
    //         collapse_tab_product_detail.addClass("hide");
    //     }
        
    //     collapse_tab_product_detail.on('click', function() {
    //         if (tab_product_detail.hasClass("hide")) {
    //             tab_product_detail.removeClass("hide");
    //             collapse_tab_product_detail.addClass("hide");
    //         }
    //     });
    // }
    
    // if(producdetailtpage_add_to_cart_action_checkout.length > 0) {
    //     var amountcart_header = $('header#header-main #header-top-contact span.quantity');
    //     var amountproduct = $('#product-detail-page form#add-to-cart #input-amount-add-to-cart');
        
    //     producdetailtpage_add_to_cart_action_checkout.on('click', function() {
    //         var get_amountcart_header = amountcart_header.html();
    //         var get_amountproduct = amountproduct.val();
            
    //         var new_value_amount_cart = parseInt(get_amountcart_header) + parseInt(get_amountproduct);
            
    //         amountcart_header.html(new_value_amount_cart);
    //     });
    // }
    
    // if(productpage_dropdown_filter_price_product.length > 0) {
    //     var item_product_price_filter = productpage_dropdown_filter_price_product.find('.dropdown-menu .dropdown-item');
    //     item_product_price_filter.on('click', function() {
    //         var datafrom = $(this).attr('data-from');
    //         var datato = $(this).attr('data-to');
    //         var get_href = window.location.href.split("/");
    //         var lasthref_origin = get_href[get_href.length - 1].split("?");
    //         var array_lasthref = lasthref_origin[lasthref_origin.length - 1].split("&");
            
    //         // xóa min
    //         array_lasthref.forEach(function(item, index) {
    //             if (item.search('min') != -1) {
    //                 array_lasthref.splice(index, 1);
    //             }
                
    //             if (item.search('max') != -1) {
    //                 array_lasthref.splice(index, 1);
    //             }
    //         });
    //         // xóa max
    //         array_lasthref.forEach(function(item, index) {
    //             if (item.search('max') != -1) {
    //                 array_lasthref.splice(index, 1);
    //             }
    //         });
    //         lasthref_origin[lasthref_origin.length - 1] = array_lasthref.join('');
    //         get_href[get_href.length - 1] = '';
            
    //         var new_href =  get_href.join('/') + lasthref_origin.join('?') + '&min=' + datafrom + '&max=' + datato;
    //         window.location.href = new_href;
    //     });
    // }
    
    // if(homepage_collections.length > 0) {
    //     $.ajax({
    //         type: 'GET',
    //         url: '/api/v1_products/collections',
    //         success: function(response) {
    //             var arr_collections = response.result.collection;
    //             var getcontainer_collections = $('#homepage-collections .container-collections-block');
    //             getcontainer_collections.html('');
                
    //             var i = 0;
    //             arr_collections.forEach(function(item) {
    //                 if (i <= 2) {
    //                     getcontainer_collections.append(create_block_collections(item, i));
    //                     i++;
    //                 }
    //             });
    //         }
    //     });
    // }
    
    // function create_block_collections(item, index) {
    //     var element_1 = "<div class='row align-items-center row-collections-block'>";
        
    //     var element_2 = "<div class='col-12 col-md-6 col-content text-center'>";
    //     var element_3 = "<h3>" + item.title + "</h3>";
    //     var element_4 = "<p></p>";
    //     var element_5 = "<a href='" + item.url + "'>Xem thêm</a>";
    //     var element_6 = "</div>";
        
    //     var element_content = element_2 + element_3 + element_4 + element_5 + element_6;
        
    //     var element_image = "<div class='col-12 col-md-6 col-image text-center'><img src='" + (item.image) + "' alt=''></div>";
        
    //     var element_8 = "</div>";
        
    //     if (index % 2 == 0) {
    //         return element_1 + element_content + element_image + element_8;
    //     } else {
    //         return element_1 + element_image + element_content + element_8;
    //     }
    // }
});