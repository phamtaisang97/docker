EGA.preloader = () => {
	$('.body__preloader').addClass("loaded");
}
$('.ega-slider').owlCarousel({
	dots: false,
	loop: true,
	items: 1,
	autoplay: true,
	autoplayTimeout: 3000,
	nav: true,
	navText: ['<svg class="ega-ic-angle-left"><use xlink:href="#ega-ic-angle-left"></use></svg>','<svg class="ega-ic-angle-right"><use xlink:href="#ega-ic-angle-right"></use></svg>'],
	responsive:{
		0:{
			dots: true,
			nav: false
		},
		992:{
			dots: false,
			nav: true,
			navText: ['<svg class="ega-ic-angle-left"><use xlink:href="#ega-ic-angle-left"></use></svg>','<svg class="ega-ic-angle-right"><use xlink:href="#ega-ic-angle-right"></use></svg>']
		}
	}																 
})

//end index

addToCartCallback = (data, isQuickView) => {
	$('#cart-popup-mb .pd-name').text(data.product_title);
	$('#cart-popup-mb img').attr("src",data.image);
	$('#cart-popup-mb .pd-price').text(Haravan.formatMoney(data.price,EGA.moneyFormat));
	Haravan.getCart(EGA.getCartAjax);

	if (window.innerWidth > 768) {
		if(isQuickView){
			EGA.modal.openModalById('cart-popup');
			setTimeout(function() {
				$('#cart-popup').removeClass('show');
				$('body').removeClass('ega-modal-showed')
			}, 4000);
		} else {
			Haravan.getCart(EGA.getCartDetail);
			EGA.modal.openModalById('cart-popup-detail');
		}
	} else {
		EGA.modal.openModalById('cart-popup-mb');
	}
}

//Get Cart Detail
EGA.getCartDetail = (cart) => {
	if(cart){
		$('#cart-popup-detail .count-cart span').text(cart.item_count);
		let html = '';
		$.each(cart.items, function(i, item) {
			html += `<tr class="ega-cart-row">
<td>
<a href="${item.url}">
<img src="${Haravan.resizeImage(item.image,'small')}" alt="${item.title}"></a>
</td>
<td>
<a class="ega-base-color__text" href="${item.url}">${item.title} <span class="ega-color--danger">${item.variant_options.join("-")}</span></a>
</td>
<td>
${Haravan.formatMoney(item.price, EGA.moneyFormat)}
</td>
<td>
<div class="ega-d--flex ega-m-y--1" data-ega-role='qty-wrapper'>
<button type="button"data-ega-role='qty-minus' class="qtyminus ega-qty-btn ega-btn ega-btn--sm ega-bg--secondary ega-btn--square ega-color--black ega-btn--outline ega-m--0">-</button>
<input type="text" data-ega-role='qty' size="4" name="updates[]" min="1" id="updates_${item.id}" data-price="${item.price}" value="${item.quantity}" class="tc line-item-qty item-quantity ega-text--center"/>
<button type="button" data-ega-role='qty-plus' class="qtyplus qty-btn  ega-btn  ega-btn--sm ega-bg--secondary ega-btn--square ega-color--black ega-btn--outline ega-m--0">+</button>
</div>
</td>
<td>
<span class="line-item-total ega-text--bold">${Haravan.formatMoney(item.line_price, EGA.moneyFormat)}</span>
</td>
</tr>`
		});
		$('#cart-popup-detail .ega-table tbody').html(html);
	}
}

//Get cart
EGA.getCartAjax = (cart) => {
	const top_cart_empty = `<div class="ega-top-cart__header ega-text--center">Chưa có sản phẩm trong giỏ!</div>
<div class="ega-top-cart__footer">
<div class="ega-top-cart__action ega-text--center">
<a class="ega-btn ega-btn--square ega-m--0 ega-border--0 ega-p-l--5 ega-p-r--5" href="/collections/all"><span class="text">VÀO CỬA HÀNG</span></a>
</div>
</div>`

	if(cart) {
		let html = `<div class="ega-top-cart__title"><h4>Giỏ hàng</h4></div>
<div class="ega-top-cart__items">`;
		$.each(cart.items,function(i, item) {
			//let title = item.product_title.length > 10? item.product_title.split(0,10)+ '...' : item.product_title;

			html += `
<div class="ega-top-cart__item ega-clearfix">
<input type="hidden" class="item_id" value="${item.variant_id}">
<input type="hidden" class="item_qty" value="${item.quantity}">
<input type="hidden" class="item_unit_price_not_formated" value="${item.price}">
<div class="ega-top-cart__item-image ega-f--left">
<a href="${item.url}"><img src="${Haravan.resizeImage(item.image,'small')}" alt="${item.product_title}"></a>
</div>
<div class="ega-top-cart__item-desc">
<a href="${ item.url}">${item.product_title}</a>
<span class="ega-top-cart__item-price">${Haravan.formatMoney(item.price, EGA.moneyFormat)}</span>
<span class="ega-top-cart__item-quantity">x ${item.quantity}</span>
<a class="ega-top-cart__item_remove" onclick="EGA.deleteCart(${item.variant_id}, EGA.getCartAjax);">
<svg class="ega-ic-times-circle">
<use xlink:href="#ega-ic-times-circle"></use>
</svg>
</a>
</div>
</div>
`
		});
		html += `</div>
<div class="ega-top-cart__action ega-clearfix">
<span class="top-checkout-price">${Haravan.formatMoney(cart.total_price, EGA.moneyFormat)}</span>
<button onclick="window.location.href=&quot;/cart&quot;" class="ega-btn ega-f--right ega-m--0 ega-border--0">Xem giỏ hàng</button>
</div>`;

		$('.ega-top-cart__qty').html(cart.item_count);

		if(cart.item_count > 0){	
			$('.ega-top-cart__content').html(html); 
		}
		else {
			$('.ega-top-cart__content').html(top_cart_empty); 
		}
	}
	else {
		$('.ega-top-cart__items').html(top_cart_empty); 
	}
}

EGA.start = () => {
	EGA.quickView();
	EGA.modal.open();
	EGA.modal.close();
	EGA.dropdown.open();
	EGA.quantity();
	EGA.alertSubscribe();
	EGA.scrolltotop();
	EGA.switchForm();
	EGA.alertSubmit();
	EGA.tabs.init();
}
EGA.preloader();
window.onload = function() {
	EGA.start();
}

$(window).on('scroll', function() {
	EGA.sticky({ $header_el : ".ega-header", element_offset : 40 });
})

productScroll = $('#ega-product-scroll');

if($(window).width() >= 1200){
	$(window).scroll(function() {
		let top = $(document).height()
		- ($(window).scrollTop() 
			 + productScroll.outerHeight(true)
			 + $('.ega-pro__recentview').outerHeight(true) 
			 + $('.ega-subscribe').outerHeight(true)
			 + $('#ega-footer').outerHeight(true));

		if ($(window).scrollTop() > $('#ega-product-main').height()) {
			productScroll.addClass('ega-pos--fixed');
		} else {
			productScroll.removeClass('ega-pos--fixed');
		}

		if (($(window).scrollTop() + productScroll.height()) > $('#ega-product-detail').height()) {
			productScroll.css('top', top);
		} else {
			productScroll.css('top', 110);
		}
	})
}

$(document).ready(function(){
    $('#sort-wrap .ega-sortby-select span').click(function(){
        $(this).parent().toggleClass('show-sort');
    })
    $('body').click(function(e){
        if(!$(e.target).parent().hasClass("ega-sortby-select")){
            $('#sort-wrap .ega-sortby-select').removeClass('show-sort');
        }
    })
})

