function resizeHeightElementRatio(elSelector, ratio = 1) {
    const element = jQuery(elSelector);
    if (element.length) {
        element.height(element.width() / ratio);
    }
}
$(document).ready(function () {
    arr_product1 = $( ".bestme-block-best-sales-product" ).find( ".bestme-product .bestme-product-top");
    arr_product2 = $( ".bestme-block-detail-product" ).find( ".bestme-product .bestme-product-top");
    arr_product3 = $( ".bestme-top-content.page-detail" ).find( ".bestme-product .bestme-product-top");
    resizeHeightElementRatio(arr_product1);
    resizeHeightElementRatio(arr_product2);
    resizeHeightElementRatio(arr_product3);
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll > 500) {
            /*Ẩn hiện button thêm vào giỏ hàng trong header chi tiết sản phẩm  desktop*/
            $('.product-detail .bestme-search-global').addClass('bestme-hidden');
            //$('.product-detail .list-action').addClass('bestme-hidden');
            $('.product-detail .bestme-block-title-header-scroll').removeClass('bestme-hidden');
            $('.product-detail .bestme-action-title-header-scroll').removeClass('bestme-hidden');
            $('#product-detail').removeClass('bestme-hidden');
            /*End*/
        } else {
            /*Ẩn hiện button thêm vào giỏ hàng trong header chi tiết sản phẩm  desktop*/
            $('.product-detail .bestme-search-global').removeClass('bestme-hidden');
            //$('.product-detail .list-action').removeClass('bestme-hidden');
            $('.product-detail .bestme-block-title-header-scroll').addClass('bestme-hidden');
            $('.product-detail .bestme-action-title-header-scroll').addClass('bestme-hidden');
            $('#product-detail').addClass('bestme-hidden');
        }
    });
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll > 400) {
            /*Ẩn hiện button thêm vào giỏ hàng trong header chi tiết sản phẩm  mobile */
            $('.bestme-action-top-detail-product').addClass('active');
        } else {

            $('.bestme-action-top-detail-product').removeClass('active');
        }
    });
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll > 400) {
            /*Header scroll*/
            $('.bestme-header__banner').addClass('bestme-hidden');
            $('.bestme-header').addClass('hidden-banner');
        } else {
            /*Ẩn hiện button thêm vào giỏ hàng trong header chi tiết sản phẩm  mobile*/
            $('.bestme-header__banner').removeClass('bestme-hidden');
            $('.bestme-header').removeClass('hidden-banner');
        }
    });
    /* Hiện dropdown khi ấn vào user */
    $('#user-icon').click(function () {
        $('.list-action-sub').toggle();
    });
    $('.user-icon').click(function () {
        $('.list-action-sub-2').toggle();
    });
    /* 20190710 */
    /*Ẩn hiện menu chính*/
    $(document).mouseup(function(e)
    {
        var container = $(".bestme-search-global");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            container.find('.bestme-search-result ').removeClass('active');
            container.parents().find('.close-search').addClass('bestme-hidden');
            container.parents().find('.bestme-list-action').removeClass('bestme-hidden');
            container.parents().find('.bestme-menu').removeClass('bestme-hidden');

        }
    });

    $(document).mouseup(function(e)
    {
        var container = $(".list-action-sub");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
           container.hide();
        }
    });
    /* end - 20190710 */

    $('.bestme-menu-text').click(function () {
        $(this).toggleClass('active');
        $('.icon-menu').toggleClass('active');
        $('body').toggleClass('active-menu');
        $('.list-menu').toggleClass('active ');
    });

    $('.icon-toogle').click(function (event) {
        event.preventDefault();
        $(this).toggleClass('active');
        $(this).parent('.menu-link').next('.sub-menu').toggleClass('active');
    });

    /* 20190710 */
    $('.list-menu-overlay').click(function () {
        $('.bestme-menu-text').removeClass('active');
        $('.icon-menu').removeClass('active');
        $('.list-menu').removeClass('active');
        $('body').removeClass('active-menu');
    });
    /* end - 20190710 */

    /*End */

    /*Ẩn hiện block lọc */
    $('.bestme-block-sort__title').click(function () {
        $(this).toggleClass('active');
        $(this).parents('.bestme-block-sort').find('.bestme-block-sort__list').toggleClass('active');
    });
    /*End*/

    /*Bật tắt menu nhỏ bên trái trang index*/
    $('.sub-menu-link').click(function () {
        $(this).toggleClass('active');
        $(this).parents('.sub-menu-item').find('.sub-menu-child').toggleClass('active');
    });
    $('.bestme-menu--sub__link').click(function (e) {
        e.preventDefault();
        $('.bestme-menu--sub__item').removeClass('active');
        $(this).parents('.bestme-menu--sub__item').toggleClass('active');
    });
    /*End */

    /*Ẩn hiện menu trong lọc là ul li */
    $('.list-link .sort-item').click(function (e) {
        e.preventDefault();
        $('.sort-item').removeClass('active');
        $(this).toggleClass('active');
    });
    /*End */

    /*Active pagination*/
    $('.bestme-pagination__item').click(function (e) {
        e.preventDefault();
        $('.bestme-pagination__item').removeClass('active');
        $(this).addClass('active');
    });
    /*End*/
    /* Hiện Danh mục sản phẩm trong mobile */
    $('.bestme-menu--sub--mobile .title ').click(function () {
        $(this).parents().toggleClass('active');
    });

    /* 20190710 */
    $('.bestme-btn--filter-mobile').click(function () {
        $(this).parents().find('.list-block-sort-mobile').addClass('active');

    });
    $('.list-block-sort-mobile .overlay').click(function () {
        $(this).parents().removeClass('active');
    });
    /* end - 20190710 */

    /* Hàm sửa fill svg*/
    $(function () {
        // For each image with an SVG class, execute the following function.
        $("img.svg").each(function () {
            // Perf tip: Cache the image as jQuery object so that we don't use the selector muliple times.
            var $img = jQuery(this);
            // Get all the attributes.
            var attributes = $img.prop("attributes");
            // Get the image's URL.
            var imgURL = $img.attr("src");
            // Fire an AJAX GET request to the URL.
            $.get(imgURL, function (data) {
                // The data you get includes the document type definition, which we don't need.
                // We are only interested in the <svg> tag inside that.
                var $svg = $(data).find('svg');
                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');
                // Loop through original image's attributes and apply on SVG
                $.each(attributes, function () {
                    $svg.attr(this.name, this.value);
                });
                // Replace image with new SVG
                $img.replaceWith($svg);
            });
        });
    });

    /*Slide chính trong trang index*/
    $('.bestme-block-slideshow').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        infinite: true,
        cssEase: 'linear',
        prevArrow: false,
        nextArrow: false,
        autoplay:true,
        autoplaySpeed: $('.bestme-block-slideshow').attr('data-time'),
    });
    /*End*/
    /*Slide trong trang chi tiết sản phẩm*/
    $('.bestme-block-detail-product--mobile-top').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        infinite: true,
        cssEase: 'linear',
        prevArrow: '<button type="button" class="slick-prev">Previous</button>',
        nextArrow: '<button type="button" class="slick-next">Next</button>',

    });
    /*End*/

    /*Slide các nhãn hợp tác gần footer*/
    $('.bestme-slide-brand').slick({
        dots: false,
        infinite: true,
        slidesToShow: jQuery('.bestme-slide-brand').attr('data-limit-inline'),
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: jQuery('.bestme-slide-brand').attr('data-limit-inline'),
                    slidesToScroll: 1,
                    infinite: true,
                    prevArrow: false,
                    nextArrow: false,
                }
            },
            {
                breakpoint: 575,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    prevArrow: false,
                    nextArrow: false,
                    infinite: true
                }
            }
        ]
    });
    /*End*/
    /*Slide các nhãn hợp tác gần footer*/
    $('#bestme-post-relate').slick({
        dots: true,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        cssEase: 'linear',
        prevArrow: false,
        nextArrow: false,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                    prevArrow: false,
                    nextArrow: false,
                }
            },
            {
                breakpoint: 575,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    prevArrow: false,
                    nextArrow: false,
                    infinite: true,
                }
            }
        ]
    });
    /*End*/

    /*Zoom ảnh trong chi tiết sản phẩm*/
    $('img.bestme-magnify-image').magnify();
    /*End*/

    /* 20190710 */
    /*Click chuyển ảnh trong chi tiết sản phẩm*/
    $('.list-image li').click(function () {
        $('.list-image li').removeClass('active');
        $(this).addClass('active');
        var srcimg = $(this).find('img').attr('src');
        $('.bestme-magnify-image').attr('src', srcimg);
        $('.bestme-magnify-image').attr('data-magnify-src', srcimg).magnify();
    });
    /*End*/
    /*Get gia tri trong so san pham mua */
    $('.increase').click(function () {
        let $parents = $(this).parent('.bestme-group--count-amount');
        let $amount = $parents.find('.bestme-input--number-amount').val();
        $amount = parseInt($amount) + 1;
        $parents.find('.bestme-input--number-amount').val($amount);
        $parents.find('.bestme-input--number-amount').trigger('change');
        if ($amount > 0) {
            $parents.find('.decrease').removeAttr('disabled');
        }
    });
    $('.decrease').click(function () {
        let $parents = $(this).parent('.bestme-group--count-amount');
        let $amount = $parents.find('.bestme-input--number-amount').val();
        $amount = parseInt($amount) - 1;
        if ($amount === 0) {
            $(this).attr('disabled', 'disabled');
        }
        $parents.find('.bestme-input--number-amount').val($amount);
        $parents.find('.bestme-input--number-amount').trigger('change');
    });
    /* end - 20190710 */
    /*End*/

    /*Xem thêm trong mobile ở chi tiết sản phẩm*/
    $('.bestme-btn--show-more-detail-product').click(function () {
        $(".bestme-product--desc-detail--mobile").toggleClass("active");
        if ($('.bestme-product--desc-detail--mobile').hasClass('active')) {
            $(".see-more").text("Rút gọn");
            $(".icon-des").addClass("ti-angle-up");
            $(".icon-des").removeClass("ti-angle-down");
            $(".see-mobile-on-click").removeClass('bestme-hidden-md');
        } else {
            $(".see-more").text("Xem thêm");
            $(".icon-des").addClass("ti-angle-down");
            $(".see-mobile-on-click").addClass('bestme-hidden-md');
        }
    });
    /*Hiện box search suggest */
    /* 20190710 */
    $('.bestme-input--search-global').focus(function (e) {
        $('.bestme-search-result').addClass('active');
        $('.bestme-list-action').addClass('bestme-hidden');
        $('.bestme-menu').addClass('bestme-hidden');
        $('.close-search').removeClass('bestme-hidden');
        e.stopPropagation();
    });
    $('.close-search').click(function () {
        $('.bestme-search-result').removeClass('active');
        $('.bestme-list-action').removeClass('bestme-hidden');
        $('.bestme-menu').removeClass('bestme-hidden');
        $('.close-search').addClass('bestme-hidden');
    });
    /* end - 20190710 */

    /*Active list tab trong account mobile*/
    $('.bestme-tab--account-mobile .title').click(function () {
        $(this).parents().toggleClass('active');
    });
    /*Active order  trong pay mobile*/
    $('.bestme-title-order-mobile ').click(function () {
        $(this).toggleClass('active');
        $('.bestme-block-list-order--mobile').toggleClass('active');
    });
    /*Active menu trong blog mobile*/
    $('.bestme-btn--list-menu-blog-mobile.open ').click(function () {
        $(this).removeClass('active');
        $('.bestme-btn--list-menu-blog-mobile.close-menu').addClass('active');
        $('.list-category-mobile').addClass('active');
    });
    $('.bestme-btn--list-menu-blog-mobile.close-menu ').click(function () {
        $(this).removeClass('active');
        $('.bestme-btn--list-menu-blog-mobile.open').addClass('active');
        $('.list-category-mobile').removeClass('active');
    });

    jQuery('.bestme-eye').on('click', function () {
        if(jQuery(this).hasClass('active')) {
            jQuery('.bestme-form__item__password input').attr('type', 'text'),
            jQuery(this).removeClass('active');
        } else {
            jQuery('.bestme-form__item__password input').attr('type', 'password'),
                jQuery(this).addClass('active');
        }
    });

    // Address setting page collapse
    $('.address-setting-panel .left a[data-toggle="collapse"]').on('click', function () {
        $('.form-collapse .collapse.show').collapse('hide');
        console.log(11111);
    })

});

/*Custom select */
var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    /*for each element, create a new DIV that will act as the selected item:*/
    a = document.createElement("DIV");
    a.setAttribute("class", "select-selected");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);
    /*for each element, create a new DIV that will contain the option list:*/
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 1; j < selElmnt.length; j++) {
        /*for each option in the original select element,
        create a new DIV that will act as an option item:*/
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.setAttribute('data-option-value', selElmnt.options[j].value);
        c.addEventListener("click", function (e) {
            /*when an item is clicked, update the original select box,
            and the selected item:*/
            var y, i, k, s, h;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            h = this.parentNode.previousSibling;
            for (i = 0; i < s.length; i++) {
                if (s.options[i].innerHTML == this.innerHTML) {
                    s.selectedIndex = i;
                    h.innerHTML = this.innerHTML;
                    y = this.parentNode.getElementsByClassName("same-as-selected");
                    for (k = 0; k < y.length; k++) {
                        y[k].removeAttribute("class");
                    }
                    this.setAttribute("class", "same-as-selected");
                    break;
                }
            }
            h.click();
        });
        b.appendChild(c);
    }
    x[i].appendChild(b);
    a.addEventListener("click", function (e) {
        /*when the select box is clicked, close any other select boxes,
        and open/close the current select box:*/
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
    });
}

function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}

/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
/*End*/

/* 20190710 */
/*// Show password
$(document).on('click', '.bestme-form__item__password .show-password', function(e) {
    var input = $(this).parent().find('>input');
    $(this).toggleClass('active');
    if ($(this).hasClass('active')){
        input.attr('type', 'text');
    } else {
        input.attr('type', 'password');
    }
});*/
/* end - 20190710 */