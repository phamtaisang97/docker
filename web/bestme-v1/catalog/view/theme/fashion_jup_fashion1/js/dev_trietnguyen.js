$(document).ready(function() {
    // var tab_product_detail = $('#product-detail-page .product-detail #myTabContent .tab-pane .tab-content-description');
    // var collapse_tab_product_detail = $('#product-detail-page .product-detail #myTabContent .dv-collapse .btn-collapse');
    // var getheight_tab_product_detail = tab_product_detail.height();
    
    // if (getheight_tab_product_detail > 215) {
    //     tab_product_detail.addClass("hide");
    // }
    
    // collapse_tab_product_detail.on('click', function() {
    //     if (tab_product_detail.hasClass("hide")) {
    //         tab_product_detail.removeClass("hide");
    //         collapse_tab_product_detail.addClass("hide");
    //     }
    // });
    
    $("html,body").animate({
        scrollTop: 0
    }, 750);

    $('#btn-scroll-top').click(function() {
        $("html,body").animate({
            scrollTop: 0
        }, 750);
    });
    
    const diffslide_hot_products = $("#diffslide-hot-products");
    const difflist_sale_off_products = $("#difflist-sale-off-products");
    const homepage_ads_banner = $("#homepage-ads-banner");
    // const homepage_collections_banner = $("#homepage-slide-products-banner #grid-collections");
    const header_main_menu = $("header.block-header .menu-main nav.bestme-block-header-menu #navbarMainMenu");
    const productdetailpage_teaser_fix = $("#product-detail-page .product-teaser-fix");
    
    if (productdetailpage_teaser_fix.length > 0) {
        if ($(document).width() < 768) {
            productdetailpage_teaser_fix.addClass('mobile');
        }
        
        var quantiy_amount_addtocart_product_detail_main = $("#product-detail-page .product-detail .quantity #input-amount-add-to-cart");
        var quantiy_amount_addtocart_product_detail_teaser_fix = productdetailpage_teaser_fix.find(".quantity-teaser-fix #input-amount-add-to-cart");
        
        quantiy_amount_addtocart_product_detail_main.change(function() {
           quantiy_amount_addtocart_product_detail_teaser_fix.val($(this).val());
        });
        
        quantiy_amount_addtocart_product_detail_teaser_fix.change(function() {
           quantiy_amount_addtocart_product_detail_main.val($(this).val());
        });
    }
    
    if(header_main_menu.length > 0) {
        var header_nav_haschild = $("header.block-header .menu-main nav.bestme-block-header-menu #navbarMainMenu ul.nav li.nav-item.has-child");
        
        header_nav_haschild.on({
            mouseover: function() {
                $(this).find("ul.child-nav").addClass('show');
                $(this).find("ul.child-nav").on({
                    mouseover: function() {
                        $(this).find("ul.child-nav").addClass('show');
                    },
                    mouseleave: function(){
                        $(this).find("ul.child-nav").removeClass('show');
                    }
                });
            },
            mouseleave: function(){
                $(this).find("ul.child-nav").removeClass('show');
            }
        });
        
        var header_nav_haschild_2 = $("header.block-header .menu-main nav.bestme-block-header-menu #navbarMainMenu ul.nav > li.nav-item.has-child-2");
        
        header_nav_haschild_2.on({
            mouseover: function() {
                $(this).find("ul.child-nav-2").addClass('show');
                $(this).find("ul.child-nav-2").on({
                    mouseover: function() {
                        $(this).find("ul.child-nav-2").addClass('show');
                    },
                    mouseleave: function(){
                        $(this).find("ul.child-nav-2").removeClass('show');
                    }
                });
                console.log('has-child-2 mouseover');
            },
            mouseleave: function(){
                $(this).find("ul.child-nav-2").removeClass('show');
                console.log('has-child-2 mouseleave');
            }
        });
    }
    
    if(diffslide_hot_products.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_products/products',
            success: function(response) {
                var arr_products = response.result;
                var getcontainer_slide_hot = $('#diffslide-hot-products #diffslide-products-carousel .carousel-inner');
                getcontainer_slide_hot.html('');
                
                var i = 0;
                arr_products.forEach(function(item) {
                    if (item.is_best_sale_product && item.product_in_stock) {
                        getcontainer_slide_hot.append(create_hot_products(item, i));
                        i++;
                    }
                });
            }
        });
    }
    
    if(difflist_sale_off_products.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_products/products',
            success: function(response) {
                var arr_products = response.result;
                var getcontainer_list_best_sale = $('#difflist-sale-off-products #list-best-sale-products');
                getcontainer_list_best_sale.html('');
                
                var i = 0;
                arr_products.forEach(function(item) {
                    if (item.percent_protduct != "") {
                        getcontainer_list_best_sale.append(create_best_sale_products(item));
                        i++;
                    }
                });
            }
        });
    }
    
    function decodeHtml(str) {
        var map = {
            '&amp;': '&',
            '&lt;': '<',
            '&gt;': '>',
            '&quot;': '"',
            '&#039;': "'"
        };
        return str.replace(/&amp;|&lt;|&gt;|&quot;|&#039;/g, function(m) {return map[m];});
    }
    if(homepage_ads_banner.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_common/custom_contents',
            success: function(response) {
                var arr_ads_banner = response.display_customizes;
                
                var homepage_section_banner_contain = homepage_ads_banner.find(".banner-contain");
                
                var html_banner = '';
                
                for (var i = 0; i < 4; i++) {
                    var ad_banner_item = homepage_ads_banner.find(".item-banner-" + i);
                    var index_item = '';
                    if (i % 2 == 0) {
                        index_item = "even";
                    } else {
                        index_item = "odd";
                    }
                    
                    switch(arr_ads_banner[i].type) {
                      case "fixed":
                        var element = "<div class='col-12 col-md-3 ads-banner-item ads-banner-item-" + index_item + " item-banner-0'>" +
                            "<img class='image mb-2' src='" + arr_ads_banner[i].content.icon + "' alt='" + arr_ads_banner[i].title + "'  width='50' height='50'>" +
                            "<div class='ads-banner-content'>" +
                            "<h4>" + arr_ads_banner[i].title + "</h4>" +
                            "<p>" + arr_ads_banner[i].content.description + "</p>" +
                            "</div></div>";
                        
                        html_banner += element;
                        break;
                      case "manual":
                        html_banner += decodeHtml("<div class='col-12 col-md-3' style='overflow: auto; height: 125px'>" + arr_ads_banner[i].content.html + "</div>");
                        break;
                      default:
                        var element = "<div class='col-12 col-md-3 ads-banner-item ads-banner-item-" + index_item + " item-banner-0'>" +
                            "<img class='image mb-2' src='" + arr_ads_banner[i].content.icon + "' alt='" + arr_ads_banner[i].title + "'  width='50' height='50'>" +
                            "<div class='ads-banner-content'>" +
                            "<h4>" + arr_ads_banner[i].title + "</h4>" +
                            "<p>" + arr_ads_banner[i].content.description + "</p>" +
                            "</div></div>";
                            
                        html_banner += element;
                    }
                    
                    // ad_banner_item.find(".image").attr("src", arr_ads_banner[i].content.icon);
                    // ad_banner_item.find(".image").attr("alt", arr_ads_banner[i].title);
                    
                    // ad_banner_item.find(".ads-banner-content h4").html(arr_ads_banner[i].title);
                    // ad_banner_item.find(".ads-banner-content p").html(arr_ads_banner[i].content.description);
                }
                homepage_section_banner_contain.html(html_banner);
            }
        });
    }
    
    // if(homepage_collections_banner.length > 0) {
    //     $.ajax({
    //         type: 'GET',
    //         url: '/api/v1_products/collections',
    //         success: function(response) {
    //             var arr_collections_banner = response.result.collection;
               
    //             for (var i = 0; i < 4; i++) {
    //                 var collections_banner_item = homepage_collections_banner.find(".item-" + i);
                    
    //                 collections_banner_item.attr("style", "background-image: url(" + arr_collections_banner[i].image  + ")");
    //                 collections_banner_item.find(".link").attr("href", arr_collections_banner[i].url);
                    
    //                 collections_banner_item.find(".content h3").html(arr_collections_banner[i].title);
    //                 collections_banner_item.find(".content p").html("");
    //             }
    //         }
    //     });
    // }
    
    
    function create_hot_products(item, index) {
        var element_1 = "<div class='carousel-item text-center " + ((index === 0) ? 'active':'') + "'><div class='col-12 slide-product-item'><div class='product-info'>";
        var element_2 = (item.percent_protduct === "") ? "" : "<div class='product-highlight text-center'><span class='sale'>" + item.percent_protduct + "</span></div>";
        var element_3 = "<a href='" + item.href + "' title='" + item.name + "'><div class='product-image' style='background-image: url(" + item.thumb + ");'></div></a>";
        var element_4 = "<div class='product-name my-3'><a href='" + item.href + "' title='" + item.name + "' class='product-title'>" + ((item.name.length > 15) ? (item.name.slice(0, 15) + '...') : item.name) + "</a></div>";
        
        // var element_5 = "<span class='product-price'><span class='price'>" + item.price + "</span></span><span class='product-price-sale'><span class='price'>" + item.compare_price + "</span></span>";
        if (item.multi_versions != 1) {
            if (item.price != "0 VND") {
                var element_5 = "<span class='product-price'><span class='price'>" + item.price + "</span></span><span class='product-price-sale'><span class='price'>" + item.compare_price + "</span></span>";
            } else {
                var element_5 = "<p>Liên hệ</p>";
            }
        } else {
            if (item.compare_version_min != "0 VND") {
                var element_5 = "<p class='product-price'><span class='price'>" + item.compare_version_min + "~" + item.compare_version_max + "</span></p><p class='product-price-sale'><span class='price'>" + item.price_version_min + "~" + item.price_version_max + "</span></p>";
            } else {
                var element_5 = "<p>Liên hệ</p>";
            }
        }
        
        var element_6 = "</div></div></div>";
        
        return element_1 + element_2 + element_3 + element_4 + element_5 + element_6;
    }
    
    function create_best_sale_products(item) {
        var element_1 = "<a href='" + item.href + "' data-toggle='tooltip' title='" + item.name + "'><div class='d-flex list-product-item my-3'>";
        var element_2 = "<div class='col-4 product-image' style='background-image: url(" + item.thumb + ")'></div>";
        var element_3 = "<div class='col-8 product-info'>";
        var element_4 = "<h5 class='product-title'>"  + ((item.name.length > 15) ? (item.name.slice(0, 15) + '...') : item.name) + "</h5>";
        
        // var element_5 = "<p class='product-price-sale'>" + item.compare_price + "</p><p class='product-price'>" + item.price + "</p>";
        if (item.multi_versions != 1) {
            if (item.price != "0 VND") {
                var element_5 = "<p class='product-price-sale'>" + item.compare_price + "</p><p class='product-price'>" + item.price + "</p>";
            } else {
                var element_5 = "<p>Liên hệ</p>";
            }
        } else {
            if (item.compare_version_min != "0 VND") {
                var element_5 = "<p class='product-price-sale'>" + item.compare_version_min + "~" + item.compare_version_max + "</p><p class='product-price'>" + item.price_version_min + "~" + item.price_version_max + "</p>";
            } else {
                var element_5 = "<p>Liên hệ</p>";
            }
        }
        var element_6 = "</div></div></a>";
        
        return element_1 + element_2 + element_3 + element_4 + element_5 + element_6;
    }
});