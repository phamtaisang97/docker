function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

// Cart add remove functions
var cart = {
    'add': function(product_id, quantity) {
        var valid = validateSerialize();
        if (!valid) {
            $('input[name="quantity"]')[0].reportValidity();
            return false;
        }
        if (jQuery('#add-to-cart button[type="submit"]').attr('data-status') == "disabled") {
            return false;
        }
        $.ajax({
            url: 'index.php?route=checkout/my_orders/addCart',
            type: 'post',
            data: $('#add-to-cart').serialize(),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('.block-header-action .count').html(json['total_product_cart']);

                    $('#add_cart_notify').modal('show');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'update': function(key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var voucher = {
    'add': function() {

    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var wishlist = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);

                $('html, body').animate({ scrollTop: 0 }, 'slow');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}

var compare = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#compare-total').html(json['total']);

                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
    e.preventDefault();

    $('#modal-agree').remove();

    var element = this;

    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function(data) {
            html  = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div>';
            html += '  </div>';
            html += '</div>';

            $('body').append(html);

            $('#modal-agree').modal('show');
        }
    });
});
/// update product detail when choose version
$(document).ready(function () {
    if (jQuery('#add-to-cart .form-check-input').length) {
        ajaxChangeVersion();
    }
    jQuery('#add-to-cart .form-check-input').on('click', function () {
        ajaxChangeVersion();
    });
});

function ajaxChangeVersion() {
    $.ajax({
        url: 'index.php?route=product/product/updateCartWhenChangeVersion',
        type: 'post',
        data: $('#add-to-cart').serialize(),
        dataType: 'json',
        beforeSend: function() {
            //TODO loading
        },
        complete: function() {
            //TODO loaded
        },
        success: function(json) {
            if (json['success']) {
                updateHtmlVersion(json['sale_on_out_of_stock'], json['product_version']);
            } else {
                jQuery('#add-to-cart button').attr('data-status', 'disabled');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}

function updateHtmlVersion(sale_on_out_of_stock, product) {
    // version
    if (product['version']) {
        // form mua tra gop
        var version = product['version'].replace(",", " - ")
        $('input#pAttribute').val(version)
    }

    //update product version
    if (product['version']) {
        if (parseInt(product['compare_price_format']) === 0) {
            jQuery('#buy-now .product-version').text(' - ' + product['version']);
            jQuery('#buy-now button').hide();
        } else {
            jQuery('#buy-now .product-version').text(' - ' + product['version'] + ' - ' + ((product['price'] == 0) ? product['compare_price_format'] : product['price_format']));
            jQuery('#buy-now button').show();
        }
    } else {
        jQuery('#buy-now .product-version').text(' - ' + ((product['price'] == 0) ? product['compare_price_format'] : product['price_format']));
    }

    //update price
    if (product['price'] == 0) {
        var compare_price = parseInt(product['compare_price_format']);
        jQuery('.block-detail .price-box').html('<span class="price">'+ (compare_price === 0 ?
            '<a href="javascript:;" data-toggle="modal" data-target="#modal-buy" class="btn btn-buy">\n'
            + '<span class="price-new">' + 'Liên hệ' + '</span></a>'
            : '<span class="price-new">' + product['compare_price_format'] + '</span>'));
        jQuery('.block-detail .price-box .price-old').html('<span class="price"></span>');
    } else {
        if (parseInt(product['price_format']) === 0) {
            jQuery('.block-detail .price-box').html(
                '<a href="javascript:;" data-toggle="modal" data-target="#modal-buy" class="btn btn-buy">\n'
                + '<span class="price-new">' + 'Liên hệ' + '</span></a>'
            );
        } else {
            jQuery('.block-detail .price-box .price-new').html('<span class="price">'+ product['price_format'] +'</span>');
            jQuery('.block-detail .price-box .price-old').html('<span class="price">'+ product['compare_price_format'] +'</span>');
        }
    }

    if (parseInt(product['compare_price_format']) === 0) {
        jQuery('.block-detail .form-action.show-on-pc').css('display', 'none');
        jQuery('.block-detail .block-custom-change .form-action button').prop('disabled', true);
        jQuery('.block-detail .block-custom-change .buy-now-mobile').css('visibility', 'hidden');
    } else {
        jQuery('.block-detail .form-action.show-on-pc').css('display', 'flex');
        jQuery('.block-detail .block-custom-change .form-action button').removeAttr('disabled');
        jQuery('.block-detail .block-custom-change .buy-now-mobile').css('visibility', 'visible');
    }
    //update status
    jQuery('#block-detail-product .code-product strong').html(product['sku']);
    //active button add cart
    //TODO : remove if check is_stock done
    // if (product['status'] == 1 && (sale_on_out_of_stock == 1 || (sale_on_out_of_stock == 0 && product['quantity'] > 0))) {
    if (product['status'] == 1 && (sale_on_out_of_stock == 1)) {
        jQuery('#add-to-cart button').removeAttr('data-status');
        jQuery('#buy-now button').removeAttr('data-status');
        jQuery('.block-detail .status-box').html('<span class="true"><i class="fa fa-check-square-o" aria-hidden="true"></i>Còn hàng</span>');
        jQuery('#add-to-cart button').removeClass("hover");
    } else {
        jQuery('#add-to-cart button').attr('data-status', 'disabled');
        jQuery('#buy-now button').attr('data-status', 'disabled');

        jQuery('.block-detail .status-box').html('<span class="false">Hết hàng</span>');
        jQuery('#add-to-cart button').addClass("hover");
    }

    /* update product version image */
    // set default values
    product.image = product.image ? product.image : jQuery('#product-image-default-src').val();
    product.image_alt = product.image_alt ? product.image_alt : '';
    if (product.image) {
        const imageSlider = jQuery('.owl-detail');
        let nextProductIndex = jQuery('.owl-detail .owl-item:not(.cloned)').length;
        let isLoadOtherVersion = false;
        if (nextProductIndex > 0) {
            // slider has at least 1 image
            // get last product image
            const lastProductImage = jQuery('.owl-detail .owl-item:not(.cloned)').last();
            // check if it is product version image and not this version
            isLoadOtherVersion = lastProductImage.find('> div').data('version') !== product.version;
            if (lastProductImage.length > 0 && lastProductImage.find('> div').hasClass('product-version-image') && isLoadOtherVersion) {
                // decrease next product index to current max product index
                --nextProductIndex;
                // remove last main slider image
                imageSlider.trigger('remove.owl.carousel', [nextProductIndex]);
            }
        }

        if (isLoadOtherVersion) {
            // remove current product image active class
            jQuery('.owl-detail .owl-item.active').removeClass('active');

            // add product version image to the end of product image slider
            const productImageHtml = `<div class="product-version-image item" data-version="${product.version}">
                <img src="${product.image}" alt="${product.image_alt}" class="img-fluid">
            </div>`;
            imageSlider.trigger('add.owl.carousel', [productImageHtml]);
            // refresh image slider
            imageSlider.trigger('refresh.owl.carousel');
        } else {
            // not insert this product version image => decrease next product index
            --nextProductIndex;
        }

        // go to product version image slide
        imageSlider.trigger('to.owl.carousel', [nextProductIndex]);
    }
}

function validateSerialize() {
    var invalidFields = jQuery('#add-to-cart').find( ":invalid" );
    if (invalidFields.length > 0) return false;
    return true;
}