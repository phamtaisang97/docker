$(document).ready(function() {
    // var tab_product_detail = $('#product-detail-page .product-detail #myTabContent .tab-pane .tab-content-description');
    // var collapse_tab_product_detail = $('#product-detail-page .product-detail #myTabContent .dv-collapse .btn-collapse');
    // var getheight_tab_product_detail = tab_product_detail.height();
    
    // if (getheight_tab_product_detail > 215) {
    //     tab_product_detail.addClass("hide");
    // }
    
    // collapse_tab_product_detail.on('click', function() {
    //     if (tab_product_detail.hasClass("hide")) {
    //         tab_product_detail.removeClass("hide");
    //         collapse_tab_product_detail.addClass("hide");
    //     }
    // });
    
    const diffslide_hot_products = $("#diffslide-hot-products");
    const difflist_sale_off_products = $("#difflist-sale-off-products");
    // const producdetailtpage_add_to_cart_action_checkout = $(".add-to-cart-action-checkout");
    const footer_logo = $("footer .logo-footer");
    const homepage_description_store = $("#homepage-description-store");
    const shoppage_title = $("#products-page #shoppage-title");
    const shoppage = $("#products-page");
    
    if (shoppage.length > 0) {
        var get_sidebarright = shoppage.find('.sidebar-right');
        var get_contentleft = shoppage.find('.content-left');
        var get_shoppage_category = get_sidebarright.attr("data-category");
        var get_shoppage_collection = get_sidebarright.attr("data-collection");
        var get_shoppage_supplier = get_sidebarright.attr("data-supplier");
        var get_shoppage_price = get_sidebarright.attr("data-price");
        const visible_attribute = get_sidebarright.data('attribute');
        const visible_tag = get_sidebarright.data('tag');
        if (get_shoppage_category == 0 && get_shoppage_collection == 0 && get_shoppage_supplier == 0 && get_shoppage_price == 0 && 0 == visible_attribute && 0 == visible_tag) {
            get_sidebarright.addClass("d-none");
            get_contentleft.attr("style", "flex: 0 0 100%; max-width: 100%;");
        }
    }
    
    if (shoppage_title.length > 0) {
        var result = "";
        var product_collection = $("#products-page #product-collection nav a.nav-link.active");
        var product_category = $("#products-page #product-category nav a.nav-link.active");
        
        if (product_collection.length > 0) {
            if (product_category.length > 0) {
                result = product_collection.attr("data-title") + " > " + product_category.attr("data-title");
            } else {
                result = product_collection.attr("data-title");
            }
        } else {
            if (product_category.length > 0) {
                result = product_category.attr("data-title");
            } else {
                result = "Danh sách Sản phẩm";
            }
        }
        shoppage_title.html(result);
    }
    
    if(homepage_description_store.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_common/blog_first',
            success: function(response) {
                var result = response.result;
                var title = homepage_description_store.find(".head h3");
                var content = homepage_description_store.find(".body");
                var hds_view_detail = homepage_description_store.find(".hds-view-detail");

                title.html((result.title.length > 20) ? result.title.slice(0, 20) + '...' : result.title);
                content.html((stripHtml(result.short_content).length > 400) ? stripHtml(result.short_content).slice(0, 400) + '...' : stripHtml(result.short_content));
                hds_view_detail.attr('href', result.link);
            }
        });
    }
    
    function stripHtml(html){
        // Create a new div element
        var temporalDivElement = document.createElement("div");
        // Set the HTML content with the providen
        temporalDivElement.innerHTML = html;
        // Retrieve the text property of the element (cross-browser support)
        return temporalDivElement.textContent || temporalDivElement.innerText || "";
    }
    
    if(footer_logo.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_common/logo',
            success: function(response) {
                var image_logo = footer_logo.find(".image");
                image_logo.attr("style", "background-image: url(" + response.logo + ")");
            }
        });
    }
    
    // if(producdetailtpage_add_to_cart_action_checkout.length > 0) {
    //     var amountcart_header = $('header#header-main span.quantity');
    //     var amountproduct = $('#product-detail-page form#add-to-cart #input-amount-add-to-cart');
        
    //     producdetailtpage_add_to_cart_action_checkout.on('click', function() {
    //         var get_amountcart_header = amountcart_header.html();
    //         var get_amountproduct = amountproduct.val();
            
    //         var new_value_amount_cart = parseInt(get_amountcart_header) + parseInt(get_amountproduct);
            
    //         amountcart_header.html(new_value_amount_cart);
    //     });
    // }
    
    if(diffslide_hot_products.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_products/products',
            success: function(response) {
                var arr_products = response.result;
                var getcontainer_slide_hot = $('#diffslide-hot-products #diffslide-products-carousel .carousel-inner');
                getcontainer_slide_hot.html('');
                
                var i = 0;
                arr_products.forEach(function(item) {
                    if (item.is_best_sale_product && item.product_in_stock) {
                        getcontainer_slide_hot.append(create_hot_products(item, i));
                        i++;
                    }
                });
            }
        });
    }
    
    if(difflist_sale_off_products.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_products/products',
            success: function(response) {
                var arr_products = response.result;
                var getcontainer_list_sale_off = $('#difflist-sale-off-products #list-sale_off-products');
                getcontainer_list_sale_off.html('');
                
                var i = 0;
                arr_products.forEach(function(item) {
                    if (item.percent_protduct != "" && item.product_in_stock && i <= 5) {
                        getcontainer_list_sale_off.append(create_sale_off_products(item));
                        i++;
                    }
                });
            }
        });
    }
    
    
    function create_hot_products(item, index) {
        var element_1 = "<div class='carousel-item text-center " + ((index === 0) ? 'active':'') + "'><div class='col-12 slide-product-item'><div class='product-info'>";
        var element_2 = (item.percent_protduct === "") ? "" : "<div class='product-highlight text-center'><span class='sale'>" + item.percent_protduct + "</span></div>";
        var element_3 = "<a href='" + item.href + "' title='" + item.name + "'><div class='product-image' style='background-image: url(" + item.thumb + ");'></div></a>";
        var element_4 = "<div class='product-name my-3'><a href='" + item.href + "' title='" + item.name + "' class='product-title'>" + (item.name.length > 25) ? item.name.slice(0, 25) : item.name + "</a></div>";
        var element_5 = "<span class='product-price'><span class='price'>" + item.price + "</span></span>";
        var element_6 = "<span class='product-price-sale'><span class='price'>" + item.compare_price + "</span></span>";
        var element_7 = "</div></div></div>";
        
        return element_1 + element_2 + element_3 + element_4 + element_5 + element_6 + element_7;
    }
    
    function create_sale_off_products(item) {
        var element_1 = "<a href='" + item.href + "'><div class='d-flex list-product-item my-3'>";
        var element_2 = "<div class='col-4 product-image' style='background-image: url(" + item.thumb + ")'></div>";
        var element_3 = "<div class='col-8 product-info'>";
        var element_4 = "<h5 class='product-title'>" + (item.name.length > 25) ? item.name.slice(0, 25) : item.name + "</h5>";
        var element_5 = "<p class='product-price'>" + item.price + "</p><p class='product-price-sale'>" + item.compare_price + "</p>";
        var element_6 = "</div></div></a>";
        
        return element_1 + element_2 + element_3 + element_4 + element_5 + element_6;
    }
});