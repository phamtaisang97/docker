$(document).ready(function() {

    $(document).scroll(function(e){
        var topHeaderHeight = $('header .fix-content').outerHeight();
        if( $('.block-header').length === 0 ) return;
        var elementOffsetTop = $('.block-header').height();
        var windowScrollTop = (window.pageYOffset || document.scrollTop)  - (document.clientTop || 0) + 10;
        if(windowScrollTop >= elementOffsetTop && !$('.block-header>.fix-content').hasClass('fixed')){
            $('.block-header>.fix-content').addClass('fixed');
        }
        else if(windowScrollTop < elementOffsetTop && $('.block-header>.fix-content').hasClass('fixed')){
            if (!$('.block-header>.fix-content').hasClass('is-home')) {
                $('.block-header>.fix-content').removeClass('fixed');
            }
        }

        if( $('.product-teaser-fix').length === 0 ) return;
        if ($('.fix-content').hasClass('fixed') && windowScrollTop >= elementOffsetTop + 330) {
            $('.product-teaser-fix').slideDown(300);
            $('.product-teaser-fix').css('top', topHeaderHeight+"px")
        } else {
            $('.product-teaser-fix').slideUp(300);
        }
    })

    function owlInit(element, numItem, nav = true, dots = false)
    {
        if(element.length) {
            element.owlCarousel({
                loop:false,
                responsiveClass:true,
                dots: dots,
                nav:nav,
                navText: ['<i class="icon icon-chevron-left"></i>', '<i class="icon icon-chevron-right"></i>'],
                URLhashListener:true,
                startPosition: 'URLHash',
                responsive:{
                    0:{
                        items: numItem,
                    },
                    480: {
                        items: numItem,
                    },
                    600:{
                        items:Math.round(numItem/2),
                    },
                    1000:{
                        items:numItem,
                    }
                }
            });
        }
    }

    if($('.block-banner .owl-home-banner').length) {
        $('.block-banner .owl-home-banner').owlCarousel({
            items:1,
            loop:true,
            margin:10,
            autoplay:true,
            autoplayTimeout:$('.block-banner .owl-home-banner').attr('data-interval'),
            autoplayHoverPause:true
        });
    }

    // owlInit($('.block-banner .owl-home-banner'), 1, false, true);
    owlInit($('.block-saleoff-product .owl-product'), $('.block-saleoff-product .owl-product').attr('data-number-item'));
    owlInit($('.block-top-sale-product .owl-product'), $('.block-top-sale-product .owl-product').attr('data-number-item'));
    owlInit($('.block-new-product .owl-product'), $('.block-new-product .owl-product').attr('data-number-item'));
    owlInit($('.block-top-sale-product .owl-best-sales'), $('.block-top-sale-product .owl-best-sales').attr('data-number-item'));

    // Product filter form
    $('.product-list-filter').on('click', '.dropdown .dropdown-item', function(e){
        var dataValue = $(this).attr('data-value');
        var parent = $(this).closest('.dropdown');
        parent.find('.dropdown-item.active').removeClass('active');
        $(this).addClass('active');
        parent.find('input[type="hidden"]').val(dataValue).change();
        parent.find('>.dropdown-toggle').text($(this).text());
        e.preventDefault();
    })

    //add owl partner
    if (jQuery('.block-partner .owl-partner').length) {
        var limit_inline = jQuery('.block-partner .owl-partner').attr('data-limit-inline');
        jQuery('.block-partner .owl-partner').owlCarousel({
            nav:false,
            startPosition: 'URLHash',
            loop: jQuery('.owl-partner').attr('data-loop') == 1 ? true : false,
            autoplay: jQuery('.owl-partner').attr('data-auto-play') == 1 ? true : false,
            dots: false,
            responsive:{
                0:{
                    items:1,
                },
                600:{
                    items:limit_inline/2,
                },
                1000:{
                    items:limit_inline,
                }
            }
        });
    }
    ///////////////////////////////////////

    // Product detail
    owlInit($('.product-detail .owl-product'), 1);
    owlInit($('.block-relate-product .owl-product'), $('.block-relate-product .owl-product').attr('data-number-item'));
    //////////////////////////////////////////////

    // Ô nhập số lượng
    $(document).on('click', '.quantity-group-input>button', function(e) {
        var val = parseInt($(this).parent().find('>input').val());
        var min = parseInt($(this).parent().find('>input').attr('min'));
        if (isNaN(val)) val = 0;
        if (isNaN(min)) min = 0;
        if( val > min && $(this).hasClass('decrease') ){
            val--;
        }
        if( $(this).hasClass('increase') ){
            val++;
        }
        $(this).parent().find('>input').val(val).change();
        e.preventDefault();
    })

    // Show password
    $(document).on('click', '.input-password-group button.show-password', function(e) {
        var input = $(this).parent().find('>input');
        $(this).toggleClass('active');
        if ($(this).hasClass('active')){
            input.attr('type', 'text');
        } else {
            input.attr('type', 'password');
        }
    });

    // Menu responsive
    $('.block-header .block-header-menu ul.nav > li.has-child > a>.toggle').on('click', function(e){
        $(this).closest('li').toggleClass('active')
        e.preventDefault();
        return false;
    })

    jQuery('.quantity').each(function () {
        var spinner = jQuery(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min'),
            max = input.attr('max');

        btnUp.click(function () {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

        btnDown.click(function () {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

    });
});

// sticky
$(document).ready(function() {
    $(window).scroll(function() {
        var topHeaderHeight = $('header .main-header').outerHeight();
        if (550 <= $(this).scrollTop()) {
            $('#buy-now').addClass("fixed");
            $('#buy-now').css('top', topHeaderHeight+"px")
        } else {
            $('#buy-now').removeClass("fixed");
        }
    });
});