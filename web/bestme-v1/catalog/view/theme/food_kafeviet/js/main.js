function owlInit(elements, numItem, nav = true, dots = false, margin = 0, responsive = null, loop = true) {
    if (elements.length) {
        elements.each(function () {
            const element = $(this);
            const interval = element.data('interval');
            const autoplayTime = undefined === element.data('autoplay_time') ? interval : element.data('autoplay_time') * 1e3
            element.owlCarousel({
                items: numItem,
                autoplay: undefined === element.data('autoplay') ? typeof interval !== 'undefined' : 1 == element.data('autoplay'),
                autoplayTimeout: autoplayTime,
                loop: undefined === element.data('loop') ? loop : 1 == element.data('loop'),
                autoHeight: true,
                autoWidth: false,
                responsiveClass: true,
                lazyLoad: true,
                dots: dots,
                nav: nav,
                margin: margin,
                navText: ['<i class="icon icon-arrow-left"></i>', '<i class="icon icon-arrow-right"></i>'],
                responsive: false
            });
        })
    }
}

function isEmpty(variable) {
    return undefined === variable || '' === variable || (Array.isArray(variable) && 0 === variable.length) || false === variable || null === variable;
}

function htmlEntityDecode(input) {
    const entities = {
        "&amp;": "&",
        "&lt;": "<",
        "&gt;": ">"
    };

    for (const prop in entities) {
        if (entities.hasOwnProperty(prop)) {
            input = input.replace(new RegExp(prop, "g"), entities[prop]);
        }
    }
    return input;
}

jQuery(document).ready(function ($) {
    /* init product slider */
    owlInit($('.owl-product').not('.owl-collection-product'), 1, false);

    // Menu Mobile
    const menuId = '.navbar-collapse.collapse';
    if ($(menuId).length > 0) {
        $(menuId).after('<div id="overlay"></div>');
        $('button.navbar-toggler').click(function () {
            $('#overlay').toggleClass('show');
        });

        $(menuId + ' i.close').click(function () {
            $('#overlay').removeClass('show');
            $(this).closest(menuId).removeClass('show');
        });
        $(window).click(function (e) {
            if ($(e.target).attr('id') === 'overlay') {
                $('#overlay').removeClass('show');
                $(menuId + '.show').removeClass('show');
            }
        });
    }

    // Password show/hide
    $(document).on('click', '.password-hint .icon', function () {
        let inputPass = $(this).parent().find('input');
        if ($(this).hasClass('icon-eye')) {
            $(this).removeClass('icon-eye').addClass('icon-no-eye');
            inputPass.attr('type', 'text');
        } else {
            $(this).addClass('icon-eye').removeClass('icon-no-eye');
            inputPass.attr('type', 'password');
        }
    });


    // Header menu fix scroll
    const topHeaderHeight = $('.header-top').height();
    const headerWrap = '.block-header';

    function fixedMenu() {
        if ($(headerWrap).length === 0 || $('body').height() < $(window).height() * 1.5) return;
        let windowScrollTop = (window.pageYOffset || document.scrollTop) - (document.clientTop || 0);

        if (windowScrollTop >= topHeaderHeight && !$(headerWrap).hasClass('fixed')) {
            $(headerWrap).addClass('fixed');
        } else if (windowScrollTop <= topHeaderHeight && $(headerWrap).hasClass('fixed')) {
            $(headerWrap).removeClass('fixed');
        }
    }

    fixedMenu();
    const productTeaser = '.product-teaser-fix';
    $(document).scroll(function () {
        fixedMenu();

        // Box buy-product in product detail page
        let windowScrollTop = (window.pageYOffset || document.scrollTop) - (document.clientTop || 0);
        if ($(productTeaser).length > 0) {
            if ($(headerWrap).hasClass('fixed') && windowScrollTop >= topHeaderHeight + 500) {
                $(productTeaser).slideDown(300);
            } else {
                $(productTeaser).slideUp(300);
            }
        }
    });

    /*Set an element Full width*/
    const fullWidtElm = '.full-width';

    function sectionFullWidth() {
        let bodyWidth = $('body').width();
        let originWidth = $('.container').width();
        $(fullWidtElm).css({
            'width': bodyWidth + 'px',
            'max-width': bodyWidth + 'px',
            'margin-left': -(bodyWidth - originWidth) / 2 + 'px'
        });
    }

    sectionFullWidth();
    $(window).resize(function (e) {
        sectionFullWidth()
    });
    /*End Set an element Full width*/

    // input quantity calculate
    $('input.custom-number-input').each(function (index, el) {
        var wrapper = document.createElement("div");
        wrapper.classList = "custom-number-input-wrapper";

        var increase = document.createElement("button");
        increase.innerHTML = '+';
        increase.classList = "increase";
        var decrease = document.createElement("button");
        decrease.innerHTML = '-';
        decrease.classList = "decrease";

        $(wrapper).append(decrease);
        $(wrapper).append($(this).clone());
        $(wrapper).append(increase);
        $(this).replaceWith($(wrapper));
    });

    $(document).on('click', '.custom-number-input-wrapper >button', function (e) {
        e.preventDefault();
        var input = $(this).parent().find('input');
        var input_val = (input.val()) ? parseInt(input.val()) : 0;
        var min = input.attr('min');
        var max = input.attr('max');
        var step = typeof input.attr('step') !== "undefined" ? input.attr('step') : 1;
        if ($(this).hasClass('increase')) {
            input_val = input_val + 1;
        } else {
            input_val = input_val - step;
        }
        if (input_val < min && typeof min !== "undefined") input_val = min;
        if (input_val > max && typeof max !== "undefined") input_val = max;
        input.val(input_val).change();
    });

    // Update cart item quantity in cart page
    const currencyUnit = ' đ';
    const cartElm = '.cart-table';

    function CalculateCartTotal() {
        let total_price = 0;
        $(cartElm + ' .row-item').each(function () {
            if ($(this).find('.total-price[data-value]').length) {
                total_price += parseInt($(this).find('.total-price[data-value]').attr('data-value'));
            }
        });

        let total_transport = parseInt($('.cart-summary .total-transport').attr('data-value'));
        let total_tax = parseInt($('.cart-summary .total-tax').attr('data-value'));
        let total_cart = total_transport + total_tax + total_price;

        $('.cart-summary .total-price').text($.number(total_price, 0, '', '.'));
        $('.cart-summary .total-cart').text($.number(total_cart, 0, '', '.'));
    }

    $(cartElm).on('change', '.custom-number-input-wrapper >input', function (e) {
        $(this).number(true, 0, '', '');
        let price = $(this).closest('.row-item').find('.price[data-value]').attr('data-value');
        price = parseInt(price) * parseInt($(this).val());
        let row_price = $(this).closest('.row-item').find('.total-price[data-value]');
        row_price.text($.number(price, 0, '', '.') + currencyUnit).attr('data-value', price);

        CalculateCartTotal();
    });
    // Update cart item quantity in cart page

    let youtubeUrl = false;
    $('.modal-youtube-video').on('shown.bs.modal', function() {
        youtubeUrl= youtubeUrl ? youtubeUrl : $(this).find('iframe').attr('src');
        $(this).find('iframe').attr('src', youtubeUrl);
    })
    $('.modal-youtube-video').on('hide.bs.modal', function() {
        $(this).find('iframe').attr('src', '');
    })

    /* header login, register */
    $(document).on('submit', '#form-login-header', function (e) {
        e.preventDefault();

        $.ajax({
            url: "/index.php?route=api/v1_common/login",
            method: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {
                if (response.status) {
                    location.reload();
                } else {
                    $('#login-header-message').show().find('span').text(response.message);
                }
            }
        });
    });

    $(document).on('submit', '#form-register-header', function (e) {
        e.preventDefault();

        $.ajax({
            url: "/index.php?route=api/v1_common/register",
            method: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {
                if (response.status) {
                    window.location.href = response.redirect_url;
                } else {
                    $('#register-header-message').show().find('span').text(response.message);
                }
            }
        });
    });

    $(document).on('submit', '#form-forgot-password-header', function (e) {
        e.preventDefault();

        $.ajax({
            url: "/index.php?route=api/v1_common/forgotPassword",
            method: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {
                if (response.status) {
                    window.location.href = response.redirect_url;
                } else {
                    $('#forgot-password-header-message').show().find('span').text(response.message);
                }
            }
        });
    });

    $(document).on('click', '.header-user-modal .alert-dismissible .btn-close', function(e) {
        $(this).closest('.alert-dismissible').hide();
    })

    // Menu responsive
    $(document).on('click', '.block-header .main-header ul.nav > li.has-child > a>.toggle', function (e) {
        $(this).closest('li').toggleClass('active');
        e.preventDefault();
        return false;
    });
    $(document).on('click', '.block-header .main-header ul.nav > li.has-child > .toggle', function (e) {
        $(this).closest('li').toggleClass('active');
        e.preventDefault();
        return false;
    });

});