// init function
const homeVideo = {
    init: loadYouTubePlayer
};

function loadYouTubePlayer(params) {
    if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {
        // create youtube iframe script
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        window.onYouTubePlayerAPIReady = function () {
            onInitYouTubePlayer(params);
        };
    }
}

/**
 *
 * @param {object} params
 * sliderType: bootstrap_carousel | owl_carousel | swiper | slick
 * videoCarouselElements
 * postThumbnailElements
 * slideshowSelector
 * slideshowHeight (optional)
 * selectorToDetectHeight (optional)
 * timeOutToDetectHeight (optional)
 */
function onInitYouTubePlayer(params) {
    if (!params) { // ignore when params is undefined
        return;
    }

    let sliderPlayer = [];
    let blogPlayer = [];
    let slideshowHeight = params.hasOwnProperty('slideshowHeight') ? params.slideshowHeight : '100%';
    let timeOutToDetectHeight = params.hasOwnProperty('timeOutToDetectHeight') ? params.timeOutToDetectHeight : 0;


    // parse video params
    let slideshowElement, slideshowSliderEvent;
    switch (params.sliderType) {
        case 'bootstrap_carousel':
            slideshowElement = $(params.slideshowSelector);
            slideshowSliderEvent = 'slide.bs.carousel';
            break;
        case 'owl_carousel':
            slideshowElement = $(params.slideshowSelector);
            slideshowSliderEvent = 'changed.owl.carousel';
            break;
        case 'swiper':
            jQuery(document).ready(function () {
                slideshowElement = document.querySelector(params.slideshowSelector).swiper
            });
            slideshowSliderEvent = 'slideChange';
            break;
        case 'slick':
            slideshowElement = $(params.slideshowSelector);
            slideshowSliderEvent = 'afterChange';
            break;
    }

    jQuery(document).ready(function () {
        setTimeout(function () {
            if(params.hasOwnProperty('selectorToDetectHeight')) {
                let items = $(params.selectorToDetectHeight);
                slideshowHeight = 0;

                for(let i = 0; i< items.length; i++) {
                    if(items.eq(i).height() > slideshowHeight) {
                        slideshowHeight = items.eq(i).height();
                    }
                }
                if(slideshowHeight == 0) {
                    slideshowHeight = 450;
                }
                slideshowHeight += 'px';
            }

            for (let i = 0; i < params.postThumbnailElements.length; i++) {
                blogPlayer.push(new YT.Player(params.postThumbnailElements[i], {
                    height: '100%',
                    width: '100%',
                    videoId: detectYoutubeUrl(params.postThumbnailElements.eq(i).data('video-url')),
                    events: {
                        'onStateChange': onBlogPlayerStateChange
                    }
                }));
            }

            for (let j = 0; j < params.videoCarouselElements.length; j++) {
                sliderPlayer.push(new YT.Player(params.videoCarouselElements[j], {
                    height: slideshowHeight,
                    width: '100%',
                    videoId: detectYoutubeUrl(params.videoCarouselElements.eq(j).data('video-url')),
                    events: {
                        'onStateChange': onSliderPlayerStateChange
                    }
                }));
            }

            if (slideshowElement) {
                slideshowElement.on(slideshowSliderEvent, function (e) {
                    for (let i in sliderPlayer) {
                        if (sliderPlayer[i].pauseVideo) {
                            sliderPlayer[i].pauseVideo();
                        }
                    }
                })
            }
        }, timeOutToDetectHeight)
    });

    function onSliderPlayerStateChange(state) {
        /**
         * list states:
         * -1 – unstarted
         *  0 – ended
         *  1 – playing
         *  2 – paused
         *  3 – buffering
         *  5 – video cued
         */

        switch (params.sliderType) {
            case 'bootstrap_carousel':
                if (state.data == 0) {
                    slideshowElement.carousel('next')
                }

                if (state.data == -1 || state.data == 3 || state.data == 1) {
                    slideshowElement.carousel('pause');
                }

                if (state.data == 2 || state.data == 0) {
                    slideshowElement.carousel({
                        pause: false
                    });
                }

                break;

            case 'owl_carousel':
                if (state.data == 0) {
                    slideshowElement.trigger('next.owl.carousel');
                }

                if (state.data == -1 || state.data == 3 || state.data == 1) {
                    slideshowElement.trigger('stop.owl.autoplay')
                }

                if (state.data == 2 || state.data == 0) {
                    let time = slideshowElement.data('time')
                    slideshowElement.trigger('play.owl.autoplay', [Math.floor(time/1000) > 0 ? time : time * 1000]);
                }

                break;

            case 'swiper':
                if (state.data == 0) {
                    slideshowElement.slideNext();
                }

                if (state.data == -1 || state.data == 3 || state.data == 1) {
                    slideshowElement.autoplay.stop();
                }

                if (state.data == 2 || state.data == 0) {
                    slideshowElement.autoplay.start();
                }

                break;

            case 'slick':
                if (state.data == 0) {
                    slideshowElement.slick('slickNext');
                }

                if (state.data == -1 || state.data == 3 || state.data == 1) {
                    slideshowElement.slick('slickPause');
                }

                if (state.data == 2 || state.data == 0) {
                    slideshowElement.slick('slickPlay');
                }

                break;
        }

        if (state.data === 1) {
            pauseAllExcept(blogPlayer);
            pauseAllExcept(sliderPlayer, jQuery(state.target.getIframe()).attr('id'));
        }
    }

    function onBlogPlayerStateChange(state) {
        if (state.data === 1) {
            pauseAllExcept(blogPlayer, jQuery(state.target.getIframe()).attr('id'));
            pauseAllExcept(sliderPlayer);
        }
    }

    function pauseAllExcept(embedElements, elementId = null) {
        for (let i = 0; i < embedElements.length; i++) {
            if (elementId && jQuery(embedElements[i].getIframe()).attr('id') === elementId) {
                continue;
            } else {
                embedElements[i].pauseVideo();
            }
        }
    }
}

function detectYoutubeUrl(urlString) {
    let url = new URL(urlString);
    if (urlString.indexOf('/embed/') > -1) {
        return urlString.split('/')[urlString.split('/').length - 1]
    } else if (urlString.indexOf('youtu.be') > -1) {
        return url.pathname.slice(1)
    } else if (urlString.indexOf('youtube.com') > -1) {
        return url.searchParams.get("v");
    }

    return false;
}