// init function
const blogDetailVideo = {
    init: loadYouTubePlayer
};

function loadYouTubePlayer(params) {
    if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {
        // create youtube iframe script
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        window.onYouTubePlayerAPIReady = function () {
            onInitYouTubePlayer(params);
        };
    }
}

/**
 *
 * @param {object} params
 * videoElementId
 * videoHeight
 * currentTheme
 */
function onInitYouTubePlayer(params) {
    if (!params) { // ignore when params is undefined
        return;
    }

    // list custom themes
    const customThemes = ['tech_sun_electro']

    let youtubePlayerConfig = {
        height: params.videoHeight,
        width: '100%',
        videoId: detectYoutubeUrl($("#" + params.videoElementId).data("video-url"))
    };

    const isCustomVideoTheme = customThemes.includes(params.currentTheme)
    if (isCustomVideoTheme) {
        youtubePlayerConfig.events = {
            'onStateChange': onPlayerStateChange
        };

        function onPlayerStateChange(state) {
            if (window.matchMedia('(min-width: 992px)').matches) {
                if (state.data == 2 || state.data == 0) {
                    $('.banner-header').show();
                }

                if (state.data == 1) {
                    $('.banner-header').hide();
                }
            }
        }
    }

    if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }

    var player = new YT.Player(params.videoElementId, youtubePlayerConfig);

    // script for custom themes
    if (isCustomVideoTheme && 'tech_sun_electro' === params.currentTheme) {
        $('.banner-header').click(function () {
            if (window.matchMedia('(min-width: 992px)').matches) {
                player.playVideo();
            }
        })
    }
}

function detectYoutubeUrl(urlString) {
    let url = new URL(urlString);
    if (urlString.indexOf('/embed/') > -1) {
        return urlString.split('/')[urlString.split('/').length - 1]
    } else if (urlString.indexOf('youtu.be') > -1) {
        return url.pathname.slice(1)
    } else if (urlString.indexOf('youtube.com') > -1) {
        return url.searchParams.get("v");
    }

    return false;
}