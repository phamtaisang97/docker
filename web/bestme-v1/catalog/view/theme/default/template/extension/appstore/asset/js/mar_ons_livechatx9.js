$(function () {
    var url = "index.php?route=extension/appstore/mar_ons_livechatx9/getCode";
    $.ajax({
        url: url,
        dataType: 'json',
        success: function (data) {
            $('head').append(htmlDecode(data));
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
});

function htmlDecode(input){
    var e = document.createElement('div');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}