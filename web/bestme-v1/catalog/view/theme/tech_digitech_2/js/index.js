function resizeHeightElementRatio(elSelector, ratio = 1) {
    const element = jQuery(elSelector);
    if (element.length) {
        element.height(element.width() / ratio);
    }
}

$(document).ready(function() {
    var arr_product = $( ".content-result" ).find( ".item-product .img-pro");
    resizeHeightElementRatio(arr_product);

    $('.toogle-search').hover(function(e) {
        // e.preventDefault();
        $('.toogle-search').css('visibility', 'hidden');
        $(this).next().addClass('show');
    }, function() {

    });
    $("body").click(function(e) {
        // e.stopPropagation();
        $('.search-sticky').removeClass('show');
        $('.toogle-search').css('visibility', 'visible');
    });
    $(".search-sticky").click(function(e) {
        e.stopPropagation();
    });
    $('.btn-showpass').click(function(e) {
        $(this).toggleClass('show');
        if ($('.form-group').hasClass('show')) {
            $('.input-pass').attr('type', 'text');
        }
    });

    $('.heart-pro').click(function(e) {
        e.preventDefault();
        $(this).toggleClass('active');
    });
    $('.show-login').click(function(e) {
        e.preventDefault();
        $(".sub-dropdown").slideToggle();
    })
    

    /** Menu mobile **/
    $(".accodition").click(function() {
        $(this).next().slideToggle();
        $(this).toggleClass('rotate');
    });
    $(".open-sidemenu").click(function() {
        $('#sidenav').toggleClass('menu-mobile');
        $('.block-overlay').toggleClass('over');
        $('body').toggleClass('no-scroll');
        $('.block-header-sale').addClass('slow-layer');
    });

    // filter
    $(".btn-filter-product").click(function() {
        $('#filer-on-mobile').addClass('menu-mobile');
        $('.block-overlay').addClass('over');
        $('body').addClass('no-scroll');
        $('.block-header-sale').addClass('slow-layer');
    });

    $(".block-overlay").click(function() {
        $('#sidenav').removeClass('menu-mobile');
        $('#filer-on-mobile').removeClass('menu-mobile');
        $(this).removeClass('over');
        $('body').removeClass('no-scroll');
        $('.block-header-sale').removeClass('slow-layer');
    });
    /** Menu mobile **/

    /** Filter mobile **/
    $(".btn-filter-product").click(function() {
        // $('#block-filter').toggleClass('menu-mobile');
        $('.block-overlay-filter').toggleClass('over');
        // $('body').toggleClass('no-scroll');
        // $('.block-header-sale').addClass('slow-layer');
    });
    /** Filter mobile **/

    $(function() {
        var star = '.star',
            selected = '.selected';

        $(star).on('click', function() {
            $(selected).each(function() {
                $(this).removeClass('selected');
            });
            $(this).addClass('selected');
        });

    });

    $(".block-category .ttl-category").click(function(e) {
        $(".list-category").slideToggle();
        e.stopPropagation();
    });

    $(".block-category .list-category").click(function(e) {
        e.stopPropagation();
    });

    $("html").click(function() {
        $(".list-category").slideUp();
        $(".search-mobile .form-group").removeClass("on");
    });

    //click show search mobile 
    $(".show-search-mb").click(function(e) {
        e.stopPropagation();
        $(".search-mobile .form-group").toggleClass("on");
    });
    $(".search-mobile .form-group").click(function(e) {
        e.stopPropagation();
    });
});
// // change quanlity
$(document).ready(function () {
    jQuery('.quantity').each(function () {
        var spinner = jQuery(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min'),
            max = input.attr('max');

        btnUp.click(function () {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

        btnDown.click(function () {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

    });
});
$('.owl-banner').owlCarousel({
    loop: true,
    margin: 10,
    autoplay: true,
    nav: true,
    dots: true,
    dotsSpeed: 1000,
    autoplaySpeed: 1000,
    navSpeed: 800,
    responsive: {
        0: {
            items: 1
        }
    }
});

$('.owl-product-02')
    .owlCarousel({
    loop: false,
    margin: 10,
    nav: true,
    // autoplay: true,
    nav: true,
    dots: true,
    dotsSpeed: 1000,
    autoplaySpeed: 2000,
    navSpeed: 800,
    autoplayTimeout: 6000,
    responsive: {
        0: {
            items: 1
        }
    }
});
const autoplayTime = undefined === jQuery('.owl-product').data('autoplay_time') ? 5 : jQuery('.owl-product').data('autoplay_time');
$('.owl-product')
    .on("initialized.owl.carousel", function (e) {
        resizeHeightElementRatio('.owl-carousel.owl-product .owl-item img');
    })
    .owlCarousel({
    loop: undefined !== jQuery('.owl-product').data('loop') && 1 == jQuery('.owl-product').data('loop'),
    autoplay: undefined !== jQuery('.owl-product').data('autoplay') && 1 == jQuery('.owl-product').data('autoplay'),
    autoplayTimeout: autoplayTime * 1e3,
    margin: 10,
    nav: true,
    // autoplay: true,
    nav: true,
    dots: true,
    dotsSpeed: 1000,
    autoplaySpeed: 2000,
    navSpeed: 800,
    responsive: {
        0: {
            items: 1
        }
    }
});

$('.owl-product-03')
    .owlCarousel({
    loop: false,
    margin: 10,
    nav: true,
    // autoplay: true,
    nav: true,
    dots: true,
    dotsSpeed: 1000,
    autoplaySpeed: 2000,
    navSpeed: 800,
    autoplayTimeout: 4000,
    responsive: {
        0: {
            items: 1
        }
    }
});

// menu
(function($) {
    'use strict';

    var defaults = {
        topOffset: 400, //px - offset to switch of fixed position
        hideDuration: 300, //ms
        stickyClass: 'is-fixed'
    };

    $.fn.stickyPanel = function(options) {
        if (this.length == 0) return this; // returns the current jQuery object

        var self = this,
            settings,
            isFixed = false, //state of panel
            stickyClass,
            animation = {
                normal: self.css('animationDuration'), //show duration
                reverse: '', //hide duration
                getStyle: function(type) {
                    return {
                        animationDirection: type,
                        animationDuration: this[type]
                    };
                }
            };

        // Init carousel
        function init() {
            settings = $.extend({}, defaults, options);
            animation.reverse = settings.hideDuration + 'ms';
            stickyClass = settings.stickyClass;
            $(window).on('scroll', onScroll).trigger('scroll');
        }

        // On scroll
        function onScroll() {
            if (window.pageYOffset > settings.topOffset) {
                if (!isFixed) {
                    isFixed = true;
                    self.addClass(stickyClass)
                        .css(animation.getStyle('normal'));
                }
            } else {
                if (isFixed) {
                    isFixed = false;

                    self.removeClass(stickyClass)
                        .each(function(index, e) {
                            // restart animation
                            // https://css-tricks.com/restart-css-animation/
                            void e.offsetWidth;
                        })
                        .addClass(stickyClass)
                        .css(animation.getStyle('reverse'));

                    setTimeout(function() {
                        self.removeClass(stickyClass);
                    }, settings.hideDuration);
                }
            }
        }

        init();

        return this;
    }
})(jQuery);

$('.owl-partner').owlCarousel({
    loop: jQuery('.owl-partner').attr('data-loop') == 1 ? true : false,
    autoplay: jQuery('.owl-partner').attr('data-auto-play') == 1 ? true : false,
    margin: 50,
    nav: true,
    nav: true,
    navText: ["<img src='catalog/view/theme/tech_digitech/images/common/icon-prev.png'>", "<img src='catalog/view/theme/tech_digitech/images/common/icon-next.png'>"],
    dots: false,
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 3
        },
        1000: {
            items: jQuery('.owl-partner').attr('data-limit-inline')
        }
    }
});
const autoplayTime2 = undefined === jQuery('.owl-related').data('autoplay_time') ? 5 : jQuery('.owl-related').data('autoplay_time');
$('.owl-related')
    .on("initialized.owl.carousel", function (e) {
        resizeHeightElementRatio('.owl-carousel .owl-item img');
    })
    .owlCarousel({
    loop: undefined !== jQuery('.owl-related').data('loop') && 1 == jQuery('.owl-related').data('loop'),
    autoplay: undefined !== jQuery('.owl-related').data('autoplay') && 1 == jQuery('.owl-related').data('autoplay'),
    autoplayTimeout: autoplayTime2 * 1e3,
    margin: 30,
    nav: true,
    nav: true,
    dots: false,
    responsive: {
        0: {
            items: jQuery('.owl-related').attr('data-number-item')
        }
    }
});

$(document).ready(function() {
    var x = $("#block-header-top").offset().top + 20;
    $(window).scroll(function() {
        var windowScrollTop = (window.pageYOffset || document.scrollTop)  - (document.clientTop || 0) || 0;
        if ($(this).scrollTop() > x) {
            $('#block-header').addClass("fixed");
        } else {
            $('#block-header').removeClass("fixed");
        }

        var topHeight = $('#block-header').height() +36;

        if ($('#block-header').hasClass('fixed') && windowScrollTop >= x + 100) {
            $('.product-teaser-fix').css('top', topHeight+"px");
            $('.product-teaser-fix').slideDown(300);
        } else {
            $('.product-teaser-fix').slideUp(300);
        }
    });
});


// double owl
document.addEventListener("DOMContentLoaded", function() {
    console.log("start");
    let galleries = document.querySelectorAll(".gallery");

    Array.prototype.forEach.call(galleries, function(el, i) {
        const $this = $(el);
        const $owl1 = $this.find(".owl-1");
        const $owl2 = $this.find(".owl-2");
        let flag = false;
        let duration = 300;

        $owl1
            .on("initialized.owl.carousel", function (e) {
                resizeHeightElementRatio('.owl-item .img');
            })
            .owlCarousel({
                items: 1,
                lazyLoad: false,
                loop: false,
                dots: false,
                margin: 10,
                nav: false,
                responsiveClass: true
            })
            .on("changed.owl.carousel", function(e) {
                if (!flag) {
                    flag = true;
                    $owl2
                        .find(".owl-item")
                        .removeClass("current")
                        .eq(e.item.index)
                        .addClass("current");
                    if (
                        $owl2
                        .find(".owl-item")
                        .eq(e.item.index)
                        .hasClass("active")
                    ) {} else {
                        $owl2.trigger("to.owl.carousel", [e.item.index, duration, true]);
                    }
                    flag = false;
                }
            });

        $owl2
            .on("initialized.owl.carousel", function() {
                $owl2
                    .find(".owl-item")
                    .eq(0)
                    .addClass("current");
            })
            .owlCarousel({
                items: 4,
                lazyLoad: false,
                loop: false,
                margin: 10,
                nav: false,
                dots: false,
                responsiveClass: true
            })
            .on("click", ".owl-item", function(e) {
                e.preventDefault();
                var number = $(this).index();
                $owl1.trigger("to.owl.carousel", [number, duration, true]);
            });
    });
});