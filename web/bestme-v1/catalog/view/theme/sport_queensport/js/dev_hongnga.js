$(document).ready(function(){
    $("#carousel-category-indicators .carousel-item a").click(function(){
        $(this).addClass("active");
        $("#carousel-category-indicators .carousel-item a").each(function() {
            $("#carousel-category-indicators .carousel-item a").removeClass("active");
        });
    });

    const footer_logo = $("footer .logo-footer");
    const footer_map = $("footer .footer-map-store");
    const homepage_section_banner = $("#home-bg.homepage-main #section-banner-icon");
    const header_top_infor = $("#header-top-infor");
    const header_info_support_header = $("#info-support-header");

    if(header_info_support_header.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_common/contact',
            success: function(response) {
                var arr_info = response.result;

                header_info_support_header.find(".hotline-info").attr("href", "tel:" + arr_info.phone);
                header_info_support_header.find(".hotline-info p").html(arr_info.phone);
            }
        });
    }

    if(header_top_infor.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_common/social',
            success: function(response) {
                var arr_social = response.result.social_configs;

                arr_social.forEach(function(item) {
                    if(item.name == "Facebook" && item.visible == 1) {
                        header_top_infor.find(".header-banner-top-left .social-facebook").html("<a href='" + item.url + "' title='" + item.title + "' class='social-facebook'><i class='fab fa-facebook-f'></i></a>");
                    }
                    if(item.name == "Instagram" && item.visible == 1) {
                        header_top_infor.find(".header-banner-top-left .social-instagram").html("<a href='" + item.url + "' title='" + item.title + "' class='social-instagram'><i class='fab fa-instagram'></i></a>");
                    }
                    if(item.name == "Youtube" && item.visible == 1) {
                        header_top_infor.find(".header-banner-top-left .social-youtube").html("<a href='" + item.url + "' title='" + item.title + "' class='social-youtube'><i class='fab fa-youtube'></i></a>");
                    }
                });
            }
        });
        $.ajax({
            type: 'GET',
            url: '/api/v1_common/contact',
            success: function(response) {
                var arr_info = response.result;

                header_top_infor.find(".header-banner-top-right .email-info").attr("title", arr_info.address);
                header_top_infor.find(".header-banner-top-right .email-info").attr("href", "mailto:" + arr_info.address);
                header_top_infor.find(".header-banner-top-right .email-info span").html(arr_info.address);
            }
        });
    }

    if(homepage_section_banner.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_common/custom_contents',
            success: function(response) {
                var arr_ads_banner = response.display_customizes;
                var homepage_section_banner_contain = homepage_section_banner.find(".banner-contain");

                var html_banner = '';

                for (var i = 0; i < 3; i++) {
                    var ad_banner_item = homepage_section_banner.find(".banner-item-" + i);

                    switch(arr_ads_banner[i].type) {
                        case "fixed":
                            var element = "" +
                                "<div class='banner-item banner-item-fixed banner-item-" + i + " my-2'>" +
                                "    <div class='row'>" +
                                "        <div class='col-12 col-md-3 banner-bg'>" +
                                "            <img class='image' src='" + arr_ads_banner[i].content.icon + "' alt='" + arr_ads_banner[i].title + "'>" +
                                "        </div>" +
                                "        <div class='col-12 col-md-9 banner-content'>" +
                                "            <h5>" + arr_ads_banner[i].title + "</h5>" +
                                "            <p>" + arr_ads_banner[i].content.description + "</p>" +
                                "        </div>" +
                                "    </div>" +
                                "</div>";

                            html_banner += element;
                            break;

                        case "manual":
                            html_banner += decodeHtml("<div class='banner-item banner-item-manual my-2'>" + arr_ads_banner[i].content.html + "</div>");
                            break;

                        default:
                            var element = "" +
                                "<div class='banner-item banner-item-fixed banner-item-" + i + " my-2'>" +
                                "    <div class='row'>" +
                                "        <div class='col-12 col-md-3 banner-bg'>" +
                                "            <img class='image' src='" + arr_ads_banner[i].content.icon + "' alt='" + arr_ads_banner[i].title + "'>" +
                                "        </div>" +
                                "        <div class='col-12 col-md-9 banner-content'>" +
                                "            <h5>" + arr_ads_banner[i].title + "</h5>" +
                                "            <p>" + arr_ads_banner[i].content.description + "</p>" +
                                "        </div>" +
                                "    </div>" +
                                "</div>";

                            html_banner += element;
                    }

                    // ad_banner_item.find(".image").attr("src", arr_ads_banner[i].content.icon);
                    // ad_banner_item.find(".image").attr("alt", arr_ads_banner[i].title);

                    // ad_banner_item.find(".banner-content h5").html(arr_ads_banner[i].title);
                    // ad_banner_item.find(".banner-content p").html(arr_ads_banner[i].content.description);
                }

                homepage_section_banner_contain.html(html_banner);
            }
        });
    }

    function decodeHtml(str) {
        var map = {
            '&amp;': '&',
            '&lt;': '<',
            '&gt;': '>',
            '&quot;': '"',
            '&#039;': "'"
        };
        return str.replace(/&amp;|&lt;|&gt;|&quot;|&#039;/g, function(m) {return map[m];});
    }
    if(footer_map.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_common/address_map',
            success: function(response) {
                var footer_map_store = footer_map.find(".map-store");
                var parse_html_map = decodeHtml(response.result.address);
                footer_map_store.html(parse_html_map);
            }
        });
    }

    if(footer_logo.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_common/logo',
            success: function(response) {
                var image_logo = footer_logo.find(".image");
                image_logo.attr("style", "background-image: url(" + response.logo + ")");
            }
        });
    }

    const homepage_category_products = $("#homepage-category-products");
    const diffslide_hot_products = $("#difflist-hot-products");
    const shoppage_title = $("#products-page #shoppage-title");

    if (shoppage_title.length > 0) {
        var result = "";
        var product_collection = $("#products-page #product-collection nav a.nav-link.active");
        var product_category = $("#products-page #product-category nav a.nav-link.active");

        if (product_collection.length > 0) {
            if (product_category.length > 0) {
                result = product_collection.attr("data-title") + ", " + product_category.attr("data-title");
            } else {
                result = product_collection.attr("data-title");
            }
        } else {
            if (product_category.length > 0) {
                result = product_category.attr("data-title");
            } else {
                result = "Danh sách Sản phẩm";
            }
        }
        shoppage_title.html(result);
    }

    if(homepage_category_products.length > 0) {
        var category_item = $("#homepage-category-products #carousel-category-indicators .category-item");

        $.ajax({
            type: 'GET',
            url: '/api/v1_products/products',
            success: function(response) {
                var arr_products = response.result;
                var getcontainer_category_content = homepage_category_products.find('#category-content');
                getcontainer_category_content.html('');

                var get_category_id = $("#homepage-category-products #carousel-category-indicators .category-item.active").attr("data-category-id");
                var i = 0;
                arr_products.forEach(function(item) {
                    if (item.percent_protduct != "" && item.product_in_stock && i <= 8) {
                        if (Array.isArray(item.categories) && item.categories.length != 0) {
                            for (var j = 0; j < item.categories.length/2; j++) {
                                if (item.categories[j].category_id == get_category_id) {
                                    getcontainer_category_content.append(create_homepage_category_products(item));
                                }
                            }
                        }
                        // console.log(arr_products);
                        i++;
                    }
                });
            }
        });

        category_item.on("click", function() {
            var get_category_id = $(this).attr("data-category-id");

            $.ajax({
                type: 'GET',
                url: '/api/v1_products/products',
                success: function(response) {
                    var arr_products = response.result;
                    var getcontainer_category_content = homepage_category_products.find('#category-content');
                    getcontainer_category_content.html('');

                    var i = 0;
                    arr_products.forEach(function(item) {
                        if (item.percent_protduct != "" && item.product_in_stock && i <= 8) {
                            if (Array.isArray(item.categories) && item.categories.length != 0) {
                                for (var j = 0; j < item.categories.length/2; j++) {
                                    if (item.categories[j].category_id == get_category_id) {
                                        getcontainer_category_content.append(create_homepage_category_products(item));
                                    }
                                }
                            }
                            i++;
                        }
                    });
                }
            });
        });

    }

    if(diffslide_hot_products.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_products/best_sale_products',
            success: function(response) {
                if (!response || !response.result || !response.result.best_sales_product) {
                    return;
                }

                var arr_products = response.result.best_sales_product;
                var getcontainer_slide_hot = diffslide_hot_products.find('#product-secondary-right');
                getcontainer_slide_hot.html('');
                var i = 0;
                arr_products.forEach(function(item) {
                    // if (item.is_best_sale_product && item.product_is_stock) {
                    getcontainer_slide_hot.append(create_hot_products(item, i));
                    i++;
                    // }
                });
            }
        });
    }

    function create_homepage_category_products(item) {
        var element_1 = "<div class='col-12 col-md-3 product-item text-center'>";

        if (item.percent_protduct != '') {
            var element_2 = "<div class='product-highlight'><span class='sale'>"+item.percent_protduct+"</span></div>";
        } else if (item.is_new_product == 1) {
            var element_2 = "<div class='product-highlight'><span class='status'>"+"new"+"</span></div>";
        } else if (item.product_in_stock != 1) {
            var element_2 = "<div class='product-highlight'><span class='status'>"+"hết"+"</span></div>";
        }

        var element_3 = "<a href='" + item.href + "' title='" + item.name + "'><div class='product-image' style='background-image: url(" + item.thumb + "); background-repeat: no-repeat; background-size: cover'></div></a>";
        var element_4 = "<h6 class='card-title font-13 text-center'><strong>" + ((item.name.length > 15) ? (item.name.slice(0, 15) + '...') : item.name) + "</strong></h6>";

        if (item.multi_versions != 1) {
            if (item.price != "0 VND") {
                var element_5 = "<p class='card-text text-center card-text-color product-price-sale'><strike class='product-price-sale'><span class='price'>" + item.price + "</span></strike><span class='product-price'><span class='price'>" + item.compare_price + "</span></span></p>";
            } else {
                var element_5 = "<p>Liên hệ</p>";
            }
        } else {
            if (item.compare_version_min != "0 VND") {
                var element_5 = "<p class='card-text text-center card-text-color product-price-sale'><strike class='product-price-sale'><span class='price'>" + item.compare_version_min + "~" + item.compare_version_max + "</span></strike><span class='product-price'><span class='price'>" + item.price_version_min + "~" + item.price_version_max + "</span></span></p>";
            } else {
                var element_5 = "<p>Liên hệ</p>";
            }
        }

        var element_6 = "<div class='action'><a href=" + item.href + "><i class='fas fa-shopping-cart icon-shopping'></i></a><a href=" + item.href + "><i class='fas fa-search icon-search'></i></a></div></div>";

        return element_1  + element_2 + element_3 + element_4 + element_5 + element_6;
    }

    function create_hot_products(item) {
        var element_1 = "" +
            "<a href='" + item.href + "'>" +
            "    <div class='d-flex list-product-item py-3 px-1'>";
        var element_2 = "" +
            "    <div class='col-44 product-image'>" +
            "        <img src='" + item.thumb + "' alt='" + item.name + "' style='width: 100%; height: 75px;'>" +
            "    </div>";
        var element_3 = "" +
            "    <div class='col-8 product-info'>";
        var element_4 = "" +
            "        <a href='" + item.href + "' " +
            "           class='product-title'>" + ((item.name.length > 15) ? (item.name.slice(0, 15) + '...') : item.name) + "</a>";

        /* if not multi versions */
        if (item.multi_versions != 1) {
            /* if has compare price > 0 */
            if (item.compare_price_master != "0 VND") {
                if (item.compare_price_master == item.price_master || item.price_master == "0 VND") {
                    var element_5 = "" +
                        "<p class='product-price'></p>" +
                        "<p class='product-price-sale'>" + item.compare_price_master + "</p>";
                } else {
                    var element_5 = "" +
                        "<p class='product-price'>" + item.compare_price_master + "</p>" +
                        "<p class='product-price-sale'>" + item.price_master + "</p>";
                }
            } else { /* contact for price */
                var element_5 = "" +
                    "<p>Liên hệ</p>";
            }
        } else { /* multi versions */
            /* if has compare price version min > 0 */
            if (item.compare_version_min != "0 VND") {
                let price_version_range = (item.price_version_min == item.price_version_max)
                    ? item.price_version_min
                    : item.price_version_min + "~" + item.price_version_max;

                let compare_price_version_range = (item.compare_version_min == item.compare_version_max)
                    ? item.compare_version_min
                    : item.compare_version_min + "~" + item.compare_version_max;

                if (price_version_range == compare_price_version_range) {
                    var element_5 = "" +
                        "<p class='product-price'></p>" +
                        "<p class='product-price-sale'>" + price_version_range + "</p>";
                } else {
                    var element_5 = "" +
                        "<p class='product-price'>" + compare_price_version_range + "</p>" +
                        "<p class='product-price-sale'>" + price_version_range + "</p>";
                }
            } else { /* contact for price */
                var element_5 = "" +
                    "<p>Liên hệ</p>";
            }
        }

        var element_6 = "" +
            "        </div>" +
            "    </div>" +
            "</a>";

        return element_1 + element_2 + element_3 + element_4 + element_5 + element_6;
    }
});