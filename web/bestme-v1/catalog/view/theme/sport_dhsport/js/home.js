// (function($, app) {
//
//     var homeCls = function() {
//
//         var vars = {};
//         var ele = {};
//
//         this.run = function() {
//             this.init();
//             this.bindEvents();
//         };
//
//         this.init = function() {
//             vars.id = 0;
//         };
//
//         this.bindEvents = function() {
//
//             initMenu();
//             initSlideBanner();
//             initS1();
//             initS2();
//             initS3();
//             initS4();
//             initS5();
//             initS6();
//             initMH();
//             initQuant();
//             initPopup();
//             initFilter();
//             initSelect();
//             initGotop();
//             initShowFilter();
//             initCollap();
//         };
//
//         this.resize = function() {
//
//         };
//         var initGotop = function() {
//             $('.go-top').click(function () {
//                 $("html, body").animate({
//                     scrollTop: 0
//                 }, 600);
//                 return false;
//             });
//         }
//         var initShowFilter = function() {
//             var overlay = $('<div class="overlay"></div>');
//             $('body').prepend(overlay);
//             $('.btn-fliter').click(function () {
//                 $('.siderBar').toggleClass('open');
//                 overlay.show();
//             })
//             overlay.click(function () {
//                 $('.siderBar').removeClass('open');
//                 $(this).hide();
//             })
//
//         }
//
//         var initQuant = function(){
//             jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"><img src="catalog/view/theme/sport_dhsport/images/home/plus.png" alt=""></div><div class="quantity-button quantity-down"><img src="catalog/view/theme/sport_dhsport/images/home/minus.png" alt=""></div></div>').insertAfter('.quantity input');
//             jQuery('.quantity').each(function () {
//                 var spinner = jQuery(this),
//                     input = spinner.find('input[type="number"]'),
//                     btnUp = spinner.find('.quantity-up'),
//                     btnDown = spinner.find('.quantity-down'),
//                     min = input.attr('min'),
//                     max = input.attr('max');
//
//                 btnUp.click(function () {
//                     var oldValue = parseFloat(input.val());
//                     if (oldValue >= max) {
//                         var newVal = oldValue;
//                     } else {
//                         var newVal = oldValue + 1;
//                     }
//                     spinner.find("input").val(newVal);
//                     spinner.find("input").trigger("change");
//                 });
//
//                 btnDown.click(function () {
//                     var oldValue = parseFloat(input.val());
//                     if (oldValue <= min) {
//                         var newVal = oldValue;
//                     } else {
//                         var newVal = oldValue - 1;
//                     }
//                     spinner.find("input").val(newVal);
//                     spinner.find("input").trigger("change");
//                 });
//
//             });
//         }
//         var initMenu = function() {
//             $(".accodition").click(function () {
//                 $(this).next().slideToggle('fast');
//                 $(this).toggleClass('rotate');
//             });
//             $(".open-sidemenu").click(function () {
//                 $('#sidenav').toggleClass('show-menu');
//                 $('.block-overlay').toggleClass('over');
//                 $('body').toggleClass('no-scroll');
//             });
//             $(".open-cart").click(function () {
//                 $('.block-cart').toggleClass('show-cart');
//                 $('.block-over').toggleClass('over');
//                 $('body').toggleClass('no-scroll');
//                 ajaxGetOrderInfo();
//             });
//             $(".block-overlay").click(function () {
//                 $('#sidenav').toggleClass('show-menu');
//                 $(this).toggleClass('over');
//                 $('body').toggleClass('no-scroll');
//             });
//             $(".block-over").click(function () {
//                 $('.block-cart').toggleClass('show-cart');
//                 $(this).toggleClass('over');
//                 $('body').toggleClass('no-scroll');
//             });
//             $(".close-cart").click(function () {
//                 $('.block-cart').toggleClass('show-cart');
//                 $('.block-over').toggleClass('over');
//                 $('body').toggleClass('no-scroll');
//             });
//         };
//
//         var initSlideBanner = function() {
//             var slideBanner = new Swiper('.swiper-banner',{
//                 autoplay: {
//                     delay: jQuery('.swiper-banner').attr('data-time'),
//                     disableOnInteraction: false,
//                 },
//                 navigation: {
//                     nextEl: '.swiper-button-next',
//                     prevEl: '.swiper-button-prev',
//                 },
//             });
//         }
//         var initFilter = function() {
//             $('.title-sidebar').click(function () {
//                 $(this).next().slideToggle();
//                 $(this).toggleClass('open');
//
//             })
//         }
//         var initCollap = function(){
//             $('.collap-icon').click(function () {
//                 $('.cart-widget').toggleClass('open');
//             })
//         }
//
//
//
//
//         var initS1 = function() {
//             var swiper = new Swiper('.swiper-new', {
//                 slidesPerView: 6,
//                 spaceBetween: 20,
//                 loop: false,
//                 navigation: {
//                   nextEl: '.swiper-button-next-n',
//                   prevEl: '.swiper-button-prev-n',
//                 },
//                 breakpoints: {
//                     // when window width is >= 320px
//                     320: {
//                         slidesPerView: 2,
//                         spaceBetween: 5
//                     },
//                     // when window width is >= 480px
//                     480: {
//                         slidesPerView: 2,
//                         spaceBetween: 5
//                     },
//                     // when window width is >= 640px
//                     768: {
//                         slidesPerView: 3,
//                         spaceBetween: 10
//                     },
//                     1200: {
//                         slidesPerView: jQuery('.swiper-new').attr('data-grid')
//                     }
//                 }
//             });
//         }
//         var initS2 = function() {
//             var swiper = new Swiper('.swiper-sale', {
//                 slidesPerView: 6,
//                 spaceBetween: 20,
//                 loop: false,
//                 navigation: {
//                   nextEl: '.swiper-button-next-s',
//                   prevEl: '.swiper-button-prev-s',
//                 },
//                 breakpoints: {
//                     // when window width is >= 320px
//                     320: {
//                         slidesPerView: 2,
//                         spaceBetween: 5
//                     },
//                     // when window width is >= 480px
//                     480: {
//                         slidesPerView: 2,
//                         spaceBetween: 5
//                     },
//                     // when window width is >= 640px
//                     768: {
//                         slidesPerView: 3,
//                         spaceBetween: 10
//                     },
//                     1200: {
//                         slidesPerView: jQuery('.swiper-sale').attr('data-grid')
//                     }
//                 }
//             });
//         }
//         var initS3 = function() {
//             var swiper = new Swiper('.swiper-popular', {
//                 slidesPerView: 6,
//                 spaceBetween: 20,
//                 loop: false,
//                 navigation: {
//                   nextEl: '.swiper-button-next-p',
//                   prevEl: '.swiper-button-prev-p',
//                 },
//                 breakpoints: {
//                     // when window width is >= 320px
//                     320: {
//                         slidesPerView: 2,
//                         spaceBetween: 5
//                     },
//                     // when window width is >= 480px
//                     480: {
//                         slidesPerView: 2,
//                         spaceBetween: 5
//                     },
//                     // when window width is >= 640px
//                     768: {
//                         slidesPerView: 3,
//                         spaceBetween: 10
//                     },
//                     1200: {
//                         slidesPerView: jQuery('.swiper-popular').attr('data-grid')
//                     }
//                 }
//             });
//         };
//
//         var initS4 = function() {
//             var swiper = new Swiper('.swiper-custom', {
//                 slidesPerView: 6,
//                 spaceBetween: 20,
//                 loop: false,
//                 navigation: {
//                     nextEl: '.swiper-button-next-p',
//                     prevEl: '.swiper-button-prev-p',
//                 },
//                 breakpoints: {
//                     // when window width is >= 320px
//                     320: {
//                         slidesPerView: 2,
//                         spaceBetween: 5
//                     },
//                     // when window width is >= 480px
//                     480: {
//                         slidesPerView: 2,
//                         spaceBetween: 5
//                     },
//                     // when window width is >= 640px
//                     768: {
//                         slidesPerView: 3,
//                         spaceBetween: 10
//                     },
//                     1200: {
//                         slidesPerView: jQuery('.swiper-custom').attr('data-grid')
//                     }
//                 }
//             });
//         };
//
//         var initS5 = function() {
//             var swiper = new Swiper('.swiper-custom-1', {
//                 slidesPerView: 6,
//                 spaceBetween: 20,
//                 loop: false,
//                 navigation: {
//                     nextEl: '.swiper-button-next-p',
//                     prevEl: '.swiper-button-prev-p',
//                 },
//                 breakpoints: {
//                     // when window width is >= 320px
//                     320: {
//                         slidesPerView: 2,
//                         spaceBetween: 5
//                     },
//                     // when window width is >= 480px
//                     480: {
//                         slidesPerView: 2,
//                         spaceBetween: 5
//                     },
//                     // when window width is >= 640px
//                     768: {
//                         slidesPerView: 3,
//                         spaceBetween: 10
//                     },
//                     1200: {
//                         slidesPerView: jQuery('.swiper-custom-1').attr('data-grid')
//                     }
//                 }
//             });
//         };
//
//         var initS6 = function() {
//             var swiper_partner = new Swiper('.swiper-partner', {
//                 slidesPerView: 4,
//                 spaceBetween: 20,
//                 loop: false,
//                 navigation: {
//                     nextEl: '.swiper-button-next-n',
//                     prevEl: '.swiper-button-prev-n',
//                 },
//                 breakpoints: {
//                     // when window width is >= 320px
//                     320: {
//                         slidesPerView: 2,
//                         spaceBetween: 5
//                     },
//                     // when window width is >= 480px
//                     480: {
//                         slidesPerView: 2,
//                         spaceBetween: 5
//                     },
//                     // when window width is >= 640px
//                     768: {
//                         slidesPerView: 3,
//                         spaceBetween: 10
//                     },
//                     1200: {
//                         slidesPerView: 4
//                     }
//                 }
//             });
//         }
//
//         var initMH = function() {
//             // item
//             $('.mh-banner').matchHeight();
//             $('.mh-address').matchHeight();
//             $('.mh-adIt').matchHeight();
//         }
//         var initPopup = function() {
//             $('a.open-modal').click(function(event) {
//                 $(this).modal({
//                     fadeDuration: 250
//                 });
//                 return false;
//             });
//         }
//         var initSelect = function() {
//             $('.js-select').select2();
//         }
//     };
//
//
//     $(document).ready(function() {
//         var homeObj = new homeCls();
//         homeObj.run();
//         // On resize
//         $(window).resize(function() {
//             homeObj.resize();
//             if ($(window).width() <= 768) {
//                 $('#sidenav').removeClass('main-menu');
//             } else {
//                 $('#sidenav').addClass('main-menu');
//             }
//         });
//     });
//
//     var initQuant = function(){
//         jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"><img src="catalog/view/theme/sport_dhsport/images/home/plus.png" alt=""></div><div class="quantity-button quantity-down"><img src="catalog/view/theme/sport_dhsport/images/home/minus.png" alt=""></div></div>').insertAfter('.quantity input');
//         jQuery('.quantity').each(function () {
//             var spinner = jQuery(this),
//                 input = spinner.find('input[type="number"]'),
//                 btnUp = spinner.find('.quantity-up'),
//                 btnDown = spinner.find('.quantity-down'),
//                 min = input.attr('min'),
//                 max = input.attr('max');
//
//             btnUp.click(function () {
//                 var oldValue = parseFloat(input.val());
//                 if (oldValue >= max) {
//                     var newVal = oldValue;
//                 } else {
//                     var newVal = oldValue + 1;
//                 }
//                 spinner.find("input").val(newVal);
//                 spinner.find("input").trigger("change");
//             });
//
//             btnDown.click(function () {
//                 var oldValue = parseFloat(input.val());
//                 if (oldValue <= min) {
//                     var newVal = oldValue;
//                 } else {
//                     var newVal = oldValue - 1;
//                 }
//                 spinner.find("input").val(newVal);
//                 spinner.find("input").trigger("change");
//             });
//
//         });
//
//         /* setup event on increasing/decreasing */
//         setupQuantityListener();
//     };
//
//     //show cart
//     function ajaxGetOrderInfo() {
//         $.ajax({
//             url: 'index.php?route=checkout/my_orders/ajaxGetOrderInfo',
//             type: 'get',
//             data: $('#add-to-cart').serialize(),
//             dataType: 'json',
//             beforeSend: function() {
//                 //TODO loading
//             },
//             complete: function() {
//                 //TODO loaded
//             },
//             success: function(data) {
//                 //TODO: why not use json parse
//                 if (data && data.order_products && data.order && data.order_products.length) {
//                     $('.count span').text(data.order_products.length);
//                     var ordorderProductListHtml = '';
//                     $.each(data.order_products, function (index, product) {
//                         var version_info = '';
//                         if (product.attributes && Object.keys(product.attributes).length > 0) {
//                             $.each(product.attributes, function (attrName, attrValue) {
//                                 version_info = version_info +
//                                     '    <p>\n' +
//                                     '        <span>' + attrName + ':</span>\n' +
//                                     '        <span>' + attrValue + '</span>\n' +
//                                     '    </p>\n';
//                             });
//                         }
//
//
//                         ordorderProductListHtml = ordorderProductListHtml + '<div class="cart-item">\n' +
//                             '            <div class="row row-s-8">\n' +
//                             '                <div class="col-4 col-s-8">\n' +
//                             '                    <div class="thumb">\n' +
//                             '                        <img class="img-fluid" src="' + product.image_url + '" alt="">\n' +
//                             '                    </div>\n' +
//                             '                </div>\n' +
//                             '                <div class="col-8 col-s-8">\n' +
//                             '                    <div class="ct">\n' +
//                             '                        <h6 class="brand">\n' +
//                             '                            ' + product.manufacturer + '\n' +
//                             '                        </h6>\n' +
//                             '                        <h5 class="name">\n' +
//                             '                            <a href="' + product.url + '">' + product.name + '</a>\n' +
//                             '                        </h5>\n' +
//                             '                        <div class="row align-center">\n' +
//                             '                            <div class="col-7">\n' +
//                             '                                ' + version_info +
//                             '                            </div>\n' +
//                             '                            <div class="col-5">\n' +
//                             '                                <div class="price">\n' +
//                             '                                    <span class="old">' + product.total_formart + '</span>\n' +
//                             '                                    <span class="new">'+ product.price_formart +'</span>\n' +
//                             '                                </div>\n' +
//                             '                            </div>\n' +
//                             '                        </div>\n' +
//                             '                        <div class="quant-wp mb-0">\n' +
//                             '                            <p>Số lượng</p>\n' +
//                             '                            <div class="quantity">\n' +
//                             '                                <input type="number" min="1" max="9999" step="1" product-version-id="'+product.product_version_id+'" value="'+product.amount+'" class="product_quantity" product-id="'+product.id+'" >\n' +
//                             '                            </div>\n' +
//                             '                        </div>\n' +
//                             '                    </div>\n' +
//                             '                </div>\n' +
//                             '            </div>\n' +
//                             '        </div>'
//                     });
//
//                     /* update order product list */
//                     $('#list-card').html(ordorderProductListHtml);
//
//                     /* update total amount */
//                     var totalAmountHtml = (data && data.order && data.order.into_money_formart) ? data.order.into_money_formart : '0đ';
//                     $('.all-price h5.price').text(totalAmountHtml);
//
//                     /* Go to payment */
//                     $('a#payment').attr('href', data.href_order_preview);
//                 } else {
//                     if (data && data.href_common_shop) {
//                         var htmlCartEmpty = '<p>Bạn không có sản phẩm nào trong giỏ hàng</p>\n' +
//                             '        <p><a style="border: 1px solid #ccc;" class="btn btn-continue" href="'+data.href_common_shop+'">Tiếp tục mua sắm</a></p>\n' +
//                             '        <br/><br/><br/><br/>'
//
//                         /* update order empty */
//                         $('#list-card').html(htmlCartEmpty);
//                     }
//                 }
//
//                 /* init quantity increasae/decrease buttons */
//                 initQuant();
//             },
//             error: function(xhr, ajaxOptions, thrownError) {
//                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
//             }
//         });
//     }
//
//     function setupQuantityListener() {
//         $('.product_quantity').bind('change paste keyup', function () {
//             var em = $(this); // retrieve the index
//             var product_id = em.attr('product-id');
//             var quantity = em.val();
//             var product_version_id = em.attr('product-version-id');
//             var validity = this.validity;
//
//             //change input quantity validate
//             if (!validity.valid && validity.rangeOverflow) {
//                 quantity = em.attr('max');
//             }
//
//             if (!validity.valid && validity.rangeUnderflow) {
//                 quantity = em.attr('min');
//             }
//
//             if (quantity == '') {
//                 quantity = 1;
//             }
//
//             $.ajax({
//                 url: 'index.php?route=checkout/my_orders/update_order_products',
//                 method: 'POST',
//                 data: {
//                     product_id: product_id,
//                     quantity: quantity,
//                     product_version_id: product_version_id,
//                 },
//                 success: function (json) {
//                     //TODO: why json parse
//                     var json = JSON.parse(json);
//                     if (json && json.data) {
//                         var data = json.data;
//                         var totalAmountHtml = (data && data.order && data.order.into_money_formart) ? data.order.into_money_formart : '0đ';
//                         $('.all-price h5.price').text(totalAmountHtml);
//                     }
//                 }
//             });
//         });
//     }
//
// }(jQuery, $.app));
