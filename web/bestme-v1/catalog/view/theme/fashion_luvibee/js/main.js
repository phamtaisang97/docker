$(document).ready(function() {
    // Show search header
    if($(window).width() >= 992) {
        $('#input-search').click(function() {
            $('.header').addClass('has-search');
            $('.wrap-header-info').removeClass('col-md-5').addClass('col-md-10');
            $('.header-info').addClass('w-100');
        });
        $(window).click(function(e) {
            var check_txt = $('#input-search').val();
            var check_length = check_txt.split(" ").length;
            if( $(e.target).attr('id') !== 'input-search' && check_length < 3){
                $('.header').removeClass('has-search');
                $('.wrap-header-info').removeClass('col-md-10').addClass('col-md-5');
                $('.header-info').removeClass('w-100');
            }
        });
    }
    // Search input change key => show box result
    $('#input-search').on('change keyup', function(event) {
        var keyword = $(this).val();
        var key_length = keyword.split(" ").length;
        if(key_length >= 3) {
            $('.search-box-ajax').removeClass('d-none').addClass('d-block');
            $('#main-wrapper').css('opacity', 0.2);
        }
        else {
            $('.search-box-ajax').removeClass('d-block').addClass('d-none');
            $('#main-wrapper').css('opacity', 1);
        }
    });
    // Select dropdown when click dropdown item
    $('.dropdown-filter').on('click', '.dropdown-item', function(event) {
        event.preventDefault();
        var key_txt = $(this).text();
        $(this).parents('.dropdown-filter').find('button').html('').append(key_txt);
    });
    // Disibled product
    if($('.pro-disabled').length > 0) {
        $('.pro-disabled a').click(function(event) {
            event.preventDefault();
        });
    }

    /** Menu mobile **/
    $(".accodition").click(function () {
        $(this).next().slideToggle('fast');
        $(this).toggleClass('rotate');
    });
    $(".open-sidemenu").click(function () {
        $('#sidenav').toggleClass('menu-mobile');
        $('.block-overlay').toggleClass('over');
        $('body').toggleClass('no-scroll');
        $('.block-header-sale').addClass('slow-layer');
    });
    $(".block-overlay").click(function () {
        $('#sidenav').toggleClass('menu-mobile');
        $(this).toggleClass('over');
        $('body').toggleClass('no-scroll');
        $('.block-header-sale').removeClass('slow-layer');
    });
    /** Menu mobile **/

    // Zoom image product
    $('#zoom_thumb').ezPlus({
        constrainType: 'height', constrainSize: 418, zoomType: 'lens',
        containLensZoom: true, gallery: 'gallery_01', cursor: 'pointer', galleryActiveClass: 'active'
    });


    // input quanty caculate
    $('input[type="number"].custom-number-input').each(function(index, el) {
        var wrapper = document.createElement("div");
        wrapper.classList = "custom-number-input-wrapper";
        
        var increase = document.createElement("button");
        increase.innerHTML  = '+';
        increase.classList = "increase";
        var decrease = document.createElement("button");
        decrease.innerHTML  = '-';
        decrease.classList = "decrease";

        $(wrapper).append(decrease);
        $(wrapper).append($(this).clone());
        $(wrapper).append(increase);
        $(this).replaceWith($(wrapper));
    });

    $(document).on('click', '.custom-number-input-wrapper >button', function(e){
        var input = $(this).parent().find('input');
        var input_val = (input.val()) ? parseInt(input.val()) : 0;
        var min = input.attr('min');
        var max = input.attr('max');
        var step = typeof input.attr('step') !== "undefined" ? input.attr('step') : 1;
        if( $(this).hasClass('increase') ){
            input_val = input_val + step;
        }
        else{
            input_val = input_val - step;
        }
        if( input_val < min && typeof min !== "undefined" ) input_val = min;
        if( input_val > max && typeof max !== "undefined" ) input_val = max;
        input.val(input_val).change();
    })

    $('.cart-table table').on('change', '.custom-number-input-wrapper >input', function(e){
        var price = $(this).closest('tr').find('.price[data-value]').attr('data-value');
        price = parseInt(price) * parseInt($(this).val());
        var row_price = $(this).closest('tr').find('.total-price[data-value]');
        row_price.text($.number(price, 0, '', '.'));
        row_price.attr('data-value', price);

        var total_price = 0;
        var total_transport = 0;
        var total_tax = 0;
        var total_cart = 0;
        $('.cart-table table tr').each(function(index, el) {
            if( $(this).find('.total-price[data-value]').length ){
                total_price += parseInt($(this).find('.total-price[data-value]').attr('data-value'));
            }
        });
        total_transport = parseInt($('.table-footer .total-transport').attr('data-value'));
        total_tax = parseInt($('.table-footer .total-tax').attr('data-value'));
        total_cart = total_transport + total_tax + total_price;
        console.log(total_cart);
        $('.table-footer .total-price').text($.number(total_price, 0, '', '.'));
        $('.table-footer .total-cart').text($.number(total_cart, 0, '', '.'));
    });

    // Fixed Menu when Scroll
    // var offset_header = $('.main-header').offset().top;
    var topHeaderHeight = $('#header').outerHeight();
    $('.product-teaser-fix').css('top', topHeaderHeight+"px");
    $('.home .header').removeClass('header-default has-shadow');
    $(window).scroll(function() {
        if($(window).scrollTop() >= 50) {
            $('.home .header').addClass('header-default has-shadow');
        }
        else {
            $('.home .header').removeClass('header-default has-shadow');
        }

        var windowScrollTop = (window.pageYOffset || document.scrollTop)  - (document.clientTop || 0);
        if( $('.product-teaser-fix').length == 0 ) return;
        if (windowScrollTop >= topHeaderHeight + 350) {
            $('.product-teaser-fix').slideDown(300);
        } else {
            $('.product-teaser-fix').slideUp(300);
        }
    });

    // Product Carousel
    // $('.owl-product').owlCarousel({
    //     loop:true,
    //     margin:30,
    //     nav:false,
    //     responsive:{
    //         0:{
    //             items:1
    //         },
    //         600:{
    //             items:2
    //         },
    //         1000:{
    //             items:4
    //         }
    //     }
    // });
    // Custommer Carousel
    $('.owl-custommer').owlCarousel({
        loop: jQuery('.owl-custommer').attr('data-loop') == 1 ? true : false,
        autoplay: jQuery('.owl-custommer').attr('data-auto-play') == 1 ? true : false,
        margin:60,
        nav:false,
        animateOut: true,
        responsive:{
            0:{
                items:1
            },
            600: {
                items:2,
            },
            1000:{
                items: jQuery('.owl-custommer').attr('data-limit-inline'),
            }
        }
    });

    // Product Carousel
    function owlInit(elements, numItem, nav = true, dots = false, margin = 0)
    {
        if(elements.length) {
            elements.each(function () {
                const element = $(this);
                const autoplayTime = undefined === element.data('autoplay_time') ? 5 : element.data('autoplay_time')
                element.owlCarousel({
                    loop: undefined !== element.data('loop') && 1 == element.data('loop'),
                    autoplay: undefined !== element.data('autoplay') && 1 == element.data('autoplay'),
                    autoplayTimeout: autoplayTime * 1e3,
                    responsiveClass:true,
                    dots: dots,
                    nav: nav,
                    margin: margin,
                    navText: ['<i class="icon icon-arrow-left"></i>', '<i class="icon icon-arrow-right"></i>'],
                    // URLhashListener:true,
                    // startPosition: 'URLHash',
                    responsive:{
                        0:{
                            items:1,
                        },
                        600:{
                            items:Math.round(numItem/2),
                        },
                        1000:{
                            items:numItem,
                        }
                    }
                });
            });
        }
    }
    owlInit($('.block-sale-product .owl-product'), 1, true, false);
    owlInit($('.block-most-sale-product .owl-product'), 4, true, false, 30);
    owlInit($('.block-new-product .owl-product'), 4, true, false, 30);
    var product_detail_related_item = 4;
    if ($('.product-detail .product-related').attr('data-number-item') != undefined) {
        product_detail_related_item = $('.product-detail .product-related').attr('data-number-item');
    }
    owlInit($('.product-detail .product-related .owl-product'), product_detail_related_item, true, false, 30);

    // Check hidden form address => add class d-none for set address default
    $('.address-form .collapse').on('show.bs.collapse', function () {
        $(this).parent().children('.default-address').addClass('d-none');
    });
    $('.address-form .collapse').on('hide.bs.collapse', function () {
        $(this).parent().children('.default-address').removeClass('d-none');
    });
        // Close form address
    $('.cancle-address').click(function() {
        $(this).parents('.address-form').children('.collapse').collapse('hide');
    });

    // Show Widget Search
    if($('.search-button').length > 0) {
        $('.search-button').click(function() {
            $('.home-search').toggleClass('show');
        });
    }

    // Show/Hide password
    if($('#your-password').length > 0) {
        $('#show-pass').click(function() {
            $(this).parents('form').find('#your-password').attr('type', 'text');
        });
        $('#hide-pass').click(function() {
            $(this).parents('form').find('#your-password').attr('type', 'password');
        });
    }



    // Close menu mobile
    if($('#navbarMainMenu').length > 0) {
        $('button.navbar-toggler').click(function() {
            $('#overlay').toggleClass('show');
            // $('#navbarMainMenu').addClass('show');
        });

        $('#navbarMainMenu i.close').click(function() {
            $('#overlay').removeClass('show');
            $(this).parent('#navbarMainMenu').removeClass('show');
        });
        $(window).click(function(e) {
            if( $(e.target).attr('id') == 'overlay' && $('#navbarMainMenu').hasClass('show') && $(e.target).parents('#navbarMainMenu').length == 0 ){
                $('#overlay').removeClass('show');
                $('#navbarMainMenu').removeClass('show');
            }
        });

        // Menu responsive
        $('.block-header .main-menu ul > li.has-child > a>.toggle').on('click', function(e){
            $(this).closest('li').toggleClass('active');
            e.preventDefault();
            return false;
        });
    }

    // Show/Hide password
    $(document).on('click', '.input-password-group button.show-password', function(e) {
        var input = $(this).parent().find('>input');
        $(this).toggleClass('active');
        if ($(this).hasClass('active')){
            $(this).find('i').removeClass('icon-eye').addClass('icon-no-eye');
            input.attr('type', 'text');
        } else {
            $(this).find('i').removeClass('icon-no-eye').addClass('icon-eye');
            input.attr('type', 'password');
        }
    });

    function addStyleAttribute($element, styleAttribute) {
        $element.prop('style', $element.prop('style') + '; ' + styleAttribute);
    }

    /* change .page-title, .padding-top css rule if is on mobile */
    const isOnMobile = $('.logo-header-menu .logo').data('is_on_mobile');
    if (undefined !== isOnMobile && 1 === parseInt(isOnMobile)) {
        const logoHeight = $('.logo-header-menu .logo img').height();
        if (undefined !== logoHeight) {
            /* old config + logo height + logo margin-y - old mobile header padding-top */
            $('.page-title').css('marginTop', -60 + logoHeight + 10 - 30);

            /* old config + logo height + logo margin-y - old mobile header padding-top */
            addStyleAttribute($('.padding_top'), 'padding-top: ' + (75 + logoHeight + 10 - 30) + 'px !important');
            addStyleAttribute($('.padding_top .modal-body'), 'padding-top: ' + (20 + logoHeight + 10 - 30) + 'px !important');
        }
    } else {
        // fix header override main content in case of custom logo height
        const headerHeight = $('#header').height();
        if (undefined !== headerHeight && 75 < headerHeight) { // old config: 75px
            addStyleAttribute($('.padding_top'), 'padding-top: ' + headerHeight + 'px !important');
        }
    }

    /* show price title */
    $('.box-product, .pro-price').on('mouseover', '.show-title', function () {
        const _this = $(this);
        if (undefined === _this.attr('title')) {
            _this.prop('title', _this.text());
        }
    });
});