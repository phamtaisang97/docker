function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

// Cart add remove functions
var cart = {
    'add': function(product_id, quantity) {
        var valid = validateSerialize();
        if (!valid) {
            $('input[name="quantity"]')[0].reportValidity();
            return false;
        }
        if (jQuery('#add-to-cart button[type="submit"]').attr('data-status') == "disabled") {
            return false;
        }
        $.ajax({
            url: 'index.php?route=checkout/my_orders/addCart',
            type: 'post',
            data: $('#add-to-cart').serialize(),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('.count-cart').html(json['total_product_cart']);

                    $('#add_cart_notify').modal('show');
                }

                ajaxGetOrderInfo();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'update': function(key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var voucher = {
    'add': function() {

    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var wishlist = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);

                $('html, body').animate({ scrollTop: 0 }, 'slow');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}

var compare = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#compare-total').html(json['total']);

                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
    e.preventDefault();

    $('#modal-agree').remove();

    var element = this;

    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function(data) {
            html  = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div>';
            html += '  </div>';
            html += '</div>';

            $('body').append(html);

            $('#modal-agree').modal('show');
        }
    });
});
/// update product detail when choose version
$(document).ready(function () {
    if (jQuery('#add-to-cart .form-check-input').length) {
        ajaxChangeVersion();
    }
    jQuery('#add-to-cart .form-check-input').on('click', function () {
        ajaxChangeVersion();
    });

    ajaxGetOrderInfo();

    $(document).on('click', '.dropdown-cart > a', function (e) {
        event.preventDefault();
        ajaxGetOrderInfo();
    });
});

function ajaxChangeVersion() {
    $.ajax({
        url: 'index.php?route=product/product/updateCartWhenChangeVersion',
        type: 'post',
        data: $('#add-to-cart').serialize(),
        dataType: 'json',
        beforeSend: function() {
            //TODO loading
        },
        complete: function() {
            //TODO loaded
        },
        success: function(json) {
            if (json['success']) {
                if (json.sale_on_out_of_stock == 1 && 1 == json.product_version.status){
                    jQuery('button.button_buying').attr('data-status', '');
                    jQuery('button.btn-addcart-bestme').attr('data-status', '');
                    jQuery('a.btn-buynow-bestme').attr('data-status', '');
                    jQuery('a.add-cart-bestme').attr('data-status', '');
                }
                else {
                    jQuery('button.button_buying').attr('data-status', 'disabled');
                    jQuery('button.btn-addcart-bestme').attr('data-status', 'disabled');
                    jQuery('a.btn-buynow-bestme').attr('data-status', 'disabled');
                    jQuery('a.add-cart-bestme').attr('data-status', 'disabled');
                }
                updateHtmlVersion(json['sale_on_out_of_stock'], json['product_version']);
            } else {
                jQuery('button.button_buying').attr('data-status', 'disabled');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}

function updateHtmlVersion(sale_on_out_of_stock, product) {
    // version
    if (product['version']) {
        // form mua tra gop
        var version = product['version'].replace(",", " - ")
        $('input#pAttribute').val(version)

        // request product form
        $('#buy-modal-form input[name=product_version_id]').val(product.product_version_id);
    }

    // update percent sale
    if('' === product['percent_sale']) {
        jQuery('#product-detail-percent-sale').addClass('d-none').text(product['percent_sale']);
    } else {
        jQuery('#product-detail-percent-sale').removeClass('d-none').text(product['percent_sale']);
    }

    //update product version
    if (product['version']) {
        if (parseInt(product['compare_price_format']) === 0) {
            jQuery('#buy-now .product-version').text(' - ' + product['version']);
            jQuery('#buy-now button').hide();
        } else {
            jQuery('#buy-now .product-version').text(' - ' + product['version'] + ' - ' + ((product['price'] == 0) ? product['compare_price_format'] : product['price_format']));
            jQuery('#buy-now button').show();
        }
    } else {
        jQuery('#buy-now .product-version').text(' - ' + ((product['price'] == 0) ? product['compare_price_format'] : product['price_format']));
    }

    //update price
    var compare_price = parseInt(product['compare_price_format']);
    if (product['price'] == 0) {
        jQuery('.gia-san-pham').html('<span class="new-price d-flex">'+ (compare_price === 0
            ? '<span class="new-price d-flex">' + '<span class="btn btn-primary" data-toggle="modal" data-target="#modal-buy">Liên hệ</span>' + '</span>' +
              '    <span class="align-self-center old-price ml-3 fz-16">' +
              '        <del></del>' +
              '    </span>'
            : '<span class="new-price d-flex">' + '<span class="fw-500 text-third fz-36">'+ product['compare_price_format'] + '</span>' +
              '<span class="align-self-center old-price ml-3 fz-16"><del>'+ product['price_format'] +'</del></span>')
        );
        jQuery('.gia-san-pham .old-price').html('');
        // jQuery('p.old-price-after').html('<del><span class="old-price fz-18 text-second mt-n3 mb-3 text-left">'+ product['compare_price_format'] +'</span></del>');
        // giá sản phẩm header
        jQuery('.gia-san-pham-header').html('<span class="gia-header">'+ (compare_price === 0 ?
            '<a data-toggle="modal" data-target="#modal-buy">\n'
            + '<span class="gia-header">' + '' + '</span></a>'
            : '<span class="gia-header">' + product['compare_price_format'] + '</span>'));
    } else {
        if (parseInt(product['price_format']) === 0) {
            jQuery('.gia-san-pham').html(comparePrice === 0
                ? '<a data-toggle="modal" data-target="#modal-buy" class="btn btn-buy">\n'
                + '<span class="new-price d-flex">' + '<span class="btn btn-primary">Liên hệ</span>' + '</span></a>'
                : '<span class="new-price d-flex">' + '<span class="fw-500 text-third fz-36">'+ product['compare_price_format'] + '</span></span>'
            );
        } else {
            jQuery('.gia-san-pham').html('<span class="new-price d-flex">' +
                '<span class="fw-500 text-third fz-36">'+ product['price_format'] +'</span>' +
                '<span class="align-self-center old-price ml-3 fz-16"><del>'+ product['compare_price_format'] +'</del></span></span>'
            );
            jQuery('.gia-san-pham-header').html('<span class="gia-header">'+ product['price_format'] +'</span>');

        }
    }
    if (parseInt(product['compare_price_format']) === 0) {
        $('.pro-action').css('visibility','hidden');
        $('.button_buying').css('visibility','hidden');
        $('.add-cart-bestme').css('visibility','hidden');
    }
    else{
        $('.pro-action').css('visibility','visible');
        $('.button_buying').css('visibility','visible');
        $('.add-cart-bestme').css('visibility','visible');
    }
    //update status
    jQuery('.code-product').html(product['sku']);
    //active button add cart
    //TODO : remove if check is_stock done
    // if (product['status'] == 1 && (sale_on_out_of_stock == 1 || (sale_on_out_of_stock == 0 && product['quantity'] > 0))) {
    if (product['status'] == 1 && (sale_on_out_of_stock == 1)) {
        jQuery('.tinh-trang').html('<span class="true"><i class="fa fa-check-square-o" aria-hidden="true"></i>Còn hàng</span>');
    } else {
        jQuery('.tinh-trang').html('<span class="false">Hết hàng</span>');
    }

    /* update product version image */
    // set default values
    product.image = product.image ? product.image : jQuery('#product-image-default-src').val();
    product.image_alt = product.image_alt ? product.image_alt : '';
    if (product.image) {
        const indicatorSlider = jQuery('#carouselProDetail .carousel-indicators');
        let nextProductIndex = jQuery('#carouselProDetail .carousel-inner .carousel-item').length;
        let isLoadOtherVersion = false;
        if (nextProductIndex > 0) {
            // slider has at least 1 image
            // get last product image
            const lastProductImage = jQuery('#carouselProDetail .carousel-inner .carousel-item').last();
            // check if it is product version image and not this version
            isLoadOtherVersion = lastProductImage.data('version') !== product.version;
            if (lastProductImage.length > 0 && lastProductImage.hasClass('product-version-image') && isLoadOtherVersion) {
                // remove last main slider image
                lastProductImage.remove();
                // decrease next product index to current max product index
                --nextProductIndex;
                // remove last indicator slider image
                indicatorSlider.trigger('remove.owl.carousel', [nextProductIndex]);
            }
        }

        if (isLoadOtherVersion) {
            // remove current product image active class
            jQuery('#carouselProDetail .carousel-inner .carousel-item.active').removeClass('active');

            // add product version image to the end of product image slider
            jQuery(`<div class="carousel-item active product-version-image" data-version="${product.version}">
                <img src="${product.image}" alt="${product.image_alt}">
            </div>`).appendTo(jQuery('#carouselProDetail .carousel-inner'));
            jQuery('#carouselProDetail').carousel();

            // add product version image to the end of indicator image slider
            const productImageIndicatorHtml = `<div class="item img-thumb img-version-thumb">
                <a data-target="#carouselProDetail" data-slide-to="${nextProductIndex}">
                    <img src="${product.image}" alt="${product.image_alt}" class="img-wrap">
                </a>
            </div>`;
            indicatorSlider.trigger('add.owl.carousel', [productImageIndicatorHtml]);

            // refresh indicator slider
            indicatorSlider.trigger('refresh.owl.carousel');
        } else {
            // not insert this product version image => decrease next product index
            --nextProductIndex;
        }

        // go to product version image slide
        jQuery('#carouselProDetail').carousel(nextProductIndex);
        indicatorSlider.trigger('to.owl.carousel', [nextProductIndex]);
    }
}

function validateSerialize() {
    var invalidFields = jQuery('#add-to-cart').find( ":invalid" );
    if (invalidFields.length > 0) return false;
    return true;
}

function ajaxGetOrderInfo() {
    $.ajax({
        url: 'index.php?route=checkout/my_orders/ajaxGetOrderInfo',
        type: 'get',
        data: $('#add-to-cart').serialize(),
        dataType: 'json',
        beforeSend: function() {
            //TODO loading
        },
        complete: function() {
            //TODO loaded
        },
        success: function(data) {
            //TODO: why not use json parse
            if (data && data.order_products && data.order && data.order_products.length) {
                let orderProductListHtml = '';
                $.each(data.order_products, function (index, product) {
                    let versionInfo = '';
                    let isPriceDisplayNone = 'd-block';
                    if (product.attributes && Object.keys(product.attributes).length > 0) {
                        $.each(product.attributes, function (attrName, attrValue) {
                            versionInfo = versionInfo +
                                '    <p>\n' +
                                '        <span>' + attrName + ':</span>\n' +
                                '        <span>' + attrValue + '</span>\n' +
                                '    </p>\n';
                        });
                    }

                    if (product.price === product.price_compare) {
                        isPriceDisplayNone = 'd-none';
                    }

                    orderProductListHtml += `<div class="cart-item d-flex">
                                                <a href="${ product.url }" class="">
                                                    <img src="${ product.image_url }" width="64px">
                                                </a>
                                                <div class="fz-12 flex-fill pl-3">
                                                    <a href="${ product.url }" class="text-fourth fz-14 mb-2 d-block fw-500">
                                                        ${ product.name }
                                                        <span class="text-fourth d-block fw-400 fz-12">${ product.attribute }</span>
                                                    </a>
                                                    <span class="d-block mb-2">Số lượng: ${ product.amount }</span>
                                                    <span class="fw-500 text-line-through ${ isPriceDisplayNone }">${ product.price_compare_formart }</span>
                                                    <span class="fw-500">${ product.price_formart }</span>
                                                </div>
                                            </div>`;
                });

                /* update order product list */
                $('.dropdown-cart .cart-item-list').html(orderProductListHtml);

                /* update total amount */
                const totalAmountHtml = (data && data.order && data.order.into_money_formart) ? data.order.into_money_formart : '0đ';

                /* update total amount in cart */
                $('.dropdown-cart .cart-total .total-price').text(totalAmountHtml);

                /* update total products in cart */
                $('.dropdown-cart .count-cart').text(data.order.amount);
            } else {
                const htmlCartEmpty = '';

                /* update order empty */
                $('.dropdown-cart .cart-item-list').html(htmlCartEmpty);
                $('.dropdown-cart .cart-total .total-price').text('0');
                $('.dropdown-cart .count-cart').text('0');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}