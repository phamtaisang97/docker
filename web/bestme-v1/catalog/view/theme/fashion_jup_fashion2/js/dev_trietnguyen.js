$(document).ready(function() {
    // var tab_product_detail = $('#product-detail-page .product-detail #myTabContent .tab-pane .tab-content-description');
    // var collapse_tab_product_detail = $('#product-detail-page .product-detail #myTabContent .dv-collapse .btn-collapse');
    // var getheight_tab_product_detail = tab_product_detail.height();
    
    // if (getheight_tab_product_detail > 215) {
    //     tab_product_detail.addClass("hide");
    // }
    
    // collapse_tab_product_detail.on('click', function() {
    //     if (tab_product_detail.hasClass("hide")) {
    //         tab_product_detail.removeClass("hide");
    //         collapse_tab_product_detail.addClass("hide");
    //     }
    // });
    
    $("html,body").animate({
        scrollTop: 0
    }, 750);

    $('#btn-scroll-top').click(function() {
        $("html,body").animate({
            scrollTop: 0
        }, 750);
    });
    
    const diffslide_hot_products = $("#diffslide-hot-products");
    const difflist_sale_off_products = $("#difflist-sale-off-products");
    const main_homepage_site = $("#homepage-site");
    const footer_logo = $("footer .logo-footer");
    const footer_map = $("footer .footer-map-store");
    const productdetailpage_teaser_fix = $("#product-detail-page .product-teaser-fix");
    const viewmore_collection = $(".view-more.view-more-collection");
    
    if (viewmore_collection.length > 0) {
        viewmore_collection.each(function() {
            var get_collection_title = $(this).attr("data-collection-title");
            $.ajax({
                type: 'GET',
                url: '/api/v1_products/collections',
                success: function(response) {
                    var arr_collection = response.result.collection;
                    console.log(arr_collection);
                    
                    arr_collection.forEach(function(item) {
                        if (item.title == get_collection_title) {
                            viewmore_collection.find(".btn-more").attr("href", item.url);
                        }
                    });
                }
            });
        });
    }
    
    if (productdetailpage_teaser_fix.length > 0) {
        if ($(document).width() < 768) {
            productdetailpage_teaser_fix.addClass('mobile');
        }
        
        var quantiy_amount_addtocart_product_detail_main = $("#product-detail-page .product-detail .quantity #input-amount-add-to-cart");
        var quantiy_amount_addtocart_product_detail_teaser_fix = productdetailpage_teaser_fix.find(".quantity-teaser-fix #input-amount-add-to-cart");
        
        quantiy_amount_addtocart_product_detail_main.change(function() {
           quantiy_amount_addtocart_product_detail_teaser_fix.val($(this).val());
        });
        
        quantiy_amount_addtocart_product_detail_teaser_fix.change(function() {
           quantiy_amount_addtocart_product_detail_main.val($(this).val());
        });
    }
    
    if (main_homepage_site.length > 0) {
        var header_block_site = $("header.block-header");
        var header_homepage_logo = $(".logo-home");
        var header_diffhomepage_logo = $(".logo-diff-home");
        
        header_homepage_logo.removeClass("d-none");
        header_diffhomepage_logo.addClass("d-none");
        
        header_block_site.addClass('header-home-page');
    }
    
    if(diffslide_hot_products.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_products/products',
            success: function(response) {
                var arr_products = response.result;
                var getcontainer_slide_hot = $('#diffslide-hot-products #diffslide-products-carousel .carousel-inner');
                getcontainer_slide_hot.html('');
                
                var i = 0;
                arr_products.forEach(function(item) {
                    if (item.is_best_sale_product && item.product_in_stock) {
                        getcontainer_slide_hot.append(create_hot_products(item, i));
                        i++;
                    }
                });
            }
        });
    }
    
    function decodeHtml(str) {
        var map = {
            '&amp;': '&',
            '&lt;': '<',
            '&gt;': '>',
            '&quot;': '"',
            '&#039;': "'"
        };
        return str.replace(/&amp;|&lt;|&gt;|&quot;|&#039;/g, function(m) {return map[m];});
    }
    if(footer_map.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_common/address_map',
            success: function(response) {
                var footer_map_store = footer_map.find(".map-store");
                var parse_html_map = decodeHtml(response.result.address);
                footer_map_store.html(parse_html_map);
            }
        });
    }
    
    if(footer_logo.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_common/logo',
            success: function(response) {
                var image_logo = footer_logo.find(".image");
                image_logo.attr("style", "background-image: url(" + response.logo + ")");
            }
        });
    }
    
    if(difflist_sale_off_products.length > 0) {
        $.ajax({
            type: 'GET',
            url: '/api/v1_products/products',
            success: function(response) {
                var arr_products = response.result;
                var getcontainer_list_best_sale = $('#difflist-sale-off-products #list-best-sale-products');
                getcontainer_list_best_sale.html('');
                
                var i = 0;
                arr_products.forEach(function(item) {
                    if (item.percent_protduct != "" && item.product_in_stock && i <= 5) {
                        getcontainer_list_best_sale.append(create_best_sale_products(item));
                        i++;
                    }
                });
            }
        });
    }
    
    
    function create_hot_products(item, index) {
        var element_1 = "<div class='carousel-item text-center " + ((index === 0) ? 'active':'') + "'><div class='col-12 slide-product-item'><div class='product-info'>";
        var element_2 = (item.percent_protduct === "") ? "" : "<div class='product-highlight text-center'><span class='sale'>" + item.percent_protduct + "</span></div>";
        var element_3 = "<a href='" + item.href + "' title='" + item.name + "'><div class='product-image' style='background-image: url(" + item.thumb + ");'></div></a>";
        var element_4 = "<div class='product-name my-3'><a href='" + item.href + "' title='" + item.name + "' class='product-title'>" + ((item.name.length > 15) ? (item.name.slice(0, 15) + '...') : item.name) + "</a></div>";
        var element_5 = "<span class='product-price'><span class='price'>" + item.price + "</span></span>";
        var element_6 = "<span class='product-price-sale'><span class='price'>" + item.compare_price + "</span></span>";
        var element_7 = "</div></div></div>";
        
        return element_1 + element_2 + element_3 + element_4 + element_5 + element_6 + element_7;
    }
    
    function create_best_sale_products(item) {
        var element_1 = "<a href='" + item.href + "'><div class='d-flex list-product-item my-3'>";
        var element_2 = "<div class='col-4 product-image' style='background-image: url(" + item.thumb + ")'></div>";
        var element_3 = "<div class='col-8 product-info'>";
            var element_4 = "<h5 class='product-title'>" + ((item.name.length > 15) ? (item.name.slice(0, 15) + '...') : item.name) + "</h5>";
        
        // var element_5 = "<p class='product-price-sale'>" + item.compare_price + "</p><p class='product-price'>" + item.price + "</p>";
        if (item.multi_versions != 1) {
            if (item.price != "0 VND") {
                var element_5 = "<p class='product-price'>" + item.price + "</p><p class='product-price-sale'>" + item.compare_price + "</p>";
            } else {
                var element_5 = "<p><a class='cart-btn cart-contact' data-toggle='modal' data-target='#modal-buy'>Liên hệ</a></p>";
            }
        } else {
            if (item.compare_version_min != "0 VND") {
                var element_5 = "<p class='product-price'>" + item.compare_version_min + "~" + item.compare_version_max + "</p><p class='product-price-sale'>" + item.price_version_min + "~" + item.price_version_max + "</p>";
            } else {
                var element_5 = "<p><a class='cart-btn cart-contact' data-toggle='modal' data-target='#modal-buy'>Liên hệ</a></p>";
            }
        }
        var element_6 = "</div></div></a>";
        
        return element_1 + element_2 + element_3 + element_4 + element_5 + element_6;
    }
});