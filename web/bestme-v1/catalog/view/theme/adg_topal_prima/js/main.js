function owlInit(element, numItem, nav = true, dots = false, margin = 0, responsive = null) {
    if (element.length) {
        let interval = element.data('interval');
        if (responsive == null) {
            responsive = {
                0: {
                    items: 1,
                },
                600: {
                    items: Math.round(numItem / 2),
                },
                991: {
                    items: numItem,
                }
            }
        }
        element.owlCarousel({
            autoplay: typeof interval !== 'undefined',
            autoplayTimeout: interval,
            autoHeight: true,
            autoWidth: false,
            loop: true,
            responsiveClass: true,
            lazyLoad: true,
            dots: dots,
            nav: nav,
            margin: margin,
            navText: ['<i class="icon icon-arrow-left"></i>', '<i class="icon icon-arrow-right"></i>'],
            // URLhashListener:true,
            // startPosition: 'URLHash',
            responsive: responsive,
        });
    }
}

/**
 *
 * @param {String} elSelector
 * @param {int} ratio calc by width/height
 */
function resizeHeightElementRatio(elSelector, ratio = 1) {
    const element = jQuery(elSelector);
    if (element.length) {
        element.height(element.width() / ratio);
    }
}

window.onload = (event) => {
    resizeHeightElementRatio('.product-item img');
    resizeHeightElementRatio('.news-box .news-item img');
    resizeHeightElementRatio('.projects-item img');
    resizeHeightElementRatio('.projects-box img');
}

jQuery(document).ready(function ($) {
    // Menu Mobile
    const menuId = '.navbar-collapse.collapse';
    if ($(menuId).length > 0) {
        $(menuId).after('<div id="overlay"></div>');
        $('button.navbar-toggler').click(function () {
            $('#overlay').toggleClass('show');
        });

        $(menuId + ' i.close').click(function () {
            $('#overlay').removeClass('show');
            $(this).closest(menuId).removeClass('show');
        });
        $(window).click(function (e) {
            if ($(e.target).attr('id') === 'overlay') {
                $('#overlay').removeClass('show');
                $(menuId + '.show').removeClass('show');
            }
        });
    }

    // Menu responsive
    $('ul.nav > li > a>.toggle').on('click', function (e) {
        $(this).closest('li').toggleClass('active');
        e.preventDefault();
        return false;
    });

    // Header menu fix scroll
    const topHeaderHeight = $('.header-top').height();
    const headerWrap = '.block-header';

    function fixedMenu() {
        if ($(headerWrap).length === 0 || $('body').height() < $(window).height() * 1.5) return;
        let windowScrollTop = (window.pageYOffset || document.scrollTop) - (document.clientTop || 0);

        if (Number.isNaN(windowScrollTop) && $(headerWrap).hasClass('fixed')) {
            $(headerWrap).removeClass('fixed');
        }

        if (windowScrollTop >= topHeaderHeight && !$(headerWrap).hasClass('fixed')) {
            $(headerWrap).addClass('fixed');
        } else if (windowScrollTop <= topHeaderHeight && $(headerWrap).hasClass('fixed')) {
            $(headerWrap).removeClass('fixed');
        }
    }

    fixedMenu();
    const productTeaser = '.product-teaser-fix';
    $(document).scroll(function () {
        fixedMenu();

        // Box buy-product in product detail page
        let windowScrollTop = (window.pageYOffset || document.scrollTop) - (document.clientTop || 0);
        if ($(productTeaser).length > 0) {
            if ($(headerWrap).hasClass('fixed') && windowScrollTop >= topHeaderHeight + 500) {
                $(productTeaser).slideDown(300);
            } else {
                $(productTeaser).slideUp(300);
            }
        }
    });
    // End Header menu fix scroll

    // Header form search
    $(document).on('click', '.header-search .collapse-search a', function(e){
        console.log($(this));
        if($(this).hasClass('show-form')) {
            $(this).closest('.header-search').addClass('show');
        } else {
            $(this).closest('.header-search').removeClass('show');
        }
    })
    // End Header form search

    /*Set an element Full width*/
    const fullWidtElm = '.full-width';

    function sectionFullWidth() {
        let bodyWidth = $('body').width();
        let originWidth = $('.container').width();
        $(fullWidtElm).css({
            'width': bodyWidth + 'px',
            'max-width': bodyWidth + 'px',
            'margin-left': -(bodyWidth - originWidth) / 2 + 'px'
        });
    }

    sectionFullWidth();
    $(window).resize(function (e) {
        sectionFullWidth()
    });
    /*End Set an element Full width*/

});