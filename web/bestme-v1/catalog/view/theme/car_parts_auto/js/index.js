/**
 *
 * @param {String} elSelector
 * @param {int} ratio calc by width/height
 */
function resizeHeightElementRatio(elSelector, ratio = 1) {
    const element = jQuery(elSelector);
    if (element.length) {
        element.height(element.width() / ratio);
    }
}

$(document).ready(function () {
    $('.toogle-search').hover(function (e) {
        // e.preventDefault();
        $('.toogle-search').css('visibility', 'hidden');
        $(this).next().addClass('show');
    },function () {

    });
    $("body").click(function (e) {
        // e.stopPropagation();
        $('.search-sticky').removeClass('show');
        $('.toogle-search').css('visibility', 'visible');
    });
    $(".search-sticky").click(function (e) {
        e.stopPropagation();
    });
    $('.btn-showpass').click(function (e) {
        $(this).toggleClass('show');
        if ($('.form-group').hasClass('show')) {
            $('.input-pass').attr('type', 'text');
        }
    });
    $('.btn-wish').click(function (e) {
        e.preventDefault();
        $('.btn-wish').toggleClass('active');
    });

    var swiper = new Swiper('.swiper-product', {
        slidesPerView: jQuery('.swiper-product').attr('data-number-item'),
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            // when window width is <= 320px
            320: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            // when window width is <= 480px
            480: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            991: {
                slidesPerView: 3,
                spaceBetween: 20
            },
            // when window width is <= 1200px
            1200: {
                slidesPerView: 3,
                spaceBetween: 30
            }
        },
        autoplay: {
            delay: $('.swiper-product').data('time'),
        },
    });

    const autoplay = 1 == $('.swiper-product-detail').data('autoplay') ? { delay: $('.swiper-product-detail').data('autoplay_time') * 1e3 } : false;
    var swiper = new Swiper('.swiper-product-detail', {
        loop: 1 == $('.swiper-product-detail').data('loop'),
        autoplay: autoplay,
        slidesPerView: jQuery('.swiper-product-detail').attr('data-number-item'),
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        /*breakpoints: {
            // when window width is <= 320px
            320: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            // when window width is <= 480px
            480: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            991: {
                slidesPerView: 3,
                spaceBetween: 20
            },
            // when window width is <= 1200px
            1200: {
                slidesPerView: 3,
                spaceBetween: 30
            }
        },*/
    });

    var swiper = new Swiper('.swiper-partner', {
        slidesPerView: jQuery('.swiper-partner').attr('data-limit-inline'),
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            // when window width is <= 320px
            320: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            // when window width is <= 480px
            480: {
                slidesPerView: 2,
                spaceBetween: 0
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            991: {
                slidesPerView: 3,
                spaceBetween: 20
            },
            // when window width is <= 640px
            1200: {
                slidesPerView: 4,
                spaceBetween: 30
            }
        },
        // autoplay: {
        //     delay: $('.swiper-product').data('time'),
        // },
    });
    var swiper = new Swiper('.swiper-banner-footer', {
        slidesPerView: 1,
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        autoplay: {
            delay: $('.swiper-banner-footer').data('time'),
        },
    });
    var swiper = new Swiper('.swiper-news', {
        slidesPerView: 1,
        spaceBetween: 30,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        autoplay: {
            delay: $('.swiper-news').data('time'),
        },
    });

    $('.owl-home-banner').owlCarousel({
        loop: true,
        margin: 10,
        autoplay: true,
        nav: true,
        navText : ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
        autoplayTimeout: $('.owl-home-banner').data('time'),
        smartSpeed: 1000,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })

    // owlInit($('#owl-rate'), 1, false, true, 32);

    $('#owl-rate').owlCarousel({
        loop: $('#owl-rate').data('loop'),
        margin: 10,
        autoplay: $('#owl-rate').data('autoplay'),
        nav: false,
        // navText : ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
        autoplayTimeout: $('#owl-rate').data('autoplay_time') * 1000,
        smartSpeed: 1000,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })


    $('.owl-partner').owlCarousel({
        loop: true,
        margin: 100,
        autoplay: false,
        nav: false,
        navText: ["<img src='images/arow-left.png'>", "<img src='images/arow-right.png'>"],
        dots: false,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })
    $('.btn-like').click(function () {
        $(this).toggleClass('active');
    })

    /** Menu mobile **/
    $(".accodition").click(function () {
        $(this).next().slideToggle('fast');
        $(this).toggleClass('rotate');
    });
    $(".open-sidemenu").click(function () {
        $('#sidenav').toggleClass('menu-mobile');
        $('.block-overlay').toggleClass('over');
        $('body').toggleClass('no-scroll');
        $('.block-header-sale').addClass('slow-layer');
    });
    $(".block-overlay").click(function () {
        $('#sidenav').toggleClass('menu-mobile');
        $(this).toggleClass('over');
        $('body').toggleClass('no-scroll');
        $('.block-header-sale').removeClass('slow-layer');
    });
    /** Menu mobile **/

    $(function () {
        var star = '.star',
            selected = '.selected';

        $(star).on('click', function () {
            $(selected).each(function () {
                $(this).removeClass('selected');
            });
            $(this).addClass('selected');
        });

    });
});

// sidemenu
$(window).resize(function () {
    if ($(window).width() <= 768) {
        $('#sidenav').removeClass('main-menu');
    } else {
        $('#sidenav').addClass('main-menu');
    }
});
$(document).ready(function () {
    if ($(window).width() <= 768) {
        $('#sidenav').removeClass('main-menu');
    } else {
        $('#sidenav').addClass('main-menu');
    }
});
// slide price
$(function () {
    $("#slider-range").slider({
        range: true,
        min: 0,
        max: 7000000,
        values: [50000, 5000000],
        slide: function (event, ui) {
            $("#amount").val(ui.values[0] + "đ" + " - " + ui.values[1] + "đ");
        }
    });
    $("#amount").val($("#slider-range").slider("values", 0) + "đ" +
        " - " + $("#slider-range").slider("values", 1) + "đ");
});
// double owl
document.addEventListener("DOMContentLoaded", function () {
    console.log("start");
    let galleries = document.querySelectorAll(".gallery");

    Array.prototype.forEach.call(galleries, function (el, i) {
        const $this = $(el);
        const $owl1 = $this.find(".owl-1");
        const $owl2 = $this.find(".owl-2");
        let flag = false;
        let duration = 300;

        $owl1
            .on("initialized.owl.carousel", function (e) {
                resizeHeightElementRatio('.owl-1 .owl-item .img');
            })
            .owlCarousel({
                items: 1,
                lazyLoad: false,
                loop: true,
                dots: false,
                margin: 10,
                nav: false,
                responsiveClass: true
            })
            .on("changed.owl.carousel", function (e) {
                if (!flag) {
                    flag = true;
                    $owl2
                        .find(".owl-item")
                        .removeClass("current")
                        .eq(e.item.index)
                        .addClass("current");
                    if (
                        $owl2
                            .find(".owl-item")
                            .eq(e.item.index)
                            .hasClass("active")
                    ) {} else {
                        $owl2.trigger("to.owl.carousel", [e.item.index, duration, true]);
                    }
                    flag = false;
                }
            });

        $owl2
            .on("initialized.owl.carousel", function () {
                $owl2
                    .find(".owl-item")
                    .eq(0)
                    .addClass("current");

                resizeHeightElementRatio('.owl-2 .owl-item .thumb');
            })
            .owlCarousel({
                items: 4,
                lazyLoad: false,
                loop: false,
                margin: 20,
                nav: false,
                dots: false,
                responsiveClass: true
            })
            .on("click", ".owl-item", function (e) {
                e.preventDefault();
                var number = $(this).index();
                $owl1.trigger("to.owl.carousel", [number, duration, true]);
            });
    });
});

// // change quanlity
$(document).ready(function () {
    jQuery('.quantity').each(function () {
        var spinner = jQuery(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min'),
            max = input.attr('max');

        btnUp.click(function () {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

        btnDown.click(function () {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

    });
});

//Select tag
var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-selectag");
for (i = 0; i < x.length; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    /*for each element, create a new DIV that will act as the selected item:*/
    a = document.createElement("DIV");
    a.setAttribute("class", "select-selected");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);
    /*for each element, create a new DIV that will contain the option list:*/
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 1; j < selElmnt.length; j++) {
        /*for each option in the original select element,
        create a new DIV that will act as an option item:*/
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function (e) {
            /*when an item is clicked, update the original select box,
            and the selected item:*/
            var y, i, k, s, h;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            h = this.parentNode.previousSibling;
            for (i = 0; i < s.length; i++) {
                if (s.options[i].innerHTML == this.innerHTML) {
                    s.selectedIndex = i;
                    h.innerHTML = this.innerHTML;
                    y = this.parentNode.getElementsByClassName("same-as-selected");
                    for (k = 0; k < y.length; k++) {
                        y[k].removeAttribute("class");
                    }
                    this.setAttribute("class", "same-as-selected");
                    break;
                }
            }
            h.click();
        });
        b.appendChild(c);
    }
    x[i].appendChild(b);
    a.addEventListener("click", function (e) {
        /*when the select box is clicked, close any other select boxes,
        and open/close the current select box:*/
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
    });
}

function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);

// sticky buy now
$(document).ready(function() {
    $(window).scroll(function() {
        var topHeaderHeight = $('#block-header-menu').outerHeight();
        if (550 <= $(this).scrollTop()) {
            $('#buy-now').addClass("fixed");
            $('#buy-now').css('top', topHeaderHeight+"px");
        } else {
            $('#buy-now').removeClass("fixed");
        }
    });
});
