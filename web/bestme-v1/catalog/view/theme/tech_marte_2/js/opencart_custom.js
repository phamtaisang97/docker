function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
    return '';
}

// Cart add remove functions
var cart = {
    'add': function(product_id, quantity) {
        var valid = validateSerialize();
        if (!valid) {
            $('input[name="quantity"]')[0].reportValidity();
            return false;
        }
        if (jQuery('#add-to-cart button[type="submit"]').attr('data-status') == "disabled") {
            return false;
        }
        $.ajax({
            url: 'index.php?route=checkout/my_orders/addCart',
            type: 'post',
            data: $('#add-to-cart').serialize(),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('.amount .quantity span').html(json['total_product_cart']);

                    $('.amount .total').html(json['total_into_money']);

                    $('#add_cart_notify').modal('show');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'update': function(key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var voucher = {
    'add': function() {

    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var wishlist = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);

                $('html, body').animate({ scrollTop: 0 }, 'slow');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}

var compare = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#compare-total').html(json['total']);

                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
    e.preventDefault();

    $('#modal-agree').remove();

    var element = this;

    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function(data) {
            html  = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div>';
            html += '  </div>';
            html += '</div>';

            $('body').append(html);

            $('#modal-agree').modal('show');
        }
    });
});
/// update product detail when choose version
$(document).ready(function () {
    if (jQuery('#add-to-cart .attr-item').length) {
        ajaxChangeVersion();
    }
    jQuery('#add-to-cart .attr-item input').on('click', function () {
        ajaxChangeVersion();
    });
});

function ajaxChangeVersion() {
    $.ajax({
        url: 'index.php?route=product/product/updateCartWhenChangeVersion',
        type: 'post',
        data: $('#add-to-cart').serialize(),
        dataType: 'json',
        beforeSend: function() {
            //TODO loading
        },
        complete: function() {
            //TODO loaded
        },
        success: function(json) {
            if (json['success']) {
                updateHtmlVersion(json['sale_on_out_of_stock'], json['product_version']);
            } else {
                jQuery('#add-to-cart button').attr('disabled', 'disabled');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}

function updateHtmlVersion(sale_on_out_of_stock, product) {
    // version
    if (product['version']) {
        // form mua tra gop
        var version = product['version'].replace(",", " - ")
        $('input#pAttribute').val(version)
    }

    //update price
    if (product['price'] == 0) {
        var compare_price = parseInt(product['compare_price_format']);

        jQuery('.product-detail .price').html(compare_price === 0
            ? '<a href="javascript:;" data-toggle="modal" data-target="#modal-buy" class="btn btn-buy btn-outline-secondary main-bg">\n' +
            '                                        <span class="font-20 text-white">\n' + 'Liên hệ' + '</span>\n' +
            '                                    </a>'
            : '<span class="font-28 font-weight-bold mr-4 second-color">\n' + product['compare_price_format'] + '</span>\n');

        // jQuery('.product-detail .price span').html(compare_price === 0 ? 'Liên hệ' : product['compare_price_format']);

        jQuery('.product-detail .price del').html('');
        jQuery('.product-teaser-fix .second-color').html(' - ' + product['version'] + (compare_price === 0 ? '' : ' - ' + product['compare_price_format']));
    } else {
        var comparePrice = parseInt(product['compare_price_format']);
        if (parseInt(product['price_format']) === 0) {
            jQuery('.product-detail .price').html(comparePrice === 0
                ? '<a href="javascript:;" data-toggle="modal" data-target="#modal-buy" class="btn btn-buy btn-outline-secondary main-bg">\n' +
                '   <span class="font-20 text-white">\n' + 'Liên hệ' + '</span>\n' +
                ' </a>'
                : '<span class="font-28 font-weight-bold mr-4 second-color">\n' +
                product['compare_price_format'] +
                '                                        </span>\n');
        } else {
            jQuery('.product-detail .price').html('<span class="font-28 font-weight-bold mr-4 second-color">' + product['price_format'] + '</span>'+'<del class="font-18 fifth-color mt-2 font-weight-400">'+product['compare_price_format']+'</del>');
            // jQuery('.product-detail .price del').html('<del class="font-18 fifth-color mt-2 font-weight-400">'+product['compare_price_format']+'</del>');
            jQuery('.product-teaser-fix .second-color').html(product['version'] + ' - ' + product['price_format']);
        }
    }

    if (parseInt(product['compare_price_format']) === 0) {
        jQuery('.product-detail .block-custom-change .buy-now-mobile button').prop('disabled', true);
        jQuery('.product-detail .block-custom-change .buy-now-mobile').css('visibility', 'hidden');
        jQuery('.product-teaser-fix button').css('visibility', 'hidden');
        jQuery('#add-to-cart button').css('visibility', 'hidden');
        jQuery('#add-to-cart input').css('visibility', 'hidden');
    } else {
        jQuery('.product-detail .block-custom-change .buy-now-mobile button').removeAttr('disabled');
        jQuery('.product-detail .block-custom-change .buy-now-mobile').css('visibility', 'visible');
        jQuery('.product-teaser-fix button').css('visibility', 'visible');
        jQuery('#add-to-cart button').css('visibility', 'visible');
        jQuery('#add-to-cart input').css('visibility', 'visible');
    }
    //active button add cart
    //TODO : remove if check is_stock done
    // if (product['status'] == 1 && (sale_on_out_of_stock == 1 || (sale_on_out_of_stock == 0 && product['quantity'] > 0))) {
    if (product['status'] == 1 && (sale_on_out_of_stock == 1)) {
        jQuery('#add-to-cart button').removeAttr('data-status');
        jQuery('.product-teaser-fix button').removeAttr('data-status');
        jQuery('.product-detail .block-status span').html('Còn hàng');
    } else {
        jQuery('#add-to-cart button').attr('data-status', 'disabled');
        jQuery('.product-teaser-fix button').attr('data-status', 'disabled');
        jQuery('.product-detail .block-status span').html('Hết hàng');
    }
}

function validateSerialize() {
    var invalidFields = jQuery('#add-to-cart').find( ":invalid" );
    if (invalidFields.length > 0) return false;
    return true;
}