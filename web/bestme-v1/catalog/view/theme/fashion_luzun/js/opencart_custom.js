function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

// Cart add remove functions
var cart = {
    'add': function(product_id, quantity) {
        var valid = validateSerialize();
        if (!valid) {
            $('input[name="quantity"]')[0].reportValidity();
            return false;
        }
        if (jQuery('#add-to-cart button[type="submit"]').attr('data-status') == "disabled") {
            return false;
        }
        $.ajax({
            url: 'index.php?route=checkout/my_orders/addCart',
            type: 'post',
            data: $('#add-to-cart').serialize(),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('.cart .count').html(json['total_product_cart']);

                    $('#add_cart_notify').modal('show');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'update': function(key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var voucher = {
    'add': function() {

    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var wishlist = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);

                $('html, body').animate({ scrollTop: 0 }, 'slow');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}

var compare = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#compare-total').html(json['total']);

                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
    e.preventDefault();

    $('#modal-agree').remove();

    var element = this;

    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function(data) {
            html  = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div>';
            html += '  </div>';
            html += '</div>';

            $('body').append(html);

            $('#modal-agree').modal('show');
        }
    });
});
/// update product detail when choose version
$(document).ready(function () {
    if (jQuery('#add-to-cart .form-check-input').length) {
        ajaxChangeVersion();
    }
    jQuery('#add-to-cart .list-inline-item input').on('click', function () {
        ajaxChangeVersion();
    });
});

function ajaxChangeVersion() {
    $.ajax({
        url: 'index.php?route=product/product/updateCartWhenChangeVersion',
        type: 'post',
        data: $('#add-to-cart').serialize(),
        dataType: 'json',
        beforeSend: function() {
            //TODO loading
        },
        complete: function() {
            //TODO loaded
        },
        success: function(json) {
            if (json['success']) {
                updateHtmlVersion(json['sale_on_out_of_stock'], json['product_version']);
            } else {
                jQuery('#add-to-cart button').attr('data-status', 'disabled');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}

function updateHtmlVersion(sale_on_out_of_stock, product) {
    // version
    if (product['version']) {
        // form mua tra gop
        var version = product['version'].replace(",", " - ")
        $('input#pAttribute').val(version)
    }

    //update product version
    if (product['version']) {
        if (parseInt(product['compare_price_format']) === 0) {
            jQuery('#buy-now .product-version').text(' - ' + product['version']);
            jQuery('#buy-now button').hide();
        } else {
            jQuery('#buy-now .product-version').text(' - ' + product['version'] + ' - ' + ((product['price'] == 0) ? product['compare_price_format'] : product['price_format']));
            jQuery('#buy-now button').show();
        }
    } else {
        jQuery('#buy-now .product-version').text(' - ' + ((product['price'] == 0) ? product['compare_price_format'] : product['price_format']));
    }

    //update price
    if (product['price'] == 0) {
        var compare_price = parseInt(product['compare_price_format']);
        jQuery('.pro-detail .pro-price .new-price').html('<span class="price">'+ (compare_price === 0 ?
            '<a href="javascript:;" data-toggle="modal" data-target="#modal-buy" class="btn btn-buy">\n'
            + '<span class="new-price fw-700 fz-28">'
            + 'Liên hệ' + '</span>\n' + '</a>'
            : '<span class="new-price text-primary fw-700 fz-28">'+ product['compare_price_format']) +'</span>');
        jQuery('.pro-detail .pro-price .old-price').html('<span class="price"></span>');
    } else {
        if (parseInt(product['price_format']) === 0) {
            jQuery('.pro-detail .pro-price .new-price').html('<span class="price">'
                + '<a href="javascript:;" data-toggle="modal" data-target="#modal-buy" class="btn btn-buy">\n'
                + '<span class="new-price fw-700 fz-28">'
                + 'Liên hệ' + '</span>\n' + '</a>'
                +'</span>');
        } else {
            jQuery('.pro-detail .pro-price .new-price').html('<span class="price">'+ product['price_format'] +'</span>');
            jQuery('.pro-detail .pro-price .old-price').html('<span class="price">'+ product['compare_price_format'] +'</span>');
        }
    }

    if (parseInt(product['compare_price_format']) === 0) {
        jQuery('.pro-detail .block-custom-change button').prop('disabled', true);
        jQuery('.pro-detail .block-custom-change .block-action-cart').hide();
        jQuery('.pro-detail .block-custom-change .buy-now-mobile').css('visibility', 'hidden');
    } else {
        jQuery('.pro-detail .block-custom-change button').removeAttr('disabled');
        jQuery('.pro-detail .block-custom-change .block-action-cart').show();
        jQuery('.pro-detail .block-custom-change .buy-now-mobile').css('visibility', 'visible');
    }
    //active button add cart
    //TODO : remove if check is_stock done
    // if (product['status'] == 1 && (sale_on_out_of_stock == 1 || (sale_on_out_of_stock == 0 && product['quantity'] > 0))) {
    if (product['status'] == 1 && (sale_on_out_of_stock == 1)) {
        jQuery('#add-to-cart button').removeAttr('data-status');
        jQuery('#buy-now button').removeAttr('data-status');
        jQuery('.product-teaser-fix button').removeAttr('data-status');
        jQuery('.pro-detail .block-status span.text-primary').html('Còn hàng');
    } else {
        jQuery('#add-to-cart button').attr('data-status', 'disabled');
        jQuery('#buy-now button').attr('data-status', 'disabled');
        jQuery('.product-teaser-fix button').attr('data-status', 'disabled');
        jQuery('.pro-detail .block-status span.text-primary').html('Hết hàng');
    }
}

function validateSerialize() {
    var invalidFields = jQuery('#add-to-cart').find( ":invalid" );
    if (invalidFields.length > 0) return false;
    return true;
}

/// v2.8.3
$(document).on('click', '.click-to-show-search', function () {
    $('#form-search-scroll').show();
    $('input#input-search-scroll').focus();
});

$(document).on("focusout", "#form-search-scroll", function(){
    $('#form-search-scroll').hide();
});

$(document).on('click', '.click-filter-show', function () {
    if ($('.sidebar-left-search').hasClass('show')) {
        $('.sidebar-left-search').removeClass('show');
    } else {
        $('.sidebar-left-search').addClass('show');
    }

    if ($('#navbarMainMenu').hasClass('show')) {
        $('#navbarMainMenu').removeClass('show');
    }
});

$(document).on('click', 'body', function (e) {
    if($(e.target).is('.click-filter-show i')){
        e.preventDefault();
        return;
    }

    if ($('.sidebar-left-search').hasClass('show')) {
        $('.sidebar-left-search').removeClass('show');
    }
    if ($('#navbarMainMenu').hasClass('show')) {
        $('#navbarMainMenu').removeClass('show');
    }
});