function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

// Cart add remove functions
var cart = {
    'add': function(product_id, quantity) {
        var valid = validateSerialize();
        if (!valid) {
            $('input[name="quantity"]')[0].reportValidity();
            return false;
        }
        if (jQuery('#add-to-cart button[type="submit"]').attr('data-status') == "disabled") {
            return false;
        }
        $.ajax({
            url: 'index.php?route=checkout/my_orders/addCart',
            type: 'post',
            data: $('#add-to-cart').serialize(),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('.count-cart').html(json['total_product_cart']);

                    $('#add_cart_notify').modal('show');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'update': function(key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var voucher = {
    'add': function() {

    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var wishlist = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);

                $('html, body').animate({ scrollTop: 0 }, 'slow');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}

var compare = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#compare-total').html(json['total']);

                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
    e.preventDefault();

    $('#modal-agree').remove();

    var element = this;

    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function(data) {
            html  = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div>';
            html += '  </div>';
            html += '</div>';

            $('body').append(html);

            $('#modal-agree').modal('show');
        }
    });
});
/// update product detail when choose version
$(document).ready(function () {
    if (jQuery('#add-to-cart .form-check-input').length) {
        ajaxChangeVersion();
    }
    jQuery('#add-to-cart .form-check-input').on('click', function () {
        ajaxChangeVersion();
    });
});

function ajaxChangeVersion() {
    $.ajax({
        url: 'index.php?route=product/product/updateCartWhenChangeVersion',
        type: 'post',
        data: $('#add-to-cart').serialize(),
        dataType: 'json',
        beforeSend: function() {
            //TODO loading
        },
        complete: function() {
            //TODO loaded
        },
        success: function(json) {
            if (json['success']) {
                if (json.sale_on_out_of_stock == 1 && 1 == json.product_version.status){
                    jQuery('button.button_buying').attr('data-status', '');
                    jQuery('button.btn-addcart-bestme').attr('data-status', '');
                    jQuery('a.btn-buynow-bestme').attr('data-status', '');
                    jQuery('a.add-cart-bestme').attr('data-status', '');
                } else {
                    jQuery('button.button_buying').attr('data-status', 'disabled');
                    jQuery('button.btn-addcart-bestme').attr('data-status', 'disabled');
                    jQuery('a.btn-buynow-bestme').attr('data-status', 'disabled');
                    jQuery('a.add-cart-bestme').attr('data-status', 'disabled');
                }

                updateHtmlVersion(json['sale_on_out_of_stock'], json['product_version']);
            } else {
                jQuery('button.button_buying').attr('data-status', 'disabled');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}

function updateHtmlVersion(sale_on_out_of_stock, product) {
    // version
    if (product['version']) {
        // form mua tra gop
        var version = product['version'].replace(",", " - ")
        $('input#pAttribute').val(version)
    }

    //update product version
    if (product['version']) {
        if (parseInt(product['compare_price_format']) === 0) {
            jQuery('.header-product-version-text').text(' - ' + product['version']);
        } else {
            jQuery('.header-product-version-text').text(' - ' + product['version']);
        }
    }

    //update price
    var compare_price = parseInt(product['compare_price_format']);
    if (product['price'] == 0) {
        jQuery('.pro-price.detail').html('<span class="price text-left">'+ (compare_price === 0
            ? '<span class="new-price fw-700 fz-36 d-flex">' + '<span data-toggle="modal" data-target="#modal-buy" class="btn btn-primary">Liên hệ</span>' + '</span>' +
              '    <span class="old-price fz-18 text-second mt-n3 mb-3 text-left">' +
              '        <del><span class="price"></span></del>' +
              '    </span>'
            : '<span class="new-price fw-700 fz-36 d-flex">' + '<span class="price">'+ product['compare_price_format'] + '</span></span>' +
              '<span class="old-price fz-18 text-second mt-n3 mb-3 text-left">' +
              '<del><span class="price">'+ product['price_format'] +'</span></del>' +
              '</span>')
        );
        jQuery('.pro-price.detail .old-price').html('');
        // jQuery('p.old-price-after').html('<del><span class="old-price fz-18 text-second mt-n3 mb-3 text-left">'+ product['compare_price_format'] +'</span></del>');
        // giá sản phẩm header
        jQuery('.pro-price-header').html('<span class="price">'+ (compare_price === 0 ?
            '<a data-toggle="modal" data-target="#modal-buy">\n'
            + '<span class="pro-price-header new-price fw-700">' + '' + '</span></a>'
            : '<span class="pro-price-header new-price fw-700">' + product['compare_price_format'] + '</span>'));
    } else {
        if (parseInt(product['price_format']) === 0) {
            jQuery('.pro-price.detail').html(comparePrice === 0
                ? '<a data-toggle="modal" data-target="#modal-buy" class="btn btn-buy">\n'
                + '<span class="new-price fw-700 fz-36 d-flex">' + '<span class="btn btn-primary">Liên hệ</span>' + '</span></a>'
                : '<span class="new-price fw-700 fz-36 d-flex">' + '<span class="price">'+ product['compare_price_format'] + '</span></span>'
            );
        } else {
            jQuery('.pro-price.detail').html('<span class="new-price fw-700 fz-36 d-flex">' +
                '<span class="price">'+ product['price_format'] +'</span>' +
                '</span>' +
                '<br>' +
               '<span class="old-price fz-18 text-second mt-n3 mb-3 text-left">' +
                '<del><span class="price">'+ product['compare_price_format'] +'</span></del>' +
                '</span>'
            );
            jQuery('.pro-price-header .new-price').html('<span class="price">'+ product['price_format'] +'</span>');

        }
    }
    if (parseInt(product['compare_price_format']) === 0) {
        $('.pro-action').css('visibility','hidden');
        $('.button_buying').css('visibility','hidden');
        $('.add-cart-bestme').css('visibility','hidden');
    }
    else{
        $('.pro-action').css('visibility','visible');
        $('.button_buying').css('visibility','visible');
        $('.add-cart-bestme').css('visibility','visible');
    }
    //update status
    jQuery('.code-product').html(product['sku']);
    //active button add cart
    //TODO : remove if check is_stock done
    // if (product['status'] == 1 && (sale_on_out_of_stock == 1 || (sale_on_out_of_stock == 0 && product['quantity'] > 0))) {
    if (product['status'] == 1 && (sale_on_out_of_stock == 1)) {
        jQuery('.tinh-trang').html('<span class="true"><i class="fa fa-check-square-o" aria-hidden="true"></i>Còn hàng</span>');
    } else {
        jQuery('.tinh-trang').html('<span class="false">Hết hàng</span>');
    }

    /* update product version image */
    // set default values
    product.image = product.image ? product.image : jQuery('#product-image-default-src').val();
    product.image_alt = product.image_alt ? product.image_alt : '';
    if (product.image) {
        const indicatorSlider = jQuery('#carouselProDetail .carousel-indicators');
        let nextProductIndex = jQuery('#carouselProDetail .carousel-inner .carousel-item').length;
        let isLoadOtherVersion = false;
        if (nextProductIndex > 0) {
            // slider has at least 1 image
            // get last product image
            const lastProductImage = jQuery('#carouselProDetail .carousel-inner .carousel-item').last();
            // check if it is product version image and not this version
            isLoadOtherVersion = lastProductImage.data('version') !== product.version;
            if (lastProductImage.length > 0 && lastProductImage.hasClass('product-version-image') && isLoadOtherVersion) {
                // remove last main slider image
                lastProductImage.remove();
                // decrease next product index to current max product index
                --nextProductIndex;
                // remove last indicator slider image
                indicatorSlider.trigger('remove.owl.carousel', [nextProductIndex]);
            }
        }

        if (isLoadOtherVersion) {
            // remove current product image active class
            jQuery('#carouselProDetail .carousel-inner .carousel-item.active').removeClass('active');

            // add product version image to the end of product image slider
            jQuery(`<div class="carousel-item active product-version-image" data-version="${product.version}">
                <img src="${product.image}" alt="${product.image_alt}">
            </div>`).appendTo(jQuery('#carouselProDetail .carousel-inner'));
            jQuery('#carouselProDetail').carousel();

            // add product version image to the end of indicator image slider
            const productImageIndicatorHtml = `<div class="item img-thumb img-version-thumb">
                <a data-target="#carouselProDetail" data-slide-to="${nextProductIndex}">
                    <img src="${product.image}" alt="${product.image_alt}" class="img-wrap">
                </a>
            </div>`;
            indicatorSlider.trigger('add.owl.carousel', [productImageIndicatorHtml]);

            // refresh indicator slider
            indicatorSlider.trigger('refresh.owl.carousel');
        } else {
            // not insert this product version image => decrease next product index
            --nextProductIndex;
        }

        // go to product version image slide
        jQuery('#carouselProDetail').carousel(nextProductIndex);
        indicatorSlider.trigger('to.owl.carousel', [nextProductIndex]);
    }
}

function validateSerialize() {
    var invalidFields = jQuery('#add-to-cart').find( ":invalid" );
    if (invalidFields.length > 0) return false;
    return true;
}