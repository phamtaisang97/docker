$(document).ready(function () {
    $('.toogle-search').hover(function (e) {
        // e.preventDefault();
        $('.toogle-search').css('visibility', 'hidden');
        $(this).next().addClass('show');
    },function () { 

     });
    $("body").click(function (e) {
        // e.stopPropagation();
        $('.search-sticky').removeClass('show');
        $('.toogle-search').css('visibility', 'visible');
    });
    $(".search-sticky").click(function (e) {
        e.stopPropagation();
    });
    $('.btn-showpass').click(function (e) {
        $(this).toggleClass('show');
        if ($('.form-group').hasClass('show')) {
            $('.input-pass').attr('type', 'text');
        }
    });

    var swiper = new Swiper('.swiper-container', {
        slidesPerView: jQuery('.swiper-container').attr('data-limit-inline'),
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            // when window width is <= 320px
            320: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            // when window width is <= 480px
            480: {
                slidesPerView: 2,
                spaceBetween: 0
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            991: {
                slidesPerView: 3,
                spaceBetween: 20
            },
            // when window width is <= 640px
            1200: {
                slidesPerView: 4,
                spaceBetween: 30
            }
        },
    });

    var swiperPartner = new Swiper('.swiper-partner', {
        slidesPerView: jQuery('.swiper-partner').attr('data-limit-inline'),
        spaceBetween: 30,
        loop: jQuery('.swiper-partner').attr('data-loop') == 1 ? true : false,
        autoplay: jQuery('.swiper-partner').attr('data-auto-play') == 1 ? true : false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            // when window width is <= 320px
            320: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            // when window width is <= 480px
            480: {
                slidesPerView: 2,
                spaceBetween: 0
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            991: {
                slidesPerView: 3,
                spaceBetween: 20
            },
            // when window width is <= 640px
            1200: {
                slidesPerView: 4,
                spaceBetween: 30
            }
        },
        // autoplay: {
        //     delay: $('.swiper-product').data('time'),
        // },
    });

    $('.owl-banner').owlCarousel({
        loop: true,
        nav: true,
        autoplay: true,
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 1
            }
        }
    })
    $('.owl-saleoff-product').owlCarousel({
        loop: true,
        margin: 30,
        autoplay: false,
        nav: true,
        navText: ["<img src='images/arow-left.png'>", "<img src='images/arow-right.png'>"],
        dots: false,
        responsive: {
            0: {
                items: 1,
            },
            576: {
                items: 2
            },
            769: {
                items: 2
            },
            991: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    })
    $('.owl-partner').owlCarousel({
        loop: true,
        margin: 100,
        autoplay: true,
        nav: false,
        navText: ["<img src='images/arow-left.png'>", "<img src='images/arow-right.png'>"],
        dots: false,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    })
    $('.btn-like').click(function () {
        $(this).toggleClass('active');
    })

    /** Menu mobile **/
    $(".accodition").click(function () {
        $(this).next().slideToggle('fast');
        $(this).toggleClass('rotate');
    });
    $(".open-sidemenu").click(function () {
        $('#sidenav').toggleClass('menu-mobile');
        $('.block-overlay').toggleClass('over');
        $('body').toggleClass('no-scroll');
        $('.block-header-sale').addClass('slow-layer');
    });
    $(".block-overlay").click(function () {
        $('#sidenav').toggleClass('menu-mobile');
        $(this).toggleClass('over');
        $('body').toggleClass('no-scroll');
        $('.block-header-sale').removeClass('slow-layer');
    });
    /** Menu mobile **/

    $(function () {
        var star = '.star',
            selected = '.selected';

        $(star).on('click', function () {
            $(selected).each(function () {
                $(this).removeClass('selected');
            });
            $(this).addClass('selected');
        });

    });
});
// menu

// modal
$(document).ready(function () {
    // popup //
    $('.button-popup').click(function () {
        console.log('hsdgfahsgfhjsgfhsg')
        var $parent = $(this).closest('.bdt-item');
        var itemId = $(this).attr('data-id');
        var buttonId = $(this).attr('id');
        $('#modal-container').removeAttr('class').addClass(buttonId);
        $('body').addClass('modal-active');
    });

    $('.close-btn').click(function () {
        $('#modal-container').addClass('out');
        $('body').removeClass('modal-active');
    });
    $('.modal-background').click(function () {
        $('#modal-container').addClass('out');
        $('body').removeClass('modal-active');
    });
    $(".modal-background .modal").click(function (e) {
        e.stopPropagation();
    });
    if ($(window).width() > 767) {
        if ($(".modal-background .modal").height() > $(window).height()) {
            $('#modal-container').css('display', 'block');
            $('#modal-container').css('overflow-y', 'scroll');
            $('#modal-container .modal-background').css('display', 'block');
        } else {
            $('#modal-container').css('display', 'table');
            $('#modal-container').css('overflow-y', 'auto');
            $('#modal-container .modal-background').css('display', 'table-cell');
        }
    } else {

    }
    if ($(window).width() <= 768) {
        $('#sidenav').removeClass('main-menu');
    } else {
        $('#sidenav').addClass('main-menu');
    }
});
// sidemenu
$(window).resize(function () {
    if ($(window).width() <= 768) {
        $('#sidenav').removeClass('main-menu');
    } else {
        $('#sidenav').addClass('main-menu');
    }
});

$(document).ready(function() {
    var x = $("#block-header").outerHeight();
    $(window).scroll(function() {
        if ($(this).scrollTop() > x) {
            $('#block-header-menu').addClass("fixed");
        } else {
            $('#block-header-menu').removeClass("fixed");
        }
    });
});


//click show search mobile 
$("html").click(function() {
    $(".list-category").slideUp();
    $(".search-mobile .form-group").removeClass("on");
});
$(".show-search-mb").click(function(e) {
    e.stopPropagation();
    $(".search-mobile .form-group").toggleClass("on");
});
$(".search-mobile .form-group").click(function(e) {
    e.stopPropagation();
});


$('.owl-detail').owlCarousel({
	loop: true,
	margin: 0,
	autoplay: true,
	nav: true,
    dots: true,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
	dotsSpeed: 1000,
	autoplaySpeed: 1000,
	navSpeed: 800,
	responsive: {
		0: {
			items: 1
		}
	}
});
$('.owl-product').owlCarousel({
	loop: true,
	margin: 30,
	autoplay: true,
	nav: false,
	dots: false,
	dotsSpeed: 1000,
	autoplaySpeed: 1000,
	navSpeed: 800,
	responsive: {
		0: {
			items: 1
        },
        420: {
			items: 2
        },
        600: {
			items: 3
        },
        768: {
			items: 4
        },
        1000:{
            items: 5
        },
        1200:{
            items: jQuery('.owl-product').attr('data-number-item'),
        }
    }
});

const autoplayTime = undefined === jQuery('.owl-product-detail').data('autoplay_time') ? 5 : jQuery('.owl-product-detail').data('autoplay_time')
$('.owl-product-detail').owlCarousel({
    loop: undefined !== jQuery('.owl-product-detail').data('loop') && 1 == jQuery('.owl-product-detail').data('loop'),
    margin: 30,
    autoplay: undefined !== jQuery('.owl-product-detail').data('autoplay') && 1 == jQuery('.owl-product-detail').data('autoplay'),
    autoplayTimeout: autoplayTime * 1e3,
    nav: false,
    dots: false,
    dotsSpeed: 1000,
    autoplaySpeed: 1000,
    navSpeed: 800,
    responsive: {
        0: {
            items: 1
        },
        420: {
            items: 2
        },
        600: {
            items: 3
        },
        768: {
            items: 4
        },
        1000:{
            items: 5
        },
        1200:{
            items: jQuery('.owl-product-detail').attr('data-number-item'),
        }
    }
});
// change quanlity
jQuery('.quantity').each(function () {
    var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

    btnUp.click(function () {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
            var newVal = oldValue;
        } else {
            var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
    });

    btnDown.click(function () {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
            var newVal = oldValue;
        } else {
            var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
    });

});

$(function () {
    $("#slider-range").slider({
        range: true,
        min: 0,
        max: 7000000,
        values: [50000, 5000000],
        slide: function (event, ui) {
            $("#amount").val(ui.values[0] + "đ" + " - " + ui.values[1] + "đ");
        }
    });
    $("#amount").val($("#slider-range").slider("values", 0) + "đ" +
        " - " + $("#slider-range").slider("values", 1) + "đ");
});

// matchheight
$(function () {
	$('.ttl-pro a').matchHeight();
});

// sticky
$(document).ready(function() {
    $(window).scroll(function() {
        var topHeaderHeight = $('#block-header-menu').outerHeight();
        if (550 <= $(this).scrollTop()) {
            $('#buy-now').addClass("fixed");
            $('#buy-now').css('top', topHeaderHeight+"px");
        } else {
            $('#buy-now').removeClass("fixed");
        }
    });
});
