function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

// Cart add remove functions
var cart = {
    'add': function(product_id, quantity) {
        var valid = validateSerialize();
        if (!valid) {
            $('input[name="quantity"]')[0].reportValidity();
            return false;
        }
        if (jQuery('#add-to-cart .btn-add.cart').attr('data-status') == "disabled") {
            return false;
        }
        $.ajax({
            url: 'index.php?route=checkout/my_orders/addCart',
            type: 'post',
            data: $('#add-to-cart').serialize(),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    $('.block-header-cart .count .number span').html(json['total_product_cart']);
                    $('.block-header-cart .count .price').html(json['total_into_money']);
                    $('.cart-mobile .number').html(json['total_product_cart']);
                    // $('#add_cart_notify').modal('show');
                    $('.block-cart').toggleClass('show-cart');
                    $('.block-over').toggleClass('over');
                    $('body').toggleClass('no-scroll');
                    ajaxGetOrderInfo();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'update': function(key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var voucher = {
    'add': function() {

    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var wishlist = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);

                $('html, body').animate({ scrollTop: 0 }, 'slow');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}

var compare = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#compare-total').html(json['total']);

                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
    e.preventDefault();

    $('#modal-agree').remove();

    var element = this;

    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function(data) {
            html  = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div>';
            html += '  </div>';
            html += '</div>';

            $('body').append(html);

            $('#modal-agree').modal('show');
        }
    });
});
/// update product detail when choose version
// $(document).ready(function () {
//     if (jQuery('#add-to-cart .form-check-input').length) {
//         ajaxChangeVersion();
//     }
//     jQuery('#add-to-cart .form-check-input').on('click', function () {
//         ajaxChangeVersion();
//     });
// });

$(document).ready(function () {
    if (jQuery('#add-to-cart .color-detail input').length) {
        ajaxChangeVersion();
    }
    jQuery('#add-to-cart .color-detail input').on('click', function () {
        ajaxChangeVersion();
    });
});

function ajaxChangeVersion() {
    $.ajax({
        url: 'index.php?route=product/product/updateCartWhenChangeVersion',
        type: 'post',
        data: $('#add-to-cart').serialize(),
        dataType: 'json',
        beforeSend: function() {
            //TODO loading
        },
        complete: function() {
            //TODO loaded
        },
        success: function(json) {
            if (json['success']) {
                updateHtmlVersion(json['sale_on_out_of_stock'], json['product_version']);
            } else {
                jQuery('#add-to-cart button').attr('data-status', 'disabled');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}

function updateHtmlVersion(sale_on_out_of_stock, product) {
    //update price
    if (product['price'] == 0) {
        var compare_price = parseInt(product['compare_price_format']);
        html_price = (compare_price === 0 ?
            '<a href="#modal-buy-for-contact" rel="modal:open" class="btn btn-add">' + '<span class="price-new text-white">' + 'Liên hệ' + '</span></a>'
            : '<span class="price-new">' + product['compare_price_format']) +'</span><span class="price-old"></span>';
        jQuery('.product-teaser-fix .second-color').html(' - ' + product['version'] + (compare_price === 0 ? '' : ' - ' + product['compare_price_format']));
    } else {
        if (parseInt(product['price_format']) === 0) {
            html_price = '<a href="#modal-buy-for-contact" rel="modal:open" class="btn btn-add">' + '<span class="price-new text-white">' + 'Liên hệ' + '</span></a>';
        } else {
            html_price = '<span class="price-old">' + product['compare_price_format'] + '</span><span class="price-new">' + product['price_format'] + '</span>';
            jQuery('.product-teaser-fix .second-color').html(product['version'] + ' - ' + product['price_format']);
        }
    }
    jQuery('.price-detail').html(html_price);


    // version
    if (product['version']) {
        // form mua tra gop
        var version = product['version'].replace(",", " - ")
        $('input#pAttribute').val(version)
    }

    if (parseInt(product['compare_price_format']) === 0) {
        jQuery('.block-detail .block-custom-change .form-action button').prop('disabled', true);
        jQuery('.block-detail .block-custom-change .buy-now-mobile').css('visibility', 'hidden');
        jQuery('.product-teaser-fix button').css('visibility', 'hidden');
        jQuery('#add-to-cart button').css('visibility', 'hidden');
        jQuery('#add-to-cart input').css('visibility', 'hidden');
        jQuery('#add-to-cart .quantity').css('visibility', 'hidden');
    } else {
        jQuery('.block-detail .block-custom-change .form-action button').removeAttr('disabled');
        jQuery('.block-detail .block-custom-change .buy-now-mobile').css('visibility', 'visible');
        jQuery('.product-teaser-fix button').css('visibility', 'visible');
        jQuery('#add-to-cart button').css('visibility', 'visible');
        jQuery('#add-to-cart input').css('visibility', 'visible');
        jQuery('#add-to-cart .quantity').css('visibility', 'visible');
    }
    //active button add cart
    //TODO : remove if check is_stock done
    // if (product['status'] == 1 && (sale_on_out_of_stock == 1 || (sale_on_out_of_stock == 0 && product['quantity'] > 0))) {
    if (product['status'] == 1 && (sale_on_out_of_stock == 1)) {
        jQuery('#add-to-cart button').removeAttr('data-status');
        jQuery('.product-teaser-fix button').removeAttr('data-status');
        jQuery('.block-status span').html('Còn hàng');
    } else {
        jQuery('#add-to-cart button').attr('data-status', 'disabled');
        jQuery('.product-teaser-fix button').attr('data-status', 'disabled');
        jQuery('.block-status span').html('Hết hàng');
    }
}

function validateSerialize() {
    var invalidFields = jQuery('#add-to-cart').find( ":invalid" );
    if (invalidFields.length > 0) return false;
    return true;
}

var homeCls = function() {

    var vars = {};
    var ele = {};

    this.run = function() {
        this.init();
        this.bindEvents();
    };

    this.init = function() {
        vars.id = 0;
    };

    this.bindEvents = function() {

        initMenu();
        initSlideBanner();
        initS1();
        initS2();
        initS3();
        initS4();
        initS5();
        initS6();
        initMH();
        initQuant();
        initPopup();
        initFilter();
        initSelect();
        initGotop();
        initShowFilter();
        initCollap();
    };

    this.resize = function() {

    };
    var initGotop = function() {
        $('.go-top').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });
    }
    var initShowFilter = function() {
        var overlay = $('<div class="overlay"></div>');
        $('body').prepend(overlay);
        $('.btn-fliter').click(function () {
            $('.siderBar').toggleClass('open');
            overlay.show();
        })
        overlay.click(function () {
            $('.siderBar').removeClass('open');
            $(this).hide();
        })

    }

    var initQuant = function(){
        jQuery('<div class="quantity-nav"><button class="quantity-button quantity-up"><img src="catalog/view/theme/other_dhpet/images/home/plus.png" alt=""></button><button class="quantity-button quantity-down"><img src="catalog/view/theme/other_dhpet/images/home/minus.png" alt=""></button></div>').insertAfter('.quantity input');
        jQuery('.quantity').each(function () {
            var spinner = jQuery(this),
                input = spinner.find('input[type="number"]'),
                btnUp = spinner.find('.quantity-up'),
                btnDown = spinner.find('.quantity-down'),
                min = input.attr('min'),
                max = input.attr('max');

            btnUp.click(function () {
                var oldValue = parseFloat(input.val());
                if (oldValue >= max) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue + 1;
                }
                spinner.find("input").val(newVal);
                spinner.find("input").trigger("change");
            });

            btnDown.click(function () {
                var oldValue = parseFloat(input.val());
                if (oldValue <= min) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue - 1;
                }
                spinner.find("input").val(newVal);
                spinner.find("input").trigger("change");
            });

        });
    }
    var initMenu = function() {
        $(".accodition").click(function () {
            $(this).next().slideToggle('fast');
            $(this).toggleClass('rotate');
        });
        $(".open-sidemenu").click(function () {
            $('#sidenav').toggleClass('show-menu');
            $('.block-overlay').toggleClass('over');
            $('body').toggleClass('no-scroll');
        });
        $(".open-cart").click(function () {
            $('.block-cart').toggleClass('show-cart');
            $('.block-over').toggleClass('over');
            $('body').toggleClass('no-scroll');
            ajaxGetOrderInfo();
        });
        $(".block-overlay").click(function () {
            $('#sidenav').toggleClass('show-menu');
            $(this).toggleClass('over');
            $('body').toggleClass('no-scroll');
        });
        $(".block-over").click(function () {
            $('.block-cart').toggleClass('show-cart');
            $(this).toggleClass('over');
            $('body').toggleClass('no-scroll');
        });
        $("body").on('click', '.close-cart',function () {
            $('.block-cart').toggleClass('show-cart');
            $('.block-over').toggleClass('over');
            $('body').toggleClass('no-scroll');
            $('#user-not-account .close').trigger('click');
        });
    };

    var initSlideBanner = function() {
        var slideBanner = new Swiper('.swiper-banner',{
            autoplay: {
                delay: jQuery('.swiper-banner').attr('data-time'),
                disableOnInteraction: false,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    }
    var initFilter = function() {
        $('.title-sidebar').click(function () {
            $(this).next().slideToggle();
            $(this).toggleClass('open');

        })
    }
    var initCollap = function(){
        $('.collap-icon').click(function () {
            $('.cart-widget').toggleClass('open');
        })
    }




    var initS1 = function() {
        var swiper = new Swiper('.swiper-new', {
            slidesPerView: 6,
            spaceBetween: 20,
            loop: false,
            navigation: {
                nextEl: '.swiper-button-next-n',
                prevEl: '.swiper-button-prev-n',
            },
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 2,
                    spaceBetween: 5
                },
                // when window width is >= 480px
                480: {
                    slidesPerView: 2,
                    spaceBetween: 5
                },
                // when window width is >= 640px
                768: {
                    slidesPerView: 3,
                    spaceBetween: 10
                },
                1200: {
                    slidesPerView: jQuery('.swiper-new').attr('data-grid')
                }
            }
        });
    }
    var initS2 = function() {
        var swiper = new Swiper('.swiper-sale', {
            slidesPerView: 6,
            spaceBetween: 20,
            loop: false,
            navigation: {
                nextEl: '.swiper-button-next-s',
                prevEl: '.swiper-button-prev-s',
            },
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 2,
                    spaceBetween: 5
                },
                // when window width is >= 480px
                480: {
                    slidesPerView: 2,
                    spaceBetween: 5
                },
                // when window width is >= 640px
                768: {
                    slidesPerView: 3,
                    spaceBetween: 10
                },
                1200: {
                    slidesPerView: jQuery('.swiper-sale').attr('data-grid')
                }
            }
        });
    }
    var initS3 = function() {
        var swiper = new Swiper('.swiper-popular', {
            slidesPerView: 6,
            spaceBetween: 20,
            loop: false,
            navigation: {
                nextEl: '.swiper-button-next-p',
                prevEl: '.swiper-button-prev-p',
            },
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 2,
                    spaceBetween: 5
                },
                // when window width is >= 480px
                480: {
                    slidesPerView: 2,
                    spaceBetween: 5
                },
                // when window width is >= 640px
                768: {
                    slidesPerView: 3,
                    spaceBetween: 10
                },
                1200: {
                    slidesPerView: jQuery('.swiper-popular').attr('data-grid')
                }
            }
        });
    };

    var initS4 = function() {
        var swiper = new Swiper('.swiper-custom', {
            slidesPerView: 6,
            spaceBetween: 20,
            loop: false,
            navigation: {
                nextEl: '.swiper-button-next-p',
                prevEl: '.swiper-button-prev-p',
            },
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 2,
                    spaceBetween: 5
                },
                // when window width is >= 480px
                480: {
                    slidesPerView: 2,
                    spaceBetween: 5
                },
                // when window width is >= 640px
                768: {
                    slidesPerView: 3,
                    spaceBetween: 10
                },
                1200: {
                    slidesPerView: jQuery('.swiper-custom').attr('data-grid')
                }
            }
        });
    };

    var initS5 = function() {
        var swiper = new Swiper('.swiper-custom-1', {
            slidesPerView: 6,
            spaceBetween: 20,
            loop: false,
            navigation: {
                nextEl: '.swiper-button-next-p',
                prevEl: '.swiper-button-prev-p',
            },
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 2,
                    spaceBetween: 5
                },
                // when window width is >= 480px
                480: {
                    slidesPerView: 2,
                    spaceBetween: 5
                },
                // when window width is >= 640px
                768: {
                    slidesPerView: 3,
                    spaceBetween: 10
                },
                1200: {
                    slidesPerView: jQuery('.swiper-custom-1').attr('data-grid')
                }
            }
        });
    };

    var initS6 = function() {
        var swiper_partner = new Swiper('.swiper-partner', {
            slidesPerView: jQuery('.swiper-partner').attr('data-limit-inline'),
            spaceBetween: 20,
            loop: false,
            navigation: {
                nextEl: '.swiper-button-next-n',
                prevEl: '.swiper-button-prev-n',
            },
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 2,
                    spaceBetween: 5
                },
                // when window width is >= 480px
                480: {
                    slidesPerView: 2,
                    spaceBetween: 5
                },
                // when window width is >= 640px
                768: {
                    slidesPerView: 3,
                    spaceBetween: 10
                },
                1200: {
                    slidesPerView: jQuery('.swiper-partner').attr('data-limit-inline')
                }
            }
        });
    }

    var initMH = function() {
        // item
        $('.mh-banner').matchHeight();
        $('.mh-address').matchHeight();
        $('.mh-adIt').matchHeight();
    }
    var initPopup = function() {
        $('a.open-modal').click(function(event) {
            $(this).modal({
                fadeDuration: 250
            });
            return false;
        });
    }
    var initSelect = function() {
        $('.js-select').select2();
    }
};

$(document).ready(function() {
    var homeObj = new homeCls();
    homeObj.run();
    // On resize
    $(window).resize(function() {
        homeObj.resize();
        if ($(window).width() <= 768) {
            $('#sidenav').removeClass('main-menu');
        } else {
            $('#sidenav').addClass('main-menu');
        }
    });
});

var initQuant = function(){
    jQuery('<div class="quantity-nav"><button class="quantity-button quantity-up"><img src="catalog/view/theme/other_dhpet/images/home/plus.png" alt=""></button><button class="quantity-button quantity-down"><img src="catalog/view/theme/other_dhpet/images/home/minus.png" alt=""></button></div>').insertAfter('.quantity input');
    jQuery('.quantity').each(function () {
        var spinner = jQuery(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min'),
            max = input.attr('max');

        btnUp.click(function () {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

        btnDown.click(function () {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

    });

    /* setup event on increasing/decreasing */
    setupQuantityListener();
};

//show cart
function ajaxGetOrderInfo() {
    $.ajax({
        url: 'index.php?route=checkout/my_orders/ajaxGetOrderInfo',
        type: 'get',
        data: $('#add-to-cart').serialize(),
        dataType: 'json',
        beforeSend: function() {
            //TODO loading
        },
        complete: function() {
            //TODO loaded
        },
        success: function(data) {
            //TODO: why not use json parse
            if (data && data.order_products && data.order && data.order_products.length) {
                $('.block-cart .all-price').css('display', 'block');
                $('.count span').text(data.order.amount);
                var ordorderProductListHtml = '';
                var is_pay = true;
                $.each(data.order_products, function (index, product) {
                    var version_info = '';
                    var is_price_display_none = '';
                    if (product.attributes && Object.keys(product.attributes).length > 0) {
                        $.each(product.attributes, function (attrName, attrValue) {
                            version_info = version_info +
                                '    <p>\n' +
                                '        <span>' + attrName + ':</span>\n' +
                                '        <span>' + attrValue + '</span>\n' +
                                '    </p>\n';
                        });
                    }
                    var out_stock = '';
                    if (!product.in_stock) {
                        is_pay = false;
                        out_stock += '<span style="display: block; color: red; font-size: 13px">Sản phẩm vừa hết hàng</span>';
                    }

                    if (product.price === product.price_compare) {
                        is_price_display_none = 'display:none';
                    }

                    ordorderProductListHtml = ordorderProductListHtml + '<div class="cart-item">\n' +
                        '            <div class="row row-s-8">\n' +
                        '                <div class="col-4 col-s-8">\n' +
                        '                    <div class="thumb">\n' +
                        '                        <img class="img-fluid" src="' + product.image_url + '" alt="">\n' +
                        '                    </div>\n' +
                        '                </div>\n' +
                        '                <div class="col-8 col-s-8">\n' +
                        '                    <div class="ct">\n' +
                        '                        <h6 class="brand">\n' +
                        '                            ' + product.manufacturer + '\n' +
                        '                        </h6>\n' +
                        '                        <h5 class="name">\n' +
                        '                            <a href="' + product.url + '">' + product.name + '</a>\n' +
                        '                        </h5>\n' +
                                                    out_stock +
                        '                        <div class="row align-center">\n' +
                        '                            <div class="col-7">\n' +
                        '                                ' + version_info +
                        '                            </div>\n' +
                        '                            <div class="col-5">\n' +
                        '                                <div class="price">\n' +
                        '                                    <span class="old" style="' + is_price_display_none + '">' + product.price_compare_formart + '</span>\n' +
                        '                                    <span class="new">'+ product.price_formart +'</span>\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                        </div>\n' +
                        '                        <div class="quant-wp mb-0">\n' +
                        '                            <p>Số lượng</p>\n' +
                        '                            <div class="quantity">\n' +
                        '                                <input type="number" min="1" max="9999" step="1" product-version-id="'+product.product_version_id+'" value="'+product.amount+'" class="product_quantity" product-id="'+product.id+'" >\n' +
                        '                            </div>\n' +
                        '                        <a href="javascript:;" class="bestme-btn bestme-btn--link remove" style="float: right;color: red;" product-version-id="' + product.product_version_id + '" product-id="' + product.id + '">Xóa</a>\n' +
                        '                        </div>\n' +
                        '                    </div>\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '        </div>'
                });

                /* update order product list */
                $('#list-card').html(ordorderProductListHtml);

                /* update total amount */
                var totalAmountHtml = (data && data.order && data.order.into_money_formart) ? data.order.into_money_formart : '0đ';

                /* update total amount in cart */
                $('.all-price h5.price').text(totalAmountHtml);

                /* update total products in cart */
                $('.cart-icon-count').text(data.order.amount);

                /* Go to payment */
                if (is_pay) {
                    $('a#payment').attr('href', data.href_order_preview_not_cart);
                } else {
                    $('a#payment').attr('href', 'javascript::void(0)');
                }
                if (is_pay && !data.customer_is_login) {
                    $('a#payment').attr('data-toggle', 'modal');
                    $('a#payment').attr('data-target', '#user-not-account');
                    $('#user-not-account .btn-login').attr('href', data.href_login);
                    $('#user-not-account .btn-continue').attr('href', data.href_order_preview_not_cart);
                }
            } else {
                var htmlCartEmpty = '<p>Bạn không có sản phẩm nào trong giỏ hàng</p>\n' +
                    '        <p><a style="border: 1px solid #ccc;position: relative;top: 0px;left: 0px;margin-top: 10px;" class="btn btn-continue close-cart" href="javascript:void(0)">Tiếp tục mua sắm</a></p>\n' +
                    '        <br/><br/><br/><br/>'

                /* update order empty */
                $('#list-card').html(htmlCartEmpty);
                $('.block-cart .all-price').css('display', 'none');
                $('.count span').text('0');
                $('.cart-icon-count').text('0');
            }

            /* init quantity increasae/decrease buttons */
            initQuant();
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}

function setupQuantityListener() {
    $('.product_quantity').bind('change paste keyup', function () {
        var em = $(this); // retrieve the index
        var product_id = em.attr('product-id');
        var quantity = em.val();
        var product_version_id = em.attr('product-version-id');
        var validity = this.validity;

        //change input quantity validate
        if (!validity.valid && validity.rangeOverflow) {
            quantity = em.attr('max');
        }

        if (!validity.valid && validity.rangeUnderflow) {
            quantity = em.attr('min');
        }

        if (quantity == '') {
            quantity = 1;
        }

        var button_quantity = $(this).parent().find('.quantity-button');

        button_quantity.prop( "disabled", true);

        $.ajax({
            url: 'index.php?route=checkout/my_orders/update_order_products',
            method: 'POST',
            data: {
                product_id: product_id,
                quantity: quantity,
                product_version_id: product_version_id,
            },
            success: function (json) {
                //TODO: why json parse
                var json = JSON.parse(json);
                if (json && json.data) {
                    var data = json.data;
                    $('.count span').text(data.order.amount);
                    var totalAmountHtml = (data && data.order && data.order.into_money_formart) ? data.order.into_money_formart : '0đ';
                    /* update total amount in cart */
                    $('.all-price h5.price').text(totalAmountHtml);

                    /* update total products in cart */
                    $('.cart-icon-count').text(data.order.amount);
                }
                button_quantity.prop( "disabled", false);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert('Không cập nhật được giỏ hàng. Vui lòng tải lại trang');
            }
        });
    });

    $('.quant-wp .remove').click(function () {
        console.log('fsdfsd');
        var product_id = $(this).attr('product-id');
        var product_version_id = $(this).attr('product-version-id');
        var r = confirm('Bạn có chắc chắn muốn xóa sản phẩm khỏi giỏ hàng ?');
        if (r === true) {
            $.ajax({
                url: 'index.php?route=checkout/my_orders/remove_order_products',
                method: 'POST',
                data: {
                    product_id: product_id,
                    product_version_id: product_version_id
                },
                success: function (json) {
                    console.log(json);
                    //TODO: why json parse
                    json = $.parseJSON(json);
                    if (json.status === true) {
                        ajaxGetOrderInfo();
                    } else {
                        console.log(json && json.message ? json.message : 'Xóa sản phẩm khỏi giỏ hàng lỗi');
                    }
                }
            });
        }
    });
}
