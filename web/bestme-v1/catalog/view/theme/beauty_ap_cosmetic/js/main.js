$(document).ready(function() {

    var topHeaderHeight = $('.block-header .header-top').height();
    $(document).scroll(function(e){
        if( $('.block-header').length === 0 ) return;
        var windowScrollTop = (window.pageYOffset || document.scrollTop)  - (document.clientTop || 0) || 0;
        if(windowScrollTop >= topHeaderHeight && !$('.block-header').hasClass('fixed')){
            $('.block-header').addClass('fixed');
        }
        else if(windowScrollTop <= topHeaderHeight && $('.block-header').hasClass('fixed')){
            $('.block-header').removeClass('fixed');
        }

        if( $('.product-teaser-fix').length == 0 ) return;
        if ($('.block-header').hasClass('fixed') && windowScrollTop >= topHeaderHeight + 500) {
            $('.product-teaser-fix').slideDown(300);
        } else {
            $('.product-teaser-fix').slideUp(300);
        }
    });

    function owlInit(elements, numItem, nav, dots, loop = true, autoplay = true) {
        if (elements.length) {
            elements.each(function () {
                const element = $(this);
                const autoplayTime = undefined === element.data('autoplay_time') ? 5 : element.data('autoplay_time')
                element.owlCarousel({
                    loop: undefined === element.data('loop') ? loop : 1 == element.data('loop'),
                    autoplay: undefined === element.data('autoplay') ? autoplay : 1 == element.data('autoplay'),
                    responsiveClass: true,
                    dots: false,
                    nav: true,
                    navText: ['<i class="icon icon-chevron-left"></i>', '<i class="icon icon-chevron-right"></i>'],
                    URLhashListener: true,
                    startPosition: 'URLHash',
                    autoplayTimeout: autoplayTime * 1e3,
                    responsive: {
                        0: {
                            items: 1,
                        },
                        600: {
                            items: Math.round(numItem / 2),
                        },
                        1000: {
                            items: numItem,
                        }
                    }
                });
            })
        }
    }

    owlInit($('.block-banner .owl-home-banner'), 1, true, true);
    owlInit($('.block-saleoff-product .owl-product'), 1);
    owlInit($('.block-top-sale-product .owl-product'), 1);
    owlInit($('.block-new-product .owl-product'), 1);
    owlInit($('.block-blogs .owl-product'), 1);
    owlInit($('.block-partner .owl-partner'), jQuery('.block-partner .owl-partner').attr('data-limit-inline'), '', '', jQuery('.block-partner .owl-partner').attr('data-loop') == 1, jQuery('.block-partner .owl-partner').attr('data-auto-play') == 1);
    // Product filter form
    if( $( ".box-product-filter#product-price #slider-range" ).length){
        var slider = $( ".box-product-filter#product-price #slider-range" ).slider({
            range: true,
            min: 0,
            step: 100000,
            max: 5000000,
            values: [ 200000, 4000000 ],
            slide: function( event, ui ) {
                $( "#amount" ).text( "Từ " + ui.values[0].toLocaleString('vi-VN') + "đ - " + ui.values[1].toLocaleString('vi-VN') + "đ" );
            }
        });
        $( "#amount" ).text( "Từ " + slider.slider( "values", 0 ).toLocaleString('vi-VN') + "đ - " + slider.slider( "values", 1 ).toLocaleString('vi-VN') + "đ" );
        $('.box-product-filter#product-price .dropdown-menu').on('click', function (e) {
            e.stopPropagation();
        });
    }
    ///////////////////////////////////////

    // Product detail
    owlInit($('.product-detail .owl-product-detail'), 1);
    // owl-thumb
    $('.product-detail .owl-product-thumb').owlCarousel({
        loop: 1 == $('.product-detail .owl-product-thumb').data('loop'),
        autoplay: 1 == $('.product-detail .owl-product-thumb').data('autoplay'),
        autoplayTimeout: $('.product-detail .owl-product-thumb').data('autoplay_time') * 1e3,
        responsiveClass:true,
        dots: false,
        nav:true,
        navText: ['<i class="icon icon-chevron-left"></i>', '<i class="icon icon-chevron-right"></i>'],
        URLhashListener:true,
        startPosition: 'URLHash',
        responsive:{
            480: {
                items: 3,
            },
            600:{
                items:3,
            },
            1000:{
                items:4,
            }
        }
    });
    owlInit($('.block-relate-product .owl-product'), jQuery('.block-relate-product .owl-product').attr('data-number-item'));
    //////////////////////////////////////////////

    // Ô nhập số lượng
    $(document).on('click', '.quantity-group-input>button', function(e) {
        var val = parseInt($(this).parent().find('>input').val());
        var min = parseInt($(this).parent().find('>input').attr('min'));
        if (isNaN(val)) val = 0;
        if (isNaN(min)) min = 0;
        if( val > min && $(this).hasClass('decrease') ){
            val--;
        }
        if( $(this).hasClass('increase') ){
            val++;
        }
        $(this).parent().find('>input').val(val).change();
        e.preventDefault();
    })

    // Show password
    $(document).on('click', '.input-password-group button.show-password', function(e) {
        var input = $(this).parent().find('>input');
        $(this).toggleClass('active');
        if ($(this).hasClass('active')){
            $(this).find('i').removeClass('icon-eye').addClass('icon-no-eye');
            input.attr('type', 'text');
        } else {
            $(this).find('i').removeClass('icon-no-eye').addClass('icon-eye');
            input.attr('type', 'password');
        }
    });

    // Menu responsive
    $('.block-header .block-header-menu ul.nav > li.has-child > a>.toggle').on('click', function(e){
        $(this).closest('li').toggleClass('active')
        e.preventDefault();
        return false;
    })
    $('.search-button').click(function(){
            $('.search-expand').toggleClass('show');
        });
});