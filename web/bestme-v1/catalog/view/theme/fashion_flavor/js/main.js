let myVar = setInterval(myTimer, 3000);
let s = 0;
function myTimer() {
  s++;
  if (s == 7) {
    s = 0;
  }
  $("#photo_"+s).click();
}

function select(selector) {
  var method = selector.substr(0,1) == '.' ? 'getElementsByClassName' : 'getElementById';
  return document[method](selector.substr(1));
}

function range() {
  var range = {left : {x : [], y : []}, right : {x : [], y : []}};
  var wrap = {
    w : select("#wrap").clientWidth,
    h : select("#wrap").clientHeight
  }
  var photo = {
    w : select(".photo")[0].clientWidth,
    h : select(".photo")[0].clientHeight
  }
  range.wrap = wrap;
  range.photo = photo;

  range.left.x = [0, wrap.w/2 - photo.w/2];
  range.left.y = [0, wrap.h - photo.w/2];
  range.right.x = [wrap.w/2 + photo.w/2, wrap.w];
  range.right.y = [0, wrap.h - photo.w/2];

  return range;
}

function sort(n) {
  var _photo = select('.photo');
  var photos = [];
  for(i=0; i<_photo.length; i++) {
    _photo[i].className = _photo[i].className.replace(/\s*photo_center\s*/, ' ');
    _photo[i].className = _photo[i].className.replace(/\s*photo_front\s*/, ' ');
    _photo[i].className = _photo[i].className.replace(/\s*photo_back\s*/, ' ');
    _photo[i].className += ' photo_front';
    _photo[i].style.left = '';
    _photo[i].style.top = '';
    photos.push(_photo[i]);
  }
  var photo_center = select('#photo_' + n);
  photo_center.className += ' photo_center';

  photo_center = photos.splice(n, 1)[0];

  var photos_left = photos.splice(0, Math.ceil(photos.length/2));
  var photos_right = photos;

  var ranges = range();
  for(let i=0; i<photos_left.length; i++){
    let position_left = 10;
    let position_top = 10;
    let scale = 0.5;
    switch(i) {
      case 0:
        position_left = 10;
        position_top = 20;
        scale = 0.5;
        break;
      case 1:
        position_left = 27;
        position_top = 15;
        scale = 0.5;
        break;
      case 2:
        position_left = 18;
        position_top = 63;
        scale = 0.7;
        break;
      default:
        position_left = 30;
        position_top = 30;
        scale = 0.5;
        break;
    }
    photos_left[i].style.left = position_left + "%";
    photos_left[i].style.top = position_top + "%";
    photos_left[i].style['transform'] = 'scale('+scale+')';
    photos_left[i].style['-webkit-transform'] = 'scale('+scale+')';
  }
  for(let i = 0; i<photos_right.length; i++){
    let position_right = 10;
    let position_top = 10;
    let scale = 0.5;
    switch(i) {
      case 0:
        position_right = 73;
        position_top = 15;
        scale = 0.5;
        break;
      case 1:
        position_right = 88;
        position_top = 22;
        scale = 0.5;
        break;
      case 2:
        position_right = 80;
        position_top = 63;
        scale = 0.7;
        break;
      default:
        position_right = 30;
        position_top = 30;
        break;
    }
    photos_right[i].style.left = position_right + "%";
    photos_right[i].style.top = position_top + "%";
    photos_right[i].style['transform'] = 'scale('+scale+')';
    photos_right[i].style['-webkit-transform'] = 'scale('+scale+')';
  }
}

function random(range) {
  var max = Math.max(range[0], range[1]);
  var min = Math.min(range[0], range[1]);
  var diff = max - min;
  var number = Math.floor(Math.random() * diff + min);
  return number;
}

function turn(elem) {
  var cls = elem.className;
  var n = elem.id.split("_")[1];

  if(! /photo_center/.test(cls)) {
    return sort(n);
  }

  if(/photo_front/.test(cls)) {
    cls = cls.replace(/photo_front/, 'photo_back');
    select('#nav_' + n).className += ' i_back ';
  } else {
    cls = cls.replace(/photo_back/, 'photo_front');
    select('#nav_' + n).className = select('#nav_' + n).className.replace(/\s*i_back\s*/, ' ');
  }
  return elem.className = cls;
}
