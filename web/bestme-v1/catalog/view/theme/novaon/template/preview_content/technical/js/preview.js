$(document).ready(function () {
    showPreview();
});

function showPreview() {
    highlight();
}

function highlight() {
    let config = getParamValue('config');
    config = JSON.parse(decodeURIComponent(config));
    if (!config) {
        return;
    }

    config.forEach(function (cf) {
        highlightOne(cf);
    });

    // scroll to first
    scrollToEl(config[0]);
}

function getParamValue(paramName) {
    let url = window.location.search.substring(1); //get rid of "?" in querystring
    let qArray = url.split('&'); //get key-value pairs
    for (let i = 0; i < qArray.length; i++) {
        let pArr = qArray[i].split('='); //split key and value
        if (pArr[0] === paramName) {
            return pArr[1]; //return value
        }
    }
}

function highlightOne(cf) {
    if (!cf || !cf['identify']) {
        return;
    }

    var borderColor = cf['border-color'] ? cf['border-color'] : 'red';

    $(cf['identify']).css('border-width', '2px');
    $(cf['identify']).css('border', '2px solid ' + borderColor);
}

function scrollToEl(cf) {
    if (!cf || !cf['identify']) {
        return;
    }

    var el = $(cf['identify']);
    if (!el || !el.offset() || !el.offset().top) {
        return;
    }

    $([document.documentElement, document.body]).animate({
        scrollTop: el.offset().top
    }, 500);
}