function showInputForm(element){
	$(element).closest('form').addClass('show-input');
}

jQuery(document).ready(function($) {

	function getGridSize() {
		    return ($(window).width() < 768) ? 2 : 3;
	}
	
	if( $('.flexslider').length ){
		$('.slide-banner .flexslider').flexslider({
			smoothHeight: true,
			animation: "fade",
			controlNav: false,
			slideshow: true,
			slideshowSpeed: 7000,
			animationSpeed: 800,
		});

		if( $('.product-views .flexslider').length ){
			var gridSize = getGridSize();
			var productFlexslider = $('.product-views .flexslider').flexslider({
				animation: "slide",
				smoothHeight: true,
				controlNav: false,
				slideshow: false,
				animationSpeed: 600,
				itemWidth: $('.product-views .flexslider').width()/gridSize,
				selector: ".slide-content >.slide-item",
			});

			// check grid size on resize event
			$(window).resize(function() {
				var gridSize = getGridSize();
				productFlexslider.minItems = gridSize;
				productFlexslider.maxItems = gridSize;
			});
		}

		$('.product-info .product-slider .flexslider#slider').flexslider({
			animation: "fade",
			controlNav: false,
			directionNav: false,
			animationLoop: false,
			slideshow: false,
			smoothHeight: true,
			sync: "#carousel"
		});
		$('.product-info .product-slider .flexslider#carousel').flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			itemWidth: 125,
			itemMargin: 5,
			asNavFor: '#slider'
		});

		// $('.slide-banner .flexslider li')
		// // .on('mouseover', function(){
		// // // 	$(this).children('img').css({'transform': 'scale(1.1)'});
		// // 	$(this).children('img').css({'animation': 'none'});
		// // })
		// // .on('mouseout', function(){
		// // 	// $(this).children('img').css({'transform': 'scale(1)'});
		// // 	$(this).children('img').removeAttr('style');
		// // })
		// .on('mousemove', function(e){
		// 	$(this).children('img').css({'transform-origin': ((e.pageX - $(this).offset().left) / $(this).width()) * 100 + '% ' + ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +'%'});
		// })
	}

	if( $('.selectpicker').length ){
		$('.selectpicker').selectpicker({

		});
	}

});