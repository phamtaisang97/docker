$(document).ready(function() {

    var topHeaderHeight = $('.block-header .header-top').height();
    $(document).scroll(function(e){
        if( $('.block-header').length === 0 ) return;
        var windowScrollTop = (window.pageYOffset || document.scrollTop)  - (document.clientTop || 0) || 0;
        if(windowScrollTop >= topHeaderHeight && !$('.block-header').hasClass('fixed')){
            $('.block-header').addClass('fixed');
        }
        else if(windowScrollTop <= topHeaderHeight && $('.block-header').hasClass('fixed')){
            $('.block-header').removeClass('fixed');
        }

        if( $('.product-teaser-fix').length == 0 ) return;
        if ($('.block-header').hasClass('fixed') && windowScrollTop >= topHeaderHeight + 500) {
            $('.product-teaser-fix').slideDown(300);
        } else {
            $('.product-teaser-fix').slideUp(300);
        }
    })

    // Product detail
    owlInit($('.product-detail .owl-product-detail'), 1, true, false, false);
    // owl-thumb
    $('.product-detail .owl-product-thumb').owlCarousel({
        loop:false,
        responsiveClass:true,
        dots: false,
        nav:true,
        navText: ['<i class="icon icon-chevron-left"></i>', '<i class="icon icon-chevron-right"></i>'],
        URLhashListener:true,
        startPosition: 'URLHash',
        responsive:{
            480: {
                items: 3,
            },
            600:{
                items:3,
            },
            1000:{
                items:4,
            }
        }
    });
    owlInit($('.block-relate-product .owl-product'), jQuery('.block-relate-product .owl-product').attr('data-number-item'));
    //////////////////////////////////////////////
    function owlInit(elements, numItem, nav = true, dots = false, autoplay = true, loop = true, autoplayTimeout = 5000 )
    {
        if(elements.length) {
            elements.each(function () {
                const element = $(this);
                const autoplayTime = undefined === element.data('autoplay_time') ? autoplayTimeout : element.data('autoplay_time');
                element.owlCarousel({
                    loop: undefined === element.data('loop') ? loop : 1 == element.data('loop'),
                    autoplay: undefined === element.data('autoplay') ? autoplay : 1 == element.data('autoplay'),
                    autoplayTimeout: autoplayTime * 1e3,
                    responsiveClass:true,
                    dots: dots,
                    nav:nav,
                    navText: ['<i class="icon icon-chevron-left"></i>', '<i class="icon icon-chevron-right"></i>'],
                    URLhashListener:true,
                    startPosition: 'URLHash',
                    responsive:{
                        0:{
                            items:1,
                        },
                        600:{
                            items:Math.round(numItem/2),
                        },
                        1000:{
                            items:numItem,
                        }
                    }
                });
            });
        }
    }
    var element = document.getElementById('object');

    var owl1 = $('.block-banner .owl-home-banner');
    owlInit(owl1, 1, true, true, true,true, owl1.data('time') ? owl1.data('time') : 5000);

    var owl2 = $('.block-saleoff-product .owl-product');
    owlInit(owl2, 1, true, false, false);

    var owl3 = $('.block-top-sale-product .owl-product');
    owlInit(owl3, 1, true, false, false);

    var owl4 = $('.block-new-product .owl-product');
    owlInit(owl4, 1, true, false, false);

    var owl5 = $('.block-blogs .owl-product');
    owlInit(owl5, 1, true, false, false);

    var owl6 = $('.block-partner .owl-partner');
    owlInit(owl6,
            owl6.attr('data-limit-inline') ? owl6.attr('data-limit-inline') : 4,
            false, false,
            owl6.attr('data-auto-play') == 1,
            owl6.attr('data-loop') == 1,
            owl1.data('time') ? owl1.data('time') : 5000
    );

    var owl7 = $('.block-partner .owl-home-banner');
    owlInit(owl7, owl7.attr('data-transition-time') ? owl7.attr('data-transition-time') : 5000);

    // Product filter form
    if( $( ".box-product-filter#product-price #slider-range" ).length){
        var slider = $( ".box-product-filter#product-price #slider-range" ).slider({
            range: true,
            min: 0,
            step: 100000,
            max: 5000000,
            values: [ 200000, 4000000 ],
            slide: function( event, ui ) {
                $( "#amount" ).text( "Từ " + ui.values[0].toLocaleString('vi-VN') + "đ - " + ui.values[1].toLocaleString('vi-VN') + "đ" );
            }
        });
        $( "#amount" ).text( "Từ " + slider.slider( "values", 0 ).toLocaleString('vi-VN') + "đ - " + slider.slider( "values", 1 ).toLocaleString('vi-VN') + "đ" );
        $('.box-product-filter#product-price .dropdown-menu').on('click', function (e) {
            e.stopPropagation();
        });
    }
    ///////////////////////////////////////



    // Ô nhập số lượng
    $(document).on('click', '.quantity-group-input>button', function(e) {
        var val = parseInt($(this).parent().find('>input').val());
        var min = parseInt($(this).parent().find('>input').attr('min'));
        if (isNaN(val)) val = 0;
        if (isNaN(min)) min = 0;
        if( val > min && $(this).hasClass('decrease') ){
            val--;
        }
        if( $(this).hasClass('increase') ){
            val++;
        }
        $(this).parent().find('>input').val(val).change();
        e.preventDefault();
    })

    // Show password
    $(document).on('click', '.input-password-group button.show-password', function(e) {
        var input = $(this).parent().find('>input');
        $(this).toggleClass('active');
        if ($(this).hasClass('active')){
            $(this).find('i').removeClass('icon-eye').addClass('icon-no-eye');
            input.attr('type', 'text');
        } else {
            $(this).find('i').removeClass('icon-no-eye').addClass('icon-eye');
            input.attr('type', 'password');
        }
    });

    // Menu responsive
    $('.block-header .block-header-menu ul.nav > li.has-child > a>.toggle').on('click', function(e){
        $(this).closest('li').toggleClass('active')
        e.preventDefault();
        return false;
    })

});
$(document).ready(function() {
    $('.icon-search .open-search').click(function() {
        $('.search-block').toggleClass("show");
    }); 
}); 
$(document).ready(function() {
    var feed = new Instafeed({
        clientId: "c6e4c736e82345a3898a0e299daa00fb",
        get: "user",
        
            accessToken: "1599696966.1677ed0.378293d7d5e74eecb5de8d0e5b748ab6",
        
        
        
        
            userId: 1599696966,
        
        limit: "6",
        resolution: "thumbnail",
    //           after: function(){
    //         	  var owl = $("#instafeed");
    //             owl.owlCarousel({
    //             items : 4, //10 items above 1000px browser width
    //             itemsDesktop : [1000,3], //5 items between 1000px and 901px
    //             itemsDesktopSmall : [900,2], // betweem 900px and 601px
    //             itemsTablet: [600,1], //2 items between 600 and 0
    //             itemsMobile : [480,1] // itemsMobile disabled - inherit from itemsTablet option
    //           });
    //         },
    });
    feed.run();
}); 