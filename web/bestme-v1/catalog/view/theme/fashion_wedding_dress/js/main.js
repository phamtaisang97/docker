$(document).ready(function() {
    // Select dropdown when click dropdown item
    $('.dropdown-filter').on('click', '.dropdown-item', function(event) {
        event.preventDefault();
        var key_txt = $(this).text();
        $(this).parents('.dropdown-filter').find('button').html('').append(key_txt);
    });
    // Disibled product
    if($('.pro-disabled').length > 0) {
        $('.pro-disabled a').click(function(event) {
            event.preventDefault();
        });
    }


    // Zoom image product
    $('#zoom_thumb').ezPlus({
        constrainType: 'height', constrainSize: 418, zoomType: 'lens',
        containLensZoom: true, gallery: 'gallery_01', cursor: 'pointer', galleryActiveClass: 'active'
    });

    // Product filter form
    if( $( ".box-product-filter#product-price #slider-range" ).length){
        var slider = $( ".box-product-filter#product-price #slider-range" ).slider({
            range: true,
            min: 0,
            step: 1,
            max: 5000000,
            values: [ 200000, 4000000 ],
            slide: function( event, ui ) {
                $( "#amount" ).text( "Từ " + ui.values[0].toLocaleString('vi-VN') + "đ - " + ui.values[1].toLocaleString('vi-VN') + "đ" );
            }
        });
        $( "#amount" ).text( "Từ " + slider.slider( "values", 0 ).toLocaleString('vi-VN') + "đ - " + slider.slider( "values", 1 ).toLocaleString('vi-VN') + "đ" );
        $('.box-product-filter#product-price .dropdown-menu').on('click', function (e) {
            e.stopPropagation();
        });
    }


    // input quanty caculate
    // $('input[type="number"].custom-number-input').each(function(index, el) {
    //     var wrapper = document.createElement("div");
    //     wrapper.classList = "custom-number-input-wrapper";
    //
    //     var increase = document.createElement("button");
    //     increase.innerHTML  = '+';
    //     increase.classList = "increase";
    //     var decrease = document.createElement("button");
    //     decrease.innerHTML  = '-';
    //     decrease.classList = "decrease";
    //
    //     $(wrapper).append(decrease);
    //     $(wrapper).append($(this).clone());
    //     $(wrapper).append(increase);
    //     $(this).replaceWith($(wrapper));
    // });

    $(document).on('click', '.custom-number-input-wrapper >button', function(e){
        var input = $(this).parent().find('input');
        var input_val = (input.val()) ? parseInt(input.val()) : 0;
        var min = input.attr('min');
        var max = input.attr('max');
        var step = typeof input.attr('step') !== "undefined" ? input.attr('step') : 1;
        if( $(this).hasClass('increase') ){
            input_val = input_val + step;
        }
        else{
            input_val = input_val - step;
        }
        if( input_val < min && typeof min !== "undefined" ) input_val = min;
        if( input_val > max && typeof max !== "undefined" ) input_val = max;
        input.val(input_val).change();
    })

    // Fixed Menu when Scroll
    // var offset_header = $('.main-header').offset().top;
    // long edit col-lg-9 -> col-lg-10, col-lg-4 -> col-lg-3
    $(window).scroll(function() {
        if($(window).scrollTop() >= 50) {
            $('.header').addClass('header-fixed');
            $('#main-wrapper').addClass('has-fixed');
            $('.left-header-search').removeClass('d-lg-flex');
            $('.main-menu-header').removeClass('col-lg-8').addClass('col-lg-10');
            $('.nav-header .navbar').removeClass('flex-lg-column').addClass('flex-lg-row');
            $('.header-search').removeClass('d-none');
            $('.logo').addClass('col-lg-3 pl-0');
        }
        else {
            $('.header').removeClass('header-fixed');
            $('#main-wrapper').removeClass('has-fixed');
            $('#form-search-scroll').hide();
            $('.left-header-search').addClass('d-lg-flex');
            $('.main-menu-header').removeClass('col-lg-10').addClass('col-lg-8');
            $('.nav-header .navbar').removeClass('flex-lg-row').addClass('flex-lg-column');
            $('.header-search').addClass('d-none');
            $('.logo').removeClass('col-lg-3 pl-0');
        }
    });

    // Product Carousel
    $('.owl-product').owlCarousel({
        loop:true,
        margin:30,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:4
            }
        }
    });
    // Custommer Carousel
    $('.owl-custommer').owlCarousel({
        loop: jQuery('.owl-custommer').attr('data-loop') == 1 ? true : false,
        autoplay: jQuery('.owl-custommer').attr('data-auto-play') == 1 ? true : false,
        margin:60,
        nav:false,
        animateOut: true,
        responsive:{
            0:{
                items:1
            },
            600: {
                items:2,
            },
            1000:{
                items: jQuery('.owl-custommer').attr('data-limit-inline'),
            }
        }
    });

    // Check hidden form address => add class d-none for set address default
    $('.address-form .collapse').on('show.bs.collapse', function () {
        $(this).parent().children('.default-address').addClass('d-none');
    });
    $('.address-form .collapse').on('hide.bs.collapse', function () {
        $(this).parent().children('.default-address').removeClass('d-none');
    });
    // Close form address
    $('.cancle-address').click(function() {
        $(this).parents('.address-form').children('.collapse').collapse('hide');
    });

    // Show Widget Search
    if($('.search-button').length > 0) {
        $('.search-button').click(function() {
            $('.home-search').toggleClass('show');
        });
    }

    // Show/Hide password
    // Show password
    $(document).on('click', '.input-password-group button.show-password', function(e) {
        var input = $(this).parent().find('>input');
        $(this).toggleClass('active');
        if ($(this).hasClass('active')){
            $(this).find('i').removeClass('icon-eye').addClass('icon-no-eye');
            input.attr('type', 'text');
        } else {
            $(this).find('i').removeClass('icon-no-eye').addClass('icon-eye');
            input.attr('type', 'password');
        }
    });



    // Close menu mobile
    if($('#navbarMainMenu').length > 0) {
        $('button.navbar-toggler').click(function() {
            $('#overlay').toggleClass('show');
            // $('#navbarMainMenu').addClass('show');
        });

        $('#navbarMainMenu i.close').click(function() {
            $('#overlay').removeClass('show');
            $(this).parent('#navbarMainMenu').removeClass('show');
        });
        $(window).click(function(e) {
            if( $(e.target).attr('id') == 'overlay' && $('#navbarMainMenu').hasClass('show') && $(e.target).parents('#navbarMainMenu').length == 0 ){
                $('#overlay').removeClass('show');
                $('#navbarMainMenu').removeClass('show');
            }
        });
    }

    jQuery('.quantity').each(function () {
        var spinner = jQuery(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min'),
            max = input.attr('max');

        btnUp.click(function () {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

        btnDown.click(function () {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

    });

});

// sticky
$(document).ready(function() {
    $(window).scroll(function() {
        var topHeaderHeight = $('header .main-header').outerHeight();
        if (550 <= $(this).scrollTop()) {
            $('#buy-now').addClass("fixed");
            $('#buy-now').css('top', topHeaderHeight+"px")
        } else {
            $('#buy-now').removeClass("fixed");
        }
    });

    // custom time slider
    // $('#carouselHome').carousel({
    //     interval: $('#carouselHome').data('time')
    // })
});