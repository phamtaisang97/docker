$(document).ready(function() {

    // Add class header-default when screen <= 992px
    var width_screen = $(window).width();
    if(width_screen <= 992) {
        // $('.header').addClass('header-default');
        $('#carouselHome').addClass('mt-3');
    }
    $(window).resize(function() {
        width_screen = $(window).width();
        if(width_screen <= 992) {
            // $('.header').addClass('header-default');
            $('#carouselHome').addClass('mt-3');
        }
    });
    // Search input change key => show box result
    $('#input-search').change(function(event) {
        var keyword = $(this).val();
        var key_length = keyword.split(" ").length;
        if(key_length >= 3) {
            $('.search-box-ajax').removeClass('d-none').addClass('d-block');
            $('#main-wrapper').css('opacity', 0.2);
        }
        else {
            $('.search-box-ajax').removeClass('d-block').addClass('d-none');
            $('#main-wrapper').css('opacity', 1);
        }
    });

    // Show payment tabs
    if($('#payment-tabContent').length > 0) {
        $('.select-payment input[name="customRadioType"]').click(function () {
            $(this).parents('.select-payment').find('input[name="customRadioType"]').removeClass('active');
            // $(this).addClass('active');
            $(this).tab('show');
        });
    }
    // Show modal Product (Xem Nhanh)
    if($('#productModal').length > 0) {
        $('.buy-now').click(function(event) {
            event.preventDefault();
            $('#productModal').modal('show');
        });
    }

    // Show tooltip
    $('a.add-favourite').tooltip();

    // Check star rate by user
    var rate = 0;
    $('.user-star-rate input[type="radio"]').change(function() {
        rate = $(this).val();
        $(".form-rate form").find('.btn[type="submit"]').attr('data-original-title', '');
    });

    $( ".form-rate form" ).submit(function( event ) {
        if ( rate === 0 ) {
            event.preventDefault();
            $(this).find('.btn[type="submit"]').tooltip('show');
            return;
        }
    });

    // Select dropdown when click dropdown item
    $('#dropdown-select').on('click', '.dropdown-item', function(event) {
        event.preventDefault();
        var key_txt = $(this).text();
        $(this).parents('#dropdown-select').find('button').html('').append(key_txt);
    });

    // Zoom image product
    $('#zoom_thumb').ezPlus({
        constrainType: 'height', constrainSize: 418, zoomType: 'lens',
        containLensZoom: true, gallery: 'gallery_01', cursor: 'pointer', galleryActiveClass: 'active'
    });


    // input quanty caculate
    $('input[type="number"].custom-number-input').each(function(index, el) {
        var wrapper = document.createElement("div");
        wrapper.classList = "custom-number-input-wrapper";
        
        var increase = document.createElement("button");
        increase.innerHTML  = '+';
        increase.classList = "increase";
        var decrease = document.createElement("button");
        decrease.innerHTML  = '-';
        decrease.classList = "decrease";

        $(wrapper).append(decrease);
        $(wrapper).append($(this).clone());
        $(wrapper).append(increase);
        $(this).replaceWith($(wrapper));
    });

    $(document).on('click', '.custom-number-input-wrapper >button', function(e){
        var input = $(this).parent().find('input');
        var input_val = (input.val()) ? parseInt(input.val()) : 0;
        var min = input.attr('min');
        var max = input.attr('max');
        var step = typeof input.attr('step') !== "undefined" ? input.attr('step') : 1;
        if( $(this).hasClass('increase') ){
            input_val = input_val + step;
        }
        else{
            input_val = input_val - step;
        }
        if( input_val < min && typeof min !== "undefined" ) input_val = min;
        if( input_val > max && typeof max !== "undefined" ) input_val = max;
        input.val(input_val).change();
    })

    $('.cart-table table').on('change', '.custom-number-input-wrapper >input', function(e){
        var price = $(this).closest('tr').find('.price[data-value]').attr('data-value');
        price = parseInt(price) * parseInt($(this).val());
        var row_price = $(this).closest('tr').find('.total-price[data-value]');
        row_price.text($.number(price, 0, '', '.'));
        row_price.attr('data-value', price);

        var total_price = 0;
        var total_transport = 0;
        var total_tax = 0;
        var total_cart = 0;
        $('.cart-table table tr').each(function(index, el) {
            if( $(this).find('.total-price[data-value]').length ){
                total_price += parseInt($(this).find('.total-price[data-value]').attr('data-value'));
            }
        });
        total_transport = parseInt($('.table-footer .total-transport').attr('data-value'));
        total_tax = parseInt($('.table-footer .total-tax').attr('data-value'));
        total_cart = total_transport + total_tax + total_price;
        console.log(total_cart);
        $('.table-footer .total-price').text($.number(total_price, 0, '', '.'));
        $('.table-footer .total-cart').text($.number(total_cart, 0, '', '.'));
    });

    // Fixed Menu when Scroll
    // var offset_header = $('.main-header').offset().top;
    // $(window).scroll(function() {
    //     if($(window).scrollTop() >= 50) {
    //         $('body').addClass('body-scroll');
    //         $('.header').addClass('default-style-fixed');
    //     }
    //     else {
    //         $('body').removeClass('body-scroll');
    //         $('.header').removeClass('default-style-fixed');
    //     }
    // });

    // Product Carousel
    function owlInit(elements, numItem, nav = true, dots = false, margin = 0, loop = true ,autoplay = true)
    {
        if(elements.length) {
            elements.each(function () {
                const element = $(this);
                const autoplayTime = undefined === element.data('autoplay_time') ? 5 : element.data('autoplay_time');
                element.owlCarousel({
                    loop: undefined === element.data('loop') ? loop : 1 == element.data('loop'),
                    autoplay: undefined === element.data('autoplay') ? autoplay : 1 == element.data('autoplay'),
                    autoplayTimeout: autoplayTime * 1e3,
                    responsiveClass:true,
                    dots: dots,
                    nav: nav,
                    margin: margin,
                    navText: ['<i class="icon icon-chevron-left"></i>', '<i class="icon icon-chevron-right"></i>'],
                    // URLhashListener:true,
                    // startPosition: 'URLHash',
                    responsive:{
                        0:{
                            items:1,
                        },
                        600:{
                            items:Math.round(numItem/2),
                        },
                        1000:{
                            items:numItem,
                        }
                    }
                });
            });
        }
    }
    owlInit($('.block-top-sale-product .owl-product'), 1, true, false, 0, false);
    owlInit($('.block-most-sale-product .owl-product, .block-newest-product .owl-product'), 4, true, false, 15, false);
    owlInit($('.product-related .owl-product'), 1, true, false, 15, false);
    owlInit($('.product-viewed .owl-product'), 1, true, false, 15, false);
    owlInit($('.owl-custommer'), jQuery('.owl-custommer').attr('data-limit-inline'), false, false, 15,jQuery('.owl-custommer').attr('data-loop') == 1,jQuery('.owl-custommer').attr('data-loop') == 1);
    owlInit($('#owl-rate'), 1, false, true, 32);
    // Check hidden form address => add class d-none for set address default
    $('.address-form .collapse').on('show.bs.collapse', function () {
        $(this).parent().children('.default-address').addClass('d-none');
    });
    $('.address-form .collapse').on('hide.bs.collapse', function () {
        $(this).parent().children('.default-address').removeClass('d-none');
    });
        // Close form address
        $('.cancle-address').click(function() {
            $(this).parents('.address-form').children('.collapse').collapse('hide');
        });

    // Show Widget Search
    if($('.search-button').length > 0) {
        $('.search-button').click(function() {
            $('.home-search').toggleClass('show');
        });
    }

    // Show/Hide password
    $(document).on('click', '.input-password-group button.show-password', function(e) {
        var input = $(this).parent().find('>input');
        $(this).toggleClass('active');
        if ($(this).hasClass('active')){
            $(this).find('i').removeClass('icon-eye').addClass('icon-no-eye');
            input.attr('type', 'text');
        } else {
            $(this).find('i').removeClass('icon-no-eye').addClass('icon-eye');
            input.attr('type', 'password');
        }
    });


    // Close menu mobile
    if($('#navbarMainMenu').length > 0) {
        $('#navbarMainMenu').parent().append('<div id="overlay"></div>');
        $('button.navbar-toggler').click(function() {
            $('#overlay').toggleClass('show');
            // $('#navbarMainMenu').addClass('show');
        });

        $('#navbarMainMenu i.close').click(function() {
            $('#overlay').removeClass('show');
            $(this).parent('#navbarMainMenu').removeClass('show');
        });
        $(window).click(function(e) {
            if( $(e.target).attr('id') == 'overlay' && $('#navbarMainMenu').hasClass('show') && $(e.target).parents('#navbarMainMenu').length == 0 ){
                $('#overlay').removeClass('show');
                $('#navbarMainMenu').removeClass('show');
            }
        });

        // Menu responsive
        $('.block-header .main-menu ul.nav > li.has-child > a>.toggle').on('click', function(e){
            $(this).closest('li').toggleClass('active');
            e.preventDefault();
            return false;
        });
    }

    // Header menu fix scroll
    var topHeaderHeight = $('.block-header').height();
    $(document).scroll(function(e){
        if( $('.block-header').length == 0 | $('body').height() < $(window).height()+250 ) return;
        var windowScrollTop = (window.pageYOffset || document.scrollTop)  - (document.clientTop || 0);
        if(windowScrollTop >= topHeaderHeight && !$('.block-header').hasClass('fixed')){
            $('.block-header').addClass('fixed');
            $('.block-header').css('min-height', $('.block-header >.fix-content').height()+topHeaderHeight+"px");
        }
        else if(windowScrollTop <= topHeaderHeight && $('.block-header').hasClass('fixed')){
            $('.block-header').removeClass('fixed');
            $('.block-header').css('min-height', "auto");
        }

        if( $('.product-teaser-fix').length == 0 ) return;

        var topHeight = $('.fix-content.main-header').height() + 30;

        if ($('.block-header').hasClass('fixed') && windowScrollTop >= topHeaderHeight + 500) {
            $('.product-teaser-fix').css('top', topHeight+"px");
            $('.product-teaser-fix').slideDown(300);
        } else {
            $('.product-teaser-fix').slideUp(300);
        }
    })

    // Address setting page collapse
    $('.address-setting-panel .left a[data-toggle="collapse"]').on('click', function () {
        $('.form-collapse .collapse.show').collapse('hide');
        console.log(11111);
    })
    
});