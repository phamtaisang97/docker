function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

// Cart add remove functions
var cart = {
    'add': function(product_id, quantity) {
        var valid = validateSerialize();
        if (!valid) {
            $('input[name="quantity"]')[0].reportValidity();
            return false;
        }
        if (jQuery('#add-to-cart button[type="submit"]').attr('data-status') == "disabled") {
            return false;
        }
        $.ajax({
            url: 'index.php?route=checkout/my_orders/addCart',
            type: 'post',
            data: $('#add-to-cart').serialize(),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('.amount .quantity span').html(json['total_product_cart']);

                    $('.amount .total').html(json['total_into_money']);

                    $('#add_cart_notify').modal('show');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'update': function(key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var voucher = {
    'add': function() {

    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var wishlist = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);

                $('html, body').animate({ scrollTop: 0 }, 'slow');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}

var compare = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#compare-total').html(json['total']);

                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
    e.preventDefault();

    $('#modal-agree').remove();

    var element = this;

    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function(data) {
            html  = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div>';
            html += '  </div>';
            html += '</div>';

            $('body').append(html);

            $('#modal-agree').modal('show');
        }
    });
});
/// update product detail when choose version
$(document).ready(function () {
    if (jQuery('#add-to-cart .attr-item').length) {
        ajaxChangeVersion();
    }
    jQuery('#add-to-cart .attr-item input').on('click', function () {
        ajaxChangeVersion();
    });
});

function ajaxChangeVersion() {
    $.ajax({
        url: 'index.php?route=product/product/updateCartWhenChangeVersion',
        type: 'post',
        data: $('#add-to-cart').serialize(),
        dataType: 'json',
        beforeSend: function() {
            //TODO loading
        },
        complete: function() {
            //TODO loaded
        },
        success: function(json) {
            if (json['success']) {
                updateHtmlVersion(json['sale_on_out_of_stock'], json['product_version']);
            } else {
                jQuery('#add-to-cart button').attr('data-status', 'disabled');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}

function updateHtmlVersion(sale_on_out_of_stock, product) {
    // version
    if (product['version']) {
        // form mua tra gop
        var version = product['version'].replace(",", " - ")
        $('input#pAttribute').val(version)
    }

    //update price
    if (product['price'] == 0) {
        var compare_price = parseInt(product['compare_price_format']);
        jQuery('.pro-detail .pro-price').html(compare_price === 0
            ? '<a href="javascript:;" data-toggle="modal" data-target="#modal-buy" class="btn btn-buy btn-primary">'+
                '<span class="new-price text-white fw-600 fz-16 mr-2">'+ 'Liên hệ'+ '</span>'+
            '</a>'
            : '<span class="new-price text-primary fw-600 fz-25 mr-2">' + product['compare_price_format'] + '</span>');

        jQuery('.pro-detail .old-price').html('');
        jQuery('.product-teaser-fix .second-color').html(' - ' + product['version'] + (compare_price === 0 ? '' : ' - ' + product['compare_price_format']));
    } else {
        var comparePrice = parseInt(product['compare_price_format']);
        if (parseInt(product['price_format']) === 0) {
            jQuery('.pro-detail .pro-price').html(comparePrice === 0
                ? '<a href="javascript:;" data-toggle="modal" data-target="#modal-buy" class="btn btn-buy btn-primary">'+
                    '<span class="new-price text-white fw-600 fz-16 mr-2">'+ 'Liên hệ'+ '</span>'+
                '</a>'
                : '<span class="new-price text-primary fw-600 fz-25 mr-2">\n' + product['compare_price_format'] + '</span>\n');
        } else {
            jQuery('.pro-detail .pro-price').html('<span class="new-price text-primary fw-600 fz-25 mr-2">' + product['price_format'] + '</span>' +
                '<span class="old-price text-info fz-18 fw-600">' + product['compare_price_format'] + '</span>');
            jQuery('.product-teaser-fix .second-color').html(product['version'] + ' - ' + product['price_format']);
        }
    }

    if (parseInt(product['compare_price_format']) === 0) {
        jQuery('.pro-detail .block-custom-change .buy-now-mobile button').prop('disabled', true);
        jQuery('.pro-detail .block-custom-change .buy-now-mobile').addClass('d-none');
        jQuery('.product-teaser-fix button').addClass('d-none');
        jQuery('.pro-detail .block-custom-change .pro-action').addClass('d-none');
        jQuery('#add-to-cart button').addClass('d-none');
        jQuery('#add-to-cart input[name="product_id"]').addClass('d-none');
        jQuery('#add-to-cart input[name="product_name_add_cart"]').addClass('d-none');
        jQuery('#add-to-cart .quantity-nav').addClass('d-none');
    } else {
        jQuery('.pro-detail .block-custom-change .buy-now-mobile button').removeAttr('disabled');
        jQuery('.pro-detail .block-custom-change .buy-now-mobile').removeClass('d-none');
        jQuery('.product-teaser-fix button').removeClass('d-none');
        jQuery('.pro-detail .block-custom-change .pro-action').removeClass('d-none');
        jQuery('#add-to-cart button').removeClass('d-none');
        jQuery('#add-to-cart input[name="product_id"]').removeClass('d-none');
        jQuery('#add-to-cart input[name="product_name_add_cart"]').removeClass('d-none');
        jQuery('#add-to-cart .quantity-nav').removeClass('d-none');
    }
    //active button add cart
    //TODO : remove if check is_stock done
    // if (product['status'] == 1 && (sale_on_out_of_stock == 1 || (sale_on_out_of_stock == 0 && product['quantity'] > 0))) {
    if (product['status'] == 1 && (sale_on_out_of_stock == 1)) {
        jQuery('#add-to-cart button').removeAttr('data-status');
        jQuery('.product-teaser-fix button').removeAttr('data-status');
        jQuery('.block-status span').html('Còn hàng');
        jQuery('#add-to-cart button[type="button"]').addClass("second-color-hover");
    } else {
        jQuery('#add-to-cart button').attr('data-status', 'disabled');
        jQuery('.product-teaser-fix button').attr('data-status', 'disabled');
        jQuery('.block-status span').html('Hết hàng');
        jQuery('#add-to-cart button').removeClass("second-color-hover");
    }

    /* update product version image */
    // set default values
    product.image = product.image ? product.image : jQuery('#product-image-default-src').val();
    product.image_alt = product.image_alt ? product.image_alt : '';
    if (product.image) {
        const indicatorSlider = jQuery('#carouselProDetail .carousel-indicators');
        let nextProductIndex = jQuery('#carouselProDetail .carousel-inner .carousel-item').length;
        let isLoadOtherVersion = false;
        if (nextProductIndex > 0) {
            // slider has at least 1 image
            // get last product image
            const lastProductImage = jQuery('#carouselProDetail .carousel-inner .carousel-item').last();
            // check if it is product version image and not this version
            isLoadOtherVersion = lastProductImage.data('version') !== product.version;
            if (lastProductImage.length > 0 && lastProductImage.hasClass('product-version-image') && isLoadOtherVersion) {
                // remove last main slider image
                lastProductImage.remove();
                // decrease next product index to current max product index
                --nextProductIndex;
                // remove last indicator slider image
                jQuery('#carouselProDetail .carousel-indicators li[data-slide-to=' + nextProductIndex + ']').remove();
            }
        }

        if (isLoadOtherVersion) {
            // remove current product image active class
            jQuery('#carouselProDetail .carousel-inner .carousel-item.active').removeClass('active');

            // add product version image to the end of product image slider
            jQuery(`<div class="carousel-item active product-version-image" data-version="${product.version}">
                <img src="${product.image}" alt="${product.image_alt}">
            </div>`).appendTo(jQuery('#carouselProDetail .carousel-inner'));
            jQuery('#carouselProDetail').carousel();

            // add product version image to the end of indicator image slider
            const productImageIndicatorHtml = `<li data-target="#carouselProDetail" data-slide-to="${ nextProductIndex }" class="active mb-2">
                <img src="${product.image}" alt="${product.image_alt}">
            </li>`;
            indicatorSlider.append(productImageIndicatorHtml);
        } else {
            // not insert this product version image => decrease next product index
            --nextProductIndex;
        }

        // go to product version image slide
        jQuery('#carouselProDetail').carousel(nextProductIndex);
        jQuery('#carouselProDetail .carousel-indicators li[data-slide-to=' + nextProductIndex + ']').trigger('click');
    }
}

function validateSerialize() {
    var invalidFields = jQuery('#add-to-cart').find( ":invalid" );
    if (invalidFields.length > 0) return false;
    return true;
}