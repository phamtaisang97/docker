# Belie - Bestme

### Welcome to Bestme

I. Install
----------

Rec

Clone project in web server public directory, e.g in apache2 (Linux): /var/www/html/\<project>

Copy config-dist.php => config.php; change url, database config, change vendor dir

Copy admin/config-dist.php => admin/config.php; change url, database config, change vendor dir

Copy index-dist.php => index.php

Copy admin/index-dist.php => admin/index.php; change url, database config, change vendor dir

Install required libraries for php if not have:

```
# sample for linux
sudo apt-get install php7.0-bcmath php7.0-curl php7.0-dom php7.0-gd php7.0-zip
```

Notice: if php5.6, replace `php7.0` with `php5.6`

Edit vendor dir in composer.json (DO NOT use the default dir such as "/system/storage")

```
...
"config": {
    "vendor-dir": "/var/www/storage_x2.bestme.test/vendor/"
},
...
```

Install required lib

```
composer install
```

Install redis extension for php

```
# sample for linux
sudo apt-get install php7.0-redis
```

then enable redis.so by add following line into php.ini

```
# sample for linux - ubuntu
# /etc/php/php7.0/apache2/php.ini
extension=redis.so

# /etc/php/php7.0/cli/php.ini
extension=redis.so

# /etc/php/php7.0/fpm/php.ini
extension=redis.so
```

Copy file /path/to/project/system/storage/vendor/scss.inc.php to /path/to/your_storage/vendor


Modify `php-amqplib` package ([source](https://stackoverflow.com/questions/62554098/trying-to-access-array-offset-on-value-of-type-int-defaultvaluebinder-php-line)): `/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Cell/DefaultValueBinder.php` on line 56:

```
} elseif ($pValue[0] === '=' && strlen($pValue) > 1) {
```
with the following:
```
} elseif (0 === strpos($pValue, '=') && strlen($pValue) > 1) {
```


If run on Linux-Apache: enable some modules for seo url works properly:

```
sudo a2enmod headers
sudo a2enmod rewrite
sudo service apache2 restart
```

Install redis:

```
sudo apt-get install php7.0-redis
vi php.ini, add this line:

extension=redis.so
```

Note:
- centos: etc/php.ini
- ubuntu, linuxmint, ...: /etc/php/7.0/cli/php.ini, /etc/php/7.0/apache2/php.ini, /etc/php/7.0/fpm/php.ini, ...
- windows: 
  - Download redis: https://pecl.php.net/package/redis/2.2.7/windows
  - Extract, copy to xampp/php/ext, edit xampp/php/php.ini add line:

    ```
    extension=php_redis.dll
    ```
         
Install xdebug

```
sudo apt-get install php7.0-xdebug

sudo vi /etc/php/7.0/apache2/php.ini

# add below lines:
; /usr/lib/php/20151012/xdebug.so
zend_extension=xdebug.so
DBGP_IDEKEY=PHPSTORM
xdebug.idekey=PHPSTORM
xdebug.enable=1
xdebug.remote_enable=1
```

Restart apache server

```
sudo service apache2 restart
```

Init database: 

- open install/opencart.sql, copy and execute in phpmyadmin.

Init data for dev environment only: 

- open dev/dev-init-db.sql, copy and execute in phpmyadmin. 
  Note: may skip sql lines truncate demo data to keep demo data.

II. Run
-------

Front page: open browser point to project front page such as http://localhost/<project>

Admin page: open browser point to admin login page such as http://localhost/<project>/admin?route=super/login.
Username/password see in dev/dev-init-db.sql, e.g admin/123456

III. Usage
----------

login as admin in dev environment: `<url>/admin?route=super/login`

IV. Config for release version
------------------------------

1. v1.5.3 - tuning and fix bugs

admin/config.php

```admin/config.php
define('BESTME_SERVER_PRICING', 'https://bestme.test/bang-gia');

// Shop name suffix
define('SHOP_NAME_SUFFIX', '.bestme.test');

// config date trial
define('PACKET_TRIAL_PERIOD_DEFAULT', 15);
```

config.php

```config.php
// hot line
define('CONFIG_HOT_LINE', '0961680486');

// Bestme homepage
define('BESTME_SERVER', 'https://www.bestme.asia/');
```


1. v1.6.1 - connect 3rd party api

admin/config.php

```admin/config.php
// for Shopee redis job queue
define('MUL_REDIS_DB_SHOPEE', 2);

// Novaon Chatbot homepage
define('CHATBOT_NOVAON_HOMEPAGE', 'https://chatbot.novaonx.com/');

// Config Partner Viettel_Post
define('VIETTEL_POST_TOKEN', 'eyJhbGciOiJFUzI1NiJ9.eyJVc2VySWQiOjY5MDE2OTIsIkZyb21Tb3VyY2UiOjUsIlRva2VuIjoiUzEzMU01N1VDSkFQUzFCOEdLNEYiLCJleHAiOjE2NTI4Njk5NDMsIlBhcnRuZXIiOjY5MDE2OTJ9.Hu2g_TiKNGT55-7CqecBxigQomB9q3JAFLD372th9GNAHGaFMbeC59vxX_6HaP92K-QLmNG-c8youePy8aMeHw');
define('VIETTEL_POST_USERNAME', 'dunght@novaon.asia');
define('VIETTEL_POST_PASSWORD', 'NOVAON12nam');

// Shopee partner
define('SHOPEE_PARTNER_ID', 843411);
define('SHOPEE_PARTNER_KEY', '455ac8f7232734f8f41924904a1be8dab2d46d1909147b2e45433577ee29c773');

// Shopee job redis queue
define('SHOPEE_JOB_REDIS_QUEUE', 'SHOPEE_JOB');

// Shopee job redis process info
define('SHOPEE_PROCESS_INFO_PREFIX', 'SHOPEE_PROCESS_INFO_');
define('SHOPEE_PROCESS_INFO_TIMEOUT', 7200); // in seconds. default: 7200s = 2h
```

config.php

```config.php
// hot line
define('CONFIG_HOT_LINE', '0961680486');

// Bestme homepage
define('BESTME_SERVER', 'https://www.bestme.asia/');
```

1. Fix staff login

admin/config.php

```admin/config.php
// for customer staff
define('MUL_REDIS_CUSTOMER_STAFF_PUBLISH_CHANEL', 'UpdateCustomerStaff');
```

config.php

```config.php
/*
 * always at the end of file!
 * define that needed for migration
 */
// migration_20190903_sync_staffs_to_welcome
define('MUL_REDIS_HOST','127.0.0.1');
define('MUL_REDIS_PORT',6379);
define('MUL_REDIS_DB',1);
define('MUL_REDIS_CUSTOMER_STAFF_PUBLISH_CHANEL', 'UpdateCustomerStaff');
/* end - define that needed for migration */
```

1. v1.6.2 - Admin Mobile + MaxLead + Tuning

admin/config.php

- modify

```admin/config.php
define('MAX_IMAGE_SIZE_UPLOAD', 2097152); // 2048 kb
```

- add new

```admin/config.php
define('MAX_FILE_SIZE_IMPORT_PRODUCTS', 4194304); // bytes ~ 4194304 = 4Mb

// AutoAds Google Shopping homepage
define('AUTOADS_MAXLEAD_SERVER', 'https://autoads.asia/cong-cu/maxlead');

// Google Analytics homepage
define('GG_ANALYTICS_SERVER', 'https://support.google.com/analytics/?hl=vi#topic=3544906');

// Facebook Pixel homepage
define('FB_PIXEL_SERVER', 'https://www.facebook.com/business/help/651294705016616');

// Novaon Chatbot homepage and guide
define('CHATBOT_NOVAON_GUIDE', 'https://drive.google.com/file/d/1yo2IrT2LUPRCfGvyXwWUhAyUwC2lVMGU/view');

// Giao hang nhanh
define('TRANSPORT_GHN_GUIDE', 'https://drive.google.com/file/d/1gVvWXaHxC9vshPFayI0EQhyVxNNqYRcP/view');

// Viettel Post
define('TRANSPORT_VIETTELPOST_GUIDE', 'https://drive.google.com/file/d/1vhJ-Dxj0x8XjNASynvbCmhRywUffi1hz/view');

// Sample product upload homepage
define('SAMPLE_PRODUCT_LIST_UPLOAD', 'https://support.bestme.asia/wp-content/uploads/2019/09/file-mau-upload-san-pham.xlsx');
```

config.php

```config.php
// Shop name suffix
define('SHOP_NAME_SUFFIX', '.bestme.asia');
```

1. v1.7.1 - Dashboard

admin/config.php

```admin/config.php
```

config.php

```config.php
// for tracking page view session for conversion report
define('WEB_TRAFFIC_COOKIE_TTL', 1800); // 1800 seconds = 30 minute
```

1. v1.7.2 - Onboarding

admin/config.php

```admin/config.php
//hot line
define('CONFIG_HOT_LINE', '0961680486');
```

1. v1.7.3 - Tuning

nothing! Yeah!

1. v1.8.1 - Theme api + Builder + Blog

nothing! Yeah!

1. v1.8.2 - Tuning

nothing! Yeah!

1. v1.8.3 - CMS admin

admin/config.php

```admin/config.php
// sync_bestme_website_data_to_welcome
define('MUL_REDIS_SYNC_BESTME_WEBSITE_DATA_PUBLISH_CHANEL', 'SyncBestmeWebsiteData');
```

config.php

```config.php
// sync_bestme_website_data_to_welcome
define('MUL_REDIS_SYNC_BESTME_WEBSITE_DATA_PUBLISH_CHANEL', 'SyncBestmeWebsiteData');
```

1. v1.9.1 - New Theme + CMS Admin

admin/config.php

```admin/config.php
// sync_bestme_website_data_to_welcome
define('MUL_REDIS_SYNC_BESTME_WEBSITE_DATA_PUBLISH_CHANEL', 'SyncBestmeWebsiteData');
```

config.php

```config.php
// sync_bestme_website_data_to_welcome
define('MUL_REDIS_SYNC_BESTME_WEBSITE_DATA_PUBLISH_CHANEL', 'SyncBestmeWebsiteData');
```

1. v1.9.2 - POS

for dev:

- admin/config.php

```admin/config.php
// Bestme homepage
define('BESTME_SERVER_POS', 'https://www.bestme.asia/bestme-pos');

// Bestme POS
define('BESTME_POS_URL_TEMPLATE', 'https://%s.bestme.asia/pos');
define('BESTME_POS_SECURE_KEY', 'KPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4aKPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4aKPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4a');
```

- config.php

```config.php
// Bestme POS
define('BESTME_POS_SECURE_KEY', 'KPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4aKPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4aKPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4a');
```

for production:

- admin/config.php

```admin/config.php
// Bestme homepage
define('BESTME_SERVER_POS', 'https://www.bestme.asia/bestme-pos');

// Bestme POS
define('BESTME_POS_URL_TEMPLATE', 'https://%s.bestme.asia/pos');
```

- multi.config.php

```multi.config.php
// Bestme POS
define('BESTME_POS_SECURE_KEY', 'KPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4aKPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4aKPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4a');
```

1. v1.9.3 - Tuning

for dev:

- admin/config.php

```admin/config.php
// Sample product upload homepage
define('SAMPLE_PRODUCT_LIST_UPLOAD_EN', 'https://support.bestme.asia/wp-content/uploads/2020/01/product_upload_sample.xlsx');
```

- config.php

```config.php
// Cloudinary for uploading images
define('TEMPORY_IMAGE_PREFIX', 'tempory_');
define('MAX_IMAGES_UPLOAD', 1000); // 1000 images
define('MAX_IMAGE_SIZE_UPLOAD', 2097152); // 2048 kb
define('CLOUD_NAME', 'novaon-x2');
define('API_KEY', 'YOUR_API_KEY');
define('API_SECRET', 'YOUR_API_SECRET');

// SHOP_ID
define('SHOP_ID', 'x2.bestme.test');
```

for production:

- admin/config.php

```admin/config.php
// Sample product upload homepage
define('SAMPLE_PRODUCT_LIST_UPLOAD_EN', 'https://support.bestme.asia/wp-content/uploads/2020/01/product_upload_sample.xlsx');
```

- multi.config.php: first remove define from admin/config.php if has, then do:

```multi.config.php
// Cloudinary for uploading images
define('TEMPORY_IMAGE_PREFIX', 'tempory_');
define('MAX_IMAGES_UPLOAD', 1000); // 1000 images
define('MAX_IMAGE_SIZE_UPLOAD', 2097152); // 2048 kb
define('CLOUD_NAME', 'novaon-x2');
define('API_KEY', 'YOUR_API_KEY');
define('API_SECRET', 'YOUR_API_SECRET');

// SHOP_ID
define('SHOP_ID', 'x2.bestme.test');
```

1. v2.0 - Tuning

for dev:

- admin/config.php

```admin/config.php
// Bestme guide
define('BESTME_INTERFACE_GUIDE', 'https://support.bestme.asia/2591-video-hd-chon-va-tuy-chinh-giao-dien/');
define('BESTME_CONTENT_GUIDE', 'https://support.bestme.asia/2601-huong-dan-cau-hinh-phan-trang-noi-dung/');
define('BESTME_MENU_GUIDE', 'https://support.bestme.asia/2603-huong-dan-tuy-chinh-menu-chinh/');
define('BESTME_DOMAIN_GUIDE', 'https://support.bestme.asia/2595-huong-dan-gan-ten-mien-2/');
define('BESTME_UPLOAD_PRODUCT_GUIDE', 'https://support.bestme.asia/2599-huong-dan-up-san-pham-len-website/');
define('BESTME_COLLECTION_GUIDE', 'https://support.bestme.asia/2688-video-huong-dan-tao-va-chinh-sua-bo-suu-tap/');
define('BESTME_GOOGLE_SHOPPING_GUIDE', 'https://support.bestme.asia/2685-video-huong-dan-ket-noi-google-shopping/');
define('BESTME_FACEBOOK_PIXEL_GUIDE', 'https://support.bestme.asia/2682-video-huong-dan-gan-ma-facebook-pixel-len-website/');
define('BESTME_GOOGLE_ANALYTICS_GUIDE', 'https://support.bestme.asia/2679-video-huong-dan-gan-ma-google-analytics-len-website/');
define('BESTME_SHOPEE_GUIDE', 'https://support.bestme.asia/2676-video-huong-dan-dong-bo-san-pham-tu-shopee-ve-bestme/');
define('BESTME_BANNER_GUIDE', 'https://support.bestme.asia/2673-video-huong-dan-thay-banner-quang-cao-tren-website/');
define('BESTME_SLIDE_GUIDE', 'https://support.bestme.asia/2691-video-huong-dan-thay-doi-slide-quang-cao-tren-website/');
```

for production:

- admin/config.php

```admin/config.php
// Bestme guide
define('BESTME_INTERFACE_GUIDE', 'https://support.bestme.asia/2591-video-hd-chon-va-tuy-chinh-giao-dien/');
define('BESTME_CONTENT_GUIDE', 'https://support.bestme.asia/2601-huong-dan-cau-hinh-phan-trang-noi-dung/');
define('BESTME_MENU_GUIDE', 'https://support.bestme.asia/2603-huong-dan-tuy-chinh-menu-chinh/');
define('BESTME_DOMAIN_GUIDE', 'https://support.bestme.asia/2595-huong-dan-gan-ten-mien-2/');
define('BESTME_UPLOAD_PRODUCT_GUIDE', 'https://support.bestme.asia/2599-huong-dan-up-san-pham-len-website/');
define('BESTME_COLLECTION_GUIDE', 'https://support.bestme.asia/2688-video-huong-dan-tao-va-chinh-sua-bo-suu-tap/');
define('BESTME_GOOGLE_SHOPPING_GUIDE', 'https://support.bestme.asia/2685-video-huong-dan-ket-noi-google-shopping/');
define('BESTME_FACEBOOK_PIXEL_GUIDE', 'https://support.bestme.asia/2682-video-huong-dan-gan-ma-facebook-pixel-len-website/');
define('BESTME_GOOGLE_ANALYTICS_GUIDE', 'https://support.bestme.asia/2679-video-huong-dan-gan-ma-google-analytics-len-website/');
define('BESTME_SHOPEE_GUIDE', 'https://support.bestme.asia/2676-video-huong-dan-dong-bo-san-pham-tu-shopee-ve-bestme/');
define('BESTME_BANNER_GUIDE', 'https://support.bestme.asia/2673-video-huong-dan-thay-banner-quang-cao-tren-website/');
define('BESTME_SLIDE_GUIDE', 'https://support.bestme.asia/2691-video-huong-dan-thay-doi-slide-quang-cao-tren-website/');
```

1. v2.1 - Discount

nothing! Yeah!

1. v2.2.1 - Store receipt

nothing! Yeah!

1. v2.3.1 - Simple GMV

nothing! Yeah!

1. v2.3.3 - More api for Social-1.1

- admin/config.php:

```admin/config.php
// Asycn Callback job redis queue
define('ASYCN_CALLBACK_JOB_REDIS_QUEUE', 'ASYCN_CALLBACK_JOB');
define('ASYNC_CALLBACK_DB', 3);
define('ASYNC_CALLBACK_CHATBOT_V2_ORDER_NOTIFY_URL', 'https://social2.novaonx.com/api/v1/orders/bestme-update');
```

- create cronjob to execute this file 'PATH/TO/PROJECT/DIR/system/cronjob/async_callback/asycn_callback.php' such as (like shopee cronjob):

```
# callback async system/cronjob/async_callback/asycn_callback.php
*/1  * * * * /usr/bin/php /var/www/html/novaon-x2/upload/system/cronjob/async_callback/asycn_callback.php >> /var/www/html/x2_cron/async_callback_logs.txt 2>&1
```

1. Add button onboarding gift

- admin/config.php:

```admin/config.php
// link onboarding gift
define('ONBOARDING_GITF_LINK', 'https://m.me/567316780340939?ref=nv36493875673167803409391589357398');
```

1. Upload category excel

- admin/config.php:

```admin/config.php
// import categories .xls config
define('MAX_ROWS_IMPORT_CATEGORIES', 501); // 500 + Header
define('MAX_FILE_SIZE_IMPORT_CATEGORIES', 2097152); // bytes ~ 2097152 = 2Mb
define('SAMPLE_CATEGORY_UPLOAD', 'https://support.bestme.asia/wp-content/uploads/2020/07/file-mau-upload-loai-san-pham.xlsx');
define('SAMPLE_CATEGORY_UPLOAD_EN', 'https://support.bestme.asia/wp-content/uploads/2020/07/category-upload-sample.xlsx');
```

1. Advance Maxlead (Pushnotification)

- .htaccess (if apache, under line "RewriteBase /") or other place due to server:

```.htaccess
RewriteRule ^NovaonAutoadsSDK.js$ index.php?route=extension/novaon/novaon_autoads_sdk [L]
```

1. v2.5.1-VNPay

- admin/config.php:

```admin/config.php
// VNPay. This will override define in VNPay. Let absent to use original value from VNPay
// But current no require any override config! Use default
```

- config.php:

```config.php
// VNPAY PAYMENT PAGE URL
define('VNPAY_PAYMENT_PAGE_URL', 'https://pay.vnpay.vn/vpcpay.html');
```

1. v2.6.1-Advance-Shopee-Connection

- admin/config.php:

```admin/config.php
// Shopee partner
define('SHOPEE_PARTNER_ID', 843411); // sandbox: 840568, real: 843411
define('SHOPEE_PARTNER_KEY', '455ac8f7232734f8f41924904a1be8dab2d46d1909147b2e45433577ee29c773'); // sandbox 424967cfa6038dbaab13f5fecb18094dd162f1e19d31b77f91b129be52780bde, real: 455ac8f7232734f8f41924904a1be8dab2d46d1909147b2e45433577ee29c773
define('SHOPEE_API_URL_PREFIX', 'https://partner.shopeemobile.com/'); // sandbox: https://partner.uat.shopeemobile.com/, real: https://partner.shopeemobile.com/

// Shopee job redis queue
define('SHOPEE_JOB_REDIS_QUEUE', 'SHOPEE_JOB');
define('SHOPEE_ORDER_JOB_REDIS_QUEUE', 'SHOPEE_ORDER_JOB');
define('SHOPEE_JOB_MAX_PROCESS', 5);
define('SHOPEE_MAX_JOB_IN_A_PROCESS', 100);

// Shopee job redis process info
define('SHOPEE_PROCESS_INFO_PREFIX', 'SHOPEE_PROCESS_INFO_');
define('SHOPEE_PROCESS_INFO_TIMEOUT', 7200); // in seconds. default: 7200s = 2h
```
- create cronjob to execute this file 'PATH/TO/PROJECT/DIR/system/cronjob/sale_channel/shopee_sync_orders.php':

```
# callback async system/cronjob/sale_channel/shopee_sync_orders.php
*/1  * * * * /usr/bin/php /PATH/TO/PROJECT/system/cronjob/sale_channel/shopee_sync_orders.php >> /PATH/TO/LOG/shopee_sync_orders.txt 2>&1
```

- dev notice:

  - enable sync interval test value (e.g 5 min) for dev only in admin/config.php: 
  
    ```
    // Shopee test option
    define('SHOPEE_SYNC_INTERVAL_TEST_VALUE', ''); // minutes, e.g: 5 for 5 min. Let empty or absent for no test
    ```
    
  - Very IMPORTANT: rollback dev option in production! (let empty or absent for no test)

1. v2.6.2-GHTK

- admin/config.php:

```admin/config.php
// GHTK. This will override define in GHTK. Let absent to use original value from GHTK
// But current no require any override config! Use default
```

1. v2.5.3-connect-social

- admin/config.php:

```admin/config.php
// Connect to NovaonX Social
define('MIX_CLIENT_ID_SSO', '5f2b9d8ed43c2339f21a86a8'); // staging: 5f2b9d8ed43c2339f21a86a8, production: xxx
define('REDIRECT_TO_NOVAON_X_SOCIAL', 'https://accounts-dev.novaonx.com?client_id=${CLIENT_ID}&shop_id=${SHOP_ID}&email=${EMAIL}'); // staging: https://accounts-dev.novaonx.com, production: https://accounts.novaonx.com
```

1. v2.6.3-lazada-connection

- admin/config.php:

```admin/config.php
// Lazada connection
//// app config
define('LAZADA_APP_KEY', 121865); // test: 121867, staging: 121865, production: xxx
define('LAZADA_APP_SECRET_KEY', '1fP9ALPOPx5aI3Dws9geTh5Q9gveKTPE'); // test: ZmFWCRbi6MIzrwEmXBvwpbNonDBYZTNL, staging: 1fP9ALPOPx5aI3Dws9geTh5Q9gveKTPE, production: xxx
define('LAZADA_API_URL_PREFIX', 'https://api.lazada.vn/rest');
//// redis config
define('MUL_REDIS_DB_LAZADA', 2);
define('LAZADA_PRODUCT_JOB_REDIS_QUEUE', 'LAZADA_PRODUCT_JOB');
define('LAZADA_PROCESS_INFO_PREFIX', 'LAZADA_PROCESS_INFO_');
define('LAZADA_PROCESS_INFO_TIMEOUT', 7200); // in seconds. default: 7200s = 2h
```

- create cronjob to execute this file 'PATH/TO/PROJECT/DIR/system/cronjob/sale_channel/lazada.php':

```
# callback async system/cronjob/sale_channel/lazada.php
*/1  * * * * /usr/bin/php /PATH/TO/PROJECT/system/cronjob/sale_channel/lazada.php >> /PATH/TO/LOG/lazada_sync_products.txt 2>&1
```

1. v2.7.1-lazada-sync-order

- admin/config.php: apped after Lazada connection > redis config

```admin/config.php
// Lazada connection
...
//// redis config
...
define('LAZADA_ORDER_JOB_REDIS_QUEUE', 'LAZADA_ORDER_JOB');
//// optimize mem
define('LAZADA_JOB_MAX_PROCESS', 5);
define('LAZADA_MAX_JOB_IN_A_PROCESS', 100);
```

- create cronjob to execute this file 'PATH/TO/PROJECT/DIR/system/cronjob/sale_channel/lazada_sync_orders.php':

```
# callback async system/cronjob/sale_channel/lazada.php
*/1  * * * * /usr/bin/php /PATH/TO/PROJECT/system/cronjob/sale_channel/lazada_sync_orders.php >> /PATH/TO/LOG/lazada_sync_orders.txt 2>&1
```

- dev notice:

  - enable sync interval test value (e.g 5 min) for dev only in admin/config.php: 
  
    ```
    // Lazada test option
    define('LAZADA_SYNC_INTERVAL_TEST_VALUE', ''); // minutes, e.g: 5 for 5 min. Let empty or absent for no test
    ```
    
  - Very IMPORTANT: rollback dev option in production! (let empty or absent for no test)

1. use email config

- admin/config.php + config.php

```
// email
define('MAIL_SMTP_HOST_NAME', 'tls://smtp.gmail.com');
define('MAIL_SMTP_PORT', '587');
define('MAIL_SMTP_USERNAME', 'bestme@mail.test');
define('MAIL_SMTP_PASSWORD', '123456');
```

1. export many products: split into parts

- admin/config.php

```
// import products .xls config
// ...
define('MAX_ROWS_EXPORT_PRODUCTS', 500); // 500 + Without Header
```

1. v2.7.3 - New Customer Design

- admin/config.php

```
// Sample customer upload homepage
define('SAMPLE_CUSTOMER_LIST_UPLOAD', 'https://docs.google.com/spreadsheets/d/13waKnlW0OSdrKHNHr0u8vbCJI4QQA4n0RhC0IzJbIfY/edit');
define('SAMPLE_CUSTOMER_LIST_UPLOAD_EN', 'https://docs.google.com/spreadsheets/d/13waKnlW0OSdrKHNHr0u8vbCJI4QQA4n0RhC0IzJbIfY/edit');
define('MAX_FILE_SIZE_IMPORT_CUSTOMER', 2097152); // bytes ~ 2097152 = 2Mb
```

1. v2.8.0-bestme-image-server

- admin/config.php:

```admin/config.php
// Config Image Server for uploading images
define('IMAGE_SERVER_UPLOAD', 'bestme'); // bestme, cloudinary

// bestme image server
define('BESTME_IMAGE_SEVER_UPLOAD_URL', 'http://127.0.0.1:8989');
define('BESTME_IMAGE_SEVER_SERVE_URL', 'http://cdn.bestme.test');
define('BESTME_IMAGE_SEVER_API_KEY', 'xxx');
// bestme image redis config for crawling image from Cloudinary to Bestme image server
define('BESTME_IMAGE_JOB_REDIS_QUEUE', 'PULL_IMAGES_FROM_CLOUDINARY');
define('MUL_REDIS_DB_BESTME_IMAGE', 2);
define('BESTME_IMAGE_JOB_MAX_PROCESS', 1);
define('BESTME_IMAGE_CJ_POLLING_INTERVAL', 600); // in seconds
```

- catalog/config.php: (in block "needed for migration")

```catalog/config.php
/*
 * always at the end of file!
 * define that needed for migration
 */
...
// bestme image redis config for crawling image from Cloudinary to Bestme image server
define('BESTME_IMAGE_JOB_REDIS_QUEUE', 'PULL_IMAGES_FROM_CLOUDINARY');
define('MUL_REDIS_DB_BESTME_IMAGE', 2);
```

- create cronjob to execute this file 'PATH/TO/PROJECT/DIR/system/cronjob/image/pull_image_from_cloudinary.php':

```
# callback async system/cronjob/image/pull_image_from_cloudinary.php
*/1  * * * * /usr/bin/php /PATH/TO/PROJECT/system/cronjob/image/pull_image_from_cloudinary.php >> /PATH/TO/LOG/pull_image_from_cloudinary.txt 2>&1
```

1. v2.8.1 - Shopee Advance Product

- No config! Yeah!

1. v2.8.2 - Transport Status Management

- admin/config.php

```
// Giao hang nhanh
define('TRANSPORT_GHN_BESTME_TOKEN', 'b577b882-c001-11ea-b354-e6945d70dd56'); // staging: b577b882-c001-11ea-b354-e6945d70dd56, production: xxx
define('TRANSPORT_GHN_BESTME_PHONE', '0977200443'); // staging: 0977200443, production: xxx

// VTPost: no need more config. Token tree algorithm makes sure callback works well!

// GHTK
// GHTK token of BESTME for callback
define('TRANSPORT_GHTK_TOKEN', ''); // staging: xxx, production: xxx
```

1. v2.8.3 - Tuning

- admin/config.php

```
// Sample product upload homepage
define('SAMPLE_PRODUCT_LIST_UPLOAD', 'https://support.bestme.asia/wp-content/uploads/2020/10/file-mau-upload-san-pham-1.xlsx');
define('SAMPLE_PRODUCT_LIST_UPLOAD_EN', 'https://support.bestme.asia/wp-content/uploads/2020/10/product_upload_sample-1.xlsx');
```

1. v2.8.4 - POS return goods

- No config! Yeah!

1. v2.9.1 - Bestme packages

- No config! Yeah!

1. v2.10.1 - Image Folder

- No config! Yeah!

1. v2.10.2 - Tuning

- Customize layout
    - Add config into default.json in catalog/view/theme/theme-directory/asset/default_config:
    
    ```
    "default_customize_layout": [
        {"name" : "Slideshow banner", "file": "slideshow-banner.twig"},
        {"name" : "Nội dung tùy chọn", "file": "display-customizes.twig"},
        {"name" : "Block collection", "file": "block-collection.twig"},
        {"name" : "Block products", "file": "block-products.twig"},
        {"name" : "Banner box", "file": "banner-box.twig"},
        {"name" : "Partners box", "file": "partner-box.twig"},
        {"name" : "News box", "file": "news-box.twig"},
        ...,
      ]
    ```
    
    - Create template follow default_customize_layout (e.g slideshow-banner.twig, ...), then include it in common/home:
    
    ```
    {% for section in sections_layout %}
         {% set path= 'tech_sun_electro/template/common/section_home/'~section['file'] %}
         {% include <path> %}
    {% endfor %}
    ```
    
1. v2.10.3 - Shopee upload product
- admin/config.php

  ```
  // Shopee upload
  define('SHOPEE_UPLOAD_JOB_REDIS_QUEUE', 'SHOPEE_UPLOAD_JOB');
  
  // One Shopee id for getting categories, logistics for caching...
  define('SHOPEE_SHOP_ID', 311278093);
  ```

- Command lấy ngành hàng và đối tác giao vận của shopee (run once after deploying, run monthly for update or on Shopee categories changed):
  
  ```
  /usr/bin/php /PATH/TO/PROJECT/system/cronjob/cache_migration/shopee_category.php
  ```

- create cronjob to execute this file 'PATH/TO/PROJECT/DIR/system/cronjob/sale_channel/shopee_upload_product.php':

  ```
  # callback async system/cronjob/sale_channel/shopee_upload_product.php
  */1  * * * * /usr/bin/php /PATH/TO/PROJECT/system/cronjob/sale_channel/shopee_upload_product.php >> /PATH/TO/LOG/shopee_upload_product.txt 2>&1
  ```
    
1. v3.1.3 - SSO Social
- admin/config.php

  ```
  //// new: sso
  define('GO_TO_NOVAON_X_SOCIAL', 'http://social.test:8082'); // staging: https://social-stg.novaonx.com, production: https://social.novaonx.com
  ```
  
1. v3.4.3 - Tuning and Fix bugs
- admin/config.php
  
  ```
  // redis config for caching queries
  define('REDIS_DB_CACHE_QUERY', 3);
  ```
  
- config.php
  
  ```
  // redis config for caching queries
  define('REDIS_DB_CACHE_QUERY', 3);
  ```

1. v3.4.3 - Finetune
- config.php

  ```
  // redis config for caching queries
  define('REDIS_DB_CACHE_QUERY', 3);
  
  // server serve url image bestme and url image new
  define('BESTME_CURRENT_IMAGE_SERVER_SERVE_URL', 'https://cdn.bestme.asia');
  define('BESTME_NEW_IMAGE_SERVER_SERVE_URL', 'https://cdn-bestme.cdn.vccloud.vn');
  // end - server serve url image bestme and url image new
  ```
  
- admin/config.php
  
  ``` 
  // redis config for caching queries
  define('REDIS_DB_CACHE_QUERY', 3);
  
  // change password
  define('CHANGE_PASSWORD_HREF', 'https://id.novaon.asia'); // dev + staging: https://accounts-dev.novaonx.com, product: https://id.novaon.asia
  ```
  
1. v3.5.1 - openApi

- admin/config.php

  ```
  define('OPEN_API_BASE_URL', 'http://bestme.asia/api/open-api');
  define('OPEN_API_SECURE_KEY', 'd3b732b3f5f7b07c54930e1f56a01999');
  define('OPEN_API_BASE_URL_IMAGE_LOGO', 'http://bestme.asia/image/AppTheme/');
  ```

  ```
  // rabbitmq config
  define('RABBITMQ_HOST', 'localhost');
  define('RABBITMQ_PORT', 5672);
  define('RABBITMQ_USER_LOGIN', 'admin');
  define('RABBITMQ_USER_PASSWORD', '123456');
  ```

- config.php

  ```
  // rabbitmq config
  define('RABBITMQ_HOST', 'localhost');
  define('RABBITMQ_PORT', 5672);
  define('RABBITMQ_USER_LOGIN', 'admin');
  define('RABBITMQ_USER_PASSWORD', '123456');
  ```

1. Config multi Enterprise
   - admin/config.php

        ```
        define('PRODUCTION_BRAND_NAME', 'Bestme');
        define('PRODUCTION_BRAND_NAME_UPPERCASE', 'BESTME');
        define('PRODUCTION_BRAND_PHONE', '0961680486');
        define('PRODUCTION_BRAND_LOGO','view/image/custom/logo-adg.png');
        define('PRODUCTION_BRAND_FAVICON','view/image/custom/favicon-adg.ico');
        ```
   - config.php

      ```
      define('PRODUCTION_BRAND_NAME', 'Bestme');
      define('PRODUCTION_BRAND_NAME_UPPERCASE', 'BESTME');
      define('PRODUCTION_BRAND_PHONE', '0961680486');
      ```

V. Config dev
------------------------------

1. chatbot registration callback

   TODO: ...

1. Show page 400/500 error

   - Edit admin/index.php, index.php, remove the following lines
   
   ```
   ini_set('display_errors', 1);
   ini_set('display_startup_errors', 1);
   ```
   
   - Copy multi.config-dist.php to multi.config.php, change some configs related to this feature, such as:
   
   ```
   define('MUL_ERR_FILEPATH', '/var/www/html/opencart/system/storage/httperr/');
   define('MUL_PRODUCTION_LOG', true);
   define('MUL_PRODUCTION_LOG_FILE', '/var/www/opencart_storage/logs/error.log');
   ```
   
   - enjoy!
1. Login SSO NOVAON ID

define('MIX_URL_SSO','https://accounts-dev.novaonx.com/');
define('MIX_CLIENT_ID_SSO','5eaff0cb9708f7588674d66a');
define('MIX_CLIENT_SECRET_SSO','66axntq4Av3WCmUVH9zzW');


7/5/2021 Move branch to gitlab novaon 13/07/2021
    - https://git.novaonx.com/
References
----------

[http://docs.opencart.com/en-gb/installation/](http://docs.opencart.com/en-gb/installation/)
>>>>>>> adg-production
