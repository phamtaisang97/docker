<?php
// HTTP
define('HTTP_SERVER', 'http://bestme.test/');
define('ENVIRONMENT', 'development');
define('ENVIRONMENT_VALUE', 'development');
// HTTPS
define('HTTPS_SERVER', 'http://bestme.test/');
define('HTTPS_DOMAIN_SERVER', 'http://localhost/opencart');

define('URL_SCHEME_HTTPS', 'https://');
// DIR
define('DIR_APPLICATION', '/var/www/html/bestme-v1/catalog/');
define('DIR_SYSTEM', '/var/www/html/bestme-v1/system/');
define('DIR_IMAGE', '/var/www/html/bestme-v1/image/');
define('DIR_ASSET', '/var/www/html/bestme-v1/asset/');
define('HTTP_ASSET', '/asset/');
define('DIR_STORAGE', '/var/www/html/storage/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'db');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'mypass');
define('DB_DATABASE', 'bestme_v1');
define('DB_PORT', '3306');
define('DB_PREFIX', 'cfr_');

// Cloudinary for uploading images
define('TEMPORY_IMAGE_PREFIX', 'tempory_');
define('MAX_IMAGES_UPLOAD', 1000); // 1000 images
define('MAX_IMAGE_SIZE_UPLOAD', 2097152); // 2048 kb
define('CLOUD_NAME', 'novaon-x2');
define('API_KEY', 'YOUR_API_KEY');
define('API_SECRET', 'YOUR_API_SECRET');

// SHOP_ID
define('SHOP_ID', 'x2.bestme.test');

//hot line
define('CONFIG_HOT_LINE', '094.442.7799');

// Bestme homepage
define('BESTME_SERVER', 'https://www.bestme.asia/');

// Shop name suffix
define('SHOP_NAME_SUFFIX', '.bestme.asia');

// for tracking page view session for conversion report
define('WEB_TRAFFIC_COOKIE_TTL', 1800); // 1800 seconds = 30 minute

// for increasing google speed insight, loading raw resources (css, js) instead of including files
define('REDIS_THEME_RESOURCE_KEY', 'theme_resources_');

// Bestme POS
define('BESTME_POS_SECURE_KEY', 'KPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4aKPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4aKPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4a');

// VNPAY PAYMENT PAGE URL
define('VNPAY_PAYMENT_PAGE_URL', 'https://pay.vnpay.vn/vpcpay.html');

/*
 * always at the end of file!
 * define that needed for migration
 */
// migration_20190903_sync_staffs_to_welcome
define('MUL_REDIS_HOST','127.0.0.1');
define('MUL_REDIS_PORT',6379);
define('MUL_REDIS_DB',1);
define('MUL_REDIS_CUSTOMER_STAFF_PUBLISH_CHANEL', 'UpdateCustomerStaff');

// sync_bestme_website_data_to_welcome
define('MUL_REDIS_SYNC_BESTME_WEBSITE_DATA_PUBLISH_CHANEL', 'SyncBestmeWebsiteData');

// bestme image redis config for crawling image from Cloudinary to Bestme image server
// bestme image server
define('BESTME_IMAGE_SEVER_UPLOAD_URL', 'http://127.0.0.1:8989');
define('BESTME_IMAGE_SEVER_SERVE_URL', 'http://cdn.bestme.test');
define('BESTME_IMAGE_SEVER_API_KEY', 'ccd50a36-8594-11ed-97bb-056d19430246');
define('BESTME_IMAGE_JOB_REDIS_QUEUE', 'PULL_IMAGES_FROM_CLOUDINARY');
define('MUL_REDIS_DB_BESTME_IMAGE', 2);
/* end - define that needed for migration */

// limit load product on homepage for speeding up loading time
//// Important: this const is used to avoid too much memory used and reduce loading time
//// Current set 32 products to satisfied with grid config 2, 4, 6 with 1, 2 product lines
//// TODO: admin config or choose other satisfied number...
define('LIMIT_PRODUCT_IN_BLOCK', 32);

//// special shop need speed up more
define('LIMIT_PRODUCT_IN_BLOCK_MIN', 12);
define('SHOPS_USE_LIMIT_PRODUCT_IN_BLOCK_MIN', '2tautocom'); // separated by ','
// end - limit load product on homepage for speeding up loading time

// email
define('MAIL_SMTP_HOST_NAME', 'tls://smtp.gmail.com');
define('MAIL_SMTP_PORT', '587');
define('MAIL_SMTP_USERNAME', 'bestme@mail.test');
define('MAIL_SMTP_PASSWORD', '123456');
define('MAIL_SMTP_FROM','no-reply@bestme.test');

// server serve url image bestme and url image new
define('BESTME_CURRENT_IMAGE_SERVER_SERVE_URL', 'https://cdn.bestme.asia');
define('BESTME_NEW_IMAGE_SERVER_SERVE_URL', 'https://cdn-bestme.cdn.vccloud.vn');
// end - server serve url image bestme and url image new

// redis config for caching queries
/* Define redis server for cache */
define('MUL_REDIS_HOST_CACHE', '127.0.0.1');
define('REDIS_DB_CACHE_QUERY', 0);

// Giao hang nhanh
define('TRANSPORT_GHN_GUIDE', 'https://docs.google.com/document/d/1g8Pfedb5BPVnkUKpLa_ZwUyDKvlpnymdywtsNDqtKCc/edit?usp=sharing');
define('TRANSPORT_GHN_BESTME_TOKEN', 'b577b882-c001-11ea-b354-e6945d70dd56'); // staging: b577b882-c001-11ea-b354-e6945d70dd56, production: xxx
define('TRANSPORT_GHN_BESTME_PHONE', '0977200443'); // staging: 0977200443, production: 0961680486


# Redis to save count traffic web
define('MUL_REDIS_DB_BESTME_COUNT_TRAFFICT',4);
define('BESTME_WEB_TRAFFICT_REDIS', 'BESTME_WEB_TRAFFICT_REDIS');

// rabbitmq config
define('RABBITMQ_HOST', 'localhost');
define('RABBITMQ_PORT', 5672);
define('RABBITMQ_USER_LOGIN', 'admin');
define('RABBITMQ_USER_PASSWORD', '123456');

// source production for adg
define('SOURCE_PRODUCTION_FOR_ADG', false);

define('PRODUCTION_BRAND_NAME', 'Bestme');
define('PRODUCTION_BRAND_NAME_UPPERCASE', 'BESTME');
define('PRODUCTION_BRAND_PHONE', '0961680486');

define('PRODUCTION_BRAND_FULL_NAME', 'Bestme Việt Nam');
define('BESTME_ORGANIZATION_SALE_PHONE', '1868688');
define('BESTME_ORGANIZATION_TECHNICAL_SP_PHONE', '1868688');
define('BESTME_ORGANIZATION_CUSTOMER_SP_PHONE', '1868688');
define('BESTME_ORGANIZATION_LOGO', 'http://cdn.hapee.vn/images/x2/hapee.png');

define('BESTME_ORGANIZATION_SOCIAL_NETWORK', [
    "https://www.facebook.com/",
    "https://vn.linkedin.com/",
    "https://www.youtube.com/",
    "https://www.instagram.com/",
    "https://vt.tiktok.com/",
    "https://vi.wikipedia.org/"
]);

define('URL_IMAGE_SERVER', 'http://127.0.0.1:8989');


define('BESTME_RESIZE_IMAGES', [
    "mobile" => [
        "product" => "w_147xh_147/",
        "product_detail" => "w_366xh_366/",
        "product_detail_thump" => "w_86xh_86/",
        "product_cart" => "w_63xh_63/",
        "product_cart_header" => "w_60xh_60/",
        "product_account_detail_order" => "w_67xh_67/",
        "blog_home" => "w_92xh_92/",
        "blog_list_first" => "w_366xh_197/",
        "blog_list" => "w_366xh_197/",
        "add_on_deal_product_image" => "w_110xh_110/",
    ],
    "desktop" => [
        "product" => "w_300xh_300/",
        "product_detail" => "w_500xh_500/",
        "product_detail_thump" => "w_132xh_132/",
        "product_cart" => "w_160xh_160/",
        "product_cart_header" => "w_60xh_60/",
        "product_account_detail_order" => "w_160xh_160/",
        "blog_home" => "w_137xh_137/",
        "blog_list_first" => "w_296xh_160/",
        "blog_list" => "w_296xh_160/",
        "add_on_deal_product_image" => "w_110xh_110/",
    ]
]);

define('COPYRIGHT', 'Công ty cổ phần Bestme');

define('HOME_PAGE_META_TITLE', 'Mua sản phẩm mỹ phẩm, thực phẩm chức năng chính hãng tại bestme.vn | bestme.vn');
define('HOME_PAGE_META_DESC', 'Bestme.vn, mua sản phẩm mỹ phẩm chính hãng, son môi, chăm sóc da, chăm sóc tóc, trang điểm môi, chăm sóc cơ thể, kem dưỡng da, sữa rửa mặt, xịt khoáng, giá tốt nhất thị trường');
define('HOME_PAGE_META_KEYWORD', 'Bestme cam kết bán sản phẩm thực phẩm chức năng, mỹ phẩm chính hãng 100%, giao hàng siêu tốc và là đơn vị phân phối độc quyền thương hiệu DHC Nhật Bản tại Việt Nam');

define('PRODUCTS_PAGE_META_TITLE', 'Mua sản phẩm mỹ phẩm, thực phẩm chức năng chính hãng tại bestme.vn | bestme.vn');
define('PRODUCTS_PAGE_META_DESC', 'Bestme.vn, mua sản phẩm mỹ phẩm chính hãng, son môi, chăm sóc da, chăm sóc tóc, trang điểm môi, chăm sóc cơ thể, kem dưỡng da, sữa rửa mặt, xịt khoáng, giá tốt nhất thị trường.');
define('PRODUCTS_PAGE_META_KEYWORD', 'Thực phẩm chức năng và mỹ phẩm chính hãng 100% từ Nhật Bản dưỡng da chống lão hóa, cải thiện da trắng sáng, làm mờ nếp nhăn, bổ sung dưỡng chất cho cơ thể khỏe mạnh');

// blogs/all
define('BLOGS_ALL_PAGE_META_TITLE', 'Cổng thông tin tin tức, sự kiện và kiến thức làm đẹp của Bestme');
define('BLOGS_ALL_PAGE_META_DESC', 'Truy cập ngay bestme.vn để bỏ túi thêm nhiều kinh nghiệm chăm sóc da, làm đẹp, giữ dáng, dưỡng tóc đẹp cho chị em phụ nữ. Review mỹ phẩm và thực phẩm chức năng hot, bán chạy và chất lượng tốt.');
define('BLOGS_ALL_PAGE_META_KEYWORD', 'Kho thông tin cung cấp cho khách hàng kiến thức và bí quyết về chăm sóc, làm đẹp da hữu ích, các mẹo trang điểm cho nàng thêm xinh cùng thông tin về sản phẩm trên Bestme');

define('BLOGS_PAGE_META_TITLE', 'Cổng thông tin tin tức, sự kiện và kiến thức làm đẹp của Bestme | bestme.vn');
define('BLOGS_PAGE_META_DESC', 'Bestme.vn, mua sản phẩm mỹ phẩm chính hãng, son môi, chăm sóc da, chăm sóc tóc, trang điểm môi, chăm sóc cơ thể, kem dưỡng da, sữa rửa mặt, xịt khoáng, giá tốt nhất thị trường.');
define('BLOGS_PAGE_META_KEYWORD', 'Kho thông tin cung cấp cho khách hàng kiến thức và bí quyết về chăm sóc, làm đẹp da hữu ích, các mẹo trang điểm cho nàng thêm xinh cùng thông tin về sản phẩm trên Bestme');

define('CONTACT_PAGE_META_TITLE', 'Kết nối cùng Bestme | bestme.vn');
define('CONTACT_PAGE_META_DESC', 'Bestme.vn, mua sản phẩm mỹ phẩm chính hãng, son môi, chăm sóc da, chăm sóc tóc, trang điểm môi, chăm sóc cơ thể, kem dưỡng da, sữa rửa mặt, xịt khoáng, giá tốt nhất thị trường.');
define('CONTACT_PAGE_META_KEYWORD', 'Hãy để lại thông tin cá nhân và nội dung yêu cầu tư vấn cho Bestme, chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất có thể để giải đáp thắc mắc và tư vấn cho bạn');

define('SEO_TITLE_BRAND', ' - Bestme.vn');

define('PRIVATE_COLLECTION_IDS', [74,79,75]);
define('PRIVATE_COLLECTION_IDS_CSR', [75]);
define('PRIVATE_MANUFACTURER_IDS', [3,1,9]);
define('PRIVATE_MANUFACTURER_IDS_CSR', []);

define('REVIEW_HMAC_SECREATE_KEY', 'Ec0m43v13mS3k43@');
define('REVIEW_SERVE_API_URL', 'https://review.bestme.vn');

define('BESTME_ELASTICSEARCH_SEVER_UPLOAD_URL', 'http://127.0.0.1:9200');

define('DMCA_URL', 'https://erp.bestme.vn');
define('DMCA_SRC', 'https://images.dmca.com/Badges/dmca-badge-w150-5x1-03.png');

// url articles on how to use gifts
define('URL_ARTICLES_ON_HOW_TO_USE_GIFTS', '/asdasd');

define('REVIEW_ENABLE', true);

// customer rank
define('CRM_API_URL', 'https://erp.bestme.vn/');
define('CRM_HMAC_KEY', 'BESTME@123');
define('BESTME_HMAC_KEY', '7UOtNGtvmx');
define('MIGRATE_CUSTOMER_RANK_DELAY', 500);
define('BESTME_IMAGE_SERVER_HOST', '127.0.0.1:8989');

// esms
define('ESMS_API_URL', 'https://rest.esms.vn/MainService.svc/json/');
define('ESMS_API_KEY', '');
define('ESMS_SECRET_KEY', '');

// ecom dhcvietnam
define('ECOM_API_URL', 'https://ecom.dhcvietnam.com.vn/');


// csr landding page
define('BESTME_CRM_CACHE_HOUR', 1);
define('BESTME_CRM_CSR_DATE_START', '2023-07-01 00:00:00');
define('BESTME_CRM_CSR_TARGET', 3000000000);
define('BESTME_CRM_CSR_DONATION_PERCENTAGE', 1);
define('BESTME_CRM_CSR_DONATION_START', 0);
